.class public Lio/realm/RealmList;
.super Ljava/util/AbstractList;
.source "RealmList.java"

# interfaces
.implements Lio/realm/OrderedRealmCollection;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lio/realm/RealmList$RealmListItr;,
        Lio/realm/RealmList$RealmItr;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "Lio/realm/RealmModel;",
        ">",
        "Ljava/util/AbstractList",
        "<TE;>;",
        "Lio/realm/OrderedRealmCollection",
        "<TE;>;"
    }
.end annotation


# static fields
.field private static final NULL_OBJECTS_NOT_ALLOWED_MESSAGE:Ljava/lang/String; = "RealmList does not accept null values"

.field private static final ONLY_IN_MANAGED_MODE_MESSAGE:Ljava/lang/String; = "This method is only available in managed mode"

.field public static final REMOVE_OUTSIDE_TRANSACTION_ERROR:Ljava/lang/String; = "Objects can only be removed from inside a write transaction"


# instance fields
.field protected className:Ljava/lang/String;

.field protected clazz:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<TE;>;"
        }
    .end annotation
.end field

.field private final collection:Lio/realm/internal/Collection;

.field protected realm:Lio/realm/BaseRealm;

.field private unmanagedList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<TE;>;"
        }
    .end annotation
.end field

.field final view:Lio/realm/internal/LinkView;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .local p0, "this":Lio/realm/RealmList;, "Lio/realm/RealmList<TE;>;"
    const/4 v0, 0x0

    .line 73
    invoke-direct {p0}, Ljava/util/AbstractList;-><init>()V

    .line 74
    iput-object v0, p0, Lio/realm/RealmList;->collection:Lio/realm/internal/Collection;

    .line 75
    iput-object v0, p0, Lio/realm/RealmList;->view:Lio/realm/internal/LinkView;

    .line 76
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lio/realm/RealmList;->unmanagedList:Ljava/util/List;

    .line 77
    return-void
.end method

.method constructor <init>(Ljava/lang/Class;Lio/realm/internal/LinkView;Lio/realm/BaseRealm;)V
    .locals 3
    .param p2, "linkView"    # Lio/realm/internal/LinkView;
    .param p3, "realm"    # Lio/realm/BaseRealm;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<TE;>;",
            "Lio/realm/internal/LinkView;",
            "Lio/realm/BaseRealm;",
            ")V"
        }
    .end annotation

    .prologue
    .line 105
    .local p0, "this":Lio/realm/RealmList;, "Lio/realm/RealmList<TE;>;"
    .local p1, "clazz":Ljava/lang/Class;, "Ljava/lang/Class<TE;>;"
    invoke-direct {p0}, Ljava/util/AbstractList;-><init>()V

    .line 106
    new-instance v0, Lio/realm/internal/Collection;

    iget-object v1, p3, Lio/realm/BaseRealm;->sharedRealm:Lio/realm/internal/SharedRealm;

    const/4 v2, 0x0

    invoke-direct {v0, v1, p2, v2}, Lio/realm/internal/Collection;-><init>(Lio/realm/internal/SharedRealm;Lio/realm/internal/LinkView;Lio/realm/internal/SortDescriptor;)V

    iput-object v0, p0, Lio/realm/RealmList;->collection:Lio/realm/internal/Collection;

    .line 107
    iput-object p1, p0, Lio/realm/RealmList;->clazz:Ljava/lang/Class;

    .line 108
    iput-object p2, p0, Lio/realm/RealmList;->view:Lio/realm/internal/LinkView;

    .line 109
    iput-object p3, p0, Lio/realm/RealmList;->realm:Lio/realm/BaseRealm;

    .line 110
    return-void
.end method

.method constructor <init>(Ljava/lang/String;Lio/realm/internal/LinkView;Lio/realm/BaseRealm;)V
    .locals 3
    .param p1, "className"    # Ljava/lang/String;
    .param p2, "linkView"    # Lio/realm/internal/LinkView;
    .param p3, "realm"    # Lio/realm/BaseRealm;

    .prologue
    .line 112
    .local p0, "this":Lio/realm/RealmList;, "Lio/realm/RealmList<TE;>;"
    invoke-direct {p0}, Ljava/util/AbstractList;-><init>()V

    .line 113
    new-instance v0, Lio/realm/internal/Collection;

    iget-object v1, p3, Lio/realm/BaseRealm;->sharedRealm:Lio/realm/internal/SharedRealm;

    const/4 v2, 0x0

    invoke-direct {v0, v1, p2, v2}, Lio/realm/internal/Collection;-><init>(Lio/realm/internal/SharedRealm;Lio/realm/internal/LinkView;Lio/realm/internal/SortDescriptor;)V

    iput-object v0, p0, Lio/realm/RealmList;->collection:Lio/realm/internal/Collection;

    .line 114
    iput-object p2, p0, Lio/realm/RealmList;->view:Lio/realm/internal/LinkView;

    .line 115
    iput-object p3, p0, Lio/realm/RealmList;->realm:Lio/realm/BaseRealm;

    .line 116
    iput-object p1, p0, Lio/realm/RealmList;->className:Ljava/lang/String;

    .line 117
    return-void
.end method

.method public varargs constructor <init>([Lio/realm/RealmModel;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([TE;)V"
        }
    .end annotation

    .prologue
    .local p0, "this":Lio/realm/RealmList;, "Lio/realm/RealmList<TE;>;"
    .local p1, "objects":[Lio/realm/RealmModel;, "[TE;"
    const/4 v0, 0x0

    .line 88
    invoke-direct {p0}, Ljava/util/AbstractList;-><init>()V

    .line 89
    if-nez p1, :cond_0

    .line 90
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "The objects argument cannot be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 92
    :cond_0
    iput-object v0, p0, Lio/realm/RealmList;->collection:Lio/realm/internal/Collection;

    .line 93
    iput-object v0, p0, Lio/realm/RealmList;->view:Lio/realm/internal/LinkView;

    .line 94
    new-instance v0, Ljava/util/ArrayList;

    array-length v1, p1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lio/realm/RealmList;->unmanagedList:Ljava/util/List;

    .line 95
    iget-object v0, p0, Lio/realm/RealmList;->unmanagedList:Ljava/util/List;

    invoke-static {v0, p1}, Ljava/util/Collections;->addAll(Ljava/util/Collection;[Ljava/lang/Object;)Z

    .line 96
    return-void
.end method

.method static synthetic access$100(Lio/realm/RealmList;)I
    .locals 1
    .param p0, "x0"    # Lio/realm/RealmList;

    .prologue
    .line 53
    iget v0, p0, Lio/realm/RealmList;->modCount:I

    return v0
.end method

.method static synthetic access$200(Lio/realm/RealmList;)I
    .locals 1
    .param p0, "x0"    # Lio/realm/RealmList;

    .prologue
    .line 53
    iget v0, p0, Lio/realm/RealmList;->modCount:I

    return v0
.end method

.method static synthetic access$300(Lio/realm/RealmList;)I
    .locals 1
    .param p0, "x0"    # Lio/realm/RealmList;

    .prologue
    .line 53
    iget v0, p0, Lio/realm/RealmList;->modCount:I

    return v0
.end method

.method static synthetic access$400(Lio/realm/RealmList;)I
    .locals 1
    .param p0, "x0"    # Lio/realm/RealmList;

    .prologue
    .line 53
    iget v0, p0, Lio/realm/RealmList;->modCount:I

    return v0
.end method

.method static synthetic access$500(Lio/realm/RealmList;)I
    .locals 1
    .param p0, "x0"    # Lio/realm/RealmList;

    .prologue
    .line 53
    iget v0, p0, Lio/realm/RealmList;->modCount:I

    return v0
.end method

.method private checkForAddRemoveListener(Ljava/lang/Object;Z)V
    .locals 2
    .param p1, "listener"    # Ljava/lang/Object;
    .param p2, "checkListener"    # Z

    .prologue
    .line 886
    .local p0, "this":Lio/realm/RealmList;, "Lio/realm/RealmList<TE;>;"
    if-eqz p2, :cond_0

    if-nez p1, :cond_0

    .line 887
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Listener should not be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 889
    :cond_0
    iget-object v0, p0, Lio/realm/RealmList;->realm:Lio/realm/BaseRealm;

    invoke-virtual {v0}, Lio/realm/BaseRealm;->checkIfValid()V

    .line 890
    iget-object v0, p0, Lio/realm/RealmList;->realm:Lio/realm/BaseRealm;

    iget-object v0, v0, Lio/realm/BaseRealm;->sharedRealm:Lio/realm/internal/SharedRealm;

    iget-object v0, v0, Lio/realm/internal/SharedRealm;->capabilities:Lio/realm/internal/Capabilities;

    const-string v1, "Listeners cannot be used on current thread."

    invoke-interface {v0, v1}, Lio/realm/internal/Capabilities;->checkCanDeliverNotification(Ljava/lang/String;)V

    .line 891
    return-void
.end method

.method private checkIndex(I)V
    .locals 4
    .param p1, "location"    # I

    .prologue
    .line 788
    .local p0, "this":Lio/realm/RealmList;, "Lio/realm/RealmList<TE;>;"
    invoke-virtual {p0}, Lio/realm/RealmList;->size()I

    move-result v0

    .line 789
    .local v0, "size":I
    if-ltz p1, :cond_0

    if-lt p1, v0, :cond_1

    .line 790
    :cond_0
    new-instance v1, Ljava/lang/IndexOutOfBoundsException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Invalid index "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", size is "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 792
    :cond_1
    return-void
.end method

.method private checkValidObject(Lio/realm/RealmModel;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TE;)V"
        }
    .end annotation

    .prologue
    .line 782
    .local p0, "this":Lio/realm/RealmList;, "Lio/realm/RealmList<TE;>;"
    .local p1, "object":Lio/realm/RealmModel;, "TE;"
    if-nez p1, :cond_0

    .line 783
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "RealmList does not accept null values"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 785
    :cond_0
    return-void
.end method

.method private checkValidView()V
    .locals 2

    .prologue
    .line 795
    .local p0, "this":Lio/realm/RealmList;, "Lio/realm/RealmList<TE;>;"
    iget-object v0, p0, Lio/realm/RealmList;->realm:Lio/realm/BaseRealm;

    invoke-virtual {v0}, Lio/realm/BaseRealm;->checkIfValid()V

    .line 796
    iget-object v0, p0, Lio/realm/RealmList;->view:Lio/realm/internal/LinkView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lio/realm/RealmList;->view:Lio/realm/internal/LinkView;

    invoke-virtual {v0}, Lio/realm/internal/LinkView;->isAttached()Z

    move-result v0

    if-nez v0, :cond_1

    .line 797
    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Realm instance has been closed or this object or its parent has been deleted."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 799
    :cond_1
    return-void
.end method

.method private copyToRealmIfNeeded(Lio/realm/RealmModel;)Lio/realm/RealmModel;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TE;)TE;"
        }
    .end annotation

    .prologue
    .line 245
    .local p0, "this":Lio/realm/RealmList;, "Lio/realm/RealmList<TE;>;"
    .local p1, "object":Lio/realm/RealmModel;, "TE;"
    instance-of v4, p1, Lio/realm/internal/RealmObjectProxy;

    if-eqz v4, :cond_5

    move-object v2, p1

    .line 246
    check-cast v2, Lio/realm/internal/RealmObjectProxy;

    .line 248
    .local v2, "proxy":Lio/realm/internal/RealmObjectProxy;
    instance-of v4, v2, Lio/realm/DynamicRealmObject;

    if-eqz v4, :cond_4

    .line 249
    iget-object v4, p0, Lio/realm/RealmList;->view:Lio/realm/internal/LinkView;

    invoke-virtual {v4}, Lio/realm/internal/LinkView;->getTargetTable()Lio/realm/internal/Table;

    move-result-object v4

    invoke-static {v4}, Lio/realm/RealmSchema;->getSchemaForTable(Lio/realm/internal/Table;)Ljava/lang/String;

    move-result-object v0

    .line 250
    .local v0, "listClassName":Ljava/lang/String;
    invoke-interface {v2}, Lio/realm/internal/RealmObjectProxy;->realmGet$proxyState()Lio/realm/ProxyState;

    move-result-object v4

    invoke-virtual {v4}, Lio/realm/ProxyState;->getRealm$realm()Lio/realm/BaseRealm;

    move-result-object v4

    iget-object v5, p0, Lio/realm/RealmList;->realm:Lio/realm/BaseRealm;

    if-ne v4, v5, :cond_2

    move-object v4, p1

    .line 251
    check-cast v4, Lio/realm/DynamicRealmObject;

    invoke-virtual {v4}, Lio/realm/DynamicRealmObject;->getType()Ljava/lang/String;

    move-result-object v1

    .line 252
    .local v1, "objectClassName":Ljava/lang/String;
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 284
    .end local v0    # "listClassName":Ljava/lang/String;
    .end local v1    # "objectClassName":Ljava/lang/String;
    .end local v2    # "proxy":Lio/realm/internal/RealmObjectProxy;
    .end local p1    # "object":Lio/realm/RealmModel;, "TE;"
    :cond_0
    :goto_0
    return-object p1

    .line 257
    .restart local v0    # "listClassName":Ljava/lang/String;
    .restart local v1    # "objectClassName":Ljava/lang/String;
    .restart local v2    # "proxy":Lio/realm/internal/RealmObjectProxy;
    .restart local p1    # "object":Lio/realm/RealmModel;, "TE;"
    :cond_1
    new-instance v4, Ljava/lang/IllegalArgumentException;

    const-string v5, "The object has a different type from list\'s. Type of the list is \'%s\', type of object is \'%s\'."

    const/4 v6, 0x2

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    aput-object v0, v6, v7

    const/4 v7, 0x1

    aput-object v1, v6, v7

    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 260
    .end local v1    # "objectClassName":Ljava/lang/String;
    :cond_2
    iget-object v4, p0, Lio/realm/RealmList;->realm:Lio/realm/BaseRealm;

    iget-wide v4, v4, Lio/realm/BaseRealm;->threadId:J

    invoke-interface {v2}, Lio/realm/internal/RealmObjectProxy;->realmGet$proxyState()Lio/realm/ProxyState;

    move-result-object v6

    invoke-virtual {v6}, Lio/realm/ProxyState;->getRealm$realm()Lio/realm/BaseRealm;

    move-result-object v6

    iget-wide v6, v6, Lio/realm/BaseRealm;->threadId:J

    cmp-long v4, v4, v6

    if-nez v4, :cond_3

    .line 264
    new-instance v4, Ljava/lang/IllegalArgumentException;

    const-string v5, "Cannot copy DynamicRealmObject between Realm instances."

    invoke-direct {v4, v5}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 266
    :cond_3
    new-instance v4, Ljava/lang/IllegalStateException;

    const-string v5, "Cannot copy an object to a Realm instance created in another thread."

    invoke-direct {v4, v5}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 270
    .end local v0    # "listClassName":Ljava/lang/String;
    :cond_4
    invoke-interface {v2}, Lio/realm/internal/RealmObjectProxy;->realmGet$proxyState()Lio/realm/ProxyState;

    move-result-object v4

    invoke-virtual {v4}, Lio/realm/ProxyState;->getRow$realm()Lio/realm/internal/Row;

    move-result-object v4

    if-eqz v4, :cond_5

    invoke-interface {v2}, Lio/realm/internal/RealmObjectProxy;->realmGet$proxyState()Lio/realm/ProxyState;

    move-result-object v4

    invoke-virtual {v4}, Lio/realm/ProxyState;->getRealm$realm()Lio/realm/BaseRealm;

    move-result-object v4

    invoke-virtual {v4}, Lio/realm/BaseRealm;->getPath()Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lio/realm/RealmList;->realm:Lio/realm/BaseRealm;

    invoke-virtual {v5}, Lio/realm/BaseRealm;->getPath()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_5

    .line 271
    iget-object v4, p0, Lio/realm/RealmList;->realm:Lio/realm/BaseRealm;

    invoke-interface {v2}, Lio/realm/internal/RealmObjectProxy;->realmGet$proxyState()Lio/realm/ProxyState;

    move-result-object v5

    invoke-virtual {v5}, Lio/realm/ProxyState;->getRealm$realm()Lio/realm/BaseRealm;

    move-result-object v5

    if-eq v4, v5, :cond_0

    .line 272
    new-instance v4, Ljava/lang/IllegalArgumentException;

    const-string v5, "Cannot copy an object from another Realm instance."

    invoke-direct {v4, v5}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 280
    .end local v2    # "proxy":Lio/realm/internal/RealmObjectProxy;
    :cond_5
    iget-object v3, p0, Lio/realm/RealmList;->realm:Lio/realm/BaseRealm;

    check-cast v3, Lio/realm/Realm;

    .line 281
    .local v3, "realm":Lio/realm/Realm;
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    invoke-virtual {v3, v4}, Lio/realm/Realm;->getTable(Ljava/lang/Class;)Lio/realm/internal/Table;

    move-result-object v4

    invoke-virtual {v4}, Lio/realm/internal/Table;->hasPrimaryKey()Z

    move-result v4

    if-eqz v4, :cond_6

    .line 282
    invoke-virtual {v3, p1}, Lio/realm/Realm;->copyToRealmOrUpdate(Lio/realm/RealmModel;)Lio/realm/RealmModel;

    move-result-object p1

    goto/16 :goto_0

    .line 284
    :cond_6
    invoke-virtual {v3, p1}, Lio/realm/Realm;->copyToRealm(Lio/realm/RealmModel;)Lio/realm/RealmModel;

    move-result-object p1

    goto/16 :goto_0
.end method

.method private firstImpl(ZLio/realm/RealmModel;)Lio/realm/RealmModel;
    .locals 2
    .param p1, "shouldThrow"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(ZTE;)TE;"
        }
    .end annotation

    .prologue
    .local p0, "this":Lio/realm/RealmList;, "Lio/realm/RealmList<TE;>;"
    .local p2, "defaultValue":Lio/realm/RealmModel;, "TE;"
    const/4 v1, 0x0

    .line 474
    invoke-virtual {p0}, Lio/realm/RealmList;->isManaged()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 475
    invoke-direct {p0}, Lio/realm/RealmList;->checkValidView()V

    .line 476
    iget-object v0, p0, Lio/realm/RealmList;->view:Lio/realm/internal/LinkView;

    invoke-virtual {v0}, Lio/realm/internal/LinkView;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    .line 477
    invoke-virtual {p0, v1}, Lio/realm/RealmList;->get(I)Lio/realm/RealmModel;

    move-result-object v0

    .line 486
    :goto_0
    return-object v0

    .line 479
    :cond_0
    iget-object v0, p0, Lio/realm/RealmList;->unmanagedList:Ljava/util/List;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lio/realm/RealmList;->unmanagedList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    .line 480
    iget-object v0, p0, Lio/realm/RealmList;->unmanagedList:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/realm/RealmModel;

    goto :goto_0

    .line 483
    :cond_1
    if-eqz p1, :cond_2

    .line 484
    new-instance v0, Ljava/lang/IndexOutOfBoundsException;

    const-string v1, "The list is empty."

    invoke-direct {v0, v1}, Ljava/lang/IndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2
    move-object v0, p2

    .line 486
    goto :goto_0
.end method

.method private isAttached()Z
    .locals 1

    .prologue
    .line 141
    .local p0, "this":Lio/realm/RealmList;, "Lio/realm/RealmList<TE;>;"
    iget-object v0, p0, Lio/realm/RealmList;->view:Lio/realm/internal/LinkView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lio/realm/RealmList;->view:Lio/realm/internal/LinkView;

    invoke-virtual {v0}, Lio/realm/internal/LinkView;->isAttached()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private lastImpl(ZLio/realm/RealmModel;)Lio/realm/RealmModel;
    .locals 2
    .param p1, "shouldThrow"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(ZTE;)TE;"
        }
    .end annotation

    .prologue
    .line 505
    .local p0, "this":Lio/realm/RealmList;, "Lio/realm/RealmList<TE;>;"
    .local p2, "defaultValue":Lio/realm/RealmModel;, "TE;"
    invoke-virtual {p0}, Lio/realm/RealmList;->isManaged()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 506
    invoke-direct {p0}, Lio/realm/RealmList;->checkValidView()V

    .line 507
    iget-object v0, p0, Lio/realm/RealmList;->view:Lio/realm/internal/LinkView;

    invoke-virtual {v0}, Lio/realm/internal/LinkView;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    .line 508
    iget-object v0, p0, Lio/realm/RealmList;->view:Lio/realm/internal/LinkView;

    invoke-virtual {v0}, Lio/realm/internal/LinkView;->size()J

    move-result-wide v0

    long-to-int v0, v0

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {p0, v0}, Lio/realm/RealmList;->get(I)Lio/realm/RealmModel;

    move-result-object v0

    .line 517
    :goto_0
    return-object v0

    .line 510
    :cond_0
    iget-object v0, p0, Lio/realm/RealmList;->unmanagedList:Ljava/util/List;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lio/realm/RealmList;->unmanagedList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    .line 511
    iget-object v0, p0, Lio/realm/RealmList;->unmanagedList:Ljava/util/List;

    iget-object v1, p0, Lio/realm/RealmList;->unmanagedList:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/realm/RealmModel;

    goto :goto_0

    .line 514
    :cond_1
    if-eqz p1, :cond_2

    .line 515
    new-instance v0, Ljava/lang/IndexOutOfBoundsException;

    const-string v1, "The list is empty."

    invoke-direct {v0, v1}, Ljava/lang/IndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2
    move-object v0, p2

    .line 517
    goto :goto_0
.end method


# virtual methods
.method public add(ILio/realm/RealmModel;)V
    .locals 6
    .param p1, "location"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(ITE;)V"
        }
    .end annotation

    .prologue
    .line 165
    .local p0, "this":Lio/realm/RealmList;, "Lio/realm/RealmList<TE;>;"
    .local p2, "object":Lio/realm/RealmModel;, "TE;"
    invoke-direct {p0, p2}, Lio/realm/RealmList;->checkValidObject(Lio/realm/RealmModel;)V

    .line 166
    invoke-virtual {p0}, Lio/realm/RealmList;->isManaged()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 167
    invoke-direct {p0}, Lio/realm/RealmList;->checkValidView()V

    .line 168
    if-ltz p1, :cond_0

    invoke-virtual {p0}, Lio/realm/RealmList;->size()I

    move-result v1

    if-le p1, v1, :cond_1

    .line 169
    :cond_0
    new-instance v1, Ljava/lang/IndexOutOfBoundsException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Invalid index "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", size is "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p0}, Lio/realm/RealmList;->size()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 171
    :cond_1
    invoke-direct {p0, p2}, Lio/realm/RealmList;->copyToRealmIfNeeded(Lio/realm/RealmModel;)Lio/realm/RealmModel;

    move-result-object v0

    check-cast v0, Lio/realm/internal/RealmObjectProxy;

    .line 172
    .local v0, "proxy":Lio/realm/internal/RealmObjectProxy;
    iget-object v1, p0, Lio/realm/RealmList;->view:Lio/realm/internal/LinkView;

    int-to-long v2, p1

    invoke-interface {v0}, Lio/realm/internal/RealmObjectProxy;->realmGet$proxyState()Lio/realm/ProxyState;

    move-result-object v4

    invoke-virtual {v4}, Lio/realm/ProxyState;->getRow$realm()Lio/realm/internal/Row;

    move-result-object v4

    invoke-interface {v4}, Lio/realm/internal/Row;->getIndex()J

    move-result-wide v4

    invoke-virtual {v1, v2, v3, v4, v5}, Lio/realm/internal/LinkView;->insert(JJ)V

    .line 176
    .end local v0    # "proxy":Lio/realm/internal/RealmObjectProxy;
    :goto_0
    iget v1, p0, Lio/realm/RealmList;->modCount:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lio/realm/RealmList;->modCount:I

    .line 177
    return-void

    .line 174
    :cond_2
    iget-object v1, p0, Lio/realm/RealmList;->unmanagedList:Ljava/util/List;

    invoke-interface {v1, p1, p2}, Ljava/util/List;->add(ILjava/lang/Object;)V

    goto :goto_0
.end method

.method public bridge synthetic add(ILjava/lang/Object;)V
    .locals 0

    .prologue
    .line 53
    .local p0, "this":Lio/realm/RealmList;, "Lio/realm/RealmList<TE;>;"
    check-cast p2, Lio/realm/RealmModel;

    invoke-virtual {p0, p1, p2}, Lio/realm/RealmList;->add(ILio/realm/RealmModel;)V

    return-void
.end method

.method public add(Lio/realm/RealmModel;)Z
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TE;)Z"
        }
    .end annotation

    .prologue
    .line 197
    .local p0, "this":Lio/realm/RealmList;, "Lio/realm/RealmList<TE;>;"
    .local p1, "object":Lio/realm/RealmModel;, "TE;"
    invoke-direct {p0, p1}, Lio/realm/RealmList;->checkValidObject(Lio/realm/RealmModel;)V

    .line 198
    invoke-virtual {p0}, Lio/realm/RealmList;->isManaged()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 199
    invoke-direct {p0}, Lio/realm/RealmList;->checkValidView()V

    .line 200
    invoke-direct {p0, p1}, Lio/realm/RealmList;->copyToRealmIfNeeded(Lio/realm/RealmModel;)Lio/realm/RealmModel;

    move-result-object v0

    check-cast v0, Lio/realm/internal/RealmObjectProxy;

    .line 201
    .local v0, "proxy":Lio/realm/internal/RealmObjectProxy;
    iget-object v1, p0, Lio/realm/RealmList;->view:Lio/realm/internal/LinkView;

    invoke-interface {v0}, Lio/realm/internal/RealmObjectProxy;->realmGet$proxyState()Lio/realm/ProxyState;

    move-result-object v2

    invoke-virtual {v2}, Lio/realm/ProxyState;->getRow$realm()Lio/realm/internal/Row;

    move-result-object v2

    invoke-interface {v2}, Lio/realm/internal/Row;->getIndex()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Lio/realm/internal/LinkView;->add(J)V

    .line 205
    .end local v0    # "proxy":Lio/realm/internal/RealmObjectProxy;
    :goto_0
    iget v1, p0, Lio/realm/RealmList;->modCount:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lio/realm/RealmList;->modCount:I

    .line 206
    const/4 v1, 0x1

    return v1

    .line 203
    :cond_0
    iget-object v1, p0, Lio/realm/RealmList;->unmanagedList:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public bridge synthetic add(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 53
    .local p0, "this":Lio/realm/RealmList;, "Lio/realm/RealmList<TE;>;"
    check-cast p1, Lio/realm/RealmModel;

    invoke-virtual {p0, p1}, Lio/realm/RealmList;->add(Lio/realm/RealmModel;)Z

    move-result v0

    return v0
.end method

.method public addChangeListener(Lio/realm/OrderedRealmCollectionChangeListener;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/realm/OrderedRealmCollectionChangeListener",
            "<",
            "Lio/realm/RealmList",
            "<TE;>;>;)V"
        }
    .end annotation

    .prologue
    .line 902
    .local p0, "this":Lio/realm/RealmList;, "Lio/realm/RealmList<TE;>;"
    .local p1, "listener":Lio/realm/OrderedRealmCollectionChangeListener;, "Lio/realm/OrderedRealmCollectionChangeListener<Lio/realm/RealmList<TE;>;>;"
    const/4 v0, 0x1

    invoke-direct {p0, p1, v0}, Lio/realm/RealmList;->checkForAddRemoveListener(Ljava/lang/Object;Z)V

    .line 903
    iget-object v0, p0, Lio/realm/RealmList;->collection:Lio/realm/internal/Collection;

    invoke-virtual {v0, p0, p1}, Lio/realm/internal/Collection;->addListener(Ljava/lang/Object;Lio/realm/OrderedRealmCollectionChangeListener;)V

    .line 904
    return-void
.end method

.method public addChangeListener(Lio/realm/RealmChangeListener;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/realm/RealmChangeListener",
            "<",
            "Lio/realm/RealmList",
            "<TE;>;>;)V"
        }
    .end annotation

    .prologue
    .line 928
    .local p0, "this":Lio/realm/RealmList;, "Lio/realm/RealmList<TE;>;"
    .local p1, "listener":Lio/realm/RealmChangeListener;, "Lio/realm/RealmChangeListener<Lio/realm/RealmList<TE;>;>;"
    const/4 v0, 0x1

    invoke-direct {p0, p1, v0}, Lio/realm/RealmList;->checkForAddRemoveListener(Ljava/lang/Object;Z)V

    .line 929
    iget-object v0, p0, Lio/realm/RealmList;->collection:Lio/realm/internal/Collection;

    invoke-virtual {v0, p0, p1}, Lio/realm/internal/Collection;->addListener(Ljava/lang/Object;Lio/realm/RealmChangeListener;)V

    .line 930
    return-void
.end method

.method public asObservable()Lrx/Observable;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable",
            "<",
            "Lio/realm/RealmList",
            "<TE;>;>;"
        }
    .end annotation

    .prologue
    .line 872
    .local p0, "this":Lio/realm/RealmList;, "Lio/realm/RealmList<TE;>;"
    iget-object v3, p0, Lio/realm/RealmList;->realm:Lio/realm/BaseRealm;

    instance-of v3, v3, Lio/realm/Realm;

    if-eqz v3, :cond_0

    .line 873
    iget-object v3, p0, Lio/realm/RealmList;->realm:Lio/realm/BaseRealm;

    iget-object v3, v3, Lio/realm/BaseRealm;->configuration:Lio/realm/RealmConfiguration;

    invoke-virtual {v3}, Lio/realm/RealmConfiguration;->getRxFactory()Lio/realm/rx/RxObservableFactory;

    move-result-object v4

    iget-object v3, p0, Lio/realm/RealmList;->realm:Lio/realm/BaseRealm;

    check-cast v3, Lio/realm/Realm;

    invoke-interface {v4, v3, p0}, Lio/realm/rx/RxObservableFactory;->from(Lio/realm/Realm;Lio/realm/RealmList;)Lrx/Observable;

    move-result-object v2

    .line 879
    :goto_0
    return-object v2

    .line 874
    :cond_0
    iget-object v3, p0, Lio/realm/RealmList;->realm:Lio/realm/BaseRealm;

    instance-of v3, v3, Lio/realm/DynamicRealm;

    if-eqz v3, :cond_1

    .line 875
    iget-object v1, p0, Lio/realm/RealmList;->realm:Lio/realm/BaseRealm;

    check-cast v1, Lio/realm/DynamicRealm;

    .line 876
    .local v1, "dynamicRealm":Lio/realm/DynamicRealm;
    move-object v0, p0

    .line 878
    .local v0, "dynamicList":Lio/realm/RealmList;, "Lio/realm/RealmList<Lio/realm/DynamicRealmObject;>;"
    iget-object v3, p0, Lio/realm/RealmList;->realm:Lio/realm/BaseRealm;

    iget-object v3, v3, Lio/realm/BaseRealm;->configuration:Lio/realm/RealmConfiguration;

    invoke-virtual {v3}, Lio/realm/RealmConfiguration;->getRxFactory()Lio/realm/rx/RxObservableFactory;

    move-result-object v3

    invoke-interface {v3, v1, v0}, Lio/realm/rx/RxObservableFactory;->from(Lio/realm/DynamicRealm;Lio/realm/RealmList;)Lrx/Observable;

    move-result-object v2

    .line 879
    .local v2, "results":Lrx/Observable;
    goto :goto_0

    .line 881
    .end local v0    # "dynamicList":Lio/realm/RealmList;, "Lio/realm/RealmList<Lio/realm/DynamicRealmObject;>;"
    .end local v1    # "dynamicRealm":Lio/realm/DynamicRealm;
    .end local v2    # "results":Lrx/Observable;
    :cond_1
    new-instance v3, Ljava/lang/UnsupportedOperationException;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v5, p0, Lio/realm/RealmList;->realm:Lio/realm/BaseRealm;

    invoke-virtual {v5}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " does not support RxJava."

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v3
.end method

.method public average(Ljava/lang/String;)D
    .locals 2
    .param p1, "fieldName"    # Ljava/lang/String;

    .prologue
    .line 649
    .local p0, "this":Lio/realm/RealmList;, "Lio/realm/RealmList<TE;>;"
    invoke-virtual {p0}, Lio/realm/RealmList;->isManaged()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 650
    invoke-virtual {p0}, Lio/realm/RealmList;->where()Lio/realm/RealmQuery;

    move-result-object v0

    invoke-virtual {v0, p1}, Lio/realm/RealmQuery;->average(Ljava/lang/String;)D

    move-result-wide v0

    return-wide v0

    .line 652
    :cond_0
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "This method is only available in managed mode"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public clear()V
    .locals 1

    .prologue
    .line 324
    .local p0, "this":Lio/realm/RealmList;, "Lio/realm/RealmList<TE;>;"
    invoke-virtual {p0}, Lio/realm/RealmList;->isManaged()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 325
    invoke-direct {p0}, Lio/realm/RealmList;->checkValidView()V

    .line 326
    iget-object v0, p0, Lio/realm/RealmList;->view:Lio/realm/internal/LinkView;

    invoke-virtual {v0}, Lio/realm/internal/LinkView;->clear()V

    .line 330
    :goto_0
    iget v0, p0, Lio/realm/RealmList;->modCount:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lio/realm/RealmList;->modCount:I

    .line 331
    return-void

    .line 328
    :cond_0
    iget-object v0, p0, Lio/realm/RealmList;->unmanagedList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    goto :goto_0
.end method

.method public contains(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "object"    # Ljava/lang/Object;

    .prologue
    .local p0, "this":Lio/realm/RealmList;, "Lio/realm/RealmList<TE;>;"
    const/4 v2, 0x0

    .line 727
    invoke-virtual {p0}, Lio/realm/RealmList;->isManaged()Z

    move-result v3

    if-eqz v3, :cond_3

    .line 728
    iget-object v3, p0, Lio/realm/RealmList;->realm:Lio/realm/BaseRealm;

    invoke-virtual {v3}, Lio/realm/BaseRealm;->checkIfValid()V

    .line 731
    instance-of v3, p1, Lio/realm/internal/RealmObjectProxy;

    if-eqz v3, :cond_1

    move-object v1, p1

    .line 732
    check-cast v1, Lio/realm/internal/RealmObjectProxy;

    .line 733
    .local v1, "proxy":Lio/realm/internal/RealmObjectProxy;
    invoke-interface {v1}, Lio/realm/internal/RealmObjectProxy;->realmGet$proxyState()Lio/realm/ProxyState;

    move-result-object v3

    invoke-virtual {v3}, Lio/realm/ProxyState;->getRow$realm()Lio/realm/internal/Row;

    move-result-object v3

    sget-object v4, Lio/realm/internal/InvalidRow;->INSTANCE:Lio/realm/internal/InvalidRow;

    if-ne v3, v4, :cond_1

    .line 745
    .end local v1    # "proxy":Lio/realm/internal/RealmObjectProxy;
    :cond_0
    :goto_0
    return v2

    .line 738
    :cond_1
    invoke-virtual {p0}, Lio/realm/RealmList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/realm/RealmModel;

    .line 739
    .local v0, "e":Lio/realm/RealmModel;, "TE;"
    invoke-virtual {v0, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 740
    const/4 v2, 0x1

    goto :goto_0

    .line 745
    .end local v0    # "e":Lio/realm/RealmModel;, "TE;"
    :cond_3
    iget-object v2, p0, Lio/realm/RealmList;->unmanagedList:Ljava/util/List;

    invoke-interface {v2, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v2

    goto :goto_0
.end method

.method public createSnapshot()Lio/realm/OrderedRealmCollectionSnapshot;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/realm/OrderedRealmCollectionSnapshot",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .local p0, "this":Lio/realm/RealmList;, "Lio/realm/RealmList<TE;>;"
    const/4 v5, 0x0

    .line 806
    invoke-virtual {p0}, Lio/realm/RealmList;->isManaged()Z

    move-result v0

    if-nez v0, :cond_0

    .line 807
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "This method is only available in managed mode"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 809
    :cond_0
    invoke-direct {p0}, Lio/realm/RealmList;->checkValidView()V

    .line 810
    iget-object v0, p0, Lio/realm/RealmList;->className:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 811
    new-instance v0, Lio/realm/OrderedRealmCollectionSnapshot;

    iget-object v1, p0, Lio/realm/RealmList;->realm:Lio/realm/BaseRealm;

    new-instance v2, Lio/realm/internal/Collection;

    iget-object v3, p0, Lio/realm/RealmList;->realm:Lio/realm/BaseRealm;

    iget-object v3, v3, Lio/realm/BaseRealm;->sharedRealm:Lio/realm/internal/SharedRealm;

    iget-object v4, p0, Lio/realm/RealmList;->view:Lio/realm/internal/LinkView;

    invoke-direct {v2, v3, v4, v5}, Lio/realm/internal/Collection;-><init>(Lio/realm/internal/SharedRealm;Lio/realm/internal/LinkView;Lio/realm/internal/SortDescriptor;)V

    iget-object v3, p0, Lio/realm/RealmList;->className:Ljava/lang/String;

    invoke-direct {v0, v1, v2, v3}, Lio/realm/OrderedRealmCollectionSnapshot;-><init>(Lio/realm/BaseRealm;Lio/realm/internal/Collection;Ljava/lang/String;)V

    .line 815
    :goto_0
    return-object v0

    :cond_1
    new-instance v0, Lio/realm/OrderedRealmCollectionSnapshot;

    iget-object v1, p0, Lio/realm/RealmList;->realm:Lio/realm/BaseRealm;

    new-instance v2, Lio/realm/internal/Collection;

    iget-object v3, p0, Lio/realm/RealmList;->realm:Lio/realm/BaseRealm;

    iget-object v3, v3, Lio/realm/BaseRealm;->sharedRealm:Lio/realm/internal/SharedRealm;

    iget-object v4, p0, Lio/realm/RealmList;->view:Lio/realm/internal/LinkView;

    invoke-direct {v2, v3, v4, v5}, Lio/realm/internal/Collection;-><init>(Lio/realm/internal/SharedRealm;Lio/realm/internal/LinkView;Lio/realm/internal/SortDescriptor;)V

    iget-object v3, p0, Lio/realm/RealmList;->clazz:Ljava/lang/Class;

    invoke-direct {v0, v1, v2, v3}, Lio/realm/OrderedRealmCollectionSnapshot;-><init>(Lio/realm/BaseRealm;Lio/realm/internal/Collection;Ljava/lang/Class;)V

    goto :goto_0
.end method

.method public deleteAllFromRealm()Z
    .locals 2

    .prologue
    .line 685
    .local p0, "this":Lio/realm/RealmList;, "Lio/realm/RealmList<TE;>;"
    invoke-virtual {p0}, Lio/realm/RealmList;->isManaged()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 686
    invoke-direct {p0}, Lio/realm/RealmList;->checkValidView()V

    .line 687
    invoke-virtual {p0}, Lio/realm/RealmList;->size()I

    move-result v0

    if-lez v0, :cond_0

    .line 688
    iget-object v0, p0, Lio/realm/RealmList;->view:Lio/realm/internal/LinkView;

    invoke-virtual {v0}, Lio/realm/internal/LinkView;->removeAllTargetRows()V

    .line 689
    iget v0, p0, Lio/realm/RealmList;->modCount:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lio/realm/RealmList;->modCount:I

    .line 690
    const/4 v0, 0x1

    .line 692
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 695
    :cond_1
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "This method is only available in managed mode"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public deleteFirstFromRealm()Z
    .locals 2

    .prologue
    .local p0, "this":Lio/realm/RealmList;, "Lio/realm/RealmList<TE;>;"
    const/4 v0, 0x0

    .line 409
    invoke-virtual {p0}, Lio/realm/RealmList;->isManaged()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 410
    invoke-virtual {p0}, Lio/realm/RealmList;->size()I

    move-result v1

    if-lez v1, :cond_0

    .line 411
    invoke-virtual {p0, v0}, Lio/realm/RealmList;->deleteFromRealm(I)V

    .line 412
    iget v0, p0, Lio/realm/RealmList;->modCount:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lio/realm/RealmList;->modCount:I

    .line 413
    const/4 v0, 0x1

    .line 415
    :cond_0
    return v0

    .line 418
    :cond_1
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "This method is only available in managed mode"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public deleteFromRealm(I)V
    .locals 2
    .param p1, "location"    # I

    .prologue
    .line 566
    .local p0, "this":Lio/realm/RealmList;, "Lio/realm/RealmList<TE;>;"
    invoke-virtual {p0}, Lio/realm/RealmList;->isManaged()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 567
    invoke-direct {p0}, Lio/realm/RealmList;->checkValidView()V

    .line 568
    iget-object v0, p0, Lio/realm/RealmList;->view:Lio/realm/internal/LinkView;

    invoke-virtual {v0, p1}, Lio/realm/internal/LinkView;->removeTargetRow(I)V

    .line 569
    iget v0, p0, Lio/realm/RealmList;->modCount:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lio/realm/RealmList;->modCount:I

    .line 573
    return-void

    .line 571
    :cond_0
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "This method is only available in managed mode"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public deleteLastFromRealm()Z
    .locals 2

    .prologue
    .line 427
    .local p0, "this":Lio/realm/RealmList;, "Lio/realm/RealmList<TE;>;"
    invoke-virtual {p0}, Lio/realm/RealmList;->isManaged()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 428
    invoke-virtual {p0}, Lio/realm/RealmList;->size()I

    move-result v0

    if-lez v0, :cond_0

    .line 429
    invoke-virtual {p0}, Lio/realm/RealmList;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {p0, v0}, Lio/realm/RealmList;->deleteFromRealm(I)V

    .line 430
    iget v0, p0, Lio/realm/RealmList;->modCount:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lio/realm/RealmList;->modCount:I

    .line 431
    const/4 v0, 0x1

    .line 433
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 436
    :cond_1
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "This method is only available in managed mode"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public first()Lio/realm/RealmModel;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TE;"
        }
    .end annotation

    .prologue
    .line 463
    .local p0, "this":Lio/realm/RealmList;, "Lio/realm/RealmList<TE;>;"
    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lio/realm/RealmList;->firstImpl(ZLio/realm/RealmModel;)Lio/realm/RealmModel;

    move-result-object v0

    return-object v0
.end method

.method public first(Lio/realm/RealmModel;)Lio/realm/RealmModel;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TE;)TE;"
        }
    .end annotation

    .prologue
    .line 470
    .local p0, "this":Lio/realm/RealmList;, "Lio/realm/RealmList<TE;>;"
    .local p1, "defaultValue":Lio/realm/RealmModel;, "TE;"
    const/4 v0, 0x0

    invoke-direct {p0, v0, p1}, Lio/realm/RealmList;->firstImpl(ZLio/realm/RealmModel;)Lio/realm/RealmModel;

    move-result-object v0

    return-object v0
.end method

.method public get(I)Lio/realm/RealmModel;
    .locals 6
    .param p1, "location"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)TE;"
        }
    .end annotation

    .prologue
    .line 450
    .local p0, "this":Lio/realm/RealmList;, "Lio/realm/RealmList<TE;>;"
    invoke-virtual {p0}, Lio/realm/RealmList;->isManaged()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 451
    invoke-direct {p0}, Lio/realm/RealmList;->checkValidView()V

    .line 452
    iget-object v2, p0, Lio/realm/RealmList;->view:Lio/realm/internal/LinkView;

    int-to-long v4, p1

    invoke-virtual {v2, v4, v5}, Lio/realm/internal/LinkView;->getTargetRowIndex(J)J

    move-result-wide v0

    .line 453
    .local v0, "rowIndex":J
    iget-object v2, p0, Lio/realm/RealmList;->realm:Lio/realm/BaseRealm;

    iget-object v3, p0, Lio/realm/RealmList;->clazz:Ljava/lang/Class;

    iget-object v4, p0, Lio/realm/RealmList;->className:Ljava/lang/String;

    invoke-virtual {v2, v3, v4, v0, v1}, Lio/realm/BaseRealm;->get(Ljava/lang/Class;Ljava/lang/String;J)Lio/realm/RealmModel;

    move-result-object v2

    .line 455
    .end local v0    # "rowIndex":J
    :goto_0
    return-object v2

    :cond_0
    iget-object v2, p0, Lio/realm/RealmList;->unmanagedList:Ljava/util/List;

    invoke-interface {v2, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lio/realm/RealmModel;

    goto :goto_0
.end method

.method public bridge synthetic get(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 53
    .local p0, "this":Lio/realm/RealmList;, "Lio/realm/RealmList<TE;>;"
    invoke-virtual {p0, p1}, Lio/realm/RealmList;->get(I)Lio/realm/RealmModel;

    move-result-object v0

    return-object v0
.end method

.method public isLoaded()Z
    .locals 1

    .prologue
    .line 704
    .local p0, "this":Lio/realm/RealmList;, "Lio/realm/RealmList<TE;>;"
    const/4 v0, 0x1

    return v0
.end method

.method public isManaged()Z
    .locals 1

    .prologue
    .line 137
    .local p0, "this":Lio/realm/RealmList;, "Lio/realm/RealmList<TE;>;"
    iget-object v0, p0, Lio/realm/RealmList;->realm:Lio/realm/BaseRealm;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isValid()Z
    .locals 1

    .prologue
    .line 123
    .local p0, "this":Lio/realm/RealmList;, "Lio/realm/RealmList<TE;>;"
    iget-object v0, p0, Lio/realm/RealmList;->realm:Lio/realm/BaseRealm;

    if-nez v0, :cond_0

    .line 124
    const/4 v0, 0x1

    .line 130
    :goto_0
    return v0

    .line 127
    :cond_0
    iget-object v0, p0, Lio/realm/RealmList;->realm:Lio/realm/BaseRealm;

    invoke-virtual {v0}, Lio/realm/BaseRealm;->isClosed()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 128
    const/4 v0, 0x0

    goto :goto_0

    .line 130
    :cond_1
    invoke-direct {p0}, Lio/realm/RealmList;->isAttached()Z

    move-result v0

    goto :goto_0
.end method

.method public iterator()Ljava/util/Iterator;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Iterator",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 754
    .local p0, "this":Lio/realm/RealmList;, "Lio/realm/RealmList<TE;>;"
    invoke-virtual {p0}, Lio/realm/RealmList;->isManaged()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 755
    new-instance v0, Lio/realm/RealmList$RealmItr;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lio/realm/RealmList$RealmItr;-><init>(Lio/realm/RealmList;Lio/realm/RealmList$1;)V

    .line 757
    :goto_0
    return-object v0

    :cond_0
    invoke-super {p0}, Ljava/util/AbstractList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    goto :goto_0
.end method

.method public last()Lio/realm/RealmModel;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TE;"
        }
    .end annotation

    .prologue
    .line 494
    .local p0, "this":Lio/realm/RealmList;, "Lio/realm/RealmList<TE;>;"
    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lio/realm/RealmList;->lastImpl(ZLio/realm/RealmModel;)Lio/realm/RealmModel;

    move-result-object v0

    return-object v0
.end method

.method public last(Lio/realm/RealmModel;)Lio/realm/RealmModel;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TE;)TE;"
        }
    .end annotation

    .prologue
    .line 501
    .local p0, "this":Lio/realm/RealmList;, "Lio/realm/RealmList<TE;>;"
    .local p1, "defaultValue":Lio/realm/RealmModel;, "TE;"
    const/4 v0, 0x0

    invoke-direct {p0, v0, p1}, Lio/realm/RealmList;->lastImpl(ZLio/realm/RealmModel;)Lio/realm/RealmModel;

    move-result-object v0

    return-object v0
.end method

.method public listIterator()Ljava/util/ListIterator;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ListIterator",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 766
    .local p0, "this":Lio/realm/RealmList;, "Lio/realm/RealmList<TE;>;"
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lio/realm/RealmList;->listIterator(I)Ljava/util/ListIterator;

    move-result-object v0

    return-object v0
.end method

.method public listIterator(I)Ljava/util/ListIterator;
    .locals 1
    .param p1, "location"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Ljava/util/ListIterator",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 774
    .local p0, "this":Lio/realm/RealmList;, "Lio/realm/RealmList<TE;>;"
    invoke-virtual {p0}, Lio/realm/RealmList;->isManaged()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 775
    new-instance v0, Lio/realm/RealmList$RealmListItr;

    invoke-direct {v0, p0, p1}, Lio/realm/RealmList$RealmListItr;-><init>(Lio/realm/RealmList;I)V

    .line 777
    :goto_0
    return-object v0

    :cond_0
    invoke-super {p0, p1}, Ljava/util/AbstractList;->listIterator(I)Ljava/util/ListIterator;

    move-result-object v0

    goto :goto_0
.end method

.method public load()Z
    .locals 1

    .prologue
    .line 712
    .local p0, "this":Lio/realm/RealmList;, "Lio/realm/RealmList<TE;>;"
    const/4 v0, 0x1

    return v0
.end method

.method public max(Ljava/lang/String;)Ljava/lang/Number;
    .locals 2
    .param p1, "fieldName"    # Ljava/lang/String;

    .prologue
    .line 625
    .local p0, "this":Lio/realm/RealmList;, "Lio/realm/RealmList<TE;>;"
    invoke-virtual {p0}, Lio/realm/RealmList;->isManaged()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 626
    invoke-virtual {p0}, Lio/realm/RealmList;->where()Lio/realm/RealmQuery;

    move-result-object v0

    invoke-virtual {v0, p1}, Lio/realm/RealmQuery;->max(Ljava/lang/String;)Ljava/lang/Number;

    move-result-object v0

    return-object v0

    .line 628
    :cond_0
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "This method is only available in managed mode"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public maxDate(Ljava/lang/String;)Ljava/util/Date;
    .locals 2
    .param p1, "fieldName"    # Ljava/lang/String;

    .prologue
    .line 661
    .local p0, "this":Lio/realm/RealmList;, "Lio/realm/RealmList<TE;>;"
    invoke-virtual {p0}, Lio/realm/RealmList;->isManaged()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 662
    invoke-virtual {p0}, Lio/realm/RealmList;->where()Lio/realm/RealmQuery;

    move-result-object v0

    invoke-virtual {v0, p1}, Lio/realm/RealmQuery;->maximumDate(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v0

    return-object v0

    .line 664
    :cond_0
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "This method is only available in managed mode"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public min(Ljava/lang/String;)Ljava/lang/Number;
    .locals 2
    .param p1, "fieldName"    # Ljava/lang/String;

    .prologue
    .line 613
    .local p0, "this":Lio/realm/RealmList;, "Lio/realm/RealmList<TE;>;"
    invoke-virtual {p0}, Lio/realm/RealmList;->isManaged()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 614
    invoke-virtual {p0}, Lio/realm/RealmList;->where()Lio/realm/RealmQuery;

    move-result-object v0

    invoke-virtual {v0, p1}, Lio/realm/RealmQuery;->min(Ljava/lang/String;)Ljava/lang/Number;

    move-result-object v0

    return-object v0

    .line 616
    :cond_0
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "This method is only available in managed mode"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public minDate(Ljava/lang/String;)Ljava/util/Date;
    .locals 2
    .param p1, "fieldName"    # Ljava/lang/String;

    .prologue
    .line 673
    .local p0, "this":Lio/realm/RealmList;, "Lio/realm/RealmList<TE;>;"
    invoke-virtual {p0}, Lio/realm/RealmList;->isManaged()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 674
    invoke-virtual {p0}, Lio/realm/RealmList;->where()Lio/realm/RealmQuery;

    move-result-object v0

    invoke-virtual {v0, p1}, Lio/realm/RealmQuery;->minimumDate(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v0

    return-object v0

    .line 676
    :cond_0
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "This method is only available in managed mode"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public move(II)V
    .locals 6
    .param p1, "oldPos"    # I
    .param p2, "newPos"    # I

    .prologue
    .line 299
    .local p0, "this":Lio/realm/RealmList;, "Lio/realm/RealmList<TE;>;"
    invoke-virtual {p0}, Lio/realm/RealmList;->isManaged()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 300
    invoke-direct {p0}, Lio/realm/RealmList;->checkValidView()V

    .line 301
    iget-object v1, p0, Lio/realm/RealmList;->view:Lio/realm/internal/LinkView;

    int-to-long v2, p1

    int-to-long v4, p2

    invoke-virtual {v1, v2, v3, v4, v5}, Lio/realm/internal/LinkView;->move(JJ)V

    .line 312
    :goto_0
    return-void

    .line 303
    :cond_0
    invoke-direct {p0, p1}, Lio/realm/RealmList;->checkIndex(I)V

    .line 304
    invoke-direct {p0, p2}, Lio/realm/RealmList;->checkIndex(I)V

    .line 305
    iget-object v1, p0, Lio/realm/RealmList;->unmanagedList:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/realm/RealmModel;

    .line 306
    .local v0, "object":Lio/realm/RealmModel;, "TE;"
    if-le p2, p1, :cond_1

    .line 307
    iget-object v1, p0, Lio/realm/RealmList;->unmanagedList:Ljava/util/List;

    add-int/lit8 v2, p2, -0x1

    invoke-interface {v1, v2, v0}, Ljava/util/List;->add(ILjava/lang/Object;)V

    goto :goto_0

    .line 309
    :cond_1
    iget-object v1, p0, Lio/realm/RealmList;->unmanagedList:Ljava/util/List;

    invoke-interface {v1, p2, v0}, Ljava/util/List;->add(ILjava/lang/Object;)V

    goto :goto_0
.end method

.method public remove(I)Lio/realm/RealmModel;
    .locals 4
    .param p1, "location"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)TE;"
        }
    .end annotation

    .prologue
    .line 344
    .local p0, "this":Lio/realm/RealmList;, "Lio/realm/RealmList<TE;>;"
    invoke-virtual {p0}, Lio/realm/RealmList;->isManaged()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 345
    invoke-direct {p0}, Lio/realm/RealmList;->checkValidView()V

    .line 346
    invoke-virtual {p0, p1}, Lio/realm/RealmList;->get(I)Lio/realm/RealmModel;

    move-result-object v0

    .line 347
    .local v0, "removedItem":Lio/realm/RealmModel;, "TE;"
    iget-object v1, p0, Lio/realm/RealmList;->view:Lio/realm/internal/LinkView;

    int-to-long v2, p1

    invoke-virtual {v1, v2, v3}, Lio/realm/internal/LinkView;->remove(J)V

    .line 351
    :goto_0
    iget v1, p0, Lio/realm/RealmList;->modCount:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lio/realm/RealmList;->modCount:I

    .line 352
    return-object v0

    .line 349
    .end local v0    # "removedItem":Lio/realm/RealmModel;, "TE;"
    :cond_0
    iget-object v1, p0, Lio/realm/RealmList;->unmanagedList:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/realm/RealmModel;

    .restart local v0    # "removedItem":Lio/realm/RealmModel;, "TE;"
    goto :goto_0
.end method

.method public bridge synthetic remove(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 53
    .local p0, "this":Lio/realm/RealmList;, "Lio/realm/RealmList<TE;>;"
    invoke-virtual {p0, p1}, Lio/realm/RealmList;->remove(I)Lio/realm/RealmModel;

    move-result-object v0

    return-object v0
.end method

.method public remove(Ljava/lang/Object;)Z
    .locals 2
    .param p1, "object"    # Ljava/lang/Object;

    .prologue
    .line 374
    .local p0, "this":Lio/realm/RealmList;, "Lio/realm/RealmList<TE;>;"
    invoke-virtual {p0}, Lio/realm/RealmList;->isManaged()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lio/realm/RealmList;->realm:Lio/realm/BaseRealm;

    invoke-virtual {v0}, Lio/realm/BaseRealm;->isInTransaction()Z

    move-result v0

    if-nez v0, :cond_0

    .line 375
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Objects can only be removed from inside a write transaction"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 377
    :cond_0
    invoke-super {p0, p1}, Ljava/util/AbstractList;->remove(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public removeAll(Ljava/util/Collection;)Z
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<*>;)Z"
        }
    .end annotation

    .prologue
    .line 398
    .local p0, "this":Lio/realm/RealmList;, "Lio/realm/RealmList<TE;>;"
    .local p1, "collection":Ljava/util/Collection;, "Ljava/util/Collection<*>;"
    invoke-virtual {p0}, Lio/realm/RealmList;->isManaged()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lio/realm/RealmList;->realm:Lio/realm/BaseRealm;

    invoke-virtual {v0}, Lio/realm/BaseRealm;->isInTransaction()Z

    move-result v0

    if-nez v0, :cond_0

    .line 399
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Objects can only be removed from inside a write transaction"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 401
    :cond_0
    invoke-super {p0, p1}, Ljava/util/AbstractList;->removeAll(Ljava/util/Collection;)Z

    move-result v0

    return v0
.end method

.method public removeAllChangeListeners()V
    .locals 2

    .prologue
    .line 952
    .local p0, "this":Lio/realm/RealmList;, "Lio/realm/RealmList<TE;>;"
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lio/realm/RealmList;->checkForAddRemoveListener(Ljava/lang/Object;Z)V

    .line 953
    iget-object v0, p0, Lio/realm/RealmList;->collection:Lio/realm/internal/Collection;

    invoke-virtual {v0}, Lio/realm/internal/Collection;->removeAllListeners()V

    .line 954
    return-void
.end method

.method public removeChangeListener(Lio/realm/OrderedRealmCollectionChangeListener;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/realm/OrderedRealmCollectionChangeListener",
            "<",
            "Lio/realm/RealmList",
            "<TE;>;>;)V"
        }
    .end annotation

    .prologue
    .line 915
    .local p0, "this":Lio/realm/RealmList;, "Lio/realm/RealmList<TE;>;"
    .local p1, "listener":Lio/realm/OrderedRealmCollectionChangeListener;, "Lio/realm/OrderedRealmCollectionChangeListener<Lio/realm/RealmList<TE;>;>;"
    const/4 v0, 0x1

    invoke-direct {p0, p1, v0}, Lio/realm/RealmList;->checkForAddRemoveListener(Ljava/lang/Object;Z)V

    .line 916
    iget-object v0, p0, Lio/realm/RealmList;->collection:Lio/realm/internal/Collection;

    invoke-virtual {v0, p0, p1}, Lio/realm/internal/Collection;->removeListener(Ljava/lang/Object;Lio/realm/OrderedRealmCollectionChangeListener;)V

    .line 917
    return-void
.end method

.method public removeChangeListener(Lio/realm/RealmChangeListener;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/realm/RealmChangeListener",
            "<",
            "Lio/realm/RealmList",
            "<TE;>;>;)V"
        }
    .end annotation

    .prologue
    .line 941
    .local p0, "this":Lio/realm/RealmList;, "Lio/realm/RealmList<TE;>;"
    .local p1, "listener":Lio/realm/RealmChangeListener;, "Lio/realm/RealmChangeListener<Lio/realm/RealmList<TE;>;>;"
    const/4 v0, 0x1

    invoke-direct {p0, p1, v0}, Lio/realm/RealmList;->checkForAddRemoveListener(Ljava/lang/Object;Z)V

    .line 942
    iget-object v0, p0, Lio/realm/RealmList;->collection:Lio/realm/internal/Collection;

    invoke-virtual {v0, p0, p1}, Lio/realm/internal/Collection;->removeListener(Ljava/lang/Object;Lio/realm/RealmChangeListener;)V

    .line 943
    return-void
.end method

.method public set(ILio/realm/RealmModel;)Lio/realm/RealmModel;
    .locals 8
    .param p1, "location"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(ITE;)TE;"
        }
    .end annotation

    .prologue
    .line 229
    .local p0, "this":Lio/realm/RealmList;, "Lio/realm/RealmList<TE;>;"
    .local p2, "object":Lio/realm/RealmModel;, "TE;"
    invoke-direct {p0, p2}, Lio/realm/RealmList;->checkValidObject(Lio/realm/RealmModel;)V

    .line 231
    invoke-virtual {p0}, Lio/realm/RealmList;->isManaged()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 232
    invoke-direct {p0}, Lio/realm/RealmList;->checkValidView()V

    .line 233
    invoke-direct {p0, p2}, Lio/realm/RealmList;->copyToRealmIfNeeded(Lio/realm/RealmModel;)Lio/realm/RealmModel;

    move-result-object v2

    check-cast v2, Lio/realm/internal/RealmObjectProxy;

    .line 234
    .local v2, "proxy":Lio/realm/internal/RealmObjectProxy;
    invoke-virtual {p0, p1}, Lio/realm/RealmList;->get(I)Lio/realm/RealmModel;

    move-result-object v0

    .line 235
    .local v0, "oldObject":Lio/realm/RealmModel;, "TE;"
    iget-object v3, p0, Lio/realm/RealmList;->view:Lio/realm/internal/LinkView;

    int-to-long v4, p1

    invoke-interface {v2}, Lio/realm/internal/RealmObjectProxy;->realmGet$proxyState()Lio/realm/ProxyState;

    move-result-object v6

    invoke-virtual {v6}, Lio/realm/ProxyState;->getRow$realm()Lio/realm/internal/Row;

    move-result-object v6

    invoke-interface {v6}, Lio/realm/internal/Row;->getIndex()J

    move-result-wide v6

    invoke-virtual {v3, v4, v5, v6, v7}, Lio/realm/internal/LinkView;->set(JJ)V

    move-object v1, v0

    .line 240
    .end local v0    # "oldObject":Lio/realm/RealmModel;, "TE;"
    .end local v2    # "proxy":Lio/realm/internal/RealmObjectProxy;
    .local v1, "oldObject":Lio/realm/RealmModel;, "TE;"
    :goto_0
    return-object v1

    .line 238
    .end local v1    # "oldObject":Lio/realm/RealmModel;, "TE;"
    :cond_0
    iget-object v3, p0, Lio/realm/RealmList;->unmanagedList:Ljava/util/List;

    invoke-interface {v3, p1, p2}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/realm/RealmModel;

    .restart local v0    # "oldObject":Lio/realm/RealmModel;, "TE;"
    move-object v1, v0

    .line 240
    .end local v0    # "oldObject":Lio/realm/RealmModel;, "TE;"
    .restart local v1    # "oldObject":Lio/realm/RealmModel;, "TE;"
    goto :goto_0
.end method

.method public bridge synthetic set(ILjava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 53
    .local p0, "this":Lio/realm/RealmList;, "Lio/realm/RealmList<TE;>;"
    check-cast p2, Lio/realm/RealmModel;

    invoke-virtual {p0, p1, p2}, Lio/realm/RealmList;->set(ILio/realm/RealmModel;)Lio/realm/RealmModel;

    move-result-object v0

    return-object v0
.end method

.method public size()I
    .locals 4

    .prologue
    .line 583
    .local p0, "this":Lio/realm/RealmList;, "Lio/realm/RealmList<TE;>;"
    invoke-virtual {p0}, Lio/realm/RealmList;->isManaged()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 584
    invoke-direct {p0}, Lio/realm/RealmList;->checkValidView()V

    .line 585
    iget-object v2, p0, Lio/realm/RealmList;->view:Lio/realm/internal/LinkView;

    invoke-virtual {v2}, Lio/realm/internal/LinkView;->size()J

    move-result-wide v0

    .line 586
    .local v0, "size":J
    const-wide/32 v2, 0x7fffffff

    cmp-long v2, v0, v2

    if-gez v2, :cond_0

    long-to-int v2, v0

    .line 588
    .end local v0    # "size":J
    :goto_0
    return v2

    .line 586
    .restart local v0    # "size":J
    :cond_0
    const v2, 0x7fffffff

    goto :goto_0

    .line 588
    .end local v0    # "size":J
    :cond_1
    iget-object v2, p0, Lio/realm/RealmList;->unmanagedList:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    goto :goto_0
.end method

.method public sort(Ljava/lang/String;)Lio/realm/RealmResults;
    .locals 1
    .param p1, "fieldName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lio/realm/RealmResults",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 526
    .local p0, "this":Lio/realm/RealmList;, "Lio/realm/RealmList<TE;>;"
    sget-object v0, Lio/realm/Sort;->ASCENDING:Lio/realm/Sort;

    invoke-virtual {p0, p1, v0}, Lio/realm/RealmList;->sort(Ljava/lang/String;Lio/realm/Sort;)Lio/realm/RealmResults;

    move-result-object v0

    return-object v0
.end method

.method public sort(Ljava/lang/String;Lio/realm/Sort;)Lio/realm/RealmResults;
    .locals 2
    .param p1, "fieldName"    # Ljava/lang/String;
    .param p2, "sortOrder"    # Lio/realm/Sort;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lio/realm/Sort;",
            ")",
            "Lio/realm/RealmResults",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 534
    .local p0, "this":Lio/realm/RealmList;, "Lio/realm/RealmList<TE;>;"
    invoke-virtual {p0}, Lio/realm/RealmList;->isManaged()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 535
    invoke-virtual {p0}, Lio/realm/RealmList;->where()Lio/realm/RealmQuery;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lio/realm/RealmQuery;->findAllSorted(Ljava/lang/String;Lio/realm/Sort;)Lio/realm/RealmResults;

    move-result-object v0

    return-object v0

    .line 537
    :cond_0
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "This method is only available in managed mode"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public sort(Ljava/lang/String;Lio/realm/Sort;Ljava/lang/String;Lio/realm/Sort;)Lio/realm/RealmResults;
    .locals 4
    .param p1, "fieldName1"    # Ljava/lang/String;
    .param p2, "sortOrder1"    # Lio/realm/Sort;
    .param p3, "fieldName2"    # Ljava/lang/String;
    .param p4, "sortOrder2"    # Lio/realm/Sort;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lio/realm/Sort;",
            "Ljava/lang/String;",
            "Lio/realm/Sort;",
            ")",
            "Lio/realm/RealmResults",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .local p0, "this":Lio/realm/RealmList;, "Lio/realm/RealmList<TE;>;"
    const/4 v1, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 546
    new-array v0, v1, [Ljava/lang/String;

    aput-object p1, v0, v2

    aput-object p3, v0, v3

    new-array v1, v1, [Lio/realm/Sort;

    aput-object p2, v1, v2

    aput-object p4, v1, v3

    invoke-virtual {p0, v0, v1}, Lio/realm/RealmList;->sort([Ljava/lang/String;[Lio/realm/Sort;)Lio/realm/RealmResults;

    move-result-object v0

    return-object v0
.end method

.method public sort([Ljava/lang/String;[Lio/realm/Sort;)Lio/realm/RealmResults;
    .locals 2
    .param p1, "fieldNames"    # [Ljava/lang/String;
    .param p2, "sortOrders"    # [Lio/realm/Sort;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "Ljava/lang/String;",
            "[",
            "Lio/realm/Sort;",
            ")",
            "Lio/realm/RealmResults",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 554
    .local p0, "this":Lio/realm/RealmList;, "Lio/realm/RealmList<TE;>;"
    invoke-virtual {p0}, Lio/realm/RealmList;->isManaged()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 555
    invoke-virtual {p0}, Lio/realm/RealmList;->where()Lio/realm/RealmQuery;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lio/realm/RealmQuery;->findAllSorted([Ljava/lang/String;[Lio/realm/Sort;)Lio/realm/RealmResults;

    move-result-object v0

    return-object v0

    .line 557
    :cond_0
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "This method is only available in managed mode"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public sum(Ljava/lang/String;)Ljava/lang/Number;
    .locals 2
    .param p1, "fieldName"    # Ljava/lang/String;

    .prologue
    .line 637
    .local p0, "this":Lio/realm/RealmList;, "Lio/realm/RealmList<TE;>;"
    invoke-virtual {p0}, Lio/realm/RealmList;->isManaged()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 638
    invoke-virtual {p0}, Lio/realm/RealmList;->where()Lio/realm/RealmQuery;

    move-result-object v0

    invoke-virtual {v0, p1}, Lio/realm/RealmQuery;->sum(Ljava/lang/String;)Ljava/lang/Number;

    move-result-object v0

    return-object v0

    .line 640
    :cond_0
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "This method is only available in managed mode"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 823
    .local p0, "this":Lio/realm/RealmList;, "Lio/realm/RealmList<TE;>;"
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 824
    .local v1, "sb":Ljava/lang/StringBuilder;
    invoke-virtual {p0}, Lio/realm/RealmList;->isManaged()Z

    move-result v2

    if-eqz v2, :cond_1

    iget-object v2, p0, Lio/realm/RealmList;->clazz:Ljava/lang/Class;

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    :goto_0
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 825
    const-string v2, "@["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 826
    invoke-virtual {p0}, Lio/realm/RealmList;->isManaged()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-direct {p0}, Lio/realm/RealmList;->isAttached()Z

    move-result v2

    if-nez v2, :cond_2

    .line 827
    const-string v2, "invalid"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 840
    :cond_0
    const-string v2, "]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 841
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    return-object v2

    .line 824
    :cond_1
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    goto :goto_0

    .line 829
    :cond_2
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    invoke-virtual {p0}, Lio/realm/RealmList;->size()I

    move-result v2

    if-ge v0, v2, :cond_0

    .line 830
    invoke-virtual {p0}, Lio/realm/RealmList;->isManaged()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 831
    invoke-virtual {p0, v0}, Lio/realm/RealmList;->get(I)Lio/realm/RealmModel;

    move-result-object v2

    check-cast v2, Lio/realm/internal/RealmObjectProxy;

    invoke-interface {v2}, Lio/realm/internal/RealmObjectProxy;->realmGet$proxyState()Lio/realm/ProxyState;

    move-result-object v2

    invoke-virtual {v2}, Lio/realm/ProxyState;->getRow$realm()Lio/realm/internal/Row;

    move-result-object v2

    invoke-interface {v2}, Lio/realm/internal/Row;->getIndex()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 835
    :goto_2
    invoke-virtual {p0}, Lio/realm/RealmList;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_3

    .line 836
    const/16 v2, 0x2c

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 829
    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 833
    :cond_4
    invoke-virtual {p0, v0}, Lio/realm/RealmList;->get(I)Lio/realm/RealmModel;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/System;->identityHashCode(Ljava/lang/Object;)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    goto :goto_2
.end method

.method public where()Lio/realm/RealmQuery;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/realm/RealmQuery",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 600
    .local p0, "this":Lio/realm/RealmList;, "Lio/realm/RealmList<TE;>;"
    invoke-virtual {p0}, Lio/realm/RealmList;->isManaged()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 601
    invoke-direct {p0}, Lio/realm/RealmList;->checkValidView()V

    .line 602
    invoke-static {p0}, Lio/realm/RealmQuery;->createQueryFromList(Lio/realm/RealmList;)Lio/realm/RealmQuery;

    move-result-object v0

    return-object v0

    .line 604
    :cond_0
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "This method is only available in managed mode"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method
