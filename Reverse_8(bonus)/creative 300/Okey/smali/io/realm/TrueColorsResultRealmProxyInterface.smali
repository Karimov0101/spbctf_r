.class public interface abstract Lio/realm/TrueColorsResultRealmProxyInterface;
.super Ljava/lang/Object;
.source "TrueColorsResultRealmProxyInterface.java"


# virtual methods
.method public abstract realmGet$config()Lcom/safonov/speedreading/training/fragment/truecolors/repository/entity/TrueColorsConfig;
.end method

.method public abstract realmGet$id()I
.end method

.method public abstract realmGet$score()I
.end method

.method public abstract realmSet$config(Lcom/safonov/speedreading/training/fragment/truecolors/repository/entity/TrueColorsConfig;)V
.end method

.method public abstract realmSet$id(I)V
.end method

.method public abstract realmSet$score(I)V
.end method
