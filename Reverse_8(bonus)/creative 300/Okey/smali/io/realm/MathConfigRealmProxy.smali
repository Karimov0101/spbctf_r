.class public Lio/realm/MathConfigRealmProxy;
.super Lcom/safonov/speedreading/training/fragment/math/repository/entity/MathConfig;
.source "MathConfigRealmProxy.java"

# interfaces
.implements Lio/realm/internal/RealmObjectProxy;
.implements Lio/realm/MathConfigRealmProxyInterface;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lio/realm/MathConfigRealmProxy$MathConfigColumnInfo;
    }
.end annotation


# static fields
.field private static final FIELD_NAMES:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private columnInfo:Lio/realm/MathConfigRealmProxy$MathConfigColumnInfo;

.field private proxyState:Lio/realm/ProxyState;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/realm/ProxyState",
            "<",
            "Lcom/safonov/speedreading/training/fragment/math/repository/entity/MathConfig;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 73
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 74
    .local v0, "fieldNames":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const-string v1, "id"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 75
    const-string v1, "complexity"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 76
    const-string v1, "duration"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 77
    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    sput-object v1, Lio/realm/MathConfigRealmProxy;->FIELD_NAMES:Ljava/util/List;

    .line 78
    return-void
.end method

.method constructor <init>()V
    .locals 1

    .prologue
    .line 80
    invoke-direct {p0}, Lcom/safonov/speedreading/training/fragment/math/repository/entity/MathConfig;-><init>()V

    .line 81
    iget-object v0, p0, Lio/realm/MathConfigRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v0}, Lio/realm/ProxyState;->setConstructionFinished()V

    .line 82
    return-void
.end method

.method public static copy(Lio/realm/Realm;Lcom/safonov/speedreading/training/fragment/math/repository/entity/MathConfig;ZLjava/util/Map;)Lcom/safonov/speedreading/training/fragment/math/repository/entity/MathConfig;
    .locals 6
    .param p0, "realm"    # Lio/realm/Realm;
    .param p1, "newObject"    # Lcom/safonov/speedreading/training/fragment/math/repository/entity/MathConfig;
    .param p2, "update"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/realm/Realm;",
            "Lcom/safonov/speedreading/training/fragment/math/repository/entity/MathConfig;",
            "Z",
            "Ljava/util/Map",
            "<",
            "Lio/realm/RealmModel;",
            "Lio/realm/internal/RealmObjectProxy;",
            ">;)",
            "Lcom/safonov/speedreading/training/fragment/math/repository/entity/MathConfig;"
        }
    .end annotation

    .prologue
    .line 384
    .local p3, "cache":Ljava/util/Map;, "Ljava/util/Map<Lio/realm/RealmModel;Lio/realm/internal/RealmObjectProxy;>;"
    invoke-interface {p3, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/realm/internal/RealmObjectProxy;

    .line 385
    .local v0, "cachedRealmObject":Lio/realm/internal/RealmObjectProxy;
    if-eqz v0, :cond_0

    .line 386
    check-cast v0, Lcom/safonov/speedreading/training/fragment/math/repository/entity/MathConfig;

    .line 393
    .end local v0    # "cachedRealmObject":Lio/realm/internal/RealmObjectProxy;
    .end local p1    # "newObject":Lcom/safonov/speedreading/training/fragment/math/repository/entity/MathConfig;
    :goto_0
    return-object v0

    .line 389
    .restart local v0    # "cachedRealmObject":Lio/realm/internal/RealmObjectProxy;
    .restart local p1    # "newObject":Lcom/safonov/speedreading/training/fragment/math/repository/entity/MathConfig;
    :cond_0
    const-class v3, Lcom/safonov/speedreading/training/fragment/math/repository/entity/MathConfig;

    move-object v2, p1

    check-cast v2, Lio/realm/MathConfigRealmProxyInterface;

    invoke-interface {v2}, Lio/realm/MathConfigRealmProxyInterface;->realmGet$id()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v4, 0x0

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v5

    invoke-virtual {p0, v3, v2, v4, v5}, Lio/realm/Realm;->createObjectInternal(Ljava/lang/Class;Ljava/lang/Object;ZLjava/util/List;)Lio/realm/RealmModel;

    move-result-object v1

    check-cast v1, Lcom/safonov/speedreading/training/fragment/math/repository/entity/MathConfig;

    .local v1, "realmObject":Lcom/safonov/speedreading/training/fragment/math/repository/entity/MathConfig;
    move-object v2, v1

    .line 390
    check-cast v2, Lio/realm/internal/RealmObjectProxy;

    invoke-interface {p3, p1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-object v2, v1

    .line 391
    check-cast v2, Lio/realm/MathConfigRealmProxyInterface;

    move-object v3, p1

    check-cast v3, Lio/realm/MathConfigRealmProxyInterface;

    invoke-interface {v3}, Lio/realm/MathConfigRealmProxyInterface;->realmGet$complexity()I

    move-result v3

    invoke-interface {v2, v3}, Lio/realm/MathConfigRealmProxyInterface;->realmSet$complexity(I)V

    move-object v2, v1

    .line 392
    check-cast v2, Lio/realm/MathConfigRealmProxyInterface;

    check-cast p1, Lio/realm/MathConfigRealmProxyInterface;

    .end local p1    # "newObject":Lcom/safonov/speedreading/training/fragment/math/repository/entity/MathConfig;
    invoke-interface {p1}, Lio/realm/MathConfigRealmProxyInterface;->realmGet$duration()J

    move-result-wide v4

    invoke-interface {v2, v4, v5}, Lio/realm/MathConfigRealmProxyInterface;->realmSet$duration(J)V

    move-object v0, v1

    .line 393
    goto :goto_0
.end method

.method public static copyOrUpdate(Lio/realm/Realm;Lcom/safonov/speedreading/training/fragment/math/repository/entity/MathConfig;ZLjava/util/Map;)Lcom/safonov/speedreading/training/fragment/math/repository/entity/MathConfig;
    .locals 19
    .param p0, "realm"    # Lio/realm/Realm;
    .param p1, "object"    # Lcom/safonov/speedreading/training/fragment/math/repository/entity/MathConfig;
    .param p2, "update"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/realm/Realm;",
            "Lcom/safonov/speedreading/training/fragment/math/repository/entity/MathConfig;",
            "Z",
            "Ljava/util/Map",
            "<",
            "Lio/realm/RealmModel;",
            "Lio/realm/internal/RealmObjectProxy;",
            ">;)",
            "Lcom/safonov/speedreading/training/fragment/math/repository/entity/MathConfig;"
        }
    .end annotation

    .prologue
    .line 345
    .local p3, "cache":Ljava/util/Map;, "Ljava/util/Map<Lio/realm/RealmModel;Lio/realm/internal/RealmObjectProxy;>;"
    move-object/from16 v0, p1

    instance-of v5, v0, Lio/realm/internal/RealmObjectProxy;

    if-eqz v5, :cond_0

    move-object/from16 v5, p1

    check-cast v5, Lio/realm/internal/RealmObjectProxy;

    invoke-interface {v5}, Lio/realm/internal/RealmObjectProxy;->realmGet$proxyState()Lio/realm/ProxyState;

    move-result-object v5

    invoke-virtual {v5}, Lio/realm/ProxyState;->getRealm$realm()Lio/realm/BaseRealm;

    move-result-object v5

    if-eqz v5, :cond_0

    move-object/from16 v5, p1

    check-cast v5, Lio/realm/internal/RealmObjectProxy;

    invoke-interface {v5}, Lio/realm/internal/RealmObjectProxy;->realmGet$proxyState()Lio/realm/ProxyState;

    move-result-object v5

    invoke-virtual {v5}, Lio/realm/ProxyState;->getRealm$realm()Lio/realm/BaseRealm;

    move-result-object v5

    iget-wide v6, v5, Lio/realm/BaseRealm;->threadId:J

    move-object/from16 v0, p0

    iget-wide v8, v0, Lio/realm/Realm;->threadId:J

    cmp-long v5, v6, v8

    if-eqz v5, :cond_0

    .line 346
    new-instance v5, Ljava/lang/IllegalArgumentException;

    const-string v6, "Objects which belong to Realm instances in other threads cannot be copied into this Realm instance."

    invoke-direct {v5, v6}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v5

    .line 348
    :cond_0
    move-object/from16 v0, p1

    instance-of v5, v0, Lio/realm/internal/RealmObjectProxy;

    if-eqz v5, :cond_1

    move-object/from16 v5, p1

    check-cast v5, Lio/realm/internal/RealmObjectProxy;

    invoke-interface {v5}, Lio/realm/internal/RealmObjectProxy;->realmGet$proxyState()Lio/realm/ProxyState;

    move-result-object v5

    invoke-virtual {v5}, Lio/realm/ProxyState;->getRealm$realm()Lio/realm/BaseRealm;

    move-result-object v5

    if-eqz v5, :cond_1

    move-object/from16 v5, p1

    check-cast v5, Lio/realm/internal/RealmObjectProxy;

    invoke-interface {v5}, Lio/realm/internal/RealmObjectProxy;->realmGet$proxyState()Lio/realm/ProxyState;

    move-result-object v5

    invoke-virtual {v5}, Lio/realm/ProxyState;->getRealm$realm()Lio/realm/BaseRealm;

    move-result-object v5

    invoke-virtual {v5}, Lio/realm/BaseRealm;->getPath()Ljava/lang/String;

    move-result-object v5

    invoke-virtual/range {p0 .. p0}, Lio/realm/Realm;->getPath()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 378
    .end local p1    # "object":Lcom/safonov/speedreading/training/fragment/math/repository/entity/MathConfig;
    :goto_0
    return-object p1

    .line 351
    .restart local p1    # "object":Lcom/safonov/speedreading/training/fragment/math/repository/entity/MathConfig;
    :cond_1
    sget-object v5, Lio/realm/BaseRealm;->objectContext:Lio/realm/BaseRealm$ThreadLocalRealmObjectContext;

    invoke-virtual {v5}, Lio/realm/BaseRealm$ThreadLocalRealmObjectContext;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lio/realm/BaseRealm$RealmObjectContext;

    .line 352
    .local v4, "objectContext":Lio/realm/BaseRealm$RealmObjectContext;
    move-object/from16 v0, p3

    move-object/from16 v1, p1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lio/realm/internal/RealmObjectProxy;

    .line 353
    .local v10, "cachedRealmObject":Lio/realm/internal/RealmObjectProxy;
    if-eqz v10, :cond_2

    .line 354
    check-cast v10, Lcom/safonov/speedreading/training/fragment/math/repository/entity/MathConfig;

    .end local v10    # "cachedRealmObject":Lio/realm/internal/RealmObjectProxy;
    move-object/from16 p1, v10

    goto :goto_0

    .line 356
    .restart local v10    # "cachedRealmObject":Lio/realm/internal/RealmObjectProxy;
    :cond_2
    const/4 v14, 0x0

    .line 357
    .local v14, "realmObject":Lcom/safonov/speedreading/training/fragment/math/repository/entity/MathConfig;
    move/from16 v11, p2

    .line 358
    .local v11, "canUpdate":Z
    if-eqz v11, :cond_3

    .line 359
    const-class v5, Lcom/safonov/speedreading/training/fragment/math/repository/entity/MathConfig;

    move-object/from16 v0, p0

    invoke-virtual {v0, v5}, Lio/realm/Realm;->getTable(Ljava/lang/Class;)Lio/realm/internal/Table;

    move-result-object v18

    .line 360
    .local v18, "table":Lio/realm/internal/Table;
    invoke-virtual/range {v18 .. v18}, Lio/realm/internal/Table;->getPrimaryKey()J

    move-result-wide v12

    .local v12, "pkColumnIndex":J
    move-object/from16 v5, p1

    .line 361
    check-cast v5, Lio/realm/MathConfigRealmProxyInterface;

    invoke-interface {v5}, Lio/realm/MathConfigRealmProxyInterface;->realmGet$id()I

    move-result v5

    int-to-long v6, v5

    move-object/from16 v0, v18

    invoke-virtual {v0, v12, v13, v6, v7}, Lio/realm/internal/Table;->findFirstLong(JJ)J

    move-result-wide v16

    .line 362
    .local v16, "rowIndex":J
    const-wide/16 v6, -0x1

    cmp-long v5, v16, v6

    if-eqz v5, :cond_4

    .line 364
    :try_start_0
    move-object/from16 v0, v18

    move-wide/from16 v1, v16

    invoke-virtual {v0, v1, v2}, Lio/realm/internal/Table;->getUncheckedRow(J)Lio/realm/internal/UncheckedRow;

    move-result-object v6

    move-object/from16 v0, p0

    iget-object v5, v0, Lio/realm/Realm;->schema:Lio/realm/RealmSchema;

    const-class v7, Lcom/safonov/speedreading/training/fragment/math/repository/entity/MathConfig;

    invoke-virtual {v5, v7}, Lio/realm/RealmSchema;->getColumnInfo(Ljava/lang/Class;)Lio/realm/internal/ColumnInfo;

    move-result-object v7

    const/4 v8, 0x0

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v9

    move-object/from16 v5, p0

    invoke-virtual/range {v4 .. v9}, Lio/realm/BaseRealm$RealmObjectContext;->set(Lio/realm/BaseRealm;Lio/realm/internal/Row;Lio/realm/internal/ColumnInfo;ZLjava/util/List;)V

    .line 365
    new-instance v15, Lio/realm/MathConfigRealmProxy;

    invoke-direct {v15}, Lio/realm/MathConfigRealmProxy;-><init>()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 366
    .end local v14    # "realmObject":Lcom/safonov/speedreading/training/fragment/math/repository/entity/MathConfig;
    .local v15, "realmObject":Lcom/safonov/speedreading/training/fragment/math/repository/entity/MathConfig;
    :try_start_1
    move-object v0, v15

    check-cast v0, Lio/realm/internal/RealmObjectProxy;

    move-object v5, v0

    move-object/from16 v0, p3

    move-object/from16 v1, p1

    invoke-interface {v0, v1, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 368
    invoke-virtual {v4}, Lio/realm/BaseRealm$RealmObjectContext;->clear()V

    move-object v14, v15

    .line 375
    .end local v12    # "pkColumnIndex":J
    .end local v15    # "realmObject":Lcom/safonov/speedreading/training/fragment/math/repository/entity/MathConfig;
    .end local v16    # "rowIndex":J
    .end local v18    # "table":Lio/realm/internal/Table;
    .restart local v14    # "realmObject":Lcom/safonov/speedreading/training/fragment/math/repository/entity/MathConfig;
    :cond_3
    :goto_1
    if-eqz v11, :cond_5

    .line 376
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p3

    invoke-static {v0, v14, v1, v2}, Lio/realm/MathConfigRealmProxy;->update(Lio/realm/Realm;Lcom/safonov/speedreading/training/fragment/math/repository/entity/MathConfig;Lcom/safonov/speedreading/training/fragment/math/repository/entity/MathConfig;Ljava/util/Map;)Lcom/safonov/speedreading/training/fragment/math/repository/entity/MathConfig;

    move-result-object p1

    goto :goto_0

    .line 368
    .restart local v12    # "pkColumnIndex":J
    .restart local v16    # "rowIndex":J
    .restart local v18    # "table":Lio/realm/internal/Table;
    :catchall_0
    move-exception v5

    :goto_2
    invoke-virtual {v4}, Lio/realm/BaseRealm$RealmObjectContext;->clear()V

    throw v5

    .line 371
    :cond_4
    const/4 v11, 0x0

    goto :goto_1

    .line 378
    .end local v12    # "pkColumnIndex":J
    .end local v16    # "rowIndex":J
    .end local v18    # "table":Lio/realm/internal/Table;
    :cond_5
    invoke-static/range {p0 .. p3}, Lio/realm/MathConfigRealmProxy;->copy(Lio/realm/Realm;Lcom/safonov/speedreading/training/fragment/math/repository/entity/MathConfig;ZLjava/util/Map;)Lcom/safonov/speedreading/training/fragment/math/repository/entity/MathConfig;

    move-result-object p1

    goto/16 :goto_0

    .line 368
    .end local v14    # "realmObject":Lcom/safonov/speedreading/training/fragment/math/repository/entity/MathConfig;
    .restart local v12    # "pkColumnIndex":J
    .restart local v15    # "realmObject":Lcom/safonov/speedreading/training/fragment/math/repository/entity/MathConfig;
    .restart local v16    # "rowIndex":J
    .restart local v18    # "table":Lio/realm/internal/Table;
    :catchall_1
    move-exception v5

    move-object v14, v15

    .end local v15    # "realmObject":Lcom/safonov/speedreading/training/fragment/math/repository/entity/MathConfig;
    .restart local v14    # "realmObject":Lcom/safonov/speedreading/training/fragment/math/repository/entity/MathConfig;
    goto :goto_2
.end method

.method public static createDetachedCopy(Lcom/safonov/speedreading/training/fragment/math/repository/entity/MathConfig;IILjava/util/Map;)Lcom/safonov/speedreading/training/fragment/math/repository/entity/MathConfig;
    .locals 6
    .param p0, "realmObject"    # Lcom/safonov/speedreading/training/fragment/math/repository/entity/MathConfig;
    .param p1, "currentDepth"    # I
    .param p2, "maxDepth"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/safonov/speedreading/training/fragment/math/repository/entity/MathConfig;",
            "II",
            "Ljava/util/Map",
            "<",
            "Lio/realm/RealmModel;",
            "Lio/realm/internal/RealmObjectProxy$CacheData",
            "<",
            "Lio/realm/RealmModel;",
            ">;>;)",
            "Lcom/safonov/speedreading/training/fragment/math/repository/entity/MathConfig;"
        }
    .end annotation

    .prologue
    .line 502
    .local p3, "cache":Ljava/util/Map;, "Ljava/util/Map<Lio/realm/RealmModel;Lio/realm/internal/RealmObjectProxy$CacheData<Lio/realm/RealmModel;>;>;"
    if-gt p1, p2, :cond_0

    if-nez p0, :cond_1

    .line 503
    :cond_0
    const/4 v2, 0x0

    .line 522
    .end local p0    # "realmObject":Lcom/safonov/speedreading/training/fragment/math/repository/entity/MathConfig;
    :goto_0
    return-object v2

    .line 505
    .restart local p0    # "realmObject":Lcom/safonov/speedreading/training/fragment/math/repository/entity/MathConfig;
    :cond_1
    invoke-interface {p3, p0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/realm/internal/RealmObjectProxy$CacheData;

    .line 507
    .local v0, "cachedObject":Lio/realm/internal/RealmObjectProxy$CacheData;, "Lio/realm/internal/RealmObjectProxy$CacheData<Lio/realm/RealmModel;>;"
    if-eqz v0, :cond_3

    .line 509
    iget v2, v0, Lio/realm/internal/RealmObjectProxy$CacheData;->minDepth:I

    if-lt p1, v2, :cond_2

    .line 510
    iget-object v2, v0, Lio/realm/internal/RealmObjectProxy$CacheData;->object:Lio/realm/RealmModel;

    check-cast v2, Lcom/safonov/speedreading/training/fragment/math/repository/entity/MathConfig;

    goto :goto_0

    .line 512
    :cond_2
    iget-object v1, v0, Lio/realm/internal/RealmObjectProxy$CacheData;->object:Lio/realm/RealmModel;

    check-cast v1, Lcom/safonov/speedreading/training/fragment/math/repository/entity/MathConfig;

    .line 513
    .local v1, "unmanagedObject":Lcom/safonov/speedreading/training/fragment/math/repository/entity/MathConfig;
    iput p1, v0, Lio/realm/internal/RealmObjectProxy$CacheData;->minDepth:I

    :goto_1
    move-object v2, v1

    .line 519
    check-cast v2, Lio/realm/MathConfigRealmProxyInterface;

    move-object v3, p0

    check-cast v3, Lio/realm/MathConfigRealmProxyInterface;

    invoke-interface {v3}, Lio/realm/MathConfigRealmProxyInterface;->realmGet$id()I

    move-result v3

    invoke-interface {v2, v3}, Lio/realm/MathConfigRealmProxyInterface;->realmSet$id(I)V

    move-object v2, v1

    .line 520
    check-cast v2, Lio/realm/MathConfigRealmProxyInterface;

    move-object v3, p0

    check-cast v3, Lio/realm/MathConfigRealmProxyInterface;

    invoke-interface {v3}, Lio/realm/MathConfigRealmProxyInterface;->realmGet$complexity()I

    move-result v3

    invoke-interface {v2, v3}, Lio/realm/MathConfigRealmProxyInterface;->realmSet$complexity(I)V

    move-object v2, v1

    .line 521
    check-cast v2, Lio/realm/MathConfigRealmProxyInterface;

    check-cast p0, Lio/realm/MathConfigRealmProxyInterface;

    .end local p0    # "realmObject":Lcom/safonov/speedreading/training/fragment/math/repository/entity/MathConfig;
    invoke-interface {p0}, Lio/realm/MathConfigRealmProxyInterface;->realmGet$duration()J

    move-result-wide v4

    invoke-interface {v2, v4, v5}, Lio/realm/MathConfigRealmProxyInterface;->realmSet$duration(J)V

    move-object v2, v1

    .line 522
    goto :goto_0

    .line 516
    .end local v1    # "unmanagedObject":Lcom/safonov/speedreading/training/fragment/math/repository/entity/MathConfig;
    .restart local p0    # "realmObject":Lcom/safonov/speedreading/training/fragment/math/repository/entity/MathConfig;
    :cond_3
    new-instance v1, Lcom/safonov/speedreading/training/fragment/math/repository/entity/MathConfig;

    invoke-direct {v1}, Lcom/safonov/speedreading/training/fragment/math/repository/entity/MathConfig;-><init>()V

    .line 517
    .restart local v1    # "unmanagedObject":Lcom/safonov/speedreading/training/fragment/math/repository/entity/MathConfig;
    new-instance v2, Lio/realm/internal/RealmObjectProxy$CacheData;

    invoke-direct {v2, p1, v1}, Lio/realm/internal/RealmObjectProxy$CacheData;-><init>(ILio/realm/RealmModel;)V

    invoke-interface {p3, p0, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1
.end method

.method public static createOrUpdateUsingJsonObject(Lio/realm/Realm;Lorg/json/JSONObject;Z)Lcom/safonov/speedreading/training/fragment/math/repository/entity/MathConfig;
    .locals 13
    .param p0, "realm"    # Lio/realm/Realm;
    .param p1, "json"    # Lorg/json/JSONObject;
    .param p2, "update"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .prologue
    .line 254
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v6

    .line 255
    .local v6, "excludeFields":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const/4 v7, 0x0

    .line 256
    .local v7, "obj":Lcom/safonov/speedreading/training/fragment/math/repository/entity/MathConfig;
    if-eqz p2, :cond_1

    .line 257
    const-class v1, Lcom/safonov/speedreading/training/fragment/math/repository/entity/MathConfig;

    invoke-virtual {p0, v1}, Lio/realm/Realm;->getTable(Ljava/lang/Class;)Lio/realm/internal/Table;

    move-result-object v12

    .line 258
    .local v12, "table":Lio/realm/internal/Table;
    invoke-virtual {v12}, Lio/realm/internal/Table;->getPrimaryKey()J

    move-result-wide v8

    .line 259
    .local v8, "pkColumnIndex":J
    const-wide/16 v10, -0x1

    .line 260
    .local v10, "rowIndex":J
    const-string v1, "id"

    invoke-virtual {p1, v1}, Lorg/json/JSONObject;->isNull(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 261
    const-string v1, "id"

    invoke-virtual {p1, v1}, Lorg/json/JSONObject;->getLong(Ljava/lang/String;)J

    move-result-wide v2

    invoke-virtual {v12, v8, v9, v2, v3}, Lio/realm/internal/Table;->findFirstLong(JJ)J

    move-result-wide v10

    .line 263
    :cond_0
    const-wide/16 v2, -0x1

    cmp-long v1, v10, v2

    if-eqz v1, :cond_1

    .line 264
    sget-object v1, Lio/realm/BaseRealm;->objectContext:Lio/realm/BaseRealm$ThreadLocalRealmObjectContext;

    invoke-virtual {v1}, Lio/realm/BaseRealm$ThreadLocalRealmObjectContext;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/realm/BaseRealm$RealmObjectContext;

    .line 266
    .local v0, "objectContext":Lio/realm/BaseRealm$RealmObjectContext;
    :try_start_0
    invoke-virtual {v12, v10, v11}, Lio/realm/internal/Table;->getUncheckedRow(J)Lio/realm/internal/UncheckedRow;

    move-result-object v2

    iget-object v1, p0, Lio/realm/Realm;->schema:Lio/realm/RealmSchema;

    const-class v3, Lcom/safonov/speedreading/training/fragment/math/repository/entity/MathConfig;

    invoke-virtual {v1, v3}, Lio/realm/RealmSchema;->getColumnInfo(Ljava/lang/Class;)Lio/realm/internal/ColumnInfo;

    move-result-object v3

    const/4 v4, 0x0

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v5

    move-object v1, p0

    invoke-virtual/range {v0 .. v5}, Lio/realm/BaseRealm$RealmObjectContext;->set(Lio/realm/BaseRealm;Lio/realm/internal/Row;Lio/realm/internal/ColumnInfo;ZLjava/util/List;)V

    .line 267
    new-instance v7, Lio/realm/MathConfigRealmProxy;

    .end local v7    # "obj":Lcom/safonov/speedreading/training/fragment/math/repository/entity/MathConfig;
    invoke-direct {v7}, Lio/realm/MathConfigRealmProxy;-><init>()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 269
    .restart local v7    # "obj":Lcom/safonov/speedreading/training/fragment/math/repository/entity/MathConfig;
    invoke-virtual {v0}, Lio/realm/BaseRealm$RealmObjectContext;->clear()V

    .line 273
    .end local v0    # "objectContext":Lio/realm/BaseRealm$RealmObjectContext;
    .end local v8    # "pkColumnIndex":J
    .end local v10    # "rowIndex":J
    .end local v12    # "table":Lio/realm/internal/Table;
    :cond_1
    if-nez v7, :cond_2

    .line 274
    const-string v1, "id"

    invoke-virtual {p1, v1}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 275
    const-string v1, "id"

    invoke-virtual {p1, v1}, Lorg/json/JSONObject;->isNull(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 276
    const-class v1, Lcom/safonov/speedreading/training/fragment/math/repository/entity/MathConfig;

    const/4 v2, 0x0

    const/4 v3, 0x1

    invoke-virtual {p0, v1, v2, v3, v6}, Lio/realm/Realm;->createObjectInternal(Ljava/lang/Class;Ljava/lang/Object;ZLjava/util/List;)Lio/realm/RealmModel;

    move-result-object v7

    .end local v7    # "obj":Lcom/safonov/speedreading/training/fragment/math/repository/entity/MathConfig;
    check-cast v7, Lio/realm/MathConfigRealmProxy;

    .line 284
    .restart local v7    # "obj":Lcom/safonov/speedreading/training/fragment/math/repository/entity/MathConfig;
    :cond_2
    :goto_0
    const-string v1, "complexity"

    invoke-virtual {p1, v1}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 285
    const-string v1, "complexity"

    invoke-virtual {p1, v1}, Lorg/json/JSONObject;->isNull(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 286
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "Trying to set non-nullable field \'complexity\' to null."

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 269
    .end local v7    # "obj":Lcom/safonov/speedreading/training/fragment/math/repository/entity/MathConfig;
    .restart local v0    # "objectContext":Lio/realm/BaseRealm$RealmObjectContext;
    .restart local v8    # "pkColumnIndex":J
    .restart local v10    # "rowIndex":J
    .restart local v12    # "table":Lio/realm/internal/Table;
    :catchall_0
    move-exception v1

    invoke-virtual {v0}, Lio/realm/BaseRealm$RealmObjectContext;->clear()V

    throw v1

    .line 278
    .end local v0    # "objectContext":Lio/realm/BaseRealm$RealmObjectContext;
    .end local v8    # "pkColumnIndex":J
    .end local v10    # "rowIndex":J
    .end local v12    # "table":Lio/realm/internal/Table;
    .restart local v7    # "obj":Lcom/safonov/speedreading/training/fragment/math/repository/entity/MathConfig;
    :cond_3
    const-class v1, Lcom/safonov/speedreading/training/fragment/math/repository/entity/MathConfig;

    const-string v2, "id"

    invoke-virtual {p1, v2}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v3, 0x1

    invoke-virtual {p0, v1, v2, v3, v6}, Lio/realm/Realm;->createObjectInternal(Ljava/lang/Class;Ljava/lang/Object;ZLjava/util/List;)Lio/realm/RealmModel;

    move-result-object v7

    .end local v7    # "obj":Lcom/safonov/speedreading/training/fragment/math/repository/entity/MathConfig;
    check-cast v7, Lio/realm/MathConfigRealmProxy;

    .restart local v7    # "obj":Lcom/safonov/speedreading/training/fragment/math/repository/entity/MathConfig;
    goto :goto_0

    .line 281
    :cond_4
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "JSON object doesn\'t have the primary key field \'id\'."

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_5
    move-object v1, v7

    .line 288
    check-cast v1, Lio/realm/MathConfigRealmProxyInterface;

    const-string v2, "complexity"

    invoke-virtual {p1, v2}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v1, v2}, Lio/realm/MathConfigRealmProxyInterface;->realmSet$complexity(I)V

    .line 291
    :cond_6
    const-string v1, "duration"

    invoke-virtual {p1, v1}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_8

    .line 292
    const-string v1, "duration"

    invoke-virtual {p1, v1}, Lorg/json/JSONObject;->isNull(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_7

    .line 293
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "Trying to set non-nullable field \'duration\' to null."

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_7
    move-object v1, v7

    .line 295
    check-cast v1, Lio/realm/MathConfigRealmProxyInterface;

    const-string v2, "duration"

    invoke-virtual {p1, v2}, Lorg/json/JSONObject;->getLong(Ljava/lang/String;)J

    move-result-wide v2

    invoke-interface {v1, v2, v3}, Lio/realm/MathConfigRealmProxyInterface;->realmSet$duration(J)V

    .line 298
    :cond_8
    return-object v7
.end method

.method public static createRealmObjectSchema(Lio/realm/RealmSchema;)Lio/realm/RealmObjectSchema;
    .locals 11
    .param p0, "realmSchema"    # Lio/realm/RealmSchema;

    .prologue
    const/4 v7, 0x0

    const/4 v3, 0x1

    .line 155
    const-string v0, "MathConfig"

    invoke-virtual {p0, v0}, Lio/realm/RealmSchema;->contains(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 156
    const-string v0, "MathConfig"

    invoke-virtual {p0, v0}, Lio/realm/RealmSchema;->create(Ljava/lang/String;)Lio/realm/RealmObjectSchema;

    move-result-object v10

    .line 157
    .local v10, "realmObjectSchema":Lio/realm/RealmObjectSchema;
    new-instance v0, Lio/realm/Property;

    const-string v1, "id"

    sget-object v2, Lio/realm/RealmFieldType;->INTEGER:Lio/realm/RealmFieldType;

    move v4, v3

    move v5, v3

    invoke-direct/range {v0 .. v5}, Lio/realm/Property;-><init>(Ljava/lang/String;Lio/realm/RealmFieldType;ZZZ)V

    invoke-virtual {v10, v0}, Lio/realm/RealmObjectSchema;->add(Lio/realm/Property;)Lio/realm/RealmObjectSchema;

    .line 158
    new-instance v4, Lio/realm/Property;

    const-string v5, "complexity"

    sget-object v6, Lio/realm/RealmFieldType;->INTEGER:Lio/realm/RealmFieldType;

    move v8, v7

    move v9, v3

    invoke-direct/range {v4 .. v9}, Lio/realm/Property;-><init>(Ljava/lang/String;Lio/realm/RealmFieldType;ZZZ)V

    invoke-virtual {v10, v4}, Lio/realm/RealmObjectSchema;->add(Lio/realm/Property;)Lio/realm/RealmObjectSchema;

    .line 159
    new-instance v4, Lio/realm/Property;

    const-string v5, "duration"

    sget-object v6, Lio/realm/RealmFieldType;->INTEGER:Lio/realm/RealmFieldType;

    move v8, v7

    move v9, v3

    invoke-direct/range {v4 .. v9}, Lio/realm/Property;-><init>(Ljava/lang/String;Lio/realm/RealmFieldType;ZZZ)V

    invoke-virtual {v10, v4}, Lio/realm/RealmObjectSchema;->add(Lio/realm/Property;)Lio/realm/RealmObjectSchema;

    .line 162
    .end local v10    # "realmObjectSchema":Lio/realm/RealmObjectSchema;
    :goto_0
    return-object v10

    :cond_0
    const-string v0, "MathConfig"

    invoke-virtual {p0, v0}, Lio/realm/RealmSchema;->get(Ljava/lang/String;)Lio/realm/RealmObjectSchema;

    move-result-object v10

    goto :goto_0
.end method

.method public static createUsingJsonStream(Lio/realm/Realm;Landroid/util/JsonReader;)Lcom/safonov/speedreading/training/fragment/math/repository/entity/MathConfig;
    .locals 6
    .param p0, "realm"    # Lio/realm/Realm;
    .param p1, "reader"    # Landroid/util/JsonReader;
    .annotation build Landroid/annotation/TargetApi;
        value = 0xb
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 305
    const/4 v0, 0x0

    .line 306
    .local v0, "jsonHasPrimaryKey":Z
    new-instance v2, Lcom/safonov/speedreading/training/fragment/math/repository/entity/MathConfig;

    invoke-direct {v2}, Lcom/safonov/speedreading/training/fragment/math/repository/entity/MathConfig;-><init>()V

    .line 307
    .local v2, "obj":Lcom/safonov/speedreading/training/fragment/math/repository/entity/MathConfig;
    invoke-virtual {p1}, Landroid/util/JsonReader;->beginObject()V

    .line 308
    :goto_0
    invoke-virtual {p1}, Landroid/util/JsonReader;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_6

    .line 309
    invoke-virtual {p1}, Landroid/util/JsonReader;->nextName()Ljava/lang/String;

    move-result-object v1

    .line 310
    .local v1, "name":Ljava/lang/String;
    const-string v3, "id"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 311
    invoke-virtual {p1}, Landroid/util/JsonReader;->peek()Landroid/util/JsonToken;

    move-result-object v3

    sget-object v4, Landroid/util/JsonToken;->NULL:Landroid/util/JsonToken;

    if-ne v3, v4, :cond_0

    .line 312
    invoke-virtual {p1}, Landroid/util/JsonReader;->skipValue()V

    .line 313
    new-instance v3, Ljava/lang/IllegalArgumentException;

    const-string v4, "Trying to set non-nullable field \'id\' to null."

    invoke-direct {v3, v4}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v3

    :cond_0
    move-object v3, v2

    .line 315
    check-cast v3, Lio/realm/MathConfigRealmProxyInterface;

    invoke-virtual {p1}, Landroid/util/JsonReader;->nextInt()I

    move-result v4

    invoke-interface {v3, v4}, Lio/realm/MathConfigRealmProxyInterface;->realmSet$id(I)V

    .line 317
    const/4 v0, 0x1

    goto :goto_0

    .line 318
    :cond_1
    const-string v3, "complexity"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 319
    invoke-virtual {p1}, Landroid/util/JsonReader;->peek()Landroid/util/JsonToken;

    move-result-object v3

    sget-object v4, Landroid/util/JsonToken;->NULL:Landroid/util/JsonToken;

    if-ne v3, v4, :cond_2

    .line 320
    invoke-virtual {p1}, Landroid/util/JsonReader;->skipValue()V

    .line 321
    new-instance v3, Ljava/lang/IllegalArgumentException;

    const-string v4, "Trying to set non-nullable field \'complexity\' to null."

    invoke-direct {v3, v4}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v3

    :cond_2
    move-object v3, v2

    .line 323
    check-cast v3, Lio/realm/MathConfigRealmProxyInterface;

    invoke-virtual {p1}, Landroid/util/JsonReader;->nextInt()I

    move-result v4

    invoke-interface {v3, v4}, Lio/realm/MathConfigRealmProxyInterface;->realmSet$complexity(I)V

    goto :goto_0

    .line 325
    :cond_3
    const-string v3, "duration"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_5

    .line 326
    invoke-virtual {p1}, Landroid/util/JsonReader;->peek()Landroid/util/JsonToken;

    move-result-object v3

    sget-object v4, Landroid/util/JsonToken;->NULL:Landroid/util/JsonToken;

    if-ne v3, v4, :cond_4

    .line 327
    invoke-virtual {p1}, Landroid/util/JsonReader;->skipValue()V

    .line 328
    new-instance v3, Ljava/lang/IllegalArgumentException;

    const-string v4, "Trying to set non-nullable field \'duration\' to null."

    invoke-direct {v3, v4}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v3

    :cond_4
    move-object v3, v2

    .line 330
    check-cast v3, Lio/realm/MathConfigRealmProxyInterface;

    invoke-virtual {p1}, Landroid/util/JsonReader;->nextLong()J

    move-result-wide v4

    invoke-interface {v3, v4, v5}, Lio/realm/MathConfigRealmProxyInterface;->realmSet$duration(J)V

    goto :goto_0

    .line 333
    :cond_5
    invoke-virtual {p1}, Landroid/util/JsonReader;->skipValue()V

    goto :goto_0

    .line 336
    .end local v1    # "name":Ljava/lang/String;
    :cond_6
    invoke-virtual {p1}, Landroid/util/JsonReader;->endObject()V

    .line 337
    if-nez v0, :cond_7

    .line 338
    new-instance v3, Ljava/lang/IllegalArgumentException;

    const-string v4, "JSON object doesn\'t have the primary key field \'id\'."

    invoke-direct {v3, v4}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 340
    :cond_7
    invoke-virtual {p0, v2}, Lio/realm/Realm;->copyToRealm(Lio/realm/RealmModel;)Lio/realm/RealmModel;

    move-result-object v2

    .end local v2    # "obj":Lcom/safonov/speedreading/training/fragment/math/repository/entity/MathConfig;
    check-cast v2, Lcom/safonov/speedreading/training/fragment/math/repository/entity/MathConfig;

    .line 341
    .restart local v2    # "obj":Lcom/safonov/speedreading/training/fragment/math/repository/entity/MathConfig;
    return-object v2
.end method

.method public static getFieldNames()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 248
    sget-object v0, Lio/realm/MathConfigRealmProxy;->FIELD_NAMES:Ljava/util/List;

    return-object v0
.end method

.method public static getTableName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 244
    const-string v0, "class_MathConfig"

    return-object v0
.end method

.method public static initTable(Lio/realm/internal/SharedRealm;)Lio/realm/internal/Table;
    .locals 4
    .param p0, "sharedRealm"    # Lio/realm/internal/SharedRealm;

    .prologue
    const/4 v3, 0x0

    .line 166
    const-string v1, "class_MathConfig"

    invoke-virtual {p0, v1}, Lio/realm/internal/SharedRealm;->hasTable(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 167
    const-string v1, "class_MathConfig"

    invoke-virtual {p0, v1}, Lio/realm/internal/SharedRealm;->getTable(Ljava/lang/String;)Lio/realm/internal/Table;

    move-result-object v0

    .line 168
    .local v0, "table":Lio/realm/internal/Table;
    sget-object v1, Lio/realm/RealmFieldType;->INTEGER:Lio/realm/RealmFieldType;

    const-string v2, "id"

    invoke-virtual {v0, v1, v2, v3}, Lio/realm/internal/Table;->addColumn(Lio/realm/RealmFieldType;Ljava/lang/String;Z)J

    .line 169
    sget-object v1, Lio/realm/RealmFieldType;->INTEGER:Lio/realm/RealmFieldType;

    const-string v2, "complexity"

    invoke-virtual {v0, v1, v2, v3}, Lio/realm/internal/Table;->addColumn(Lio/realm/RealmFieldType;Ljava/lang/String;Z)J

    .line 170
    sget-object v1, Lio/realm/RealmFieldType;->INTEGER:Lio/realm/RealmFieldType;

    const-string v2, "duration"

    invoke-virtual {v0, v1, v2, v3}, Lio/realm/internal/Table;->addColumn(Lio/realm/RealmFieldType;Ljava/lang/String;Z)J

    .line 171
    const-string v1, "id"

    invoke-virtual {v0, v1}, Lio/realm/internal/Table;->getColumnIndex(Ljava/lang/String;)J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lio/realm/internal/Table;->addSearchIndex(J)V

    .line 172
    const-string v1, "id"

    invoke-virtual {v0, v1}, Lio/realm/internal/Table;->setPrimaryKey(Ljava/lang/String;)V

    .line 175
    .end local v0    # "table":Lio/realm/internal/Table;
    :goto_0
    return-object v0

    :cond_0
    const-string v1, "class_MathConfig"

    invoke-virtual {p0, v1}, Lio/realm/internal/SharedRealm;->getTable(Ljava/lang/String;)Lio/realm/internal/Table;

    move-result-object v0

    goto :goto_0
.end method

.method public static insert(Lio/realm/Realm;Lcom/safonov/speedreading/training/fragment/math/repository/entity/MathConfig;Ljava/util/Map;)J
    .locals 18
    .param p0, "realm"    # Lio/realm/Realm;
    .param p1, "object"    # Lcom/safonov/speedreading/training/fragment/math/repository/entity/MathConfig;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/realm/Realm;",
            "Lcom/safonov/speedreading/training/fragment/math/repository/entity/MathConfig;",
            "Ljava/util/Map",
            "<",
            "Lio/realm/RealmModel;",
            "Ljava/lang/Long;",
            ">;)J"
        }
    .end annotation

    .prologue
    .line 398
    .local p2, "cache":Ljava/util/Map;, "Ljava/util/Map<Lio/realm/RealmModel;Ljava/lang/Long;>;"
    move-object/from16 v0, p1

    instance-of v6, v0, Lio/realm/internal/RealmObjectProxy;

    if-eqz v6, :cond_0

    move-object/from16 v6, p1

    check-cast v6, Lio/realm/internal/RealmObjectProxy;

    invoke-interface {v6}, Lio/realm/internal/RealmObjectProxy;->realmGet$proxyState()Lio/realm/ProxyState;

    move-result-object v6

    invoke-virtual {v6}, Lio/realm/ProxyState;->getRealm$realm()Lio/realm/BaseRealm;

    move-result-object v6

    if-eqz v6, :cond_0

    move-object/from16 v6, p1

    check-cast v6, Lio/realm/internal/RealmObjectProxy;

    invoke-interface {v6}, Lio/realm/internal/RealmObjectProxy;->realmGet$proxyState()Lio/realm/ProxyState;

    move-result-object v6

    invoke-virtual {v6}, Lio/realm/ProxyState;->getRealm$realm()Lio/realm/BaseRealm;

    move-result-object v6

    invoke-virtual {v6}, Lio/realm/BaseRealm;->getPath()Ljava/lang/String;

    move-result-object v6

    invoke-virtual/range {p0 .. p0}, Lio/realm/Realm;->getPath()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 399
    check-cast p1, Lio/realm/internal/RealmObjectProxy;

    .end local p1    # "object":Lcom/safonov/speedreading/training/fragment/math/repository/entity/MathConfig;
    invoke-interface/range {p1 .. p1}, Lio/realm/internal/RealmObjectProxy;->realmGet$proxyState()Lio/realm/ProxyState;

    move-result-object v6

    invoke-virtual {v6}, Lio/realm/ProxyState;->getRow$realm()Lio/realm/internal/Row;

    move-result-object v6

    invoke-interface {v6}, Lio/realm/internal/Row;->getIndex()J

    move-result-wide v10

    .line 418
    :goto_0
    return-wide v10

    .line 401
    .restart local p1    # "object":Lcom/safonov/speedreading/training/fragment/math/repository/entity/MathConfig;
    :cond_0
    const-class v6, Lcom/safonov/speedreading/training/fragment/math/repository/entity/MathConfig;

    move-object/from16 v0, p0

    invoke-virtual {v0, v6}, Lio/realm/Realm;->getTable(Ljava/lang/Class;)Lio/realm/internal/Table;

    move-result-object v17

    .line 402
    .local v17, "table":Lio/realm/internal/Table;
    invoke-virtual/range {v17 .. v17}, Lio/realm/internal/Table;->getNativeTablePointer()J

    move-result-wide v2

    .line 403
    .local v2, "tableNativePtr":J
    move-object/from16 v0, p0

    iget-object v6, v0, Lio/realm/Realm;->schema:Lio/realm/RealmSchema;

    const-class v7, Lcom/safonov/speedreading/training/fragment/math/repository/entity/MathConfig;

    invoke-virtual {v6, v7}, Lio/realm/RealmSchema;->getColumnInfo(Ljava/lang/Class;)Lio/realm/internal/ColumnInfo;

    move-result-object v15

    check-cast v15, Lio/realm/MathConfigRealmProxy$MathConfigColumnInfo;

    .line 404
    .local v15, "columnInfo":Lio/realm/MathConfigRealmProxy$MathConfigColumnInfo;
    invoke-virtual/range {v17 .. v17}, Lio/realm/internal/Table;->getPrimaryKey()J

    move-result-wide v4

    .line 405
    .local v4, "pkColumnIndex":J
    const-wide/16 v10, -0x1

    .local v10, "rowIndex":J
    move-object/from16 v6, p1

    .line 406
    check-cast v6, Lio/realm/MathConfigRealmProxyInterface;

    invoke-interface {v6}, Lio/realm/MathConfigRealmProxyInterface;->realmGet$id()I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v16

    .line 407
    .local v16, "primaryKeyValue":Ljava/lang/Integer;
    if-eqz v16, :cond_1

    move-object/from16 v6, p1

    .line 408
    check-cast v6, Lio/realm/MathConfigRealmProxyInterface;

    invoke-interface {v6}, Lio/realm/MathConfigRealmProxyInterface;->realmGet$id()I

    move-result v6

    int-to-long v6, v6

    invoke-static/range {v2 .. v7}, Lio/realm/internal/Table;->nativeFindFirstInt(JJJ)J

    move-result-wide v10

    .line 410
    :cond_1
    const-wide/16 v6, -0x1

    cmp-long v6, v10, v6

    if-nez v6, :cond_2

    move-object/from16 v6, p1

    .line 411
    check-cast v6, Lio/realm/MathConfigRealmProxyInterface;

    invoke-interface {v6}, Lio/realm/MathConfigRealmProxyInterface;->realmGet$id()I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    const/4 v7, 0x0

    move-object/from16 v0, v17

    invoke-virtual {v0, v6, v7}, Lio/realm/internal/Table;->addEmptyRowWithPrimaryKey(Ljava/lang/Object;Z)J

    move-result-wide v10

    .line 415
    :goto_1
    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    move-object/from16 v0, p2

    move-object/from16 v1, p1

    invoke-interface {v0, v1, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 416
    iget-wide v8, v15, Lio/realm/MathConfigRealmProxy$MathConfigColumnInfo;->complexityIndex:J

    move-object/from16 v6, p1

    check-cast v6, Lio/realm/MathConfigRealmProxyInterface;

    invoke-interface {v6}, Lio/realm/MathConfigRealmProxyInterface;->realmGet$complexity()I

    move-result v6

    int-to-long v12, v6

    const/4 v14, 0x0

    move-wide v6, v2

    invoke-static/range {v6 .. v14}, Lio/realm/internal/Table;->nativeSetLong(JJJJZ)V

    .line 417
    iget-wide v8, v15, Lio/realm/MathConfigRealmProxy$MathConfigColumnInfo;->durationIndex:J

    check-cast p1, Lio/realm/MathConfigRealmProxyInterface;

    .end local p1    # "object":Lcom/safonov/speedreading/training/fragment/math/repository/entity/MathConfig;
    invoke-interface/range {p1 .. p1}, Lio/realm/MathConfigRealmProxyInterface;->realmGet$duration()J

    move-result-wide v12

    const/4 v14, 0x0

    move-wide v6, v2

    invoke-static/range {v6 .. v14}, Lio/realm/internal/Table;->nativeSetLong(JJJJZ)V

    goto :goto_0

    .line 413
    .restart local p1    # "object":Lcom/safonov/speedreading/training/fragment/math/repository/entity/MathConfig;
    :cond_2
    invoke-static/range {v16 .. v16}, Lio/realm/internal/Table;->throwDuplicatePrimaryKeyException(Ljava/lang/Object;)V

    goto :goto_1
.end method

.method public static insert(Lio/realm/Realm;Ljava/util/Iterator;Ljava/util/Map;)V
    .locals 19
    .param p0, "realm"    # Lio/realm/Realm;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/realm/Realm;",
            "Ljava/util/Iterator",
            "<+",
            "Lio/realm/RealmModel;",
            ">;",
            "Ljava/util/Map",
            "<",
            "Lio/realm/RealmModel;",
            "Ljava/lang/Long;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 422
    .local p1, "objects":Ljava/util/Iterator;, "Ljava/util/Iterator<+Lio/realm/RealmModel;>;"
    .local p2, "cache":Ljava/util/Map;, "Ljava/util/Map<Lio/realm/RealmModel;Ljava/lang/Long;>;"
    const-class v6, Lcom/safonov/speedreading/training/fragment/math/repository/entity/MathConfig;

    move-object/from16 v0, p0

    invoke-virtual {v0, v6}, Lio/realm/Realm;->getTable(Ljava/lang/Class;)Lio/realm/internal/Table;

    move-result-object v18

    .line 423
    .local v18, "table":Lio/realm/internal/Table;
    invoke-virtual/range {v18 .. v18}, Lio/realm/internal/Table;->getNativeTablePointer()J

    move-result-wide v2

    .line 424
    .local v2, "tableNativePtr":J
    move-object/from16 v0, p0

    iget-object v6, v0, Lio/realm/Realm;->schema:Lio/realm/RealmSchema;

    const-class v7, Lcom/safonov/speedreading/training/fragment/math/repository/entity/MathConfig;

    invoke-virtual {v6, v7}, Lio/realm/RealmSchema;->getColumnInfo(Ljava/lang/Class;)Lio/realm/internal/ColumnInfo;

    move-result-object v15

    check-cast v15, Lio/realm/MathConfigRealmProxy$MathConfigColumnInfo;

    .line 425
    .local v15, "columnInfo":Lio/realm/MathConfigRealmProxy$MathConfigColumnInfo;
    invoke-virtual/range {v18 .. v18}, Lio/realm/internal/Table;->getPrimaryKey()J

    move-result-wide v4

    .line 426
    .local v4, "pkColumnIndex":J
    const/16 v16, 0x0

    .line 427
    .local v16, "object":Lcom/safonov/speedreading/training/fragment/math/repository/entity/MathConfig;
    :cond_0
    :goto_0
    invoke-interface/range {p1 .. p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_4

    .line 428
    invoke-interface/range {p1 .. p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v16

    .end local v16    # "object":Lcom/safonov/speedreading/training/fragment/math/repository/entity/MathConfig;
    check-cast v16, Lcom/safonov/speedreading/training/fragment/math/repository/entity/MathConfig;

    .line 429
    .restart local v16    # "object":Lcom/safonov/speedreading/training/fragment/math/repository/entity/MathConfig;
    move-object/from16 v0, p2

    move-object/from16 v1, v16

    invoke-interface {v0, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_0

    .line 430
    move-object/from16 v0, v16

    instance-of v6, v0, Lio/realm/internal/RealmObjectProxy;

    if-eqz v6, :cond_1

    move-object/from16 v6, v16

    check-cast v6, Lio/realm/internal/RealmObjectProxy;

    invoke-interface {v6}, Lio/realm/internal/RealmObjectProxy;->realmGet$proxyState()Lio/realm/ProxyState;

    move-result-object v6

    invoke-virtual {v6}, Lio/realm/ProxyState;->getRealm$realm()Lio/realm/BaseRealm;

    move-result-object v6

    if-eqz v6, :cond_1

    move-object/from16 v6, v16

    check-cast v6, Lio/realm/internal/RealmObjectProxy;

    invoke-interface {v6}, Lio/realm/internal/RealmObjectProxy;->realmGet$proxyState()Lio/realm/ProxyState;

    move-result-object v6

    invoke-virtual {v6}, Lio/realm/ProxyState;->getRealm$realm()Lio/realm/BaseRealm;

    move-result-object v6

    invoke-virtual {v6}, Lio/realm/BaseRealm;->getPath()Ljava/lang/String;

    move-result-object v6

    invoke-virtual/range {p0 .. p0}, Lio/realm/Realm;->getPath()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1

    move-object/from16 v6, v16

    .line 431
    check-cast v6, Lio/realm/internal/RealmObjectProxy;

    invoke-interface {v6}, Lio/realm/internal/RealmObjectProxy;->realmGet$proxyState()Lio/realm/ProxyState;

    move-result-object v6

    invoke-virtual {v6}, Lio/realm/ProxyState;->getRow$realm()Lio/realm/internal/Row;

    move-result-object v6

    invoke-interface {v6}, Lio/realm/internal/Row;->getIndex()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    move-object/from16 v0, p2

    move-object/from16 v1, v16

    invoke-interface {v0, v1, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 434
    :cond_1
    const-wide/16 v10, -0x1

    .local v10, "rowIndex":J
    move-object/from16 v6, v16

    .line 435
    check-cast v6, Lio/realm/MathConfigRealmProxyInterface;

    invoke-interface {v6}, Lio/realm/MathConfigRealmProxyInterface;->realmGet$id()I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v17

    .line 436
    .local v17, "primaryKeyValue":Ljava/lang/Integer;
    if-eqz v17, :cond_2

    move-object/from16 v6, v16

    .line 437
    check-cast v6, Lio/realm/MathConfigRealmProxyInterface;

    invoke-interface {v6}, Lio/realm/MathConfigRealmProxyInterface;->realmGet$id()I

    move-result v6

    int-to-long v6, v6

    invoke-static/range {v2 .. v7}, Lio/realm/internal/Table;->nativeFindFirstInt(JJJ)J

    move-result-wide v10

    .line 439
    :cond_2
    const-wide/16 v6, -0x1

    cmp-long v6, v10, v6

    if-nez v6, :cond_3

    move-object/from16 v6, v16

    .line 440
    check-cast v6, Lio/realm/MathConfigRealmProxyInterface;

    invoke-interface {v6}, Lio/realm/MathConfigRealmProxyInterface;->realmGet$id()I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    const/4 v7, 0x0

    move-object/from16 v0, v18

    invoke-virtual {v0, v6, v7}, Lio/realm/internal/Table;->addEmptyRowWithPrimaryKey(Ljava/lang/Object;Z)J

    move-result-wide v10

    .line 444
    :goto_1
    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    move-object/from16 v0, p2

    move-object/from16 v1, v16

    invoke-interface {v0, v1, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 445
    iget-wide v8, v15, Lio/realm/MathConfigRealmProxy$MathConfigColumnInfo;->complexityIndex:J

    move-object/from16 v6, v16

    check-cast v6, Lio/realm/MathConfigRealmProxyInterface;

    invoke-interface {v6}, Lio/realm/MathConfigRealmProxyInterface;->realmGet$complexity()I

    move-result v6

    int-to-long v12, v6

    const/4 v14, 0x0

    move-wide v6, v2

    invoke-static/range {v6 .. v14}, Lio/realm/internal/Table;->nativeSetLong(JJJJZ)V

    .line 446
    iget-wide v8, v15, Lio/realm/MathConfigRealmProxy$MathConfigColumnInfo;->durationIndex:J

    move-object/from16 v6, v16

    check-cast v6, Lio/realm/MathConfigRealmProxyInterface;

    invoke-interface {v6}, Lio/realm/MathConfigRealmProxyInterface;->realmGet$duration()J

    move-result-wide v12

    const/4 v14, 0x0

    move-wide v6, v2

    invoke-static/range {v6 .. v14}, Lio/realm/internal/Table;->nativeSetLong(JJJJZ)V

    goto/16 :goto_0

    .line 442
    :cond_3
    invoke-static/range {v17 .. v17}, Lio/realm/internal/Table;->throwDuplicatePrimaryKeyException(Ljava/lang/Object;)V

    goto :goto_1

    .line 449
    .end local v10    # "rowIndex":J
    .end local v17    # "primaryKeyValue":Ljava/lang/Integer;
    :cond_4
    return-void
.end method

.method public static insertOrUpdate(Lio/realm/Realm;Lcom/safonov/speedreading/training/fragment/math/repository/entity/MathConfig;Ljava/util/Map;)J
    .locals 18
    .param p0, "realm"    # Lio/realm/Realm;
    .param p1, "object"    # Lcom/safonov/speedreading/training/fragment/math/repository/entity/MathConfig;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/realm/Realm;",
            "Lcom/safonov/speedreading/training/fragment/math/repository/entity/MathConfig;",
            "Ljava/util/Map",
            "<",
            "Lio/realm/RealmModel;",
            "Ljava/lang/Long;",
            ">;)J"
        }
    .end annotation

    .prologue
    .line 452
    .local p2, "cache":Ljava/util/Map;, "Ljava/util/Map<Lio/realm/RealmModel;Ljava/lang/Long;>;"
    move-object/from16 v0, p1

    instance-of v6, v0, Lio/realm/internal/RealmObjectProxy;

    if-eqz v6, :cond_0

    move-object/from16 v6, p1

    check-cast v6, Lio/realm/internal/RealmObjectProxy;

    invoke-interface {v6}, Lio/realm/internal/RealmObjectProxy;->realmGet$proxyState()Lio/realm/ProxyState;

    move-result-object v6

    invoke-virtual {v6}, Lio/realm/ProxyState;->getRealm$realm()Lio/realm/BaseRealm;

    move-result-object v6

    if-eqz v6, :cond_0

    move-object/from16 v6, p1

    check-cast v6, Lio/realm/internal/RealmObjectProxy;

    invoke-interface {v6}, Lio/realm/internal/RealmObjectProxy;->realmGet$proxyState()Lio/realm/ProxyState;

    move-result-object v6

    invoke-virtual {v6}, Lio/realm/ProxyState;->getRealm$realm()Lio/realm/BaseRealm;

    move-result-object v6

    invoke-virtual {v6}, Lio/realm/BaseRealm;->getPath()Ljava/lang/String;

    move-result-object v6

    invoke-virtual/range {p0 .. p0}, Lio/realm/Realm;->getPath()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 453
    check-cast p1, Lio/realm/internal/RealmObjectProxy;

    .end local p1    # "object":Lcom/safonov/speedreading/training/fragment/math/repository/entity/MathConfig;
    invoke-interface/range {p1 .. p1}, Lio/realm/internal/RealmObjectProxy;->realmGet$proxyState()Lio/realm/ProxyState;

    move-result-object v6

    invoke-virtual {v6}, Lio/realm/ProxyState;->getRow$realm()Lio/realm/internal/Row;

    move-result-object v6

    invoke-interface {v6}, Lio/realm/internal/Row;->getIndex()J

    move-result-wide v10

    .line 470
    :goto_0
    return-wide v10

    .line 455
    .restart local p1    # "object":Lcom/safonov/speedreading/training/fragment/math/repository/entity/MathConfig;
    :cond_0
    const-class v6, Lcom/safonov/speedreading/training/fragment/math/repository/entity/MathConfig;

    move-object/from16 v0, p0

    invoke-virtual {v0, v6}, Lio/realm/Realm;->getTable(Ljava/lang/Class;)Lio/realm/internal/Table;

    move-result-object v17

    .line 456
    .local v17, "table":Lio/realm/internal/Table;
    invoke-virtual/range {v17 .. v17}, Lio/realm/internal/Table;->getNativeTablePointer()J

    move-result-wide v2

    .line 457
    .local v2, "tableNativePtr":J
    move-object/from16 v0, p0

    iget-object v6, v0, Lio/realm/Realm;->schema:Lio/realm/RealmSchema;

    const-class v7, Lcom/safonov/speedreading/training/fragment/math/repository/entity/MathConfig;

    invoke-virtual {v6, v7}, Lio/realm/RealmSchema;->getColumnInfo(Ljava/lang/Class;)Lio/realm/internal/ColumnInfo;

    move-result-object v15

    check-cast v15, Lio/realm/MathConfigRealmProxy$MathConfigColumnInfo;

    .line 458
    .local v15, "columnInfo":Lio/realm/MathConfigRealmProxy$MathConfigColumnInfo;
    invoke-virtual/range {v17 .. v17}, Lio/realm/internal/Table;->getPrimaryKey()J

    move-result-wide v4

    .line 459
    .local v4, "pkColumnIndex":J
    const-wide/16 v10, -0x1

    .local v10, "rowIndex":J
    move-object/from16 v6, p1

    .line 460
    check-cast v6, Lio/realm/MathConfigRealmProxyInterface;

    invoke-interface {v6}, Lio/realm/MathConfigRealmProxyInterface;->realmGet$id()I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v16

    .line 461
    .local v16, "primaryKeyValue":Ljava/lang/Integer;
    if-eqz v16, :cond_1

    move-object/from16 v6, p1

    .line 462
    check-cast v6, Lio/realm/MathConfigRealmProxyInterface;

    invoke-interface {v6}, Lio/realm/MathConfigRealmProxyInterface;->realmGet$id()I

    move-result v6

    int-to-long v6, v6

    invoke-static/range {v2 .. v7}, Lio/realm/internal/Table;->nativeFindFirstInt(JJJ)J

    move-result-wide v10

    .line 464
    :cond_1
    const-wide/16 v6, -0x1

    cmp-long v6, v10, v6

    if-nez v6, :cond_2

    move-object/from16 v6, p1

    .line 465
    check-cast v6, Lio/realm/MathConfigRealmProxyInterface;

    invoke-interface {v6}, Lio/realm/MathConfigRealmProxyInterface;->realmGet$id()I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    const/4 v7, 0x0

    move-object/from16 v0, v17

    invoke-virtual {v0, v6, v7}, Lio/realm/internal/Table;->addEmptyRowWithPrimaryKey(Ljava/lang/Object;Z)J

    move-result-wide v10

    .line 467
    :cond_2
    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    move-object/from16 v0, p2

    move-object/from16 v1, p1

    invoke-interface {v0, v1, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 468
    iget-wide v8, v15, Lio/realm/MathConfigRealmProxy$MathConfigColumnInfo;->complexityIndex:J

    move-object/from16 v6, p1

    check-cast v6, Lio/realm/MathConfigRealmProxyInterface;

    invoke-interface {v6}, Lio/realm/MathConfigRealmProxyInterface;->realmGet$complexity()I

    move-result v6

    int-to-long v12, v6

    const/4 v14, 0x0

    move-wide v6, v2

    invoke-static/range {v6 .. v14}, Lio/realm/internal/Table;->nativeSetLong(JJJJZ)V

    .line 469
    iget-wide v8, v15, Lio/realm/MathConfigRealmProxy$MathConfigColumnInfo;->durationIndex:J

    check-cast p1, Lio/realm/MathConfigRealmProxyInterface;

    .end local p1    # "object":Lcom/safonov/speedreading/training/fragment/math/repository/entity/MathConfig;
    invoke-interface/range {p1 .. p1}, Lio/realm/MathConfigRealmProxyInterface;->realmGet$duration()J

    move-result-wide v12

    const/4 v14, 0x0

    move-wide v6, v2

    invoke-static/range {v6 .. v14}, Lio/realm/internal/Table;->nativeSetLong(JJJJZ)V

    goto :goto_0
.end method

.method public static insertOrUpdate(Lio/realm/Realm;Ljava/util/Iterator;Ljava/util/Map;)V
    .locals 19
    .param p0, "realm"    # Lio/realm/Realm;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/realm/Realm;",
            "Ljava/util/Iterator",
            "<+",
            "Lio/realm/RealmModel;",
            ">;",
            "Ljava/util/Map",
            "<",
            "Lio/realm/RealmModel;",
            "Ljava/lang/Long;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 474
    .local p1, "objects":Ljava/util/Iterator;, "Ljava/util/Iterator<+Lio/realm/RealmModel;>;"
    .local p2, "cache":Ljava/util/Map;, "Ljava/util/Map<Lio/realm/RealmModel;Ljava/lang/Long;>;"
    const-class v6, Lcom/safonov/speedreading/training/fragment/math/repository/entity/MathConfig;

    move-object/from16 v0, p0

    invoke-virtual {v0, v6}, Lio/realm/Realm;->getTable(Ljava/lang/Class;)Lio/realm/internal/Table;

    move-result-object v18

    .line 475
    .local v18, "table":Lio/realm/internal/Table;
    invoke-virtual/range {v18 .. v18}, Lio/realm/internal/Table;->getNativeTablePointer()J

    move-result-wide v2

    .line 476
    .local v2, "tableNativePtr":J
    move-object/from16 v0, p0

    iget-object v6, v0, Lio/realm/Realm;->schema:Lio/realm/RealmSchema;

    const-class v7, Lcom/safonov/speedreading/training/fragment/math/repository/entity/MathConfig;

    invoke-virtual {v6, v7}, Lio/realm/RealmSchema;->getColumnInfo(Ljava/lang/Class;)Lio/realm/internal/ColumnInfo;

    move-result-object v15

    check-cast v15, Lio/realm/MathConfigRealmProxy$MathConfigColumnInfo;

    .line 477
    .local v15, "columnInfo":Lio/realm/MathConfigRealmProxy$MathConfigColumnInfo;
    invoke-virtual/range {v18 .. v18}, Lio/realm/internal/Table;->getPrimaryKey()J

    move-result-wide v4

    .line 478
    .local v4, "pkColumnIndex":J
    const/16 v16, 0x0

    .line 479
    .local v16, "object":Lcom/safonov/speedreading/training/fragment/math/repository/entity/MathConfig;
    :cond_0
    :goto_0
    invoke-interface/range {p1 .. p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_4

    .line 480
    invoke-interface/range {p1 .. p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v16

    .end local v16    # "object":Lcom/safonov/speedreading/training/fragment/math/repository/entity/MathConfig;
    check-cast v16, Lcom/safonov/speedreading/training/fragment/math/repository/entity/MathConfig;

    .line 481
    .restart local v16    # "object":Lcom/safonov/speedreading/training/fragment/math/repository/entity/MathConfig;
    move-object/from16 v0, p2

    move-object/from16 v1, v16

    invoke-interface {v0, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_0

    .line 482
    move-object/from16 v0, v16

    instance-of v6, v0, Lio/realm/internal/RealmObjectProxy;

    if-eqz v6, :cond_1

    move-object/from16 v6, v16

    check-cast v6, Lio/realm/internal/RealmObjectProxy;

    invoke-interface {v6}, Lio/realm/internal/RealmObjectProxy;->realmGet$proxyState()Lio/realm/ProxyState;

    move-result-object v6

    invoke-virtual {v6}, Lio/realm/ProxyState;->getRealm$realm()Lio/realm/BaseRealm;

    move-result-object v6

    if-eqz v6, :cond_1

    move-object/from16 v6, v16

    check-cast v6, Lio/realm/internal/RealmObjectProxy;

    invoke-interface {v6}, Lio/realm/internal/RealmObjectProxy;->realmGet$proxyState()Lio/realm/ProxyState;

    move-result-object v6

    invoke-virtual {v6}, Lio/realm/ProxyState;->getRealm$realm()Lio/realm/BaseRealm;

    move-result-object v6

    invoke-virtual {v6}, Lio/realm/BaseRealm;->getPath()Ljava/lang/String;

    move-result-object v6

    invoke-virtual/range {p0 .. p0}, Lio/realm/Realm;->getPath()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1

    move-object/from16 v6, v16

    .line 483
    check-cast v6, Lio/realm/internal/RealmObjectProxy;

    invoke-interface {v6}, Lio/realm/internal/RealmObjectProxy;->realmGet$proxyState()Lio/realm/ProxyState;

    move-result-object v6

    invoke-virtual {v6}, Lio/realm/ProxyState;->getRow$realm()Lio/realm/internal/Row;

    move-result-object v6

    invoke-interface {v6}, Lio/realm/internal/Row;->getIndex()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    move-object/from16 v0, p2

    move-object/from16 v1, v16

    invoke-interface {v0, v1, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 486
    :cond_1
    const-wide/16 v10, -0x1

    .local v10, "rowIndex":J
    move-object/from16 v6, v16

    .line 487
    check-cast v6, Lio/realm/MathConfigRealmProxyInterface;

    invoke-interface {v6}, Lio/realm/MathConfigRealmProxyInterface;->realmGet$id()I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v17

    .line 488
    .local v17, "primaryKeyValue":Ljava/lang/Integer;
    if-eqz v17, :cond_2

    move-object/from16 v6, v16

    .line 489
    check-cast v6, Lio/realm/MathConfigRealmProxyInterface;

    invoke-interface {v6}, Lio/realm/MathConfigRealmProxyInterface;->realmGet$id()I

    move-result v6

    int-to-long v6, v6

    invoke-static/range {v2 .. v7}, Lio/realm/internal/Table;->nativeFindFirstInt(JJJ)J

    move-result-wide v10

    .line 491
    :cond_2
    const-wide/16 v6, -0x1

    cmp-long v6, v10, v6

    if-nez v6, :cond_3

    move-object/from16 v6, v16

    .line 492
    check-cast v6, Lio/realm/MathConfigRealmProxyInterface;

    invoke-interface {v6}, Lio/realm/MathConfigRealmProxyInterface;->realmGet$id()I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    const/4 v7, 0x0

    move-object/from16 v0, v18

    invoke-virtual {v0, v6, v7}, Lio/realm/internal/Table;->addEmptyRowWithPrimaryKey(Ljava/lang/Object;Z)J

    move-result-wide v10

    .line 494
    :cond_3
    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    move-object/from16 v0, p2

    move-object/from16 v1, v16

    invoke-interface {v0, v1, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 495
    iget-wide v8, v15, Lio/realm/MathConfigRealmProxy$MathConfigColumnInfo;->complexityIndex:J

    move-object/from16 v6, v16

    check-cast v6, Lio/realm/MathConfigRealmProxyInterface;

    invoke-interface {v6}, Lio/realm/MathConfigRealmProxyInterface;->realmGet$complexity()I

    move-result v6

    int-to-long v12, v6

    const/4 v14, 0x0

    move-wide v6, v2

    invoke-static/range {v6 .. v14}, Lio/realm/internal/Table;->nativeSetLong(JJJJZ)V

    .line 496
    iget-wide v8, v15, Lio/realm/MathConfigRealmProxy$MathConfigColumnInfo;->durationIndex:J

    move-object/from16 v6, v16

    check-cast v6, Lio/realm/MathConfigRealmProxyInterface;

    invoke-interface {v6}, Lio/realm/MathConfigRealmProxyInterface;->realmGet$duration()J

    move-result-wide v12

    const/4 v14, 0x0

    move-wide v6, v2

    invoke-static/range {v6 .. v14}, Lio/realm/internal/Table;->nativeSetLong(JJJJZ)V

    goto/16 :goto_0

    .line 499
    .end local v10    # "rowIndex":J
    .end local v17    # "primaryKeyValue":Ljava/lang/Integer;
    :cond_4
    return-void
.end method

.method static update(Lio/realm/Realm;Lcom/safonov/speedreading/training/fragment/math/repository/entity/MathConfig;Lcom/safonov/speedreading/training/fragment/math/repository/entity/MathConfig;Ljava/util/Map;)Lcom/safonov/speedreading/training/fragment/math/repository/entity/MathConfig;
    .locals 4
    .param p0, "realm"    # Lio/realm/Realm;
    .param p1, "realmObject"    # Lcom/safonov/speedreading/training/fragment/math/repository/entity/MathConfig;
    .param p2, "newObject"    # Lcom/safonov/speedreading/training/fragment/math/repository/entity/MathConfig;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/realm/Realm;",
            "Lcom/safonov/speedreading/training/fragment/math/repository/entity/MathConfig;",
            "Lcom/safonov/speedreading/training/fragment/math/repository/entity/MathConfig;",
            "Ljava/util/Map",
            "<",
            "Lio/realm/RealmModel;",
            "Lio/realm/internal/RealmObjectProxy;",
            ">;)",
            "Lcom/safonov/speedreading/training/fragment/math/repository/entity/MathConfig;"
        }
    .end annotation

    .prologue
    .line 526
    .local p3, "cache":Ljava/util/Map;, "Ljava/util/Map<Lio/realm/RealmModel;Lio/realm/internal/RealmObjectProxy;>;"
    move-object v0, p1

    check-cast v0, Lio/realm/MathConfigRealmProxyInterface;

    move-object v1, p2

    check-cast v1, Lio/realm/MathConfigRealmProxyInterface;

    invoke-interface {v1}, Lio/realm/MathConfigRealmProxyInterface;->realmGet$complexity()I

    move-result v1

    invoke-interface {v0, v1}, Lio/realm/MathConfigRealmProxyInterface;->realmSet$complexity(I)V

    move-object v0, p1

    .line 527
    check-cast v0, Lio/realm/MathConfigRealmProxyInterface;

    check-cast p2, Lio/realm/MathConfigRealmProxyInterface;

    .end local p2    # "newObject":Lcom/safonov/speedreading/training/fragment/math/repository/entity/MathConfig;
    invoke-interface {p2}, Lio/realm/MathConfigRealmProxyInterface;->realmGet$duration()J

    move-result-wide v2

    invoke-interface {v0, v2, v3}, Lio/realm/MathConfigRealmProxyInterface;->realmSet$duration(J)V

    .line 528
    return-object p1
.end method

.method public static validateTable(Lio/realm/internal/SharedRealm;Z)Lio/realm/MathConfigRealmProxy$MathConfigColumnInfo;
    .locals 12
    .param p0, "sharedRealm"    # Lio/realm/internal/SharedRealm;
    .param p1, "allowExtraColumns"    # Z

    .prologue
    const-wide/16 v8, 0x3

    .line 179
    const-string v7, "class_MathConfig"

    invoke-virtual {p0, v7}, Lio/realm/internal/SharedRealm;->hasTable(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_f

    .line 180
    const-string v7, "class_MathConfig"

    invoke-virtual {p0, v7}, Lio/realm/internal/SharedRealm;->getTable(Ljava/lang/String;)Lio/realm/internal/Table;

    move-result-object v6

    .line 181
    .local v6, "table":Lio/realm/internal/Table;
    invoke-virtual {v6}, Lio/realm/internal/Table;->getColumnCount()J

    move-result-wide v0

    .line 182
    .local v0, "columnCount":J
    cmp-long v7, v0, v8

    if-eqz v7, :cond_1

    .line 183
    cmp-long v7, v0, v8

    if-gez v7, :cond_0

    .line 184
    new-instance v7, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->getPath()Ljava/lang/String;

    move-result-object v8

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Field count is less than expected - expected 3 but was "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-direct {v7, v8, v9}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v7

    .line 186
    :cond_0
    if-eqz p1, :cond_2

    .line 187
    const-string v7, "Field count is more than expected - expected 3 but was %1$d"

    const/4 v8, 0x1

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v10

    aput-object v10, v8, v9

    invoke-static {v7, v8}, Lio/realm/log/RealmLog;->debug(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 192
    :cond_1
    new-instance v3, Ljava/util/HashMap;

    invoke-direct {v3}, Ljava/util/HashMap;-><init>()V

    .line 193
    .local v3, "columnTypes":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lio/realm/RealmFieldType;>;"
    const-wide/16 v4, 0x0

    .local v4, "i":J
    :goto_0
    cmp-long v7, v4, v0

    if-gez v7, :cond_3

    .line 194
    invoke-virtual {v6, v4, v5}, Lio/realm/internal/Table;->getColumnName(J)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v4, v5}, Lio/realm/internal/Table;->getColumnType(J)Lio/realm/RealmFieldType;

    move-result-object v8

    invoke-interface {v3, v7, v8}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 193
    const-wide/16 v8, 0x1

    add-long/2addr v4, v8

    goto :goto_0

    .line 189
    .end local v3    # "columnTypes":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lio/realm/RealmFieldType;>;"
    .end local v4    # "i":J
    :cond_2
    new-instance v7, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->getPath()Ljava/lang/String;

    move-result-object v8

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Field count is more than expected - expected 3 but was "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-direct {v7, v8, v9}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v7

    .line 197
    .restart local v3    # "columnTypes":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lio/realm/RealmFieldType;>;"
    .restart local v4    # "i":J
    :cond_3
    new-instance v2, Lio/realm/MathConfigRealmProxy$MathConfigColumnInfo;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->getPath()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v2, v7, v6}, Lio/realm/MathConfigRealmProxy$MathConfigColumnInfo;-><init>(Ljava/lang/String;Lio/realm/internal/Table;)V

    .line 199
    .local v2, "columnInfo":Lio/realm/MathConfigRealmProxy$MathConfigColumnInfo;
    invoke-virtual {v6}, Lio/realm/internal/Table;->hasPrimaryKey()Z

    move-result v7

    if-nez v7, :cond_4

    .line 200
    new-instance v7, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->getPath()Ljava/lang/String;

    move-result-object v8

    const-string v9, "Primary key not defined for field \'id\' in existing Realm file. @PrimaryKey was added."

    invoke-direct {v7, v8, v9}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v7

    .line 202
    :cond_4
    invoke-virtual {v6}, Lio/realm/internal/Table;->getPrimaryKey()J

    move-result-wide v8

    iget-wide v10, v2, Lio/realm/MathConfigRealmProxy$MathConfigColumnInfo;->idIndex:J

    cmp-long v7, v8, v10

    if-eqz v7, :cond_5

    .line 203
    new-instance v7, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->getPath()Ljava/lang/String;

    move-result-object v8

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Primary Key annotation definition was changed, from field "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v6}, Lio/realm/internal/Table;->getPrimaryKey()J

    move-result-wide v10

    invoke-virtual {v6, v10, v11}, Lio/realm/internal/Table;->getColumnName(J)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " to field id"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-direct {v7, v8, v9}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v7

    .line 207
    :cond_5
    const-string v7, "id"

    invoke-interface {v3, v7}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_6

    .line 208
    new-instance v7, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->getPath()Ljava/lang/String;

    move-result-object v8

    const-string v9, "Missing field \'id\' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn()."

    invoke-direct {v7, v8, v9}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v7

    .line 210
    :cond_6
    const-string v7, "id"

    invoke-interface {v3, v7}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    sget-object v8, Lio/realm/RealmFieldType;->INTEGER:Lio/realm/RealmFieldType;

    if-eq v7, v8, :cond_7

    .line 211
    new-instance v7, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->getPath()Ljava/lang/String;

    move-result-object v8

    const-string v9, "Invalid type \'int\' for field \'id\' in existing Realm file."

    invoke-direct {v7, v8, v9}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v7

    .line 213
    :cond_7
    iget-wide v8, v2, Lio/realm/MathConfigRealmProxy$MathConfigColumnInfo;->idIndex:J

    invoke-virtual {v6, v8, v9}, Lio/realm/internal/Table;->isColumnNullable(J)Z

    move-result v7

    if-eqz v7, :cond_8

    iget-wide v8, v2, Lio/realm/MathConfigRealmProxy$MathConfigColumnInfo;->idIndex:J

    invoke-virtual {v6, v8, v9}, Lio/realm/internal/Table;->findFirstNull(J)J

    move-result-wide v8

    const-wide/16 v10, -0x1

    cmp-long v7, v8, v10

    if-eqz v7, :cond_8

    .line 214
    new-instance v7, Ljava/lang/IllegalStateException;

    const-string v8, "Cannot migrate an object with null value in field \'id\'. Either maintain the same type for primary key field \'id\', or remove the object with null value before migration."

    invoke-direct {v7, v8}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v7

    .line 216
    :cond_8
    const-string v7, "id"

    invoke-virtual {v6, v7}, Lio/realm/internal/Table;->getColumnIndex(Ljava/lang/String;)J

    move-result-wide v8

    invoke-virtual {v6, v8, v9}, Lio/realm/internal/Table;->hasSearchIndex(J)Z

    move-result v7

    if-nez v7, :cond_9

    .line 217
    new-instance v7, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->getPath()Ljava/lang/String;

    move-result-object v8

    const-string v9, "Index not defined for field \'id\' in existing Realm file. Either set @Index or migrate using io.realm.internal.Table.removeSearchIndex()."

    invoke-direct {v7, v8, v9}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v7

    .line 219
    :cond_9
    const-string v7, "complexity"

    invoke-interface {v3, v7}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_a

    .line 220
    new-instance v7, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->getPath()Ljava/lang/String;

    move-result-object v8

    const-string v9, "Missing field \'complexity\' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn()."

    invoke-direct {v7, v8, v9}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v7

    .line 222
    :cond_a
    const-string v7, "complexity"

    invoke-interface {v3, v7}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    sget-object v8, Lio/realm/RealmFieldType;->INTEGER:Lio/realm/RealmFieldType;

    if-eq v7, v8, :cond_b

    .line 223
    new-instance v7, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->getPath()Ljava/lang/String;

    move-result-object v8

    const-string v9, "Invalid type \'int\' for field \'complexity\' in existing Realm file."

    invoke-direct {v7, v8, v9}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v7

    .line 225
    :cond_b
    iget-wide v8, v2, Lio/realm/MathConfigRealmProxy$MathConfigColumnInfo;->complexityIndex:J

    invoke-virtual {v6, v8, v9}, Lio/realm/internal/Table;->isColumnNullable(J)Z

    move-result v7

    if-eqz v7, :cond_c

    .line 226
    new-instance v7, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->getPath()Ljava/lang/String;

    move-result-object v8

    const-string v9, "Field \'complexity\' does support null values in the existing Realm file. Use corresponding boxed type for field \'complexity\' or migrate using RealmObjectSchema.setNullable()."

    invoke-direct {v7, v8, v9}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v7

    .line 228
    :cond_c
    const-string v7, "duration"

    invoke-interface {v3, v7}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_d

    .line 229
    new-instance v7, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->getPath()Ljava/lang/String;

    move-result-object v8

    const-string v9, "Missing field \'duration\' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn()."

    invoke-direct {v7, v8, v9}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v7

    .line 231
    :cond_d
    const-string v7, "duration"

    invoke-interface {v3, v7}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    sget-object v8, Lio/realm/RealmFieldType;->INTEGER:Lio/realm/RealmFieldType;

    if-eq v7, v8, :cond_e

    .line 232
    new-instance v7, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->getPath()Ljava/lang/String;

    move-result-object v8

    const-string v9, "Invalid type \'long\' for field \'duration\' in existing Realm file."

    invoke-direct {v7, v8, v9}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v7

    .line 234
    :cond_e
    iget-wide v8, v2, Lio/realm/MathConfigRealmProxy$MathConfigColumnInfo;->durationIndex:J

    invoke-virtual {v6, v8, v9}, Lio/realm/internal/Table;->isColumnNullable(J)Z

    move-result v7

    if-eqz v7, :cond_10

    .line 235
    new-instance v7, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->getPath()Ljava/lang/String;

    move-result-object v8

    const-string v9, "Field \'duration\' does support null values in the existing Realm file. Use corresponding boxed type for field \'duration\' or migrate using RealmObjectSchema.setNullable()."

    invoke-direct {v7, v8, v9}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v7

    .line 239
    .end local v0    # "columnCount":J
    .end local v2    # "columnInfo":Lio/realm/MathConfigRealmProxy$MathConfigColumnInfo;
    .end local v3    # "columnTypes":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lio/realm/RealmFieldType;>;"
    .end local v4    # "i":J
    .end local v6    # "table":Lio/realm/internal/Table;
    :cond_f
    new-instance v7, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->getPath()Ljava/lang/String;

    move-result-object v8

    const-string v9, "The \'MathConfig\' class is missing from the schema for this Realm."

    invoke-direct {v7, v8, v9}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v7

    .line 237
    .restart local v0    # "columnCount":J
    .restart local v2    # "columnInfo":Lio/realm/MathConfigRealmProxy$MathConfigColumnInfo;
    .restart local v3    # "columnTypes":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lio/realm/RealmFieldType;>;"
    .restart local v4    # "i":J
    .restart local v6    # "table":Lio/realm/internal/Table;
    :cond_10
    return-object v2
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 12
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v5, 0x1

    const/4 v6, 0x0

    .line 572
    if-ne p0, p1, :cond_1

    .line 586
    :cond_0
    :goto_0
    return v5

    .line 573
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v7

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v8

    if-eq v7, v8, :cond_3

    :cond_2
    move v5, v6

    goto :goto_0

    :cond_3
    move-object v0, p1

    .line 574
    check-cast v0, Lio/realm/MathConfigRealmProxy;

    .line 576
    .local v0, "aMathConfig":Lio/realm/MathConfigRealmProxy;
    iget-object v7, p0, Lio/realm/MathConfigRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v7}, Lio/realm/ProxyState;->getRealm$realm()Lio/realm/BaseRealm;

    move-result-object v7

    invoke-virtual {v7}, Lio/realm/BaseRealm;->getPath()Ljava/lang/String;

    move-result-object v3

    .line 577
    .local v3, "path":Ljava/lang/String;
    iget-object v7, v0, Lio/realm/MathConfigRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v7}, Lio/realm/ProxyState;->getRealm$realm()Lio/realm/BaseRealm;

    move-result-object v7

    invoke-virtual {v7}, Lio/realm/BaseRealm;->getPath()Ljava/lang/String;

    move-result-object v1

    .line 578
    .local v1, "otherPath":Ljava/lang/String;
    if-eqz v3, :cond_5

    invoke-virtual {v3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_6

    :cond_4
    move v5, v6

    goto :goto_0

    :cond_5
    if-nez v1, :cond_4

    .line 580
    :cond_6
    iget-object v7, p0, Lio/realm/MathConfigRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v7}, Lio/realm/ProxyState;->getRow$realm()Lio/realm/internal/Row;

    move-result-object v7

    invoke-interface {v7}, Lio/realm/internal/Row;->getTable()Lio/realm/internal/Table;

    move-result-object v7

    invoke-virtual {v7}, Lio/realm/internal/Table;->getName()Ljava/lang/String;

    move-result-object v4

    .line 581
    .local v4, "tableName":Ljava/lang/String;
    iget-object v7, v0, Lio/realm/MathConfigRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v7}, Lio/realm/ProxyState;->getRow$realm()Lio/realm/internal/Row;

    move-result-object v7

    invoke-interface {v7}, Lio/realm/internal/Row;->getTable()Lio/realm/internal/Table;

    move-result-object v7

    invoke-virtual {v7}, Lio/realm/internal/Table;->getName()Ljava/lang/String;

    move-result-object v2

    .line 582
    .local v2, "otherTableName":Ljava/lang/String;
    if-eqz v4, :cond_8

    invoke-virtual {v4, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_9

    :cond_7
    move v5, v6

    goto :goto_0

    :cond_8
    if-nez v2, :cond_7

    .line 584
    :cond_9
    iget-object v7, p0, Lio/realm/MathConfigRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v7}, Lio/realm/ProxyState;->getRow$realm()Lio/realm/internal/Row;

    move-result-object v7

    invoke-interface {v7}, Lio/realm/internal/Row;->getIndex()J

    move-result-wide v8

    iget-object v7, v0, Lio/realm/MathConfigRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v7}, Lio/realm/ProxyState;->getRow$realm()Lio/realm/internal/Row;

    move-result-object v7

    invoke-interface {v7}, Lio/realm/internal/Row;->getIndex()J

    move-result-wide v10

    cmp-long v7, v8, v10

    if-eqz v7, :cond_0

    move v5, v6

    goto :goto_0
.end method

.method public hashCode()I
    .locals 8

    .prologue
    const/4 v5, 0x0

    .line 559
    iget-object v6, p0, Lio/realm/MathConfigRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v6}, Lio/realm/ProxyState;->getRealm$realm()Lio/realm/BaseRealm;

    move-result-object v6

    invoke-virtual {v6}, Lio/realm/BaseRealm;->getPath()Ljava/lang/String;

    move-result-object v0

    .line 560
    .local v0, "realmName":Ljava/lang/String;
    iget-object v6, p0, Lio/realm/MathConfigRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v6}, Lio/realm/ProxyState;->getRow$realm()Lio/realm/internal/Row;

    move-result-object v6

    invoke-interface {v6}, Lio/realm/internal/Row;->getTable()Lio/realm/internal/Table;

    move-result-object v6

    invoke-virtual {v6}, Lio/realm/internal/Table;->getName()Ljava/lang/String;

    move-result-object v4

    .line 561
    .local v4, "tableName":Ljava/lang/String;
    iget-object v6, p0, Lio/realm/MathConfigRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v6}, Lio/realm/ProxyState;->getRow$realm()Lio/realm/internal/Row;

    move-result-object v6

    invoke-interface {v6}, Lio/realm/internal/Row;->getIndex()J

    move-result-wide v2

    .line 563
    .local v2, "rowIndex":J
    const/16 v1, 0x11

    .line 564
    .local v1, "result":I
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v6

    :goto_0
    add-int/lit16 v1, v6, 0x20f

    .line 565
    mul-int/lit8 v6, v1, 0x1f

    if-eqz v4, :cond_0

    invoke-virtual {v4}, Ljava/lang/String;->hashCode()I

    move-result v5

    :cond_0
    add-int v1, v6, v5

    .line 566
    mul-int/lit8 v5, v1, 0x1f

    const/16 v6, 0x20

    ushr-long v6, v2, v6

    xor-long/2addr v6, v2

    long-to-int v6, v6

    add-int v1, v5, v6

    .line 567
    return v1

    :cond_1
    move v6, v5

    .line 564
    goto :goto_0
.end method

.method public realm$injectObjectContext()V
    .locals 3

    .prologue
    .line 86
    iget-object v1, p0, Lio/realm/MathConfigRealmProxy;->proxyState:Lio/realm/ProxyState;

    if-eqz v1, :cond_0

    .line 96
    :goto_0
    return-void

    .line 89
    :cond_0
    sget-object v1, Lio/realm/BaseRealm;->objectContext:Lio/realm/BaseRealm$ThreadLocalRealmObjectContext;

    invoke-virtual {v1}, Lio/realm/BaseRealm$ThreadLocalRealmObjectContext;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/realm/BaseRealm$RealmObjectContext;

    .line 90
    .local v0, "context":Lio/realm/BaseRealm$RealmObjectContext;
    invoke-virtual {v0}, Lio/realm/BaseRealm$RealmObjectContext;->getColumnInfo()Lio/realm/internal/ColumnInfo;

    move-result-object v1

    check-cast v1, Lio/realm/MathConfigRealmProxy$MathConfigColumnInfo;

    iput-object v1, p0, Lio/realm/MathConfigRealmProxy;->columnInfo:Lio/realm/MathConfigRealmProxy$MathConfigColumnInfo;

    .line 91
    new-instance v1, Lio/realm/ProxyState;

    invoke-direct {v1, p0}, Lio/realm/ProxyState;-><init>(Lio/realm/RealmModel;)V

    iput-object v1, p0, Lio/realm/MathConfigRealmProxy;->proxyState:Lio/realm/ProxyState;

    .line 92
    iget-object v1, p0, Lio/realm/MathConfigRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v0}, Lio/realm/BaseRealm$RealmObjectContext;->getRealm()Lio/realm/BaseRealm;

    move-result-object v2

    invoke-virtual {v1, v2}, Lio/realm/ProxyState;->setRealm$realm(Lio/realm/BaseRealm;)V

    .line 93
    iget-object v1, p0, Lio/realm/MathConfigRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v0}, Lio/realm/BaseRealm$RealmObjectContext;->getRow()Lio/realm/internal/Row;

    move-result-object v2

    invoke-virtual {v1, v2}, Lio/realm/ProxyState;->setRow$realm(Lio/realm/internal/Row;)V

    .line 94
    iget-object v1, p0, Lio/realm/MathConfigRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v0}, Lio/realm/BaseRealm$RealmObjectContext;->getAcceptDefaultValue()Z

    move-result v2

    invoke-virtual {v1, v2}, Lio/realm/ProxyState;->setAcceptDefaultValue$realm(Z)V

    .line 95
    iget-object v1, p0, Lio/realm/MathConfigRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v0}, Lio/realm/BaseRealm$RealmObjectContext;->getExcludeFields()Ljava/util/List;

    move-result-object v2

    invoke-virtual {v1, v2}, Lio/realm/ProxyState;->setExcludeFields$realm(Ljava/util/List;)V

    goto :goto_0
.end method

.method public realmGet$complexity()I
    .locals 4

    .prologue
    .line 116
    iget-object v0, p0, Lio/realm/MathConfigRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v0}, Lio/realm/ProxyState;->getRealm$realm()Lio/realm/BaseRealm;

    move-result-object v0

    invoke-virtual {v0}, Lio/realm/BaseRealm;->checkIfValid()V

    .line 117
    iget-object v0, p0, Lio/realm/MathConfigRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v0}, Lio/realm/ProxyState;->getRow$realm()Lio/realm/internal/Row;

    move-result-object v0

    iget-object v1, p0, Lio/realm/MathConfigRealmProxy;->columnInfo:Lio/realm/MathConfigRealmProxy$MathConfigColumnInfo;

    iget-wide v2, v1, Lio/realm/MathConfigRealmProxy$MathConfigColumnInfo;->complexityIndex:J

    invoke-interface {v0, v2, v3}, Lio/realm/internal/Row;->getLong(J)J

    move-result-wide v0

    long-to-int v0, v0

    return v0
.end method

.method public realmGet$duration()J
    .locals 4

    .prologue
    .line 136
    iget-object v0, p0, Lio/realm/MathConfigRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v0}, Lio/realm/ProxyState;->getRealm$realm()Lio/realm/BaseRealm;

    move-result-object v0

    invoke-virtual {v0}, Lio/realm/BaseRealm;->checkIfValid()V

    .line 137
    iget-object v0, p0, Lio/realm/MathConfigRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v0}, Lio/realm/ProxyState;->getRow$realm()Lio/realm/internal/Row;

    move-result-object v0

    iget-object v1, p0, Lio/realm/MathConfigRealmProxy;->columnInfo:Lio/realm/MathConfigRealmProxy$MathConfigColumnInfo;

    iget-wide v2, v1, Lio/realm/MathConfigRealmProxy$MathConfigColumnInfo;->durationIndex:J

    invoke-interface {v0, v2, v3}, Lio/realm/internal/Row;->getLong(J)J

    move-result-wide v0

    return-wide v0
.end method

.method public realmGet$id()I
    .locals 4

    .prologue
    .line 100
    iget-object v0, p0, Lio/realm/MathConfigRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v0}, Lio/realm/ProxyState;->getRealm$realm()Lio/realm/BaseRealm;

    move-result-object v0

    invoke-virtual {v0}, Lio/realm/BaseRealm;->checkIfValid()V

    .line 101
    iget-object v0, p0, Lio/realm/MathConfigRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v0}, Lio/realm/ProxyState;->getRow$realm()Lio/realm/internal/Row;

    move-result-object v0

    iget-object v1, p0, Lio/realm/MathConfigRealmProxy;->columnInfo:Lio/realm/MathConfigRealmProxy$MathConfigColumnInfo;

    iget-wide v2, v1, Lio/realm/MathConfigRealmProxy$MathConfigColumnInfo;->idIndex:J

    invoke-interface {v0, v2, v3}, Lio/realm/internal/Row;->getLong(J)J

    move-result-wide v0

    long-to-int v0, v0

    return v0
.end method

.method public realmGet$proxyState()Lio/realm/ProxyState;
    .locals 1

    .prologue
    .line 554
    iget-object v0, p0, Lio/realm/MathConfigRealmProxy;->proxyState:Lio/realm/ProxyState;

    return-object v0
.end method

.method public realmSet$complexity(I)V
    .locals 9
    .param p1, "value"    # I

    .prologue
    .line 121
    iget-object v1, p0, Lio/realm/MathConfigRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v1}, Lio/realm/ProxyState;->isUnderConstruction()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 122
    iget-object v1, p0, Lio/realm/MathConfigRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v1}, Lio/realm/ProxyState;->getAcceptDefaultValue$realm()Z

    move-result v1

    if-nez v1, :cond_0

    .line 132
    :goto_0
    return-void

    .line 125
    :cond_0
    iget-object v1, p0, Lio/realm/MathConfigRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v1}, Lio/realm/ProxyState;->getRow$realm()Lio/realm/internal/Row;

    move-result-object v0

    .line 126
    .local v0, "row":Lio/realm/internal/Row;
    invoke-interface {v0}, Lio/realm/internal/Row;->getTable()Lio/realm/internal/Table;

    move-result-object v1

    iget-object v2, p0, Lio/realm/MathConfigRealmProxy;->columnInfo:Lio/realm/MathConfigRealmProxy$MathConfigColumnInfo;

    iget-wide v2, v2, Lio/realm/MathConfigRealmProxy$MathConfigColumnInfo;->complexityIndex:J

    invoke-interface {v0}, Lio/realm/internal/Row;->getIndex()J

    move-result-wide v4

    int-to-long v6, p1

    const/4 v8, 0x1

    invoke-virtual/range {v1 .. v8}, Lio/realm/internal/Table;->setLong(JJJZ)V

    goto :goto_0

    .line 130
    .end local v0    # "row":Lio/realm/internal/Row;
    :cond_1
    iget-object v1, p0, Lio/realm/MathConfigRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v1}, Lio/realm/ProxyState;->getRealm$realm()Lio/realm/BaseRealm;

    move-result-object v1

    invoke-virtual {v1}, Lio/realm/BaseRealm;->checkIfValid()V

    .line 131
    iget-object v1, p0, Lio/realm/MathConfigRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v1}, Lio/realm/ProxyState;->getRow$realm()Lio/realm/internal/Row;

    move-result-object v1

    iget-object v2, p0, Lio/realm/MathConfigRealmProxy;->columnInfo:Lio/realm/MathConfigRealmProxy$MathConfigColumnInfo;

    iget-wide v2, v2, Lio/realm/MathConfigRealmProxy$MathConfigColumnInfo;->complexityIndex:J

    int-to-long v4, p1

    invoke-interface {v1, v2, v3, v4, v5}, Lio/realm/internal/Row;->setLong(JJ)V

    goto :goto_0
.end method

.method public realmSet$duration(J)V
    .locals 9
    .param p1, "value"    # J

    .prologue
    .line 141
    iget-object v1, p0, Lio/realm/MathConfigRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v1}, Lio/realm/ProxyState;->isUnderConstruction()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 142
    iget-object v1, p0, Lio/realm/MathConfigRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v1}, Lio/realm/ProxyState;->getAcceptDefaultValue$realm()Z

    move-result v1

    if-nez v1, :cond_0

    .line 152
    :goto_0
    return-void

    .line 145
    :cond_0
    iget-object v1, p0, Lio/realm/MathConfigRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v1}, Lio/realm/ProxyState;->getRow$realm()Lio/realm/internal/Row;

    move-result-object v0

    .line 146
    .local v0, "row":Lio/realm/internal/Row;
    invoke-interface {v0}, Lio/realm/internal/Row;->getTable()Lio/realm/internal/Table;

    move-result-object v1

    iget-object v2, p0, Lio/realm/MathConfigRealmProxy;->columnInfo:Lio/realm/MathConfigRealmProxy$MathConfigColumnInfo;

    iget-wide v2, v2, Lio/realm/MathConfigRealmProxy$MathConfigColumnInfo;->durationIndex:J

    invoke-interface {v0}, Lio/realm/internal/Row;->getIndex()J

    move-result-wide v4

    const/4 v8, 0x1

    move-wide v6, p1

    invoke-virtual/range {v1 .. v8}, Lio/realm/internal/Table;->setLong(JJJZ)V

    goto :goto_0

    .line 150
    .end local v0    # "row":Lio/realm/internal/Row;
    :cond_1
    iget-object v1, p0, Lio/realm/MathConfigRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v1}, Lio/realm/ProxyState;->getRealm$realm()Lio/realm/BaseRealm;

    move-result-object v1

    invoke-virtual {v1}, Lio/realm/BaseRealm;->checkIfValid()V

    .line 151
    iget-object v1, p0, Lio/realm/MathConfigRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v1}, Lio/realm/ProxyState;->getRow$realm()Lio/realm/internal/Row;

    move-result-object v1

    iget-object v2, p0, Lio/realm/MathConfigRealmProxy;->columnInfo:Lio/realm/MathConfigRealmProxy$MathConfigColumnInfo;

    iget-wide v2, v2, Lio/realm/MathConfigRealmProxy$MathConfigColumnInfo;->durationIndex:J

    invoke-interface {v1, v2, v3, p1, p2}, Lio/realm/internal/Row;->setLong(JJ)V

    goto :goto_0
.end method

.method public realmSet$id(I)V
    .locals 2
    .param p1, "value"    # I

    .prologue
    .line 105
    iget-object v0, p0, Lio/realm/MathConfigRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v0}, Lio/realm/ProxyState;->isUnderConstruction()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 107
    return-void

    .line 110
    :cond_0
    iget-object v0, p0, Lio/realm/MathConfigRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v0}, Lio/realm/ProxyState;->getRealm$realm()Lio/realm/BaseRealm;

    move-result-object v0

    invoke-virtual {v0}, Lio/realm/BaseRealm;->checkIfValid()V

    .line 111
    new-instance v0, Lio/realm/exceptions/RealmException;

    const-string v1, "Primary key field \'id\' cannot be changed after object was created."

    invoke-direct {v0, v1}, Lio/realm/exceptions/RealmException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 533
    invoke-static {p0}, Lio/realm/RealmObject;->isValid(Lio/realm/RealmModel;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 534
    const-string v1, "Invalid object"

    .line 549
    :goto_0
    return-object v1

    .line 536
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "MathConfig = ["

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 537
    .local v0, "stringBuilder":Ljava/lang/StringBuilder;
    const-string v1, "{id:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 538
    invoke-virtual {p0}, Lio/realm/MathConfigRealmProxy;->realmGet$id()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 539
    const-string v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 540
    const-string v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 541
    const-string v1, "{complexity:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 542
    invoke-virtual {p0}, Lio/realm/MathConfigRealmProxy;->realmGet$complexity()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 543
    const-string v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 544
    const-string v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 545
    const-string v1, "{duration:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 546
    invoke-virtual {p0}, Lio/realm/MathConfigRealmProxy;->realmGet$duration()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 547
    const-string v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 548
    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 549
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method
