.class public interface abstract Lio/realm/EvenNumbersConfigRealmProxyInterface;
.super Ljava/lang/Object;
.source "EvenNumbersConfigRealmProxyInterface.java"


# virtual methods
.method public abstract realmGet$columnCount()I
.end method

.method public abstract realmGet$digitsPerNumber()I
.end method

.method public abstract realmGet$evenNumberCount()I
.end method

.method public abstract realmGet$id()I
.end method

.method public abstract realmGet$rowCount()I
.end method

.method public abstract realmGet$trainingDuration()I
.end method

.method public abstract realmSet$columnCount(I)V
.end method

.method public abstract realmSet$digitsPerNumber(I)V
.end method

.method public abstract realmSet$evenNumberCount(I)V
.end method

.method public abstract realmSet$id(I)V
.end method

.method public abstract realmSet$rowCount(I)V
.end method

.method public abstract realmSet$trainingDuration(I)V
.end method
