.class public Lio/realm/TrueColorsConfigRealmProxy;
.super Lcom/safonov/speedreading/training/fragment/truecolors/repository/entity/TrueColorsConfig;
.source "TrueColorsConfigRealmProxy.java"

# interfaces
.implements Lio/realm/internal/RealmObjectProxy;
.implements Lio/realm/TrueColorsConfigRealmProxyInterface;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lio/realm/TrueColorsConfigRealmProxy$TrueColorsConfigColumnInfo;
    }
.end annotation


# static fields
.field private static final FIELD_NAMES:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private columnInfo:Lio/realm/TrueColorsConfigRealmProxy$TrueColorsConfigColumnInfo;

.field private proxyState:Lio/realm/ProxyState;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/realm/ProxyState",
            "<",
            "Lcom/safonov/speedreading/training/fragment/truecolors/repository/entity/TrueColorsConfig;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 69
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 70
    .local v0, "fieldNames":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const-string v1, "id"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 71
    const-string v1, "showTime"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 72
    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    sput-object v1, Lio/realm/TrueColorsConfigRealmProxy;->FIELD_NAMES:Ljava/util/List;

    .line 73
    return-void
.end method

.method constructor <init>()V
    .locals 1

    .prologue
    .line 75
    invoke-direct {p0}, Lcom/safonov/speedreading/training/fragment/truecolors/repository/entity/TrueColorsConfig;-><init>()V

    .line 76
    iget-object v0, p0, Lio/realm/TrueColorsConfigRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v0}, Lio/realm/ProxyState;->setConstructionFinished()V

    .line 77
    return-void
.end method

.method public static copy(Lio/realm/Realm;Lcom/safonov/speedreading/training/fragment/truecolors/repository/entity/TrueColorsConfig;ZLjava/util/Map;)Lcom/safonov/speedreading/training/fragment/truecolors/repository/entity/TrueColorsConfig;
    .locals 6
    .param p0, "realm"    # Lio/realm/Realm;
    .param p1, "newObject"    # Lcom/safonov/speedreading/training/fragment/truecolors/repository/entity/TrueColorsConfig;
    .param p2, "update"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/realm/Realm;",
            "Lcom/safonov/speedreading/training/fragment/truecolors/repository/entity/TrueColorsConfig;",
            "Z",
            "Ljava/util/Map",
            "<",
            "Lio/realm/RealmModel;",
            "Lio/realm/internal/RealmObjectProxy;",
            ">;)",
            "Lcom/safonov/speedreading/training/fragment/truecolors/repository/entity/TrueColorsConfig;"
        }
    .end annotation

    .prologue
    .line 334
    .local p3, "cache":Ljava/util/Map;, "Ljava/util/Map<Lio/realm/RealmModel;Lio/realm/internal/RealmObjectProxy;>;"
    invoke-interface {p3, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/realm/internal/RealmObjectProxy;

    .line 335
    .local v0, "cachedRealmObject":Lio/realm/internal/RealmObjectProxy;
    if-eqz v0, :cond_0

    .line 336
    check-cast v0, Lcom/safonov/speedreading/training/fragment/truecolors/repository/entity/TrueColorsConfig;

    .line 342
    .end local v0    # "cachedRealmObject":Lio/realm/internal/RealmObjectProxy;
    .end local p1    # "newObject":Lcom/safonov/speedreading/training/fragment/truecolors/repository/entity/TrueColorsConfig;
    :goto_0
    return-object v0

    .line 339
    .restart local v0    # "cachedRealmObject":Lio/realm/internal/RealmObjectProxy;
    .restart local p1    # "newObject":Lcom/safonov/speedreading/training/fragment/truecolors/repository/entity/TrueColorsConfig;
    :cond_0
    const-class v3, Lcom/safonov/speedreading/training/fragment/truecolors/repository/entity/TrueColorsConfig;

    move-object v2, p1

    check-cast v2, Lio/realm/TrueColorsConfigRealmProxyInterface;

    invoke-interface {v2}, Lio/realm/TrueColorsConfigRealmProxyInterface;->realmGet$id()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v4, 0x0

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v5

    invoke-virtual {p0, v3, v2, v4, v5}, Lio/realm/Realm;->createObjectInternal(Ljava/lang/Class;Ljava/lang/Object;ZLjava/util/List;)Lio/realm/RealmModel;

    move-result-object v1

    check-cast v1, Lcom/safonov/speedreading/training/fragment/truecolors/repository/entity/TrueColorsConfig;

    .local v1, "realmObject":Lcom/safonov/speedreading/training/fragment/truecolors/repository/entity/TrueColorsConfig;
    move-object v2, v1

    .line 340
    check-cast v2, Lio/realm/internal/RealmObjectProxy;

    invoke-interface {p3, p1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-object v2, v1

    .line 341
    check-cast v2, Lio/realm/TrueColorsConfigRealmProxyInterface;

    check-cast p1, Lio/realm/TrueColorsConfigRealmProxyInterface;

    .end local p1    # "newObject":Lcom/safonov/speedreading/training/fragment/truecolors/repository/entity/TrueColorsConfig;
    invoke-interface {p1}, Lio/realm/TrueColorsConfigRealmProxyInterface;->realmGet$showTime()I

    move-result v3

    invoke-interface {v2, v3}, Lio/realm/TrueColorsConfigRealmProxyInterface;->realmSet$showTime(I)V

    move-object v0, v1

    .line 342
    goto :goto_0
.end method

.method public static copyOrUpdate(Lio/realm/Realm;Lcom/safonov/speedreading/training/fragment/truecolors/repository/entity/TrueColorsConfig;ZLjava/util/Map;)Lcom/safonov/speedreading/training/fragment/truecolors/repository/entity/TrueColorsConfig;
    .locals 19
    .param p0, "realm"    # Lio/realm/Realm;
    .param p1, "object"    # Lcom/safonov/speedreading/training/fragment/truecolors/repository/entity/TrueColorsConfig;
    .param p2, "update"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/realm/Realm;",
            "Lcom/safonov/speedreading/training/fragment/truecolors/repository/entity/TrueColorsConfig;",
            "Z",
            "Ljava/util/Map",
            "<",
            "Lio/realm/RealmModel;",
            "Lio/realm/internal/RealmObjectProxy;",
            ">;)",
            "Lcom/safonov/speedreading/training/fragment/truecolors/repository/entity/TrueColorsConfig;"
        }
    .end annotation

    .prologue
    .line 295
    .local p3, "cache":Ljava/util/Map;, "Ljava/util/Map<Lio/realm/RealmModel;Lio/realm/internal/RealmObjectProxy;>;"
    move-object/from16 v0, p1

    instance-of v5, v0, Lio/realm/internal/RealmObjectProxy;

    if-eqz v5, :cond_0

    move-object/from16 v5, p1

    check-cast v5, Lio/realm/internal/RealmObjectProxy;

    invoke-interface {v5}, Lio/realm/internal/RealmObjectProxy;->realmGet$proxyState()Lio/realm/ProxyState;

    move-result-object v5

    invoke-virtual {v5}, Lio/realm/ProxyState;->getRealm$realm()Lio/realm/BaseRealm;

    move-result-object v5

    if-eqz v5, :cond_0

    move-object/from16 v5, p1

    check-cast v5, Lio/realm/internal/RealmObjectProxy;

    invoke-interface {v5}, Lio/realm/internal/RealmObjectProxy;->realmGet$proxyState()Lio/realm/ProxyState;

    move-result-object v5

    invoke-virtual {v5}, Lio/realm/ProxyState;->getRealm$realm()Lio/realm/BaseRealm;

    move-result-object v5

    iget-wide v6, v5, Lio/realm/BaseRealm;->threadId:J

    move-object/from16 v0, p0

    iget-wide v8, v0, Lio/realm/Realm;->threadId:J

    cmp-long v5, v6, v8

    if-eqz v5, :cond_0

    .line 296
    new-instance v5, Ljava/lang/IllegalArgumentException;

    const-string v6, "Objects which belong to Realm instances in other threads cannot be copied into this Realm instance."

    invoke-direct {v5, v6}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v5

    .line 298
    :cond_0
    move-object/from16 v0, p1

    instance-of v5, v0, Lio/realm/internal/RealmObjectProxy;

    if-eqz v5, :cond_1

    move-object/from16 v5, p1

    check-cast v5, Lio/realm/internal/RealmObjectProxy;

    invoke-interface {v5}, Lio/realm/internal/RealmObjectProxy;->realmGet$proxyState()Lio/realm/ProxyState;

    move-result-object v5

    invoke-virtual {v5}, Lio/realm/ProxyState;->getRealm$realm()Lio/realm/BaseRealm;

    move-result-object v5

    if-eqz v5, :cond_1

    move-object/from16 v5, p1

    check-cast v5, Lio/realm/internal/RealmObjectProxy;

    invoke-interface {v5}, Lio/realm/internal/RealmObjectProxy;->realmGet$proxyState()Lio/realm/ProxyState;

    move-result-object v5

    invoke-virtual {v5}, Lio/realm/ProxyState;->getRealm$realm()Lio/realm/BaseRealm;

    move-result-object v5

    invoke-virtual {v5}, Lio/realm/BaseRealm;->getPath()Ljava/lang/String;

    move-result-object v5

    invoke-virtual/range {p0 .. p0}, Lio/realm/Realm;->getPath()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 328
    .end local p1    # "object":Lcom/safonov/speedreading/training/fragment/truecolors/repository/entity/TrueColorsConfig;
    :goto_0
    return-object p1

    .line 301
    .restart local p1    # "object":Lcom/safonov/speedreading/training/fragment/truecolors/repository/entity/TrueColorsConfig;
    :cond_1
    sget-object v5, Lio/realm/BaseRealm;->objectContext:Lio/realm/BaseRealm$ThreadLocalRealmObjectContext;

    invoke-virtual {v5}, Lio/realm/BaseRealm$ThreadLocalRealmObjectContext;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lio/realm/BaseRealm$RealmObjectContext;

    .line 302
    .local v4, "objectContext":Lio/realm/BaseRealm$RealmObjectContext;
    move-object/from16 v0, p3

    move-object/from16 v1, p1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lio/realm/internal/RealmObjectProxy;

    .line 303
    .local v10, "cachedRealmObject":Lio/realm/internal/RealmObjectProxy;
    if-eqz v10, :cond_2

    .line 304
    check-cast v10, Lcom/safonov/speedreading/training/fragment/truecolors/repository/entity/TrueColorsConfig;

    .end local v10    # "cachedRealmObject":Lio/realm/internal/RealmObjectProxy;
    move-object/from16 p1, v10

    goto :goto_0

    .line 306
    .restart local v10    # "cachedRealmObject":Lio/realm/internal/RealmObjectProxy;
    :cond_2
    const/4 v14, 0x0

    .line 307
    .local v14, "realmObject":Lcom/safonov/speedreading/training/fragment/truecolors/repository/entity/TrueColorsConfig;
    move/from16 v11, p2

    .line 308
    .local v11, "canUpdate":Z
    if-eqz v11, :cond_3

    .line 309
    const-class v5, Lcom/safonov/speedreading/training/fragment/truecolors/repository/entity/TrueColorsConfig;

    move-object/from16 v0, p0

    invoke-virtual {v0, v5}, Lio/realm/Realm;->getTable(Ljava/lang/Class;)Lio/realm/internal/Table;

    move-result-object v18

    .line 310
    .local v18, "table":Lio/realm/internal/Table;
    invoke-virtual/range {v18 .. v18}, Lio/realm/internal/Table;->getPrimaryKey()J

    move-result-wide v12

    .local v12, "pkColumnIndex":J
    move-object/from16 v5, p1

    .line 311
    check-cast v5, Lio/realm/TrueColorsConfigRealmProxyInterface;

    invoke-interface {v5}, Lio/realm/TrueColorsConfigRealmProxyInterface;->realmGet$id()I

    move-result v5

    int-to-long v6, v5

    move-object/from16 v0, v18

    invoke-virtual {v0, v12, v13, v6, v7}, Lio/realm/internal/Table;->findFirstLong(JJ)J

    move-result-wide v16

    .line 312
    .local v16, "rowIndex":J
    const-wide/16 v6, -0x1

    cmp-long v5, v16, v6

    if-eqz v5, :cond_4

    .line 314
    :try_start_0
    move-object/from16 v0, v18

    move-wide/from16 v1, v16

    invoke-virtual {v0, v1, v2}, Lio/realm/internal/Table;->getUncheckedRow(J)Lio/realm/internal/UncheckedRow;

    move-result-object v6

    move-object/from16 v0, p0

    iget-object v5, v0, Lio/realm/Realm;->schema:Lio/realm/RealmSchema;

    const-class v7, Lcom/safonov/speedreading/training/fragment/truecolors/repository/entity/TrueColorsConfig;

    invoke-virtual {v5, v7}, Lio/realm/RealmSchema;->getColumnInfo(Ljava/lang/Class;)Lio/realm/internal/ColumnInfo;

    move-result-object v7

    const/4 v8, 0x0

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v9

    move-object/from16 v5, p0

    invoke-virtual/range {v4 .. v9}, Lio/realm/BaseRealm$RealmObjectContext;->set(Lio/realm/BaseRealm;Lio/realm/internal/Row;Lio/realm/internal/ColumnInfo;ZLjava/util/List;)V

    .line 315
    new-instance v15, Lio/realm/TrueColorsConfigRealmProxy;

    invoke-direct {v15}, Lio/realm/TrueColorsConfigRealmProxy;-><init>()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 316
    .end local v14    # "realmObject":Lcom/safonov/speedreading/training/fragment/truecolors/repository/entity/TrueColorsConfig;
    .local v15, "realmObject":Lcom/safonov/speedreading/training/fragment/truecolors/repository/entity/TrueColorsConfig;
    :try_start_1
    move-object v0, v15

    check-cast v0, Lio/realm/internal/RealmObjectProxy;

    move-object v5, v0

    move-object/from16 v0, p3

    move-object/from16 v1, p1

    invoke-interface {v0, v1, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 318
    invoke-virtual {v4}, Lio/realm/BaseRealm$RealmObjectContext;->clear()V

    move-object v14, v15

    .line 325
    .end local v12    # "pkColumnIndex":J
    .end local v15    # "realmObject":Lcom/safonov/speedreading/training/fragment/truecolors/repository/entity/TrueColorsConfig;
    .end local v16    # "rowIndex":J
    .end local v18    # "table":Lio/realm/internal/Table;
    .restart local v14    # "realmObject":Lcom/safonov/speedreading/training/fragment/truecolors/repository/entity/TrueColorsConfig;
    :cond_3
    :goto_1
    if-eqz v11, :cond_5

    .line 326
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p3

    invoke-static {v0, v14, v1, v2}, Lio/realm/TrueColorsConfigRealmProxy;->update(Lio/realm/Realm;Lcom/safonov/speedreading/training/fragment/truecolors/repository/entity/TrueColorsConfig;Lcom/safonov/speedreading/training/fragment/truecolors/repository/entity/TrueColorsConfig;Ljava/util/Map;)Lcom/safonov/speedreading/training/fragment/truecolors/repository/entity/TrueColorsConfig;

    move-result-object p1

    goto :goto_0

    .line 318
    .restart local v12    # "pkColumnIndex":J
    .restart local v16    # "rowIndex":J
    .restart local v18    # "table":Lio/realm/internal/Table;
    :catchall_0
    move-exception v5

    :goto_2
    invoke-virtual {v4}, Lio/realm/BaseRealm$RealmObjectContext;->clear()V

    throw v5

    .line 321
    :cond_4
    const/4 v11, 0x0

    goto :goto_1

    .line 328
    .end local v12    # "pkColumnIndex":J
    .end local v16    # "rowIndex":J
    .end local v18    # "table":Lio/realm/internal/Table;
    :cond_5
    invoke-static/range {p0 .. p3}, Lio/realm/TrueColorsConfigRealmProxy;->copy(Lio/realm/Realm;Lcom/safonov/speedreading/training/fragment/truecolors/repository/entity/TrueColorsConfig;ZLjava/util/Map;)Lcom/safonov/speedreading/training/fragment/truecolors/repository/entity/TrueColorsConfig;

    move-result-object p1

    goto/16 :goto_0

    .line 318
    .end local v14    # "realmObject":Lcom/safonov/speedreading/training/fragment/truecolors/repository/entity/TrueColorsConfig;
    .restart local v12    # "pkColumnIndex":J
    .restart local v15    # "realmObject":Lcom/safonov/speedreading/training/fragment/truecolors/repository/entity/TrueColorsConfig;
    .restart local v16    # "rowIndex":J
    .restart local v18    # "table":Lio/realm/internal/Table;
    :catchall_1
    move-exception v5

    move-object v14, v15

    .end local v15    # "realmObject":Lcom/safonov/speedreading/training/fragment/truecolors/repository/entity/TrueColorsConfig;
    .restart local v14    # "realmObject":Lcom/safonov/speedreading/training/fragment/truecolors/repository/entity/TrueColorsConfig;
    goto :goto_2
.end method

.method public static createDetachedCopy(Lcom/safonov/speedreading/training/fragment/truecolors/repository/entity/TrueColorsConfig;IILjava/util/Map;)Lcom/safonov/speedreading/training/fragment/truecolors/repository/entity/TrueColorsConfig;
    .locals 4
    .param p0, "realmObject"    # Lcom/safonov/speedreading/training/fragment/truecolors/repository/entity/TrueColorsConfig;
    .param p1, "currentDepth"    # I
    .param p2, "maxDepth"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/safonov/speedreading/training/fragment/truecolors/repository/entity/TrueColorsConfig;",
            "II",
            "Ljava/util/Map",
            "<",
            "Lio/realm/RealmModel;",
            "Lio/realm/internal/RealmObjectProxy$CacheData",
            "<",
            "Lio/realm/RealmModel;",
            ">;>;)",
            "Lcom/safonov/speedreading/training/fragment/truecolors/repository/entity/TrueColorsConfig;"
        }
    .end annotation

    .prologue
    .line 447
    .local p3, "cache":Ljava/util/Map;, "Ljava/util/Map<Lio/realm/RealmModel;Lio/realm/internal/RealmObjectProxy$CacheData<Lio/realm/RealmModel;>;>;"
    if-gt p1, p2, :cond_0

    if-nez p0, :cond_1

    .line 448
    :cond_0
    const/4 v2, 0x0

    .line 466
    .end local p0    # "realmObject":Lcom/safonov/speedreading/training/fragment/truecolors/repository/entity/TrueColorsConfig;
    :goto_0
    return-object v2

    .line 450
    .restart local p0    # "realmObject":Lcom/safonov/speedreading/training/fragment/truecolors/repository/entity/TrueColorsConfig;
    :cond_1
    invoke-interface {p3, p0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/realm/internal/RealmObjectProxy$CacheData;

    .line 452
    .local v0, "cachedObject":Lio/realm/internal/RealmObjectProxy$CacheData;, "Lio/realm/internal/RealmObjectProxy$CacheData<Lio/realm/RealmModel;>;"
    if-eqz v0, :cond_3

    .line 454
    iget v2, v0, Lio/realm/internal/RealmObjectProxy$CacheData;->minDepth:I

    if-lt p1, v2, :cond_2

    .line 455
    iget-object v2, v0, Lio/realm/internal/RealmObjectProxy$CacheData;->object:Lio/realm/RealmModel;

    check-cast v2, Lcom/safonov/speedreading/training/fragment/truecolors/repository/entity/TrueColorsConfig;

    goto :goto_0

    .line 457
    :cond_2
    iget-object v1, v0, Lio/realm/internal/RealmObjectProxy$CacheData;->object:Lio/realm/RealmModel;

    check-cast v1, Lcom/safonov/speedreading/training/fragment/truecolors/repository/entity/TrueColorsConfig;

    .line 458
    .local v1, "unmanagedObject":Lcom/safonov/speedreading/training/fragment/truecolors/repository/entity/TrueColorsConfig;
    iput p1, v0, Lio/realm/internal/RealmObjectProxy$CacheData;->minDepth:I

    :goto_1
    move-object v2, v1

    .line 464
    check-cast v2, Lio/realm/TrueColorsConfigRealmProxyInterface;

    move-object v3, p0

    check-cast v3, Lio/realm/TrueColorsConfigRealmProxyInterface;

    invoke-interface {v3}, Lio/realm/TrueColorsConfigRealmProxyInterface;->realmGet$id()I

    move-result v3

    invoke-interface {v2, v3}, Lio/realm/TrueColorsConfigRealmProxyInterface;->realmSet$id(I)V

    move-object v2, v1

    .line 465
    check-cast v2, Lio/realm/TrueColorsConfigRealmProxyInterface;

    check-cast p0, Lio/realm/TrueColorsConfigRealmProxyInterface;

    .end local p0    # "realmObject":Lcom/safonov/speedreading/training/fragment/truecolors/repository/entity/TrueColorsConfig;
    invoke-interface {p0}, Lio/realm/TrueColorsConfigRealmProxyInterface;->realmGet$showTime()I

    move-result v3

    invoke-interface {v2, v3}, Lio/realm/TrueColorsConfigRealmProxyInterface;->realmSet$showTime(I)V

    move-object v2, v1

    .line 466
    goto :goto_0

    .line 461
    .end local v1    # "unmanagedObject":Lcom/safonov/speedreading/training/fragment/truecolors/repository/entity/TrueColorsConfig;
    .restart local p0    # "realmObject":Lcom/safonov/speedreading/training/fragment/truecolors/repository/entity/TrueColorsConfig;
    :cond_3
    new-instance v1, Lcom/safonov/speedreading/training/fragment/truecolors/repository/entity/TrueColorsConfig;

    invoke-direct {v1}, Lcom/safonov/speedreading/training/fragment/truecolors/repository/entity/TrueColorsConfig;-><init>()V

    .line 462
    .restart local v1    # "unmanagedObject":Lcom/safonov/speedreading/training/fragment/truecolors/repository/entity/TrueColorsConfig;
    new-instance v2, Lio/realm/internal/RealmObjectProxy$CacheData;

    invoke-direct {v2, p1, v1}, Lio/realm/internal/RealmObjectProxy$CacheData;-><init>(ILio/realm/RealmModel;)V

    invoke-interface {p3, p0, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1
.end method

.method public static createOrUpdateUsingJsonObject(Lio/realm/Realm;Lorg/json/JSONObject;Z)Lcom/safonov/speedreading/training/fragment/truecolors/repository/entity/TrueColorsConfig;
    .locals 13
    .param p0, "realm"    # Lio/realm/Realm;
    .param p1, "json"    # Lorg/json/JSONObject;
    .param p2, "update"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .prologue
    .line 218
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v6

    .line 219
    .local v6, "excludeFields":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const/4 v7, 0x0

    .line 220
    .local v7, "obj":Lcom/safonov/speedreading/training/fragment/truecolors/repository/entity/TrueColorsConfig;
    if-eqz p2, :cond_1

    .line 221
    const-class v1, Lcom/safonov/speedreading/training/fragment/truecolors/repository/entity/TrueColorsConfig;

    invoke-virtual {p0, v1}, Lio/realm/Realm;->getTable(Ljava/lang/Class;)Lio/realm/internal/Table;

    move-result-object v12

    .line 222
    .local v12, "table":Lio/realm/internal/Table;
    invoke-virtual {v12}, Lio/realm/internal/Table;->getPrimaryKey()J

    move-result-wide v8

    .line 223
    .local v8, "pkColumnIndex":J
    const-wide/16 v10, -0x1

    .line 224
    .local v10, "rowIndex":J
    const-string v1, "id"

    invoke-virtual {p1, v1}, Lorg/json/JSONObject;->isNull(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 225
    const-string v1, "id"

    invoke-virtual {p1, v1}, Lorg/json/JSONObject;->getLong(Ljava/lang/String;)J

    move-result-wide v2

    invoke-virtual {v12, v8, v9, v2, v3}, Lio/realm/internal/Table;->findFirstLong(JJ)J

    move-result-wide v10

    .line 227
    :cond_0
    const-wide/16 v2, -0x1

    cmp-long v1, v10, v2

    if-eqz v1, :cond_1

    .line 228
    sget-object v1, Lio/realm/BaseRealm;->objectContext:Lio/realm/BaseRealm$ThreadLocalRealmObjectContext;

    invoke-virtual {v1}, Lio/realm/BaseRealm$ThreadLocalRealmObjectContext;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/realm/BaseRealm$RealmObjectContext;

    .line 230
    .local v0, "objectContext":Lio/realm/BaseRealm$RealmObjectContext;
    :try_start_0
    invoke-virtual {v12, v10, v11}, Lio/realm/internal/Table;->getUncheckedRow(J)Lio/realm/internal/UncheckedRow;

    move-result-object v2

    iget-object v1, p0, Lio/realm/Realm;->schema:Lio/realm/RealmSchema;

    const-class v3, Lcom/safonov/speedreading/training/fragment/truecolors/repository/entity/TrueColorsConfig;

    invoke-virtual {v1, v3}, Lio/realm/RealmSchema;->getColumnInfo(Ljava/lang/Class;)Lio/realm/internal/ColumnInfo;

    move-result-object v3

    const/4 v4, 0x0

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v5

    move-object v1, p0

    invoke-virtual/range {v0 .. v5}, Lio/realm/BaseRealm$RealmObjectContext;->set(Lio/realm/BaseRealm;Lio/realm/internal/Row;Lio/realm/internal/ColumnInfo;ZLjava/util/List;)V

    .line 231
    new-instance v7, Lio/realm/TrueColorsConfigRealmProxy;

    .end local v7    # "obj":Lcom/safonov/speedreading/training/fragment/truecolors/repository/entity/TrueColorsConfig;
    invoke-direct {v7}, Lio/realm/TrueColorsConfigRealmProxy;-><init>()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 233
    .restart local v7    # "obj":Lcom/safonov/speedreading/training/fragment/truecolors/repository/entity/TrueColorsConfig;
    invoke-virtual {v0}, Lio/realm/BaseRealm$RealmObjectContext;->clear()V

    .line 237
    .end local v0    # "objectContext":Lio/realm/BaseRealm$RealmObjectContext;
    .end local v8    # "pkColumnIndex":J
    .end local v10    # "rowIndex":J
    .end local v12    # "table":Lio/realm/internal/Table;
    :cond_1
    if-nez v7, :cond_2

    .line 238
    const-string v1, "id"

    invoke-virtual {p1, v1}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 239
    const-string v1, "id"

    invoke-virtual {p1, v1}, Lorg/json/JSONObject;->isNull(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 240
    const-class v1, Lcom/safonov/speedreading/training/fragment/truecolors/repository/entity/TrueColorsConfig;

    const/4 v2, 0x0

    const/4 v3, 0x1

    invoke-virtual {p0, v1, v2, v3, v6}, Lio/realm/Realm;->createObjectInternal(Ljava/lang/Class;Ljava/lang/Object;ZLjava/util/List;)Lio/realm/RealmModel;

    move-result-object v7

    .end local v7    # "obj":Lcom/safonov/speedreading/training/fragment/truecolors/repository/entity/TrueColorsConfig;
    check-cast v7, Lio/realm/TrueColorsConfigRealmProxy;

    .line 248
    .restart local v7    # "obj":Lcom/safonov/speedreading/training/fragment/truecolors/repository/entity/TrueColorsConfig;
    :cond_2
    :goto_0
    const-string v1, "showTime"

    invoke-virtual {p1, v1}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 249
    const-string v1, "showTime"

    invoke-virtual {p1, v1}, Lorg/json/JSONObject;->isNull(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 250
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "Trying to set non-nullable field \'showTime\' to null."

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 233
    .end local v7    # "obj":Lcom/safonov/speedreading/training/fragment/truecolors/repository/entity/TrueColorsConfig;
    .restart local v0    # "objectContext":Lio/realm/BaseRealm$RealmObjectContext;
    .restart local v8    # "pkColumnIndex":J
    .restart local v10    # "rowIndex":J
    .restart local v12    # "table":Lio/realm/internal/Table;
    :catchall_0
    move-exception v1

    invoke-virtual {v0}, Lio/realm/BaseRealm$RealmObjectContext;->clear()V

    throw v1

    .line 242
    .end local v0    # "objectContext":Lio/realm/BaseRealm$RealmObjectContext;
    .end local v8    # "pkColumnIndex":J
    .end local v10    # "rowIndex":J
    .end local v12    # "table":Lio/realm/internal/Table;
    .restart local v7    # "obj":Lcom/safonov/speedreading/training/fragment/truecolors/repository/entity/TrueColorsConfig;
    :cond_3
    const-class v1, Lcom/safonov/speedreading/training/fragment/truecolors/repository/entity/TrueColorsConfig;

    const-string v2, "id"

    invoke-virtual {p1, v2}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v3, 0x1

    invoke-virtual {p0, v1, v2, v3, v6}, Lio/realm/Realm;->createObjectInternal(Ljava/lang/Class;Ljava/lang/Object;ZLjava/util/List;)Lio/realm/RealmModel;

    move-result-object v7

    .end local v7    # "obj":Lcom/safonov/speedreading/training/fragment/truecolors/repository/entity/TrueColorsConfig;
    check-cast v7, Lio/realm/TrueColorsConfigRealmProxy;

    .restart local v7    # "obj":Lcom/safonov/speedreading/training/fragment/truecolors/repository/entity/TrueColorsConfig;
    goto :goto_0

    .line 245
    :cond_4
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "JSON object doesn\'t have the primary key field \'id\'."

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_5
    move-object v1, v7

    .line 252
    check-cast v1, Lio/realm/TrueColorsConfigRealmProxyInterface;

    const-string v2, "showTime"

    invoke-virtual {p1, v2}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v1, v2}, Lio/realm/TrueColorsConfigRealmProxyInterface;->realmSet$showTime(I)V

    .line 255
    :cond_6
    return-object v7
.end method

.method public static createRealmObjectSchema(Lio/realm/RealmSchema;)Lio/realm/RealmObjectSchema;
    .locals 11
    .param p0, "realmSchema"    # Lio/realm/RealmSchema;

    .prologue
    const/4 v7, 0x0

    const/4 v3, 0x1

    .line 130
    const-string v0, "TrueColorsConfig"

    invoke-virtual {p0, v0}, Lio/realm/RealmSchema;->contains(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 131
    const-string v0, "TrueColorsConfig"

    invoke-virtual {p0, v0}, Lio/realm/RealmSchema;->create(Ljava/lang/String;)Lio/realm/RealmObjectSchema;

    move-result-object v10

    .line 132
    .local v10, "realmObjectSchema":Lio/realm/RealmObjectSchema;
    new-instance v0, Lio/realm/Property;

    const-string v1, "id"

    sget-object v2, Lio/realm/RealmFieldType;->INTEGER:Lio/realm/RealmFieldType;

    move v4, v3

    move v5, v3

    invoke-direct/range {v0 .. v5}, Lio/realm/Property;-><init>(Ljava/lang/String;Lio/realm/RealmFieldType;ZZZ)V

    invoke-virtual {v10, v0}, Lio/realm/RealmObjectSchema;->add(Lio/realm/Property;)Lio/realm/RealmObjectSchema;

    .line 133
    new-instance v4, Lio/realm/Property;

    const-string v5, "showTime"

    sget-object v6, Lio/realm/RealmFieldType;->INTEGER:Lio/realm/RealmFieldType;

    move v8, v7

    move v9, v3

    invoke-direct/range {v4 .. v9}, Lio/realm/Property;-><init>(Ljava/lang/String;Lio/realm/RealmFieldType;ZZZ)V

    invoke-virtual {v10, v4}, Lio/realm/RealmObjectSchema;->add(Lio/realm/Property;)Lio/realm/RealmObjectSchema;

    .line 136
    .end local v10    # "realmObjectSchema":Lio/realm/RealmObjectSchema;
    :goto_0
    return-object v10

    :cond_0
    const-string v0, "TrueColorsConfig"

    invoke-virtual {p0, v0}, Lio/realm/RealmSchema;->get(Ljava/lang/String;)Lio/realm/RealmObjectSchema;

    move-result-object v10

    goto :goto_0
.end method

.method public static createUsingJsonStream(Lio/realm/Realm;Landroid/util/JsonReader;)Lcom/safonov/speedreading/training/fragment/truecolors/repository/entity/TrueColorsConfig;
    .locals 5
    .param p0, "realm"    # Lio/realm/Realm;
    .param p1, "reader"    # Landroid/util/JsonReader;
    .annotation build Landroid/annotation/TargetApi;
        value = 0xb
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 262
    const/4 v0, 0x0

    .line 263
    .local v0, "jsonHasPrimaryKey":Z
    new-instance v2, Lcom/safonov/speedreading/training/fragment/truecolors/repository/entity/TrueColorsConfig;

    invoke-direct {v2}, Lcom/safonov/speedreading/training/fragment/truecolors/repository/entity/TrueColorsConfig;-><init>()V

    .line 264
    .local v2, "obj":Lcom/safonov/speedreading/training/fragment/truecolors/repository/entity/TrueColorsConfig;
    invoke-virtual {p1}, Landroid/util/JsonReader;->beginObject()V

    .line 265
    :goto_0
    invoke-virtual {p1}, Landroid/util/JsonReader;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_4

    .line 266
    invoke-virtual {p1}, Landroid/util/JsonReader;->nextName()Ljava/lang/String;

    move-result-object v1

    .line 267
    .local v1, "name":Ljava/lang/String;
    const-string v3, "id"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 268
    invoke-virtual {p1}, Landroid/util/JsonReader;->peek()Landroid/util/JsonToken;

    move-result-object v3

    sget-object v4, Landroid/util/JsonToken;->NULL:Landroid/util/JsonToken;

    if-ne v3, v4, :cond_0

    .line 269
    invoke-virtual {p1}, Landroid/util/JsonReader;->skipValue()V

    .line 270
    new-instance v3, Ljava/lang/IllegalArgumentException;

    const-string v4, "Trying to set non-nullable field \'id\' to null."

    invoke-direct {v3, v4}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v3

    :cond_0
    move-object v3, v2

    .line 272
    check-cast v3, Lio/realm/TrueColorsConfigRealmProxyInterface;

    invoke-virtual {p1}, Landroid/util/JsonReader;->nextInt()I

    move-result v4

    invoke-interface {v3, v4}, Lio/realm/TrueColorsConfigRealmProxyInterface;->realmSet$id(I)V

    .line 274
    const/4 v0, 0x1

    goto :goto_0

    .line 275
    :cond_1
    const-string v3, "showTime"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 276
    invoke-virtual {p1}, Landroid/util/JsonReader;->peek()Landroid/util/JsonToken;

    move-result-object v3

    sget-object v4, Landroid/util/JsonToken;->NULL:Landroid/util/JsonToken;

    if-ne v3, v4, :cond_2

    .line 277
    invoke-virtual {p1}, Landroid/util/JsonReader;->skipValue()V

    .line 278
    new-instance v3, Ljava/lang/IllegalArgumentException;

    const-string v4, "Trying to set non-nullable field \'showTime\' to null."

    invoke-direct {v3, v4}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v3

    :cond_2
    move-object v3, v2

    .line 280
    check-cast v3, Lio/realm/TrueColorsConfigRealmProxyInterface;

    invoke-virtual {p1}, Landroid/util/JsonReader;->nextInt()I

    move-result v4

    invoke-interface {v3, v4}, Lio/realm/TrueColorsConfigRealmProxyInterface;->realmSet$showTime(I)V

    goto :goto_0

    .line 283
    :cond_3
    invoke-virtual {p1}, Landroid/util/JsonReader;->skipValue()V

    goto :goto_0

    .line 286
    .end local v1    # "name":Ljava/lang/String;
    :cond_4
    invoke-virtual {p1}, Landroid/util/JsonReader;->endObject()V

    .line 287
    if-nez v0, :cond_5

    .line 288
    new-instance v3, Ljava/lang/IllegalArgumentException;

    const-string v4, "JSON object doesn\'t have the primary key field \'id\'."

    invoke-direct {v3, v4}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 290
    :cond_5
    invoke-virtual {p0, v2}, Lio/realm/Realm;->copyToRealm(Lio/realm/RealmModel;)Lio/realm/RealmModel;

    move-result-object v2

    .end local v2    # "obj":Lcom/safonov/speedreading/training/fragment/truecolors/repository/entity/TrueColorsConfig;
    check-cast v2, Lcom/safonov/speedreading/training/fragment/truecolors/repository/entity/TrueColorsConfig;

    .line 291
    .restart local v2    # "obj":Lcom/safonov/speedreading/training/fragment/truecolors/repository/entity/TrueColorsConfig;
    return-object v2
.end method

.method public static getFieldNames()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 212
    sget-object v0, Lio/realm/TrueColorsConfigRealmProxy;->FIELD_NAMES:Ljava/util/List;

    return-object v0
.end method

.method public static getTableName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 208
    const-string v0, "class_TrueColorsConfig"

    return-object v0
.end method

.method public static initTable(Lio/realm/internal/SharedRealm;)Lio/realm/internal/Table;
    .locals 4
    .param p0, "sharedRealm"    # Lio/realm/internal/SharedRealm;

    .prologue
    const/4 v3, 0x0

    .line 140
    const-string v1, "class_TrueColorsConfig"

    invoke-virtual {p0, v1}, Lio/realm/internal/SharedRealm;->hasTable(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 141
    const-string v1, "class_TrueColorsConfig"

    invoke-virtual {p0, v1}, Lio/realm/internal/SharedRealm;->getTable(Ljava/lang/String;)Lio/realm/internal/Table;

    move-result-object v0

    .line 142
    .local v0, "table":Lio/realm/internal/Table;
    sget-object v1, Lio/realm/RealmFieldType;->INTEGER:Lio/realm/RealmFieldType;

    const-string v2, "id"

    invoke-virtual {v0, v1, v2, v3}, Lio/realm/internal/Table;->addColumn(Lio/realm/RealmFieldType;Ljava/lang/String;Z)J

    .line 143
    sget-object v1, Lio/realm/RealmFieldType;->INTEGER:Lio/realm/RealmFieldType;

    const-string v2, "showTime"

    invoke-virtual {v0, v1, v2, v3}, Lio/realm/internal/Table;->addColumn(Lio/realm/RealmFieldType;Ljava/lang/String;Z)J

    .line 144
    const-string v1, "id"

    invoke-virtual {v0, v1}, Lio/realm/internal/Table;->getColumnIndex(Ljava/lang/String;)J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lio/realm/internal/Table;->addSearchIndex(J)V

    .line 145
    const-string v1, "id"

    invoke-virtual {v0, v1}, Lio/realm/internal/Table;->setPrimaryKey(Ljava/lang/String;)V

    .line 148
    .end local v0    # "table":Lio/realm/internal/Table;
    :goto_0
    return-object v0

    :cond_0
    const-string v1, "class_TrueColorsConfig"

    invoke-virtual {p0, v1}, Lio/realm/internal/SharedRealm;->getTable(Ljava/lang/String;)Lio/realm/internal/Table;

    move-result-object v0

    goto :goto_0
.end method

.method public static insert(Lio/realm/Realm;Lcom/safonov/speedreading/training/fragment/truecolors/repository/entity/TrueColorsConfig;Ljava/util/Map;)J
    .locals 18
    .param p0, "realm"    # Lio/realm/Realm;
    .param p1, "object"    # Lcom/safonov/speedreading/training/fragment/truecolors/repository/entity/TrueColorsConfig;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/realm/Realm;",
            "Lcom/safonov/speedreading/training/fragment/truecolors/repository/entity/TrueColorsConfig;",
            "Ljava/util/Map",
            "<",
            "Lio/realm/RealmModel;",
            "Ljava/lang/Long;",
            ">;)J"
        }
    .end annotation

    .prologue
    .line 347
    .local p2, "cache":Ljava/util/Map;, "Ljava/util/Map<Lio/realm/RealmModel;Ljava/lang/Long;>;"
    move-object/from16 v0, p1

    instance-of v6, v0, Lio/realm/internal/RealmObjectProxy;

    if-eqz v6, :cond_0

    move-object/from16 v6, p1

    check-cast v6, Lio/realm/internal/RealmObjectProxy;

    invoke-interface {v6}, Lio/realm/internal/RealmObjectProxy;->realmGet$proxyState()Lio/realm/ProxyState;

    move-result-object v6

    invoke-virtual {v6}, Lio/realm/ProxyState;->getRealm$realm()Lio/realm/BaseRealm;

    move-result-object v6

    if-eqz v6, :cond_0

    move-object/from16 v6, p1

    check-cast v6, Lio/realm/internal/RealmObjectProxy;

    invoke-interface {v6}, Lio/realm/internal/RealmObjectProxy;->realmGet$proxyState()Lio/realm/ProxyState;

    move-result-object v6

    invoke-virtual {v6}, Lio/realm/ProxyState;->getRealm$realm()Lio/realm/BaseRealm;

    move-result-object v6

    invoke-virtual {v6}, Lio/realm/BaseRealm;->getPath()Ljava/lang/String;

    move-result-object v6

    invoke-virtual/range {p0 .. p0}, Lio/realm/Realm;->getPath()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 348
    check-cast p1, Lio/realm/internal/RealmObjectProxy;

    .end local p1    # "object":Lcom/safonov/speedreading/training/fragment/truecolors/repository/entity/TrueColorsConfig;
    invoke-interface/range {p1 .. p1}, Lio/realm/internal/RealmObjectProxy;->realmGet$proxyState()Lio/realm/ProxyState;

    move-result-object v6

    invoke-virtual {v6}, Lio/realm/ProxyState;->getRow$realm()Lio/realm/internal/Row;

    move-result-object v6

    invoke-interface {v6}, Lio/realm/internal/Row;->getIndex()J

    move-result-wide v10

    .line 366
    :goto_0
    return-wide v10

    .line 350
    .restart local p1    # "object":Lcom/safonov/speedreading/training/fragment/truecolors/repository/entity/TrueColorsConfig;
    :cond_0
    const-class v6, Lcom/safonov/speedreading/training/fragment/truecolors/repository/entity/TrueColorsConfig;

    move-object/from16 v0, p0

    invoke-virtual {v0, v6}, Lio/realm/Realm;->getTable(Ljava/lang/Class;)Lio/realm/internal/Table;

    move-result-object v17

    .line 351
    .local v17, "table":Lio/realm/internal/Table;
    invoke-virtual/range {v17 .. v17}, Lio/realm/internal/Table;->getNativeTablePointer()J

    move-result-wide v2

    .line 352
    .local v2, "tableNativePtr":J
    move-object/from16 v0, p0

    iget-object v6, v0, Lio/realm/Realm;->schema:Lio/realm/RealmSchema;

    const-class v7, Lcom/safonov/speedreading/training/fragment/truecolors/repository/entity/TrueColorsConfig;

    invoke-virtual {v6, v7}, Lio/realm/RealmSchema;->getColumnInfo(Ljava/lang/Class;)Lio/realm/internal/ColumnInfo;

    move-result-object v15

    check-cast v15, Lio/realm/TrueColorsConfigRealmProxy$TrueColorsConfigColumnInfo;

    .line 353
    .local v15, "columnInfo":Lio/realm/TrueColorsConfigRealmProxy$TrueColorsConfigColumnInfo;
    invoke-virtual/range {v17 .. v17}, Lio/realm/internal/Table;->getPrimaryKey()J

    move-result-wide v4

    .line 354
    .local v4, "pkColumnIndex":J
    const-wide/16 v10, -0x1

    .local v10, "rowIndex":J
    move-object/from16 v6, p1

    .line 355
    check-cast v6, Lio/realm/TrueColorsConfigRealmProxyInterface;

    invoke-interface {v6}, Lio/realm/TrueColorsConfigRealmProxyInterface;->realmGet$id()I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v16

    .line 356
    .local v16, "primaryKeyValue":Ljava/lang/Integer;
    if-eqz v16, :cond_1

    move-object/from16 v6, p1

    .line 357
    check-cast v6, Lio/realm/TrueColorsConfigRealmProxyInterface;

    invoke-interface {v6}, Lio/realm/TrueColorsConfigRealmProxyInterface;->realmGet$id()I

    move-result v6

    int-to-long v6, v6

    invoke-static/range {v2 .. v7}, Lio/realm/internal/Table;->nativeFindFirstInt(JJJ)J

    move-result-wide v10

    .line 359
    :cond_1
    const-wide/16 v6, -0x1

    cmp-long v6, v10, v6

    if-nez v6, :cond_2

    move-object/from16 v6, p1

    .line 360
    check-cast v6, Lio/realm/TrueColorsConfigRealmProxyInterface;

    invoke-interface {v6}, Lio/realm/TrueColorsConfigRealmProxyInterface;->realmGet$id()I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    const/4 v7, 0x0

    move-object/from16 v0, v17

    invoke-virtual {v0, v6, v7}, Lio/realm/internal/Table;->addEmptyRowWithPrimaryKey(Ljava/lang/Object;Z)J

    move-result-wide v10

    .line 364
    :goto_1
    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    move-object/from16 v0, p2

    move-object/from16 v1, p1

    invoke-interface {v0, v1, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 365
    iget-wide v8, v15, Lio/realm/TrueColorsConfigRealmProxy$TrueColorsConfigColumnInfo;->showTimeIndex:J

    check-cast p1, Lio/realm/TrueColorsConfigRealmProxyInterface;

    .end local p1    # "object":Lcom/safonov/speedreading/training/fragment/truecolors/repository/entity/TrueColorsConfig;
    invoke-interface/range {p1 .. p1}, Lio/realm/TrueColorsConfigRealmProxyInterface;->realmGet$showTime()I

    move-result v6

    int-to-long v12, v6

    const/4 v14, 0x0

    move-wide v6, v2

    invoke-static/range {v6 .. v14}, Lio/realm/internal/Table;->nativeSetLong(JJJJZ)V

    goto :goto_0

    .line 362
    .restart local p1    # "object":Lcom/safonov/speedreading/training/fragment/truecolors/repository/entity/TrueColorsConfig;
    :cond_2
    invoke-static/range {v16 .. v16}, Lio/realm/internal/Table;->throwDuplicatePrimaryKeyException(Ljava/lang/Object;)V

    goto :goto_1
.end method

.method public static insert(Lio/realm/Realm;Ljava/util/Iterator;Ljava/util/Map;)V
    .locals 19
    .param p0, "realm"    # Lio/realm/Realm;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/realm/Realm;",
            "Ljava/util/Iterator",
            "<+",
            "Lio/realm/RealmModel;",
            ">;",
            "Ljava/util/Map",
            "<",
            "Lio/realm/RealmModel;",
            "Ljava/lang/Long;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 370
    .local p1, "objects":Ljava/util/Iterator;, "Ljava/util/Iterator<+Lio/realm/RealmModel;>;"
    .local p2, "cache":Ljava/util/Map;, "Ljava/util/Map<Lio/realm/RealmModel;Ljava/lang/Long;>;"
    const-class v6, Lcom/safonov/speedreading/training/fragment/truecolors/repository/entity/TrueColorsConfig;

    move-object/from16 v0, p0

    invoke-virtual {v0, v6}, Lio/realm/Realm;->getTable(Ljava/lang/Class;)Lio/realm/internal/Table;

    move-result-object v18

    .line 371
    .local v18, "table":Lio/realm/internal/Table;
    invoke-virtual/range {v18 .. v18}, Lio/realm/internal/Table;->getNativeTablePointer()J

    move-result-wide v2

    .line 372
    .local v2, "tableNativePtr":J
    move-object/from16 v0, p0

    iget-object v6, v0, Lio/realm/Realm;->schema:Lio/realm/RealmSchema;

    const-class v7, Lcom/safonov/speedreading/training/fragment/truecolors/repository/entity/TrueColorsConfig;

    invoke-virtual {v6, v7}, Lio/realm/RealmSchema;->getColumnInfo(Ljava/lang/Class;)Lio/realm/internal/ColumnInfo;

    move-result-object v15

    check-cast v15, Lio/realm/TrueColorsConfigRealmProxy$TrueColorsConfigColumnInfo;

    .line 373
    .local v15, "columnInfo":Lio/realm/TrueColorsConfigRealmProxy$TrueColorsConfigColumnInfo;
    invoke-virtual/range {v18 .. v18}, Lio/realm/internal/Table;->getPrimaryKey()J

    move-result-wide v4

    .line 374
    .local v4, "pkColumnIndex":J
    const/16 v16, 0x0

    .line 375
    .local v16, "object":Lcom/safonov/speedreading/training/fragment/truecolors/repository/entity/TrueColorsConfig;
    :cond_0
    :goto_0
    invoke-interface/range {p1 .. p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_4

    .line 376
    invoke-interface/range {p1 .. p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v16

    .end local v16    # "object":Lcom/safonov/speedreading/training/fragment/truecolors/repository/entity/TrueColorsConfig;
    check-cast v16, Lcom/safonov/speedreading/training/fragment/truecolors/repository/entity/TrueColorsConfig;

    .line 377
    .restart local v16    # "object":Lcom/safonov/speedreading/training/fragment/truecolors/repository/entity/TrueColorsConfig;
    move-object/from16 v0, p2

    move-object/from16 v1, v16

    invoke-interface {v0, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_0

    .line 378
    move-object/from16 v0, v16

    instance-of v6, v0, Lio/realm/internal/RealmObjectProxy;

    if-eqz v6, :cond_1

    move-object/from16 v6, v16

    check-cast v6, Lio/realm/internal/RealmObjectProxy;

    invoke-interface {v6}, Lio/realm/internal/RealmObjectProxy;->realmGet$proxyState()Lio/realm/ProxyState;

    move-result-object v6

    invoke-virtual {v6}, Lio/realm/ProxyState;->getRealm$realm()Lio/realm/BaseRealm;

    move-result-object v6

    if-eqz v6, :cond_1

    move-object/from16 v6, v16

    check-cast v6, Lio/realm/internal/RealmObjectProxy;

    invoke-interface {v6}, Lio/realm/internal/RealmObjectProxy;->realmGet$proxyState()Lio/realm/ProxyState;

    move-result-object v6

    invoke-virtual {v6}, Lio/realm/ProxyState;->getRealm$realm()Lio/realm/BaseRealm;

    move-result-object v6

    invoke-virtual {v6}, Lio/realm/BaseRealm;->getPath()Ljava/lang/String;

    move-result-object v6

    invoke-virtual/range {p0 .. p0}, Lio/realm/Realm;->getPath()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1

    move-object/from16 v6, v16

    .line 379
    check-cast v6, Lio/realm/internal/RealmObjectProxy;

    invoke-interface {v6}, Lio/realm/internal/RealmObjectProxy;->realmGet$proxyState()Lio/realm/ProxyState;

    move-result-object v6

    invoke-virtual {v6}, Lio/realm/ProxyState;->getRow$realm()Lio/realm/internal/Row;

    move-result-object v6

    invoke-interface {v6}, Lio/realm/internal/Row;->getIndex()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    move-object/from16 v0, p2

    move-object/from16 v1, v16

    invoke-interface {v0, v1, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 382
    :cond_1
    const-wide/16 v10, -0x1

    .local v10, "rowIndex":J
    move-object/from16 v6, v16

    .line 383
    check-cast v6, Lio/realm/TrueColorsConfigRealmProxyInterface;

    invoke-interface {v6}, Lio/realm/TrueColorsConfigRealmProxyInterface;->realmGet$id()I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v17

    .line 384
    .local v17, "primaryKeyValue":Ljava/lang/Integer;
    if-eqz v17, :cond_2

    move-object/from16 v6, v16

    .line 385
    check-cast v6, Lio/realm/TrueColorsConfigRealmProxyInterface;

    invoke-interface {v6}, Lio/realm/TrueColorsConfigRealmProxyInterface;->realmGet$id()I

    move-result v6

    int-to-long v6, v6

    invoke-static/range {v2 .. v7}, Lio/realm/internal/Table;->nativeFindFirstInt(JJJ)J

    move-result-wide v10

    .line 387
    :cond_2
    const-wide/16 v6, -0x1

    cmp-long v6, v10, v6

    if-nez v6, :cond_3

    move-object/from16 v6, v16

    .line 388
    check-cast v6, Lio/realm/TrueColorsConfigRealmProxyInterface;

    invoke-interface {v6}, Lio/realm/TrueColorsConfigRealmProxyInterface;->realmGet$id()I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    const/4 v7, 0x0

    move-object/from16 v0, v18

    invoke-virtual {v0, v6, v7}, Lio/realm/internal/Table;->addEmptyRowWithPrimaryKey(Ljava/lang/Object;Z)J

    move-result-wide v10

    .line 392
    :goto_1
    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    move-object/from16 v0, p2

    move-object/from16 v1, v16

    invoke-interface {v0, v1, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 393
    iget-wide v8, v15, Lio/realm/TrueColorsConfigRealmProxy$TrueColorsConfigColumnInfo;->showTimeIndex:J

    move-object/from16 v6, v16

    check-cast v6, Lio/realm/TrueColorsConfigRealmProxyInterface;

    invoke-interface {v6}, Lio/realm/TrueColorsConfigRealmProxyInterface;->realmGet$showTime()I

    move-result v6

    int-to-long v12, v6

    const/4 v14, 0x0

    move-wide v6, v2

    invoke-static/range {v6 .. v14}, Lio/realm/internal/Table;->nativeSetLong(JJJJZ)V

    goto/16 :goto_0

    .line 390
    :cond_3
    invoke-static/range {v17 .. v17}, Lio/realm/internal/Table;->throwDuplicatePrimaryKeyException(Ljava/lang/Object;)V

    goto :goto_1

    .line 396
    .end local v10    # "rowIndex":J
    .end local v17    # "primaryKeyValue":Ljava/lang/Integer;
    :cond_4
    return-void
.end method

.method public static insertOrUpdate(Lio/realm/Realm;Lcom/safonov/speedreading/training/fragment/truecolors/repository/entity/TrueColorsConfig;Ljava/util/Map;)J
    .locals 18
    .param p0, "realm"    # Lio/realm/Realm;
    .param p1, "object"    # Lcom/safonov/speedreading/training/fragment/truecolors/repository/entity/TrueColorsConfig;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/realm/Realm;",
            "Lcom/safonov/speedreading/training/fragment/truecolors/repository/entity/TrueColorsConfig;",
            "Ljava/util/Map",
            "<",
            "Lio/realm/RealmModel;",
            "Ljava/lang/Long;",
            ">;)J"
        }
    .end annotation

    .prologue
    .line 399
    .local p2, "cache":Ljava/util/Map;, "Ljava/util/Map<Lio/realm/RealmModel;Ljava/lang/Long;>;"
    move-object/from16 v0, p1

    instance-of v6, v0, Lio/realm/internal/RealmObjectProxy;

    if-eqz v6, :cond_0

    move-object/from16 v6, p1

    check-cast v6, Lio/realm/internal/RealmObjectProxy;

    invoke-interface {v6}, Lio/realm/internal/RealmObjectProxy;->realmGet$proxyState()Lio/realm/ProxyState;

    move-result-object v6

    invoke-virtual {v6}, Lio/realm/ProxyState;->getRealm$realm()Lio/realm/BaseRealm;

    move-result-object v6

    if-eqz v6, :cond_0

    move-object/from16 v6, p1

    check-cast v6, Lio/realm/internal/RealmObjectProxy;

    invoke-interface {v6}, Lio/realm/internal/RealmObjectProxy;->realmGet$proxyState()Lio/realm/ProxyState;

    move-result-object v6

    invoke-virtual {v6}, Lio/realm/ProxyState;->getRealm$realm()Lio/realm/BaseRealm;

    move-result-object v6

    invoke-virtual {v6}, Lio/realm/BaseRealm;->getPath()Ljava/lang/String;

    move-result-object v6

    invoke-virtual/range {p0 .. p0}, Lio/realm/Realm;->getPath()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 400
    check-cast p1, Lio/realm/internal/RealmObjectProxy;

    .end local p1    # "object":Lcom/safonov/speedreading/training/fragment/truecolors/repository/entity/TrueColorsConfig;
    invoke-interface/range {p1 .. p1}, Lio/realm/internal/RealmObjectProxy;->realmGet$proxyState()Lio/realm/ProxyState;

    move-result-object v6

    invoke-virtual {v6}, Lio/realm/ProxyState;->getRow$realm()Lio/realm/internal/Row;

    move-result-object v6

    invoke-interface {v6}, Lio/realm/internal/Row;->getIndex()J

    move-result-wide v10

    .line 416
    :goto_0
    return-wide v10

    .line 402
    .restart local p1    # "object":Lcom/safonov/speedreading/training/fragment/truecolors/repository/entity/TrueColorsConfig;
    :cond_0
    const-class v6, Lcom/safonov/speedreading/training/fragment/truecolors/repository/entity/TrueColorsConfig;

    move-object/from16 v0, p0

    invoke-virtual {v0, v6}, Lio/realm/Realm;->getTable(Ljava/lang/Class;)Lio/realm/internal/Table;

    move-result-object v17

    .line 403
    .local v17, "table":Lio/realm/internal/Table;
    invoke-virtual/range {v17 .. v17}, Lio/realm/internal/Table;->getNativeTablePointer()J

    move-result-wide v2

    .line 404
    .local v2, "tableNativePtr":J
    move-object/from16 v0, p0

    iget-object v6, v0, Lio/realm/Realm;->schema:Lio/realm/RealmSchema;

    const-class v7, Lcom/safonov/speedreading/training/fragment/truecolors/repository/entity/TrueColorsConfig;

    invoke-virtual {v6, v7}, Lio/realm/RealmSchema;->getColumnInfo(Ljava/lang/Class;)Lio/realm/internal/ColumnInfo;

    move-result-object v15

    check-cast v15, Lio/realm/TrueColorsConfigRealmProxy$TrueColorsConfigColumnInfo;

    .line 405
    .local v15, "columnInfo":Lio/realm/TrueColorsConfigRealmProxy$TrueColorsConfigColumnInfo;
    invoke-virtual/range {v17 .. v17}, Lio/realm/internal/Table;->getPrimaryKey()J

    move-result-wide v4

    .line 406
    .local v4, "pkColumnIndex":J
    const-wide/16 v10, -0x1

    .local v10, "rowIndex":J
    move-object/from16 v6, p1

    .line 407
    check-cast v6, Lio/realm/TrueColorsConfigRealmProxyInterface;

    invoke-interface {v6}, Lio/realm/TrueColorsConfigRealmProxyInterface;->realmGet$id()I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v16

    .line 408
    .local v16, "primaryKeyValue":Ljava/lang/Integer;
    if-eqz v16, :cond_1

    move-object/from16 v6, p1

    .line 409
    check-cast v6, Lio/realm/TrueColorsConfigRealmProxyInterface;

    invoke-interface {v6}, Lio/realm/TrueColorsConfigRealmProxyInterface;->realmGet$id()I

    move-result v6

    int-to-long v6, v6

    invoke-static/range {v2 .. v7}, Lio/realm/internal/Table;->nativeFindFirstInt(JJJ)J

    move-result-wide v10

    .line 411
    :cond_1
    const-wide/16 v6, -0x1

    cmp-long v6, v10, v6

    if-nez v6, :cond_2

    move-object/from16 v6, p1

    .line 412
    check-cast v6, Lio/realm/TrueColorsConfigRealmProxyInterface;

    invoke-interface {v6}, Lio/realm/TrueColorsConfigRealmProxyInterface;->realmGet$id()I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    const/4 v7, 0x0

    move-object/from16 v0, v17

    invoke-virtual {v0, v6, v7}, Lio/realm/internal/Table;->addEmptyRowWithPrimaryKey(Ljava/lang/Object;Z)J

    move-result-wide v10

    .line 414
    :cond_2
    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    move-object/from16 v0, p2

    move-object/from16 v1, p1

    invoke-interface {v0, v1, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 415
    iget-wide v8, v15, Lio/realm/TrueColorsConfigRealmProxy$TrueColorsConfigColumnInfo;->showTimeIndex:J

    check-cast p1, Lio/realm/TrueColorsConfigRealmProxyInterface;

    .end local p1    # "object":Lcom/safonov/speedreading/training/fragment/truecolors/repository/entity/TrueColorsConfig;
    invoke-interface/range {p1 .. p1}, Lio/realm/TrueColorsConfigRealmProxyInterface;->realmGet$showTime()I

    move-result v6

    int-to-long v12, v6

    const/4 v14, 0x0

    move-wide v6, v2

    invoke-static/range {v6 .. v14}, Lio/realm/internal/Table;->nativeSetLong(JJJJZ)V

    goto :goto_0
.end method

.method public static insertOrUpdate(Lio/realm/Realm;Ljava/util/Iterator;Ljava/util/Map;)V
    .locals 19
    .param p0, "realm"    # Lio/realm/Realm;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/realm/Realm;",
            "Ljava/util/Iterator",
            "<+",
            "Lio/realm/RealmModel;",
            ">;",
            "Ljava/util/Map",
            "<",
            "Lio/realm/RealmModel;",
            "Ljava/lang/Long;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 420
    .local p1, "objects":Ljava/util/Iterator;, "Ljava/util/Iterator<+Lio/realm/RealmModel;>;"
    .local p2, "cache":Ljava/util/Map;, "Ljava/util/Map<Lio/realm/RealmModel;Ljava/lang/Long;>;"
    const-class v6, Lcom/safonov/speedreading/training/fragment/truecolors/repository/entity/TrueColorsConfig;

    move-object/from16 v0, p0

    invoke-virtual {v0, v6}, Lio/realm/Realm;->getTable(Ljava/lang/Class;)Lio/realm/internal/Table;

    move-result-object v18

    .line 421
    .local v18, "table":Lio/realm/internal/Table;
    invoke-virtual/range {v18 .. v18}, Lio/realm/internal/Table;->getNativeTablePointer()J

    move-result-wide v2

    .line 422
    .local v2, "tableNativePtr":J
    move-object/from16 v0, p0

    iget-object v6, v0, Lio/realm/Realm;->schema:Lio/realm/RealmSchema;

    const-class v7, Lcom/safonov/speedreading/training/fragment/truecolors/repository/entity/TrueColorsConfig;

    invoke-virtual {v6, v7}, Lio/realm/RealmSchema;->getColumnInfo(Ljava/lang/Class;)Lio/realm/internal/ColumnInfo;

    move-result-object v15

    check-cast v15, Lio/realm/TrueColorsConfigRealmProxy$TrueColorsConfigColumnInfo;

    .line 423
    .local v15, "columnInfo":Lio/realm/TrueColorsConfigRealmProxy$TrueColorsConfigColumnInfo;
    invoke-virtual/range {v18 .. v18}, Lio/realm/internal/Table;->getPrimaryKey()J

    move-result-wide v4

    .line 424
    .local v4, "pkColumnIndex":J
    const/16 v16, 0x0

    .line 425
    .local v16, "object":Lcom/safonov/speedreading/training/fragment/truecolors/repository/entity/TrueColorsConfig;
    :cond_0
    :goto_0
    invoke-interface/range {p1 .. p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_4

    .line 426
    invoke-interface/range {p1 .. p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v16

    .end local v16    # "object":Lcom/safonov/speedreading/training/fragment/truecolors/repository/entity/TrueColorsConfig;
    check-cast v16, Lcom/safonov/speedreading/training/fragment/truecolors/repository/entity/TrueColorsConfig;

    .line 427
    .restart local v16    # "object":Lcom/safonov/speedreading/training/fragment/truecolors/repository/entity/TrueColorsConfig;
    move-object/from16 v0, p2

    move-object/from16 v1, v16

    invoke-interface {v0, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_0

    .line 428
    move-object/from16 v0, v16

    instance-of v6, v0, Lio/realm/internal/RealmObjectProxy;

    if-eqz v6, :cond_1

    move-object/from16 v6, v16

    check-cast v6, Lio/realm/internal/RealmObjectProxy;

    invoke-interface {v6}, Lio/realm/internal/RealmObjectProxy;->realmGet$proxyState()Lio/realm/ProxyState;

    move-result-object v6

    invoke-virtual {v6}, Lio/realm/ProxyState;->getRealm$realm()Lio/realm/BaseRealm;

    move-result-object v6

    if-eqz v6, :cond_1

    move-object/from16 v6, v16

    check-cast v6, Lio/realm/internal/RealmObjectProxy;

    invoke-interface {v6}, Lio/realm/internal/RealmObjectProxy;->realmGet$proxyState()Lio/realm/ProxyState;

    move-result-object v6

    invoke-virtual {v6}, Lio/realm/ProxyState;->getRealm$realm()Lio/realm/BaseRealm;

    move-result-object v6

    invoke-virtual {v6}, Lio/realm/BaseRealm;->getPath()Ljava/lang/String;

    move-result-object v6

    invoke-virtual/range {p0 .. p0}, Lio/realm/Realm;->getPath()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1

    move-object/from16 v6, v16

    .line 429
    check-cast v6, Lio/realm/internal/RealmObjectProxy;

    invoke-interface {v6}, Lio/realm/internal/RealmObjectProxy;->realmGet$proxyState()Lio/realm/ProxyState;

    move-result-object v6

    invoke-virtual {v6}, Lio/realm/ProxyState;->getRow$realm()Lio/realm/internal/Row;

    move-result-object v6

    invoke-interface {v6}, Lio/realm/internal/Row;->getIndex()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    move-object/from16 v0, p2

    move-object/from16 v1, v16

    invoke-interface {v0, v1, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 432
    :cond_1
    const-wide/16 v10, -0x1

    .local v10, "rowIndex":J
    move-object/from16 v6, v16

    .line 433
    check-cast v6, Lio/realm/TrueColorsConfigRealmProxyInterface;

    invoke-interface {v6}, Lio/realm/TrueColorsConfigRealmProxyInterface;->realmGet$id()I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v17

    .line 434
    .local v17, "primaryKeyValue":Ljava/lang/Integer;
    if-eqz v17, :cond_2

    move-object/from16 v6, v16

    .line 435
    check-cast v6, Lio/realm/TrueColorsConfigRealmProxyInterface;

    invoke-interface {v6}, Lio/realm/TrueColorsConfigRealmProxyInterface;->realmGet$id()I

    move-result v6

    int-to-long v6, v6

    invoke-static/range {v2 .. v7}, Lio/realm/internal/Table;->nativeFindFirstInt(JJJ)J

    move-result-wide v10

    .line 437
    :cond_2
    const-wide/16 v6, -0x1

    cmp-long v6, v10, v6

    if-nez v6, :cond_3

    move-object/from16 v6, v16

    .line 438
    check-cast v6, Lio/realm/TrueColorsConfigRealmProxyInterface;

    invoke-interface {v6}, Lio/realm/TrueColorsConfigRealmProxyInterface;->realmGet$id()I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    const/4 v7, 0x0

    move-object/from16 v0, v18

    invoke-virtual {v0, v6, v7}, Lio/realm/internal/Table;->addEmptyRowWithPrimaryKey(Ljava/lang/Object;Z)J

    move-result-wide v10

    .line 440
    :cond_3
    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    move-object/from16 v0, p2

    move-object/from16 v1, v16

    invoke-interface {v0, v1, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 441
    iget-wide v8, v15, Lio/realm/TrueColorsConfigRealmProxy$TrueColorsConfigColumnInfo;->showTimeIndex:J

    move-object/from16 v6, v16

    check-cast v6, Lio/realm/TrueColorsConfigRealmProxyInterface;

    invoke-interface {v6}, Lio/realm/TrueColorsConfigRealmProxyInterface;->realmGet$showTime()I

    move-result v6

    int-to-long v12, v6

    const/4 v14, 0x0

    move-wide v6, v2

    invoke-static/range {v6 .. v14}, Lio/realm/internal/Table;->nativeSetLong(JJJJZ)V

    goto/16 :goto_0

    .line 444
    .end local v10    # "rowIndex":J
    .end local v17    # "primaryKeyValue":Ljava/lang/Integer;
    :cond_4
    return-void
.end method

.method static update(Lio/realm/Realm;Lcom/safonov/speedreading/training/fragment/truecolors/repository/entity/TrueColorsConfig;Lcom/safonov/speedreading/training/fragment/truecolors/repository/entity/TrueColorsConfig;Ljava/util/Map;)Lcom/safonov/speedreading/training/fragment/truecolors/repository/entity/TrueColorsConfig;
    .locals 2
    .param p0, "realm"    # Lio/realm/Realm;
    .param p1, "realmObject"    # Lcom/safonov/speedreading/training/fragment/truecolors/repository/entity/TrueColorsConfig;
    .param p2, "newObject"    # Lcom/safonov/speedreading/training/fragment/truecolors/repository/entity/TrueColorsConfig;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/realm/Realm;",
            "Lcom/safonov/speedreading/training/fragment/truecolors/repository/entity/TrueColorsConfig;",
            "Lcom/safonov/speedreading/training/fragment/truecolors/repository/entity/TrueColorsConfig;",
            "Ljava/util/Map",
            "<",
            "Lio/realm/RealmModel;",
            "Lio/realm/internal/RealmObjectProxy;",
            ">;)",
            "Lcom/safonov/speedreading/training/fragment/truecolors/repository/entity/TrueColorsConfig;"
        }
    .end annotation

    .prologue
    .line 470
    .local p3, "cache":Ljava/util/Map;, "Ljava/util/Map<Lio/realm/RealmModel;Lio/realm/internal/RealmObjectProxy;>;"
    move-object v0, p1

    check-cast v0, Lio/realm/TrueColorsConfigRealmProxyInterface;

    check-cast p2, Lio/realm/TrueColorsConfigRealmProxyInterface;

    .end local p2    # "newObject":Lcom/safonov/speedreading/training/fragment/truecolors/repository/entity/TrueColorsConfig;
    invoke-interface {p2}, Lio/realm/TrueColorsConfigRealmProxyInterface;->realmGet$showTime()I

    move-result v1

    invoke-interface {v0, v1}, Lio/realm/TrueColorsConfigRealmProxyInterface;->realmSet$showTime(I)V

    .line 471
    return-object p1
.end method

.method public static validateTable(Lio/realm/internal/SharedRealm;Z)Lio/realm/TrueColorsConfigRealmProxy$TrueColorsConfigColumnInfo;
    .locals 12
    .param p0, "sharedRealm"    # Lio/realm/internal/SharedRealm;
    .param p1, "allowExtraColumns"    # Z

    .prologue
    const-wide/16 v8, 0x2

    .line 152
    const-string v7, "class_TrueColorsConfig"

    invoke-virtual {p0, v7}, Lio/realm/internal/SharedRealm;->hasTable(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_c

    .line 153
    const-string v7, "class_TrueColorsConfig"

    invoke-virtual {p0, v7}, Lio/realm/internal/SharedRealm;->getTable(Ljava/lang/String;)Lio/realm/internal/Table;

    move-result-object v6

    .line 154
    .local v6, "table":Lio/realm/internal/Table;
    invoke-virtual {v6}, Lio/realm/internal/Table;->getColumnCount()J

    move-result-wide v0

    .line 155
    .local v0, "columnCount":J
    cmp-long v7, v0, v8

    if-eqz v7, :cond_1

    .line 156
    cmp-long v7, v0, v8

    if-gez v7, :cond_0

    .line 157
    new-instance v7, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->getPath()Ljava/lang/String;

    move-result-object v8

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Field count is less than expected - expected 2 but was "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-direct {v7, v8, v9}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v7

    .line 159
    :cond_0
    if-eqz p1, :cond_2

    .line 160
    const-string v7, "Field count is more than expected - expected 2 but was %1$d"

    const/4 v8, 0x1

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v10

    aput-object v10, v8, v9

    invoke-static {v7, v8}, Lio/realm/log/RealmLog;->debug(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 165
    :cond_1
    new-instance v3, Ljava/util/HashMap;

    invoke-direct {v3}, Ljava/util/HashMap;-><init>()V

    .line 166
    .local v3, "columnTypes":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lio/realm/RealmFieldType;>;"
    const-wide/16 v4, 0x0

    .local v4, "i":J
    :goto_0
    cmp-long v7, v4, v0

    if-gez v7, :cond_3

    .line 167
    invoke-virtual {v6, v4, v5}, Lio/realm/internal/Table;->getColumnName(J)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v4, v5}, Lio/realm/internal/Table;->getColumnType(J)Lio/realm/RealmFieldType;

    move-result-object v8

    invoke-interface {v3, v7, v8}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 166
    const-wide/16 v8, 0x1

    add-long/2addr v4, v8

    goto :goto_0

    .line 162
    .end local v3    # "columnTypes":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lio/realm/RealmFieldType;>;"
    .end local v4    # "i":J
    :cond_2
    new-instance v7, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->getPath()Ljava/lang/String;

    move-result-object v8

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Field count is more than expected - expected 2 but was "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-direct {v7, v8, v9}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v7

    .line 170
    .restart local v3    # "columnTypes":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lio/realm/RealmFieldType;>;"
    .restart local v4    # "i":J
    :cond_3
    new-instance v2, Lio/realm/TrueColorsConfigRealmProxy$TrueColorsConfigColumnInfo;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->getPath()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v2, v7, v6}, Lio/realm/TrueColorsConfigRealmProxy$TrueColorsConfigColumnInfo;-><init>(Ljava/lang/String;Lio/realm/internal/Table;)V

    .line 172
    .local v2, "columnInfo":Lio/realm/TrueColorsConfigRealmProxy$TrueColorsConfigColumnInfo;
    invoke-virtual {v6}, Lio/realm/internal/Table;->hasPrimaryKey()Z

    move-result v7

    if-nez v7, :cond_4

    .line 173
    new-instance v7, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->getPath()Ljava/lang/String;

    move-result-object v8

    const-string v9, "Primary key not defined for field \'id\' in existing Realm file. @PrimaryKey was added."

    invoke-direct {v7, v8, v9}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v7

    .line 175
    :cond_4
    invoke-virtual {v6}, Lio/realm/internal/Table;->getPrimaryKey()J

    move-result-wide v8

    iget-wide v10, v2, Lio/realm/TrueColorsConfigRealmProxy$TrueColorsConfigColumnInfo;->idIndex:J

    cmp-long v7, v8, v10

    if-eqz v7, :cond_5

    .line 176
    new-instance v7, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->getPath()Ljava/lang/String;

    move-result-object v8

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Primary Key annotation definition was changed, from field "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v6}, Lio/realm/internal/Table;->getPrimaryKey()J

    move-result-wide v10

    invoke-virtual {v6, v10, v11}, Lio/realm/internal/Table;->getColumnName(J)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " to field id"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-direct {v7, v8, v9}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v7

    .line 180
    :cond_5
    const-string v7, "id"

    invoke-interface {v3, v7}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_6

    .line 181
    new-instance v7, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->getPath()Ljava/lang/String;

    move-result-object v8

    const-string v9, "Missing field \'id\' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn()."

    invoke-direct {v7, v8, v9}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v7

    .line 183
    :cond_6
    const-string v7, "id"

    invoke-interface {v3, v7}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    sget-object v8, Lio/realm/RealmFieldType;->INTEGER:Lio/realm/RealmFieldType;

    if-eq v7, v8, :cond_7

    .line 184
    new-instance v7, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->getPath()Ljava/lang/String;

    move-result-object v8

    const-string v9, "Invalid type \'int\' for field \'id\' in existing Realm file."

    invoke-direct {v7, v8, v9}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v7

    .line 186
    :cond_7
    iget-wide v8, v2, Lio/realm/TrueColorsConfigRealmProxy$TrueColorsConfigColumnInfo;->idIndex:J

    invoke-virtual {v6, v8, v9}, Lio/realm/internal/Table;->isColumnNullable(J)Z

    move-result v7

    if-eqz v7, :cond_8

    iget-wide v8, v2, Lio/realm/TrueColorsConfigRealmProxy$TrueColorsConfigColumnInfo;->idIndex:J

    invoke-virtual {v6, v8, v9}, Lio/realm/internal/Table;->findFirstNull(J)J

    move-result-wide v8

    const-wide/16 v10, -0x1

    cmp-long v7, v8, v10

    if-eqz v7, :cond_8

    .line 187
    new-instance v7, Ljava/lang/IllegalStateException;

    const-string v8, "Cannot migrate an object with null value in field \'id\'. Either maintain the same type for primary key field \'id\', or remove the object with null value before migration."

    invoke-direct {v7, v8}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v7

    .line 189
    :cond_8
    const-string v7, "id"

    invoke-virtual {v6, v7}, Lio/realm/internal/Table;->getColumnIndex(Ljava/lang/String;)J

    move-result-wide v8

    invoke-virtual {v6, v8, v9}, Lio/realm/internal/Table;->hasSearchIndex(J)Z

    move-result v7

    if-nez v7, :cond_9

    .line 190
    new-instance v7, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->getPath()Ljava/lang/String;

    move-result-object v8

    const-string v9, "Index not defined for field \'id\' in existing Realm file. Either set @Index or migrate using io.realm.internal.Table.removeSearchIndex()."

    invoke-direct {v7, v8, v9}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v7

    .line 192
    :cond_9
    const-string v7, "showTime"

    invoke-interface {v3, v7}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_a

    .line 193
    new-instance v7, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->getPath()Ljava/lang/String;

    move-result-object v8

    const-string v9, "Missing field \'showTime\' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn()."

    invoke-direct {v7, v8, v9}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v7

    .line 195
    :cond_a
    const-string v7, "showTime"

    invoke-interface {v3, v7}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    sget-object v8, Lio/realm/RealmFieldType;->INTEGER:Lio/realm/RealmFieldType;

    if-eq v7, v8, :cond_b

    .line 196
    new-instance v7, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->getPath()Ljava/lang/String;

    move-result-object v8

    const-string v9, "Invalid type \'int\' for field \'showTime\' in existing Realm file."

    invoke-direct {v7, v8, v9}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v7

    .line 198
    :cond_b
    iget-wide v8, v2, Lio/realm/TrueColorsConfigRealmProxy$TrueColorsConfigColumnInfo;->showTimeIndex:J

    invoke-virtual {v6, v8, v9}, Lio/realm/internal/Table;->isColumnNullable(J)Z

    move-result v7

    if-eqz v7, :cond_d

    .line 199
    new-instance v7, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->getPath()Ljava/lang/String;

    move-result-object v8

    const-string v9, "Field \'showTime\' does support null values in the existing Realm file. Use corresponding boxed type for field \'showTime\' or migrate using RealmObjectSchema.setNullable()."

    invoke-direct {v7, v8, v9}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v7

    .line 203
    .end local v0    # "columnCount":J
    .end local v2    # "columnInfo":Lio/realm/TrueColorsConfigRealmProxy$TrueColorsConfigColumnInfo;
    .end local v3    # "columnTypes":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lio/realm/RealmFieldType;>;"
    .end local v4    # "i":J
    .end local v6    # "table":Lio/realm/internal/Table;
    :cond_c
    new-instance v7, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->getPath()Ljava/lang/String;

    move-result-object v8

    const-string v9, "The \'TrueColorsConfig\' class is missing from the schema for this Realm."

    invoke-direct {v7, v8, v9}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v7

    .line 201
    .restart local v0    # "columnCount":J
    .restart local v2    # "columnInfo":Lio/realm/TrueColorsConfigRealmProxy$TrueColorsConfigColumnInfo;
    .restart local v3    # "columnTypes":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lio/realm/RealmFieldType;>;"
    .restart local v4    # "i":J
    .restart local v6    # "table":Lio/realm/internal/Table;
    :cond_d
    return-object v2
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 12
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v5, 0x1

    const/4 v6, 0x0

    .line 511
    if-ne p0, p1, :cond_1

    .line 525
    :cond_0
    :goto_0
    return v5

    .line 512
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v7

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v8

    if-eq v7, v8, :cond_3

    :cond_2
    move v5, v6

    goto :goto_0

    :cond_3
    move-object v0, p1

    .line 513
    check-cast v0, Lio/realm/TrueColorsConfigRealmProxy;

    .line 515
    .local v0, "aTrueColorsConfig":Lio/realm/TrueColorsConfigRealmProxy;
    iget-object v7, p0, Lio/realm/TrueColorsConfigRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v7}, Lio/realm/ProxyState;->getRealm$realm()Lio/realm/BaseRealm;

    move-result-object v7

    invoke-virtual {v7}, Lio/realm/BaseRealm;->getPath()Ljava/lang/String;

    move-result-object v3

    .line 516
    .local v3, "path":Ljava/lang/String;
    iget-object v7, v0, Lio/realm/TrueColorsConfigRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v7}, Lio/realm/ProxyState;->getRealm$realm()Lio/realm/BaseRealm;

    move-result-object v7

    invoke-virtual {v7}, Lio/realm/BaseRealm;->getPath()Ljava/lang/String;

    move-result-object v1

    .line 517
    .local v1, "otherPath":Ljava/lang/String;
    if-eqz v3, :cond_5

    invoke-virtual {v3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_6

    :cond_4
    move v5, v6

    goto :goto_0

    :cond_5
    if-nez v1, :cond_4

    .line 519
    :cond_6
    iget-object v7, p0, Lio/realm/TrueColorsConfigRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v7}, Lio/realm/ProxyState;->getRow$realm()Lio/realm/internal/Row;

    move-result-object v7

    invoke-interface {v7}, Lio/realm/internal/Row;->getTable()Lio/realm/internal/Table;

    move-result-object v7

    invoke-virtual {v7}, Lio/realm/internal/Table;->getName()Ljava/lang/String;

    move-result-object v4

    .line 520
    .local v4, "tableName":Ljava/lang/String;
    iget-object v7, v0, Lio/realm/TrueColorsConfigRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v7}, Lio/realm/ProxyState;->getRow$realm()Lio/realm/internal/Row;

    move-result-object v7

    invoke-interface {v7}, Lio/realm/internal/Row;->getTable()Lio/realm/internal/Table;

    move-result-object v7

    invoke-virtual {v7}, Lio/realm/internal/Table;->getName()Ljava/lang/String;

    move-result-object v2

    .line 521
    .local v2, "otherTableName":Ljava/lang/String;
    if-eqz v4, :cond_8

    invoke-virtual {v4, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_9

    :cond_7
    move v5, v6

    goto :goto_0

    :cond_8
    if-nez v2, :cond_7

    .line 523
    :cond_9
    iget-object v7, p0, Lio/realm/TrueColorsConfigRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v7}, Lio/realm/ProxyState;->getRow$realm()Lio/realm/internal/Row;

    move-result-object v7

    invoke-interface {v7}, Lio/realm/internal/Row;->getIndex()J

    move-result-wide v8

    iget-object v7, v0, Lio/realm/TrueColorsConfigRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v7}, Lio/realm/ProxyState;->getRow$realm()Lio/realm/internal/Row;

    move-result-object v7

    invoke-interface {v7}, Lio/realm/internal/Row;->getIndex()J

    move-result-wide v10

    cmp-long v7, v8, v10

    if-eqz v7, :cond_0

    move v5, v6

    goto :goto_0
.end method

.method public hashCode()I
    .locals 8

    .prologue
    const/4 v5, 0x0

    .line 498
    iget-object v6, p0, Lio/realm/TrueColorsConfigRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v6}, Lio/realm/ProxyState;->getRealm$realm()Lio/realm/BaseRealm;

    move-result-object v6

    invoke-virtual {v6}, Lio/realm/BaseRealm;->getPath()Ljava/lang/String;

    move-result-object v0

    .line 499
    .local v0, "realmName":Ljava/lang/String;
    iget-object v6, p0, Lio/realm/TrueColorsConfigRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v6}, Lio/realm/ProxyState;->getRow$realm()Lio/realm/internal/Row;

    move-result-object v6

    invoke-interface {v6}, Lio/realm/internal/Row;->getTable()Lio/realm/internal/Table;

    move-result-object v6

    invoke-virtual {v6}, Lio/realm/internal/Table;->getName()Ljava/lang/String;

    move-result-object v4

    .line 500
    .local v4, "tableName":Ljava/lang/String;
    iget-object v6, p0, Lio/realm/TrueColorsConfigRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v6}, Lio/realm/ProxyState;->getRow$realm()Lio/realm/internal/Row;

    move-result-object v6

    invoke-interface {v6}, Lio/realm/internal/Row;->getIndex()J

    move-result-wide v2

    .line 502
    .local v2, "rowIndex":J
    const/16 v1, 0x11

    .line 503
    .local v1, "result":I
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v6

    :goto_0
    add-int/lit16 v1, v6, 0x20f

    .line 504
    mul-int/lit8 v6, v1, 0x1f

    if-eqz v4, :cond_0

    invoke-virtual {v4}, Ljava/lang/String;->hashCode()I

    move-result v5

    :cond_0
    add-int v1, v6, v5

    .line 505
    mul-int/lit8 v5, v1, 0x1f

    const/16 v6, 0x20

    ushr-long v6, v2, v6

    xor-long/2addr v6, v2

    long-to-int v6, v6

    add-int v1, v5, v6

    .line 506
    return v1

    :cond_1
    move v6, v5

    .line 503
    goto :goto_0
.end method

.method public realm$injectObjectContext()V
    .locals 3

    .prologue
    .line 81
    iget-object v1, p0, Lio/realm/TrueColorsConfigRealmProxy;->proxyState:Lio/realm/ProxyState;

    if-eqz v1, :cond_0

    .line 91
    :goto_0
    return-void

    .line 84
    :cond_0
    sget-object v1, Lio/realm/BaseRealm;->objectContext:Lio/realm/BaseRealm$ThreadLocalRealmObjectContext;

    invoke-virtual {v1}, Lio/realm/BaseRealm$ThreadLocalRealmObjectContext;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/realm/BaseRealm$RealmObjectContext;

    .line 85
    .local v0, "context":Lio/realm/BaseRealm$RealmObjectContext;
    invoke-virtual {v0}, Lio/realm/BaseRealm$RealmObjectContext;->getColumnInfo()Lio/realm/internal/ColumnInfo;

    move-result-object v1

    check-cast v1, Lio/realm/TrueColorsConfigRealmProxy$TrueColorsConfigColumnInfo;

    iput-object v1, p0, Lio/realm/TrueColorsConfigRealmProxy;->columnInfo:Lio/realm/TrueColorsConfigRealmProxy$TrueColorsConfigColumnInfo;

    .line 86
    new-instance v1, Lio/realm/ProxyState;

    invoke-direct {v1, p0}, Lio/realm/ProxyState;-><init>(Lio/realm/RealmModel;)V

    iput-object v1, p0, Lio/realm/TrueColorsConfigRealmProxy;->proxyState:Lio/realm/ProxyState;

    .line 87
    iget-object v1, p0, Lio/realm/TrueColorsConfigRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v0}, Lio/realm/BaseRealm$RealmObjectContext;->getRealm()Lio/realm/BaseRealm;

    move-result-object v2

    invoke-virtual {v1, v2}, Lio/realm/ProxyState;->setRealm$realm(Lio/realm/BaseRealm;)V

    .line 88
    iget-object v1, p0, Lio/realm/TrueColorsConfigRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v0}, Lio/realm/BaseRealm$RealmObjectContext;->getRow()Lio/realm/internal/Row;

    move-result-object v2

    invoke-virtual {v1, v2}, Lio/realm/ProxyState;->setRow$realm(Lio/realm/internal/Row;)V

    .line 89
    iget-object v1, p0, Lio/realm/TrueColorsConfigRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v0}, Lio/realm/BaseRealm$RealmObjectContext;->getAcceptDefaultValue()Z

    move-result v2

    invoke-virtual {v1, v2}, Lio/realm/ProxyState;->setAcceptDefaultValue$realm(Z)V

    .line 90
    iget-object v1, p0, Lio/realm/TrueColorsConfigRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v0}, Lio/realm/BaseRealm$RealmObjectContext;->getExcludeFields()Ljava/util/List;

    move-result-object v2

    invoke-virtual {v1, v2}, Lio/realm/ProxyState;->setExcludeFields$realm(Ljava/util/List;)V

    goto :goto_0
.end method

.method public realmGet$id()I
    .locals 4

    .prologue
    .line 95
    iget-object v0, p0, Lio/realm/TrueColorsConfigRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v0}, Lio/realm/ProxyState;->getRealm$realm()Lio/realm/BaseRealm;

    move-result-object v0

    invoke-virtual {v0}, Lio/realm/BaseRealm;->checkIfValid()V

    .line 96
    iget-object v0, p0, Lio/realm/TrueColorsConfigRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v0}, Lio/realm/ProxyState;->getRow$realm()Lio/realm/internal/Row;

    move-result-object v0

    iget-object v1, p0, Lio/realm/TrueColorsConfigRealmProxy;->columnInfo:Lio/realm/TrueColorsConfigRealmProxy$TrueColorsConfigColumnInfo;

    iget-wide v2, v1, Lio/realm/TrueColorsConfigRealmProxy$TrueColorsConfigColumnInfo;->idIndex:J

    invoke-interface {v0, v2, v3}, Lio/realm/internal/Row;->getLong(J)J

    move-result-wide v0

    long-to-int v0, v0

    return v0
.end method

.method public realmGet$proxyState()Lio/realm/ProxyState;
    .locals 1

    .prologue
    .line 493
    iget-object v0, p0, Lio/realm/TrueColorsConfigRealmProxy;->proxyState:Lio/realm/ProxyState;

    return-object v0
.end method

.method public realmGet$showTime()I
    .locals 4

    .prologue
    .line 111
    iget-object v0, p0, Lio/realm/TrueColorsConfigRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v0}, Lio/realm/ProxyState;->getRealm$realm()Lio/realm/BaseRealm;

    move-result-object v0

    invoke-virtual {v0}, Lio/realm/BaseRealm;->checkIfValid()V

    .line 112
    iget-object v0, p0, Lio/realm/TrueColorsConfigRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v0}, Lio/realm/ProxyState;->getRow$realm()Lio/realm/internal/Row;

    move-result-object v0

    iget-object v1, p0, Lio/realm/TrueColorsConfigRealmProxy;->columnInfo:Lio/realm/TrueColorsConfigRealmProxy$TrueColorsConfigColumnInfo;

    iget-wide v2, v1, Lio/realm/TrueColorsConfigRealmProxy$TrueColorsConfigColumnInfo;->showTimeIndex:J

    invoke-interface {v0, v2, v3}, Lio/realm/internal/Row;->getLong(J)J

    move-result-wide v0

    long-to-int v0, v0

    return v0
.end method

.method public realmSet$id(I)V
    .locals 2
    .param p1, "value"    # I

    .prologue
    .line 100
    iget-object v0, p0, Lio/realm/TrueColorsConfigRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v0}, Lio/realm/ProxyState;->isUnderConstruction()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 102
    return-void

    .line 105
    :cond_0
    iget-object v0, p0, Lio/realm/TrueColorsConfigRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v0}, Lio/realm/ProxyState;->getRealm$realm()Lio/realm/BaseRealm;

    move-result-object v0

    invoke-virtual {v0}, Lio/realm/BaseRealm;->checkIfValid()V

    .line 106
    new-instance v0, Lio/realm/exceptions/RealmException;

    const-string v1, "Primary key field \'id\' cannot be changed after object was created."

    invoke-direct {v0, v1}, Lio/realm/exceptions/RealmException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public realmSet$showTime(I)V
    .locals 9
    .param p1, "value"    # I

    .prologue
    .line 116
    iget-object v1, p0, Lio/realm/TrueColorsConfigRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v1}, Lio/realm/ProxyState;->isUnderConstruction()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 117
    iget-object v1, p0, Lio/realm/TrueColorsConfigRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v1}, Lio/realm/ProxyState;->getAcceptDefaultValue$realm()Z

    move-result v1

    if-nez v1, :cond_0

    .line 127
    :goto_0
    return-void

    .line 120
    :cond_0
    iget-object v1, p0, Lio/realm/TrueColorsConfigRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v1}, Lio/realm/ProxyState;->getRow$realm()Lio/realm/internal/Row;

    move-result-object v0

    .line 121
    .local v0, "row":Lio/realm/internal/Row;
    invoke-interface {v0}, Lio/realm/internal/Row;->getTable()Lio/realm/internal/Table;

    move-result-object v1

    iget-object v2, p0, Lio/realm/TrueColorsConfigRealmProxy;->columnInfo:Lio/realm/TrueColorsConfigRealmProxy$TrueColorsConfigColumnInfo;

    iget-wide v2, v2, Lio/realm/TrueColorsConfigRealmProxy$TrueColorsConfigColumnInfo;->showTimeIndex:J

    invoke-interface {v0}, Lio/realm/internal/Row;->getIndex()J

    move-result-wide v4

    int-to-long v6, p1

    const/4 v8, 0x1

    invoke-virtual/range {v1 .. v8}, Lio/realm/internal/Table;->setLong(JJJZ)V

    goto :goto_0

    .line 125
    .end local v0    # "row":Lio/realm/internal/Row;
    :cond_1
    iget-object v1, p0, Lio/realm/TrueColorsConfigRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v1}, Lio/realm/ProxyState;->getRealm$realm()Lio/realm/BaseRealm;

    move-result-object v1

    invoke-virtual {v1}, Lio/realm/BaseRealm;->checkIfValid()V

    .line 126
    iget-object v1, p0, Lio/realm/TrueColorsConfigRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v1}, Lio/realm/ProxyState;->getRow$realm()Lio/realm/internal/Row;

    move-result-object v1

    iget-object v2, p0, Lio/realm/TrueColorsConfigRealmProxy;->columnInfo:Lio/realm/TrueColorsConfigRealmProxy$TrueColorsConfigColumnInfo;

    iget-wide v2, v2, Lio/realm/TrueColorsConfigRealmProxy$TrueColorsConfigColumnInfo;->showTimeIndex:J

    int-to-long v4, p1

    invoke-interface {v1, v2, v3, v4, v5}, Lio/realm/internal/Row;->setLong(JJ)V

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 476
    invoke-static {p0}, Lio/realm/RealmObject;->isValid(Lio/realm/RealmModel;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 477
    const-string v1, "Invalid object"

    .line 488
    :goto_0
    return-object v1

    .line 479
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "TrueColorsConfig = ["

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 480
    .local v0, "stringBuilder":Ljava/lang/StringBuilder;
    const-string v1, "{id:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 481
    invoke-virtual {p0}, Lio/realm/TrueColorsConfigRealmProxy;->realmGet$id()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 482
    const-string v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 483
    const-string v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 484
    const-string v1, "{showTime:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 485
    invoke-virtual {p0}, Lio/realm/TrueColorsConfigRealmProxy;->realmGet$showTime()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 486
    const-string v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 487
    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 488
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method
