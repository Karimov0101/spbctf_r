.class abstract Lio/realm/OrderedRealmCollectionImpl;
.super Ljava/util/AbstractList;
.source "OrderedRealmCollectionImpl.java"

# interfaces
.implements Lio/realm/OrderedRealmCollection;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lio/realm/OrderedRealmCollectionImpl$RealmCollectionListIterator;,
        Lio/realm/OrderedRealmCollectionImpl$RealmCollectionIterator;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "Lio/realm/RealmModel;",
        ">",
        "Ljava/util/AbstractList",
        "<TE;>;",
        "Lio/realm/OrderedRealmCollection",
        "<TE;>;"
    }
.end annotation


# static fields
.field private static final NOT_SUPPORTED_MESSAGE:Ljava/lang/String; = "This method is not supported by \'RealmResults\' or \'OrderedRealmCollectionSnapshot\'."


# instance fields
.field className:Ljava/lang/String;

.field classSpec:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<TE;>;"
        }
    .end annotation
.end field

.field final collection:Lio/realm/internal/Collection;

.field final realm:Lio/realm/BaseRealm;


# direct methods
.method constructor <init>(Lio/realm/BaseRealm;Lio/realm/internal/Collection;Ljava/lang/Class;)V
    .locals 0
    .param p1, "realm"    # Lio/realm/BaseRealm;
    .param p2, "collection"    # Lio/realm/internal/Collection;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/realm/BaseRealm;",
            "Lio/realm/internal/Collection;",
            "Ljava/lang/Class",
            "<TE;>;)V"
        }
    .end annotation

    .prologue
    .line 30
    .local p0, "this":Lio/realm/OrderedRealmCollectionImpl;, "Lio/realm/OrderedRealmCollectionImpl<TE;>;"
    .local p3, "clazz":Ljava/lang/Class;, "Ljava/lang/Class<TE;>;"
    invoke-direct {p0}, Ljava/util/AbstractList;-><init>()V

    .line 31
    iput-object p1, p0, Lio/realm/OrderedRealmCollectionImpl;->realm:Lio/realm/BaseRealm;

    .line 32
    iput-object p3, p0, Lio/realm/OrderedRealmCollectionImpl;->classSpec:Ljava/lang/Class;

    .line 33
    iput-object p2, p0, Lio/realm/OrderedRealmCollectionImpl;->collection:Lio/realm/internal/Collection;

    .line 34
    return-void
.end method

.method constructor <init>(Lio/realm/BaseRealm;Lio/realm/internal/Collection;Ljava/lang/String;)V
    .locals 0
    .param p1, "realm"    # Lio/realm/BaseRealm;
    .param p2, "collection"    # Lio/realm/internal/Collection;
    .param p3, "className"    # Ljava/lang/String;

    .prologue
    .line 36
    .local p0, "this":Lio/realm/OrderedRealmCollectionImpl;, "Lio/realm/OrderedRealmCollectionImpl<TE;>;"
    invoke-direct {p0}, Ljava/util/AbstractList;-><init>()V

    .line 37
    iput-object p1, p0, Lio/realm/OrderedRealmCollectionImpl;->realm:Lio/realm/BaseRealm;

    .line 38
    iput-object p3, p0, Lio/realm/OrderedRealmCollectionImpl;->className:Ljava/lang/String;

    .line 39
    iput-object p2, p0, Lio/realm/OrderedRealmCollectionImpl;->collection:Lio/realm/internal/Collection;

    .line 40
    return-void
.end method

.method private firstImpl(ZLio/realm/RealmModel;)Lio/realm/RealmModel;
    .locals 4
    .param p1, "shouldThrow"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(ZTE;)TE;"
        }
    .end annotation

    .prologue
    .line 126
    .local p0, "this":Lio/realm/OrderedRealmCollectionImpl;, "Lio/realm/OrderedRealmCollectionImpl<TE;>;"
    .local p2, "defaultValue":Lio/realm/RealmModel;, "TE;"
    iget-object v1, p0, Lio/realm/OrderedRealmCollectionImpl;->collection:Lio/realm/internal/Collection;

    invoke-virtual {v1}, Lio/realm/internal/Collection;->firstUncheckedRow()Lio/realm/internal/UncheckedRow;

    move-result-object v0

    .line 128
    .local v0, "row":Lio/realm/internal/UncheckedRow;
    if-eqz v0, :cond_1

    .line 129
    iget-object v1, p0, Lio/realm/OrderedRealmCollectionImpl;->realm:Lio/realm/BaseRealm;

    iget-object v2, p0, Lio/realm/OrderedRealmCollectionImpl;->classSpec:Ljava/lang/Class;

    iget-object v3, p0, Lio/realm/OrderedRealmCollectionImpl;->className:Ljava/lang/String;

    invoke-virtual {v1, v2, v3, v0}, Lio/realm/BaseRealm;->get(Ljava/lang/Class;Ljava/lang/String;Lio/realm/internal/UncheckedRow;)Lio/realm/RealmModel;

    move-result-object p2

    .line 134
    .end local p2    # "defaultValue":Lio/realm/RealmModel;, "TE;"
    :cond_0
    return-object p2

    .line 131
    .restart local p2    # "defaultValue":Lio/realm/RealmModel;, "TE;"
    :cond_1
    if-eqz p1, :cond_0

    .line 132
    new-instance v1, Ljava/lang/IndexOutOfBoundsException;

    const-string v2, "No results were found."

    invoke-direct {v1, v2}, Ljava/lang/IndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method private getColumnIndexForSort(Ljava/lang/String;)J
    .locals 6
    .param p1, "fieldName"    # Ljava/lang/String;

    .prologue
    .line 237
    .local p0, "this":Lio/realm/OrderedRealmCollectionImpl;, "Lio/realm/OrderedRealmCollectionImpl<TE;>;"
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 238
    :cond_0
    new-instance v2, Ljava/lang/IllegalArgumentException;

    const-string v3, "Non-empty field name required."

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 240
    :cond_1
    const-string v2, "."

    invoke-virtual {p1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 241
    new-instance v2, Ljava/lang/IllegalArgumentException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Aggregates on child object fields are not supported: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 243
    :cond_2
    iget-object v2, p0, Lio/realm/OrderedRealmCollectionImpl;->collection:Lio/realm/internal/Collection;

    invoke-virtual {v2}, Lio/realm/internal/Collection;->getTable()Lio/realm/internal/Table;

    move-result-object v2

    invoke-virtual {v2, p1}, Lio/realm/internal/Table;->getColumnIndex(Ljava/lang/String;)J

    move-result-wide v0

    .line 244
    .local v0, "columnIndex":J
    const-wide/16 v2, 0x0

    cmp-long v2, v0, v2

    if-gez v2, :cond_3

    .line 245
    new-instance v2, Ljava/lang/IllegalArgumentException;

    const-string v3, "Field \'%s\' does not exist."

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object p1, v4, v5

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 247
    :cond_3
    return-wide v0
.end method

.method private lastImpl(ZLio/realm/RealmModel;)Lio/realm/RealmModel;
    .locals 4
    .param p1, "shouldThrow"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(ZTE;)TE;"
        }
    .end annotation

    .prologue
    .line 157
    .local p0, "this":Lio/realm/OrderedRealmCollectionImpl;, "Lio/realm/OrderedRealmCollectionImpl<TE;>;"
    .local p2, "defaultValue":Lio/realm/RealmModel;, "TE;"
    iget-object v1, p0, Lio/realm/OrderedRealmCollectionImpl;->collection:Lio/realm/internal/Collection;

    invoke-virtual {v1}, Lio/realm/internal/Collection;->lastUncheckedRow()Lio/realm/internal/UncheckedRow;

    move-result-object v0

    .line 159
    .local v0, "row":Lio/realm/internal/UncheckedRow;
    if-eqz v0, :cond_1

    .line 160
    iget-object v1, p0, Lio/realm/OrderedRealmCollectionImpl;->realm:Lio/realm/BaseRealm;

    iget-object v2, p0, Lio/realm/OrderedRealmCollectionImpl;->classSpec:Ljava/lang/Class;

    iget-object v3, p0, Lio/realm/OrderedRealmCollectionImpl;->className:Ljava/lang/String;

    invoke-virtual {v1, v2, v3, v0}, Lio/realm/BaseRealm;->get(Ljava/lang/Class;Ljava/lang/String;Lio/realm/internal/UncheckedRow;)Lio/realm/RealmModel;

    move-result-object p2

    .line 165
    .end local p2    # "defaultValue":Lio/realm/RealmModel;, "TE;"
    :cond_0
    return-object p2

    .line 162
    .restart local p2    # "defaultValue":Lio/realm/RealmModel;, "TE;"
    :cond_1
    if-eqz p1, :cond_0

    .line 163
    new-instance v1, Ljava/lang/IndexOutOfBoundsException;

    const-string v2, "No results were found."

    invoke-direct {v1, v2}, Ljava/lang/IndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v1
.end method


# virtual methods
.method public add(ILio/realm/RealmModel;)V
    .locals 2
    .param p1, "index"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(ITE;)V"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 491
    .local p0, "this":Lio/realm/OrderedRealmCollectionImpl;, "Lio/realm/OrderedRealmCollectionImpl<TE;>;"
    .local p2, "element":Lio/realm/RealmModel;, "TE;"
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "This method is not supported by \'RealmResults\' or \'OrderedRealmCollectionSnapshot\'."

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public bridge synthetic add(ILjava/lang/Object;)V
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 19
    .local p0, "this":Lio/realm/OrderedRealmCollectionImpl;, "Lio/realm/OrderedRealmCollectionImpl<TE;>;"
    check-cast p2, Lio/realm/RealmModel;

    invoke-virtual {p0, p1, p2}, Lio/realm/OrderedRealmCollectionImpl;->add(ILio/realm/RealmModel;)V

    return-void
.end method

.method public add(Lio/realm/RealmModel;)Z
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TE;)Z"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 480
    .local p0, "this":Lio/realm/OrderedRealmCollectionImpl;, "Lio/realm/OrderedRealmCollectionImpl<TE;>;"
    .local p1, "element":Lio/realm/RealmModel;, "TE;"
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "This method is not supported by \'RealmResults\' or \'OrderedRealmCollectionSnapshot\'."

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public bridge synthetic add(Ljava/lang/Object;)Z
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 19
    .local p0, "this":Lio/realm/OrderedRealmCollectionImpl;, "Lio/realm/OrderedRealmCollectionImpl<TE;>;"
    check-cast p1, Lio/realm/RealmModel;

    invoke-virtual {p0, p1}, Lio/realm/OrderedRealmCollectionImpl;->add(Lio/realm/RealmModel;)Z

    move-result v0

    return v0
.end method

.method public addAll(ILjava/util/Collection;)Z
    .locals 2
    .param p1, "location"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/util/Collection",
            "<+TE;>;)Z"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 503
    .local p0, "this":Lio/realm/OrderedRealmCollectionImpl;, "Lio/realm/OrderedRealmCollectionImpl<TE;>;"
    .local p2, "collection":Ljava/util/Collection;, "Ljava/util/Collection<+TE;>;"
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "This method is not supported by \'RealmResults\' or \'OrderedRealmCollectionSnapshot\'."

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public addAll(Ljava/util/Collection;)Z
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<+TE;>;)Z"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 514
    .local p0, "this":Lio/realm/OrderedRealmCollectionImpl;, "Lio/realm/OrderedRealmCollectionImpl<TE;>;"
    .local p1, "collection":Ljava/util/Collection;, "Ljava/util/Collection<+TE;>;"
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "This method is not supported by \'RealmResults\' or \'OrderedRealmCollectionSnapshot\'."

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public average(Ljava/lang/String;)D
    .locals 6
    .param p1, "fieldName"    # Ljava/lang/String;

    .prologue
    .line 373
    .local p0, "this":Lio/realm/OrderedRealmCollectionImpl;, "Lio/realm/OrderedRealmCollectionImpl<TE;>;"
    iget-object v1, p0, Lio/realm/OrderedRealmCollectionImpl;->realm:Lio/realm/BaseRealm;

    invoke-virtual {v1}, Lio/realm/BaseRealm;->checkIfValid()V

    .line 374
    invoke-direct {p0, p1}, Lio/realm/OrderedRealmCollectionImpl;->getColumnIndexForSort(Ljava/lang/String;)J

    move-result-wide v2

    .line 376
    .local v2, "columnIndex":J
    iget-object v1, p0, Lio/realm/OrderedRealmCollectionImpl;->collection:Lio/realm/internal/Collection;

    sget-object v4, Lio/realm/internal/Collection$Aggregate;->AVERAGE:Lio/realm/internal/Collection$Aggregate;

    invoke-virtual {v1, v4, v2, v3}, Lio/realm/internal/Collection;->aggregateNumber(Lio/realm/internal/Collection$Aggregate;J)Ljava/lang/Number;

    move-result-object v0

    .line 377
    .local v0, "avg":Ljava/lang/Number;
    invoke-virtual {v0}, Ljava/lang/Number;->doubleValue()D

    move-result-wide v4

    return-wide v4
.end method

.method public clear()V
    .locals 2
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 469
    .local p0, "this":Lio/realm/OrderedRealmCollectionImpl;, "Lio/realm/OrderedRealmCollectionImpl<TE;>;"
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "This method is not supported by \'RealmResults\' or \'OrderedRealmCollectionSnapshot\'."

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public contains(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "object"    # Ljava/lang/Object;

    .prologue
    .local p0, "this":Lio/realm/OrderedRealmCollectionImpl;, "Lio/realm/OrderedRealmCollectionImpl<TE;>;"
    const/4 v2, 0x0

    .line 78
    invoke-virtual {p0}, Lio/realm/OrderedRealmCollectionImpl;->isLoaded()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 80
    instance-of v3, p1, Lio/realm/internal/RealmObjectProxy;

    if-eqz v3, :cond_1

    move-object v1, p1

    .line 81
    check-cast v1, Lio/realm/internal/RealmObjectProxy;

    .line 82
    .local v1, "proxy":Lio/realm/internal/RealmObjectProxy;
    invoke-interface {v1}, Lio/realm/internal/RealmObjectProxy;->realmGet$proxyState()Lio/realm/ProxyState;

    move-result-object v3

    invoke-virtual {v3}, Lio/realm/ProxyState;->getRow$realm()Lio/realm/internal/Row;

    move-result-object v3

    sget-object v4, Lio/realm/internal/InvalidRow;->INSTANCE:Lio/realm/internal/InvalidRow;

    if-ne v3, v4, :cond_1

    .line 93
    .end local v1    # "proxy":Lio/realm/internal/RealmObjectProxy;
    :cond_0
    :goto_0
    return v2

    .line 87
    :cond_1
    invoke-virtual {p0}, Lio/realm/OrderedRealmCollectionImpl;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/realm/RealmModel;

    .line 88
    .local v0, "e":Lio/realm/RealmModel;, "TE;"
    invoke-virtual {v0, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 89
    const/4 v2, 0x1

    goto :goto_0
.end method

.method createLoadedResults(Lio/realm/internal/Collection;)Lio/realm/RealmResults;
    .locals 3
    .param p1, "newCollection"    # Lio/realm/internal/Collection;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/realm/internal/Collection;",
            ")",
            "Lio/realm/RealmResults",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 552
    .local p0, "this":Lio/realm/OrderedRealmCollectionImpl;, "Lio/realm/OrderedRealmCollectionImpl<TE;>;"
    iget-object v1, p0, Lio/realm/OrderedRealmCollectionImpl;->className:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 553
    new-instance v0, Lio/realm/RealmResults;

    iget-object v1, p0, Lio/realm/OrderedRealmCollectionImpl;->realm:Lio/realm/BaseRealm;

    iget-object v2, p0, Lio/realm/OrderedRealmCollectionImpl;->className:Ljava/lang/String;

    invoke-direct {v0, v1, p1, v2}, Lio/realm/RealmResults;-><init>(Lio/realm/BaseRealm;Lio/realm/internal/Collection;Ljava/lang/String;)V

    .line 557
    .local v0, "results":Lio/realm/RealmResults;, "Lio/realm/RealmResults<TE;>;"
    :goto_0
    invoke-virtual {v0}, Lio/realm/RealmResults;->load()Z

    .line 558
    return-object v0

    .line 555
    .end local v0    # "results":Lio/realm/RealmResults;, "Lio/realm/RealmResults<TE;>;"
    :cond_0
    new-instance v0, Lio/realm/RealmResults;

    iget-object v1, p0, Lio/realm/OrderedRealmCollectionImpl;->realm:Lio/realm/BaseRealm;

    iget-object v2, p0, Lio/realm/OrderedRealmCollectionImpl;->classSpec:Ljava/lang/Class;

    invoke-direct {v0, v1, p1, v2}, Lio/realm/RealmResults;-><init>(Lio/realm/BaseRealm;Lio/realm/internal/Collection;Ljava/lang/Class;)V

    .restart local v0    # "results":Lio/realm/RealmResults;, "Lio/realm/RealmResults<TE;>;"
    goto :goto_0
.end method

.method public createSnapshot()Lio/realm/OrderedRealmCollectionSnapshot;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/realm/OrderedRealmCollectionSnapshot",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 531
    .local p0, "this":Lio/realm/OrderedRealmCollectionImpl;, "Lio/realm/OrderedRealmCollectionImpl<TE;>;"
    iget-object v0, p0, Lio/realm/OrderedRealmCollectionImpl;->className:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 532
    new-instance v0, Lio/realm/OrderedRealmCollectionSnapshot;

    iget-object v1, p0, Lio/realm/OrderedRealmCollectionImpl;->realm:Lio/realm/BaseRealm;

    iget-object v2, p0, Lio/realm/OrderedRealmCollectionImpl;->collection:Lio/realm/internal/Collection;

    iget-object v3, p0, Lio/realm/OrderedRealmCollectionImpl;->className:Ljava/lang/String;

    invoke-direct {v0, v1, v2, v3}, Lio/realm/OrderedRealmCollectionSnapshot;-><init>(Lio/realm/BaseRealm;Lio/realm/internal/Collection;Ljava/lang/String;)V

    .line 534
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lio/realm/OrderedRealmCollectionSnapshot;

    iget-object v1, p0, Lio/realm/OrderedRealmCollectionImpl;->realm:Lio/realm/BaseRealm;

    iget-object v2, p0, Lio/realm/OrderedRealmCollectionImpl;->collection:Lio/realm/internal/Collection;

    iget-object v3, p0, Lio/realm/OrderedRealmCollectionImpl;->classSpec:Ljava/lang/Class;

    invoke-direct {v0, v1, v2, v3}, Lio/realm/OrderedRealmCollectionSnapshot;-><init>(Lio/realm/BaseRealm;Lio/realm/internal/Collection;Ljava/lang/Class;)V

    goto :goto_0
.end method

.method public deleteAllFromRealm()Z
    .locals 1

    .prologue
    .line 185
    .local p0, "this":Lio/realm/OrderedRealmCollectionImpl;, "Lio/realm/OrderedRealmCollectionImpl<TE;>;"
    iget-object v0, p0, Lio/realm/OrderedRealmCollectionImpl;->realm:Lio/realm/BaseRealm;

    invoke-virtual {v0}, Lio/realm/BaseRealm;->checkIfValid()V

    .line 186
    invoke-virtual {p0}, Lio/realm/OrderedRealmCollectionImpl;->size()I

    move-result v0

    if-lez v0, :cond_0

    .line 187
    iget-object v0, p0, Lio/realm/OrderedRealmCollectionImpl;->collection:Lio/realm/internal/Collection;

    invoke-virtual {v0}, Lio/realm/internal/Collection;->clear()V

    .line 188
    const/4 v0, 0x1

    .line 190
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public deleteFirstFromRealm()Z
    .locals 1

    .prologue
    .line 457
    .local p0, "this":Lio/realm/OrderedRealmCollectionImpl;, "Lio/realm/OrderedRealmCollectionImpl<TE;>;"
    iget-object v0, p0, Lio/realm/OrderedRealmCollectionImpl;->realm:Lio/realm/BaseRealm;

    invoke-virtual {v0}, Lio/realm/BaseRealm;->checkIfValidAndInTransaction()V

    .line 458
    iget-object v0, p0, Lio/realm/OrderedRealmCollectionImpl;->collection:Lio/realm/internal/Collection;

    invoke-virtual {v0}, Lio/realm/internal/Collection;->deleteFirst()Z

    move-result v0

    return v0
.end method

.method public deleteFromRealm(I)V
    .locals 4
    .param p1, "location"    # I

    .prologue
    .line 176
    .local p0, "this":Lio/realm/OrderedRealmCollectionImpl;, "Lio/realm/OrderedRealmCollectionImpl<TE;>;"
    iget-object v0, p0, Lio/realm/OrderedRealmCollectionImpl;->realm:Lio/realm/BaseRealm;

    invoke-virtual {v0}, Lio/realm/BaseRealm;->checkIfValidAndInTransaction()V

    .line 177
    iget-object v0, p0, Lio/realm/OrderedRealmCollectionImpl;->collection:Lio/realm/internal/Collection;

    int-to-long v2, p1

    invoke-virtual {v0, v2, v3}, Lio/realm/internal/Collection;->delete(J)V

    .line 178
    return-void
.end method

.method public deleteLastFromRealm()Z
    .locals 1

    .prologue
    .line 445
    .local p0, "this":Lio/realm/OrderedRealmCollectionImpl;, "Lio/realm/OrderedRealmCollectionImpl<TE;>;"
    iget-object v0, p0, Lio/realm/OrderedRealmCollectionImpl;->realm:Lio/realm/BaseRealm;

    invoke-virtual {v0}, Lio/realm/BaseRealm;->checkIfValidAndInTransaction()V

    .line 446
    iget-object v0, p0, Lio/realm/OrderedRealmCollectionImpl;->collection:Lio/realm/internal/Collection;

    invoke-virtual {v0}, Lio/realm/internal/Collection;->deleteLast()Z

    move-result v0

    return v0
.end method

.method public first()Lio/realm/RealmModel;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TE;"
        }
    .end annotation

    .prologue
    .line 114
    .local p0, "this":Lio/realm/OrderedRealmCollectionImpl;, "Lio/realm/OrderedRealmCollectionImpl<TE;>;"
    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lio/realm/OrderedRealmCollectionImpl;->firstImpl(ZLio/realm/RealmModel;)Lio/realm/RealmModel;

    move-result-object v0

    return-object v0
.end method

.method public first(Lio/realm/RealmModel;)Lio/realm/RealmModel;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TE;)TE;"
        }
    .end annotation

    .prologue
    .line 122
    .local p0, "this":Lio/realm/OrderedRealmCollectionImpl;, "Lio/realm/OrderedRealmCollectionImpl<TE;>;"
    .local p1, "defaultValue":Lio/realm/RealmModel;, "TE;"
    const/4 v0, 0x0

    invoke-direct {p0, v0, p1}, Lio/realm/OrderedRealmCollectionImpl;->firstImpl(ZLio/realm/RealmModel;)Lio/realm/RealmModel;

    move-result-object v0

    return-object v0
.end method

.method public get(I)Lio/realm/RealmModel;
    .locals 4
    .param p1, "location"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)TE;"
        }
    .end annotation

    .prologue
    .line 105
    .local p0, "this":Lio/realm/OrderedRealmCollectionImpl;, "Lio/realm/OrderedRealmCollectionImpl<TE;>;"
    iget-object v0, p0, Lio/realm/OrderedRealmCollectionImpl;->realm:Lio/realm/BaseRealm;

    invoke-virtual {v0}, Lio/realm/BaseRealm;->checkIfValid()V

    .line 106
    iget-object v0, p0, Lio/realm/OrderedRealmCollectionImpl;->realm:Lio/realm/BaseRealm;

    iget-object v1, p0, Lio/realm/OrderedRealmCollectionImpl;->classSpec:Ljava/lang/Class;

    iget-object v2, p0, Lio/realm/OrderedRealmCollectionImpl;->className:Ljava/lang/String;

    iget-object v3, p0, Lio/realm/OrderedRealmCollectionImpl;->collection:Lio/realm/internal/Collection;

    invoke-virtual {v3, p1}, Lio/realm/internal/Collection;->getUncheckedRow(I)Lio/realm/internal/UncheckedRow;

    move-result-object v3

    invoke-virtual {v0, v1, v2, v3}, Lio/realm/BaseRealm;->get(Ljava/lang/Class;Ljava/lang/String;Lio/realm/internal/UncheckedRow;)Lio/realm/RealmModel;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 19
    .local p0, "this":Lio/realm/OrderedRealmCollectionImpl;, "Lio/realm/OrderedRealmCollectionImpl<TE;>;"
    invoke-virtual {p0, p1}, Lio/realm/OrderedRealmCollectionImpl;->get(I)Lio/realm/RealmModel;

    move-result-object v0

    return-object v0
.end method

.method getCollection()Lio/realm/internal/Collection;
    .locals 1

    .prologue
    .line 47
    .local p0, "this":Lio/realm/OrderedRealmCollectionImpl;, "Lio/realm/OrderedRealmCollectionImpl<TE;>;"
    iget-object v0, p0, Lio/realm/OrderedRealmCollectionImpl;->collection:Lio/realm/internal/Collection;

    return-object v0
.end method

.method getTable()Lio/realm/internal/Table;
    .locals 1

    .prologue
    .line 43
    .local p0, "this":Lio/realm/OrderedRealmCollectionImpl;, "Lio/realm/OrderedRealmCollectionImpl<TE;>;"
    iget-object v0, p0, Lio/realm/OrderedRealmCollectionImpl;->collection:Lio/realm/internal/Collection;

    invoke-virtual {v0}, Lio/realm/internal/Collection;->getTable()Lio/realm/internal/Table;

    move-result-object v0

    return-object v0
.end method

.method public isManaged()Z
    .locals 1

    .prologue
    .line 66
    .local p0, "this":Lio/realm/OrderedRealmCollectionImpl;, "Lio/realm/OrderedRealmCollectionImpl<TE;>;"
    const/4 v0, 0x1

    return v0
.end method

.method public isValid()Z
    .locals 1

    .prologue
    .line 55
    .local p0, "this":Lio/realm/OrderedRealmCollectionImpl;, "Lio/realm/OrderedRealmCollectionImpl<TE;>;"
    iget-object v0, p0, Lio/realm/OrderedRealmCollectionImpl;->collection:Lio/realm/internal/Collection;

    invoke-virtual {v0}, Lio/realm/internal/Collection;->isValid()Z

    move-result v0

    return v0
.end method

.method public iterator()Ljava/util/Iterator;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Iterator",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 203
    .local p0, "this":Lio/realm/OrderedRealmCollectionImpl;, "Lio/realm/OrderedRealmCollectionImpl<TE;>;"
    new-instance v0, Lio/realm/OrderedRealmCollectionImpl$RealmCollectionIterator;

    invoke-direct {v0, p0}, Lio/realm/OrderedRealmCollectionImpl$RealmCollectionIterator;-><init>(Lio/realm/OrderedRealmCollectionImpl;)V

    return-object v0
.end method

.method public last()Lio/realm/RealmModel;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TE;"
        }
    .end annotation

    .prologue
    .line 144
    .local p0, "this":Lio/realm/OrderedRealmCollectionImpl;, "Lio/realm/OrderedRealmCollectionImpl<TE;>;"
    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lio/realm/OrderedRealmCollectionImpl;->lastImpl(ZLio/realm/RealmModel;)Lio/realm/RealmModel;

    move-result-object v0

    return-object v0
.end method

.method public last(Lio/realm/RealmModel;)Lio/realm/RealmModel;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TE;)TE;"
        }
    .end annotation

    .prologue
    .line 152
    .local p0, "this":Lio/realm/OrderedRealmCollectionImpl;, "Lio/realm/OrderedRealmCollectionImpl<TE;>;"
    .local p1, "defaultValue":Lio/realm/RealmModel;, "TE;"
    const/4 v0, 0x0

    invoke-direct {p0, v0, p1}, Lio/realm/OrderedRealmCollectionImpl;->lastImpl(ZLio/realm/RealmModel;)Lio/realm/RealmModel;

    move-result-object v0

    return-object v0
.end method

.method public listIterator()Ljava/util/ListIterator;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ListIterator",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 215
    .local p0, "this":Lio/realm/OrderedRealmCollectionImpl;, "Lio/realm/OrderedRealmCollectionImpl<TE;>;"
    new-instance v0, Lio/realm/OrderedRealmCollectionImpl$RealmCollectionListIterator;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lio/realm/OrderedRealmCollectionImpl$RealmCollectionListIterator;-><init>(Lio/realm/OrderedRealmCollectionImpl;I)V

    return-object v0
.end method

.method public listIterator(I)Ljava/util/ListIterator;
    .locals 1
    .param p1, "location"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Ljava/util/ListIterator",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 230
    .local p0, "this":Lio/realm/OrderedRealmCollectionImpl;, "Lio/realm/OrderedRealmCollectionImpl<TE;>;"
    new-instance v0, Lio/realm/OrderedRealmCollectionImpl$RealmCollectionListIterator;

    invoke-direct {v0, p0, p1}, Lio/realm/OrderedRealmCollectionImpl$RealmCollectionListIterator;-><init>(Lio/realm/OrderedRealmCollectionImpl;I)V

    return-object v0
.end method

.method public max(Ljava/lang/String;)Ljava/lang/Number;
    .locals 4
    .param p1, "fieldName"    # Ljava/lang/String;

    .prologue
    .line 335
    .local p0, "this":Lio/realm/OrderedRealmCollectionImpl;, "Lio/realm/OrderedRealmCollectionImpl<TE;>;"
    iget-object v2, p0, Lio/realm/OrderedRealmCollectionImpl;->realm:Lio/realm/BaseRealm;

    invoke-virtual {v2}, Lio/realm/BaseRealm;->checkIfValid()V

    .line 336
    invoke-direct {p0, p1}, Lio/realm/OrderedRealmCollectionImpl;->getColumnIndexForSort(Ljava/lang/String;)J

    move-result-wide v0

    .line 337
    .local v0, "columnIndex":J
    iget-object v2, p0, Lio/realm/OrderedRealmCollectionImpl;->collection:Lio/realm/internal/Collection;

    sget-object v3, Lio/realm/internal/Collection$Aggregate;->MAXIMUM:Lio/realm/internal/Collection$Aggregate;

    invoke-virtual {v2, v3, v0, v1}, Lio/realm/internal/Collection;->aggregateNumber(Lio/realm/internal/Collection$Aggregate;J)Ljava/lang/Number;

    move-result-object v2

    return-object v2
.end method

.method public maxDate(Ljava/lang/String;)Ljava/util/Date;
    .locals 4
    .param p1, "fieldName"    # Ljava/lang/String;

    .prologue
    .line 352
    .local p0, "this":Lio/realm/OrderedRealmCollectionImpl;, "Lio/realm/OrderedRealmCollectionImpl<TE;>;"
    iget-object v2, p0, Lio/realm/OrderedRealmCollectionImpl;->realm:Lio/realm/BaseRealm;

    invoke-virtual {v2}, Lio/realm/BaseRealm;->checkIfValid()V

    .line 353
    invoke-direct {p0, p1}, Lio/realm/OrderedRealmCollectionImpl;->getColumnIndexForSort(Ljava/lang/String;)J

    move-result-wide v0

    .line 354
    .local v0, "columnIndex":J
    iget-object v2, p0, Lio/realm/OrderedRealmCollectionImpl;->collection:Lio/realm/internal/Collection;

    sget-object v3, Lio/realm/internal/Collection$Aggregate;->MAXIMUM:Lio/realm/internal/Collection$Aggregate;

    invoke-virtual {v2, v3, v0, v1}, Lio/realm/internal/Collection;->aggregateDate(Lio/realm/internal/Collection$Aggregate;J)Ljava/util/Date;

    move-result-object v2

    return-object v2
.end method

.method public min(Ljava/lang/String;)Ljava/lang/Number;
    .locals 4
    .param p1, "fieldName"    # Ljava/lang/String;

    .prologue
    .line 315
    .local p0, "this":Lio/realm/OrderedRealmCollectionImpl;, "Lio/realm/OrderedRealmCollectionImpl<TE;>;"
    iget-object v2, p0, Lio/realm/OrderedRealmCollectionImpl;->realm:Lio/realm/BaseRealm;

    invoke-virtual {v2}, Lio/realm/BaseRealm;->checkIfValid()V

    .line 316
    invoke-direct {p0, p1}, Lio/realm/OrderedRealmCollectionImpl;->getColumnIndexForSort(Ljava/lang/String;)J

    move-result-wide v0

    .line 317
    .local v0, "columnIndex":J
    iget-object v2, p0, Lio/realm/OrderedRealmCollectionImpl;->collection:Lio/realm/internal/Collection;

    sget-object v3, Lio/realm/internal/Collection$Aggregate;->MINIMUM:Lio/realm/internal/Collection$Aggregate;

    invoke-virtual {v2, v3, v0, v1}, Lio/realm/internal/Collection;->aggregateNumber(Lio/realm/internal/Collection$Aggregate;J)Ljava/lang/Number;

    move-result-object v2

    return-object v2
.end method

.method public minDate(Ljava/lang/String;)Ljava/util/Date;
    .locals 4
    .param p1, "fieldName"    # Ljava/lang/String;

    .prologue
    .line 325
    .local p0, "this":Lio/realm/OrderedRealmCollectionImpl;, "Lio/realm/OrderedRealmCollectionImpl<TE;>;"
    iget-object v2, p0, Lio/realm/OrderedRealmCollectionImpl;->realm:Lio/realm/BaseRealm;

    invoke-virtual {v2}, Lio/realm/BaseRealm;->checkIfValid()V

    .line 326
    invoke-direct {p0, p1}, Lio/realm/OrderedRealmCollectionImpl;->getColumnIndexForSort(Ljava/lang/String;)J

    move-result-wide v0

    .line 327
    .local v0, "columnIndex":J
    iget-object v2, p0, Lio/realm/OrderedRealmCollectionImpl;->collection:Lio/realm/internal/Collection;

    sget-object v3, Lio/realm/internal/Collection$Aggregate;->MINIMUM:Lio/realm/internal/Collection$Aggregate;

    invoke-virtual {v2, v3, v0, v1}, Lio/realm/internal/Collection;->aggregateDate(Lio/realm/internal/Collection$Aggregate;J)Ljava/util/Date;

    move-result-object v2

    return-object v2
.end method

.method public remove(I)Lio/realm/RealmModel;
    .locals 2
    .param p1, "index"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)TE;"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 390
    .local p0, "this":Lio/realm/OrderedRealmCollectionImpl;, "Lio/realm/OrderedRealmCollectionImpl<TE;>;"
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "This method is not supported by \'RealmResults\' or \'OrderedRealmCollectionSnapshot\'."

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public bridge synthetic remove(I)Ljava/lang/Object;
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 19
    .local p0, "this":Lio/realm/OrderedRealmCollectionImpl;, "Lio/realm/OrderedRealmCollectionImpl<TE;>;"
    invoke-virtual {p0, p1}, Lio/realm/OrderedRealmCollectionImpl;->remove(I)Lio/realm/RealmModel;

    move-result-object v0

    return-object v0
.end method

.method public remove(Ljava/lang/Object;)Z
    .locals 2
    .param p1, "object"    # Ljava/lang/Object;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 401
    .local p0, "this":Lio/realm/OrderedRealmCollectionImpl;, "Lio/realm/OrderedRealmCollectionImpl<TE;>;"
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "This method is not supported by \'RealmResults\' or \'OrderedRealmCollectionSnapshot\'."

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public removeAll(Ljava/util/Collection;)Z
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<*>;)Z"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 412
    .local p0, "this":Lio/realm/OrderedRealmCollectionImpl;, "Lio/realm/OrderedRealmCollectionImpl<TE;>;"
    .local p1, "collection":Ljava/util/Collection;, "Ljava/util/Collection<*>;"
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "This method is not supported by \'RealmResults\' or \'OrderedRealmCollectionSnapshot\'."

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public retainAll(Ljava/util/Collection;)Z
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<*>;)Z"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 434
    .local p0, "this":Lio/realm/OrderedRealmCollectionImpl;, "Lio/realm/OrderedRealmCollectionImpl<TE;>;"
    .local p1, "collection":Ljava/util/Collection;, "Ljava/util/Collection<*>;"
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "This method is not supported by \'RealmResults\' or \'OrderedRealmCollectionSnapshot\'."

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public set(ILio/realm/RealmModel;)Lio/realm/RealmModel;
    .locals 2
    .param p1, "location"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(ITE;)TE;"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 423
    .local p0, "this":Lio/realm/OrderedRealmCollectionImpl;, "Lio/realm/OrderedRealmCollectionImpl<TE;>;"
    .local p2, "object":Lio/realm/RealmModel;, "TE;"
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "This method is not supported by \'RealmResults\' or \'OrderedRealmCollectionSnapshot\'."

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public bridge synthetic set(ILjava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 19
    .local p0, "this":Lio/realm/OrderedRealmCollectionImpl;, "Lio/realm/OrderedRealmCollectionImpl<TE;>;"
    check-cast p2, Lio/realm/RealmModel;

    invoke-virtual {p0, p1, p2}, Lio/realm/OrderedRealmCollectionImpl;->set(ILio/realm/RealmModel;)Lio/realm/RealmModel;

    move-result-object v0

    return-object v0
.end method

.method public size()I
    .locals 4

    .prologue
    .line 303
    .local p0, "this":Lio/realm/OrderedRealmCollectionImpl;, "Lio/realm/OrderedRealmCollectionImpl<TE;>;"
    invoke-virtual {p0}, Lio/realm/OrderedRealmCollectionImpl;->isLoaded()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 304
    iget-object v2, p0, Lio/realm/OrderedRealmCollectionImpl;->collection:Lio/realm/internal/Collection;

    invoke-virtual {v2}, Lio/realm/internal/Collection;->size()J

    move-result-wide v0

    .line 305
    .local v0, "size":J
    const-wide/32 v2, 0x7fffffff

    cmp-long v2, v0, v2

    if-lez v2, :cond_0

    const v2, 0x7fffffff

    .line 307
    .end local v0    # "size":J
    :goto_0
    return v2

    .line 305
    .restart local v0    # "size":J
    :cond_0
    long-to-int v2, v0

    goto :goto_0

    .line 307
    .end local v0    # "size":J
    :cond_1
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public sort(Ljava/lang/String;)Lio/realm/RealmResults;
    .locals 4
    .param p1, "fieldName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lio/realm/RealmResults",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 255
    .local p0, "this":Lio/realm/OrderedRealmCollectionImpl;, "Lio/realm/OrderedRealmCollectionImpl<TE;>;"
    iget-object v2, p0, Lio/realm/OrderedRealmCollectionImpl;->collection:Lio/realm/internal/Collection;

    .line 256
    invoke-virtual {v2}, Lio/realm/internal/Collection;->getTable()Lio/realm/internal/Table;

    move-result-object v2

    sget-object v3, Lio/realm/Sort;->ASCENDING:Lio/realm/Sort;

    invoke-static {v2, p1, v3}, Lio/realm/internal/SortDescriptor;->getInstanceForSort(Lio/realm/internal/Table;Ljava/lang/String;Lio/realm/Sort;)Lio/realm/internal/SortDescriptor;

    move-result-object v0

    .line 258
    .local v0, "sortDescriptor":Lio/realm/internal/SortDescriptor;
    iget-object v2, p0, Lio/realm/OrderedRealmCollectionImpl;->collection:Lio/realm/internal/Collection;

    invoke-virtual {v2, v0}, Lio/realm/internal/Collection;->sort(Lio/realm/internal/SortDescriptor;)Lio/realm/internal/Collection;

    move-result-object v1

    .line 259
    .local v1, "sortedCollection":Lio/realm/internal/Collection;
    invoke-virtual {p0, v1}, Lio/realm/OrderedRealmCollectionImpl;->createLoadedResults(Lio/realm/internal/Collection;)Lio/realm/RealmResults;

    move-result-object v2

    return-object v2
.end method

.method public sort(Ljava/lang/String;Lio/realm/Sort;)Lio/realm/RealmResults;
    .locals 3
    .param p1, "fieldName"    # Ljava/lang/String;
    .param p2, "sortOrder"    # Lio/realm/Sort;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lio/realm/Sort;",
            ")",
            "Lio/realm/RealmResults",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 267
    .local p0, "this":Lio/realm/OrderedRealmCollectionImpl;, "Lio/realm/OrderedRealmCollectionImpl<TE;>;"
    iget-object v2, p0, Lio/realm/OrderedRealmCollectionImpl;->collection:Lio/realm/internal/Collection;

    .line 268
    invoke-virtual {v2}, Lio/realm/internal/Collection;->getTable()Lio/realm/internal/Table;

    move-result-object v2

    invoke-static {v2, p1, p2}, Lio/realm/internal/SortDescriptor;->getInstanceForSort(Lio/realm/internal/Table;Ljava/lang/String;Lio/realm/Sort;)Lio/realm/internal/SortDescriptor;

    move-result-object v0

    .line 270
    .local v0, "sortDescriptor":Lio/realm/internal/SortDescriptor;
    iget-object v2, p0, Lio/realm/OrderedRealmCollectionImpl;->collection:Lio/realm/internal/Collection;

    invoke-virtual {v2, v0}, Lio/realm/internal/Collection;->sort(Lio/realm/internal/SortDescriptor;)Lio/realm/internal/Collection;

    move-result-object v1

    .line 271
    .local v1, "sortedCollection":Lio/realm/internal/Collection;
    invoke-virtual {p0, v1}, Lio/realm/OrderedRealmCollectionImpl;->createLoadedResults(Lio/realm/internal/Collection;)Lio/realm/RealmResults;

    move-result-object v2

    return-object v2
.end method

.method public sort(Ljava/lang/String;Lio/realm/Sort;Ljava/lang/String;Lio/realm/Sort;)Lio/realm/RealmResults;
    .locals 4
    .param p1, "fieldName1"    # Ljava/lang/String;
    .param p2, "sortOrder1"    # Lio/realm/Sort;
    .param p3, "fieldName2"    # Ljava/lang/String;
    .param p4, "sortOrder2"    # Lio/realm/Sort;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lio/realm/Sort;",
            "Ljava/lang/String;",
            "Lio/realm/Sort;",
            ")",
            "Lio/realm/RealmResults",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .local p0, "this":Lio/realm/OrderedRealmCollectionImpl;, "Lio/realm/OrderedRealmCollectionImpl<TE;>;"
    const/4 v1, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 291
    new-array v0, v1, [Ljava/lang/String;

    aput-object p1, v0, v2

    aput-object p3, v0, v3

    new-array v1, v1, [Lio/realm/Sort;

    aput-object p2, v1, v2

    aput-object p4, v1, v3

    invoke-virtual {p0, v0, v1}, Lio/realm/OrderedRealmCollectionImpl;->sort([Ljava/lang/String;[Lio/realm/Sort;)Lio/realm/RealmResults;

    move-result-object v0

    return-object v0
.end method

.method public sort([Ljava/lang/String;[Lio/realm/Sort;)Lio/realm/RealmResults;
    .locals 3
    .param p1, "fieldNames"    # [Ljava/lang/String;
    .param p2, "sortOrders"    # [Lio/realm/Sort;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "Ljava/lang/String;",
            "[",
            "Lio/realm/Sort;",
            ")",
            "Lio/realm/RealmResults",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 279
    .local p0, "this":Lio/realm/OrderedRealmCollectionImpl;, "Lio/realm/OrderedRealmCollectionImpl<TE;>;"
    iget-object v2, p0, Lio/realm/OrderedRealmCollectionImpl;->collection:Lio/realm/internal/Collection;

    .line 280
    invoke-virtual {v2}, Lio/realm/internal/Collection;->getTable()Lio/realm/internal/Table;

    move-result-object v2

    invoke-static {v2, p1, p2}, Lio/realm/internal/SortDescriptor;->getInstanceForSort(Lio/realm/internal/Table;[Ljava/lang/String;[Lio/realm/Sort;)Lio/realm/internal/SortDescriptor;

    move-result-object v0

    .line 282
    .local v0, "sortDescriptor":Lio/realm/internal/SortDescriptor;
    iget-object v2, p0, Lio/realm/OrderedRealmCollectionImpl;->collection:Lio/realm/internal/Collection;

    invoke-virtual {v2, v0}, Lio/realm/internal/Collection;->sort(Lio/realm/internal/SortDescriptor;)Lio/realm/internal/Collection;

    move-result-object v1

    .line 283
    .local v1, "sortedCollection":Lio/realm/internal/Collection;
    invoke-virtual {p0, v1}, Lio/realm/OrderedRealmCollectionImpl;->createLoadedResults(Lio/realm/internal/Collection;)Lio/realm/RealmResults;

    move-result-object v2

    return-object v2
.end method

.method public sum(Ljava/lang/String;)Ljava/lang/Number;
    .locals 4
    .param p1, "fieldName"    # Ljava/lang/String;

    .prologue
    .line 363
    .local p0, "this":Lio/realm/OrderedRealmCollectionImpl;, "Lio/realm/OrderedRealmCollectionImpl<TE;>;"
    iget-object v2, p0, Lio/realm/OrderedRealmCollectionImpl;->realm:Lio/realm/BaseRealm;

    invoke-virtual {v2}, Lio/realm/BaseRealm;->checkIfValid()V

    .line 364
    invoke-direct {p0, p1}, Lio/realm/OrderedRealmCollectionImpl;->getColumnIndexForSort(Ljava/lang/String;)J

    move-result-wide v0

    .line 365
    .local v0, "columnIndex":J
    iget-object v2, p0, Lio/realm/OrderedRealmCollectionImpl;->collection:Lio/realm/internal/Collection;

    sget-object v3, Lio/realm/internal/Collection$Aggregate;->SUM:Lio/realm/internal/Collection$Aggregate;

    invoke-virtual {v2, v3, v0, v1}, Lio/realm/internal/Collection;->aggregateNumber(Lio/realm/internal/Collection$Aggregate;J)Ljava/lang/Number;

    move-result-object v2

    return-object v2
.end method
