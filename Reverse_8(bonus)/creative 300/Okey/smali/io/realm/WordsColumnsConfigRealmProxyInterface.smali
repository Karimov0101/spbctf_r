.class public interface abstract Lio/realm/WordsColumnsConfigRealmProxyInterface;
.super Ljava/lang/Object;
.source "WordsColumnsConfigRealmProxyInterface.java"


# virtual methods
.method public abstract realmGet$columnCount()I
.end method

.method public abstract realmGet$id()I
.end method

.method public abstract realmGet$rowCount()I
.end method

.method public abstract realmGet$speed()I
.end method

.method public abstract realmGet$trainingDuration()J
.end method

.method public abstract realmGet$wordsPerItem()I
.end method

.method public abstract realmSet$columnCount(I)V
.end method

.method public abstract realmSet$id(I)V
.end method

.method public abstract realmSet$rowCount(I)V
.end method

.method public abstract realmSet$speed(I)V
.end method

.method public abstract realmSet$trainingDuration(J)V
.end method

.method public abstract realmSet$wordsPerItem(I)V
.end method
