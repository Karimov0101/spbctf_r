.class public Lio/realm/RememberNumberResultRealmProxy;
.super Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberResult;
.source "RememberNumberResultRealmProxy.java"

# interfaces
.implements Lio/realm/internal/RealmObjectProxy;
.implements Lio/realm/RememberNumberResultRealmProxyInterface;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lio/realm/RememberNumberResultRealmProxy$RememberNumberResultColumnInfo;
    }
.end annotation


# static fields
.field private static final FIELD_NAMES:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private columnInfo:Lio/realm/RememberNumberResultRealmProxy$RememberNumberResultColumnInfo;

.field private proxyState:Lio/realm/ProxyState;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/realm/ProxyState",
            "<",
            "Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberResult;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 77
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 78
    .local v0, "fieldNames":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const-string v1, "id"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 79
    const-string v1, "score"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 80
    const-string v1, "unixTime"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 81
    const-string v1, "config"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 82
    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    sput-object v1, Lio/realm/RememberNumberResultRealmProxy;->FIELD_NAMES:Ljava/util/List;

    .line 83
    return-void
.end method

.method constructor <init>()V
    .locals 1

    .prologue
    .line 85
    invoke-direct {p0}, Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberResult;-><init>()V

    .line 86
    iget-object v0, p0, Lio/realm/RememberNumberResultRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v0}, Lio/realm/ProxyState;->setConstructionFinished()V

    .line 87
    return-void
.end method

.method public static copy(Lio/realm/Realm;Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberResult;ZLjava/util/Map;)Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberResult;
    .locals 8
    .param p0, "realm"    # Lio/realm/Realm;
    .param p1, "newObject"    # Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberResult;
    .param p2, "update"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/realm/Realm;",
            "Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberResult;",
            "Z",
            "Ljava/util/Map",
            "<",
            "Lio/realm/RealmModel;",
            "Lio/realm/internal/RealmObjectProxy;",
            ">;)",
            "Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberResult;"
        }
    .end annotation

    .prologue
    .line 478
    .local p3, "cache":Ljava/util/Map;, "Ljava/util/Map<Lio/realm/RealmModel;Lio/realm/internal/RealmObjectProxy;>;"
    invoke-interface {p3, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lio/realm/internal/RealmObjectProxy;

    .line 479
    .local v1, "cachedRealmObject":Lio/realm/internal/RealmObjectProxy;
    if-eqz v1, :cond_0

    .line 480
    check-cast v1, Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberResult;

    .line 499
    .end local v1    # "cachedRealmObject":Lio/realm/internal/RealmObjectProxy;
    .end local p1    # "newObject":Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberResult;
    :goto_0
    return-object v1

    .line 483
    .restart local v1    # "cachedRealmObject":Lio/realm/internal/RealmObjectProxy;
    .restart local p1    # "newObject":Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberResult;
    :cond_0
    const-class v5, Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberResult;

    move-object v4, p1

    check-cast v4, Lio/realm/RememberNumberResultRealmProxyInterface;

    invoke-interface {v4}, Lio/realm/RememberNumberResultRealmProxyInterface;->realmGet$id()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    const/4 v6, 0x0

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v7

    invoke-virtual {p0, v5, v4, v6, v7}, Lio/realm/Realm;->createObjectInternal(Ljava/lang/Class;Ljava/lang/Object;ZLjava/util/List;)Lio/realm/RealmModel;

    move-result-object v3

    check-cast v3, Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberResult;

    .local v3, "realmObject":Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberResult;
    move-object v4, v3

    .line 484
    check-cast v4, Lio/realm/internal/RealmObjectProxy;

    invoke-interface {p3, p1, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-object v4, v3

    .line 485
    check-cast v4, Lio/realm/RememberNumberResultRealmProxyInterface;

    move-object v5, p1

    check-cast v5, Lio/realm/RememberNumberResultRealmProxyInterface;

    invoke-interface {v5}, Lio/realm/RememberNumberResultRealmProxyInterface;->realmGet$score()I

    move-result v5

    invoke-interface {v4, v5}, Lio/realm/RememberNumberResultRealmProxyInterface;->realmSet$score(I)V

    move-object v4, v3

    .line 486
    check-cast v4, Lio/realm/RememberNumberResultRealmProxyInterface;

    move-object v5, p1

    check-cast v5, Lio/realm/RememberNumberResultRealmProxyInterface;

    invoke-interface {v5}, Lio/realm/RememberNumberResultRealmProxyInterface;->realmGet$unixTime()J

    move-result-wide v6

    invoke-interface {v4, v6, v7}, Lio/realm/RememberNumberResultRealmProxyInterface;->realmSet$unixTime(J)V

    .line 488
    check-cast p1, Lio/realm/RememberNumberResultRealmProxyInterface;

    .end local p1    # "newObject":Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberResult;
    invoke-interface {p1}, Lio/realm/RememberNumberResultRealmProxyInterface;->realmGet$config()Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberConfig;

    move-result-object v2

    .line 489
    .local v2, "configObj":Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberConfig;
    if-eqz v2, :cond_2

    .line 490
    invoke-interface {p3, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberConfig;

    .line 491
    .local v0, "cacheconfig":Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberConfig;
    if-eqz v0, :cond_1

    move-object v4, v3

    .line 492
    check-cast v4, Lio/realm/RememberNumberResultRealmProxyInterface;

    invoke-interface {v4, v0}, Lio/realm/RememberNumberResultRealmProxyInterface;->realmSet$config(Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberConfig;)V

    .end local v0    # "cacheconfig":Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberConfig;
    :goto_1
    move-object v1, v3

    .line 499
    goto :goto_0

    .restart local v0    # "cacheconfig":Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberConfig;
    :cond_1
    move-object v4, v3

    .line 494
    check-cast v4, Lio/realm/RememberNumberResultRealmProxyInterface;

    invoke-static {p0, v2, p2, p3}, Lio/realm/RememberNumberConfigRealmProxy;->copyOrUpdate(Lio/realm/Realm;Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberConfig;ZLjava/util/Map;)Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberConfig;

    move-result-object v5

    invoke-interface {v4, v5}, Lio/realm/RememberNumberResultRealmProxyInterface;->realmSet$config(Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberConfig;)V

    goto :goto_1

    .end local v0    # "cacheconfig":Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberConfig;
    :cond_2
    move-object v4, v3

    .line 497
    check-cast v4, Lio/realm/RememberNumberResultRealmProxyInterface;

    const/4 v5, 0x0

    invoke-interface {v4, v5}, Lio/realm/RememberNumberResultRealmProxyInterface;->realmSet$config(Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberConfig;)V

    goto :goto_1
.end method

.method public static copyOrUpdate(Lio/realm/Realm;Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberResult;ZLjava/util/Map;)Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberResult;
    .locals 19
    .param p0, "realm"    # Lio/realm/Realm;
    .param p1, "object"    # Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberResult;
    .param p2, "update"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/realm/Realm;",
            "Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberResult;",
            "Z",
            "Ljava/util/Map",
            "<",
            "Lio/realm/RealmModel;",
            "Lio/realm/internal/RealmObjectProxy;",
            ">;)",
            "Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberResult;"
        }
    .end annotation

    .prologue
    .line 439
    .local p3, "cache":Ljava/util/Map;, "Ljava/util/Map<Lio/realm/RealmModel;Lio/realm/internal/RealmObjectProxy;>;"
    move-object/from16 v0, p1

    instance-of v5, v0, Lio/realm/internal/RealmObjectProxy;

    if-eqz v5, :cond_0

    move-object/from16 v5, p1

    check-cast v5, Lio/realm/internal/RealmObjectProxy;

    invoke-interface {v5}, Lio/realm/internal/RealmObjectProxy;->realmGet$proxyState()Lio/realm/ProxyState;

    move-result-object v5

    invoke-virtual {v5}, Lio/realm/ProxyState;->getRealm$realm()Lio/realm/BaseRealm;

    move-result-object v5

    if-eqz v5, :cond_0

    move-object/from16 v5, p1

    check-cast v5, Lio/realm/internal/RealmObjectProxy;

    invoke-interface {v5}, Lio/realm/internal/RealmObjectProxy;->realmGet$proxyState()Lio/realm/ProxyState;

    move-result-object v5

    invoke-virtual {v5}, Lio/realm/ProxyState;->getRealm$realm()Lio/realm/BaseRealm;

    move-result-object v5

    iget-wide v6, v5, Lio/realm/BaseRealm;->threadId:J

    move-object/from16 v0, p0

    iget-wide v8, v0, Lio/realm/Realm;->threadId:J

    cmp-long v5, v6, v8

    if-eqz v5, :cond_0

    .line 440
    new-instance v5, Ljava/lang/IllegalArgumentException;

    const-string v6, "Objects which belong to Realm instances in other threads cannot be copied into this Realm instance."

    invoke-direct {v5, v6}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v5

    .line 442
    :cond_0
    move-object/from16 v0, p1

    instance-of v5, v0, Lio/realm/internal/RealmObjectProxy;

    if-eqz v5, :cond_1

    move-object/from16 v5, p1

    check-cast v5, Lio/realm/internal/RealmObjectProxy;

    invoke-interface {v5}, Lio/realm/internal/RealmObjectProxy;->realmGet$proxyState()Lio/realm/ProxyState;

    move-result-object v5

    invoke-virtual {v5}, Lio/realm/ProxyState;->getRealm$realm()Lio/realm/BaseRealm;

    move-result-object v5

    if-eqz v5, :cond_1

    move-object/from16 v5, p1

    check-cast v5, Lio/realm/internal/RealmObjectProxy;

    invoke-interface {v5}, Lio/realm/internal/RealmObjectProxy;->realmGet$proxyState()Lio/realm/ProxyState;

    move-result-object v5

    invoke-virtual {v5}, Lio/realm/ProxyState;->getRealm$realm()Lio/realm/BaseRealm;

    move-result-object v5

    invoke-virtual {v5}, Lio/realm/BaseRealm;->getPath()Ljava/lang/String;

    move-result-object v5

    invoke-virtual/range {p0 .. p0}, Lio/realm/Realm;->getPath()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 472
    .end local p1    # "object":Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberResult;
    :goto_0
    return-object p1

    .line 445
    .restart local p1    # "object":Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberResult;
    :cond_1
    sget-object v5, Lio/realm/BaseRealm;->objectContext:Lio/realm/BaseRealm$ThreadLocalRealmObjectContext;

    invoke-virtual {v5}, Lio/realm/BaseRealm$ThreadLocalRealmObjectContext;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lio/realm/BaseRealm$RealmObjectContext;

    .line 446
    .local v4, "objectContext":Lio/realm/BaseRealm$RealmObjectContext;
    move-object/from16 v0, p3

    move-object/from16 v1, p1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lio/realm/internal/RealmObjectProxy;

    .line 447
    .local v10, "cachedRealmObject":Lio/realm/internal/RealmObjectProxy;
    if-eqz v10, :cond_2

    .line 448
    check-cast v10, Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberResult;

    .end local v10    # "cachedRealmObject":Lio/realm/internal/RealmObjectProxy;
    move-object/from16 p1, v10

    goto :goto_0

    .line 450
    .restart local v10    # "cachedRealmObject":Lio/realm/internal/RealmObjectProxy;
    :cond_2
    const/4 v14, 0x0

    .line 451
    .local v14, "realmObject":Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberResult;
    move/from16 v11, p2

    .line 452
    .local v11, "canUpdate":Z
    if-eqz v11, :cond_3

    .line 453
    const-class v5, Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberResult;

    move-object/from16 v0, p0

    invoke-virtual {v0, v5}, Lio/realm/Realm;->getTable(Ljava/lang/Class;)Lio/realm/internal/Table;

    move-result-object v18

    .line 454
    .local v18, "table":Lio/realm/internal/Table;
    invoke-virtual/range {v18 .. v18}, Lio/realm/internal/Table;->getPrimaryKey()J

    move-result-wide v12

    .local v12, "pkColumnIndex":J
    move-object/from16 v5, p1

    .line 455
    check-cast v5, Lio/realm/RememberNumberResultRealmProxyInterface;

    invoke-interface {v5}, Lio/realm/RememberNumberResultRealmProxyInterface;->realmGet$id()I

    move-result v5

    int-to-long v6, v5

    move-object/from16 v0, v18

    invoke-virtual {v0, v12, v13, v6, v7}, Lio/realm/internal/Table;->findFirstLong(JJ)J

    move-result-wide v16

    .line 456
    .local v16, "rowIndex":J
    const-wide/16 v6, -0x1

    cmp-long v5, v16, v6

    if-eqz v5, :cond_4

    .line 458
    :try_start_0
    move-object/from16 v0, v18

    move-wide/from16 v1, v16

    invoke-virtual {v0, v1, v2}, Lio/realm/internal/Table;->getUncheckedRow(J)Lio/realm/internal/UncheckedRow;

    move-result-object v6

    move-object/from16 v0, p0

    iget-object v5, v0, Lio/realm/Realm;->schema:Lio/realm/RealmSchema;

    const-class v7, Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberResult;

    invoke-virtual {v5, v7}, Lio/realm/RealmSchema;->getColumnInfo(Ljava/lang/Class;)Lio/realm/internal/ColumnInfo;

    move-result-object v7

    const/4 v8, 0x0

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v9

    move-object/from16 v5, p0

    invoke-virtual/range {v4 .. v9}, Lio/realm/BaseRealm$RealmObjectContext;->set(Lio/realm/BaseRealm;Lio/realm/internal/Row;Lio/realm/internal/ColumnInfo;ZLjava/util/List;)V

    .line 459
    new-instance v15, Lio/realm/RememberNumberResultRealmProxy;

    invoke-direct {v15}, Lio/realm/RememberNumberResultRealmProxy;-><init>()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 460
    .end local v14    # "realmObject":Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberResult;
    .local v15, "realmObject":Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberResult;
    :try_start_1
    move-object v0, v15

    check-cast v0, Lio/realm/internal/RealmObjectProxy;

    move-object v5, v0

    move-object/from16 v0, p3

    move-object/from16 v1, p1

    invoke-interface {v0, v1, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 462
    invoke-virtual {v4}, Lio/realm/BaseRealm$RealmObjectContext;->clear()V

    move-object v14, v15

    .line 469
    .end local v12    # "pkColumnIndex":J
    .end local v15    # "realmObject":Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberResult;
    .end local v16    # "rowIndex":J
    .end local v18    # "table":Lio/realm/internal/Table;
    .restart local v14    # "realmObject":Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberResult;
    :cond_3
    :goto_1
    if-eqz v11, :cond_5

    .line 470
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p3

    invoke-static {v0, v14, v1, v2}, Lio/realm/RememberNumberResultRealmProxy;->update(Lio/realm/Realm;Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberResult;Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberResult;Ljava/util/Map;)Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberResult;

    move-result-object p1

    goto :goto_0

    .line 462
    .restart local v12    # "pkColumnIndex":J
    .restart local v16    # "rowIndex":J
    .restart local v18    # "table":Lio/realm/internal/Table;
    :catchall_0
    move-exception v5

    :goto_2
    invoke-virtual {v4}, Lio/realm/BaseRealm$RealmObjectContext;->clear()V

    throw v5

    .line 465
    :cond_4
    const/4 v11, 0x0

    goto :goto_1

    .line 472
    .end local v12    # "pkColumnIndex":J
    .end local v16    # "rowIndex":J
    .end local v18    # "table":Lio/realm/internal/Table;
    :cond_5
    invoke-static/range {p0 .. p3}, Lio/realm/RememberNumberResultRealmProxy;->copy(Lio/realm/Realm;Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberResult;ZLjava/util/Map;)Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberResult;

    move-result-object p1

    goto/16 :goto_0

    .line 462
    .end local v14    # "realmObject":Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberResult;
    .restart local v12    # "pkColumnIndex":J
    .restart local v15    # "realmObject":Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberResult;
    .restart local v16    # "rowIndex":J
    .restart local v18    # "table":Lio/realm/internal/Table;
    :catchall_1
    move-exception v5

    move-object v14, v15

    .end local v15    # "realmObject":Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberResult;
    .restart local v14    # "realmObject":Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberResult;
    goto :goto_2
.end method

.method public static createDetachedCopy(Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberResult;IILjava/util/Map;)Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberResult;
    .locals 6
    .param p0, "realmObject"    # Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberResult;
    .param p1, "currentDepth"    # I
    .param p2, "maxDepth"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberResult;",
            "II",
            "Ljava/util/Map",
            "<",
            "Lio/realm/RealmModel;",
            "Lio/realm/internal/RealmObjectProxy$CacheData",
            "<",
            "Lio/realm/RealmModel;",
            ">;>;)",
            "Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberResult;"
        }
    .end annotation

    .prologue
    .line 648
    .local p3, "cache":Ljava/util/Map;, "Ljava/util/Map<Lio/realm/RealmModel;Lio/realm/internal/RealmObjectProxy$CacheData<Lio/realm/RealmModel;>;>;"
    if-gt p1, p2, :cond_0

    if-nez p0, :cond_1

    .line 649
    :cond_0
    const/4 v2, 0x0

    .line 671
    .end local p0    # "realmObject":Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberResult;
    :goto_0
    return-object v2

    .line 651
    .restart local p0    # "realmObject":Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberResult;
    :cond_1
    invoke-interface {p3, p0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/realm/internal/RealmObjectProxy$CacheData;

    .line 653
    .local v0, "cachedObject":Lio/realm/internal/RealmObjectProxy$CacheData;, "Lio/realm/internal/RealmObjectProxy$CacheData<Lio/realm/RealmModel;>;"
    if-eqz v0, :cond_3

    .line 655
    iget v2, v0, Lio/realm/internal/RealmObjectProxy$CacheData;->minDepth:I

    if-lt p1, v2, :cond_2

    .line 656
    iget-object v2, v0, Lio/realm/internal/RealmObjectProxy$CacheData;->object:Lio/realm/RealmModel;

    check-cast v2, Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberResult;

    goto :goto_0

    .line 658
    :cond_2
    iget-object v1, v0, Lio/realm/internal/RealmObjectProxy$CacheData;->object:Lio/realm/RealmModel;

    check-cast v1, Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberResult;

    .line 659
    .local v1, "unmanagedObject":Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberResult;
    iput p1, v0, Lio/realm/internal/RealmObjectProxy$CacheData;->minDepth:I

    :goto_1
    move-object v2, v1

    .line 665
    check-cast v2, Lio/realm/RememberNumberResultRealmProxyInterface;

    move-object v3, p0

    check-cast v3, Lio/realm/RememberNumberResultRealmProxyInterface;

    invoke-interface {v3}, Lio/realm/RememberNumberResultRealmProxyInterface;->realmGet$id()I

    move-result v3

    invoke-interface {v2, v3}, Lio/realm/RememberNumberResultRealmProxyInterface;->realmSet$id(I)V

    move-object v2, v1

    .line 666
    check-cast v2, Lio/realm/RememberNumberResultRealmProxyInterface;

    move-object v3, p0

    check-cast v3, Lio/realm/RememberNumberResultRealmProxyInterface;

    invoke-interface {v3}, Lio/realm/RememberNumberResultRealmProxyInterface;->realmGet$score()I

    move-result v3

    invoke-interface {v2, v3}, Lio/realm/RememberNumberResultRealmProxyInterface;->realmSet$score(I)V

    move-object v2, v1

    .line 667
    check-cast v2, Lio/realm/RememberNumberResultRealmProxyInterface;

    move-object v3, p0

    check-cast v3, Lio/realm/RememberNumberResultRealmProxyInterface;

    invoke-interface {v3}, Lio/realm/RememberNumberResultRealmProxyInterface;->realmGet$unixTime()J

    move-result-wide v4

    invoke-interface {v2, v4, v5}, Lio/realm/RememberNumberResultRealmProxyInterface;->realmSet$unixTime(J)V

    move-object v2, v1

    .line 670
    check-cast v2, Lio/realm/RememberNumberResultRealmProxyInterface;

    check-cast p0, Lio/realm/RememberNumberResultRealmProxyInterface;

    .end local p0    # "realmObject":Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberResult;
    invoke-interface {p0}, Lio/realm/RememberNumberResultRealmProxyInterface;->realmGet$config()Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberConfig;

    move-result-object v3

    add-int/lit8 v4, p1, 0x1

    invoke-static {v3, v4, p2, p3}, Lio/realm/RememberNumberConfigRealmProxy;->createDetachedCopy(Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberConfig;IILjava/util/Map;)Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberConfig;

    move-result-object v3

    invoke-interface {v2, v3}, Lio/realm/RememberNumberResultRealmProxyInterface;->realmSet$config(Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberConfig;)V

    move-object v2, v1

    .line 671
    goto :goto_0

    .line 662
    .end local v1    # "unmanagedObject":Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberResult;
    .restart local p0    # "realmObject":Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberResult;
    :cond_3
    new-instance v1, Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberResult;

    invoke-direct {v1}, Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberResult;-><init>()V

    .line 663
    .restart local v1    # "unmanagedObject":Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberResult;
    new-instance v2, Lio/realm/internal/RealmObjectProxy$CacheData;

    invoke-direct {v2, p1, v1}, Lio/realm/internal/RealmObjectProxy$CacheData;-><init>(ILio/realm/RealmModel;)V

    invoke-interface {p3, p0, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1
.end method

.method public static createOrUpdateUsingJsonObject(Lio/realm/Realm;Lorg/json/JSONObject;Z)Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberResult;
    .locals 16
    .param p0, "realm"    # Lio/realm/Realm;
    .param p1, "json"    # Lorg/json/JSONObject;
    .param p2, "update"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .prologue
    .line 329
    new-instance v9, Ljava/util/ArrayList;

    const/4 v3, 0x1

    invoke-direct {v9, v3}, Ljava/util/ArrayList;-><init>(I)V

    .line 330
    .local v9, "excludeFields":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const/4 v10, 0x0

    .line 331
    .local v10, "obj":Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberResult;
    if-eqz p2, :cond_1

    .line 332
    const-class v3, Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberResult;

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Lio/realm/Realm;->getTable(Ljava/lang/Class;)Lio/realm/internal/Table;

    move-result-object v11

    .line 333
    .local v11, "table":Lio/realm/internal/Table;
    invoke-virtual {v11}, Lio/realm/internal/Table;->getPrimaryKey()J

    move-result-wide v12

    .line 334
    .local v12, "pkColumnIndex":J
    const-wide/16 v14, -0x1

    .line 335
    .local v14, "rowIndex":J
    const-string v3, "id"

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Lorg/json/JSONObject;->isNull(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 336
    const-string v3, "id"

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Lorg/json/JSONObject;->getLong(Ljava/lang/String;)J

    move-result-wide v4

    invoke-virtual {v11, v12, v13, v4, v5}, Lio/realm/internal/Table;->findFirstLong(JJ)J

    move-result-wide v14

    .line 338
    :cond_0
    const-wide/16 v4, -0x1

    cmp-long v3, v14, v4

    if-eqz v3, :cond_1

    .line 339
    sget-object v3, Lio/realm/BaseRealm;->objectContext:Lio/realm/BaseRealm$ThreadLocalRealmObjectContext;

    invoke-virtual {v3}, Lio/realm/BaseRealm$ThreadLocalRealmObjectContext;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lio/realm/BaseRealm$RealmObjectContext;

    .line 341
    .local v2, "objectContext":Lio/realm/BaseRealm$RealmObjectContext;
    :try_start_0
    invoke-virtual {v11, v14, v15}, Lio/realm/internal/Table;->getUncheckedRow(J)Lio/realm/internal/UncheckedRow;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v3, v0, Lio/realm/Realm;->schema:Lio/realm/RealmSchema;

    const-class v5, Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberResult;

    invoke-virtual {v3, v5}, Lio/realm/RealmSchema;->getColumnInfo(Ljava/lang/Class;)Lio/realm/internal/ColumnInfo;

    move-result-object v5

    const/4 v6, 0x0

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v7

    move-object/from16 v3, p0

    invoke-virtual/range {v2 .. v7}, Lio/realm/BaseRealm$RealmObjectContext;->set(Lio/realm/BaseRealm;Lio/realm/internal/Row;Lio/realm/internal/ColumnInfo;ZLjava/util/List;)V

    .line 342
    new-instance v10, Lio/realm/RememberNumberResultRealmProxy;

    .end local v10    # "obj":Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberResult;
    invoke-direct {v10}, Lio/realm/RememberNumberResultRealmProxy;-><init>()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 344
    .restart local v10    # "obj":Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberResult;
    invoke-virtual {v2}, Lio/realm/BaseRealm$RealmObjectContext;->clear()V

    .line 348
    .end local v2    # "objectContext":Lio/realm/BaseRealm$RealmObjectContext;
    .end local v11    # "table":Lio/realm/internal/Table;
    .end local v12    # "pkColumnIndex":J
    .end local v14    # "rowIndex":J
    :cond_1
    if-nez v10, :cond_3

    .line 349
    const-string v3, "config"

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 350
    const-string v3, "config"

    invoke-interface {v9, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 352
    :cond_2
    const-string v3, "id"

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_5

    .line 353
    const-string v3, "id"

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Lorg/json/JSONObject;->isNull(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 354
    const-class v3, Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberResult;

    const/4 v4, 0x0

    const/4 v5, 0x1

    move-object/from16 v0, p0

    invoke-virtual {v0, v3, v4, v5, v9}, Lio/realm/Realm;->createObjectInternal(Ljava/lang/Class;Ljava/lang/Object;ZLjava/util/List;)Lio/realm/RealmModel;

    move-result-object v10

    .end local v10    # "obj":Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberResult;
    check-cast v10, Lio/realm/RememberNumberResultRealmProxy;

    .line 362
    .restart local v10    # "obj":Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberResult;
    :cond_3
    :goto_0
    const-string v3, "score"

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_7

    .line 363
    const-string v3, "score"

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Lorg/json/JSONObject;->isNull(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_6

    .line 364
    new-instance v3, Ljava/lang/IllegalArgumentException;

    const-string v4, "Trying to set non-nullable field \'score\' to null."

    invoke-direct {v3, v4}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 344
    .end local v10    # "obj":Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberResult;
    .restart local v2    # "objectContext":Lio/realm/BaseRealm$RealmObjectContext;
    .restart local v11    # "table":Lio/realm/internal/Table;
    .restart local v12    # "pkColumnIndex":J
    .restart local v14    # "rowIndex":J
    :catchall_0
    move-exception v3

    invoke-virtual {v2}, Lio/realm/BaseRealm$RealmObjectContext;->clear()V

    throw v3

    .line 356
    .end local v2    # "objectContext":Lio/realm/BaseRealm$RealmObjectContext;
    .end local v11    # "table":Lio/realm/internal/Table;
    .end local v12    # "pkColumnIndex":J
    .end local v14    # "rowIndex":J
    .restart local v10    # "obj":Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberResult;
    :cond_4
    const-class v3, Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberResult;

    const-string v4, "id"

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    const/4 v5, 0x1

    move-object/from16 v0, p0

    invoke-virtual {v0, v3, v4, v5, v9}, Lio/realm/Realm;->createObjectInternal(Ljava/lang/Class;Ljava/lang/Object;ZLjava/util/List;)Lio/realm/RealmModel;

    move-result-object v10

    .end local v10    # "obj":Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberResult;
    check-cast v10, Lio/realm/RememberNumberResultRealmProxy;

    .restart local v10    # "obj":Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberResult;
    goto :goto_0

    .line 359
    :cond_5
    new-instance v3, Ljava/lang/IllegalArgumentException;

    const-string v4, "JSON object doesn\'t have the primary key field \'id\'."

    invoke-direct {v3, v4}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v3

    :cond_6
    move-object v3, v10

    .line 366
    check-cast v3, Lio/realm/RememberNumberResultRealmProxyInterface;

    const-string v4, "score"

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v4

    invoke-interface {v3, v4}, Lio/realm/RememberNumberResultRealmProxyInterface;->realmSet$score(I)V

    .line 369
    :cond_7
    const-string v3, "unixTime"

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_9

    .line 370
    const-string v3, "unixTime"

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Lorg/json/JSONObject;->isNull(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_8

    .line 371
    new-instance v3, Ljava/lang/IllegalArgumentException;

    const-string v4, "Trying to set non-nullable field \'unixTime\' to null."

    invoke-direct {v3, v4}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v3

    :cond_8
    move-object v3, v10

    .line 373
    check-cast v3, Lio/realm/RememberNumberResultRealmProxyInterface;

    const-string v4, "unixTime"

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Lorg/json/JSONObject;->getLong(Ljava/lang/String;)J

    move-result-wide v4

    invoke-interface {v3, v4, v5}, Lio/realm/RememberNumberResultRealmProxyInterface;->realmSet$unixTime(J)V

    .line 376
    :cond_9
    const-string v3, "config"

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_a

    .line 377
    const-string v3, "config"

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Lorg/json/JSONObject;->isNull(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_b

    move-object v3, v10

    .line 378
    check-cast v3, Lio/realm/RememberNumberResultRealmProxyInterface;

    const/4 v4, 0x0

    invoke-interface {v3, v4}, Lio/realm/RememberNumberResultRealmProxyInterface;->realmSet$config(Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberConfig;)V

    .line 384
    :cond_a
    :goto_1
    return-object v10

    .line 380
    :cond_b
    const-string v3, "config"

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v3

    move-object/from16 v0, p0

    move/from16 v1, p2

    invoke-static {v0, v3, v1}, Lio/realm/RememberNumberConfigRealmProxy;->createOrUpdateUsingJsonObject(Lio/realm/Realm;Lorg/json/JSONObject;Z)Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberConfig;

    move-result-object v8

    .local v8, "configObj":Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberConfig;
    move-object v3, v10

    .line 381
    check-cast v3, Lio/realm/RememberNumberResultRealmProxyInterface;

    invoke-interface {v3, v8}, Lio/realm/RememberNumberResultRealmProxyInterface;->realmSet$config(Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberConfig;)V

    goto :goto_1
.end method

.method public static createRealmObjectSchema(Lio/realm/RealmSchema;)Lio/realm/RealmObjectSchema;
    .locals 11
    .param p0, "realmSchema"    # Lio/realm/RealmSchema;

    .prologue
    const/4 v7, 0x0

    const/4 v3, 0x1

    .line 209
    const-string v0, "RememberNumberResult"

    invoke-virtual {p0, v0}, Lio/realm/RealmSchema;->contains(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 210
    const-string v0, "RememberNumberResult"

    invoke-virtual {p0, v0}, Lio/realm/RealmSchema;->create(Ljava/lang/String;)Lio/realm/RealmObjectSchema;

    move-result-object v10

    .line 211
    .local v10, "realmObjectSchema":Lio/realm/RealmObjectSchema;
    new-instance v0, Lio/realm/Property;

    const-string v1, "id"

    sget-object v2, Lio/realm/RealmFieldType;->INTEGER:Lio/realm/RealmFieldType;

    move v4, v3

    move v5, v3

    invoke-direct/range {v0 .. v5}, Lio/realm/Property;-><init>(Ljava/lang/String;Lio/realm/RealmFieldType;ZZZ)V

    invoke-virtual {v10, v0}, Lio/realm/RealmObjectSchema;->add(Lio/realm/Property;)Lio/realm/RealmObjectSchema;

    .line 212
    new-instance v4, Lio/realm/Property;

    const-string v5, "score"

    sget-object v6, Lio/realm/RealmFieldType;->INTEGER:Lio/realm/RealmFieldType;

    move v8, v7

    move v9, v3

    invoke-direct/range {v4 .. v9}, Lio/realm/Property;-><init>(Ljava/lang/String;Lio/realm/RealmFieldType;ZZZ)V

    invoke-virtual {v10, v4}, Lio/realm/RealmObjectSchema;->add(Lio/realm/Property;)Lio/realm/RealmObjectSchema;

    .line 213
    new-instance v4, Lio/realm/Property;

    const-string v5, "unixTime"

    sget-object v6, Lio/realm/RealmFieldType;->INTEGER:Lio/realm/RealmFieldType;

    move v8, v7

    move v9, v3

    invoke-direct/range {v4 .. v9}, Lio/realm/Property;-><init>(Ljava/lang/String;Lio/realm/RealmFieldType;ZZZ)V

    invoke-virtual {v10, v4}, Lio/realm/RealmObjectSchema;->add(Lio/realm/Property;)Lio/realm/RealmObjectSchema;

    .line 214
    const-string v0, "RememberNumberConfig"

    invoke-virtual {p0, v0}, Lio/realm/RealmSchema;->contains(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 215
    invoke-static {p0}, Lio/realm/RememberNumberConfigRealmProxy;->createRealmObjectSchema(Lio/realm/RealmSchema;)Lio/realm/RealmObjectSchema;

    .line 217
    :cond_0
    new-instance v0, Lio/realm/Property;

    const-string v1, "config"

    sget-object v2, Lio/realm/RealmFieldType;->OBJECT:Lio/realm/RealmFieldType;

    const-string v3, "RememberNumberConfig"

    invoke-virtual {p0, v3}, Lio/realm/RealmSchema;->get(Ljava/lang/String;)Lio/realm/RealmObjectSchema;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lio/realm/Property;-><init>(Ljava/lang/String;Lio/realm/RealmFieldType;Lio/realm/RealmObjectSchema;)V

    invoke-virtual {v10, v0}, Lio/realm/RealmObjectSchema;->add(Lio/realm/Property;)Lio/realm/RealmObjectSchema;

    .line 220
    .end local v10    # "realmObjectSchema":Lio/realm/RealmObjectSchema;
    :goto_0
    return-object v10

    :cond_1
    const-string v0, "RememberNumberResult"

    invoke-virtual {p0, v0}, Lio/realm/RealmSchema;->get(Ljava/lang/String;)Lio/realm/RealmObjectSchema;

    move-result-object v10

    goto :goto_0
.end method

.method public static createUsingJsonStream(Lio/realm/Realm;Landroid/util/JsonReader;)Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberResult;
    .locals 8
    .param p0, "realm"    # Lio/realm/Realm;
    .param p1, "reader"    # Landroid/util/JsonReader;
    .annotation build Landroid/annotation/TargetApi;
        value = 0xb
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 391
    const/4 v1, 0x0

    .line 392
    .local v1, "jsonHasPrimaryKey":Z
    new-instance v3, Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberResult;

    invoke-direct {v3}, Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberResult;-><init>()V

    .line 393
    .local v3, "obj":Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberResult;
    invoke-virtual {p1}, Landroid/util/JsonReader;->beginObject()V

    .line 394
    :goto_0
    invoke-virtual {p1}, Landroid/util/JsonReader;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_8

    .line 395
    invoke-virtual {p1}, Landroid/util/JsonReader;->nextName()Ljava/lang/String;

    move-result-object v2

    .line 396
    .local v2, "name":Ljava/lang/String;
    const-string v4, "id"

    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 397
    invoke-virtual {p1}, Landroid/util/JsonReader;->peek()Landroid/util/JsonToken;

    move-result-object v4

    sget-object v5, Landroid/util/JsonToken;->NULL:Landroid/util/JsonToken;

    if-ne v4, v5, :cond_0

    .line 398
    invoke-virtual {p1}, Landroid/util/JsonReader;->skipValue()V

    .line 399
    new-instance v4, Ljava/lang/IllegalArgumentException;

    const-string v5, "Trying to set non-nullable field \'id\' to null."

    invoke-direct {v4, v5}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v4

    :cond_0
    move-object v4, v3

    .line 401
    check-cast v4, Lio/realm/RememberNumberResultRealmProxyInterface;

    invoke-virtual {p1}, Landroid/util/JsonReader;->nextInt()I

    move-result v5

    invoke-interface {v4, v5}, Lio/realm/RememberNumberResultRealmProxyInterface;->realmSet$id(I)V

    .line 403
    const/4 v1, 0x1

    goto :goto_0

    .line 404
    :cond_1
    const-string v4, "score"

    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 405
    invoke-virtual {p1}, Landroid/util/JsonReader;->peek()Landroid/util/JsonToken;

    move-result-object v4

    sget-object v5, Landroid/util/JsonToken;->NULL:Landroid/util/JsonToken;

    if-ne v4, v5, :cond_2

    .line 406
    invoke-virtual {p1}, Landroid/util/JsonReader;->skipValue()V

    .line 407
    new-instance v4, Ljava/lang/IllegalArgumentException;

    const-string v5, "Trying to set non-nullable field \'score\' to null."

    invoke-direct {v4, v5}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v4

    :cond_2
    move-object v4, v3

    .line 409
    check-cast v4, Lio/realm/RememberNumberResultRealmProxyInterface;

    invoke-virtual {p1}, Landroid/util/JsonReader;->nextInt()I

    move-result v5

    invoke-interface {v4, v5}, Lio/realm/RememberNumberResultRealmProxyInterface;->realmSet$score(I)V

    goto :goto_0

    .line 411
    :cond_3
    const-string v4, "unixTime"

    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_5

    .line 412
    invoke-virtual {p1}, Landroid/util/JsonReader;->peek()Landroid/util/JsonToken;

    move-result-object v4

    sget-object v5, Landroid/util/JsonToken;->NULL:Landroid/util/JsonToken;

    if-ne v4, v5, :cond_4

    .line 413
    invoke-virtual {p1}, Landroid/util/JsonReader;->skipValue()V

    .line 414
    new-instance v4, Ljava/lang/IllegalArgumentException;

    const-string v5, "Trying to set non-nullable field \'unixTime\' to null."

    invoke-direct {v4, v5}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v4

    :cond_4
    move-object v4, v3

    .line 416
    check-cast v4, Lio/realm/RememberNumberResultRealmProxyInterface;

    invoke-virtual {p1}, Landroid/util/JsonReader;->nextLong()J

    move-result-wide v6

    invoke-interface {v4, v6, v7}, Lio/realm/RememberNumberResultRealmProxyInterface;->realmSet$unixTime(J)V

    goto :goto_0

    .line 418
    :cond_5
    const-string v4, "config"

    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_7

    .line 419
    invoke-virtual {p1}, Landroid/util/JsonReader;->peek()Landroid/util/JsonToken;

    move-result-object v4

    sget-object v5, Landroid/util/JsonToken;->NULL:Landroid/util/JsonToken;

    if-ne v4, v5, :cond_6

    .line 420
    invoke-virtual {p1}, Landroid/util/JsonReader;->skipValue()V

    move-object v4, v3

    .line 421
    check-cast v4, Lio/realm/RememberNumberResultRealmProxyInterface;

    const/4 v5, 0x0

    invoke-interface {v4, v5}, Lio/realm/RememberNumberResultRealmProxyInterface;->realmSet$config(Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberConfig;)V

    goto/16 :goto_0

    .line 423
    :cond_6
    invoke-static {p0, p1}, Lio/realm/RememberNumberConfigRealmProxy;->createUsingJsonStream(Lio/realm/Realm;Landroid/util/JsonReader;)Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberConfig;

    move-result-object v0

    .local v0, "configObj":Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberConfig;
    move-object v4, v3

    .line 424
    check-cast v4, Lio/realm/RememberNumberResultRealmProxyInterface;

    invoke-interface {v4, v0}, Lio/realm/RememberNumberResultRealmProxyInterface;->realmSet$config(Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberConfig;)V

    goto/16 :goto_0

    .line 427
    .end local v0    # "configObj":Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberConfig;
    :cond_7
    invoke-virtual {p1}, Landroid/util/JsonReader;->skipValue()V

    goto/16 :goto_0

    .line 430
    .end local v2    # "name":Ljava/lang/String;
    :cond_8
    invoke-virtual {p1}, Landroid/util/JsonReader;->endObject()V

    .line 431
    if-nez v1, :cond_9

    .line 432
    new-instance v4, Ljava/lang/IllegalArgumentException;

    const-string v5, "JSON object doesn\'t have the primary key field \'id\'."

    invoke-direct {v4, v5}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 434
    :cond_9
    invoke-virtual {p0, v3}, Lio/realm/Realm;->copyToRealm(Lio/realm/RealmModel;)Lio/realm/RealmModel;

    move-result-object v3

    .end local v3    # "obj":Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberResult;
    check-cast v3, Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberResult;

    .line 435
    .restart local v3    # "obj":Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberResult;
    return-object v3
.end method

.method public static getFieldNames()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 323
    sget-object v0, Lio/realm/RememberNumberResultRealmProxy;->FIELD_NAMES:Ljava/util/List;

    return-object v0
.end method

.method public static getTableName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 319
    const-string v0, "class_RememberNumberResult"

    return-object v0
.end method

.method public static initTable(Lio/realm/internal/SharedRealm;)Lio/realm/internal/Table;
    .locals 4
    .param p0, "sharedRealm"    # Lio/realm/internal/SharedRealm;

    .prologue
    const/4 v3, 0x0

    .line 224
    const-string v1, "class_RememberNumberResult"

    invoke-virtual {p0, v1}, Lio/realm/internal/SharedRealm;->hasTable(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 225
    const-string v1, "class_RememberNumberResult"

    invoke-virtual {p0, v1}, Lio/realm/internal/SharedRealm;->getTable(Ljava/lang/String;)Lio/realm/internal/Table;

    move-result-object v0

    .line 226
    .local v0, "table":Lio/realm/internal/Table;
    sget-object v1, Lio/realm/RealmFieldType;->INTEGER:Lio/realm/RealmFieldType;

    const-string v2, "id"

    invoke-virtual {v0, v1, v2, v3}, Lio/realm/internal/Table;->addColumn(Lio/realm/RealmFieldType;Ljava/lang/String;Z)J

    .line 227
    sget-object v1, Lio/realm/RealmFieldType;->INTEGER:Lio/realm/RealmFieldType;

    const-string v2, "score"

    invoke-virtual {v0, v1, v2, v3}, Lio/realm/internal/Table;->addColumn(Lio/realm/RealmFieldType;Ljava/lang/String;Z)J

    .line 228
    sget-object v1, Lio/realm/RealmFieldType;->INTEGER:Lio/realm/RealmFieldType;

    const-string v2, "unixTime"

    invoke-virtual {v0, v1, v2, v3}, Lio/realm/internal/Table;->addColumn(Lio/realm/RealmFieldType;Ljava/lang/String;Z)J

    .line 229
    const-string v1, "class_RememberNumberConfig"

    invoke-virtual {p0, v1}, Lio/realm/internal/SharedRealm;->hasTable(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 230
    invoke-static {p0}, Lio/realm/RememberNumberConfigRealmProxy;->initTable(Lio/realm/internal/SharedRealm;)Lio/realm/internal/Table;

    .line 232
    :cond_0
    sget-object v1, Lio/realm/RealmFieldType;->OBJECT:Lio/realm/RealmFieldType;

    const-string v2, "config"

    const-string v3, "class_RememberNumberConfig"

    invoke-virtual {p0, v3}, Lio/realm/internal/SharedRealm;->getTable(Ljava/lang/String;)Lio/realm/internal/Table;

    move-result-object v3

    invoke-virtual {v0, v1, v2, v3}, Lio/realm/internal/Table;->addColumnLink(Lio/realm/RealmFieldType;Ljava/lang/String;Lio/realm/internal/Table;)J

    .line 233
    const-string v1, "id"

    invoke-virtual {v0, v1}, Lio/realm/internal/Table;->getColumnIndex(Ljava/lang/String;)J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lio/realm/internal/Table;->addSearchIndex(J)V

    .line 234
    const-string v1, "id"

    invoke-virtual {v0, v1}, Lio/realm/internal/Table;->setPrimaryKey(Ljava/lang/String;)V

    .line 237
    .end local v0    # "table":Lio/realm/internal/Table;
    :goto_0
    return-object v0

    :cond_1
    const-string v1, "class_RememberNumberResult"

    invoke-virtual {p0, v1}, Lio/realm/internal/SharedRealm;->getTable(Ljava/lang/String;)Lio/realm/internal/Table;

    move-result-object v0

    goto :goto_0
.end method

.method public static insert(Lio/realm/Realm;Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberResult;Ljava/util/Map;)J
    .locals 22
    .param p0, "realm"    # Lio/realm/Realm;
    .param p1, "object"    # Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberResult;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/realm/Realm;",
            "Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberResult;",
            "Ljava/util/Map",
            "<",
            "Lio/realm/RealmModel;",
            "Ljava/lang/Long;",
            ">;)J"
        }
    .end annotation

    .prologue
    .line 504
    .local p2, "cache":Ljava/util/Map;, "Ljava/util/Map<Lio/realm/RealmModel;Ljava/lang/Long;>;"
    move-object/from16 v0, p1

    instance-of v8, v0, Lio/realm/internal/RealmObjectProxy;

    if-eqz v8, :cond_1

    move-object/from16 v8, p1

    check-cast v8, Lio/realm/internal/RealmObjectProxy;

    invoke-interface {v8}, Lio/realm/internal/RealmObjectProxy;->realmGet$proxyState()Lio/realm/ProxyState;

    move-result-object v8

    invoke-virtual {v8}, Lio/realm/ProxyState;->getRealm$realm()Lio/realm/BaseRealm;

    move-result-object v8

    if-eqz v8, :cond_1

    move-object/from16 v8, p1

    check-cast v8, Lio/realm/internal/RealmObjectProxy;

    invoke-interface {v8}, Lio/realm/internal/RealmObjectProxy;->realmGet$proxyState()Lio/realm/ProxyState;

    move-result-object v8

    invoke-virtual {v8}, Lio/realm/ProxyState;->getRealm$realm()Lio/realm/BaseRealm;

    move-result-object v8

    invoke-virtual {v8}, Lio/realm/BaseRealm;->getPath()Ljava/lang/String;

    move-result-object v8

    invoke-virtual/range {p0 .. p0}, Lio/realm/Realm;->getPath()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_1

    .line 505
    check-cast p1, Lio/realm/internal/RealmObjectProxy;

    .end local p1    # "object":Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberResult;
    invoke-interface/range {p1 .. p1}, Lio/realm/internal/RealmObjectProxy;->realmGet$proxyState()Lio/realm/ProxyState;

    move-result-object v8

    invoke-virtual {v8}, Lio/realm/ProxyState;->getRow$realm()Lio/realm/internal/Row;

    move-result-object v8

    invoke-interface {v8}, Lio/realm/internal/Row;->getIndex()J

    move-result-wide v12

    .line 533
    :cond_0
    :goto_0
    return-wide v12

    .line 507
    .restart local p1    # "object":Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberResult;
    :cond_1
    const-class v8, Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberResult;

    move-object/from16 v0, p0

    invoke-virtual {v0, v8}, Lio/realm/Realm;->getTable(Ljava/lang/Class;)Lio/realm/internal/Table;

    move-result-object v21

    .line 508
    .local v21, "table":Lio/realm/internal/Table;
    invoke-virtual/range {v21 .. v21}, Lio/realm/internal/Table;->getNativeTablePointer()J

    move-result-wide v4

    .line 509
    .local v4, "tableNativePtr":J
    move-object/from16 v0, p0

    iget-object v8, v0, Lio/realm/Realm;->schema:Lio/realm/RealmSchema;

    const-class v9, Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberResult;

    invoke-virtual {v8, v9}, Lio/realm/RealmSchema;->getColumnInfo(Ljava/lang/Class;)Lio/realm/internal/ColumnInfo;

    move-result-object v18

    check-cast v18, Lio/realm/RememberNumberResultRealmProxy$RememberNumberResultColumnInfo;

    .line 510
    .local v18, "columnInfo":Lio/realm/RememberNumberResultRealmProxy$RememberNumberResultColumnInfo;
    invoke-virtual/range {v21 .. v21}, Lio/realm/internal/Table;->getPrimaryKey()J

    move-result-wide v6

    .line 511
    .local v6, "pkColumnIndex":J
    const-wide/16 v12, -0x1

    .local v12, "rowIndex":J
    move-object/from16 v8, p1

    .line 512
    check-cast v8, Lio/realm/RememberNumberResultRealmProxyInterface;

    invoke-interface {v8}, Lio/realm/RememberNumberResultRealmProxyInterface;->realmGet$id()I

    move-result v8

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v20

    .line 513
    .local v20, "primaryKeyValue":Ljava/lang/Integer;
    if-eqz v20, :cond_2

    move-object/from16 v8, p1

    .line 514
    check-cast v8, Lio/realm/RememberNumberResultRealmProxyInterface;

    invoke-interface {v8}, Lio/realm/RememberNumberResultRealmProxyInterface;->realmGet$id()I

    move-result v8

    int-to-long v8, v8

    invoke-static/range {v4 .. v9}, Lio/realm/internal/Table;->nativeFindFirstInt(JJJ)J

    move-result-wide v12

    .line 516
    :cond_2
    const-wide/16 v8, -0x1

    cmp-long v8, v12, v8

    if-nez v8, :cond_4

    move-object/from16 v8, p1

    .line 517
    check-cast v8, Lio/realm/RememberNumberResultRealmProxyInterface;

    invoke-interface {v8}, Lio/realm/RememberNumberResultRealmProxyInterface;->realmGet$id()I

    move-result v8

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    const/4 v9, 0x0

    move-object/from16 v0, v21

    invoke-virtual {v0, v8, v9}, Lio/realm/internal/Table;->addEmptyRowWithPrimaryKey(Ljava/lang/Object;Z)J

    move-result-wide v12

    .line 521
    :goto_1
    invoke-static {v12, v13}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    move-object/from16 v0, p2

    move-object/from16 v1, p1

    invoke-interface {v0, v1, v8}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 522
    move-object/from16 v0, v18

    iget-wide v10, v0, Lio/realm/RememberNumberResultRealmProxy$RememberNumberResultColumnInfo;->scoreIndex:J

    move-object/from16 v8, p1

    check-cast v8, Lio/realm/RememberNumberResultRealmProxyInterface;

    invoke-interface {v8}, Lio/realm/RememberNumberResultRealmProxyInterface;->realmGet$score()I

    move-result v8

    int-to-long v14, v8

    const/16 v16, 0x0

    move-wide v8, v4

    invoke-static/range {v8 .. v16}, Lio/realm/internal/Table;->nativeSetLong(JJJJZ)V

    .line 523
    move-object/from16 v0, v18

    iget-wide v10, v0, Lio/realm/RememberNumberResultRealmProxy$RememberNumberResultColumnInfo;->unixTimeIndex:J

    move-object/from16 v8, p1

    check-cast v8, Lio/realm/RememberNumberResultRealmProxyInterface;

    invoke-interface {v8}, Lio/realm/RememberNumberResultRealmProxyInterface;->realmGet$unixTime()J

    move-result-wide v14

    const/16 v16, 0x0

    move-wide v8, v4

    invoke-static/range {v8 .. v16}, Lio/realm/internal/Table;->nativeSetLong(JJJJZ)V

    .line 525
    check-cast p1, Lio/realm/RememberNumberResultRealmProxyInterface;

    .end local p1    # "object":Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberResult;
    invoke-interface/range {p1 .. p1}, Lio/realm/RememberNumberResultRealmProxyInterface;->realmGet$config()Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberConfig;

    move-result-object v19

    .line 526
    .local v19, "configObj":Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberConfig;
    if-eqz v19, :cond_0

    .line 527
    move-object/from16 v0, p2

    move-object/from16 v1, v19

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v17

    check-cast v17, Ljava/lang/Long;

    .line 528
    .local v17, "cacheconfig":Ljava/lang/Long;
    if-nez v17, :cond_3

    .line 529
    move-object/from16 v0, p0

    move-object/from16 v1, v19

    move-object/from16 v2, p2

    invoke-static {v0, v1, v2}, Lio/realm/RememberNumberConfigRealmProxy;->insert(Lio/realm/Realm;Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberConfig;Ljava/util/Map;)J

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v17

    .line 531
    :cond_3
    move-object/from16 v0, v18

    iget-wide v10, v0, Lio/realm/RememberNumberResultRealmProxy$RememberNumberResultColumnInfo;->configIndex:J

    invoke-virtual/range {v17 .. v17}, Ljava/lang/Long;->longValue()J

    move-result-wide v14

    const/16 v16, 0x0

    move-wide v8, v4

    invoke-static/range {v8 .. v16}, Lio/realm/internal/Table;->nativeSetLink(JJJJZ)V

    goto/16 :goto_0

    .line 519
    .end local v17    # "cacheconfig":Ljava/lang/Long;
    .end local v19    # "configObj":Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberConfig;
    .restart local p1    # "object":Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberResult;
    :cond_4
    invoke-static/range {v20 .. v20}, Lio/realm/internal/Table;->throwDuplicatePrimaryKeyException(Ljava/lang/Object;)V

    goto :goto_1
.end method

.method public static insert(Lio/realm/Realm;Ljava/util/Iterator;Ljava/util/Map;)V
    .locals 23
    .param p0, "realm"    # Lio/realm/Realm;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/realm/Realm;",
            "Ljava/util/Iterator",
            "<+",
            "Lio/realm/RealmModel;",
            ">;",
            "Ljava/util/Map",
            "<",
            "Lio/realm/RealmModel;",
            "Ljava/lang/Long;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 537
    .local p1, "objects":Ljava/util/Iterator;, "Ljava/util/Iterator<+Lio/realm/RealmModel;>;"
    .local p2, "cache":Ljava/util/Map;, "Ljava/util/Map<Lio/realm/RealmModel;Ljava/lang/Long;>;"
    const-class v8, Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberResult;

    move-object/from16 v0, p0

    invoke-virtual {v0, v8}, Lio/realm/Realm;->getTable(Ljava/lang/Class;)Lio/realm/internal/Table;

    move-result-object v22

    .line 538
    .local v22, "table":Lio/realm/internal/Table;
    invoke-virtual/range {v22 .. v22}, Lio/realm/internal/Table;->getNativeTablePointer()J

    move-result-wide v4

    .line 539
    .local v4, "tableNativePtr":J
    move-object/from16 v0, p0

    iget-object v8, v0, Lio/realm/Realm;->schema:Lio/realm/RealmSchema;

    const-class v9, Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberResult;

    invoke-virtual {v8, v9}, Lio/realm/RealmSchema;->getColumnInfo(Ljava/lang/Class;)Lio/realm/internal/ColumnInfo;

    move-result-object v18

    check-cast v18, Lio/realm/RememberNumberResultRealmProxy$RememberNumberResultColumnInfo;

    .line 540
    .local v18, "columnInfo":Lio/realm/RememberNumberResultRealmProxy$RememberNumberResultColumnInfo;
    invoke-virtual/range {v22 .. v22}, Lio/realm/internal/Table;->getPrimaryKey()J

    move-result-wide v6

    .line 541
    .local v6, "pkColumnIndex":J
    const/16 v20, 0x0

    .line 542
    .local v20, "object":Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberResult;
    :cond_0
    :goto_0
    invoke-interface/range {p1 .. p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_5

    .line 543
    invoke-interface/range {p1 .. p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v20

    .end local v20    # "object":Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberResult;
    check-cast v20, Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberResult;

    .line 544
    .restart local v20    # "object":Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberResult;
    move-object/from16 v0, p2

    move-object/from16 v1, v20

    invoke-interface {v0, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v8

    if-nez v8, :cond_0

    .line 545
    move-object/from16 v0, v20

    instance-of v8, v0, Lio/realm/internal/RealmObjectProxy;

    if-eqz v8, :cond_1

    move-object/from16 v8, v20

    check-cast v8, Lio/realm/internal/RealmObjectProxy;

    invoke-interface {v8}, Lio/realm/internal/RealmObjectProxy;->realmGet$proxyState()Lio/realm/ProxyState;

    move-result-object v8

    invoke-virtual {v8}, Lio/realm/ProxyState;->getRealm$realm()Lio/realm/BaseRealm;

    move-result-object v8

    if-eqz v8, :cond_1

    move-object/from16 v8, v20

    check-cast v8, Lio/realm/internal/RealmObjectProxy;

    invoke-interface {v8}, Lio/realm/internal/RealmObjectProxy;->realmGet$proxyState()Lio/realm/ProxyState;

    move-result-object v8

    invoke-virtual {v8}, Lio/realm/ProxyState;->getRealm$realm()Lio/realm/BaseRealm;

    move-result-object v8

    invoke-virtual {v8}, Lio/realm/BaseRealm;->getPath()Ljava/lang/String;

    move-result-object v8

    invoke-virtual/range {p0 .. p0}, Lio/realm/Realm;->getPath()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_1

    move-object/from16 v8, v20

    .line 546
    check-cast v8, Lio/realm/internal/RealmObjectProxy;

    invoke-interface {v8}, Lio/realm/internal/RealmObjectProxy;->realmGet$proxyState()Lio/realm/ProxyState;

    move-result-object v8

    invoke-virtual {v8}, Lio/realm/ProxyState;->getRow$realm()Lio/realm/internal/Row;

    move-result-object v8

    invoke-interface {v8}, Lio/realm/internal/Row;->getIndex()J

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    move-object/from16 v0, p2

    move-object/from16 v1, v20

    invoke-interface {v0, v1, v8}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 549
    :cond_1
    const-wide/16 v12, -0x1

    .local v12, "rowIndex":J
    move-object/from16 v8, v20

    .line 550
    check-cast v8, Lio/realm/RememberNumberResultRealmProxyInterface;

    invoke-interface {v8}, Lio/realm/RememberNumberResultRealmProxyInterface;->realmGet$id()I

    move-result v8

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v21

    .line 551
    .local v21, "primaryKeyValue":Ljava/lang/Integer;
    if-eqz v21, :cond_2

    move-object/from16 v8, v20

    .line 552
    check-cast v8, Lio/realm/RememberNumberResultRealmProxyInterface;

    invoke-interface {v8}, Lio/realm/RememberNumberResultRealmProxyInterface;->realmGet$id()I

    move-result v8

    int-to-long v8, v8

    invoke-static/range {v4 .. v9}, Lio/realm/internal/Table;->nativeFindFirstInt(JJJ)J

    move-result-wide v12

    .line 554
    :cond_2
    const-wide/16 v8, -0x1

    cmp-long v8, v12, v8

    if-nez v8, :cond_4

    move-object/from16 v8, v20

    .line 555
    check-cast v8, Lio/realm/RememberNumberResultRealmProxyInterface;

    invoke-interface {v8}, Lio/realm/RememberNumberResultRealmProxyInterface;->realmGet$id()I

    move-result v8

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    const/4 v9, 0x0

    move-object/from16 v0, v22

    invoke-virtual {v0, v8, v9}, Lio/realm/internal/Table;->addEmptyRowWithPrimaryKey(Ljava/lang/Object;Z)J

    move-result-wide v12

    .line 559
    :goto_1
    invoke-static {v12, v13}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    move-object/from16 v0, p2

    move-object/from16 v1, v20

    invoke-interface {v0, v1, v8}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 560
    move-object/from16 v0, v18

    iget-wide v10, v0, Lio/realm/RememberNumberResultRealmProxy$RememberNumberResultColumnInfo;->scoreIndex:J

    move-object/from16 v8, v20

    check-cast v8, Lio/realm/RememberNumberResultRealmProxyInterface;

    invoke-interface {v8}, Lio/realm/RememberNumberResultRealmProxyInterface;->realmGet$score()I

    move-result v8

    int-to-long v14, v8

    const/16 v16, 0x0

    move-wide v8, v4

    invoke-static/range {v8 .. v16}, Lio/realm/internal/Table;->nativeSetLong(JJJJZ)V

    .line 561
    move-object/from16 v0, v18

    iget-wide v10, v0, Lio/realm/RememberNumberResultRealmProxy$RememberNumberResultColumnInfo;->unixTimeIndex:J

    move-object/from16 v8, v20

    check-cast v8, Lio/realm/RememberNumberResultRealmProxyInterface;

    invoke-interface {v8}, Lio/realm/RememberNumberResultRealmProxyInterface;->realmGet$unixTime()J

    move-result-wide v14

    const/16 v16, 0x0

    move-wide v8, v4

    invoke-static/range {v8 .. v16}, Lio/realm/internal/Table;->nativeSetLong(JJJJZ)V

    move-object/from16 v8, v20

    .line 563
    check-cast v8, Lio/realm/RememberNumberResultRealmProxyInterface;

    invoke-interface {v8}, Lio/realm/RememberNumberResultRealmProxyInterface;->realmGet$config()Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberConfig;

    move-result-object v19

    .line 564
    .local v19, "configObj":Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberConfig;
    if-eqz v19, :cond_0

    .line 565
    move-object/from16 v0, p2

    move-object/from16 v1, v19

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v17

    check-cast v17, Ljava/lang/Long;

    .line 566
    .local v17, "cacheconfig":Ljava/lang/Long;
    if-nez v17, :cond_3

    .line 567
    move-object/from16 v0, p0

    move-object/from16 v1, v19

    move-object/from16 v2, p2

    invoke-static {v0, v1, v2}, Lio/realm/RememberNumberConfigRealmProxy;->insert(Lio/realm/Realm;Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberConfig;Ljava/util/Map;)J

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v17

    .line 569
    :cond_3
    move-object/from16 v0, v18

    iget-wide v10, v0, Lio/realm/RememberNumberResultRealmProxy$RememberNumberResultColumnInfo;->configIndex:J

    invoke-virtual/range {v17 .. v17}, Ljava/lang/Long;->longValue()J

    move-result-wide v14

    const/16 v16, 0x0

    move-object/from16 v9, v22

    invoke-virtual/range {v9 .. v16}, Lio/realm/internal/Table;->setLink(JJJZ)V

    goto/16 :goto_0

    .line 557
    .end local v17    # "cacheconfig":Ljava/lang/Long;
    .end local v19    # "configObj":Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberConfig;
    :cond_4
    invoke-static/range {v21 .. v21}, Lio/realm/internal/Table;->throwDuplicatePrimaryKeyException(Ljava/lang/Object;)V

    goto :goto_1

    .line 573
    .end local v12    # "rowIndex":J
    .end local v21    # "primaryKeyValue":Ljava/lang/Integer;
    :cond_5
    return-void
.end method

.method public static insertOrUpdate(Lio/realm/Realm;Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberResult;Ljava/util/Map;)J
    .locals 22
    .param p0, "realm"    # Lio/realm/Realm;
    .param p1, "object"    # Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberResult;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/realm/Realm;",
            "Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberResult;",
            "Ljava/util/Map",
            "<",
            "Lio/realm/RealmModel;",
            "Ljava/lang/Long;",
            ">;)J"
        }
    .end annotation

    .prologue
    .line 576
    .local p2, "cache":Ljava/util/Map;, "Ljava/util/Map<Lio/realm/RealmModel;Ljava/lang/Long;>;"
    move-object/from16 v0, p1

    instance-of v8, v0, Lio/realm/internal/RealmObjectProxy;

    if-eqz v8, :cond_0

    move-object/from16 v8, p1

    check-cast v8, Lio/realm/internal/RealmObjectProxy;

    invoke-interface {v8}, Lio/realm/internal/RealmObjectProxy;->realmGet$proxyState()Lio/realm/ProxyState;

    move-result-object v8

    invoke-virtual {v8}, Lio/realm/ProxyState;->getRealm$realm()Lio/realm/BaseRealm;

    move-result-object v8

    if-eqz v8, :cond_0

    move-object/from16 v8, p1

    check-cast v8, Lio/realm/internal/RealmObjectProxy;

    invoke-interface {v8}, Lio/realm/internal/RealmObjectProxy;->realmGet$proxyState()Lio/realm/ProxyState;

    move-result-object v8

    invoke-virtual {v8}, Lio/realm/ProxyState;->getRealm$realm()Lio/realm/BaseRealm;

    move-result-object v8

    invoke-virtual {v8}, Lio/realm/BaseRealm;->getPath()Ljava/lang/String;

    move-result-object v8

    invoke-virtual/range {p0 .. p0}, Lio/realm/Realm;->getPath()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_0

    .line 577
    check-cast p1, Lio/realm/internal/RealmObjectProxy;

    .end local p1    # "object":Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberResult;
    invoke-interface/range {p1 .. p1}, Lio/realm/internal/RealmObjectProxy;->realmGet$proxyState()Lio/realm/ProxyState;

    move-result-object v8

    invoke-virtual {v8}, Lio/realm/ProxyState;->getRow$realm()Lio/realm/internal/Row;

    move-result-object v8

    invoke-interface {v8}, Lio/realm/internal/Row;->getIndex()J

    move-result-wide v12

    .line 605
    :goto_0
    return-wide v12

    .line 579
    .restart local p1    # "object":Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberResult;
    :cond_0
    const-class v8, Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberResult;

    move-object/from16 v0, p0

    invoke-virtual {v0, v8}, Lio/realm/Realm;->getTable(Ljava/lang/Class;)Lio/realm/internal/Table;

    move-result-object v21

    .line 580
    .local v21, "table":Lio/realm/internal/Table;
    invoke-virtual/range {v21 .. v21}, Lio/realm/internal/Table;->getNativeTablePointer()J

    move-result-wide v4

    .line 581
    .local v4, "tableNativePtr":J
    move-object/from16 v0, p0

    iget-object v8, v0, Lio/realm/Realm;->schema:Lio/realm/RealmSchema;

    const-class v9, Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberResult;

    invoke-virtual {v8, v9}, Lio/realm/RealmSchema;->getColumnInfo(Ljava/lang/Class;)Lio/realm/internal/ColumnInfo;

    move-result-object v18

    check-cast v18, Lio/realm/RememberNumberResultRealmProxy$RememberNumberResultColumnInfo;

    .line 582
    .local v18, "columnInfo":Lio/realm/RememberNumberResultRealmProxy$RememberNumberResultColumnInfo;
    invoke-virtual/range {v21 .. v21}, Lio/realm/internal/Table;->getPrimaryKey()J

    move-result-wide v6

    .line 583
    .local v6, "pkColumnIndex":J
    const-wide/16 v12, -0x1

    .local v12, "rowIndex":J
    move-object/from16 v8, p1

    .line 584
    check-cast v8, Lio/realm/RememberNumberResultRealmProxyInterface;

    invoke-interface {v8}, Lio/realm/RememberNumberResultRealmProxyInterface;->realmGet$id()I

    move-result v8

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v20

    .line 585
    .local v20, "primaryKeyValue":Ljava/lang/Integer;
    if-eqz v20, :cond_1

    move-object/from16 v8, p1

    .line 586
    check-cast v8, Lio/realm/RememberNumberResultRealmProxyInterface;

    invoke-interface {v8}, Lio/realm/RememberNumberResultRealmProxyInterface;->realmGet$id()I

    move-result v8

    int-to-long v8, v8

    invoke-static/range {v4 .. v9}, Lio/realm/internal/Table;->nativeFindFirstInt(JJJ)J

    move-result-wide v12

    .line 588
    :cond_1
    const-wide/16 v8, -0x1

    cmp-long v8, v12, v8

    if-nez v8, :cond_2

    move-object/from16 v8, p1

    .line 589
    check-cast v8, Lio/realm/RememberNumberResultRealmProxyInterface;

    invoke-interface {v8}, Lio/realm/RememberNumberResultRealmProxyInterface;->realmGet$id()I

    move-result v8

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    const/4 v9, 0x0

    move-object/from16 v0, v21

    invoke-virtual {v0, v8, v9}, Lio/realm/internal/Table;->addEmptyRowWithPrimaryKey(Ljava/lang/Object;Z)J

    move-result-wide v12

    .line 591
    :cond_2
    invoke-static {v12, v13}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    move-object/from16 v0, p2

    move-object/from16 v1, p1

    invoke-interface {v0, v1, v8}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 592
    move-object/from16 v0, v18

    iget-wide v10, v0, Lio/realm/RememberNumberResultRealmProxy$RememberNumberResultColumnInfo;->scoreIndex:J

    move-object/from16 v8, p1

    check-cast v8, Lio/realm/RememberNumberResultRealmProxyInterface;

    invoke-interface {v8}, Lio/realm/RememberNumberResultRealmProxyInterface;->realmGet$score()I

    move-result v8

    int-to-long v14, v8

    const/16 v16, 0x0

    move-wide v8, v4

    invoke-static/range {v8 .. v16}, Lio/realm/internal/Table;->nativeSetLong(JJJJZ)V

    .line 593
    move-object/from16 v0, v18

    iget-wide v10, v0, Lio/realm/RememberNumberResultRealmProxy$RememberNumberResultColumnInfo;->unixTimeIndex:J

    move-object/from16 v8, p1

    check-cast v8, Lio/realm/RememberNumberResultRealmProxyInterface;

    invoke-interface {v8}, Lio/realm/RememberNumberResultRealmProxyInterface;->realmGet$unixTime()J

    move-result-wide v14

    const/16 v16, 0x0

    move-wide v8, v4

    invoke-static/range {v8 .. v16}, Lio/realm/internal/Table;->nativeSetLong(JJJJZ)V

    .line 595
    check-cast p1, Lio/realm/RememberNumberResultRealmProxyInterface;

    .end local p1    # "object":Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberResult;
    invoke-interface/range {p1 .. p1}, Lio/realm/RememberNumberResultRealmProxyInterface;->realmGet$config()Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberConfig;

    move-result-object v19

    .line 596
    .local v19, "configObj":Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberConfig;
    if-eqz v19, :cond_4

    .line 597
    move-object/from16 v0, p2

    move-object/from16 v1, v19

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v17

    check-cast v17, Ljava/lang/Long;

    .line 598
    .local v17, "cacheconfig":Ljava/lang/Long;
    if-nez v17, :cond_3

    .line 599
    move-object/from16 v0, p0

    move-object/from16 v1, v19

    move-object/from16 v2, p2

    invoke-static {v0, v1, v2}, Lio/realm/RememberNumberConfigRealmProxy;->insertOrUpdate(Lio/realm/Realm;Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberConfig;Ljava/util/Map;)J

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v17

    .line 601
    :cond_3
    move-object/from16 v0, v18

    iget-wide v10, v0, Lio/realm/RememberNumberResultRealmProxy$RememberNumberResultColumnInfo;->configIndex:J

    invoke-virtual/range {v17 .. v17}, Ljava/lang/Long;->longValue()J

    move-result-wide v14

    const/16 v16, 0x0

    move-wide v8, v4

    invoke-static/range {v8 .. v16}, Lio/realm/internal/Table;->nativeSetLink(JJJJZ)V

    goto/16 :goto_0

    .line 603
    .end local v17    # "cacheconfig":Ljava/lang/Long;
    :cond_4
    move-object/from16 v0, v18

    iget-wide v10, v0, Lio/realm/RememberNumberResultRealmProxy$RememberNumberResultColumnInfo;->configIndex:J

    move-wide v8, v4

    invoke-static/range {v8 .. v13}, Lio/realm/internal/Table;->nativeNullifyLink(JJJ)V

    goto/16 :goto_0
.end method

.method public static insertOrUpdate(Lio/realm/Realm;Ljava/util/Iterator;Ljava/util/Map;)V
    .locals 23
    .param p0, "realm"    # Lio/realm/Realm;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/realm/Realm;",
            "Ljava/util/Iterator",
            "<+",
            "Lio/realm/RealmModel;",
            ">;",
            "Ljava/util/Map",
            "<",
            "Lio/realm/RealmModel;",
            "Ljava/lang/Long;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 609
    .local p1, "objects":Ljava/util/Iterator;, "Ljava/util/Iterator<+Lio/realm/RealmModel;>;"
    .local p2, "cache":Ljava/util/Map;, "Ljava/util/Map<Lio/realm/RealmModel;Ljava/lang/Long;>;"
    const-class v8, Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberResult;

    move-object/from16 v0, p0

    invoke-virtual {v0, v8}, Lio/realm/Realm;->getTable(Ljava/lang/Class;)Lio/realm/internal/Table;

    move-result-object v22

    .line 610
    .local v22, "table":Lio/realm/internal/Table;
    invoke-virtual/range {v22 .. v22}, Lio/realm/internal/Table;->getNativeTablePointer()J

    move-result-wide v4

    .line 611
    .local v4, "tableNativePtr":J
    move-object/from16 v0, p0

    iget-object v8, v0, Lio/realm/Realm;->schema:Lio/realm/RealmSchema;

    const-class v9, Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberResult;

    invoke-virtual {v8, v9}, Lio/realm/RealmSchema;->getColumnInfo(Ljava/lang/Class;)Lio/realm/internal/ColumnInfo;

    move-result-object v18

    check-cast v18, Lio/realm/RememberNumberResultRealmProxy$RememberNumberResultColumnInfo;

    .line 612
    .local v18, "columnInfo":Lio/realm/RememberNumberResultRealmProxy$RememberNumberResultColumnInfo;
    invoke-virtual/range {v22 .. v22}, Lio/realm/internal/Table;->getPrimaryKey()J

    move-result-wide v6

    .line 613
    .local v6, "pkColumnIndex":J
    const/16 v20, 0x0

    .line 614
    .local v20, "object":Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberResult;
    :cond_0
    :goto_0
    invoke-interface/range {p1 .. p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_6

    .line 615
    invoke-interface/range {p1 .. p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v20

    .end local v20    # "object":Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberResult;
    check-cast v20, Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberResult;

    .line 616
    .restart local v20    # "object":Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberResult;
    move-object/from16 v0, p2

    move-object/from16 v1, v20

    invoke-interface {v0, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v8

    if-nez v8, :cond_0

    .line 617
    move-object/from16 v0, v20

    instance-of v8, v0, Lio/realm/internal/RealmObjectProxy;

    if-eqz v8, :cond_1

    move-object/from16 v8, v20

    check-cast v8, Lio/realm/internal/RealmObjectProxy;

    invoke-interface {v8}, Lio/realm/internal/RealmObjectProxy;->realmGet$proxyState()Lio/realm/ProxyState;

    move-result-object v8

    invoke-virtual {v8}, Lio/realm/ProxyState;->getRealm$realm()Lio/realm/BaseRealm;

    move-result-object v8

    if-eqz v8, :cond_1

    move-object/from16 v8, v20

    check-cast v8, Lio/realm/internal/RealmObjectProxy;

    invoke-interface {v8}, Lio/realm/internal/RealmObjectProxy;->realmGet$proxyState()Lio/realm/ProxyState;

    move-result-object v8

    invoke-virtual {v8}, Lio/realm/ProxyState;->getRealm$realm()Lio/realm/BaseRealm;

    move-result-object v8

    invoke-virtual {v8}, Lio/realm/BaseRealm;->getPath()Ljava/lang/String;

    move-result-object v8

    invoke-virtual/range {p0 .. p0}, Lio/realm/Realm;->getPath()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_1

    move-object/from16 v8, v20

    .line 618
    check-cast v8, Lio/realm/internal/RealmObjectProxy;

    invoke-interface {v8}, Lio/realm/internal/RealmObjectProxy;->realmGet$proxyState()Lio/realm/ProxyState;

    move-result-object v8

    invoke-virtual {v8}, Lio/realm/ProxyState;->getRow$realm()Lio/realm/internal/Row;

    move-result-object v8

    invoke-interface {v8}, Lio/realm/internal/Row;->getIndex()J

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    move-object/from16 v0, p2

    move-object/from16 v1, v20

    invoke-interface {v0, v1, v8}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 621
    :cond_1
    const-wide/16 v12, -0x1

    .local v12, "rowIndex":J
    move-object/from16 v8, v20

    .line 622
    check-cast v8, Lio/realm/RememberNumberResultRealmProxyInterface;

    invoke-interface {v8}, Lio/realm/RememberNumberResultRealmProxyInterface;->realmGet$id()I

    move-result v8

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v21

    .line 623
    .local v21, "primaryKeyValue":Ljava/lang/Integer;
    if-eqz v21, :cond_2

    move-object/from16 v8, v20

    .line 624
    check-cast v8, Lio/realm/RememberNumberResultRealmProxyInterface;

    invoke-interface {v8}, Lio/realm/RememberNumberResultRealmProxyInterface;->realmGet$id()I

    move-result v8

    int-to-long v8, v8

    invoke-static/range {v4 .. v9}, Lio/realm/internal/Table;->nativeFindFirstInt(JJJ)J

    move-result-wide v12

    .line 626
    :cond_2
    const-wide/16 v8, -0x1

    cmp-long v8, v12, v8

    if-nez v8, :cond_3

    move-object/from16 v8, v20

    .line 627
    check-cast v8, Lio/realm/RememberNumberResultRealmProxyInterface;

    invoke-interface {v8}, Lio/realm/RememberNumberResultRealmProxyInterface;->realmGet$id()I

    move-result v8

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    const/4 v9, 0x0

    move-object/from16 v0, v22

    invoke-virtual {v0, v8, v9}, Lio/realm/internal/Table;->addEmptyRowWithPrimaryKey(Ljava/lang/Object;Z)J

    move-result-wide v12

    .line 629
    :cond_3
    invoke-static {v12, v13}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    move-object/from16 v0, p2

    move-object/from16 v1, v20

    invoke-interface {v0, v1, v8}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 630
    move-object/from16 v0, v18

    iget-wide v10, v0, Lio/realm/RememberNumberResultRealmProxy$RememberNumberResultColumnInfo;->scoreIndex:J

    move-object/from16 v8, v20

    check-cast v8, Lio/realm/RememberNumberResultRealmProxyInterface;

    invoke-interface {v8}, Lio/realm/RememberNumberResultRealmProxyInterface;->realmGet$score()I

    move-result v8

    int-to-long v14, v8

    const/16 v16, 0x0

    move-wide v8, v4

    invoke-static/range {v8 .. v16}, Lio/realm/internal/Table;->nativeSetLong(JJJJZ)V

    .line 631
    move-object/from16 v0, v18

    iget-wide v10, v0, Lio/realm/RememberNumberResultRealmProxy$RememberNumberResultColumnInfo;->unixTimeIndex:J

    move-object/from16 v8, v20

    check-cast v8, Lio/realm/RememberNumberResultRealmProxyInterface;

    invoke-interface {v8}, Lio/realm/RememberNumberResultRealmProxyInterface;->realmGet$unixTime()J

    move-result-wide v14

    const/16 v16, 0x0

    move-wide v8, v4

    invoke-static/range {v8 .. v16}, Lio/realm/internal/Table;->nativeSetLong(JJJJZ)V

    move-object/from16 v8, v20

    .line 633
    check-cast v8, Lio/realm/RememberNumberResultRealmProxyInterface;

    invoke-interface {v8}, Lio/realm/RememberNumberResultRealmProxyInterface;->realmGet$config()Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberConfig;

    move-result-object v19

    .line 634
    .local v19, "configObj":Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberConfig;
    if-eqz v19, :cond_5

    .line 635
    move-object/from16 v0, p2

    move-object/from16 v1, v19

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v17

    check-cast v17, Ljava/lang/Long;

    .line 636
    .local v17, "cacheconfig":Ljava/lang/Long;
    if-nez v17, :cond_4

    .line 637
    move-object/from16 v0, p0

    move-object/from16 v1, v19

    move-object/from16 v2, p2

    invoke-static {v0, v1, v2}, Lio/realm/RememberNumberConfigRealmProxy;->insertOrUpdate(Lio/realm/Realm;Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberConfig;Ljava/util/Map;)J

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v17

    .line 639
    :cond_4
    move-object/from16 v0, v18

    iget-wide v10, v0, Lio/realm/RememberNumberResultRealmProxy$RememberNumberResultColumnInfo;->configIndex:J

    invoke-virtual/range {v17 .. v17}, Ljava/lang/Long;->longValue()J

    move-result-wide v14

    const/16 v16, 0x0

    move-wide v8, v4

    invoke-static/range {v8 .. v16}, Lio/realm/internal/Table;->nativeSetLink(JJJJZ)V

    goto/16 :goto_0

    .line 641
    .end local v17    # "cacheconfig":Ljava/lang/Long;
    :cond_5
    move-object/from16 v0, v18

    iget-wide v10, v0, Lio/realm/RememberNumberResultRealmProxy$RememberNumberResultColumnInfo;->configIndex:J

    move-wide v8, v4

    invoke-static/range {v8 .. v13}, Lio/realm/internal/Table;->nativeNullifyLink(JJJ)V

    goto/16 :goto_0

    .line 645
    .end local v12    # "rowIndex":J
    .end local v19    # "configObj":Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberConfig;
    .end local v21    # "primaryKeyValue":Ljava/lang/Integer;
    :cond_6
    return-void
.end method

.method static update(Lio/realm/Realm;Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberResult;Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberResult;Ljava/util/Map;)Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberResult;
    .locals 6
    .param p0, "realm"    # Lio/realm/Realm;
    .param p1, "realmObject"    # Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberResult;
    .param p2, "newObject"    # Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberResult;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/realm/Realm;",
            "Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberResult;",
            "Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberResult;",
            "Ljava/util/Map",
            "<",
            "Lio/realm/RealmModel;",
            "Lio/realm/internal/RealmObjectProxy;",
            ">;)",
            "Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberResult;"
        }
    .end annotation

    .prologue
    .line 675
    .local p3, "cache":Ljava/util/Map;, "Ljava/util/Map<Lio/realm/RealmModel;Lio/realm/internal/RealmObjectProxy;>;"
    move-object v2, p1

    check-cast v2, Lio/realm/RememberNumberResultRealmProxyInterface;

    move-object v3, p2

    check-cast v3, Lio/realm/RememberNumberResultRealmProxyInterface;

    invoke-interface {v3}, Lio/realm/RememberNumberResultRealmProxyInterface;->realmGet$score()I

    move-result v3

    invoke-interface {v2, v3}, Lio/realm/RememberNumberResultRealmProxyInterface;->realmSet$score(I)V

    move-object v2, p1

    .line 676
    check-cast v2, Lio/realm/RememberNumberResultRealmProxyInterface;

    move-object v3, p2

    check-cast v3, Lio/realm/RememberNumberResultRealmProxyInterface;

    invoke-interface {v3}, Lio/realm/RememberNumberResultRealmProxyInterface;->realmGet$unixTime()J

    move-result-wide v4

    invoke-interface {v2, v4, v5}, Lio/realm/RememberNumberResultRealmProxyInterface;->realmSet$unixTime(J)V

    .line 677
    check-cast p2, Lio/realm/RememberNumberResultRealmProxyInterface;

    .end local p2    # "newObject":Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberResult;
    invoke-interface {p2}, Lio/realm/RememberNumberResultRealmProxyInterface;->realmGet$config()Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberConfig;

    move-result-object v1

    .line 678
    .local v1, "configObj":Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberConfig;
    if-eqz v1, :cond_1

    .line 679
    invoke-interface {p3, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberConfig;

    .line 680
    .local v0, "cacheconfig":Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberConfig;
    if-eqz v0, :cond_0

    move-object v2, p1

    .line 681
    check-cast v2, Lio/realm/RememberNumberResultRealmProxyInterface;

    invoke-interface {v2, v0}, Lio/realm/RememberNumberResultRealmProxyInterface;->realmSet$config(Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberConfig;)V

    .line 688
    .end local v0    # "cacheconfig":Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberConfig;
    :goto_0
    return-object p1

    .restart local v0    # "cacheconfig":Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberConfig;
    :cond_0
    move-object v2, p1

    .line 683
    check-cast v2, Lio/realm/RememberNumberResultRealmProxyInterface;

    const/4 v3, 0x1

    invoke-static {p0, v1, v3, p3}, Lio/realm/RememberNumberConfigRealmProxy;->copyOrUpdate(Lio/realm/Realm;Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberConfig;ZLjava/util/Map;)Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberConfig;

    move-result-object v3

    invoke-interface {v2, v3}, Lio/realm/RememberNumberResultRealmProxyInterface;->realmSet$config(Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberConfig;)V

    goto :goto_0

    .end local v0    # "cacheconfig":Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberConfig;
    :cond_1
    move-object v2, p1

    .line 686
    check-cast v2, Lio/realm/RememberNumberResultRealmProxyInterface;

    const/4 v3, 0x0

    invoke-interface {v2, v3}, Lio/realm/RememberNumberResultRealmProxyInterface;->realmSet$config(Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberConfig;)V

    goto :goto_0
.end method

.method public static validateTable(Lio/realm/internal/SharedRealm;Z)Lio/realm/RememberNumberResultRealmProxy$RememberNumberResultColumnInfo;
    .locals 14
    .param p0, "sharedRealm"    # Lio/realm/internal/SharedRealm;
    .param p1, "allowExtraColumns"    # Z

    .prologue
    const-wide/16 v10, 0x4

    .line 241
    const-string v8, "class_RememberNumberResult"

    invoke-virtual {p0, v8}, Lio/realm/internal/SharedRealm;->hasTable(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_13

    .line 242
    const-string v8, "class_RememberNumberResult"

    invoke-virtual {p0, v8}, Lio/realm/internal/SharedRealm;->getTable(Ljava/lang/String;)Lio/realm/internal/Table;

    move-result-object v6

    .line 243
    .local v6, "table":Lio/realm/internal/Table;
    invoke-virtual {v6}, Lio/realm/internal/Table;->getColumnCount()J

    move-result-wide v0

    .line 244
    .local v0, "columnCount":J
    cmp-long v8, v0, v10

    if-eqz v8, :cond_1

    .line 245
    cmp-long v8, v0, v10

    if-gez v8, :cond_0

    .line 246
    new-instance v8, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->getPath()Ljava/lang/String;

    move-result-object v9

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "Field count is less than expected - expected 4 but was "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-direct {v8, v9, v10}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v8

    .line 248
    :cond_0
    if-eqz p1, :cond_2

    .line 249
    const-string v8, "Field count is more than expected - expected 4 but was %1$d"

    const/4 v9, 0x1

    new-array v9, v9, [Ljava/lang/Object;

    const/4 v10, 0x0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v11

    aput-object v11, v9, v10

    invoke-static {v8, v9}, Lio/realm/log/RealmLog;->debug(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 254
    :cond_1
    new-instance v3, Ljava/util/HashMap;

    invoke-direct {v3}, Ljava/util/HashMap;-><init>()V

    .line 255
    .local v3, "columnTypes":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lio/realm/RealmFieldType;>;"
    const-wide/16 v4, 0x0

    .local v4, "i":J
    :goto_0
    cmp-long v8, v4, v0

    if-gez v8, :cond_3

    .line 256
    invoke-virtual {v6, v4, v5}, Lio/realm/internal/Table;->getColumnName(J)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v4, v5}, Lio/realm/internal/Table;->getColumnType(J)Lio/realm/RealmFieldType;

    move-result-object v9

    invoke-interface {v3, v8, v9}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 255
    const-wide/16 v8, 0x1

    add-long/2addr v4, v8

    goto :goto_0

    .line 251
    .end local v3    # "columnTypes":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lio/realm/RealmFieldType;>;"
    .end local v4    # "i":J
    :cond_2
    new-instance v8, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->getPath()Ljava/lang/String;

    move-result-object v9

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "Field count is more than expected - expected 4 but was "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-direct {v8, v9, v10}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v8

    .line 259
    .restart local v3    # "columnTypes":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lio/realm/RealmFieldType;>;"
    .restart local v4    # "i":J
    :cond_3
    new-instance v2, Lio/realm/RememberNumberResultRealmProxy$RememberNumberResultColumnInfo;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->getPath()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v2, v8, v6}, Lio/realm/RememberNumberResultRealmProxy$RememberNumberResultColumnInfo;-><init>(Ljava/lang/String;Lio/realm/internal/Table;)V

    .line 261
    .local v2, "columnInfo":Lio/realm/RememberNumberResultRealmProxy$RememberNumberResultColumnInfo;
    invoke-virtual {v6}, Lio/realm/internal/Table;->hasPrimaryKey()Z

    move-result v8

    if-nez v8, :cond_4

    .line 262
    new-instance v8, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->getPath()Ljava/lang/String;

    move-result-object v9

    const-string v10, "Primary key not defined for field \'id\' in existing Realm file. @PrimaryKey was added."

    invoke-direct {v8, v9, v10}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v8

    .line 264
    :cond_4
    invoke-virtual {v6}, Lio/realm/internal/Table;->getPrimaryKey()J

    move-result-wide v8

    iget-wide v10, v2, Lio/realm/RememberNumberResultRealmProxy$RememberNumberResultColumnInfo;->idIndex:J

    cmp-long v8, v8, v10

    if-eqz v8, :cond_5

    .line 265
    new-instance v8, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->getPath()Ljava/lang/String;

    move-result-object v9

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "Primary Key annotation definition was changed, from field "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v6}, Lio/realm/internal/Table;->getPrimaryKey()J

    move-result-wide v12

    invoke-virtual {v6, v12, v13}, Lio/realm/internal/Table;->getColumnName(J)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, " to field id"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-direct {v8, v9, v10}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v8

    .line 269
    :cond_5
    const-string v8, "id"

    invoke-interface {v3, v8}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v8

    if-nez v8, :cond_6

    .line 270
    new-instance v8, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->getPath()Ljava/lang/String;

    move-result-object v9

    const-string v10, "Missing field \'id\' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn()."

    invoke-direct {v8, v9, v10}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v8

    .line 272
    :cond_6
    const-string v8, "id"

    invoke-interface {v3, v8}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    sget-object v9, Lio/realm/RealmFieldType;->INTEGER:Lio/realm/RealmFieldType;

    if-eq v8, v9, :cond_7

    .line 273
    new-instance v8, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->getPath()Ljava/lang/String;

    move-result-object v9

    const-string v10, "Invalid type \'int\' for field \'id\' in existing Realm file."

    invoke-direct {v8, v9, v10}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v8

    .line 275
    :cond_7
    iget-wide v8, v2, Lio/realm/RememberNumberResultRealmProxy$RememberNumberResultColumnInfo;->idIndex:J

    invoke-virtual {v6, v8, v9}, Lio/realm/internal/Table;->isColumnNullable(J)Z

    move-result v8

    if-eqz v8, :cond_8

    iget-wide v8, v2, Lio/realm/RememberNumberResultRealmProxy$RememberNumberResultColumnInfo;->idIndex:J

    invoke-virtual {v6, v8, v9}, Lio/realm/internal/Table;->findFirstNull(J)J

    move-result-wide v8

    const-wide/16 v10, -0x1

    cmp-long v8, v8, v10

    if-eqz v8, :cond_8

    .line 276
    new-instance v8, Ljava/lang/IllegalStateException;

    const-string v9, "Cannot migrate an object with null value in field \'id\'. Either maintain the same type for primary key field \'id\', or remove the object with null value before migration."

    invoke-direct {v8, v9}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v8

    .line 278
    :cond_8
    const-string v8, "id"

    invoke-virtual {v6, v8}, Lio/realm/internal/Table;->getColumnIndex(Ljava/lang/String;)J

    move-result-wide v8

    invoke-virtual {v6, v8, v9}, Lio/realm/internal/Table;->hasSearchIndex(J)Z

    move-result v8

    if-nez v8, :cond_9

    .line 279
    new-instance v8, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->getPath()Ljava/lang/String;

    move-result-object v9

    const-string v10, "Index not defined for field \'id\' in existing Realm file. Either set @Index or migrate using io.realm.internal.Table.removeSearchIndex()."

    invoke-direct {v8, v9, v10}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v8

    .line 281
    :cond_9
    const-string v8, "score"

    invoke-interface {v3, v8}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v8

    if-nez v8, :cond_a

    .line 282
    new-instance v8, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->getPath()Ljava/lang/String;

    move-result-object v9

    const-string v10, "Missing field \'score\' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn()."

    invoke-direct {v8, v9, v10}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v8

    .line 284
    :cond_a
    const-string v8, "score"

    invoke-interface {v3, v8}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    sget-object v9, Lio/realm/RealmFieldType;->INTEGER:Lio/realm/RealmFieldType;

    if-eq v8, v9, :cond_b

    .line 285
    new-instance v8, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->getPath()Ljava/lang/String;

    move-result-object v9

    const-string v10, "Invalid type \'int\' for field \'score\' in existing Realm file."

    invoke-direct {v8, v9, v10}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v8

    .line 287
    :cond_b
    iget-wide v8, v2, Lio/realm/RememberNumberResultRealmProxy$RememberNumberResultColumnInfo;->scoreIndex:J

    invoke-virtual {v6, v8, v9}, Lio/realm/internal/Table;->isColumnNullable(J)Z

    move-result v8

    if-eqz v8, :cond_c

    .line 288
    new-instance v8, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->getPath()Ljava/lang/String;

    move-result-object v9

    const-string v10, "Field \'score\' does support null values in the existing Realm file. Use corresponding boxed type for field \'score\' or migrate using RealmObjectSchema.setNullable()."

    invoke-direct {v8, v9, v10}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v8

    .line 290
    :cond_c
    const-string v8, "unixTime"

    invoke-interface {v3, v8}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v8

    if-nez v8, :cond_d

    .line 291
    new-instance v8, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->getPath()Ljava/lang/String;

    move-result-object v9

    const-string v10, "Missing field \'unixTime\' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn()."

    invoke-direct {v8, v9, v10}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v8

    .line 293
    :cond_d
    const-string v8, "unixTime"

    invoke-interface {v3, v8}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    sget-object v9, Lio/realm/RealmFieldType;->INTEGER:Lio/realm/RealmFieldType;

    if-eq v8, v9, :cond_e

    .line 294
    new-instance v8, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->getPath()Ljava/lang/String;

    move-result-object v9

    const-string v10, "Invalid type \'long\' for field \'unixTime\' in existing Realm file."

    invoke-direct {v8, v9, v10}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v8

    .line 296
    :cond_e
    iget-wide v8, v2, Lio/realm/RememberNumberResultRealmProxy$RememberNumberResultColumnInfo;->unixTimeIndex:J

    invoke-virtual {v6, v8, v9}, Lio/realm/internal/Table;->isColumnNullable(J)Z

    move-result v8

    if-eqz v8, :cond_f

    .line 297
    new-instance v8, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->getPath()Ljava/lang/String;

    move-result-object v9

    const-string v10, "Field \'unixTime\' does support null values in the existing Realm file. Use corresponding boxed type for field \'unixTime\' or migrate using RealmObjectSchema.setNullable()."

    invoke-direct {v8, v9, v10}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v8

    .line 299
    :cond_f
    const-string v8, "config"

    invoke-interface {v3, v8}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v8

    if-nez v8, :cond_10

    .line 300
    new-instance v8, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->getPath()Ljava/lang/String;

    move-result-object v9

    const-string v10, "Missing field \'config\' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn()."

    invoke-direct {v8, v9, v10}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v8

    .line 302
    :cond_10
    const-string v8, "config"

    invoke-interface {v3, v8}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    sget-object v9, Lio/realm/RealmFieldType;->OBJECT:Lio/realm/RealmFieldType;

    if-eq v8, v9, :cond_11

    .line 303
    new-instance v8, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->getPath()Ljava/lang/String;

    move-result-object v9

    const-string v10, "Invalid type \'RememberNumberConfig\' for field \'config\'"

    invoke-direct {v8, v9, v10}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v8

    .line 305
    :cond_11
    const-string v8, "class_RememberNumberConfig"

    invoke-virtual {p0, v8}, Lio/realm/internal/SharedRealm;->hasTable(Ljava/lang/String;)Z

    move-result v8

    if-nez v8, :cond_12

    .line 306
    new-instance v8, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->getPath()Ljava/lang/String;

    move-result-object v9

    const-string v10, "Missing class \'class_RememberNumberConfig\' for field \'config\'"

    invoke-direct {v8, v9, v10}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v8

    .line 308
    :cond_12
    const-string v8, "class_RememberNumberConfig"

    invoke-virtual {p0, v8}, Lio/realm/internal/SharedRealm;->getTable(Ljava/lang/String;)Lio/realm/internal/Table;

    move-result-object v7

    .line 309
    .local v7, "table_3":Lio/realm/internal/Table;
    iget-wide v8, v2, Lio/realm/RememberNumberResultRealmProxy$RememberNumberResultColumnInfo;->configIndex:J

    invoke-virtual {v6, v8, v9}, Lio/realm/internal/Table;->getLinkTarget(J)Lio/realm/internal/Table;

    move-result-object v8

    invoke-virtual {v8, v7}, Lio/realm/internal/Table;->hasSameSchema(Lio/realm/internal/Table;)Z

    move-result v8

    if-nez v8, :cond_14

    .line 310
    new-instance v8, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->getPath()Ljava/lang/String;

    move-result-object v9

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "Invalid RealmObject for field \'config\': \'"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    iget-wide v12, v2, Lio/realm/RememberNumberResultRealmProxy$RememberNumberResultColumnInfo;->configIndex:J

    invoke-virtual {v6, v12, v13}, Lio/realm/internal/Table;->getLinkTarget(J)Lio/realm/internal/Table;

    move-result-object v11

    invoke-virtual {v11}, Lio/realm/internal/Table;->getName()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, "\' expected - was \'"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v7}, Lio/realm/internal/Table;->getName()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, "\'"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-direct {v8, v9, v10}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v8

    .line 314
    .end local v0    # "columnCount":J
    .end local v2    # "columnInfo":Lio/realm/RememberNumberResultRealmProxy$RememberNumberResultColumnInfo;
    .end local v3    # "columnTypes":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lio/realm/RealmFieldType;>;"
    .end local v4    # "i":J
    .end local v6    # "table":Lio/realm/internal/Table;
    .end local v7    # "table_3":Lio/realm/internal/Table;
    :cond_13
    new-instance v8, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->getPath()Ljava/lang/String;

    move-result-object v9

    const-string v10, "The \'RememberNumberResult\' class is missing from the schema for this Realm."

    invoke-direct {v8, v9, v10}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v8

    .line 312
    .restart local v0    # "columnCount":J
    .restart local v2    # "columnInfo":Lio/realm/RememberNumberResultRealmProxy$RememberNumberResultColumnInfo;
    .restart local v3    # "columnTypes":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lio/realm/RealmFieldType;>;"
    .restart local v4    # "i":J
    .restart local v6    # "table":Lio/realm/internal/Table;
    .restart local v7    # "table_3":Lio/realm/internal/Table;
    :cond_14
    return-object v2
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 12
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v5, 0x1

    const/4 v6, 0x0

    .line 736
    if-ne p0, p1, :cond_1

    .line 750
    :cond_0
    :goto_0
    return v5

    .line 737
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v7

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v8

    if-eq v7, v8, :cond_3

    :cond_2
    move v5, v6

    goto :goto_0

    :cond_3
    move-object v0, p1

    .line 738
    check-cast v0, Lio/realm/RememberNumberResultRealmProxy;

    .line 740
    .local v0, "aRememberNumberResult":Lio/realm/RememberNumberResultRealmProxy;
    iget-object v7, p0, Lio/realm/RememberNumberResultRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v7}, Lio/realm/ProxyState;->getRealm$realm()Lio/realm/BaseRealm;

    move-result-object v7

    invoke-virtual {v7}, Lio/realm/BaseRealm;->getPath()Ljava/lang/String;

    move-result-object v3

    .line 741
    .local v3, "path":Ljava/lang/String;
    iget-object v7, v0, Lio/realm/RememberNumberResultRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v7}, Lio/realm/ProxyState;->getRealm$realm()Lio/realm/BaseRealm;

    move-result-object v7

    invoke-virtual {v7}, Lio/realm/BaseRealm;->getPath()Ljava/lang/String;

    move-result-object v1

    .line 742
    .local v1, "otherPath":Ljava/lang/String;
    if-eqz v3, :cond_5

    invoke-virtual {v3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_6

    :cond_4
    move v5, v6

    goto :goto_0

    :cond_5
    if-nez v1, :cond_4

    .line 744
    :cond_6
    iget-object v7, p0, Lio/realm/RememberNumberResultRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v7}, Lio/realm/ProxyState;->getRow$realm()Lio/realm/internal/Row;

    move-result-object v7

    invoke-interface {v7}, Lio/realm/internal/Row;->getTable()Lio/realm/internal/Table;

    move-result-object v7

    invoke-virtual {v7}, Lio/realm/internal/Table;->getName()Ljava/lang/String;

    move-result-object v4

    .line 745
    .local v4, "tableName":Ljava/lang/String;
    iget-object v7, v0, Lio/realm/RememberNumberResultRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v7}, Lio/realm/ProxyState;->getRow$realm()Lio/realm/internal/Row;

    move-result-object v7

    invoke-interface {v7}, Lio/realm/internal/Row;->getTable()Lio/realm/internal/Table;

    move-result-object v7

    invoke-virtual {v7}, Lio/realm/internal/Table;->getName()Ljava/lang/String;

    move-result-object v2

    .line 746
    .local v2, "otherTableName":Ljava/lang/String;
    if-eqz v4, :cond_8

    invoke-virtual {v4, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_9

    :cond_7
    move v5, v6

    goto :goto_0

    :cond_8
    if-nez v2, :cond_7

    .line 748
    :cond_9
    iget-object v7, p0, Lio/realm/RememberNumberResultRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v7}, Lio/realm/ProxyState;->getRow$realm()Lio/realm/internal/Row;

    move-result-object v7

    invoke-interface {v7}, Lio/realm/internal/Row;->getIndex()J

    move-result-wide v8

    iget-object v7, v0, Lio/realm/RememberNumberResultRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v7}, Lio/realm/ProxyState;->getRow$realm()Lio/realm/internal/Row;

    move-result-object v7

    invoke-interface {v7}, Lio/realm/internal/Row;->getIndex()J

    move-result-wide v10

    cmp-long v7, v8, v10

    if-eqz v7, :cond_0

    move v5, v6

    goto :goto_0
.end method

.method public hashCode()I
    .locals 8

    .prologue
    const/4 v5, 0x0

    .line 723
    iget-object v6, p0, Lio/realm/RememberNumberResultRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v6}, Lio/realm/ProxyState;->getRealm$realm()Lio/realm/BaseRealm;

    move-result-object v6

    invoke-virtual {v6}, Lio/realm/BaseRealm;->getPath()Ljava/lang/String;

    move-result-object v0

    .line 724
    .local v0, "realmName":Ljava/lang/String;
    iget-object v6, p0, Lio/realm/RememberNumberResultRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v6}, Lio/realm/ProxyState;->getRow$realm()Lio/realm/internal/Row;

    move-result-object v6

    invoke-interface {v6}, Lio/realm/internal/Row;->getTable()Lio/realm/internal/Table;

    move-result-object v6

    invoke-virtual {v6}, Lio/realm/internal/Table;->getName()Ljava/lang/String;

    move-result-object v4

    .line 725
    .local v4, "tableName":Ljava/lang/String;
    iget-object v6, p0, Lio/realm/RememberNumberResultRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v6}, Lio/realm/ProxyState;->getRow$realm()Lio/realm/internal/Row;

    move-result-object v6

    invoke-interface {v6}, Lio/realm/internal/Row;->getIndex()J

    move-result-wide v2

    .line 727
    .local v2, "rowIndex":J
    const/16 v1, 0x11

    .line 728
    .local v1, "result":I
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v6

    :goto_0
    add-int/lit16 v1, v6, 0x20f

    .line 729
    mul-int/lit8 v6, v1, 0x1f

    if-eqz v4, :cond_0

    invoke-virtual {v4}, Ljava/lang/String;->hashCode()I

    move-result v5

    :cond_0
    add-int v1, v6, v5

    .line 730
    mul-int/lit8 v5, v1, 0x1f

    const/16 v6, 0x20

    ushr-long v6, v2, v6

    xor-long/2addr v6, v2

    long-to-int v6, v6

    add-int v1, v5, v6

    .line 731
    return v1

    :cond_1
    move v6, v5

    .line 728
    goto :goto_0
.end method

.method public realm$injectObjectContext()V
    .locals 3

    .prologue
    .line 91
    iget-object v1, p0, Lio/realm/RememberNumberResultRealmProxy;->proxyState:Lio/realm/ProxyState;

    if-eqz v1, :cond_0

    .line 101
    :goto_0
    return-void

    .line 94
    :cond_0
    sget-object v1, Lio/realm/BaseRealm;->objectContext:Lio/realm/BaseRealm$ThreadLocalRealmObjectContext;

    invoke-virtual {v1}, Lio/realm/BaseRealm$ThreadLocalRealmObjectContext;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/realm/BaseRealm$RealmObjectContext;

    .line 95
    .local v0, "context":Lio/realm/BaseRealm$RealmObjectContext;
    invoke-virtual {v0}, Lio/realm/BaseRealm$RealmObjectContext;->getColumnInfo()Lio/realm/internal/ColumnInfo;

    move-result-object v1

    check-cast v1, Lio/realm/RememberNumberResultRealmProxy$RememberNumberResultColumnInfo;

    iput-object v1, p0, Lio/realm/RememberNumberResultRealmProxy;->columnInfo:Lio/realm/RememberNumberResultRealmProxy$RememberNumberResultColumnInfo;

    .line 96
    new-instance v1, Lio/realm/ProxyState;

    invoke-direct {v1, p0}, Lio/realm/ProxyState;-><init>(Lio/realm/RealmModel;)V

    iput-object v1, p0, Lio/realm/RememberNumberResultRealmProxy;->proxyState:Lio/realm/ProxyState;

    .line 97
    iget-object v1, p0, Lio/realm/RememberNumberResultRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v0}, Lio/realm/BaseRealm$RealmObjectContext;->getRealm()Lio/realm/BaseRealm;

    move-result-object v2

    invoke-virtual {v1, v2}, Lio/realm/ProxyState;->setRealm$realm(Lio/realm/BaseRealm;)V

    .line 98
    iget-object v1, p0, Lio/realm/RememberNumberResultRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v0}, Lio/realm/BaseRealm$RealmObjectContext;->getRow()Lio/realm/internal/Row;

    move-result-object v2

    invoke-virtual {v1, v2}, Lio/realm/ProxyState;->setRow$realm(Lio/realm/internal/Row;)V

    .line 99
    iget-object v1, p0, Lio/realm/RememberNumberResultRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v0}, Lio/realm/BaseRealm$RealmObjectContext;->getAcceptDefaultValue()Z

    move-result v2

    invoke-virtual {v1, v2}, Lio/realm/ProxyState;->setAcceptDefaultValue$realm(Z)V

    .line 100
    iget-object v1, p0, Lio/realm/RememberNumberResultRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v0}, Lio/realm/BaseRealm$RealmObjectContext;->getExcludeFields()Ljava/util/List;

    move-result-object v2

    invoke-virtual {v1, v2}, Lio/realm/ProxyState;->setExcludeFields$realm(Ljava/util/List;)V

    goto :goto_0
.end method

.method public realmGet$config()Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberConfig;
    .locals 6

    .prologue
    .line 160
    iget-object v0, p0, Lio/realm/RememberNumberResultRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v0}, Lio/realm/ProxyState;->getRealm$realm()Lio/realm/BaseRealm;

    move-result-object v0

    invoke-virtual {v0}, Lio/realm/BaseRealm;->checkIfValid()V

    .line 161
    iget-object v0, p0, Lio/realm/RememberNumberResultRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v0}, Lio/realm/ProxyState;->getRow$realm()Lio/realm/internal/Row;

    move-result-object v0

    iget-object v1, p0, Lio/realm/RememberNumberResultRealmProxy;->columnInfo:Lio/realm/RememberNumberResultRealmProxy$RememberNumberResultColumnInfo;

    iget-wide v2, v1, Lio/realm/RememberNumberResultRealmProxy$RememberNumberResultColumnInfo;->configIndex:J

    invoke-interface {v0, v2, v3}, Lio/realm/internal/Row;->isNullLink(J)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 162
    const/4 v0, 0x0

    .line 164
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lio/realm/RememberNumberResultRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v0}, Lio/realm/ProxyState;->getRealm$realm()Lio/realm/BaseRealm;

    move-result-object v0

    const-class v1, Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberConfig;

    iget-object v2, p0, Lio/realm/RememberNumberResultRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v2}, Lio/realm/ProxyState;->getRow$realm()Lio/realm/internal/Row;

    move-result-object v2

    iget-object v3, p0, Lio/realm/RememberNumberResultRealmProxy;->columnInfo:Lio/realm/RememberNumberResultRealmProxy$RememberNumberResultColumnInfo;

    iget-wide v4, v3, Lio/realm/RememberNumberResultRealmProxy$RememberNumberResultColumnInfo;->configIndex:J

    invoke-interface {v2, v4, v5}, Lio/realm/internal/Row;->getLink(J)J

    move-result-wide v2

    const/4 v4, 0x0

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v5

    invoke-virtual/range {v0 .. v5}, Lio/realm/BaseRealm;->get(Ljava/lang/Class;JZLjava/util/List;)Lio/realm/RealmModel;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberConfig;

    goto :goto_0
.end method

.method public realmGet$id()I
    .locals 4

    .prologue
    .line 105
    iget-object v0, p0, Lio/realm/RememberNumberResultRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v0}, Lio/realm/ProxyState;->getRealm$realm()Lio/realm/BaseRealm;

    move-result-object v0

    invoke-virtual {v0}, Lio/realm/BaseRealm;->checkIfValid()V

    .line 106
    iget-object v0, p0, Lio/realm/RememberNumberResultRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v0}, Lio/realm/ProxyState;->getRow$realm()Lio/realm/internal/Row;

    move-result-object v0

    iget-object v1, p0, Lio/realm/RememberNumberResultRealmProxy;->columnInfo:Lio/realm/RememberNumberResultRealmProxy$RememberNumberResultColumnInfo;

    iget-wide v2, v1, Lio/realm/RememberNumberResultRealmProxy$RememberNumberResultColumnInfo;->idIndex:J

    invoke-interface {v0, v2, v3}, Lio/realm/internal/Row;->getLong(J)J

    move-result-wide v0

    long-to-int v0, v0

    return v0
.end method

.method public realmGet$proxyState()Lio/realm/ProxyState;
    .locals 1

    .prologue
    .line 718
    iget-object v0, p0, Lio/realm/RememberNumberResultRealmProxy;->proxyState:Lio/realm/ProxyState;

    return-object v0
.end method

.method public realmGet$score()I
    .locals 4

    .prologue
    .line 121
    iget-object v0, p0, Lio/realm/RememberNumberResultRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v0}, Lio/realm/ProxyState;->getRealm$realm()Lio/realm/BaseRealm;

    move-result-object v0

    invoke-virtual {v0}, Lio/realm/BaseRealm;->checkIfValid()V

    .line 122
    iget-object v0, p0, Lio/realm/RememberNumberResultRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v0}, Lio/realm/ProxyState;->getRow$realm()Lio/realm/internal/Row;

    move-result-object v0

    iget-object v1, p0, Lio/realm/RememberNumberResultRealmProxy;->columnInfo:Lio/realm/RememberNumberResultRealmProxy$RememberNumberResultColumnInfo;

    iget-wide v2, v1, Lio/realm/RememberNumberResultRealmProxy$RememberNumberResultColumnInfo;->scoreIndex:J

    invoke-interface {v0, v2, v3}, Lio/realm/internal/Row;->getLong(J)J

    move-result-wide v0

    long-to-int v0, v0

    return v0
.end method

.method public realmGet$unixTime()J
    .locals 4

    .prologue
    .line 141
    iget-object v0, p0, Lio/realm/RememberNumberResultRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v0}, Lio/realm/ProxyState;->getRealm$realm()Lio/realm/BaseRealm;

    move-result-object v0

    invoke-virtual {v0}, Lio/realm/BaseRealm;->checkIfValid()V

    .line 142
    iget-object v0, p0, Lio/realm/RememberNumberResultRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v0}, Lio/realm/ProxyState;->getRow$realm()Lio/realm/internal/Row;

    move-result-object v0

    iget-object v1, p0, Lio/realm/RememberNumberResultRealmProxy;->columnInfo:Lio/realm/RememberNumberResultRealmProxy$RememberNumberResultColumnInfo;

    iget-wide v2, v1, Lio/realm/RememberNumberResultRealmProxy$RememberNumberResultColumnInfo;->unixTimeIndex:J

    invoke-interface {v0, v2, v3}, Lio/realm/internal/Row;->getLong(J)J

    move-result-wide v0

    return-wide v0
.end method

.method public realmSet$config(Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberConfig;)V
    .locals 9
    .param p1, "value"    # Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberConfig;

    .prologue
    .line 168
    iget-object v1, p0, Lio/realm/RememberNumberResultRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v1}, Lio/realm/ProxyState;->isUnderConstruction()Z

    move-result v1

    if-eqz v1, :cond_6

    .line 169
    iget-object v1, p0, Lio/realm/RememberNumberResultRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v1}, Lio/realm/ProxyState;->getAcceptDefaultValue$realm()Z

    move-result v1

    if-nez v1, :cond_1

    .line 206
    :cond_0
    :goto_0
    return-void

    .line 172
    :cond_1
    iget-object v1, p0, Lio/realm/RememberNumberResultRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v1}, Lio/realm/ProxyState;->getExcludeFields$realm()Ljava/util/List;

    move-result-object v1

    const-string v2, "config"

    invoke-interface {v1, v2}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 175
    if-eqz p1, :cond_2

    invoke-static {p1}, Lio/realm/RealmObject;->isManaged(Lio/realm/RealmModel;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 176
    iget-object v1, p0, Lio/realm/RememberNumberResultRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v1}, Lio/realm/ProxyState;->getRealm$realm()Lio/realm/BaseRealm;

    move-result-object v1

    check-cast v1, Lio/realm/Realm;

    invoke-virtual {v1, p1}, Lio/realm/Realm;->copyToRealm(Lio/realm/RealmModel;)Lio/realm/RealmModel;

    move-result-object p1

    .end local p1    # "value":Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberConfig;
    check-cast p1, Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberConfig;

    .line 178
    .restart local p1    # "value":Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberConfig;
    :cond_2
    iget-object v1, p0, Lio/realm/RememberNumberResultRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v1}, Lio/realm/ProxyState;->getRow$realm()Lio/realm/internal/Row;

    move-result-object v0

    .line 179
    .local v0, "row":Lio/realm/internal/Row;
    if-nez p1, :cond_3

    .line 181
    iget-object v1, p0, Lio/realm/RememberNumberResultRealmProxy;->columnInfo:Lio/realm/RememberNumberResultRealmProxy$RememberNumberResultColumnInfo;

    iget-wide v2, v1, Lio/realm/RememberNumberResultRealmProxy$RememberNumberResultColumnInfo;->configIndex:J

    invoke-interface {v0, v2, v3}, Lio/realm/internal/Row;->nullifyLink(J)V

    goto :goto_0

    .line 184
    :cond_3
    invoke-static {p1}, Lio/realm/RealmObject;->isValid(Lio/realm/RealmModel;)Z

    move-result v1

    if-nez v1, :cond_4

    .line 185
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "\'value\' is not a valid managed object."

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_4
    move-object v1, p1

    .line 187
    check-cast v1, Lio/realm/internal/RealmObjectProxy;

    invoke-interface {v1}, Lio/realm/internal/RealmObjectProxy;->realmGet$proxyState()Lio/realm/ProxyState;

    move-result-object v1

    invoke-virtual {v1}, Lio/realm/ProxyState;->getRealm$realm()Lio/realm/BaseRealm;

    move-result-object v1

    iget-object v2, p0, Lio/realm/RememberNumberResultRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v2}, Lio/realm/ProxyState;->getRealm$realm()Lio/realm/BaseRealm;

    move-result-object v2

    if-eq v1, v2, :cond_5

    .line 188
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "\'value\' belongs to a different Realm."

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 190
    :cond_5
    invoke-interface {v0}, Lio/realm/internal/Row;->getTable()Lio/realm/internal/Table;

    move-result-object v1

    iget-object v2, p0, Lio/realm/RememberNumberResultRealmProxy;->columnInfo:Lio/realm/RememberNumberResultRealmProxy$RememberNumberResultColumnInfo;

    iget-wide v2, v2, Lio/realm/RememberNumberResultRealmProxy$RememberNumberResultColumnInfo;->configIndex:J

    invoke-interface {v0}, Lio/realm/internal/Row;->getIndex()J

    move-result-wide v4

    move-object v6, p1

    check-cast v6, Lio/realm/internal/RealmObjectProxy;

    invoke-interface {v6}, Lio/realm/internal/RealmObjectProxy;->realmGet$proxyState()Lio/realm/ProxyState;

    move-result-object v6

    invoke-virtual {v6}, Lio/realm/ProxyState;->getRow$realm()Lio/realm/internal/Row;

    move-result-object v6

    invoke-interface {v6}, Lio/realm/internal/Row;->getIndex()J

    move-result-wide v6

    const/4 v8, 0x1

    invoke-virtual/range {v1 .. v8}, Lio/realm/internal/Table;->setLink(JJJZ)V

    goto :goto_0

    .line 194
    .end local v0    # "row":Lio/realm/internal/Row;
    :cond_6
    iget-object v1, p0, Lio/realm/RememberNumberResultRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v1}, Lio/realm/ProxyState;->getRealm$realm()Lio/realm/BaseRealm;

    move-result-object v1

    invoke-virtual {v1}, Lio/realm/BaseRealm;->checkIfValid()V

    .line 195
    if-nez p1, :cond_7

    .line 196
    iget-object v1, p0, Lio/realm/RememberNumberResultRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v1}, Lio/realm/ProxyState;->getRow$realm()Lio/realm/internal/Row;

    move-result-object v1

    iget-object v2, p0, Lio/realm/RememberNumberResultRealmProxy;->columnInfo:Lio/realm/RememberNumberResultRealmProxy$RememberNumberResultColumnInfo;

    iget-wide v2, v2, Lio/realm/RememberNumberResultRealmProxy$RememberNumberResultColumnInfo;->configIndex:J

    invoke-interface {v1, v2, v3}, Lio/realm/internal/Row;->nullifyLink(J)V

    goto/16 :goto_0

    .line 199
    :cond_7
    invoke-static {p1}, Lio/realm/RealmObject;->isManaged(Lio/realm/RealmModel;)Z

    move-result v1

    if-eqz v1, :cond_8

    invoke-static {p1}, Lio/realm/RealmObject;->isValid(Lio/realm/RealmModel;)Z

    move-result v1

    if-nez v1, :cond_9

    .line 200
    :cond_8
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "\'value\' is not a valid managed object."

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_9
    move-object v1, p1

    .line 202
    check-cast v1, Lio/realm/internal/RealmObjectProxy;

    invoke-interface {v1}, Lio/realm/internal/RealmObjectProxy;->realmGet$proxyState()Lio/realm/ProxyState;

    move-result-object v1

    invoke-virtual {v1}, Lio/realm/ProxyState;->getRealm$realm()Lio/realm/BaseRealm;

    move-result-object v1

    iget-object v2, p0, Lio/realm/RememberNumberResultRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v2}, Lio/realm/ProxyState;->getRealm$realm()Lio/realm/BaseRealm;

    move-result-object v2

    if-eq v1, v2, :cond_a

    .line 203
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "\'value\' belongs to a different Realm."

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 205
    :cond_a
    iget-object v1, p0, Lio/realm/RememberNumberResultRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v1}, Lio/realm/ProxyState;->getRow$realm()Lio/realm/internal/Row;

    move-result-object v2

    iget-object v1, p0, Lio/realm/RememberNumberResultRealmProxy;->columnInfo:Lio/realm/RememberNumberResultRealmProxy$RememberNumberResultColumnInfo;

    iget-wide v4, v1, Lio/realm/RememberNumberResultRealmProxy$RememberNumberResultColumnInfo;->configIndex:J

    move-object v1, p1

    check-cast v1, Lio/realm/internal/RealmObjectProxy;

    invoke-interface {v1}, Lio/realm/internal/RealmObjectProxy;->realmGet$proxyState()Lio/realm/ProxyState;

    move-result-object v1

    invoke-virtual {v1}, Lio/realm/ProxyState;->getRow$realm()Lio/realm/internal/Row;

    move-result-object v1

    invoke-interface {v1}, Lio/realm/internal/Row;->getIndex()J

    move-result-wide v6

    invoke-interface {v2, v4, v5, v6, v7}, Lio/realm/internal/Row;->setLink(JJ)V

    goto/16 :goto_0
.end method

.method public realmSet$id(I)V
    .locals 2
    .param p1, "value"    # I

    .prologue
    .line 110
    iget-object v0, p0, Lio/realm/RememberNumberResultRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v0}, Lio/realm/ProxyState;->isUnderConstruction()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 112
    return-void

    .line 115
    :cond_0
    iget-object v0, p0, Lio/realm/RememberNumberResultRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v0}, Lio/realm/ProxyState;->getRealm$realm()Lio/realm/BaseRealm;

    move-result-object v0

    invoke-virtual {v0}, Lio/realm/BaseRealm;->checkIfValid()V

    .line 116
    new-instance v0, Lio/realm/exceptions/RealmException;

    const-string v1, "Primary key field \'id\' cannot be changed after object was created."

    invoke-direct {v0, v1}, Lio/realm/exceptions/RealmException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public realmSet$score(I)V
    .locals 9
    .param p1, "value"    # I

    .prologue
    .line 126
    iget-object v1, p0, Lio/realm/RememberNumberResultRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v1}, Lio/realm/ProxyState;->isUnderConstruction()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 127
    iget-object v1, p0, Lio/realm/RememberNumberResultRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v1}, Lio/realm/ProxyState;->getAcceptDefaultValue$realm()Z

    move-result v1

    if-nez v1, :cond_0

    .line 137
    :goto_0
    return-void

    .line 130
    :cond_0
    iget-object v1, p0, Lio/realm/RememberNumberResultRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v1}, Lio/realm/ProxyState;->getRow$realm()Lio/realm/internal/Row;

    move-result-object v0

    .line 131
    .local v0, "row":Lio/realm/internal/Row;
    invoke-interface {v0}, Lio/realm/internal/Row;->getTable()Lio/realm/internal/Table;

    move-result-object v1

    iget-object v2, p0, Lio/realm/RememberNumberResultRealmProxy;->columnInfo:Lio/realm/RememberNumberResultRealmProxy$RememberNumberResultColumnInfo;

    iget-wide v2, v2, Lio/realm/RememberNumberResultRealmProxy$RememberNumberResultColumnInfo;->scoreIndex:J

    invoke-interface {v0}, Lio/realm/internal/Row;->getIndex()J

    move-result-wide v4

    int-to-long v6, p1

    const/4 v8, 0x1

    invoke-virtual/range {v1 .. v8}, Lio/realm/internal/Table;->setLong(JJJZ)V

    goto :goto_0

    .line 135
    .end local v0    # "row":Lio/realm/internal/Row;
    :cond_1
    iget-object v1, p0, Lio/realm/RememberNumberResultRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v1}, Lio/realm/ProxyState;->getRealm$realm()Lio/realm/BaseRealm;

    move-result-object v1

    invoke-virtual {v1}, Lio/realm/BaseRealm;->checkIfValid()V

    .line 136
    iget-object v1, p0, Lio/realm/RememberNumberResultRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v1}, Lio/realm/ProxyState;->getRow$realm()Lio/realm/internal/Row;

    move-result-object v1

    iget-object v2, p0, Lio/realm/RememberNumberResultRealmProxy;->columnInfo:Lio/realm/RememberNumberResultRealmProxy$RememberNumberResultColumnInfo;

    iget-wide v2, v2, Lio/realm/RememberNumberResultRealmProxy$RememberNumberResultColumnInfo;->scoreIndex:J

    int-to-long v4, p1

    invoke-interface {v1, v2, v3, v4, v5}, Lio/realm/internal/Row;->setLong(JJ)V

    goto :goto_0
.end method

.method public realmSet$unixTime(J)V
    .locals 9
    .param p1, "value"    # J

    .prologue
    .line 146
    iget-object v1, p0, Lio/realm/RememberNumberResultRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v1}, Lio/realm/ProxyState;->isUnderConstruction()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 147
    iget-object v1, p0, Lio/realm/RememberNumberResultRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v1}, Lio/realm/ProxyState;->getAcceptDefaultValue$realm()Z

    move-result v1

    if-nez v1, :cond_0

    .line 157
    :goto_0
    return-void

    .line 150
    :cond_0
    iget-object v1, p0, Lio/realm/RememberNumberResultRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v1}, Lio/realm/ProxyState;->getRow$realm()Lio/realm/internal/Row;

    move-result-object v0

    .line 151
    .local v0, "row":Lio/realm/internal/Row;
    invoke-interface {v0}, Lio/realm/internal/Row;->getTable()Lio/realm/internal/Table;

    move-result-object v1

    iget-object v2, p0, Lio/realm/RememberNumberResultRealmProxy;->columnInfo:Lio/realm/RememberNumberResultRealmProxy$RememberNumberResultColumnInfo;

    iget-wide v2, v2, Lio/realm/RememberNumberResultRealmProxy$RememberNumberResultColumnInfo;->unixTimeIndex:J

    invoke-interface {v0}, Lio/realm/internal/Row;->getIndex()J

    move-result-wide v4

    const/4 v8, 0x1

    move-wide v6, p1

    invoke-virtual/range {v1 .. v8}, Lio/realm/internal/Table;->setLong(JJJZ)V

    goto :goto_0

    .line 155
    .end local v0    # "row":Lio/realm/internal/Row;
    :cond_1
    iget-object v1, p0, Lio/realm/RememberNumberResultRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v1}, Lio/realm/ProxyState;->getRealm$realm()Lio/realm/BaseRealm;

    move-result-object v1

    invoke-virtual {v1}, Lio/realm/BaseRealm;->checkIfValid()V

    .line 156
    iget-object v1, p0, Lio/realm/RememberNumberResultRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v1}, Lio/realm/ProxyState;->getRow$realm()Lio/realm/internal/Row;

    move-result-object v1

    iget-object v2, p0, Lio/realm/RememberNumberResultRealmProxy;->columnInfo:Lio/realm/RememberNumberResultRealmProxy$RememberNumberResultColumnInfo;

    iget-wide v2, v2, Lio/realm/RememberNumberResultRealmProxy$RememberNumberResultColumnInfo;->unixTimeIndex:J

    invoke-interface {v1, v2, v3, p1, p2}, Lio/realm/internal/Row;->setLong(JJ)V

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 693
    invoke-static {p0}, Lio/realm/RealmObject;->isValid(Lio/realm/RealmModel;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 694
    const-string v1, "Invalid object"

    .line 713
    :goto_0
    return-object v1

    .line 696
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "RememberNumberResult = ["

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 697
    .local v0, "stringBuilder":Ljava/lang/StringBuilder;
    const-string v1, "{id:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 698
    invoke-virtual {p0}, Lio/realm/RememberNumberResultRealmProxy;->realmGet$id()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 699
    const-string v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 700
    const-string v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 701
    const-string v1, "{score:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 702
    invoke-virtual {p0}, Lio/realm/RememberNumberResultRealmProxy;->realmGet$score()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 703
    const-string v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 704
    const-string v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 705
    const-string v1, "{unixTime:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 706
    invoke-virtual {p0}, Lio/realm/RememberNumberResultRealmProxy;->realmGet$unixTime()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 707
    const-string v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 708
    const-string v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 709
    const-string v1, "{config:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 710
    invoke-virtual {p0}, Lio/realm/RememberNumberResultRealmProxy;->realmGet$config()Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberConfig;

    move-result-object v1

    if-eqz v1, :cond_1

    const-string v1, "RememberNumberConfig"

    :goto_1
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 711
    const-string v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 712
    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 713
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 710
    :cond_1
    const-string v1, "null"

    goto :goto_1
.end method
