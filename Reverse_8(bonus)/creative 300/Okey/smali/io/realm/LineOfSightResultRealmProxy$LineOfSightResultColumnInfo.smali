.class final Lio/realm/LineOfSightResultRealmProxy$LineOfSightResultColumnInfo;
.super Lio/realm/internal/ColumnInfo;
.source "LineOfSightResultRealmProxy.java"

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lio/realm/LineOfSightResultRealmProxy;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = "LineOfSightResultColumnInfo"
.end annotation


# instance fields
.field public configIndex:J

.field public foundMistakeCountIndex:J

.field public idIndex:J

.field public mistakeCountIndex:J

.field public unixTimeIndex:J


# direct methods
.method constructor <init>(Ljava/lang/String;Lio/realm/internal/Table;)V
    .locals 4
    .param p1, "path"    # Ljava/lang/String;
    .param p2, "table"    # Lio/realm/internal/Table;

    .prologue
    .line 43
    invoke-direct {p0}, Lio/realm/internal/ColumnInfo;-><init>()V

    .line 44
    new-instance v0, Ljava/util/HashMap;

    const/4 v1, 0x5

    invoke-direct {v0, v1}, Ljava/util/HashMap;-><init>(I)V

    .line 45
    .local v0, "indicesMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Long;>;"
    const-string v1, "LineOfSightResult"

    const-string v2, "id"

    invoke-virtual {p0, p1, p2, v1, v2}, Lio/realm/LineOfSightResultRealmProxy$LineOfSightResultColumnInfo;->getValidColumnIndex(Ljava/lang/String;Lio/realm/internal/Table;Ljava/lang/String;Ljava/lang/String;)J

    move-result-wide v2

    iput-wide v2, p0, Lio/realm/LineOfSightResultRealmProxy$LineOfSightResultColumnInfo;->idIndex:J

    .line 46
    const-string v1, "id"

    iget-wide v2, p0, Lio/realm/LineOfSightResultRealmProxy$LineOfSightResultColumnInfo;->idIndex:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 47
    const-string v1, "LineOfSightResult"

    const-string v2, "mistakeCount"

    invoke-virtual {p0, p1, p2, v1, v2}, Lio/realm/LineOfSightResultRealmProxy$LineOfSightResultColumnInfo;->getValidColumnIndex(Ljava/lang/String;Lio/realm/internal/Table;Ljava/lang/String;Ljava/lang/String;)J

    move-result-wide v2

    iput-wide v2, p0, Lio/realm/LineOfSightResultRealmProxy$LineOfSightResultColumnInfo;->mistakeCountIndex:J

    .line 48
    const-string v1, "mistakeCount"

    iget-wide v2, p0, Lio/realm/LineOfSightResultRealmProxy$LineOfSightResultColumnInfo;->mistakeCountIndex:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 49
    const-string v1, "LineOfSightResult"

    const-string v2, "foundMistakeCount"

    invoke-virtual {p0, p1, p2, v1, v2}, Lio/realm/LineOfSightResultRealmProxy$LineOfSightResultColumnInfo;->getValidColumnIndex(Ljava/lang/String;Lio/realm/internal/Table;Ljava/lang/String;Ljava/lang/String;)J

    move-result-wide v2

    iput-wide v2, p0, Lio/realm/LineOfSightResultRealmProxy$LineOfSightResultColumnInfo;->foundMistakeCountIndex:J

    .line 50
    const-string v1, "foundMistakeCount"

    iget-wide v2, p0, Lio/realm/LineOfSightResultRealmProxy$LineOfSightResultColumnInfo;->foundMistakeCountIndex:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 51
    const-string v1, "LineOfSightResult"

    const-string v2, "unixTime"

    invoke-virtual {p0, p1, p2, v1, v2}, Lio/realm/LineOfSightResultRealmProxy$LineOfSightResultColumnInfo;->getValidColumnIndex(Ljava/lang/String;Lio/realm/internal/Table;Ljava/lang/String;Ljava/lang/String;)J

    move-result-wide v2

    iput-wide v2, p0, Lio/realm/LineOfSightResultRealmProxy$LineOfSightResultColumnInfo;->unixTimeIndex:J

    .line 52
    const-string v1, "unixTime"

    iget-wide v2, p0, Lio/realm/LineOfSightResultRealmProxy$LineOfSightResultColumnInfo;->unixTimeIndex:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 53
    const-string v1, "LineOfSightResult"

    const-string v2, "config"

    invoke-virtual {p0, p1, p2, v1, v2}, Lio/realm/LineOfSightResultRealmProxy$LineOfSightResultColumnInfo;->getValidColumnIndex(Ljava/lang/String;Lio/realm/internal/Table;Ljava/lang/String;Ljava/lang/String;)J

    move-result-wide v2

    iput-wide v2, p0, Lio/realm/LineOfSightResultRealmProxy$LineOfSightResultColumnInfo;->configIndex:J

    .line 54
    const-string v1, "config"

    iget-wide v2, p0, Lio/realm/LineOfSightResultRealmProxy$LineOfSightResultColumnInfo;->configIndex:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 56
    invoke-virtual {p0, v0}, Lio/realm/LineOfSightResultRealmProxy$LineOfSightResultColumnInfo;->setIndicesMap(Ljava/util/Map;)V

    .line 57
    return-void
.end method


# virtual methods
.method public final clone()Lio/realm/LineOfSightResultRealmProxy$LineOfSightResultColumnInfo;
    .locals 1

    .prologue
    .line 73
    invoke-super {p0}, Lio/realm/internal/ColumnInfo;->clone()Lio/realm/internal/ColumnInfo;

    move-result-object v0

    check-cast v0, Lio/realm/LineOfSightResultRealmProxy$LineOfSightResultColumnInfo;

    return-object v0
.end method

.method public bridge synthetic clone()Lio/realm/internal/ColumnInfo;
    .locals 1

    .prologue
    .line 34
    invoke-virtual {p0}, Lio/realm/LineOfSightResultRealmProxy$LineOfSightResultColumnInfo;->clone()Lio/realm/LineOfSightResultRealmProxy$LineOfSightResultColumnInfo;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 34
    invoke-virtual {p0}, Lio/realm/LineOfSightResultRealmProxy$LineOfSightResultColumnInfo;->clone()Lio/realm/LineOfSightResultRealmProxy$LineOfSightResultColumnInfo;

    move-result-object v0

    return-object v0
.end method

.method public final copyColumnInfoFrom(Lio/realm/internal/ColumnInfo;)V
    .locals 4
    .param p1, "other"    # Lio/realm/internal/ColumnInfo;

    .prologue
    .line 61
    move-object v0, p1

    check-cast v0, Lio/realm/LineOfSightResultRealmProxy$LineOfSightResultColumnInfo;

    .line 62
    .local v0, "otherInfo":Lio/realm/LineOfSightResultRealmProxy$LineOfSightResultColumnInfo;
    iget-wide v2, v0, Lio/realm/LineOfSightResultRealmProxy$LineOfSightResultColumnInfo;->idIndex:J

    iput-wide v2, p0, Lio/realm/LineOfSightResultRealmProxy$LineOfSightResultColumnInfo;->idIndex:J

    .line 63
    iget-wide v2, v0, Lio/realm/LineOfSightResultRealmProxy$LineOfSightResultColumnInfo;->mistakeCountIndex:J

    iput-wide v2, p0, Lio/realm/LineOfSightResultRealmProxy$LineOfSightResultColumnInfo;->mistakeCountIndex:J

    .line 64
    iget-wide v2, v0, Lio/realm/LineOfSightResultRealmProxy$LineOfSightResultColumnInfo;->foundMistakeCountIndex:J

    iput-wide v2, p0, Lio/realm/LineOfSightResultRealmProxy$LineOfSightResultColumnInfo;->foundMistakeCountIndex:J

    .line 65
    iget-wide v2, v0, Lio/realm/LineOfSightResultRealmProxy$LineOfSightResultColumnInfo;->unixTimeIndex:J

    iput-wide v2, p0, Lio/realm/LineOfSightResultRealmProxy$LineOfSightResultColumnInfo;->unixTimeIndex:J

    .line 66
    iget-wide v2, v0, Lio/realm/LineOfSightResultRealmProxy$LineOfSightResultColumnInfo;->configIndex:J

    iput-wide v2, p0, Lio/realm/LineOfSightResultRealmProxy$LineOfSightResultColumnInfo;->configIndex:J

    .line 68
    invoke-virtual {v0}, Lio/realm/LineOfSightResultRealmProxy$LineOfSightResultColumnInfo;->getIndicesMap()Ljava/util/Map;

    move-result-object v1

    invoke-virtual {p0, v1}, Lio/realm/LineOfSightResultRealmProxy$LineOfSightResultColumnInfo;->setIndicesMap(Ljava/util/Map;)V

    .line 69
    return-void
.end method
