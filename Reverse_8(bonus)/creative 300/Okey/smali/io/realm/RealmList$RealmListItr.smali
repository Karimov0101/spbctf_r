.class Lio/realm/RealmList$RealmListItr;
.super Lio/realm/RealmList$RealmItr;
.source "RealmList.java"

# interfaces
.implements Ljava/util/ListIterator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lio/realm/RealmList;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "RealmListItr"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lio/realm/RealmList",
        "<TE;>.RealmItr;",
        "Ljava/util/ListIterator",
        "<TE;>;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lio/realm/RealmList;


# direct methods
.method constructor <init>(Lio/realm/RealmList;I)V
    .locals 3
    .param p2, "index"    # I

    .prologue
    .line 1041
    .local p0, "this":Lio/realm/RealmList$RealmListItr;, "Lio/realm/RealmList<TE;>.RealmListItr;"
    iput-object p1, p0, Lio/realm/RealmList$RealmListItr;->this$0:Lio/realm/RealmList;

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lio/realm/RealmList$RealmItr;-><init>(Lio/realm/RealmList;Lio/realm/RealmList$1;)V

    .line 1042
    if-ltz p2, :cond_0

    invoke-virtual {p1}, Lio/realm/RealmList;->size()I

    move-result v0

    if-gt p2, v0, :cond_0

    .line 1043
    iput p2, p0, Lio/realm/RealmList$RealmListItr;->cursor:I

    .line 1047
    return-void

    .line 1045
    :cond_0
    new-instance v0, Ljava/lang/IndexOutOfBoundsException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Starting location must be a valid index: [0, "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lio/realm/RealmList;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "]. Index was "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v0
.end method


# virtual methods
.method public add(Lio/realm/RealmModel;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TE;)V"
        }
    .end annotation

    .prologue
    .line 1111
    .local p0, "this":Lio/realm/RealmList$RealmListItr;, "Lio/realm/RealmList<TE;>.RealmListItr;"
    .local p1, "e":Lio/realm/RealmModel;, "TE;"
    iget-object v2, p0, Lio/realm/RealmList$RealmListItr;->this$0:Lio/realm/RealmList;

    iget-object v2, v2, Lio/realm/RealmList;->realm:Lio/realm/BaseRealm;

    invoke-virtual {v2}, Lio/realm/BaseRealm;->checkIfValid()V

    .line 1112
    invoke-virtual {p0}, Lio/realm/RealmList$RealmListItr;->checkConcurrentModification()V

    .line 1114
    :try_start_0
    iget v1, p0, Lio/realm/RealmList$RealmListItr;->cursor:I

    .line 1115
    .local v1, "i":I
    iget-object v2, p0, Lio/realm/RealmList$RealmListItr;->this$0:Lio/realm/RealmList;

    invoke-virtual {v2, v1, p1}, Lio/realm/RealmList;->add(ILio/realm/RealmModel;)V

    .line 1116
    const/4 v2, -0x1

    iput v2, p0, Lio/realm/RealmList$RealmListItr;->lastRet:I

    .line 1117
    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Lio/realm/RealmList$RealmListItr;->cursor:I

    .line 1118
    iget-object v2, p0, Lio/realm/RealmList$RealmListItr;->this$0:Lio/realm/RealmList;

    invoke-static {v2}, Lio/realm/RealmList;->access$500(Lio/realm/RealmList;)I

    move-result v2

    iput v2, p0, Lio/realm/RealmList$RealmListItr;->expectedModCount:I
    :try_end_0
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1122
    return-void

    .line 1119
    .end local v1    # "i":I
    :catch_0
    move-exception v0

    .line 1120
    .local v0, "ex":Ljava/lang/IndexOutOfBoundsException;
    new-instance v2, Ljava/util/ConcurrentModificationException;

    invoke-direct {v2}, Ljava/util/ConcurrentModificationException;-><init>()V

    throw v2
.end method

.method public bridge synthetic add(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 1039
    .local p0, "this":Lio/realm/RealmList$RealmListItr;, "Lio/realm/RealmList<TE;>.RealmListItr;"
    check-cast p1, Lio/realm/RealmModel;

    invoke-virtual {p0, p1}, Lio/realm/RealmList$RealmListItr;->add(Lio/realm/RealmModel;)V

    return-void
.end method

.method public hasPrevious()Z
    .locals 1

    .prologue
    .line 1053
    .local p0, "this":Lio/realm/RealmList$RealmListItr;, "Lio/realm/RealmList<TE;>.RealmListItr;"
    iget v0, p0, Lio/realm/RealmList$RealmListItr;->cursor:I

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public nextIndex()I
    .locals 1

    .prologue
    .line 1076
    .local p0, "this":Lio/realm/RealmList$RealmListItr;, "Lio/realm/RealmList<TE;>.RealmListItr;"
    iget v0, p0, Lio/realm/RealmList$RealmListItr;->cursor:I

    return v0
.end method

.method public previous()Lio/realm/RealmModel;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TE;"
        }
    .end annotation

    .prologue
    .line 1060
    .local p0, "this":Lio/realm/RealmList$RealmListItr;, "Lio/realm/RealmList<TE;>.RealmListItr;"
    invoke-virtual {p0}, Lio/realm/RealmList$RealmListItr;->checkConcurrentModification()V

    .line 1061
    iget v3, p0, Lio/realm/RealmList$RealmListItr;->cursor:I

    add-int/lit8 v1, v3, -0x1

    .line 1063
    .local v1, "i":I
    :try_start_0
    iget-object v3, p0, Lio/realm/RealmList$RealmListItr;->this$0:Lio/realm/RealmList;

    invoke-virtual {v3, v1}, Lio/realm/RealmList;->get(I)Lio/realm/RealmModel;

    move-result-object v2

    .line 1064
    .local v2, "previous":Lio/realm/RealmModel;, "TE;"
    iput v1, p0, Lio/realm/RealmList$RealmListItr;->cursor:I

    iput v1, p0, Lio/realm/RealmList$RealmListItr;->lastRet:I
    :try_end_0
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1065
    return-object v2

    .line 1066
    .end local v2    # "previous":Lio/realm/RealmModel;, "TE;"
    :catch_0
    move-exception v0

    .line 1067
    .local v0, "e":Ljava/lang/IndexOutOfBoundsException;
    invoke-virtual {p0}, Lio/realm/RealmList$RealmListItr;->checkConcurrentModification()V

    .line 1068
    new-instance v3, Ljava/util/NoSuchElementException;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Cannot access index less than zero. This was "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ". Remember to check hasPrevious() before using previous()."

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/util/NoSuchElementException;-><init>(Ljava/lang/String;)V

    throw v3
.end method

.method public bridge synthetic previous()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1039
    .local p0, "this":Lio/realm/RealmList$RealmListItr;, "Lio/realm/RealmList<TE;>.RealmListItr;"
    invoke-virtual {p0}, Lio/realm/RealmList$RealmListItr;->previous()Lio/realm/RealmModel;

    move-result-object v0

    return-object v0
.end method

.method public previousIndex()I
    .locals 1

    .prologue
    .line 1083
    .local p0, "this":Lio/realm/RealmList$RealmListItr;, "Lio/realm/RealmList<TE;>.RealmListItr;"
    iget v0, p0, Lio/realm/RealmList$RealmListItr;->cursor:I

    add-int/lit8 v0, v0, -0x1

    return v0
.end method

.method public set(Lio/realm/RealmModel;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TE;)V"
        }
    .end annotation

    .prologue
    .line 1090
    .local p0, "this":Lio/realm/RealmList$RealmListItr;, "Lio/realm/RealmList<TE;>.RealmListItr;"
    .local p1, "e":Lio/realm/RealmModel;, "TE;"
    iget-object v1, p0, Lio/realm/RealmList$RealmListItr;->this$0:Lio/realm/RealmList;

    iget-object v1, v1, Lio/realm/RealmList;->realm:Lio/realm/BaseRealm;

    invoke-virtual {v1}, Lio/realm/BaseRealm;->checkIfValid()V

    .line 1091
    iget v1, p0, Lio/realm/RealmList$RealmListItr;->lastRet:I

    if-gez v1, :cond_0

    .line 1092
    new-instance v1, Ljava/lang/IllegalStateException;

    invoke-direct {v1}, Ljava/lang/IllegalStateException;-><init>()V

    throw v1

    .line 1094
    :cond_0
    invoke-virtual {p0}, Lio/realm/RealmList$RealmListItr;->checkConcurrentModification()V

    .line 1097
    :try_start_0
    iget-object v1, p0, Lio/realm/RealmList$RealmListItr;->this$0:Lio/realm/RealmList;

    iget v2, p0, Lio/realm/RealmList$RealmListItr;->lastRet:I

    invoke-virtual {v1, v2, p1}, Lio/realm/RealmList;->set(ILio/realm/RealmModel;)Lio/realm/RealmModel;

    .line 1098
    iget-object v1, p0, Lio/realm/RealmList$RealmListItr;->this$0:Lio/realm/RealmList;

    invoke-static {v1}, Lio/realm/RealmList;->access$400(Lio/realm/RealmList;)I

    move-result v1

    iput v1, p0, Lio/realm/RealmList$RealmListItr;->expectedModCount:I
    :try_end_0
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1102
    return-void

    .line 1099
    :catch_0
    move-exception v0

    .line 1100
    .local v0, "ex":Ljava/lang/IndexOutOfBoundsException;
    new-instance v1, Ljava/util/ConcurrentModificationException;

    invoke-direct {v1}, Ljava/util/ConcurrentModificationException;-><init>()V

    throw v1
.end method

.method public bridge synthetic set(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 1039
    .local p0, "this":Lio/realm/RealmList$RealmListItr;, "Lio/realm/RealmList<TE;>.RealmListItr;"
    check-cast p1, Lio/realm/RealmModel;

    invoke-virtual {p0, p1}, Lio/realm/RealmList$RealmListItr;->set(Lio/realm/RealmModel;)V

    return-void
.end method
