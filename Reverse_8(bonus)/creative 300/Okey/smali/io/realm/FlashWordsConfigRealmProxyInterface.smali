.class public interface abstract Lio/realm/FlashWordsConfigRealmProxyInterface;
.super Ljava/lang/Object;
.source "FlashWordsConfigRealmProxyInterface.java"


# virtual methods
.method public abstract realmGet$boarType()I
.end method

.method public abstract realmGet$id()I
.end method

.method public abstract realmGet$speed()I
.end method

.method public abstract realmGet$trainingDuration()J
.end method

.method public abstract realmSet$boarType(I)V
.end method

.method public abstract realmSet$id(I)V
.end method

.method public abstract realmSet$speed(I)V
.end method

.method public abstract realmSet$trainingDuration(J)V
.end method
