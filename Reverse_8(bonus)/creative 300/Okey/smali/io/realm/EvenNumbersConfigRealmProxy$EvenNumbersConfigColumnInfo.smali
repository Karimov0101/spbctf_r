.class final Lio/realm/EvenNumbersConfigRealmProxy$EvenNumbersConfigColumnInfo;
.super Lio/realm/internal/ColumnInfo;
.source "EvenNumbersConfigRealmProxy.java"

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lio/realm/EvenNumbersConfigRealmProxy;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = "EvenNumbersConfigColumnInfo"
.end annotation


# instance fields
.field public columnCountIndex:J

.field public digitsPerNumberIndex:J

.field public evenNumberCountIndex:J

.field public idIndex:J

.field public rowCountIndex:J

.field public trainingDurationIndex:J


# direct methods
.method constructor <init>(Ljava/lang/String;Lio/realm/internal/Table;)V
    .locals 4
    .param p1, "path"    # Ljava/lang/String;
    .param p2, "table"    # Lio/realm/internal/Table;

    .prologue
    .line 44
    invoke-direct {p0}, Lio/realm/internal/ColumnInfo;-><init>()V

    .line 45
    new-instance v0, Ljava/util/HashMap;

    const/4 v1, 0x6

    invoke-direct {v0, v1}, Ljava/util/HashMap;-><init>(I)V

    .line 46
    .local v0, "indicesMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Long;>;"
    const-string v1, "EvenNumbersConfig"

    const-string v2, "id"

    invoke-virtual {p0, p1, p2, v1, v2}, Lio/realm/EvenNumbersConfigRealmProxy$EvenNumbersConfigColumnInfo;->getValidColumnIndex(Ljava/lang/String;Lio/realm/internal/Table;Ljava/lang/String;Ljava/lang/String;)J

    move-result-wide v2

    iput-wide v2, p0, Lio/realm/EvenNumbersConfigRealmProxy$EvenNumbersConfigColumnInfo;->idIndex:J

    .line 47
    const-string v1, "id"

    iget-wide v2, p0, Lio/realm/EvenNumbersConfigRealmProxy$EvenNumbersConfigColumnInfo;->idIndex:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 48
    const-string v1, "EvenNumbersConfig"

    const-string v2, "rowCount"

    invoke-virtual {p0, p1, p2, v1, v2}, Lio/realm/EvenNumbersConfigRealmProxy$EvenNumbersConfigColumnInfo;->getValidColumnIndex(Ljava/lang/String;Lio/realm/internal/Table;Ljava/lang/String;Ljava/lang/String;)J

    move-result-wide v2

    iput-wide v2, p0, Lio/realm/EvenNumbersConfigRealmProxy$EvenNumbersConfigColumnInfo;->rowCountIndex:J

    .line 49
    const-string v1, "rowCount"

    iget-wide v2, p0, Lio/realm/EvenNumbersConfigRealmProxy$EvenNumbersConfigColumnInfo;->rowCountIndex:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 50
    const-string v1, "EvenNumbersConfig"

    const-string v2, "columnCount"

    invoke-virtual {p0, p1, p2, v1, v2}, Lio/realm/EvenNumbersConfigRealmProxy$EvenNumbersConfigColumnInfo;->getValidColumnIndex(Ljava/lang/String;Lio/realm/internal/Table;Ljava/lang/String;Ljava/lang/String;)J

    move-result-wide v2

    iput-wide v2, p0, Lio/realm/EvenNumbersConfigRealmProxy$EvenNumbersConfigColumnInfo;->columnCountIndex:J

    .line 51
    const-string v1, "columnCount"

    iget-wide v2, p0, Lio/realm/EvenNumbersConfigRealmProxy$EvenNumbersConfigColumnInfo;->columnCountIndex:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 52
    const-string v1, "EvenNumbersConfig"

    const-string v2, "evenNumberCount"

    invoke-virtual {p0, p1, p2, v1, v2}, Lio/realm/EvenNumbersConfigRealmProxy$EvenNumbersConfigColumnInfo;->getValidColumnIndex(Ljava/lang/String;Lio/realm/internal/Table;Ljava/lang/String;Ljava/lang/String;)J

    move-result-wide v2

    iput-wide v2, p0, Lio/realm/EvenNumbersConfigRealmProxy$EvenNumbersConfigColumnInfo;->evenNumberCountIndex:J

    .line 53
    const-string v1, "evenNumberCount"

    iget-wide v2, p0, Lio/realm/EvenNumbersConfigRealmProxy$EvenNumbersConfigColumnInfo;->evenNumberCountIndex:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 54
    const-string v1, "EvenNumbersConfig"

    const-string v2, "digitsPerNumber"

    invoke-virtual {p0, p1, p2, v1, v2}, Lio/realm/EvenNumbersConfigRealmProxy$EvenNumbersConfigColumnInfo;->getValidColumnIndex(Ljava/lang/String;Lio/realm/internal/Table;Ljava/lang/String;Ljava/lang/String;)J

    move-result-wide v2

    iput-wide v2, p0, Lio/realm/EvenNumbersConfigRealmProxy$EvenNumbersConfigColumnInfo;->digitsPerNumberIndex:J

    .line 55
    const-string v1, "digitsPerNumber"

    iget-wide v2, p0, Lio/realm/EvenNumbersConfigRealmProxy$EvenNumbersConfigColumnInfo;->digitsPerNumberIndex:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 56
    const-string v1, "EvenNumbersConfig"

    const-string v2, "trainingDuration"

    invoke-virtual {p0, p1, p2, v1, v2}, Lio/realm/EvenNumbersConfigRealmProxy$EvenNumbersConfigColumnInfo;->getValidColumnIndex(Ljava/lang/String;Lio/realm/internal/Table;Ljava/lang/String;Ljava/lang/String;)J

    move-result-wide v2

    iput-wide v2, p0, Lio/realm/EvenNumbersConfigRealmProxy$EvenNumbersConfigColumnInfo;->trainingDurationIndex:J

    .line 57
    const-string v1, "trainingDuration"

    iget-wide v2, p0, Lio/realm/EvenNumbersConfigRealmProxy$EvenNumbersConfigColumnInfo;->trainingDurationIndex:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 59
    invoke-virtual {p0, v0}, Lio/realm/EvenNumbersConfigRealmProxy$EvenNumbersConfigColumnInfo;->setIndicesMap(Ljava/util/Map;)V

    .line 60
    return-void
.end method


# virtual methods
.method public final clone()Lio/realm/EvenNumbersConfigRealmProxy$EvenNumbersConfigColumnInfo;
    .locals 1

    .prologue
    .line 77
    invoke-super {p0}, Lio/realm/internal/ColumnInfo;->clone()Lio/realm/internal/ColumnInfo;

    move-result-object v0

    check-cast v0, Lio/realm/EvenNumbersConfigRealmProxy$EvenNumbersConfigColumnInfo;

    return-object v0
.end method

.method public bridge synthetic clone()Lio/realm/internal/ColumnInfo;
    .locals 1

    .prologue
    .line 34
    invoke-virtual {p0}, Lio/realm/EvenNumbersConfigRealmProxy$EvenNumbersConfigColumnInfo;->clone()Lio/realm/EvenNumbersConfigRealmProxy$EvenNumbersConfigColumnInfo;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 34
    invoke-virtual {p0}, Lio/realm/EvenNumbersConfigRealmProxy$EvenNumbersConfigColumnInfo;->clone()Lio/realm/EvenNumbersConfigRealmProxy$EvenNumbersConfigColumnInfo;

    move-result-object v0

    return-object v0
.end method

.method public final copyColumnInfoFrom(Lio/realm/internal/ColumnInfo;)V
    .locals 4
    .param p1, "other"    # Lio/realm/internal/ColumnInfo;

    .prologue
    .line 64
    move-object v0, p1

    check-cast v0, Lio/realm/EvenNumbersConfigRealmProxy$EvenNumbersConfigColumnInfo;

    .line 65
    .local v0, "otherInfo":Lio/realm/EvenNumbersConfigRealmProxy$EvenNumbersConfigColumnInfo;
    iget-wide v2, v0, Lio/realm/EvenNumbersConfigRealmProxy$EvenNumbersConfigColumnInfo;->idIndex:J

    iput-wide v2, p0, Lio/realm/EvenNumbersConfigRealmProxy$EvenNumbersConfigColumnInfo;->idIndex:J

    .line 66
    iget-wide v2, v0, Lio/realm/EvenNumbersConfigRealmProxy$EvenNumbersConfigColumnInfo;->rowCountIndex:J

    iput-wide v2, p0, Lio/realm/EvenNumbersConfigRealmProxy$EvenNumbersConfigColumnInfo;->rowCountIndex:J

    .line 67
    iget-wide v2, v0, Lio/realm/EvenNumbersConfigRealmProxy$EvenNumbersConfigColumnInfo;->columnCountIndex:J

    iput-wide v2, p0, Lio/realm/EvenNumbersConfigRealmProxy$EvenNumbersConfigColumnInfo;->columnCountIndex:J

    .line 68
    iget-wide v2, v0, Lio/realm/EvenNumbersConfigRealmProxy$EvenNumbersConfigColumnInfo;->evenNumberCountIndex:J

    iput-wide v2, p0, Lio/realm/EvenNumbersConfigRealmProxy$EvenNumbersConfigColumnInfo;->evenNumberCountIndex:J

    .line 69
    iget-wide v2, v0, Lio/realm/EvenNumbersConfigRealmProxy$EvenNumbersConfigColumnInfo;->digitsPerNumberIndex:J

    iput-wide v2, p0, Lio/realm/EvenNumbersConfigRealmProxy$EvenNumbersConfigColumnInfo;->digitsPerNumberIndex:J

    .line 70
    iget-wide v2, v0, Lio/realm/EvenNumbersConfigRealmProxy$EvenNumbersConfigColumnInfo;->trainingDurationIndex:J

    iput-wide v2, p0, Lio/realm/EvenNumbersConfigRealmProxy$EvenNumbersConfigColumnInfo;->trainingDurationIndex:J

    .line 72
    invoke-virtual {v0}, Lio/realm/EvenNumbersConfigRealmProxy$EvenNumbersConfigColumnInfo;->getIndicesMap()Ljava/util/Map;

    move-result-object v1

    invoke-virtual {p0, v1}, Lio/realm/EvenNumbersConfigRealmProxy$EvenNumbersConfigColumnInfo;->setIndicesMap(Ljava/util/Map;)V

    .line 73
    return-void
.end method
