.class public interface abstract Lio/realm/LineOfSightConfigRealmProxyInterface;
.super Ljava/lang/Object;
.source "LineOfSightConfigRealmProxyInterface.java"


# virtual methods
.method public abstract realmGet$columnCount()I
.end method

.method public abstract realmGet$fieldType()I
.end method

.method public abstract realmGet$id()I
.end method

.method public abstract realmGet$mistakeProbability()I
.end method

.method public abstract realmGet$rowCount()I
.end method

.method public abstract realmGet$showCount()I
.end method

.method public abstract realmGet$showDelay()J
.end method

.method public abstract realmSet$columnCount(I)V
.end method

.method public abstract realmSet$fieldType(I)V
.end method

.method public abstract realmSet$id(I)V
.end method

.method public abstract realmSet$mistakeProbability(I)V
.end method

.method public abstract realmSet$rowCount(I)V
.end method

.method public abstract realmSet$showCount(I)V
.end method

.method public abstract realmSet$showDelay(J)V
.end method
