.class public interface abstract Lio/realm/MathResultRealmProxyInterface;
.super Ljava/lang/Object;
.source "MathResultRealmProxyInterface.java"


# virtual methods
.method public abstract realmGet$config()Lcom/safonov/speedreading/training/fragment/math/repository/entity/MathConfig;
.end method

.method public abstract realmGet$id()I
.end method

.method public abstract realmGet$score()I
.end method

.method public abstract realmSet$config(Lcom/safonov/speedreading/training/fragment/math/repository/entity/MathConfig;)V
.end method

.method public abstract realmSet$id(I)V
.end method

.method public abstract realmSet$score(I)V
.end method
