.class public interface abstract Lio/realm/CupTimerResultRealmProxyInterface;
.super Ljava/lang/Object;
.source "CupTimerResultRealmProxyInterface.java"


# virtual methods
.method public abstract realmGet$config()Lcom/safonov/speedreading/training/fragment/cuptimer/repository/entity/CupTimerConfig;
.end method

.method public abstract realmGet$id()I
.end method

.method public abstract realmGet$unixTime()J
.end method

.method public abstract realmSet$config(Lcom/safonov/speedreading/training/fragment/cuptimer/repository/entity/CupTimerConfig;)V
.end method

.method public abstract realmSet$id(I)V
.end method

.method public abstract realmSet$unixTime(J)V
.end method
