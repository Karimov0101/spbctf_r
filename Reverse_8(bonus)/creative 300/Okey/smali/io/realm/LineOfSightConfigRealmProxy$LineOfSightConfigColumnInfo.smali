.class final Lio/realm/LineOfSightConfigRealmProxy$LineOfSightConfigColumnInfo;
.super Lio/realm/internal/ColumnInfo;
.source "LineOfSightConfigRealmProxy.java"

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lio/realm/LineOfSightConfigRealmProxy;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = "LineOfSightConfigColumnInfo"
.end annotation


# instance fields
.field public columnCountIndex:J

.field public fieldTypeIndex:J

.field public idIndex:J

.field public mistakeProbabilityIndex:J

.field public rowCountIndex:J

.field public showCountIndex:J

.field public showDelayIndex:J


# direct methods
.method constructor <init>(Ljava/lang/String;Lio/realm/internal/Table;)V
    .locals 4
    .param p1, "path"    # Ljava/lang/String;
    .param p2, "table"    # Lio/realm/internal/Table;

    .prologue
    .line 45
    invoke-direct {p0}, Lio/realm/internal/ColumnInfo;-><init>()V

    .line 46
    new-instance v0, Ljava/util/HashMap;

    const/4 v1, 0x7

    invoke-direct {v0, v1}, Ljava/util/HashMap;-><init>(I)V

    .line 47
    .local v0, "indicesMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Long;>;"
    const-string v1, "LineOfSightConfig"

    const-string v2, "id"

    invoke-virtual {p0, p1, p2, v1, v2}, Lio/realm/LineOfSightConfigRealmProxy$LineOfSightConfigColumnInfo;->getValidColumnIndex(Ljava/lang/String;Lio/realm/internal/Table;Ljava/lang/String;Ljava/lang/String;)J

    move-result-wide v2

    iput-wide v2, p0, Lio/realm/LineOfSightConfigRealmProxy$LineOfSightConfigColumnInfo;->idIndex:J

    .line 48
    const-string v1, "id"

    iget-wide v2, p0, Lio/realm/LineOfSightConfigRealmProxy$LineOfSightConfigColumnInfo;->idIndex:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 49
    const-string v1, "LineOfSightConfig"

    const-string v2, "rowCount"

    invoke-virtual {p0, p1, p2, v1, v2}, Lio/realm/LineOfSightConfigRealmProxy$LineOfSightConfigColumnInfo;->getValidColumnIndex(Ljava/lang/String;Lio/realm/internal/Table;Ljava/lang/String;Ljava/lang/String;)J

    move-result-wide v2

    iput-wide v2, p0, Lio/realm/LineOfSightConfigRealmProxy$LineOfSightConfigColumnInfo;->rowCountIndex:J

    .line 50
    const-string v1, "rowCount"

    iget-wide v2, p0, Lio/realm/LineOfSightConfigRealmProxy$LineOfSightConfigColumnInfo;->rowCountIndex:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 51
    const-string v1, "LineOfSightConfig"

    const-string v2, "columnCount"

    invoke-virtual {p0, p1, p2, v1, v2}, Lio/realm/LineOfSightConfigRealmProxy$LineOfSightConfigColumnInfo;->getValidColumnIndex(Ljava/lang/String;Lio/realm/internal/Table;Ljava/lang/String;Ljava/lang/String;)J

    move-result-wide v2

    iput-wide v2, p0, Lio/realm/LineOfSightConfigRealmProxy$LineOfSightConfigColumnInfo;->columnCountIndex:J

    .line 52
    const-string v1, "columnCount"

    iget-wide v2, p0, Lio/realm/LineOfSightConfigRealmProxy$LineOfSightConfigColumnInfo;->columnCountIndex:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 53
    const-string v1, "LineOfSightConfig"

    const-string v2, "fieldType"

    invoke-virtual {p0, p1, p2, v1, v2}, Lio/realm/LineOfSightConfigRealmProxy$LineOfSightConfigColumnInfo;->getValidColumnIndex(Ljava/lang/String;Lio/realm/internal/Table;Ljava/lang/String;Ljava/lang/String;)J

    move-result-wide v2

    iput-wide v2, p0, Lio/realm/LineOfSightConfigRealmProxy$LineOfSightConfigColumnInfo;->fieldTypeIndex:J

    .line 54
    const-string v1, "fieldType"

    iget-wide v2, p0, Lio/realm/LineOfSightConfigRealmProxy$LineOfSightConfigColumnInfo;->fieldTypeIndex:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 55
    const-string v1, "LineOfSightConfig"

    const-string v2, "showCount"

    invoke-virtual {p0, p1, p2, v1, v2}, Lio/realm/LineOfSightConfigRealmProxy$LineOfSightConfigColumnInfo;->getValidColumnIndex(Ljava/lang/String;Lio/realm/internal/Table;Ljava/lang/String;Ljava/lang/String;)J

    move-result-wide v2

    iput-wide v2, p0, Lio/realm/LineOfSightConfigRealmProxy$LineOfSightConfigColumnInfo;->showCountIndex:J

    .line 56
    const-string v1, "showCount"

    iget-wide v2, p0, Lio/realm/LineOfSightConfigRealmProxy$LineOfSightConfigColumnInfo;->showCountIndex:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 57
    const-string v1, "LineOfSightConfig"

    const-string v2, "showDelay"

    invoke-virtual {p0, p1, p2, v1, v2}, Lio/realm/LineOfSightConfigRealmProxy$LineOfSightConfigColumnInfo;->getValidColumnIndex(Ljava/lang/String;Lio/realm/internal/Table;Ljava/lang/String;Ljava/lang/String;)J

    move-result-wide v2

    iput-wide v2, p0, Lio/realm/LineOfSightConfigRealmProxy$LineOfSightConfigColumnInfo;->showDelayIndex:J

    .line 58
    const-string v1, "showDelay"

    iget-wide v2, p0, Lio/realm/LineOfSightConfigRealmProxy$LineOfSightConfigColumnInfo;->showDelayIndex:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 59
    const-string v1, "LineOfSightConfig"

    const-string v2, "mistakeProbability"

    invoke-virtual {p0, p1, p2, v1, v2}, Lio/realm/LineOfSightConfigRealmProxy$LineOfSightConfigColumnInfo;->getValidColumnIndex(Ljava/lang/String;Lio/realm/internal/Table;Ljava/lang/String;Ljava/lang/String;)J

    move-result-wide v2

    iput-wide v2, p0, Lio/realm/LineOfSightConfigRealmProxy$LineOfSightConfigColumnInfo;->mistakeProbabilityIndex:J

    .line 60
    const-string v1, "mistakeProbability"

    iget-wide v2, p0, Lio/realm/LineOfSightConfigRealmProxy$LineOfSightConfigColumnInfo;->mistakeProbabilityIndex:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 62
    invoke-virtual {p0, v0}, Lio/realm/LineOfSightConfigRealmProxy$LineOfSightConfigColumnInfo;->setIndicesMap(Ljava/util/Map;)V

    .line 63
    return-void
.end method


# virtual methods
.method public final clone()Lio/realm/LineOfSightConfigRealmProxy$LineOfSightConfigColumnInfo;
    .locals 1

    .prologue
    .line 81
    invoke-super {p0}, Lio/realm/internal/ColumnInfo;->clone()Lio/realm/internal/ColumnInfo;

    move-result-object v0

    check-cast v0, Lio/realm/LineOfSightConfigRealmProxy$LineOfSightConfigColumnInfo;

    return-object v0
.end method

.method public bridge synthetic clone()Lio/realm/internal/ColumnInfo;
    .locals 1

    .prologue
    .line 34
    invoke-virtual {p0}, Lio/realm/LineOfSightConfigRealmProxy$LineOfSightConfigColumnInfo;->clone()Lio/realm/LineOfSightConfigRealmProxy$LineOfSightConfigColumnInfo;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 34
    invoke-virtual {p0}, Lio/realm/LineOfSightConfigRealmProxy$LineOfSightConfigColumnInfo;->clone()Lio/realm/LineOfSightConfigRealmProxy$LineOfSightConfigColumnInfo;

    move-result-object v0

    return-object v0
.end method

.method public final copyColumnInfoFrom(Lio/realm/internal/ColumnInfo;)V
    .locals 4
    .param p1, "other"    # Lio/realm/internal/ColumnInfo;

    .prologue
    .line 67
    move-object v0, p1

    check-cast v0, Lio/realm/LineOfSightConfigRealmProxy$LineOfSightConfigColumnInfo;

    .line 68
    .local v0, "otherInfo":Lio/realm/LineOfSightConfigRealmProxy$LineOfSightConfigColumnInfo;
    iget-wide v2, v0, Lio/realm/LineOfSightConfigRealmProxy$LineOfSightConfigColumnInfo;->idIndex:J

    iput-wide v2, p0, Lio/realm/LineOfSightConfigRealmProxy$LineOfSightConfigColumnInfo;->idIndex:J

    .line 69
    iget-wide v2, v0, Lio/realm/LineOfSightConfigRealmProxy$LineOfSightConfigColumnInfo;->rowCountIndex:J

    iput-wide v2, p0, Lio/realm/LineOfSightConfigRealmProxy$LineOfSightConfigColumnInfo;->rowCountIndex:J

    .line 70
    iget-wide v2, v0, Lio/realm/LineOfSightConfigRealmProxy$LineOfSightConfigColumnInfo;->columnCountIndex:J

    iput-wide v2, p0, Lio/realm/LineOfSightConfigRealmProxy$LineOfSightConfigColumnInfo;->columnCountIndex:J

    .line 71
    iget-wide v2, v0, Lio/realm/LineOfSightConfigRealmProxy$LineOfSightConfigColumnInfo;->fieldTypeIndex:J

    iput-wide v2, p0, Lio/realm/LineOfSightConfigRealmProxy$LineOfSightConfigColumnInfo;->fieldTypeIndex:J

    .line 72
    iget-wide v2, v0, Lio/realm/LineOfSightConfigRealmProxy$LineOfSightConfigColumnInfo;->showCountIndex:J

    iput-wide v2, p0, Lio/realm/LineOfSightConfigRealmProxy$LineOfSightConfigColumnInfo;->showCountIndex:J

    .line 73
    iget-wide v2, v0, Lio/realm/LineOfSightConfigRealmProxy$LineOfSightConfigColumnInfo;->showDelayIndex:J

    iput-wide v2, p0, Lio/realm/LineOfSightConfigRealmProxy$LineOfSightConfigColumnInfo;->showDelayIndex:J

    .line 74
    iget-wide v2, v0, Lio/realm/LineOfSightConfigRealmProxy$LineOfSightConfigColumnInfo;->mistakeProbabilityIndex:J

    iput-wide v2, p0, Lio/realm/LineOfSightConfigRealmProxy$LineOfSightConfigColumnInfo;->mistakeProbabilityIndex:J

    .line 76
    invoke-virtual {v0}, Lio/realm/LineOfSightConfigRealmProxy$LineOfSightConfigColumnInfo;->getIndicesMap()Ljava/util/Map;

    move-result-object v1

    invoke-virtual {p0, v1}, Lio/realm/LineOfSightConfigRealmProxy$LineOfSightConfigColumnInfo;->setIndicesMap(Ljava/util/Map;)V

    .line 77
    return-void
.end method
