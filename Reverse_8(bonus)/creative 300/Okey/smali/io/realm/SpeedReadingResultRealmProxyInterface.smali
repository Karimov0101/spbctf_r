.class public interface abstract Lio/realm/SpeedReadingResultRealmProxyInterface;
.super Ljava/lang/Object;
.source "SpeedReadingResultRealmProxyInterface.java"


# virtual methods
.method public abstract realmGet$averageSpeed()I
.end method

.method public abstract realmGet$config()Lcom/safonov/speedreading/training/fragment/speedreading/repository/entity/SpeedReadingConfig;
.end method

.method public abstract realmGet$id()I
.end method

.method public abstract realmGet$maxSpeed()I
.end method

.method public abstract realmGet$unixTime()J
.end method

.method public abstract realmSet$averageSpeed(I)V
.end method

.method public abstract realmSet$config(Lcom/safonov/speedreading/training/fragment/speedreading/repository/entity/SpeedReadingConfig;)V
.end method

.method public abstract realmSet$id(I)V
.end method

.method public abstract realmSet$maxSpeed(I)V
.end method

.method public abstract realmSet$unixTime(J)V
.end method
