.class Lio/realm/rx/RealmObservableFactory$8;
.super Ljava/lang/Object;
.source "RealmObservableFactory.java"

# interfaces
.implements Lrx/Observable$OnSubscribe;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lio/realm/rx/RealmObservableFactory;->from(Lio/realm/Realm;Lio/realm/RealmList;)Lrx/Observable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lrx/Observable$OnSubscribe",
        "<",
        "Lio/realm/RealmList",
        "<TE;>;>;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lio/realm/rx/RealmObservableFactory;

.field final synthetic val$list:Lio/realm/RealmList;

.field final synthetic val$realmConfig:Lio/realm/RealmConfiguration;


# direct methods
.method constructor <init>(Lio/realm/rx/RealmObservableFactory;Lio/realm/RealmConfiguration;Lio/realm/RealmList;)V
    .locals 0
    .param p1, "this$0"    # Lio/realm/rx/RealmObservableFactory;

    .prologue
    .line 208
    iput-object p1, p0, Lio/realm/rx/RealmObservableFactory$8;->this$0:Lio/realm/rx/RealmObservableFactory;

    iput-object p2, p0, Lio/realm/rx/RealmObservableFactory$8;->val$realmConfig:Lio/realm/RealmConfiguration;

    iput-object p3, p0, Lio/realm/rx/RealmObservableFactory$8;->val$list:Lio/realm/RealmList;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic call(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 208
    check-cast p1, Lrx/Subscriber;

    invoke-virtual {p0, p1}, Lio/realm/rx/RealmObservableFactory$8;->call(Lrx/Subscriber;)V

    return-void
.end method

.method public call(Lrx/Subscriber;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lrx/Subscriber",
            "<-",
            "Lio/realm/RealmList",
            "<TE;>;>;)V"
        }
    .end annotation

    .prologue
    .line 213
    .local p1, "subscriber":Lrx/Subscriber;, "Lrx/Subscriber<-Lio/realm/RealmList<TE;>;>;"
    iget-object v2, p0, Lio/realm/rx/RealmObservableFactory$8;->val$realmConfig:Lio/realm/RealmConfiguration;

    invoke-static {v2}, Lio/realm/Realm;->getInstance(Lio/realm/RealmConfiguration;)Lio/realm/Realm;

    move-result-object v1

    .line 214
    .local v1, "observableRealm":Lio/realm/Realm;
    iget-object v2, p0, Lio/realm/rx/RealmObservableFactory$8;->this$0:Lio/realm/rx/RealmObservableFactory;

    iget-object v2, v2, Lio/realm/rx/RealmObservableFactory;->listRefs:Ljava/lang/ThreadLocal;

    invoke-virtual {v2}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lio/realm/rx/RealmObservableFactory$StrongReferenceCounter;

    iget-object v3, p0, Lio/realm/rx/RealmObservableFactory$8;->val$list:Lio/realm/RealmList;

    invoke-virtual {v2, v3}, Lio/realm/rx/RealmObservableFactory$StrongReferenceCounter;->acquireReference(Ljava/lang/Object;)V

    .line 216
    new-instance v0, Lio/realm/rx/RealmObservableFactory$8$1;

    invoke-direct {v0, p0, p1}, Lio/realm/rx/RealmObservableFactory$8$1;-><init>(Lio/realm/rx/RealmObservableFactory$8;Lrx/Subscriber;)V

    .line 224
    .local v0, "listener":Lio/realm/RealmChangeListener;, "Lio/realm/RealmChangeListener<Lio/realm/RealmList<TE;>;>;"
    iget-object v2, p0, Lio/realm/rx/RealmObservableFactory$8;->val$list:Lio/realm/RealmList;

    invoke-virtual {v2, v0}, Lio/realm/RealmList;->addChangeListener(Lio/realm/RealmChangeListener;)V

    .line 225
    new-instance v2, Lio/realm/rx/RealmObservableFactory$8$2;

    invoke-direct {v2, p0, v0, v1}, Lio/realm/rx/RealmObservableFactory$8$2;-><init>(Lio/realm/rx/RealmObservableFactory$8;Lio/realm/RealmChangeListener;Lio/realm/Realm;)V

    invoke-static {v2}, Lrx/subscriptions/Subscriptions;->create(Lrx/functions/Action0;)Lrx/Subscription;

    move-result-object v2

    invoke-virtual {p1, v2}, Lrx/Subscriber;->add(Lrx/Subscription;)V

    .line 236
    iget-object v2, p0, Lio/realm/rx/RealmObservableFactory$8;->val$list:Lio/realm/RealmList;

    invoke-virtual {p1, v2}, Lrx/Subscriber;->onNext(Ljava/lang/Object;)V

    .line 237
    return-void
.end method
