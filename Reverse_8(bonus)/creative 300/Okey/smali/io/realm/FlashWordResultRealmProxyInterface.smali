.class public interface abstract Lio/realm/FlashWordResultRealmProxyInterface;
.super Ljava/lang/Object;
.source "FlashWordResultRealmProxyInterface.java"


# virtual methods
.method public abstract realmGet$config()Lcom/safonov/speedreading/training/fragment/flashword/repository/entity/FlashWordsConfig;
.end method

.method public abstract realmGet$id()I
.end method

.method public abstract realmGet$unixTime()J
.end method

.method public abstract realmSet$config(Lcom/safonov/speedreading/training/fragment/flashword/repository/entity/FlashWordsConfig;)V
.end method

.method public abstract realmSet$id(I)V
.end method

.method public abstract realmSet$unixTime(J)V
.end method
