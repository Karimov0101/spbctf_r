.class public Lio/realm/Realm;
.super Lio/realm/BaseRealm;
.source "Realm.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lio/realm/Realm$Transaction;
    }
.end annotation


# static fields
.field public static final DEFAULT_REALM_NAME:Ljava/lang/String; = "default.realm"

.field private static defaultConfiguration:Lio/realm/RealmConfiguration;


# direct methods
.method constructor <init>(Lio/realm/RealmConfiguration;)V
    .locals 0
    .param p1, "configuration"    # Lio/realm/RealmConfiguration;

    .prologue
    .line 139
    invoke-direct {p0, p1}, Lio/realm/BaseRealm;-><init>(Lio/realm/RealmConfiguration;)V

    .line 140
    return-void
.end method

.method private checkHasPrimaryKey(Ljava/lang/Class;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<+",
            "Lio/realm/RealmModel;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1530
    .local p1, "clazz":Ljava/lang/Class;, "Ljava/lang/Class<+Lio/realm/RealmModel;>;"
    iget-object v0, p0, Lio/realm/Realm;->schema:Lio/realm/RealmSchema;

    invoke-virtual {v0, p1}, Lio/realm/RealmSchema;->getTable(Ljava/lang/Class;)Lio/realm/internal/Table;

    move-result-object v0

    invoke-virtual {v0}, Lio/realm/internal/Table;->hasPrimaryKey()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1531
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "A RealmObject with no @PrimaryKey cannot be updated: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Ljava/lang/Class;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1533
    :cond_0
    return-void
.end method

.method private checkMaxDepth(I)V
    .locals 3
    .param p1, "maxDepth"    # I

    .prologue
    .line 1536
    if-gez p1, :cond_0

    .line 1537
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "maxDepth must be > 0. It was: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1539
    :cond_0
    return-void
.end method

.method private checkNotNullObject(Lio/realm/RealmModel;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E::",
            "Lio/realm/RealmModel;",
            ">(TE;)V"
        }
    .end annotation

    .prologue
    .line 1524
    .local p1, "object":Lio/realm/RealmModel;, "TE;"
    if-nez p1, :cond_0

    .line 1525
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Null objects cannot be copied into Realm."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1527
    :cond_0
    return-void
.end method

.method private checkValidObjectForDetach(Lio/realm/RealmModel;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E::",
            "Lio/realm/RealmModel;",
            ">(TE;)V"
        }
    .end annotation

    .prologue
    .line 1542
    .local p1, "realmObject":Lio/realm/RealmModel;, "TE;"
    if-nez p1, :cond_0

    .line 1543
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Null objects cannot be copied from Realm."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1545
    :cond_0
    invoke-static {p1}, Lio/realm/RealmObject;->isManaged(Lio/realm/RealmModel;)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-static {p1}, Lio/realm/RealmObject;->isValid(Lio/realm/RealmModel;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 1546
    :cond_1
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Only valid managed objects can be copied from Realm."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1548
    :cond_2
    instance-of v0, p1, Lio/realm/DynamicRealmObject;

    if-eqz v0, :cond_3

    .line 1549
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "DynamicRealmObject cannot be copied from Realm."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1551
    :cond_3
    return-void
.end method

.method public static compactRealm(Lio/realm/RealmConfiguration;)Z
    .locals 2
    .param p0, "configuration"    # Lio/realm/RealmConfiguration;

    .prologue
    .line 1624
    invoke-virtual {p0}, Lio/realm/RealmConfiguration;->isSyncConfiguration()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1625
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Compacting is not supported yet on synced Realms. See https://github.com/realm/realm-core/issues/2345"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1627
    :cond_0
    invoke-static {p0}, Lio/realm/BaseRealm;->compactRealm(Lio/realm/RealmConfiguration;)Z

    move-result v0

    return v0
.end method

.method private copyOrUpdate(Lio/realm/RealmModel;ZLjava/util/Map;)Lio/realm/RealmModel;
    .locals 1
    .param p2, "update"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E::",
            "Lio/realm/RealmModel;",
            ">(TE;Z",
            "Ljava/util/Map",
            "<",
            "Lio/realm/RealmModel;",
            "Lio/realm/internal/RealmObjectProxy;",
            ">;)TE;"
        }
    .end annotation

    .prologue
    .line 1514
    .local p1, "object":Lio/realm/RealmModel;, "TE;"
    .local p3, "cache":Ljava/util/Map;, "Ljava/util/Map<Lio/realm/RealmModel;Lio/realm/internal/RealmObjectProxy;>;"
    invoke-virtual {p0}, Lio/realm/Realm;->checkIfValid()V

    .line 1515
    iget-object v0, p0, Lio/realm/Realm;->configuration:Lio/realm/RealmConfiguration;

    invoke-virtual {v0}, Lio/realm/RealmConfiguration;->getSchemaMediator()Lio/realm/internal/RealmProxyMediator;

    move-result-object v0

    invoke-virtual {v0, p0, p1, p2, p3}, Lio/realm/internal/RealmProxyMediator;->copyOrUpdate(Lio/realm/Realm;Lio/realm/RealmModel;ZLjava/util/Map;)Lio/realm/RealmModel;

    move-result-object v0

    return-object v0
.end method

.method static createAndValidate(Lio/realm/RealmConfiguration;[Lio/realm/internal/ColumnIndices;)Lio/realm/Realm;
    .locals 14
    .param p0, "configuration"    # Lio/realm/RealmConfiguration;
    .param p1, "globalCacheArray"    # [Lio/realm/internal/ColumnIndices;

    .prologue
    .line 284
    new-instance v4, Lio/realm/Realm;

    invoke-direct {v4, p0}, Lio/realm/Realm;-><init>(Lio/realm/RealmConfiguration;)V

    .line 286
    .local v4, "realm":Lio/realm/Realm;
    invoke-virtual {v4}, Lio/realm/Realm;->getVersion()J

    move-result-wide v2

    .line 287
    .local v2, "currentVersion":J
    invoke-virtual {p0}, Lio/realm/RealmConfiguration;->getSchemaVersion()J

    move-result-wide v6

    .line 289
    .local v6, "requiredVersion":J
    invoke-static {p1, v6, v7}, Lio/realm/RealmCache;->findColumnIndices([Lio/realm/internal/ColumnIndices;J)Lio/realm/internal/ColumnIndices;

    move-result-object v0

    .line 291
    .local v0, "columnIndices":Lio/realm/internal/ColumnIndices;
    if-eqz v0, :cond_0

    .line 293
    iget-object v8, v4, Lio/realm/Realm;->schema:Lio/realm/RealmSchema;

    invoke-virtual {v0}, Lio/realm/internal/ColumnIndices;->clone()Lio/realm/internal/ColumnIndices;

    move-result-object v9

    iput-object v9, v8, Lio/realm/RealmSchema;->columnIndices:Lio/realm/internal/ColumnIndices;

    .line 324
    :goto_0
    return-object v4

    .line 295
    :cond_0
    invoke-virtual {p0}, Lio/realm/RealmConfiguration;->isSyncConfiguration()Z

    move-result v5

    .line 297
    .local v5, "syncingConfig":Z
    if-nez v5, :cond_2

    const-wide/16 v8, -0x1

    cmp-long v8, v2, v8

    if-eqz v8, :cond_2

    .line 298
    cmp-long v8, v2, v6

    if-gez v8, :cond_1

    .line 299
    invoke-virtual {v4}, Lio/realm/Realm;->doClose()V

    .line 300
    new-instance v8, Lio/realm/exceptions/RealmMigrationNeededException;

    .line 301
    invoke-virtual {p0}, Lio/realm/RealmConfiguration;->getPath()Ljava/lang/String;

    move-result-object v9

    const-string v10, "Realm on disk need to migrate from v%s to v%s"

    const/4 v11, 0x2

    new-array v11, v11, [Ljava/lang/Object;

    const/4 v12, 0x0

    .line 302
    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v13

    aput-object v13, v11, v12

    const/4 v12, 0x1

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v13

    aput-object v13, v11, v12

    invoke-static {v10, v11}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    invoke-direct {v8, v9, v10}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v8

    .line 304
    :cond_1
    cmp-long v8, v6, v2

    if-gez v8, :cond_2

    .line 305
    invoke-virtual {v4}, Lio/realm/Realm;->doClose()V

    .line 306
    new-instance v8, Ljava/lang/IllegalArgumentException;

    const-string v9, "Realm on disk is newer than the one specified: v%s vs. v%s"

    const/4 v10, 0x2

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v11, 0x0

    .line 307
    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v12

    aput-object v12, v10, v11

    const/4 v11, 0x1

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v12

    aput-object v12, v10, v11

    invoke-static {v9, v10}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    invoke-direct {v8, v9}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v8

    .line 313
    :cond_2
    if-nez v5, :cond_3

    .line 314
    :try_start_0
    invoke-static {v4}, Lio/realm/Realm;->initializeRealm(Lio/realm/Realm;)V
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 318
    :catch_0
    move-exception v1

    .line 319
    .local v1, "e":Ljava/lang/RuntimeException;
    invoke-virtual {v4}, Lio/realm/Realm;->doClose()V

    .line 320
    throw v1

    .line 316
    .end local v1    # "e":Ljava/lang/RuntimeException;
    :cond_3
    :try_start_1
    invoke-static {v4}, Lio/realm/Realm;->initializeSyncedRealm(Lio/realm/Realm;)V
    :try_end_1
    .catch Ljava/lang/RuntimeException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0
.end method

.method private createDetachedCopy(Lio/realm/RealmModel;ILjava/util/Map;)Lio/realm/RealmModel;
    .locals 1
    .param p2, "maxDepth"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E::",
            "Lio/realm/RealmModel;",
            ">(TE;I",
            "Ljava/util/Map",
            "<",
            "Lio/realm/RealmModel;",
            "Lio/realm/internal/RealmObjectProxy$CacheData",
            "<",
            "Lio/realm/RealmModel;",
            ">;>;)TE;"
        }
    .end annotation

    .prologue
    .line 1519
    .local p1, "object":Lio/realm/RealmModel;, "TE;"
    .local p3, "cache":Ljava/util/Map;, "Ljava/util/Map<Lio/realm/RealmModel;Lio/realm/internal/RealmObjectProxy$CacheData<Lio/realm/RealmModel;>;>;"
    invoke-virtual {p0}, Lio/realm/Realm;->checkIfValid()V

    .line 1520
    iget-object v0, p0, Lio/realm/Realm;->configuration:Lio/realm/RealmConfiguration;

    invoke-virtual {v0}, Lio/realm/RealmConfiguration;->getSchemaMediator()Lio/realm/internal/RealmProxyMediator;

    move-result-object v0

    invoke-virtual {v0, p1, p2, p3}, Lio/realm/internal/RealmProxyMediator;->createDetachedCopy(Lio/realm/RealmModel;ILjava/util/Map;)Lio/realm/RealmModel;

    move-result-object v0

    return-object v0
.end method

.method static createInstance(Lio/realm/RealmConfiguration;[Lio/realm/internal/ColumnIndices;)Lio/realm/Realm;
    .locals 4
    .param p0, "configuration"    # Lio/realm/RealmConfiguration;
    .param p1, "globalCacheArray"    # [Lio/realm/internal/ColumnIndices;

    .prologue
    .line 265
    :try_start_0
    invoke-static {p0, p1}, Lio/realm/Realm;->createAndValidate(Lio/realm/RealmConfiguration;[Lio/realm/internal/ColumnIndices;)Lio/realm/Realm;
    :try_end_0
    .catch Lio/realm/exceptions/RealmMigrationNeededException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    .line 279
    :goto_0
    return-object v2

    .line 267
    :catch_0
    move-exception v0

    .line 268
    .local v0, "e":Lio/realm/exceptions/RealmMigrationNeededException;
    invoke-virtual {p0}, Lio/realm/RealmConfiguration;->shouldDeleteRealmIfMigrationNeeded()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 269
    invoke-static {p0}, Lio/realm/Realm;->deleteRealm(Lio/realm/RealmConfiguration;)Z

    .line 279
    :goto_1
    invoke-static {p0, p1}, Lio/realm/Realm;->createAndValidate(Lio/realm/RealmConfiguration;[Lio/realm/internal/ColumnIndices;)Lio/realm/Realm;

    move-result-object v2

    goto :goto_0

    .line 272
    :cond_0
    :try_start_1
    invoke-static {p0, v0}, Lio/realm/Realm;->migrateRealm(Lio/realm/RealmConfiguration;Lio/realm/exceptions/RealmMigrationNeededException;)V
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_1

    .line 273
    :catch_1
    move-exception v1

    .line 275
    .local v1, "fileNotFoundException":Ljava/io/FileNotFoundException;
    new-instance v2, Lio/realm/exceptions/RealmFileException;

    sget-object v3, Lio/realm/exceptions/RealmFileException$Kind;->NOT_FOUND:Lio/realm/exceptions/RealmFileException$Kind;

    invoke-direct {v2, v3, v1}, Lio/realm/exceptions/RealmFileException;-><init>(Lio/realm/exceptions/RealmFileException$Kind;Ljava/lang/Throwable;)V

    throw v2
.end method

.method public static deleteRealm(Lio/realm/RealmConfiguration;)Z
    .locals 1
    .param p0, "configuration"    # Lio/realm/RealmConfiguration;

    .prologue
    .line 1606
    invoke-static {p0}, Lio/realm/BaseRealm;->deleteRealm(Lio/realm/RealmConfiguration;)Z

    move-result v0

    return v0
.end method

.method public static getDefaultInstance()Lio/realm/Realm;
    .locals 2

    .prologue
    .line 207
    sget-object v0, Lio/realm/Realm;->defaultConfiguration:Lio/realm/RealmConfiguration;

    if-nez v0, :cond_0

    .line 208
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Call `Realm.init(Context)` before calling this method."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 210
    :cond_0
    sget-object v0, Lio/realm/Realm;->defaultConfiguration:Lio/realm/RealmConfiguration;

    const-class v1, Lio/realm/Realm;

    invoke-static {v0, v1}, Lio/realm/RealmCache;->createRealmOrGetFromCache(Lio/realm/RealmConfiguration;Ljava/lang/Class;)Lio/realm/BaseRealm;

    move-result-object v0

    check-cast v0, Lio/realm/Realm;

    return-object v0
.end method

.method public static getDefaultModule()Ljava/lang/Object;
    .locals 7

    .prologue
    .line 1682
    const-string v3, "io.realm.DefaultRealmModule"

    .line 1686
    .local v3, "moduleName":Ljava/lang/String;
    :try_start_0
    invoke-static {v3}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    .line 1687
    .local v0, "clazz":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    invoke-virtual {v0}, Ljava/lang/Class;->getDeclaredConstructors()[Ljava/lang/reflect/Constructor;

    move-result-object v4

    const/4 v5, 0x0

    aget-object v1, v4, v5

    .line 1688
    .local v1, "constructor":Ljava/lang/reflect/Constructor;, "Ljava/lang/reflect/Constructor<*>;"
    const/4 v4, 0x1

    invoke-virtual {v1, v4}, Ljava/lang/reflect/Constructor;->setAccessible(Z)V

    .line 1689
    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Object;

    invoke-virtual {v1, v4}, Ljava/lang/reflect/Constructor;->newInstance([Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/InstantiationException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_3

    move-result-object v4

    .line 1691
    .end local v0    # "clazz":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    .end local v1    # "constructor":Ljava/lang/reflect/Constructor;, "Ljava/lang/reflect/Constructor<*>;"
    :goto_0
    return-object v4

    .line 1690
    :catch_0
    move-exception v2

    .line 1691
    .local v2, "e":Ljava/lang/ClassNotFoundException;
    const/4 v4, 0x0

    goto :goto_0

    .line 1692
    .end local v2    # "e":Ljava/lang/ClassNotFoundException;
    :catch_1
    move-exception v2

    .line 1693
    .local v2, "e":Ljava/lang/reflect/InvocationTargetException;
    new-instance v4, Lio/realm/exceptions/RealmException;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Could not create an instance of "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5, v2}, Lio/realm/exceptions/RealmException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v4

    .line 1694
    .end local v2    # "e":Ljava/lang/reflect/InvocationTargetException;
    :catch_2
    move-exception v2

    .line 1695
    .local v2, "e":Ljava/lang/InstantiationException;
    new-instance v4, Lio/realm/exceptions/RealmException;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Could not create an instance of "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5, v2}, Lio/realm/exceptions/RealmException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v4

    .line 1696
    .end local v2    # "e":Ljava/lang/InstantiationException;
    :catch_3
    move-exception v2

    .line 1697
    .local v2, "e":Ljava/lang/IllegalAccessException;
    new-instance v4, Lio/realm/exceptions/RealmException;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Could not create an instance of "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5, v2}, Lio/realm/exceptions/RealmException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v4
.end method

.method private getFullStringScanner(Ljava/io/InputStream;)Ljava/util/Scanner;
    .locals 2
    .param p1, "in"    # Ljava/io/InputStream;

    .prologue
    .line 830
    new-instance v0, Ljava/util/Scanner;

    const-string v1, "UTF-8"

    invoke-direct {v0, p1, v1}, Ljava/util/Scanner;-><init>(Ljava/io/InputStream;Ljava/lang/String;)V

    const-string v1, "\\A"

    invoke-virtual {v0, v1}, Ljava/util/Scanner;->useDelimiter(Ljava/lang/String;)Ljava/util/Scanner;

    move-result-object v0

    return-object v0
.end method

.method public static getGlobalInstanceCount(Lio/realm/RealmConfiguration;)I
    .locals 2
    .param p0, "configuration"    # Lio/realm/RealmConfiguration;

    .prologue
    .line 1709
    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>(I)V

    .line 1710
    .local v0, "globalCount":Ljava/util/concurrent/atomic/AtomicInteger;
    new-instance v1, Lio/realm/Realm$4;

    invoke-direct {v1, v0}, Lio/realm/Realm$4;-><init>(Ljava/util/concurrent/atomic/AtomicInteger;)V

    invoke-static {p0, v1}, Lio/realm/RealmCache;->invokeWithGlobalRefCount(Lio/realm/RealmConfiguration;Lio/realm/RealmCache$Callback;)V

    .line 1716
    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v1

    return v1
.end method

.method public static getInstance(Lio/realm/RealmConfiguration;)Lio/realm/Realm;
    .locals 2
    .param p0, "configuration"    # Lio/realm/RealmConfiguration;

    .prologue
    .line 225
    if-nez p0, :cond_0

    .line 226
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "A non-null RealmConfiguration must be provided"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 228
    :cond_0
    const-class v0, Lio/realm/Realm;

    invoke-static {p0, v0}, Lio/realm/RealmCache;->createRealmOrGetFromCache(Lio/realm/RealmConfiguration;Ljava/lang/Class;)Lio/realm/BaseRealm;

    move-result-object v0

    check-cast v0, Lio/realm/Realm;

    return-object v0
.end method

.method public static getLocalInstanceCount(Lio/realm/RealmConfiguration;)I
    .locals 1
    .param p0, "configuration"    # Lio/realm/RealmConfiguration;

    .prologue
    .line 1727
    invoke-static {p0}, Lio/realm/RealmCache;->getLocalThreadCount(Lio/realm/RealmConfiguration;)I

    move-result v0

    return v0
.end method

.method public static declared-synchronized init(Landroid/content/Context;)V
    .locals 4
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 184
    const-class v1, Lio/realm/Realm;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lio/realm/BaseRealm;->applicationContext:Landroid/content/Context;

    if-nez v0, :cond_1

    .line 185
    if-nez p0, :cond_0

    .line 186
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v2, "Non-null context required."

    invoke-direct {v0, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 184
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    .line 188
    :cond_0
    :try_start_1
    invoke-static {p0}, Lio/realm/internal/RealmCore;->loadLibrary(Landroid/content/Context;)V

    .line 189
    new-instance v0, Lio/realm/RealmConfiguration$Builder;

    invoke-direct {v0, p0}, Lio/realm/RealmConfiguration$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0}, Lio/realm/RealmConfiguration$Builder;->build()Lio/realm/RealmConfiguration;

    move-result-object v0

    sput-object v0, Lio/realm/Realm;->defaultConfiguration:Lio/realm/RealmConfiguration;

    .line 190
    invoke-static {}, Lio/realm/internal/ObjectServerFacade;->getSyncFacadeIfPossible()Lio/realm/internal/ObjectServerFacade;

    move-result-object v0

    invoke-virtual {v0, p0}, Lio/realm/internal/ObjectServerFacade;->init(Landroid/content/Context;)V

    .line 191
    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    sput-object v0, Lio/realm/BaseRealm;->applicationContext:Landroid/content/Context;

    .line 192
    new-instance v0, Ljava/io/File;

    invoke-virtual {p0}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v2

    const-string v3, ".realm.temp"

    invoke-direct {v0, v2, v3}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-static {v0}, Lio/realm/internal/SharedRealm;->initialize(Ljava/io/File;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 194
    :cond_1
    monitor-exit v1

    return-void
.end method

.method private static initializeRealm(Lio/realm/Realm;)V
    .locals 13
    .param p0, "realm"    # Lio/realm/Realm;

    .prologue
    const/4 v9, 0x0

    .line 330
    const/4 v1, 0x0

    .line 332
    .local v1, "commitChanges":Z
    :try_start_0
    invoke-virtual {p0}, Lio/realm/Realm;->beginTransaction()V

    .line 333
    invoke-virtual {p0}, Lio/realm/Realm;->getVersion()J

    move-result-wide v2

    .line 334
    .local v2, "currentVersion":J
    const-wide/16 v10, -0x1

    cmp-long v10, v2, v10

    if-nez v10, :cond_0

    const/4 v9, 0x1

    .line 335
    .local v9, "unversioned":Z
    :cond_0
    move v1, v9

    .line 337
    if-eqz v9, :cond_1

    .line 338
    iget-object v10, p0, Lio/realm/Realm;->configuration:Lio/realm/RealmConfiguration;

    invoke-virtual {v10}, Lio/realm/RealmConfiguration;->getSchemaVersion()J

    move-result-wide v10

    invoke-virtual {p0, v10, v11}, Lio/realm/Realm;->setVersion(J)V

    .line 340
    :cond_1
    iget-object v10, p0, Lio/realm/Realm;->configuration:Lio/realm/RealmConfiguration;

    invoke-virtual {v10}, Lio/realm/RealmConfiguration;->getSchemaMediator()Lio/realm/internal/RealmProxyMediator;

    move-result-object v5

    .line 341
    .local v5, "mediator":Lio/realm/internal/RealmProxyMediator;
    invoke-virtual {v5}, Lio/realm/internal/RealmProxyMediator;->getModelClasses()Ljava/util/Set;

    move-result-object v7

    .line 343
    .local v7, "modelClasses":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Class<+Lio/realm/RealmModel;>;>;"
    new-instance v0, Ljava/util/HashMap;

    invoke-interface {v7}, Ljava/util/Set;->size()I

    move-result v10

    invoke-direct {v0, v10}, Ljava/util/HashMap;-><init>(I)V

    .line 344
    .local v0, "columnInfoMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/Class<+Lio/realm/RealmModel;>;Lio/realm/internal/ColumnInfo;>;"
    invoke-interface {v7}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v10

    :goto_0
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v11

    if-eqz v11, :cond_3

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/Class;

    .line 346
    .local v6, "modelClass":Ljava/lang/Class;, "Ljava/lang/Class<+Lio/realm/RealmModel;>;"
    if-eqz v9, :cond_2

    .line 347
    iget-object v11, p0, Lio/realm/Realm;->sharedRealm:Lio/realm/internal/SharedRealm;

    invoke-virtual {v5, v6, v11}, Lio/realm/internal/RealmProxyMediator;->createTable(Ljava/lang/Class;Lio/realm/internal/SharedRealm;)Lio/realm/internal/Table;

    .line 349
    :cond_2
    iget-object v11, p0, Lio/realm/Realm;->sharedRealm:Lio/realm/internal/SharedRealm;

    const/4 v12, 0x0

    invoke-virtual {v5, v6, v11, v12}, Lio/realm/internal/RealmProxyMediator;->validateTable(Ljava/lang/Class;Lio/realm/internal/SharedRealm;Z)Lio/realm/internal/ColumnInfo;

    move-result-object v11

    invoke-interface {v0, v6, v11}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 361
    .end local v0    # "columnInfoMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/Class<+Lio/realm/RealmModel;>;Lio/realm/internal/ColumnInfo;>;"
    .end local v2    # "currentVersion":J
    .end local v5    # "mediator":Lio/realm/internal/RealmProxyMediator;
    .end local v6    # "modelClass":Ljava/lang/Class;, "Ljava/lang/Class<+Lio/realm/RealmModel;>;"
    .end local v7    # "modelClasses":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Class<+Lio/realm/RealmModel;>;>;"
    .end local v9    # "unversioned":Z
    :catch_0
    move-exception v4

    .line 362
    .local v4, "e":Ljava/lang/Exception;
    const/4 v1, 0x0

    .line 363
    :try_start_1
    throw v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 365
    .end local v4    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v10

    if-eqz v1, :cond_7

    .line 366
    invoke-virtual {p0}, Lio/realm/Realm;->commitTransaction()V

    .line 368
    :goto_1
    throw v10

    .line 352
    .restart local v0    # "columnInfoMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/Class<+Lio/realm/RealmModel;>;Lio/realm/internal/ColumnInfo;>;"
    .restart local v2    # "currentVersion":J
    .restart local v5    # "mediator":Lio/realm/internal/RealmProxyMediator;
    .restart local v7    # "modelClasses":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Class<+Lio/realm/RealmModel;>;>;"
    .restart local v9    # "unversioned":Z
    :cond_3
    :try_start_2
    iget-object v10, p0, Lio/realm/Realm;->schema:Lio/realm/RealmSchema;

    new-instance v11, Lio/realm/internal/ColumnIndices;

    if-eqz v9, :cond_4

    iget-object v12, p0, Lio/realm/Realm;->configuration:Lio/realm/RealmConfiguration;

    .line 353
    invoke-virtual {v12}, Lio/realm/RealmConfiguration;->getSchemaVersion()J

    move-result-wide v2

    .end local v2    # "currentVersion":J
    :cond_4
    invoke-direct {v11, v2, v3, v0}, Lio/realm/internal/ColumnIndices;-><init>(JLjava/util/Map;)V

    iput-object v11, v10, Lio/realm/RealmSchema;->columnIndices:Lio/realm/internal/ColumnIndices;

    .line 355
    if-eqz v9, :cond_5

    .line 356
    iget-object v10, p0, Lio/realm/Realm;->configuration:Lio/realm/RealmConfiguration;

    invoke-virtual {v10}, Lio/realm/RealmConfiguration;->getInitialDataTransaction()Lio/realm/Realm$Transaction;

    move-result-object v8

    .line 357
    .local v8, "transaction":Lio/realm/Realm$Transaction;
    if-eqz v8, :cond_5

    .line 358
    invoke-interface {v8, p0}, Lio/realm/Realm$Transaction;->execute(Lio/realm/Realm;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 365
    .end local v8    # "transaction":Lio/realm/Realm$Transaction;
    :cond_5
    if-eqz v1, :cond_6

    .line 366
    invoke-virtual {p0}, Lio/realm/Realm;->commitTransaction()V

    .line 371
    :goto_2
    return-void

    .line 368
    :cond_6
    invoke-virtual {p0}, Lio/realm/Realm;->cancelTransaction()V

    goto :goto_2

    .end local v0    # "columnInfoMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/Class<+Lio/realm/RealmModel;>;Lio/realm/internal/ColumnInfo;>;"
    .end local v5    # "mediator":Lio/realm/internal/RealmProxyMediator;
    .end local v7    # "modelClasses":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Class<+Lio/realm/RealmModel;>;>;"
    .end local v9    # "unversioned":Z
    :cond_7
    invoke-virtual {p0}, Lio/realm/Realm;->cancelTransaction()V

    goto :goto_1
.end method

.method private static initializeSyncedRealm(Lio/realm/Realm;)V
    .locals 23
    .param p0, "realm"    # Lio/realm/Realm;

    .prologue
    .line 376
    const/4 v3, 0x0

    .line 378
    .local v3, "commitChanges":Z
    :try_start_0
    invoke-virtual/range {p0 .. p0}, Lio/realm/Realm;->beginTransaction()V

    .line 379
    invoke-virtual/range {p0 .. p0}, Lio/realm/Realm;->getVersion()J

    move-result-wide v4

    .line 380
    .local v4, "currentVersion":J
    const-wide/16 v18, -0x1

    cmp-long v18, v4, v18

    if-nez v18, :cond_0

    const/16 v17, 0x1

    .line 382
    .local v17, "unversioned":Z
    :goto_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lio/realm/Realm;->configuration:Lio/realm/RealmConfiguration;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Lio/realm/RealmConfiguration;->getSchemaMediator()Lio/realm/internal/RealmProxyMediator;

    move-result-object v7

    .line 383
    .local v7, "mediator":Lio/realm/internal/RealmProxyMediator;
    invoke-virtual {v7}, Lio/realm/internal/RealmProxyMediator;->getModelClasses()Ljava/util/Set;

    move-result-object v9

    .line 385
    .local v9, "modelClasses":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Class<+Lio/realm/RealmModel;>;>;"
    new-instance v13, Ljava/util/ArrayList;

    invoke-direct {v13}, Ljava/util/ArrayList;-><init>()V

    .line 386
    .local v13, "realmObjectSchemas":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lio/realm/RealmObjectSchema;>;"
    new-instance v14, Lio/realm/RealmSchema;

    invoke-direct {v14}, Lio/realm/RealmSchema;-><init>()V

    .line 387
    .local v14, "realmSchemaCache":Lio/realm/RealmSchema;
    invoke-interface {v9}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v18

    :goto_1
    invoke-interface/range {v18 .. v18}, Ljava/util/Iterator;->hasNext()Z

    move-result v19

    if-eqz v19, :cond_1

    invoke-interface/range {v18 .. v18}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/Class;

    .line 388
    .local v8, "modelClass":Ljava/lang/Class;, "Ljava/lang/Class<+Lio/realm/RealmModel;>;"
    invoke-virtual {v7, v8, v14}, Lio/realm/internal/RealmProxyMediator;->createRealmObjectSchema(Ljava/lang/Class;Lio/realm/RealmSchema;)Lio/realm/RealmObjectSchema;

    move-result-object v12

    .line 389
    .local v12, "realmObjectSchema":Lio/realm/RealmObjectSchema;
    invoke-virtual {v13, v12}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    .line 420
    .end local v4    # "currentVersion":J
    .end local v7    # "mediator":Lio/realm/internal/RealmProxyMediator;
    .end local v8    # "modelClass":Ljava/lang/Class;, "Ljava/lang/Class<+Lio/realm/RealmModel;>;"
    .end local v9    # "modelClasses":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Class<+Lio/realm/RealmModel;>;>;"
    .end local v12    # "realmObjectSchema":Lio/realm/RealmObjectSchema;
    .end local v13    # "realmObjectSchemas":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lio/realm/RealmObjectSchema;>;"
    .end local v14    # "realmSchemaCache":Lio/realm/RealmSchema;
    .end local v17    # "unversioned":Z
    :catch_0
    move-exception v6

    .line 421
    .local v6, "e":Ljava/lang/Exception;
    const/4 v3, 0x0

    .line 422
    :try_start_1
    throw v6
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 424
    .end local v6    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v18

    if-eqz v3, :cond_8

    .line 425
    invoke-virtual/range {p0 .. p0}, Lio/realm/Realm;->commitTransaction()V

    .line 427
    :goto_2
    throw v18

    .line 380
    .restart local v4    # "currentVersion":J
    :cond_0
    const/16 v17, 0x0

    goto :goto_0

    .line 393
    .restart local v7    # "mediator":Lio/realm/internal/RealmProxyMediator;
    .restart local v9    # "modelClasses":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Class<+Lio/realm/RealmModel;>;>;"
    .restart local v13    # "realmObjectSchemas":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lio/realm/RealmObjectSchema;>;"
    .restart local v14    # "realmSchemaCache":Lio/realm/RealmSchema;
    .restart local v17    # "unversioned":Z
    :cond_1
    :try_start_2
    new-instance v15, Lio/realm/RealmSchema;

    invoke-direct {v15, v13}, Lio/realm/RealmSchema;-><init>(Ljava/util/ArrayList;)V

    .line 394
    .local v15, "schema":Lio/realm/RealmSchema;
    move-object/from16 v0, p0

    iget-object v0, v0, Lio/realm/Realm;->configuration:Lio/realm/RealmConfiguration;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Lio/realm/RealmConfiguration;->getSchemaVersion()J

    move-result-wide v10

    .line 395
    .local v10, "newVersion":J
    move-object/from16 v0, p0

    iget-object v0, v0, Lio/realm/Realm;->sharedRealm:Lio/realm/internal/SharedRealm;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    invoke-virtual {v0, v15}, Lio/realm/internal/SharedRealm;->requiresMigration(Lio/realm/RealmSchema;)Z

    move-result v18

    if-eqz v18, :cond_3

    .line 396
    cmp-long v18, v4, v10

    if-ltz v18, :cond_2

    .line 397
    new-instance v18, Ljava/lang/IllegalArgumentException;

    const-string v19, "The schema was changed but the schema version was not updated. The configured schema version (%d) must be higher than the one in the Realm file (%d) in order to update the schema."

    const/16 v20, 0x2

    move/from16 v0, v20

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v20, v0

    const/16 v21, 0x0

    .line 399
    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v22

    aput-object v22, v20, v21

    const/16 v21, 0x1

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v22

    aput-object v22, v20, v21

    .line 397
    invoke-static/range {v19 .. v20}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v19

    invoke-direct/range {v18 .. v19}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v18

    .line 401
    :cond_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lio/realm/Realm;->sharedRealm:Lio/realm/internal/SharedRealm;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    invoke-virtual {v0, v15, v10, v11}, Lio/realm/internal/SharedRealm;->updateSchema(Lio/realm/RealmSchema;J)V

    .line 403
    move-object/from16 v0, p0

    invoke-virtual {v0, v10, v11}, Lio/realm/Realm;->setVersion(J)V

    .line 404
    const/4 v3, 0x1

    .line 407
    :cond_3
    new-instance v2, Ljava/util/HashMap;

    invoke-interface {v9}, Ljava/util/Set;->size()I

    move-result v18

    move/from16 v0, v18

    invoke-direct {v2, v0}, Ljava/util/HashMap;-><init>(I)V

    .line 408
    .local v2, "columnInfoMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/Class<+Lio/realm/RealmModel;>;Lio/realm/internal/ColumnInfo;>;"
    invoke-interface {v9}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v18

    :goto_3
    invoke-interface/range {v18 .. v18}, Ljava/util/Iterator;->hasNext()Z

    move-result v19

    if-eqz v19, :cond_4

    invoke-interface/range {v18 .. v18}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/Class;

    .line 409
    .restart local v8    # "modelClass":Ljava/lang/Class;, "Ljava/lang/Class<+Lio/realm/RealmModel;>;"
    move-object/from16 v0, p0

    iget-object v0, v0, Lio/realm/Realm;->sharedRealm:Lio/realm/internal/SharedRealm;

    move-object/from16 v19, v0

    const/16 v20, 0x0

    move-object/from16 v0, v19

    move/from16 v1, v20

    invoke-virtual {v7, v8, v0, v1}, Lio/realm/internal/RealmProxyMediator;->validateTable(Ljava/lang/Class;Lio/realm/internal/SharedRealm;Z)Lio/realm/internal/ColumnInfo;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-interface {v2, v8, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_3

    .line 412
    .end local v8    # "modelClass":Ljava/lang/Class;, "Ljava/lang/Class<+Lio/realm/RealmModel;>;"
    :cond_4
    move-object/from16 v0, p0

    iget-object v0, v0, Lio/realm/Realm;->schema:Lio/realm/RealmSchema;

    move-object/from16 v18, v0

    new-instance v19, Lio/realm/internal/ColumnIndices;

    if-eqz v17, :cond_6

    .end local v10    # "newVersion":J
    :goto_4
    move-object/from16 v0, v19

    invoke-direct {v0, v10, v11, v2}, Lio/realm/internal/ColumnIndices;-><init>(JLjava/util/Map;)V

    move-object/from16 v0, v19

    move-object/from16 v1, v18

    iput-object v0, v1, Lio/realm/RealmSchema;->columnIndices:Lio/realm/internal/ColumnIndices;

    .line 414
    if-eqz v17, :cond_5

    .line 415
    move-object/from16 v0, p0

    iget-object v0, v0, Lio/realm/Realm;->configuration:Lio/realm/RealmConfiguration;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Lio/realm/RealmConfiguration;->getInitialDataTransaction()Lio/realm/Realm$Transaction;

    move-result-object v16

    .line 416
    .local v16, "transaction":Lio/realm/Realm$Transaction;
    if-eqz v16, :cond_5

    .line 417
    move-object/from16 v0, v16

    move-object/from16 v1, p0

    invoke-interface {v0, v1}, Lio/realm/Realm$Transaction;->execute(Lio/realm/Realm;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 424
    .end local v16    # "transaction":Lio/realm/Realm$Transaction;
    :cond_5
    if-eqz v3, :cond_7

    .line 425
    invoke-virtual/range {p0 .. p0}, Lio/realm/Realm;->commitTransaction()V

    .line 430
    :goto_5
    return-void

    .restart local v10    # "newVersion":J
    :cond_6
    move-wide v10, v4

    .line 412
    goto :goto_4

    .line 427
    .end local v10    # "newVersion":J
    :cond_7
    invoke-virtual/range {p0 .. p0}, Lio/realm/Realm;->cancelTransaction()V

    goto :goto_5

    .end local v2    # "columnInfoMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/Class<+Lio/realm/RealmModel;>;Lio/realm/internal/ColumnInfo;>;"
    .end local v4    # "currentVersion":J
    .end local v7    # "mediator":Lio/realm/internal/RealmProxyMediator;
    .end local v9    # "modelClasses":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Class<+Lio/realm/RealmModel;>;>;"
    .end local v13    # "realmObjectSchemas":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lio/realm/RealmObjectSchema;>;"
    .end local v14    # "realmSchemaCache":Lio/realm/RealmSchema;
    .end local v15    # "schema":Lio/realm/RealmSchema;
    .end local v17    # "unversioned":Z
    :cond_8
    invoke-virtual/range {p0 .. p0}, Lio/realm/Realm;->cancelTransaction()V

    goto/16 :goto_2
.end method

.method public static migrateRealm(Lio/realm/RealmConfiguration;)V
    .locals 1
    .param p0, "configuration"    # Lio/realm/RealmConfiguration;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/FileNotFoundException;
        }
    .end annotation

    .prologue
    .line 1561
    const/4 v0, 0x0

    check-cast v0, Lio/realm/RealmMigration;

    invoke-static {p0, v0}, Lio/realm/Realm;->migrateRealm(Lio/realm/RealmConfiguration;Lio/realm/RealmMigration;)V

    .line 1562
    return-void
.end method

.method public static migrateRealm(Lio/realm/RealmConfiguration;Lio/realm/RealmMigration;)V
    .locals 2
    .param p0, "configuration"    # Lio/realm/RealmConfiguration;
    .param p1, "migration"    # Lio/realm/RealmMigration;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/FileNotFoundException;
        }
    .end annotation

    .prologue
    .line 1590
    new-instance v0, Lio/realm/Realm$3;

    invoke-direct {v0}, Lio/realm/Realm$3;-><init>()V

    const/4 v1, 0x0

    invoke-static {p0, p1, v0, v1}, Lio/realm/BaseRealm;->migrateRealm(Lio/realm/RealmConfiguration;Lio/realm/RealmMigration;Lio/realm/BaseRealm$MigrationCallback;Lio/realm/exceptions/RealmMigrationNeededException;)V

    .line 1595
    return-void
.end method

.method private static migrateRealm(Lio/realm/RealmConfiguration;Lio/realm/exceptions/RealmMigrationNeededException;)V
    .locals 2
    .param p0, "configuration"    # Lio/realm/RealmConfiguration;
    .param p1, "cause"    # Lio/realm/exceptions/RealmMigrationNeededException;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/FileNotFoundException;
        }
    .end annotation

    .prologue
    .line 1573
    const/4 v0, 0x0

    new-instance v1, Lio/realm/Realm$2;

    invoke-direct {v1}, Lio/realm/Realm$2;-><init>()V

    invoke-static {p0, v0, v1, p1}, Lio/realm/BaseRealm;->migrateRealm(Lio/realm/RealmConfiguration;Lio/realm/RealmMigration;Lio/realm/BaseRealm$MigrationCallback;Lio/realm/exceptions/RealmMigrationNeededException;)V

    .line 1578
    return-void
.end method

.method public static removeDefaultConfiguration()V
    .locals 1

    .prologue
    .line 250
    const/4 v0, 0x0

    sput-object v0, Lio/realm/Realm;->defaultConfiguration:Lio/realm/RealmConfiguration;

    .line 251
    return-void
.end method

.method public static setDefaultConfiguration(Lio/realm/RealmConfiguration;)V
    .locals 2
    .param p0, "configuration"    # Lio/realm/RealmConfiguration;

    .prologue
    .line 239
    if-nez p0, :cond_0

    .line 240
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "A non-null RealmConfiguration must be provided"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 242
    :cond_0
    sput-object p0, Lio/realm/Realm;->defaultConfiguration:Lio/realm/RealmConfiguration;

    .line 243
    return-void
.end method


# virtual methods
.method public addChangeListener(Lio/realm/RealmChangeListener;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/realm/RealmChangeListener",
            "<",
            "Lio/realm/Realm;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1284
    .local p1, "listener":Lio/realm/RealmChangeListener;, "Lio/realm/RealmChangeListener<Lio/realm/Realm;>;"
    invoke-virtual {p0, p1}, Lio/realm/Realm;->addListener(Lio/realm/RealmChangeListener;)V

    .line 1285
    return-void
.end method

.method public asObservable()Lrx/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable",
            "<",
            "Lio/realm/Realm;",
            ">;"
        }
    .end annotation

    .prologue
    .line 147
    iget-object v0, p0, Lio/realm/Realm;->configuration:Lio/realm/RealmConfiguration;

    invoke-virtual {v0}, Lio/realm/RealmConfiguration;->getRxFactory()Lio/realm/rx/RxObservableFactory;

    move-result-object v0

    invoke-interface {v0, p0}, Lio/realm/rx/RxObservableFactory;->from(Lio/realm/Realm;)Lrx/Observable;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic beginTransaction()V
    .locals 0

    .prologue
    .line 126
    invoke-super {p0}, Lio/realm/BaseRealm;->beginTransaction()V

    return-void
.end method

.method public bridge synthetic cancelTransaction()V
    .locals 0

    .prologue
    .line 126
    invoke-super {p0}, Lio/realm/BaseRealm;->cancelTransaction()V

    return-void
.end method

.method public bridge synthetic close()V
    .locals 0

    .prologue
    .line 126
    invoke-super {p0}, Lio/realm/BaseRealm;->close()V

    return-void
.end method

.method public bridge synthetic commitTransaction()V
    .locals 0

    .prologue
    .line 126
    invoke-super {p0}, Lio/realm/BaseRealm;->commitTransaction()V

    return-void
.end method

.method public copyFromRealm(Lio/realm/RealmModel;)Lio/realm/RealmModel;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E::",
            "Lio/realm/RealmModel;",
            ">(TE;)TE;"
        }
    .end annotation

    .prologue
    .line 1225
    .local p1, "realmObject":Lio/realm/RealmModel;, "TE;"
    const v0, 0x7fffffff

    invoke-virtual {p0, p1, v0}, Lio/realm/Realm;->copyFromRealm(Lio/realm/RealmModel;I)Lio/realm/RealmModel;

    move-result-object v0

    return-object v0
.end method

.method public copyFromRealm(Lio/realm/RealmModel;I)Lio/realm/RealmModel;
    .locals 1
    .param p2, "maxDepth"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E::",
            "Lio/realm/RealmModel;",
            ">(TE;I)TE;"
        }
    .end annotation

    .prologue
    .line 1250
    .local p1, "realmObject":Lio/realm/RealmModel;, "TE;"
    invoke-direct {p0, p2}, Lio/realm/Realm;->checkMaxDepth(I)V

    .line 1251
    invoke-direct {p0, p1}, Lio/realm/Realm;->checkValidObjectForDetach(Lio/realm/RealmModel;)V

    .line 1252
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    invoke-direct {p0, p1, p2, v0}, Lio/realm/Realm;->createDetachedCopy(Lio/realm/RealmModel;ILjava/util/Map;)Lio/realm/RealmModel;

    move-result-object v0

    return-object v0
.end method

.method public copyFromRealm(Ljava/lang/Iterable;)Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E::",
            "Lio/realm/RealmModel;",
            ">(",
            "Ljava/lang/Iterable",
            "<TE;>;)",
            "Ljava/util/List",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 1167
    .local p1, "realmObjects":Ljava/lang/Iterable;, "Ljava/lang/Iterable<TE;>;"
    const v0, 0x7fffffff

    invoke-virtual {p0, p1, v0}, Lio/realm/Realm;->copyFromRealm(Ljava/lang/Iterable;I)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public copyFromRealm(Ljava/lang/Iterable;I)Ljava/util/List;
    .locals 5
    .param p2, "maxDepth"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E::",
            "Lio/realm/RealmModel;",
            ">(",
            "Ljava/lang/Iterable",
            "<TE;>;I)",
            "Ljava/util/List",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 1192
    .local p1, "realmObjects":Ljava/lang/Iterable;, "Ljava/lang/Iterable<TE;>;"
    invoke-direct {p0, p2}, Lio/realm/Realm;->checkMaxDepth(I)V

    .line 1193
    if-nez p1, :cond_1

    .line 1194
    new-instance v2, Ljava/util/ArrayList;

    const/4 v3, 0x0

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(I)V

    .line 1204
    :cond_0
    return-object v2

    .line 1197
    :cond_1
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 1198
    .local v2, "unmanagedObjects":Ljava/util/ArrayList;, "Ljava/util/ArrayList<TE;>;"
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 1199
    .local v0, "listCache":Ljava/util/Map;, "Ljava/util/Map<Lio/realm/RealmModel;Lio/realm/internal/RealmObjectProxy$CacheData<Lio/realm/RealmModel;>;>;"
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lio/realm/RealmModel;

    .line 1200
    .local v1, "object":Lio/realm/RealmModel;, "TE;"
    invoke-direct {p0, v1}, Lio/realm/Realm;->checkValidObjectForDetach(Lio/realm/RealmModel;)V

    .line 1201
    invoke-direct {p0, v1, p2, v0}, Lio/realm/Realm;->createDetachedCopy(Lio/realm/RealmModel;ILjava/util/Map;)Lio/realm/RealmModel;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public copyToRealm(Lio/realm/RealmModel;)Lio/realm/RealmModel;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E::",
            "Lio/realm/RealmModel;",
            ">(TE;)TE;"
        }
    .end annotation

    .prologue
    .line 932
    .local p1, "object":Lio/realm/RealmModel;, "TE;"
    invoke-direct {p0, p1}, Lio/realm/Realm;->checkNotNullObject(Lio/realm/RealmModel;)V

    .line 933
    const/4 v0, 0x0

    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    invoke-direct {p0, p1, v0, v1}, Lio/realm/Realm;->copyOrUpdate(Lio/realm/RealmModel;ZLjava/util/Map;)Lio/realm/RealmModel;

    move-result-object v0

    return-object v0
.end method

.method public copyToRealm(Ljava/lang/Iterable;)Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E::",
            "Lio/realm/RealmModel;",
            ">(",
            "Ljava/lang/Iterable",
            "<TE;>;)",
            "Ljava/util/List",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 970
    .local p1, "objects":Ljava/lang/Iterable;, "Ljava/lang/Iterable<TE;>;"
    if-nez p1, :cond_1

    .line 971
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 980
    :cond_0
    return-object v2

    .line 973
    :cond_1
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 974
    .local v0, "cache":Ljava/util/Map;, "Ljava/util/Map<Lio/realm/RealmModel;Lio/realm/internal/RealmObjectProxy;>;"
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 975
    .local v2, "realmObjects":Ljava/util/ArrayList;, "Ljava/util/ArrayList<TE;>;"
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lio/realm/RealmModel;

    .line 976
    .local v1, "object":Lio/realm/RealmModel;, "TE;"
    invoke-direct {p0, v1}, Lio/realm/Realm;->checkNotNullObject(Lio/realm/RealmModel;)V

    .line 977
    const/4 v4, 0x0

    invoke-direct {p0, v1, v4, v0}, Lio/realm/Realm;->copyOrUpdate(Lio/realm/RealmModel;ZLjava/util/Map;)Lio/realm/RealmModel;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public copyToRealmOrUpdate(Lio/realm/RealmModel;)Lio/realm/RealmModel;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E::",
            "Lio/realm/RealmModel;",
            ">(TE;)TE;"
        }
    .end annotation

    .prologue
    .line 951
    .local p1, "object":Lio/realm/RealmModel;, "TE;"
    invoke-direct {p0, p1}, Lio/realm/Realm;->checkNotNullObject(Lio/realm/RealmModel;)V

    .line 952
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-direct {p0, v0}, Lio/realm/Realm;->checkHasPrimaryKey(Ljava/lang/Class;)V

    .line 953
    const/4 v0, 0x1

    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    invoke-direct {p0, p1, v0, v1}, Lio/realm/Realm;->copyOrUpdate(Lio/realm/RealmModel;ZLjava/util/Map;)Lio/realm/RealmModel;

    move-result-object v0

    return-object v0
.end method

.method public copyToRealmOrUpdate(Ljava/lang/Iterable;)Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E::",
            "Lio/realm/RealmModel;",
            ">(",
            "Ljava/lang/Iterable",
            "<TE;>;)",
            "Ljava/util/List",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 1135
    .local p1, "objects":Ljava/lang/Iterable;, "Ljava/lang/Iterable<TE;>;"
    if-nez p1, :cond_1

    .line 1136
    new-instance v2, Ljava/util/ArrayList;

    const/4 v3, 0x0

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(I)V

    .line 1146
    :cond_0
    return-object v2

    .line 1139
    :cond_1
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 1140
    .local v0, "cache":Ljava/util/Map;, "Ljava/util/Map<Lio/realm/RealmModel;Lio/realm/internal/RealmObjectProxy;>;"
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 1141
    .local v2, "realmObjects":Ljava/util/ArrayList;, "Ljava/util/ArrayList<TE;>;"
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lio/realm/RealmModel;

    .line 1142
    .local v1, "object":Lio/realm/RealmModel;, "TE;"
    invoke-direct {p0, v1}, Lio/realm/Realm;->checkNotNullObject(Lio/realm/RealmModel;)V

    .line 1143
    const/4 v4, 0x1

    invoke-direct {p0, v1, v4, v0}, Lio/realm/Realm;->copyOrUpdate(Lio/realm/RealmModel;ZLjava/util/Map;)Lio/realm/RealmModel;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public createAllFromJson(Ljava/lang/Class;Ljava/io/InputStream;)V
    .locals 3
    .param p2, "inputStream"    # Ljava/io/InputStream;
    .annotation build Landroid/annotation/TargetApi;
        value = 0xb
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E::",
            "Lio/realm/RealmModel;",
            ">(",
            "Ljava/lang/Class",
            "<TE;>;",
            "Ljava/io/InputStream;",
            ")V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 563
    .local p1, "clazz":Ljava/lang/Class;, "Ljava/lang/Class<TE;>;"
    if-eqz p1, :cond_0

    if-nez p2, :cond_1

    .line 578
    :cond_0
    :goto_0
    return-void

    .line 566
    :cond_1
    invoke-virtual {p0}, Lio/realm/Realm;->checkIfValid()V

    .line 568
    new-instance v0, Landroid/util/JsonReader;

    new-instance v1, Ljava/io/InputStreamReader;

    const-string v2, "UTF-8"

    invoke-direct {v1, p2, v2}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;Ljava/lang/String;)V

    invoke-direct {v0, v1}, Landroid/util/JsonReader;-><init>(Ljava/io/Reader;)V

    .line 570
    .local v0, "reader":Landroid/util/JsonReader;
    :try_start_0
    invoke-virtual {v0}, Landroid/util/JsonReader;->beginArray()V

    .line 571
    :goto_1
    invoke-virtual {v0}, Landroid/util/JsonReader;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 572
    iget-object v1, p0, Lio/realm/Realm;->configuration:Lio/realm/RealmConfiguration;

    invoke-virtual {v1}, Lio/realm/RealmConfiguration;->getSchemaMediator()Lio/realm/internal/RealmProxyMediator;

    move-result-object v1

    invoke-virtual {v1, p1, p0, v0}, Lio/realm/internal/RealmProxyMediator;->createUsingJsonStream(Ljava/lang/Class;Lio/realm/Realm;Landroid/util/JsonReader;)Lio/realm/RealmModel;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    .line 576
    :catchall_0
    move-exception v1

    invoke-virtual {v0}, Landroid/util/JsonReader;->close()V

    throw v1

    .line 574
    :cond_2
    :try_start_1
    invoke-virtual {v0}, Landroid/util/JsonReader;->endArray()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 576
    invoke-virtual {v0}, Landroid/util/JsonReader;->close()V

    goto :goto_0
.end method

.method public createAllFromJson(Ljava/lang/Class;Ljava/lang/String;)V
    .locals 4
    .param p2, "json"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E::",
            "Lio/realm/RealmModel;",
            ">(",
            "Ljava/lang/Class",
            "<TE;>;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 501
    .local p1, "clazz":Ljava/lang/Class;, "Ljava/lang/Class<TE;>;"
    if-eqz p1, :cond_0

    if-eqz p2, :cond_0

    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v2

    if-nez v2, :cond_1

    .line 513
    :cond_0
    :goto_0
    return-void

    .line 507
    :cond_1
    :try_start_0
    new-instance v0, Lorg/json/JSONArray;

    invoke-direct {v0, p2}, Lorg/json/JSONArray;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 512
    .local v0, "arr":Lorg/json/JSONArray;
    invoke-virtual {p0, p1, v0}, Lio/realm/Realm;->createAllFromJson(Ljava/lang/Class;Lorg/json/JSONArray;)V

    goto :goto_0

    .line 508
    .end local v0    # "arr":Lorg/json/JSONArray;
    :catch_0
    move-exception v1

    .line 509
    .local v1, "e":Lorg/json/JSONException;
    new-instance v2, Lio/realm/exceptions/RealmException;

    const-string v3, "Could not create JSON array from string"

    invoke-direct {v2, v3, v1}, Lio/realm/exceptions/RealmException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v2
.end method

.method public createAllFromJson(Ljava/lang/Class;Lorg/json/JSONArray;)V
    .locals 5
    .param p2, "json"    # Lorg/json/JSONArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E::",
            "Lio/realm/RealmModel;",
            ">(",
            "Ljava/lang/Class",
            "<TE;>;",
            "Lorg/json/JSONArray;",
            ")V"
        }
    .end annotation

    .prologue
    .line 445
    .local p1, "clazz":Ljava/lang/Class;, "Ljava/lang/Class<TE;>;"
    if-eqz p1, :cond_0

    if-nez p2, :cond_1

    .line 457
    :cond_0
    return-void

    .line 448
    :cond_1
    invoke-virtual {p0}, Lio/realm/Realm;->checkIfValid()V

    .line 450
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    invoke-virtual {p2}, Lorg/json/JSONArray;->length()I

    move-result v2

    if-ge v1, v2, :cond_0

    .line 452
    :try_start_0
    iget-object v2, p0, Lio/realm/Realm;->configuration:Lio/realm/RealmConfiguration;

    invoke-virtual {v2}, Lio/realm/RealmConfiguration;->getSchemaMediator()Lio/realm/internal/RealmProxyMediator;

    move-result-object v2

    invoke-virtual {p2, v1}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v2, p1, p0, v3, v4}, Lio/realm/internal/RealmProxyMediator;->createOrUpdateUsingJsonObject(Ljava/lang/Class;Lio/realm/Realm;Lorg/json/JSONObject;Z)Lio/realm/RealmModel;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 450
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 453
    :catch_0
    move-exception v0

    .line 454
    .local v0, "e":Lorg/json/JSONException;
    new-instance v2, Lio/realm/exceptions/RealmException;

    const-string v3, "Could not map JSON"

    invoke-direct {v2, v3, v0}, Lio/realm/exceptions/RealmException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v2
.end method

.method public createObject(Ljava/lang/Class;)Lio/realm/RealmModel;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E::",
            "Lio/realm/RealmModel;",
            ">(",
            "Ljava/lang/Class",
            "<TE;>;)TE;"
        }
    .end annotation

    .prologue
    .line 846
    .local p1, "clazz":Ljava/lang/Class;, "Ljava/lang/Class<TE;>;"
    invoke-virtual {p0}, Lio/realm/Realm;->checkIfValid()V

    .line 847
    const/4 v0, 0x1

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v1

    invoke-virtual {p0, p1, v0, v1}, Lio/realm/Realm;->createObjectInternal(Ljava/lang/Class;ZLjava/util/List;)Lio/realm/RealmModel;

    move-result-object v0

    return-object v0
.end method

.method public createObject(Ljava/lang/Class;Ljava/lang/Object;)Lio/realm/RealmModel;
    .locals 2
    .param p2, "primaryKeyValue"    # Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E::",
            "Lio/realm/RealmModel;",
            ">(",
            "Ljava/lang/Class",
            "<TE;>;",
            "Ljava/lang/Object;",
            ")TE;"
        }
    .end annotation

    .prologue
    .line 890
    .local p1, "clazz":Ljava/lang/Class;, "Ljava/lang/Class<TE;>;"
    invoke-virtual {p0}, Lio/realm/Realm;->checkIfValid()V

    .line 891
    const/4 v0, 0x1

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v1

    invoke-virtual {p0, p1, p2, v0, v1}, Lio/realm/Realm;->createObjectInternal(Ljava/lang/Class;Ljava/lang/Object;ZLjava/util/List;)Lio/realm/RealmModel;

    move-result-object v0

    return-object v0
.end method

.method public createObjectFromJson(Ljava/lang/Class;Ljava/io/InputStream;)Lio/realm/RealmModel;
    .locals 8
    .param p2, "inputStream"    # Ljava/io/InputStream;
    .annotation build Landroid/annotation/TargetApi;
        value = 0xb
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E::",
            "Lio/realm/RealmModel;",
            ">(",
            "Ljava/lang/Class",
            "<TE;>;",
            "Ljava/io/InputStream;",
            ")TE;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 754
    .local p1, "clazz":Ljava/lang/Class;, "Ljava/lang/Class<TE;>;"
    if-eqz p1, :cond_0

    if-nez p2, :cond_2

    .line 755
    :cond_0
    const/4 v3, 0x0

    .line 784
    :cond_1
    :goto_0
    return-object v3

    .line 757
    :cond_2
    invoke-virtual {p0}, Lio/realm/Realm;->checkIfValid()V

    .line 759
    iget-object v6, p0, Lio/realm/Realm;->schema:Lio/realm/RealmSchema;

    invoke-virtual {v6, p1}, Lio/realm/RealmSchema;->getTable(Ljava/lang/Class;)Lio/realm/internal/Table;

    move-result-object v5

    .line 760
    .local v5, "table":Lio/realm/internal/Table;
    invoke-virtual {v5}, Lio/realm/internal/Table;->hasPrimaryKey()Z

    move-result v6

    if-eqz v6, :cond_4

    .line 763
    const/4 v4, 0x0

    .line 765
    .local v4, "scanner":Ljava/util/Scanner;
    :try_start_0
    invoke-direct {p0, p2}, Lio/realm/Realm;->getFullStringScanner(Ljava/io/InputStream;)Ljava/util/Scanner;

    move-result-object v4

    .line 766
    new-instance v1, Lorg/json/JSONObject;

    invoke-virtual {v4}, Ljava/util/Scanner;->next()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v1, v6}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 767
    .local v1, "json":Lorg/json/JSONObject;
    iget-object v6, p0, Lio/realm/Realm;->configuration:Lio/realm/RealmConfiguration;

    invoke-virtual {v6}, Lio/realm/RealmConfiguration;->getSchemaMediator()Lio/realm/internal/RealmProxyMediator;

    move-result-object v6

    const/4 v7, 0x0

    invoke-virtual {v6, p1, p0, v1, v7}, Lio/realm/internal/RealmProxyMediator;->createOrUpdateUsingJsonObject(Ljava/lang/Class;Lio/realm/Realm;Lorg/json/JSONObject;Z)Lio/realm/RealmModel;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v3

    .line 772
    .local v3, "realmObject":Lio/realm/RealmModel;, "TE;"
    if-eqz v4, :cond_1

    .line 773
    invoke-virtual {v4}, Ljava/util/Scanner;->close()V

    goto :goto_0

    .line 769
    .end local v1    # "json":Lorg/json/JSONObject;
    .end local v3    # "realmObject":Lio/realm/RealmModel;, "TE;"
    :catch_0
    move-exception v0

    .line 770
    .local v0, "e":Lorg/json/JSONException;
    :try_start_1
    new-instance v6, Lio/realm/exceptions/RealmException;

    const-string v7, "Failed to read JSON"

    invoke-direct {v6, v7, v0}, Lio/realm/exceptions/RealmException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v6
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 772
    .end local v0    # "e":Lorg/json/JSONException;
    :catchall_0
    move-exception v6

    if-eqz v4, :cond_3

    .line 773
    invoke-virtual {v4}, Ljava/util/Scanner;->close()V

    :cond_3
    throw v6

    .line 777
    .end local v4    # "scanner":Ljava/util/Scanner;
    :cond_4
    new-instance v2, Landroid/util/JsonReader;

    new-instance v6, Ljava/io/InputStreamReader;

    const-string v7, "UTF-8"

    invoke-direct {v6, p2, v7}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;Ljava/lang/String;)V

    invoke-direct {v2, v6}, Landroid/util/JsonReader;-><init>(Ljava/io/Reader;)V

    .line 779
    .local v2, "reader":Landroid/util/JsonReader;
    :try_start_2
    iget-object v6, p0, Lio/realm/Realm;->configuration:Lio/realm/RealmConfiguration;

    invoke-virtual {v6}, Lio/realm/RealmConfiguration;->getSchemaMediator()Lio/realm/internal/RealmProxyMediator;

    move-result-object v6

    invoke-virtual {v6, p1, p0, v2}, Lio/realm/internal/RealmProxyMediator;->createUsingJsonStream(Ljava/lang/Class;Lio/realm/Realm;Landroid/util/JsonReader;)Lio/realm/RealmModel;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result-object v3

    .line 781
    .restart local v3    # "realmObject":Lio/realm/RealmModel;, "TE;"
    invoke-virtual {v2}, Landroid/util/JsonReader;->close()V

    goto :goto_0

    .end local v3    # "realmObject":Lio/realm/RealmModel;, "TE;"
    :catchall_1
    move-exception v6

    invoke-virtual {v2}, Landroid/util/JsonReader;->close()V

    throw v6
.end method

.method public createObjectFromJson(Ljava/lang/Class;Ljava/lang/String;)Lio/realm/RealmModel;
    .locals 4
    .param p2, "json"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E::",
            "Lio/realm/RealmModel;",
            ">(",
            "Ljava/lang/Class",
            "<TE;>;",
            "Ljava/lang/String;",
            ")TE;"
        }
    .end annotation

    .prologue
    .line 690
    .local p1, "clazz":Ljava/lang/Class;, "Ljava/lang/Class<TE;>;"
    if-eqz p1, :cond_0

    if-eqz p2, :cond_0

    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v2

    if-nez v2, :cond_1

    .line 691
    :cond_0
    const/4 v2, 0x0

    .line 701
    :goto_0
    return-object v2

    .line 696
    :cond_1
    :try_start_0
    new-instance v1, Lorg/json/JSONObject;

    invoke-direct {v1, p2}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 701
    .local v1, "obj":Lorg/json/JSONObject;
    invoke-virtual {p0, p1, v1}, Lio/realm/Realm;->createObjectFromJson(Ljava/lang/Class;Lorg/json/JSONObject;)Lio/realm/RealmModel;

    move-result-object v2

    goto :goto_0

    .line 697
    .end local v1    # "obj":Lorg/json/JSONObject;
    :catch_0
    move-exception v0

    .line 698
    .local v0, "e":Lorg/json/JSONException;
    new-instance v2, Lio/realm/exceptions/RealmException;

    const-string v3, "Could not create Json object from string"

    invoke-direct {v2, v3, v0}, Lio/realm/exceptions/RealmException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v2
.end method

.method public createObjectFromJson(Ljava/lang/Class;Lorg/json/JSONObject;)Lio/realm/RealmModel;
    .locals 3
    .param p2, "json"    # Lorg/json/JSONObject;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E::",
            "Lio/realm/RealmModel;",
            ">(",
            "Ljava/lang/Class",
            "<TE;>;",
            "Lorg/json/JSONObject;",
            ")TE;"
        }
    .end annotation

    .prologue
    .line 637
    .local p1, "clazz":Ljava/lang/Class;, "Ljava/lang/Class<TE;>;"
    if-eqz p1, :cond_0

    if-nez p2, :cond_1

    .line 638
    :cond_0
    const/4 v1, 0x0

    .line 643
    :goto_0
    return-object v1

    .line 640
    :cond_1
    invoke-virtual {p0}, Lio/realm/Realm;->checkIfValid()V

    .line 643
    :try_start_0
    iget-object v1, p0, Lio/realm/Realm;->configuration:Lio/realm/RealmConfiguration;

    invoke-virtual {v1}, Lio/realm/RealmConfiguration;->getSchemaMediator()Lio/realm/internal/RealmProxyMediator;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, p1, p0, p2, v2}, Lio/realm/internal/RealmProxyMediator;->createOrUpdateUsingJsonObject(Ljava/lang/Class;Lio/realm/Realm;Lorg/json/JSONObject;Z)Lio/realm/RealmModel;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    goto :goto_0

    .line 644
    :catch_0
    move-exception v0

    .line 645
    .local v0, "e":Lorg/json/JSONException;
    new-instance v1, Lio/realm/exceptions/RealmException;

    const-string v2, "Could not map JSON"

    invoke-direct {v1, v2, v0}, Lio/realm/exceptions/RealmException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method

.method createObjectInternal(Ljava/lang/Class;Ljava/lang/Object;ZLjava/util/List;)Lio/realm/RealmModel;
    .locals 7
    .param p2, "primaryKeyValue"    # Ljava/lang/Object;
    .param p3, "acceptDefaultValue"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E::",
            "Lio/realm/RealmModel;",
            ">(",
            "Ljava/lang/Class",
            "<TE;>;",
            "Ljava/lang/Object;",
            "Z",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)TE;"
        }
    .end annotation

    .prologue
    .line 913
    .local p1, "clazz":Ljava/lang/Class;, "Ljava/lang/Class<TE;>;"
    .local p4, "excludeFields":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    iget-object v0, p0, Lio/realm/Realm;->schema:Lio/realm/RealmSchema;

    invoke-virtual {v0, p1}, Lio/realm/RealmSchema;->getTable(Ljava/lang/Class;)Lio/realm/internal/Table;

    move-result-object v6

    .line 914
    .local v6, "table":Lio/realm/internal/Table;
    invoke-virtual {v6, p2}, Lio/realm/internal/Table;->addEmptyRowWithPrimaryKey(Ljava/lang/Object;)J

    move-result-wide v2

    .local v2, "rowIndex":J
    move-object v0, p0

    move-object v1, p1

    move v4, p3

    move-object v5, p4

    .line 915
    invoke-virtual/range {v0 .. v5}, Lio/realm/Realm;->get(Ljava/lang/Class;JZLjava/util/List;)Lio/realm/RealmModel;

    move-result-object v0

    return-object v0
.end method

.method createObjectInternal(Ljava/lang/Class;ZLjava/util/List;)Lio/realm/RealmModel;
    .locals 8
    .param p2, "acceptDefaultValue"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E::",
            "Lio/realm/RealmModel;",
            ">(",
            "Ljava/lang/Class",
            "<TE;>;Z",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)TE;"
        }
    .end annotation

    .prologue
    .line 864
    .local p1, "clazz":Ljava/lang/Class;, "Ljava/lang/Class<TE;>;"
    .local p3, "excludeFields":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    iget-object v0, p0, Lio/realm/Realm;->schema:Lio/realm/RealmSchema;

    invoke-virtual {v0, p1}, Lio/realm/RealmSchema;->getTable(Ljava/lang/Class;)Lio/realm/internal/Table;

    move-result-object v6

    .line 866
    .local v6, "table":Lio/realm/internal/Table;
    invoke-virtual {v6}, Lio/realm/internal/Table;->hasPrimaryKey()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 867
    new-instance v0, Lio/realm/exceptions/RealmException;

    const-string v1, "\'%s\' has a primary key, use \'createObject(Class<E>, Object)\' instead."

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    .line 868
    invoke-virtual {v6}, Lio/realm/internal/Table;->getName()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Lio/realm/internal/Table;->tableNameToClassName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v4, v5

    .line 867
    invoke-static {v1, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lio/realm/exceptions/RealmException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 870
    :cond_0
    invoke-virtual {v6}, Lio/realm/internal/Table;->addEmptyRow()J

    move-result-wide v2

    .local v2, "rowIndex":J
    move-object v0, p0

    move-object v1, p1

    move v4, p2

    move-object v5, p3

    .line 871
    invoke-virtual/range {v0 .. v5}, Lio/realm/Realm;->get(Ljava/lang/Class;JZLjava/util/List;)Lio/realm/RealmModel;

    move-result-object v0

    return-object v0
.end method

.method public createOrUpdateAllFromJson(Ljava/lang/Class;Ljava/io/InputStream;)V
    .locals 7
    .param p2, "in"    # Ljava/io/InputStream;
    .annotation build Landroid/annotation/TargetApi;
        value = 0xb
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E::",
            "Lio/realm/RealmModel;",
            ">(",
            "Ljava/lang/Class",
            "<TE;>;",
            "Ljava/io/InputStream;",
            ")V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 599
    .local p1, "clazz":Ljava/lang/Class;, "Ljava/lang/Class<TE;>;"
    if-eqz p1, :cond_0

    if-nez p2, :cond_1

    .line 621
    :cond_0
    :goto_0
    return-void

    .line 602
    :cond_1
    invoke-virtual {p0}, Lio/realm/Realm;->checkIfValid()V

    .line 603
    invoke-direct {p0, p1}, Lio/realm/Realm;->checkHasPrimaryKey(Ljava/lang/Class;)V

    .line 607
    const/4 v3, 0x0

    .line 609
    .local v3, "scanner":Ljava/util/Scanner;
    :try_start_0
    invoke-direct {p0, p2}, Lio/realm/Realm;->getFullStringScanner(Ljava/io/InputStream;)Ljava/util/Scanner;

    move-result-object v3

    .line 610
    new-instance v2, Lorg/json/JSONArray;

    invoke-virtual {v3}, Ljava/util/Scanner;->next()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v2, v4}, Lorg/json/JSONArray;-><init>(Ljava/lang/String;)V

    .line 611
    .local v2, "json":Lorg/json/JSONArray;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    invoke-virtual {v2}, Lorg/json/JSONArray;->length()I

    move-result v4

    if-ge v1, v4, :cond_2

    .line 612
    iget-object v4, p0, Lio/realm/Realm;->configuration:Lio/realm/RealmConfiguration;

    invoke-virtual {v4}, Lio/realm/RealmConfiguration;->getSchemaMediator()Lio/realm/internal/RealmProxyMediator;

    move-result-object v4

    invoke-virtual {v2, v1}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v5

    const/4 v6, 0x1

    invoke-virtual {v4, p1, p0, v5, v6}, Lio/realm/internal/RealmProxyMediator;->createOrUpdateUsingJsonObject(Ljava/lang/Class;Lio/realm/Realm;Lorg/json/JSONObject;Z)Lio/realm/RealmModel;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 611
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 617
    :cond_2
    if-eqz v3, :cond_0

    .line 618
    invoke-virtual {v3}, Ljava/util/Scanner;->close()V

    goto :goto_0

    .line 614
    .end local v1    # "i":I
    .end local v2    # "json":Lorg/json/JSONArray;
    :catch_0
    move-exception v0

    .line 615
    .local v0, "e":Lorg/json/JSONException;
    :try_start_1
    new-instance v4, Lio/realm/exceptions/RealmException;

    const-string v5, "Failed to read JSON"

    invoke-direct {v4, v5, v0}, Lio/realm/exceptions/RealmException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 617
    .end local v0    # "e":Lorg/json/JSONException;
    :catchall_0
    move-exception v4

    if-eqz v3, :cond_3

    .line 618
    invoke-virtual {v3}, Ljava/util/Scanner;->close()V

    :cond_3
    throw v4
.end method

.method public createOrUpdateAllFromJson(Ljava/lang/Class;Ljava/lang/String;)V
    .locals 4
    .param p2, "json"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E::",
            "Lio/realm/RealmModel;",
            ">(",
            "Ljava/lang/Class",
            "<TE;>;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 531
    .local p1, "clazz":Ljava/lang/Class;, "Ljava/lang/Class<TE;>;"
    if-eqz p1, :cond_0

    if-eqz p2, :cond_0

    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v2

    if-nez v2, :cond_1

    .line 545
    :cond_0
    :goto_0
    return-void

    .line 534
    :cond_1
    invoke-virtual {p0}, Lio/realm/Realm;->checkIfValid()V

    .line 535
    invoke-direct {p0, p1}, Lio/realm/Realm;->checkHasPrimaryKey(Ljava/lang/Class;)V

    .line 539
    :try_start_0
    new-instance v0, Lorg/json/JSONArray;

    invoke-direct {v0, p2}, Lorg/json/JSONArray;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 544
    .local v0, "arr":Lorg/json/JSONArray;
    invoke-virtual {p0, p1, v0}, Lio/realm/Realm;->createOrUpdateAllFromJson(Ljava/lang/Class;Lorg/json/JSONArray;)V

    goto :goto_0

    .line 540
    .end local v0    # "arr":Lorg/json/JSONArray;
    :catch_0
    move-exception v1

    .line 541
    .local v1, "e":Lorg/json/JSONException;
    new-instance v2, Lio/realm/exceptions/RealmException;

    const-string v3, "Could not create JSON array from string"

    invoke-direct {v2, v3, v1}, Lio/realm/exceptions/RealmException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v2
.end method

.method public createOrUpdateAllFromJson(Ljava/lang/Class;Lorg/json/JSONArray;)V
    .locals 5
    .param p2, "json"    # Lorg/json/JSONArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E::",
            "Lio/realm/RealmModel;",
            ">(",
            "Ljava/lang/Class",
            "<TE;>;",
            "Lorg/json/JSONArray;",
            ")V"
        }
    .end annotation

    .prologue
    .line 475
    .local p1, "clazz":Ljava/lang/Class;, "Ljava/lang/Class<TE;>;"
    if-eqz p1, :cond_0

    if-nez p2, :cond_1

    .line 487
    :cond_0
    return-void

    .line 478
    :cond_1
    invoke-virtual {p0}, Lio/realm/Realm;->checkIfValid()V

    .line 479
    invoke-direct {p0, p1}, Lio/realm/Realm;->checkHasPrimaryKey(Ljava/lang/Class;)V

    .line 480
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    invoke-virtual {p2}, Lorg/json/JSONArray;->length()I

    move-result v2

    if-ge v1, v2, :cond_0

    .line 482
    :try_start_0
    iget-object v2, p0, Lio/realm/Realm;->configuration:Lio/realm/RealmConfiguration;

    invoke-virtual {v2}, Lio/realm/RealmConfiguration;->getSchemaMediator()Lio/realm/internal/RealmProxyMediator;

    move-result-object v2

    invoke-virtual {p2, v1}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v3

    const/4 v4, 0x1

    invoke-virtual {v2, p1, p0, v3, v4}, Lio/realm/internal/RealmProxyMediator;->createOrUpdateUsingJsonObject(Ljava/lang/Class;Lio/realm/Realm;Lorg/json/JSONObject;Z)Lio/realm/RealmModel;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 480
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 483
    :catch_0
    move-exception v0

    .line 484
    .local v0, "e":Lorg/json/JSONException;
    new-instance v2, Lio/realm/exceptions/RealmException;

    const-string v3, "Could not map JSON"

    invoke-direct {v2, v3, v0}, Lio/realm/exceptions/RealmException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v2
.end method

.method public createOrUpdateObjectFromJson(Ljava/lang/Class;Ljava/io/InputStream;)Lio/realm/RealmModel;
    .locals 5
    .param p2, "in"    # Ljava/io/InputStream;
    .annotation build Landroid/annotation/TargetApi;
        value = 0xb
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E::",
            "Lio/realm/RealmModel;",
            ">(",
            "Ljava/lang/Class",
            "<TE;>;",
            "Ljava/io/InputStream;",
            ")TE;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 807
    .local p1, "clazz":Ljava/lang/Class;, "Ljava/lang/Class<TE;>;"
    if-eqz p1, :cond_0

    if-nez p2, :cond_2

    .line 808
    :cond_0
    const/4 v3, 0x0

    .line 819
    :cond_1
    :goto_0
    return-object v3

    .line 810
    :cond_2
    invoke-virtual {p0}, Lio/realm/Realm;->checkIfValid()V

    .line 811
    invoke-direct {p0, p1}, Lio/realm/Realm;->checkHasPrimaryKey(Ljava/lang/Class;)V

    .line 815
    const/4 v2, 0x0

    .line 817
    .local v2, "scanner":Ljava/util/Scanner;
    :try_start_0
    invoke-direct {p0, p2}, Lio/realm/Realm;->getFullStringScanner(Ljava/io/InputStream;)Ljava/util/Scanner;

    move-result-object v2

    .line 818
    new-instance v1, Lorg/json/JSONObject;

    invoke-virtual {v2}, Ljava/util/Scanner;->next()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v3}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 819
    .local v1, "json":Lorg/json/JSONObject;
    invoke-virtual {p0, p1, v1}, Lio/realm/Realm;->createOrUpdateObjectFromJson(Ljava/lang/Class;Lorg/json/JSONObject;)Lio/realm/RealmModel;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v3

    .line 823
    if-eqz v2, :cond_1

    .line 824
    invoke-virtual {v2}, Ljava/util/Scanner;->close()V

    goto :goto_0

    .line 820
    .end local v1    # "json":Lorg/json/JSONObject;
    :catch_0
    move-exception v0

    .line 821
    .local v0, "e":Lorg/json/JSONException;
    :try_start_1
    new-instance v3, Lio/realm/exceptions/RealmException;

    const-string v4, "Failed to read JSON"

    invoke-direct {v3, v4, v0}, Lio/realm/exceptions/RealmException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 823
    .end local v0    # "e":Lorg/json/JSONException;
    :catchall_0
    move-exception v3

    if-eqz v2, :cond_3

    .line 824
    invoke-virtual {v2}, Ljava/util/Scanner;->close()V

    :cond_3
    throw v3
.end method

.method public createOrUpdateObjectFromJson(Ljava/lang/Class;Ljava/lang/String;)Lio/realm/RealmModel;
    .locals 4
    .param p2, "json"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E::",
            "Lio/realm/RealmModel;",
            ">(",
            "Ljava/lang/Class",
            "<TE;>;",
            "Ljava/lang/String;",
            ")TE;"
        }
    .end annotation

    .prologue
    .line 721
    .local p1, "clazz":Ljava/lang/Class;, "Ljava/lang/Class<TE;>;"
    if-eqz p1, :cond_0

    if-eqz p2, :cond_0

    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v2

    if-nez v2, :cond_1

    .line 722
    :cond_0
    const/4 v2, 0x0

    .line 734
    :goto_0
    return-object v2

    .line 724
    :cond_1
    invoke-virtual {p0}, Lio/realm/Realm;->checkIfValid()V

    .line 725
    invoke-direct {p0, p1}, Lio/realm/Realm;->checkHasPrimaryKey(Ljava/lang/Class;)V

    .line 729
    :try_start_0
    new-instance v1, Lorg/json/JSONObject;

    invoke-direct {v1, p2}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 734
    .local v1, "obj":Lorg/json/JSONObject;
    invoke-virtual {p0, p1, v1}, Lio/realm/Realm;->createOrUpdateObjectFromJson(Ljava/lang/Class;Lorg/json/JSONObject;)Lio/realm/RealmModel;

    move-result-object v2

    goto :goto_0

    .line 730
    .end local v1    # "obj":Lorg/json/JSONObject;
    :catch_0
    move-exception v0

    .line 731
    .local v0, "e":Lorg/json/JSONException;
    new-instance v2, Lio/realm/exceptions/RealmException;

    const-string v3, "Could not create Json object from string"

    invoke-direct {v2, v3, v0}, Lio/realm/exceptions/RealmException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v2
.end method

.method public createOrUpdateObjectFromJson(Ljava/lang/Class;Lorg/json/JSONObject;)Lio/realm/RealmModel;
    .locals 3
    .param p2, "json"    # Lorg/json/JSONObject;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E::",
            "Lio/realm/RealmModel;",
            ">(",
            "Ljava/lang/Class",
            "<TE;>;",
            "Lorg/json/JSONObject;",
            ")TE;"
        }
    .end annotation

    .prologue
    .line 665
    .local p1, "clazz":Ljava/lang/Class;, "Ljava/lang/Class<TE;>;"
    if-eqz p1, :cond_0

    if-nez p2, :cond_1

    .line 666
    :cond_0
    const/4 v1, 0x0

    .line 671
    :goto_0
    return-object v1

    .line 668
    :cond_1
    invoke-virtual {p0}, Lio/realm/Realm;->checkIfValid()V

    .line 669
    invoke-direct {p0, p1}, Lio/realm/Realm;->checkHasPrimaryKey(Ljava/lang/Class;)V

    .line 671
    :try_start_0
    iget-object v1, p0, Lio/realm/Realm;->configuration:Lio/realm/RealmConfiguration;

    invoke-virtual {v1}, Lio/realm/RealmConfiguration;->getSchemaMediator()Lio/realm/internal/RealmProxyMediator;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, p1, p0, p2, v2}, Lio/realm/internal/RealmProxyMediator;->createOrUpdateUsingJsonObject(Ljava/lang/Class;Lio/realm/Realm;Lorg/json/JSONObject;Z)Lio/realm/RealmModel;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    goto :goto_0

    .line 672
    :catch_0
    move-exception v0

    .line 673
    .local v0, "e":Lorg/json/JSONException;
    new-instance v1, Lio/realm/exceptions/RealmException;

    const-string v2, "Could not map JSON"

    invoke-direct {v1, v2, v0}, Lio/realm/exceptions/RealmException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method

.method public delete(Ljava/lang/Class;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<+",
            "Lio/realm/RealmModel;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1507
    .local p1, "clazz":Ljava/lang/Class;, "Ljava/lang/Class<+Lio/realm/RealmModel;>;"
    invoke-virtual {p0}, Lio/realm/Realm;->checkIfValid()V

    .line 1508
    iget-object v0, p0, Lio/realm/Realm;->schema:Lio/realm/RealmSchema;

    invoke-virtual {v0, p1}, Lio/realm/RealmSchema;->getTable(Ljava/lang/Class;)Lio/realm/internal/Table;

    move-result-object v0

    invoke-virtual {v0}, Lio/realm/internal/Table;->clear()V

    .line 1509
    return-void
.end method

.method public bridge synthetic deleteAll()V
    .locals 0

    .prologue
    .line 126
    invoke-super {p0}, Lio/realm/BaseRealm;->deleteAll()V

    return-void
.end method

.method public executeTransaction(Lio/realm/Realm$Transaction;)V
    .locals 3
    .param p1, "transaction"    # Lio/realm/Realm$Transaction;

    .prologue
    .line 1319
    if-nez p1, :cond_0

    .line 1320
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "Transaction should not be null"

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 1323
    :cond_0
    invoke-virtual {p0}, Lio/realm/Realm;->beginTransaction()V

    .line 1325
    :try_start_0
    invoke-interface {p1, p0}, Lio/realm/Realm$Transaction;->execute(Lio/realm/Realm;)V

    .line 1326
    invoke-virtual {p0}, Lio/realm/Realm;->commitTransaction()V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 1335
    return-void

    .line 1327
    :catch_0
    move-exception v0

    .line 1328
    .local v0, "e":Ljava/lang/Throwable;
    invoke-virtual {p0}, Lio/realm/Realm;->isInTransaction()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1329
    invoke-virtual {p0}, Lio/realm/Realm;->cancelTransaction()V

    .line 1333
    :goto_0
    throw v0

    .line 1331
    :cond_1
    const-string v1, "Could not cancel transaction, not currently in a transaction."

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v1, v2}, Lio/realm/log/RealmLog;->warn(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public executeTransactionAsync(Lio/realm/Realm$Transaction;)Lio/realm/RealmAsyncTask;
    .locals 1
    .param p1, "transaction"    # Lio/realm/Realm$Transaction;

    .prologue
    const/4 v0, 0x0

    .line 1346
    invoke-virtual {p0, p1, v0, v0}, Lio/realm/Realm;->executeTransactionAsync(Lio/realm/Realm$Transaction;Lio/realm/Realm$Transaction$OnSuccess;Lio/realm/Realm$Transaction$OnError;)Lio/realm/RealmAsyncTask;

    move-result-object v0

    return-object v0
.end method

.method public executeTransactionAsync(Lio/realm/Realm$Transaction;Lio/realm/Realm$Transaction$OnError;)Lio/realm/RealmAsyncTask;
    .locals 2
    .param p1, "transaction"    # Lio/realm/Realm$Transaction;
    .param p2, "onError"    # Lio/realm/Realm$Transaction$OnError;

    .prologue
    .line 1376
    if-nez p2, :cond_0

    .line 1377
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "onError callback can\'t be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1380
    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0, p2}, Lio/realm/Realm;->executeTransactionAsync(Lio/realm/Realm$Transaction;Lio/realm/Realm$Transaction$OnSuccess;Lio/realm/Realm$Transaction$OnError;)Lio/realm/RealmAsyncTask;

    move-result-object v0

    return-object v0
.end method

.method public executeTransactionAsync(Lio/realm/Realm$Transaction;Lio/realm/Realm$Transaction$OnSuccess;)Lio/realm/RealmAsyncTask;
    .locals 2
    .param p1, "transaction"    # Lio/realm/Realm$Transaction;
    .param p2, "onSuccess"    # Lio/realm/Realm$Transaction$OnSuccess;

    .prologue
    .line 1359
    if-nez p2, :cond_0

    .line 1360
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "onSuccess callback can\'t be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1363
    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, v0}, Lio/realm/Realm;->executeTransactionAsync(Lio/realm/Realm$Transaction;Lio/realm/Realm$Transaction$OnSuccess;Lio/realm/Realm$Transaction$OnError;)Lio/realm/RealmAsyncTask;

    move-result-object v0

    return-object v0
.end method

.method public executeTransactionAsync(Lio/realm/Realm$Transaction;Lio/realm/Realm$Transaction$OnSuccess;Lio/realm/Realm$Transaction$OnError;)Lio/realm/RealmAsyncTask;
    .locals 10
    .param p1, "transaction"    # Lio/realm/Realm$Transaction;
    .param p2, "onSuccess"    # Lio/realm/Realm$Transaction$OnSuccess;
    .param p3, "onError"    # Lio/realm/Realm$Transaction$OnError;

    .prologue
    .line 1396
    invoke-virtual {p0}, Lio/realm/Realm;->checkIfValid()V

    .line 1398
    if-nez p1, :cond_0

    .line 1399
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Transaction should not be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1403
    :cond_0
    iget-object v0, p0, Lio/realm/Realm;->sharedRealm:Lio/realm/internal/SharedRealm;

    iget-object v0, v0, Lio/realm/internal/SharedRealm;->capabilities:Lio/realm/internal/Capabilities;

    invoke-interface {v0}, Lio/realm/internal/Capabilities;->canDeliverNotification()Z

    move-result v4

    .line 1407
    .local v4, "canDeliverNotification":Z
    if-nez p2, :cond_1

    if-eqz p3, :cond_2

    .line 1408
    :cond_1
    iget-object v0, p0, Lio/realm/Realm;->sharedRealm:Lio/realm/internal/SharedRealm;

    iget-object v0, v0, Lio/realm/internal/SharedRealm;->capabilities:Lio/realm/internal/Capabilities;

    const-string v1, "Callback cannot be delivered on current thread."

    invoke-interface {v0, v1}, Lio/realm/internal/Capabilities;->checkCanDeliverNotification(Ljava/lang/String;)V

    .line 1413
    :cond_2
    invoke-virtual {p0}, Lio/realm/Realm;->getConfiguration()Lio/realm/RealmConfiguration;

    move-result-object v2

    .line 1415
    .local v2, "realmConfiguration":Lio/realm/RealmConfiguration;
    iget-object v0, p0, Lio/realm/Realm;->sharedRealm:Lio/realm/internal/SharedRealm;

    iget-object v6, v0, Lio/realm/internal/SharedRealm;->realmNotifier:Lio/realm/internal/RealmNotifier;

    .line 1417
    .local v6, "realmNotifier":Lio/realm/internal/RealmNotifier;
    sget-object v9, Lio/realm/Realm;->asyncTaskExecutor:Lio/realm/internal/async/RealmThreadPoolExecutor;

    new-instance v0, Lio/realm/Realm$1;

    move-object v1, p0

    move-object v3, p1

    move-object v5, p2

    move-object v7, p3

    invoke-direct/range {v0 .. v7}, Lio/realm/Realm$1;-><init>(Lio/realm/Realm;Lio/realm/RealmConfiguration;Lio/realm/Realm$Transaction;ZLio/realm/Realm$Transaction$OnSuccess;Lio/realm/internal/RealmNotifier;Lio/realm/Realm$Transaction$OnError;)V

    invoke-virtual {v9, v0}, Lio/realm/internal/async/RealmThreadPoolExecutor;->submitTransaction(Ljava/lang/Runnable;)Ljava/util/concurrent/Future;

    move-result-object v8

    .line 1497
    .local v8, "pendingTransaction":Ljava/util/concurrent/Future;, "Ljava/util/concurrent/Future<*>;"
    new-instance v0, Lio/realm/internal/async/RealmAsyncTaskImpl;

    sget-object v1, Lio/realm/Realm;->asyncTaskExecutor:Lio/realm/internal/async/RealmThreadPoolExecutor;

    invoke-direct {v0, v8, v1}, Lio/realm/internal/async/RealmAsyncTaskImpl;-><init>(Ljava/util/concurrent/Future;Ljava/util/concurrent/ThreadPoolExecutor;)V

    return-object v0
.end method

.method public bridge synthetic getConfiguration()Lio/realm/RealmConfiguration;
    .locals 1

    .prologue
    .line 126
    invoke-super {p0}, Lio/realm/BaseRealm;->getConfiguration()Lio/realm/RealmConfiguration;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getPath()Ljava/lang/String;
    .locals 1

    .prologue
    .line 126
    invoke-super {p0}, Lio/realm/BaseRealm;->getPath()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getSchema()Lio/realm/RealmSchema;
    .locals 1

    .prologue
    .line 126
    invoke-super {p0}, Lio/realm/BaseRealm;->getSchema()Lio/realm/RealmSchema;

    move-result-object v0

    return-object v0
.end method

.method getTable(Ljava/lang/Class;)Lio/realm/internal/Table;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<+",
            "Lio/realm/RealmModel;",
            ">;)",
            "Lio/realm/internal/Table;"
        }
    .end annotation

    .prologue
    .line 1631
    .local p1, "clazz":Ljava/lang/Class;, "Ljava/lang/Class<+Lio/realm/RealmModel;>;"
    iget-object v0, p0, Lio/realm/Realm;->schema:Lio/realm/RealmSchema;

    invoke-virtual {v0, p1}, Lio/realm/RealmSchema;->getTable(Ljava/lang/Class;)Lio/realm/internal/Table;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getVersion()J
    .locals 2

    .prologue
    .line 126
    invoke-super {p0}, Lio/realm/BaseRealm;->getVersion()J

    move-result-wide v0

    return-wide v0
.end method

.method public insert(Lio/realm/RealmModel;)V
    .locals 3
    .param p1, "object"    # Lio/realm/RealmModel;

    .prologue
    .line 1043
    invoke-virtual {p0}, Lio/realm/Realm;->checkIfValidAndInTransaction()V

    .line 1044
    if-nez p1, :cond_0

    .line 1045
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "Null object cannot be inserted into Realm."

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 1047
    :cond_0
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 1048
    .local v0, "cache":Ljava/util/Map;, "Ljava/util/Map<Lio/realm/RealmModel;Ljava/lang/Long;>;"
    iget-object v1, p0, Lio/realm/Realm;->configuration:Lio/realm/RealmConfiguration;

    invoke-virtual {v1}, Lio/realm/RealmConfiguration;->getSchemaMediator()Lio/realm/internal/RealmProxyMediator;

    move-result-object v1

    invoke-virtual {v1, p0, p1, v0}, Lio/realm/internal/RealmProxyMediator;->insert(Lio/realm/Realm;Lio/realm/RealmModel;Ljava/util/Map;)V

    .line 1049
    return-void
.end method

.method public insert(Ljava/util/Collection;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<+",
            "Lio/realm/RealmModel;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1007
    .local p1, "objects":Ljava/util/Collection;, "Ljava/util/Collection<+Lio/realm/RealmModel;>;"
    invoke-virtual {p0}, Lio/realm/Realm;->checkIfValidAndInTransaction()V

    .line 1008
    if-nez p1, :cond_0

    .line 1009
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Null objects cannot be inserted into Realm."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1011
    :cond_0
    invoke-interface {p1}, Ljava/util/Collection;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1015
    :goto_0
    return-void

    .line 1014
    :cond_1
    iget-object v0, p0, Lio/realm/Realm;->configuration:Lio/realm/RealmConfiguration;

    invoke-virtual {v0}, Lio/realm/RealmConfiguration;->getSchemaMediator()Lio/realm/internal/RealmProxyMediator;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lio/realm/internal/RealmProxyMediator;->insert(Lio/realm/Realm;Ljava/util/Collection;)V

    goto :goto_0
.end method

.method public insertOrUpdate(Lio/realm/RealmModel;)V
    .locals 3
    .param p1, "object"    # Lio/realm/RealmModel;

    .prologue
    .line 1113
    invoke-virtual {p0}, Lio/realm/Realm;->checkIfValidAndInTransaction()V

    .line 1114
    if-nez p1, :cond_0

    .line 1115
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "Null object cannot be inserted into Realm."

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 1117
    :cond_0
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 1118
    .local v0, "cache":Ljava/util/Map;, "Ljava/util/Map<Lio/realm/RealmModel;Ljava/lang/Long;>;"
    iget-object v1, p0, Lio/realm/Realm;->configuration:Lio/realm/RealmConfiguration;

    invoke-virtual {v1}, Lio/realm/RealmConfiguration;->getSchemaMediator()Lio/realm/internal/RealmProxyMediator;

    move-result-object v1

    invoke-virtual {v1, p0, p1, v0}, Lio/realm/internal/RealmProxyMediator;->insertOrUpdate(Lio/realm/Realm;Lio/realm/RealmModel;Ljava/util/Map;)V

    .line 1119
    return-void
.end method

.method public insertOrUpdate(Ljava/util/Collection;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<+",
            "Lio/realm/RealmModel;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1078
    .local p1, "objects":Ljava/util/Collection;, "Ljava/util/Collection<+Lio/realm/RealmModel;>;"
    invoke-virtual {p0}, Lio/realm/Realm;->checkIfValidAndInTransaction()V

    .line 1079
    if-nez p1, :cond_0

    .line 1080
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Null objects cannot be inserted into Realm."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1082
    :cond_0
    invoke-interface {p1}, Ljava/util/Collection;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1086
    :goto_0
    return-void

    .line 1085
    :cond_1
    iget-object v0, p0, Lio/realm/Realm;->configuration:Lio/realm/RealmConfiguration;

    invoke-virtual {v0}, Lio/realm/RealmConfiguration;->getSchemaMediator()Lio/realm/internal/RealmProxyMediator;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lio/realm/internal/RealmProxyMediator;->insertOrUpdate(Lio/realm/Realm;Ljava/util/Collection;)V

    goto :goto_0
.end method

.method public bridge synthetic isAutoRefresh()Z
    .locals 1

    .prologue
    .line 126
    invoke-super {p0}, Lio/realm/BaseRealm;->isAutoRefresh()Z

    move-result v0

    return v0
.end method

.method public bridge synthetic isClosed()Z
    .locals 1

    .prologue
    .line 126
    invoke-super {p0}, Lio/realm/BaseRealm;->isClosed()Z

    move-result v0

    return v0
.end method

.method public bridge synthetic isEmpty()Z
    .locals 1

    .prologue
    .line 126
    invoke-super {p0}, Lio/realm/BaseRealm;->isEmpty()Z

    move-result v0

    return v0
.end method

.method public bridge synthetic isInTransaction()Z
    .locals 1

    .prologue
    .line 126
    invoke-super {p0}, Lio/realm/BaseRealm;->isInTransaction()Z

    move-result v0

    return v0
.end method

.method public removeAllChangeListeners()V
    .locals 0

    .prologue
    .line 1306
    invoke-virtual {p0}, Lio/realm/Realm;->removeAllListeners()V

    .line 1307
    return-void
.end method

.method public removeChangeListener(Lio/realm/RealmChangeListener;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/realm/RealmChangeListener",
            "<",
            "Lio/realm/Realm;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1296
    .local p1, "listener":Lio/realm/RealmChangeListener;, "Lio/realm/RealmChangeListener<Lio/realm/Realm;>;"
    invoke-virtual {p0, p1}, Lio/realm/Realm;->removeListener(Lio/realm/RealmChangeListener;)V

    .line 1297
    return-void
.end method

.method public bridge synthetic setAutoRefresh(Z)V
    .locals 0

    .prologue
    .line 126
    invoke-super {p0, p1}, Lio/realm/BaseRealm;->setAutoRefresh(Z)V

    return-void
.end method

.method public bridge synthetic stopWaitForChange()V
    .locals 0

    .prologue
    .line 126
    invoke-super {p0}, Lio/realm/BaseRealm;->stopWaitForChange()V

    return-void
.end method

.method updateSchemaCache([Lio/realm/internal/ColumnIndices;)Lio/realm/internal/ColumnIndices;
    .locals 17
    .param p1, "globalCacheArray"    # [Lio/realm/internal/ColumnIndices;

    .prologue
    .line 1643
    move-object/from16 v0, p0

    iget-object v14, v0, Lio/realm/Realm;->sharedRealm:Lio/realm/internal/SharedRealm;

    invoke-virtual {v14}, Lio/realm/internal/SharedRealm;->getSchemaVersion()J

    move-result-wide v8

    .line 1644
    .local v8, "currentSchemaVersion":J
    move-object/from16 v0, p0

    iget-object v14, v0, Lio/realm/Realm;->schema:Lio/realm/RealmSchema;

    iget-object v14, v14, Lio/realm/RealmSchema;->columnIndices:Lio/realm/internal/ColumnIndices;

    invoke-virtual {v14}, Lio/realm/internal/ColumnIndices;->getSchemaVersion()J

    move-result-wide v4

    .line 1645
    .local v4, "cacheSchemaVersion":J
    cmp-long v14, v8, v4

    if-nez v14, :cond_0

    .line 1646
    const/4 v7, 0x0

    .line 1670
    :goto_0
    return-object v7

    .line 1649
    :cond_0
    const/4 v7, 0x0

    .line 1650
    .local v7, "createdGlobalCache":Lio/realm/internal/ColumnIndices;
    invoke-virtual/range {p0 .. p0}, Lio/realm/Realm;->getConfiguration()Lio/realm/RealmConfiguration;

    move-result-object v14

    invoke-virtual {v14}, Lio/realm/RealmConfiguration;->getSchemaMediator()Lio/realm/internal/RealmProxyMediator;

    move-result-object v12

    .line 1651
    .local v12, "mediator":Lio/realm/internal/RealmProxyMediator;
    move-object/from16 v0, p1

    invoke-static {v0, v8, v9}, Lio/realm/RealmCache;->findColumnIndices([Lio/realm/internal/ColumnIndices;J)Lio/realm/internal/ColumnIndices;

    move-result-object v2

    .line 1653
    .local v2, "cacheForCurrentVersion":Lio/realm/internal/ColumnIndices;
    if-nez v2, :cond_2

    .line 1655
    invoke-virtual {v12}, Lio/realm/internal/RealmProxyMediator;->getModelClasses()Ljava/util/Set;

    move-result-object v13

    .line 1657
    .local v13, "modelClasses":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Class<+Lio/realm/RealmModel;>;>;"
    new-instance v11, Ljava/util/HashMap;

    invoke-interface {v13}, Ljava/util/Set;->size()I

    move-result v14

    invoke-direct {v11, v14}, Ljava/util/HashMap;-><init>(I)V

    .line 1659
    .local v11, "map":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/Class<+Lio/realm/RealmModel;>;Lio/realm/internal/ColumnInfo;>;"
    :try_start_0
    invoke-interface {v13}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v14

    :goto_1
    invoke-interface {v14}, Ljava/util/Iterator;->hasNext()Z

    move-result v15

    if-eqz v15, :cond_1

    invoke-interface {v14}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Class;

    .line 1660
    .local v3, "clazz":Ljava/lang/Class;, "Ljava/lang/Class<+Lio/realm/RealmModel;>;"
    move-object/from16 v0, p0

    iget-object v15, v0, Lio/realm/Realm;->sharedRealm:Lio/realm/internal/SharedRealm;

    const/16 v16, 0x1

    move/from16 v0, v16

    invoke-virtual {v12, v3, v15, v0}, Lio/realm/internal/RealmProxyMediator;->validateTable(Ljava/lang/Class;Lio/realm/internal/SharedRealm;Z)Lio/realm/internal/ColumnInfo;

    move-result-object v6

    .line 1661
    .local v6, "columnInfo":Lio/realm/internal/ColumnInfo;
    invoke-interface {v11, v3, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Lio/realm/exceptions/RealmMigrationNeededException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 1663
    .end local v3    # "clazz":Ljava/lang/Class;, "Ljava/lang/Class<+Lio/realm/RealmModel;>;"
    .end local v6    # "columnInfo":Lio/realm/internal/ColumnInfo;
    :catch_0
    move-exception v10

    .line 1664
    .local v10, "e":Lio/realm/exceptions/RealmMigrationNeededException;
    throw v10

    .line 1667
    .end local v10    # "e":Lio/realm/exceptions/RealmMigrationNeededException;
    :cond_1
    new-instance v7, Lio/realm/internal/ColumnIndices;

    .end local v7    # "createdGlobalCache":Lio/realm/internal/ColumnIndices;
    invoke-direct {v7, v8, v9, v11}, Lio/realm/internal/ColumnIndices;-><init>(JLjava/util/Map;)V

    .restart local v7    # "createdGlobalCache":Lio/realm/internal/ColumnIndices;
    move-object v2, v7

    .line 1669
    .end local v11    # "map":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/Class<+Lio/realm/RealmModel;>;Lio/realm/internal/ColumnInfo;>;"
    .end local v13    # "modelClasses":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Class<+Lio/realm/RealmModel;>;>;"
    :cond_2
    move-object/from16 v0, p0

    iget-object v14, v0, Lio/realm/Realm;->schema:Lio/realm/RealmSchema;

    iget-object v14, v14, Lio/realm/RealmSchema;->columnIndices:Lio/realm/internal/ColumnIndices;

    invoke-virtual {v14, v2, v12}, Lio/realm/internal/ColumnIndices;->copyFrom(Lio/realm/internal/ColumnIndices;Lio/realm/internal/RealmProxyMediator;)V

    goto :goto_0
.end method

.method public bridge synthetic waitForChange()Z
    .locals 1

    .prologue
    .line 126
    invoke-super {p0}, Lio/realm/BaseRealm;->waitForChange()Z

    move-result v0

    return v0
.end method

.method public where(Ljava/lang/Class;)Lio/realm/RealmQuery;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E::",
            "Lio/realm/RealmModel;",
            ">(",
            "Ljava/lang/Class",
            "<TE;>;)",
            "Lio/realm/RealmQuery",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 1263
    .local p1, "clazz":Ljava/lang/Class;, "Ljava/lang/Class<TE;>;"
    invoke-virtual {p0}, Lio/realm/Realm;->checkIfValid()V

    .line 1264
    invoke-static {p0, p1}, Lio/realm/RealmQuery;->createQuery(Lio/realm/Realm;Ljava/lang/Class;)Lio/realm/RealmQuery;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic writeCopyTo(Ljava/io/File;)V
    .locals 0

    .prologue
    .line 126
    invoke-super {p0, p1}, Lio/realm/BaseRealm;->writeCopyTo(Ljava/io/File;)V

    return-void
.end method

.method public bridge synthetic writeEncryptedCopyTo(Ljava/io/File;[B)V
    .locals 0

    .prologue
    .line 126
    invoke-super {p0, p1, p2}, Lio/realm/BaseRealm;->writeEncryptedCopyTo(Ljava/io/File;[B)V

    return-void
.end method
