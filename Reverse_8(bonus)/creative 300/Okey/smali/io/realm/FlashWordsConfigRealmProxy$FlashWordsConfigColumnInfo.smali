.class final Lio/realm/FlashWordsConfigRealmProxy$FlashWordsConfigColumnInfo;
.super Lio/realm/internal/ColumnInfo;
.source "FlashWordsConfigRealmProxy.java"

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lio/realm/FlashWordsConfigRealmProxy;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = "FlashWordsConfigColumnInfo"
.end annotation


# instance fields
.field public boarTypeIndex:J

.field public idIndex:J

.field public speedIndex:J

.field public trainingDurationIndex:J


# direct methods
.method constructor <init>(Ljava/lang/String;Lio/realm/internal/Table;)V
    .locals 4
    .param p1, "path"    # Ljava/lang/String;
    .param p2, "table"    # Lio/realm/internal/Table;

    .prologue
    .line 42
    invoke-direct {p0}, Lio/realm/internal/ColumnInfo;-><init>()V

    .line 43
    new-instance v0, Ljava/util/HashMap;

    const/4 v1, 0x4

    invoke-direct {v0, v1}, Ljava/util/HashMap;-><init>(I)V

    .line 44
    .local v0, "indicesMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Long;>;"
    const-string v1, "FlashWordsConfig"

    const-string v2, "id"

    invoke-virtual {p0, p1, p2, v1, v2}, Lio/realm/FlashWordsConfigRealmProxy$FlashWordsConfigColumnInfo;->getValidColumnIndex(Ljava/lang/String;Lio/realm/internal/Table;Ljava/lang/String;Ljava/lang/String;)J

    move-result-wide v2

    iput-wide v2, p0, Lio/realm/FlashWordsConfigRealmProxy$FlashWordsConfigColumnInfo;->idIndex:J

    .line 45
    const-string v1, "id"

    iget-wide v2, p0, Lio/realm/FlashWordsConfigRealmProxy$FlashWordsConfigColumnInfo;->idIndex:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 46
    const-string v1, "FlashWordsConfig"

    const-string v2, "speed"

    invoke-virtual {p0, p1, p2, v1, v2}, Lio/realm/FlashWordsConfigRealmProxy$FlashWordsConfigColumnInfo;->getValidColumnIndex(Ljava/lang/String;Lio/realm/internal/Table;Ljava/lang/String;Ljava/lang/String;)J

    move-result-wide v2

    iput-wide v2, p0, Lio/realm/FlashWordsConfigRealmProxy$FlashWordsConfigColumnInfo;->speedIndex:J

    .line 47
    const-string v1, "speed"

    iget-wide v2, p0, Lio/realm/FlashWordsConfigRealmProxy$FlashWordsConfigColumnInfo;->speedIndex:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 48
    const-string v1, "FlashWordsConfig"

    const-string v2, "boarType"

    invoke-virtual {p0, p1, p2, v1, v2}, Lio/realm/FlashWordsConfigRealmProxy$FlashWordsConfigColumnInfo;->getValidColumnIndex(Ljava/lang/String;Lio/realm/internal/Table;Ljava/lang/String;Ljava/lang/String;)J

    move-result-wide v2

    iput-wide v2, p0, Lio/realm/FlashWordsConfigRealmProxy$FlashWordsConfigColumnInfo;->boarTypeIndex:J

    .line 49
    const-string v1, "boarType"

    iget-wide v2, p0, Lio/realm/FlashWordsConfigRealmProxy$FlashWordsConfigColumnInfo;->boarTypeIndex:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 50
    const-string v1, "FlashWordsConfig"

    const-string v2, "trainingDuration"

    invoke-virtual {p0, p1, p2, v1, v2}, Lio/realm/FlashWordsConfigRealmProxy$FlashWordsConfigColumnInfo;->getValidColumnIndex(Ljava/lang/String;Lio/realm/internal/Table;Ljava/lang/String;Ljava/lang/String;)J

    move-result-wide v2

    iput-wide v2, p0, Lio/realm/FlashWordsConfigRealmProxy$FlashWordsConfigColumnInfo;->trainingDurationIndex:J

    .line 51
    const-string v1, "trainingDuration"

    iget-wide v2, p0, Lio/realm/FlashWordsConfigRealmProxy$FlashWordsConfigColumnInfo;->trainingDurationIndex:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 53
    invoke-virtual {p0, v0}, Lio/realm/FlashWordsConfigRealmProxy$FlashWordsConfigColumnInfo;->setIndicesMap(Ljava/util/Map;)V

    .line 54
    return-void
.end method


# virtual methods
.method public final clone()Lio/realm/FlashWordsConfigRealmProxy$FlashWordsConfigColumnInfo;
    .locals 1

    .prologue
    .line 69
    invoke-super {p0}, Lio/realm/internal/ColumnInfo;->clone()Lio/realm/internal/ColumnInfo;

    move-result-object v0

    check-cast v0, Lio/realm/FlashWordsConfigRealmProxy$FlashWordsConfigColumnInfo;

    return-object v0
.end method

.method public bridge synthetic clone()Lio/realm/internal/ColumnInfo;
    .locals 1

    .prologue
    .line 34
    invoke-virtual {p0}, Lio/realm/FlashWordsConfigRealmProxy$FlashWordsConfigColumnInfo;->clone()Lio/realm/FlashWordsConfigRealmProxy$FlashWordsConfigColumnInfo;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 34
    invoke-virtual {p0}, Lio/realm/FlashWordsConfigRealmProxy$FlashWordsConfigColumnInfo;->clone()Lio/realm/FlashWordsConfigRealmProxy$FlashWordsConfigColumnInfo;

    move-result-object v0

    return-object v0
.end method

.method public final copyColumnInfoFrom(Lio/realm/internal/ColumnInfo;)V
    .locals 4
    .param p1, "other"    # Lio/realm/internal/ColumnInfo;

    .prologue
    .line 58
    move-object v0, p1

    check-cast v0, Lio/realm/FlashWordsConfigRealmProxy$FlashWordsConfigColumnInfo;

    .line 59
    .local v0, "otherInfo":Lio/realm/FlashWordsConfigRealmProxy$FlashWordsConfigColumnInfo;
    iget-wide v2, v0, Lio/realm/FlashWordsConfigRealmProxy$FlashWordsConfigColumnInfo;->idIndex:J

    iput-wide v2, p0, Lio/realm/FlashWordsConfigRealmProxy$FlashWordsConfigColumnInfo;->idIndex:J

    .line 60
    iget-wide v2, v0, Lio/realm/FlashWordsConfigRealmProxy$FlashWordsConfigColumnInfo;->speedIndex:J

    iput-wide v2, p0, Lio/realm/FlashWordsConfigRealmProxy$FlashWordsConfigColumnInfo;->speedIndex:J

    .line 61
    iget-wide v2, v0, Lio/realm/FlashWordsConfigRealmProxy$FlashWordsConfigColumnInfo;->boarTypeIndex:J

    iput-wide v2, p0, Lio/realm/FlashWordsConfigRealmProxy$FlashWordsConfigColumnInfo;->boarTypeIndex:J

    .line 62
    iget-wide v2, v0, Lio/realm/FlashWordsConfigRealmProxy$FlashWordsConfigColumnInfo;->trainingDurationIndex:J

    iput-wide v2, p0, Lio/realm/FlashWordsConfigRealmProxy$FlashWordsConfigColumnInfo;->trainingDurationIndex:J

    .line 64
    invoke-virtual {v0}, Lio/realm/FlashWordsConfigRealmProxy$FlashWordsConfigColumnInfo;->getIndicesMap()Ljava/util/Map;

    move-result-object v1

    invoke-virtual {p0, v1}, Lio/realm/FlashWordsConfigRealmProxy$FlashWordsConfigColumnInfo;->setIndicesMap(Ljava/util/Map;)V

    .line 65
    return-void
.end method
