.class public Lio/realm/WordsColumnsConfigRealmProxy;
.super Lcom/safonov/speedreading/training/fragment/wordscolumns/repository/entity/WordsColumnsConfig;
.source "WordsColumnsConfigRealmProxy.java"

# interfaces
.implements Lio/realm/internal/RealmObjectProxy;
.implements Lio/realm/WordsColumnsConfigRealmProxyInterface;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lio/realm/WordsColumnsConfigRealmProxy$WordsColumnsConfigColumnInfo;
    }
.end annotation


# static fields
.field private static final FIELD_NAMES:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private columnInfo:Lio/realm/WordsColumnsConfigRealmProxy$WordsColumnsConfigColumnInfo;

.field private proxyState:Lio/realm/ProxyState;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/realm/ProxyState",
            "<",
            "Lcom/safonov/speedreading/training/fragment/wordscolumns/repository/entity/WordsColumnsConfig;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 85
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 86
    .local v0, "fieldNames":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const-string v1, "id"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 87
    const-string v1, "rowCount"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 88
    const-string v1, "columnCount"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 89
    const-string v1, "wordsPerItem"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 90
    const-string v1, "speed"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 91
    const-string v1, "trainingDuration"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 92
    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    sput-object v1, Lio/realm/WordsColumnsConfigRealmProxy;->FIELD_NAMES:Ljava/util/List;

    .line 93
    return-void
.end method

.method constructor <init>()V
    .locals 1

    .prologue
    .line 95
    invoke-direct {p0}, Lcom/safonov/speedreading/training/fragment/wordscolumns/repository/entity/WordsColumnsConfig;-><init>()V

    .line 96
    iget-object v0, p0, Lio/realm/WordsColumnsConfigRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v0}, Lio/realm/ProxyState;->setConstructionFinished()V

    .line 97
    return-void
.end method

.method public static copy(Lio/realm/Realm;Lcom/safonov/speedreading/training/fragment/wordscolumns/repository/entity/WordsColumnsConfig;ZLjava/util/Map;)Lcom/safonov/speedreading/training/fragment/wordscolumns/repository/entity/WordsColumnsConfig;
    .locals 6
    .param p0, "realm"    # Lio/realm/Realm;
    .param p1, "newObject"    # Lcom/safonov/speedreading/training/fragment/wordscolumns/repository/entity/WordsColumnsConfig;
    .param p2, "update"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/realm/Realm;",
            "Lcom/safonov/speedreading/training/fragment/wordscolumns/repository/entity/WordsColumnsConfig;",
            "Z",
            "Ljava/util/Map",
            "<",
            "Lio/realm/RealmModel;",
            "Lio/realm/internal/RealmObjectProxy;",
            ">;)",
            "Lcom/safonov/speedreading/training/fragment/wordscolumns/repository/entity/WordsColumnsConfig;"
        }
    .end annotation

    .prologue
    .line 534
    .local p3, "cache":Ljava/util/Map;, "Ljava/util/Map<Lio/realm/RealmModel;Lio/realm/internal/RealmObjectProxy;>;"
    invoke-interface {p3, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/realm/internal/RealmObjectProxy;

    .line 535
    .local v0, "cachedRealmObject":Lio/realm/internal/RealmObjectProxy;
    if-eqz v0, :cond_0

    .line 536
    check-cast v0, Lcom/safonov/speedreading/training/fragment/wordscolumns/repository/entity/WordsColumnsConfig;

    .line 546
    .end local v0    # "cachedRealmObject":Lio/realm/internal/RealmObjectProxy;
    .end local p1    # "newObject":Lcom/safonov/speedreading/training/fragment/wordscolumns/repository/entity/WordsColumnsConfig;
    :goto_0
    return-object v0

    .line 539
    .restart local v0    # "cachedRealmObject":Lio/realm/internal/RealmObjectProxy;
    .restart local p1    # "newObject":Lcom/safonov/speedreading/training/fragment/wordscolumns/repository/entity/WordsColumnsConfig;
    :cond_0
    const-class v3, Lcom/safonov/speedreading/training/fragment/wordscolumns/repository/entity/WordsColumnsConfig;

    move-object v2, p1

    check-cast v2, Lio/realm/WordsColumnsConfigRealmProxyInterface;

    invoke-interface {v2}, Lio/realm/WordsColumnsConfigRealmProxyInterface;->realmGet$id()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v4, 0x0

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v5

    invoke-virtual {p0, v3, v2, v4, v5}, Lio/realm/Realm;->createObjectInternal(Ljava/lang/Class;Ljava/lang/Object;ZLjava/util/List;)Lio/realm/RealmModel;

    move-result-object v1

    check-cast v1, Lcom/safonov/speedreading/training/fragment/wordscolumns/repository/entity/WordsColumnsConfig;

    .local v1, "realmObject":Lcom/safonov/speedreading/training/fragment/wordscolumns/repository/entity/WordsColumnsConfig;
    move-object v2, v1

    .line 540
    check-cast v2, Lio/realm/internal/RealmObjectProxy;

    invoke-interface {p3, p1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-object v2, v1

    .line 541
    check-cast v2, Lio/realm/WordsColumnsConfigRealmProxyInterface;

    move-object v3, p1

    check-cast v3, Lio/realm/WordsColumnsConfigRealmProxyInterface;

    invoke-interface {v3}, Lio/realm/WordsColumnsConfigRealmProxyInterface;->realmGet$rowCount()I

    move-result v3

    invoke-interface {v2, v3}, Lio/realm/WordsColumnsConfigRealmProxyInterface;->realmSet$rowCount(I)V

    move-object v2, v1

    .line 542
    check-cast v2, Lio/realm/WordsColumnsConfigRealmProxyInterface;

    move-object v3, p1

    check-cast v3, Lio/realm/WordsColumnsConfigRealmProxyInterface;

    invoke-interface {v3}, Lio/realm/WordsColumnsConfigRealmProxyInterface;->realmGet$columnCount()I

    move-result v3

    invoke-interface {v2, v3}, Lio/realm/WordsColumnsConfigRealmProxyInterface;->realmSet$columnCount(I)V

    move-object v2, v1

    .line 543
    check-cast v2, Lio/realm/WordsColumnsConfigRealmProxyInterface;

    move-object v3, p1

    check-cast v3, Lio/realm/WordsColumnsConfigRealmProxyInterface;

    invoke-interface {v3}, Lio/realm/WordsColumnsConfigRealmProxyInterface;->realmGet$wordsPerItem()I

    move-result v3

    invoke-interface {v2, v3}, Lio/realm/WordsColumnsConfigRealmProxyInterface;->realmSet$wordsPerItem(I)V

    move-object v2, v1

    .line 544
    check-cast v2, Lio/realm/WordsColumnsConfigRealmProxyInterface;

    move-object v3, p1

    check-cast v3, Lio/realm/WordsColumnsConfigRealmProxyInterface;

    invoke-interface {v3}, Lio/realm/WordsColumnsConfigRealmProxyInterface;->realmGet$speed()I

    move-result v3

    invoke-interface {v2, v3}, Lio/realm/WordsColumnsConfigRealmProxyInterface;->realmSet$speed(I)V

    move-object v2, v1

    .line 545
    check-cast v2, Lio/realm/WordsColumnsConfigRealmProxyInterface;

    check-cast p1, Lio/realm/WordsColumnsConfigRealmProxyInterface;

    .end local p1    # "newObject":Lcom/safonov/speedreading/training/fragment/wordscolumns/repository/entity/WordsColumnsConfig;
    invoke-interface {p1}, Lio/realm/WordsColumnsConfigRealmProxyInterface;->realmGet$trainingDuration()J

    move-result-wide v4

    invoke-interface {v2, v4, v5}, Lio/realm/WordsColumnsConfigRealmProxyInterface;->realmSet$trainingDuration(J)V

    move-object v0, v1

    .line 546
    goto :goto_0
.end method

.method public static copyOrUpdate(Lio/realm/Realm;Lcom/safonov/speedreading/training/fragment/wordscolumns/repository/entity/WordsColumnsConfig;ZLjava/util/Map;)Lcom/safonov/speedreading/training/fragment/wordscolumns/repository/entity/WordsColumnsConfig;
    .locals 19
    .param p0, "realm"    # Lio/realm/Realm;
    .param p1, "object"    # Lcom/safonov/speedreading/training/fragment/wordscolumns/repository/entity/WordsColumnsConfig;
    .param p2, "update"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/realm/Realm;",
            "Lcom/safonov/speedreading/training/fragment/wordscolumns/repository/entity/WordsColumnsConfig;",
            "Z",
            "Ljava/util/Map",
            "<",
            "Lio/realm/RealmModel;",
            "Lio/realm/internal/RealmObjectProxy;",
            ">;)",
            "Lcom/safonov/speedreading/training/fragment/wordscolumns/repository/entity/WordsColumnsConfig;"
        }
    .end annotation

    .prologue
    .line 495
    .local p3, "cache":Ljava/util/Map;, "Ljava/util/Map<Lio/realm/RealmModel;Lio/realm/internal/RealmObjectProxy;>;"
    move-object/from16 v0, p1

    instance-of v5, v0, Lio/realm/internal/RealmObjectProxy;

    if-eqz v5, :cond_0

    move-object/from16 v5, p1

    check-cast v5, Lio/realm/internal/RealmObjectProxy;

    invoke-interface {v5}, Lio/realm/internal/RealmObjectProxy;->realmGet$proxyState()Lio/realm/ProxyState;

    move-result-object v5

    invoke-virtual {v5}, Lio/realm/ProxyState;->getRealm$realm()Lio/realm/BaseRealm;

    move-result-object v5

    if-eqz v5, :cond_0

    move-object/from16 v5, p1

    check-cast v5, Lio/realm/internal/RealmObjectProxy;

    invoke-interface {v5}, Lio/realm/internal/RealmObjectProxy;->realmGet$proxyState()Lio/realm/ProxyState;

    move-result-object v5

    invoke-virtual {v5}, Lio/realm/ProxyState;->getRealm$realm()Lio/realm/BaseRealm;

    move-result-object v5

    iget-wide v6, v5, Lio/realm/BaseRealm;->threadId:J

    move-object/from16 v0, p0

    iget-wide v8, v0, Lio/realm/Realm;->threadId:J

    cmp-long v5, v6, v8

    if-eqz v5, :cond_0

    .line 496
    new-instance v5, Ljava/lang/IllegalArgumentException;

    const-string v6, "Objects which belong to Realm instances in other threads cannot be copied into this Realm instance."

    invoke-direct {v5, v6}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v5

    .line 498
    :cond_0
    move-object/from16 v0, p1

    instance-of v5, v0, Lio/realm/internal/RealmObjectProxy;

    if-eqz v5, :cond_1

    move-object/from16 v5, p1

    check-cast v5, Lio/realm/internal/RealmObjectProxy;

    invoke-interface {v5}, Lio/realm/internal/RealmObjectProxy;->realmGet$proxyState()Lio/realm/ProxyState;

    move-result-object v5

    invoke-virtual {v5}, Lio/realm/ProxyState;->getRealm$realm()Lio/realm/BaseRealm;

    move-result-object v5

    if-eqz v5, :cond_1

    move-object/from16 v5, p1

    check-cast v5, Lio/realm/internal/RealmObjectProxy;

    invoke-interface {v5}, Lio/realm/internal/RealmObjectProxy;->realmGet$proxyState()Lio/realm/ProxyState;

    move-result-object v5

    invoke-virtual {v5}, Lio/realm/ProxyState;->getRealm$realm()Lio/realm/BaseRealm;

    move-result-object v5

    invoke-virtual {v5}, Lio/realm/BaseRealm;->getPath()Ljava/lang/String;

    move-result-object v5

    invoke-virtual/range {p0 .. p0}, Lio/realm/Realm;->getPath()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 528
    .end local p1    # "object":Lcom/safonov/speedreading/training/fragment/wordscolumns/repository/entity/WordsColumnsConfig;
    :goto_0
    return-object p1

    .line 501
    .restart local p1    # "object":Lcom/safonov/speedreading/training/fragment/wordscolumns/repository/entity/WordsColumnsConfig;
    :cond_1
    sget-object v5, Lio/realm/BaseRealm;->objectContext:Lio/realm/BaseRealm$ThreadLocalRealmObjectContext;

    invoke-virtual {v5}, Lio/realm/BaseRealm$ThreadLocalRealmObjectContext;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lio/realm/BaseRealm$RealmObjectContext;

    .line 502
    .local v4, "objectContext":Lio/realm/BaseRealm$RealmObjectContext;
    move-object/from16 v0, p3

    move-object/from16 v1, p1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lio/realm/internal/RealmObjectProxy;

    .line 503
    .local v10, "cachedRealmObject":Lio/realm/internal/RealmObjectProxy;
    if-eqz v10, :cond_2

    .line 504
    check-cast v10, Lcom/safonov/speedreading/training/fragment/wordscolumns/repository/entity/WordsColumnsConfig;

    .end local v10    # "cachedRealmObject":Lio/realm/internal/RealmObjectProxy;
    move-object/from16 p1, v10

    goto :goto_0

    .line 506
    .restart local v10    # "cachedRealmObject":Lio/realm/internal/RealmObjectProxy;
    :cond_2
    const/4 v14, 0x0

    .line 507
    .local v14, "realmObject":Lcom/safonov/speedreading/training/fragment/wordscolumns/repository/entity/WordsColumnsConfig;
    move/from16 v11, p2

    .line 508
    .local v11, "canUpdate":Z
    if-eqz v11, :cond_3

    .line 509
    const-class v5, Lcom/safonov/speedreading/training/fragment/wordscolumns/repository/entity/WordsColumnsConfig;

    move-object/from16 v0, p0

    invoke-virtual {v0, v5}, Lio/realm/Realm;->getTable(Ljava/lang/Class;)Lio/realm/internal/Table;

    move-result-object v18

    .line 510
    .local v18, "table":Lio/realm/internal/Table;
    invoke-virtual/range {v18 .. v18}, Lio/realm/internal/Table;->getPrimaryKey()J

    move-result-wide v12

    .local v12, "pkColumnIndex":J
    move-object/from16 v5, p1

    .line 511
    check-cast v5, Lio/realm/WordsColumnsConfigRealmProxyInterface;

    invoke-interface {v5}, Lio/realm/WordsColumnsConfigRealmProxyInterface;->realmGet$id()I

    move-result v5

    int-to-long v6, v5

    move-object/from16 v0, v18

    invoke-virtual {v0, v12, v13, v6, v7}, Lio/realm/internal/Table;->findFirstLong(JJ)J

    move-result-wide v16

    .line 512
    .local v16, "rowIndex":J
    const-wide/16 v6, -0x1

    cmp-long v5, v16, v6

    if-eqz v5, :cond_4

    .line 514
    :try_start_0
    move-object/from16 v0, v18

    move-wide/from16 v1, v16

    invoke-virtual {v0, v1, v2}, Lio/realm/internal/Table;->getUncheckedRow(J)Lio/realm/internal/UncheckedRow;

    move-result-object v6

    move-object/from16 v0, p0

    iget-object v5, v0, Lio/realm/Realm;->schema:Lio/realm/RealmSchema;

    const-class v7, Lcom/safonov/speedreading/training/fragment/wordscolumns/repository/entity/WordsColumnsConfig;

    invoke-virtual {v5, v7}, Lio/realm/RealmSchema;->getColumnInfo(Ljava/lang/Class;)Lio/realm/internal/ColumnInfo;

    move-result-object v7

    const/4 v8, 0x0

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v9

    move-object/from16 v5, p0

    invoke-virtual/range {v4 .. v9}, Lio/realm/BaseRealm$RealmObjectContext;->set(Lio/realm/BaseRealm;Lio/realm/internal/Row;Lio/realm/internal/ColumnInfo;ZLjava/util/List;)V

    .line 515
    new-instance v15, Lio/realm/WordsColumnsConfigRealmProxy;

    invoke-direct {v15}, Lio/realm/WordsColumnsConfigRealmProxy;-><init>()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 516
    .end local v14    # "realmObject":Lcom/safonov/speedreading/training/fragment/wordscolumns/repository/entity/WordsColumnsConfig;
    .local v15, "realmObject":Lcom/safonov/speedreading/training/fragment/wordscolumns/repository/entity/WordsColumnsConfig;
    :try_start_1
    move-object v0, v15

    check-cast v0, Lio/realm/internal/RealmObjectProxy;

    move-object v5, v0

    move-object/from16 v0, p3

    move-object/from16 v1, p1

    invoke-interface {v0, v1, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 518
    invoke-virtual {v4}, Lio/realm/BaseRealm$RealmObjectContext;->clear()V

    move-object v14, v15

    .line 525
    .end local v12    # "pkColumnIndex":J
    .end local v15    # "realmObject":Lcom/safonov/speedreading/training/fragment/wordscolumns/repository/entity/WordsColumnsConfig;
    .end local v16    # "rowIndex":J
    .end local v18    # "table":Lio/realm/internal/Table;
    .restart local v14    # "realmObject":Lcom/safonov/speedreading/training/fragment/wordscolumns/repository/entity/WordsColumnsConfig;
    :cond_3
    :goto_1
    if-eqz v11, :cond_5

    .line 526
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p3

    invoke-static {v0, v14, v1, v2}, Lio/realm/WordsColumnsConfigRealmProxy;->update(Lio/realm/Realm;Lcom/safonov/speedreading/training/fragment/wordscolumns/repository/entity/WordsColumnsConfig;Lcom/safonov/speedreading/training/fragment/wordscolumns/repository/entity/WordsColumnsConfig;Ljava/util/Map;)Lcom/safonov/speedreading/training/fragment/wordscolumns/repository/entity/WordsColumnsConfig;

    move-result-object p1

    goto :goto_0

    .line 518
    .restart local v12    # "pkColumnIndex":J
    .restart local v16    # "rowIndex":J
    .restart local v18    # "table":Lio/realm/internal/Table;
    :catchall_0
    move-exception v5

    :goto_2
    invoke-virtual {v4}, Lio/realm/BaseRealm$RealmObjectContext;->clear()V

    throw v5

    .line 521
    :cond_4
    const/4 v11, 0x0

    goto :goto_1

    .line 528
    .end local v12    # "pkColumnIndex":J
    .end local v16    # "rowIndex":J
    .end local v18    # "table":Lio/realm/internal/Table;
    :cond_5
    invoke-static/range {p0 .. p3}, Lio/realm/WordsColumnsConfigRealmProxy;->copy(Lio/realm/Realm;Lcom/safonov/speedreading/training/fragment/wordscolumns/repository/entity/WordsColumnsConfig;ZLjava/util/Map;)Lcom/safonov/speedreading/training/fragment/wordscolumns/repository/entity/WordsColumnsConfig;

    move-result-object p1

    goto/16 :goto_0

    .line 518
    .end local v14    # "realmObject":Lcom/safonov/speedreading/training/fragment/wordscolumns/repository/entity/WordsColumnsConfig;
    .restart local v12    # "pkColumnIndex":J
    .restart local v15    # "realmObject":Lcom/safonov/speedreading/training/fragment/wordscolumns/repository/entity/WordsColumnsConfig;
    .restart local v16    # "rowIndex":J
    .restart local v18    # "table":Lio/realm/internal/Table;
    :catchall_1
    move-exception v5

    move-object v14, v15

    .end local v15    # "realmObject":Lcom/safonov/speedreading/training/fragment/wordscolumns/repository/entity/WordsColumnsConfig;
    .restart local v14    # "realmObject":Lcom/safonov/speedreading/training/fragment/wordscolumns/repository/entity/WordsColumnsConfig;
    goto :goto_2
.end method

.method public static createDetachedCopy(Lcom/safonov/speedreading/training/fragment/wordscolumns/repository/entity/WordsColumnsConfig;IILjava/util/Map;)Lcom/safonov/speedreading/training/fragment/wordscolumns/repository/entity/WordsColumnsConfig;
    .locals 6
    .param p0, "realmObject"    # Lcom/safonov/speedreading/training/fragment/wordscolumns/repository/entity/WordsColumnsConfig;
    .param p1, "currentDepth"    # I
    .param p2, "maxDepth"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/safonov/speedreading/training/fragment/wordscolumns/repository/entity/WordsColumnsConfig;",
            "II",
            "Ljava/util/Map",
            "<",
            "Lio/realm/RealmModel;",
            "Lio/realm/internal/RealmObjectProxy$CacheData",
            "<",
            "Lio/realm/RealmModel;",
            ">;>;)",
            "Lcom/safonov/speedreading/training/fragment/wordscolumns/repository/entity/WordsColumnsConfig;"
        }
    .end annotation

    .prologue
    .line 667
    .local p3, "cache":Ljava/util/Map;, "Ljava/util/Map<Lio/realm/RealmModel;Lio/realm/internal/RealmObjectProxy$CacheData<Lio/realm/RealmModel;>;>;"
    if-gt p1, p2, :cond_0

    if-nez p0, :cond_1

    .line 668
    :cond_0
    const/4 v2, 0x0

    .line 690
    .end local p0    # "realmObject":Lcom/safonov/speedreading/training/fragment/wordscolumns/repository/entity/WordsColumnsConfig;
    :goto_0
    return-object v2

    .line 670
    .restart local p0    # "realmObject":Lcom/safonov/speedreading/training/fragment/wordscolumns/repository/entity/WordsColumnsConfig;
    :cond_1
    invoke-interface {p3, p0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/realm/internal/RealmObjectProxy$CacheData;

    .line 672
    .local v0, "cachedObject":Lio/realm/internal/RealmObjectProxy$CacheData;, "Lio/realm/internal/RealmObjectProxy$CacheData<Lio/realm/RealmModel;>;"
    if-eqz v0, :cond_3

    .line 674
    iget v2, v0, Lio/realm/internal/RealmObjectProxy$CacheData;->minDepth:I

    if-lt p1, v2, :cond_2

    .line 675
    iget-object v2, v0, Lio/realm/internal/RealmObjectProxy$CacheData;->object:Lio/realm/RealmModel;

    check-cast v2, Lcom/safonov/speedreading/training/fragment/wordscolumns/repository/entity/WordsColumnsConfig;

    goto :goto_0

    .line 677
    :cond_2
    iget-object v1, v0, Lio/realm/internal/RealmObjectProxy$CacheData;->object:Lio/realm/RealmModel;

    check-cast v1, Lcom/safonov/speedreading/training/fragment/wordscolumns/repository/entity/WordsColumnsConfig;

    .line 678
    .local v1, "unmanagedObject":Lcom/safonov/speedreading/training/fragment/wordscolumns/repository/entity/WordsColumnsConfig;
    iput p1, v0, Lio/realm/internal/RealmObjectProxy$CacheData;->minDepth:I

    :goto_1
    move-object v2, v1

    .line 684
    check-cast v2, Lio/realm/WordsColumnsConfigRealmProxyInterface;

    move-object v3, p0

    check-cast v3, Lio/realm/WordsColumnsConfigRealmProxyInterface;

    invoke-interface {v3}, Lio/realm/WordsColumnsConfigRealmProxyInterface;->realmGet$id()I

    move-result v3

    invoke-interface {v2, v3}, Lio/realm/WordsColumnsConfigRealmProxyInterface;->realmSet$id(I)V

    move-object v2, v1

    .line 685
    check-cast v2, Lio/realm/WordsColumnsConfigRealmProxyInterface;

    move-object v3, p0

    check-cast v3, Lio/realm/WordsColumnsConfigRealmProxyInterface;

    invoke-interface {v3}, Lio/realm/WordsColumnsConfigRealmProxyInterface;->realmGet$rowCount()I

    move-result v3

    invoke-interface {v2, v3}, Lio/realm/WordsColumnsConfigRealmProxyInterface;->realmSet$rowCount(I)V

    move-object v2, v1

    .line 686
    check-cast v2, Lio/realm/WordsColumnsConfigRealmProxyInterface;

    move-object v3, p0

    check-cast v3, Lio/realm/WordsColumnsConfigRealmProxyInterface;

    invoke-interface {v3}, Lio/realm/WordsColumnsConfigRealmProxyInterface;->realmGet$columnCount()I

    move-result v3

    invoke-interface {v2, v3}, Lio/realm/WordsColumnsConfigRealmProxyInterface;->realmSet$columnCount(I)V

    move-object v2, v1

    .line 687
    check-cast v2, Lio/realm/WordsColumnsConfigRealmProxyInterface;

    move-object v3, p0

    check-cast v3, Lio/realm/WordsColumnsConfigRealmProxyInterface;

    invoke-interface {v3}, Lio/realm/WordsColumnsConfigRealmProxyInterface;->realmGet$wordsPerItem()I

    move-result v3

    invoke-interface {v2, v3}, Lio/realm/WordsColumnsConfigRealmProxyInterface;->realmSet$wordsPerItem(I)V

    move-object v2, v1

    .line 688
    check-cast v2, Lio/realm/WordsColumnsConfigRealmProxyInterface;

    move-object v3, p0

    check-cast v3, Lio/realm/WordsColumnsConfigRealmProxyInterface;

    invoke-interface {v3}, Lio/realm/WordsColumnsConfigRealmProxyInterface;->realmGet$speed()I

    move-result v3

    invoke-interface {v2, v3}, Lio/realm/WordsColumnsConfigRealmProxyInterface;->realmSet$speed(I)V

    move-object v2, v1

    .line 689
    check-cast v2, Lio/realm/WordsColumnsConfigRealmProxyInterface;

    check-cast p0, Lio/realm/WordsColumnsConfigRealmProxyInterface;

    .end local p0    # "realmObject":Lcom/safonov/speedreading/training/fragment/wordscolumns/repository/entity/WordsColumnsConfig;
    invoke-interface {p0}, Lio/realm/WordsColumnsConfigRealmProxyInterface;->realmGet$trainingDuration()J

    move-result-wide v4

    invoke-interface {v2, v4, v5}, Lio/realm/WordsColumnsConfigRealmProxyInterface;->realmSet$trainingDuration(J)V

    move-object v2, v1

    .line 690
    goto :goto_0

    .line 681
    .end local v1    # "unmanagedObject":Lcom/safonov/speedreading/training/fragment/wordscolumns/repository/entity/WordsColumnsConfig;
    .restart local p0    # "realmObject":Lcom/safonov/speedreading/training/fragment/wordscolumns/repository/entity/WordsColumnsConfig;
    :cond_3
    new-instance v1, Lcom/safonov/speedreading/training/fragment/wordscolumns/repository/entity/WordsColumnsConfig;

    invoke-direct {v1}, Lcom/safonov/speedreading/training/fragment/wordscolumns/repository/entity/WordsColumnsConfig;-><init>()V

    .line 682
    .restart local v1    # "unmanagedObject":Lcom/safonov/speedreading/training/fragment/wordscolumns/repository/entity/WordsColumnsConfig;
    new-instance v2, Lio/realm/internal/RealmObjectProxy$CacheData;

    invoke-direct {v2, p1, v1}, Lio/realm/internal/RealmObjectProxy$CacheData;-><init>(ILio/realm/RealmModel;)V

    invoke-interface {p3, p0, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1
.end method

.method public static createOrUpdateUsingJsonObject(Lio/realm/Realm;Lorg/json/JSONObject;Z)Lcom/safonov/speedreading/training/fragment/wordscolumns/repository/entity/WordsColumnsConfig;
    .locals 13
    .param p0, "realm"    # Lio/realm/Realm;
    .param p1, "json"    # Lorg/json/JSONObject;
    .param p2, "update"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .prologue
    .line 362
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v6

    .line 363
    .local v6, "excludeFields":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const/4 v7, 0x0

    .line 364
    .local v7, "obj":Lcom/safonov/speedreading/training/fragment/wordscolumns/repository/entity/WordsColumnsConfig;
    if-eqz p2, :cond_1

    .line 365
    const-class v1, Lcom/safonov/speedreading/training/fragment/wordscolumns/repository/entity/WordsColumnsConfig;

    invoke-virtual {p0, v1}, Lio/realm/Realm;->getTable(Ljava/lang/Class;)Lio/realm/internal/Table;

    move-result-object v12

    .line 366
    .local v12, "table":Lio/realm/internal/Table;
    invoke-virtual {v12}, Lio/realm/internal/Table;->getPrimaryKey()J

    move-result-wide v8

    .line 367
    .local v8, "pkColumnIndex":J
    const-wide/16 v10, -0x1

    .line 368
    .local v10, "rowIndex":J
    const-string v1, "id"

    invoke-virtual {p1, v1}, Lorg/json/JSONObject;->isNull(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 369
    const-string v1, "id"

    invoke-virtual {p1, v1}, Lorg/json/JSONObject;->getLong(Ljava/lang/String;)J

    move-result-wide v2

    invoke-virtual {v12, v8, v9, v2, v3}, Lio/realm/internal/Table;->findFirstLong(JJ)J

    move-result-wide v10

    .line 371
    :cond_0
    const-wide/16 v2, -0x1

    cmp-long v1, v10, v2

    if-eqz v1, :cond_1

    .line 372
    sget-object v1, Lio/realm/BaseRealm;->objectContext:Lio/realm/BaseRealm$ThreadLocalRealmObjectContext;

    invoke-virtual {v1}, Lio/realm/BaseRealm$ThreadLocalRealmObjectContext;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/realm/BaseRealm$RealmObjectContext;

    .line 374
    .local v0, "objectContext":Lio/realm/BaseRealm$RealmObjectContext;
    :try_start_0
    invoke-virtual {v12, v10, v11}, Lio/realm/internal/Table;->getUncheckedRow(J)Lio/realm/internal/UncheckedRow;

    move-result-object v2

    iget-object v1, p0, Lio/realm/Realm;->schema:Lio/realm/RealmSchema;

    const-class v3, Lcom/safonov/speedreading/training/fragment/wordscolumns/repository/entity/WordsColumnsConfig;

    invoke-virtual {v1, v3}, Lio/realm/RealmSchema;->getColumnInfo(Ljava/lang/Class;)Lio/realm/internal/ColumnInfo;

    move-result-object v3

    const/4 v4, 0x0

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v5

    move-object v1, p0

    invoke-virtual/range {v0 .. v5}, Lio/realm/BaseRealm$RealmObjectContext;->set(Lio/realm/BaseRealm;Lio/realm/internal/Row;Lio/realm/internal/ColumnInfo;ZLjava/util/List;)V

    .line 375
    new-instance v7, Lio/realm/WordsColumnsConfigRealmProxy;

    .end local v7    # "obj":Lcom/safonov/speedreading/training/fragment/wordscolumns/repository/entity/WordsColumnsConfig;
    invoke-direct {v7}, Lio/realm/WordsColumnsConfigRealmProxy;-><init>()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 377
    .restart local v7    # "obj":Lcom/safonov/speedreading/training/fragment/wordscolumns/repository/entity/WordsColumnsConfig;
    invoke-virtual {v0}, Lio/realm/BaseRealm$RealmObjectContext;->clear()V

    .line 381
    .end local v0    # "objectContext":Lio/realm/BaseRealm$RealmObjectContext;
    .end local v8    # "pkColumnIndex":J
    .end local v10    # "rowIndex":J
    .end local v12    # "table":Lio/realm/internal/Table;
    :cond_1
    if-nez v7, :cond_2

    .line 382
    const-string v1, "id"

    invoke-virtual {p1, v1}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 383
    const-string v1, "id"

    invoke-virtual {p1, v1}, Lorg/json/JSONObject;->isNull(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 384
    const-class v1, Lcom/safonov/speedreading/training/fragment/wordscolumns/repository/entity/WordsColumnsConfig;

    const/4 v2, 0x0

    const/4 v3, 0x1

    invoke-virtual {p0, v1, v2, v3, v6}, Lio/realm/Realm;->createObjectInternal(Ljava/lang/Class;Ljava/lang/Object;ZLjava/util/List;)Lio/realm/RealmModel;

    move-result-object v7

    .end local v7    # "obj":Lcom/safonov/speedreading/training/fragment/wordscolumns/repository/entity/WordsColumnsConfig;
    check-cast v7, Lio/realm/WordsColumnsConfigRealmProxy;

    .line 392
    .restart local v7    # "obj":Lcom/safonov/speedreading/training/fragment/wordscolumns/repository/entity/WordsColumnsConfig;
    :cond_2
    :goto_0
    const-string v1, "rowCount"

    invoke-virtual {p1, v1}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 393
    const-string v1, "rowCount"

    invoke-virtual {p1, v1}, Lorg/json/JSONObject;->isNull(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 394
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "Trying to set non-nullable field \'rowCount\' to null."

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 377
    .end local v7    # "obj":Lcom/safonov/speedreading/training/fragment/wordscolumns/repository/entity/WordsColumnsConfig;
    .restart local v0    # "objectContext":Lio/realm/BaseRealm$RealmObjectContext;
    .restart local v8    # "pkColumnIndex":J
    .restart local v10    # "rowIndex":J
    .restart local v12    # "table":Lio/realm/internal/Table;
    :catchall_0
    move-exception v1

    invoke-virtual {v0}, Lio/realm/BaseRealm$RealmObjectContext;->clear()V

    throw v1

    .line 386
    .end local v0    # "objectContext":Lio/realm/BaseRealm$RealmObjectContext;
    .end local v8    # "pkColumnIndex":J
    .end local v10    # "rowIndex":J
    .end local v12    # "table":Lio/realm/internal/Table;
    .restart local v7    # "obj":Lcom/safonov/speedreading/training/fragment/wordscolumns/repository/entity/WordsColumnsConfig;
    :cond_3
    const-class v1, Lcom/safonov/speedreading/training/fragment/wordscolumns/repository/entity/WordsColumnsConfig;

    const-string v2, "id"

    invoke-virtual {p1, v2}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v3, 0x1

    invoke-virtual {p0, v1, v2, v3, v6}, Lio/realm/Realm;->createObjectInternal(Ljava/lang/Class;Ljava/lang/Object;ZLjava/util/List;)Lio/realm/RealmModel;

    move-result-object v7

    .end local v7    # "obj":Lcom/safonov/speedreading/training/fragment/wordscolumns/repository/entity/WordsColumnsConfig;
    check-cast v7, Lio/realm/WordsColumnsConfigRealmProxy;

    .restart local v7    # "obj":Lcom/safonov/speedreading/training/fragment/wordscolumns/repository/entity/WordsColumnsConfig;
    goto :goto_0

    .line 389
    :cond_4
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "JSON object doesn\'t have the primary key field \'id\'."

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_5
    move-object v1, v7

    .line 396
    check-cast v1, Lio/realm/WordsColumnsConfigRealmProxyInterface;

    const-string v2, "rowCount"

    invoke-virtual {p1, v2}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v1, v2}, Lio/realm/WordsColumnsConfigRealmProxyInterface;->realmSet$rowCount(I)V

    .line 399
    :cond_6
    const-string v1, "columnCount"

    invoke-virtual {p1, v1}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_8

    .line 400
    const-string v1, "columnCount"

    invoke-virtual {p1, v1}, Lorg/json/JSONObject;->isNull(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_7

    .line 401
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "Trying to set non-nullable field \'columnCount\' to null."

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_7
    move-object v1, v7

    .line 403
    check-cast v1, Lio/realm/WordsColumnsConfigRealmProxyInterface;

    const-string v2, "columnCount"

    invoke-virtual {p1, v2}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v1, v2}, Lio/realm/WordsColumnsConfigRealmProxyInterface;->realmSet$columnCount(I)V

    .line 406
    :cond_8
    const-string v1, "wordsPerItem"

    invoke-virtual {p1, v1}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_a

    .line 407
    const-string v1, "wordsPerItem"

    invoke-virtual {p1, v1}, Lorg/json/JSONObject;->isNull(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_9

    .line 408
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "Trying to set non-nullable field \'wordsPerItem\' to null."

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_9
    move-object v1, v7

    .line 410
    check-cast v1, Lio/realm/WordsColumnsConfigRealmProxyInterface;

    const-string v2, "wordsPerItem"

    invoke-virtual {p1, v2}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v1, v2}, Lio/realm/WordsColumnsConfigRealmProxyInterface;->realmSet$wordsPerItem(I)V

    .line 413
    :cond_a
    const-string v1, "speed"

    invoke-virtual {p1, v1}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_c

    .line 414
    const-string v1, "speed"

    invoke-virtual {p1, v1}, Lorg/json/JSONObject;->isNull(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_b

    .line 415
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "Trying to set non-nullable field \'speed\' to null."

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_b
    move-object v1, v7

    .line 417
    check-cast v1, Lio/realm/WordsColumnsConfigRealmProxyInterface;

    const-string v2, "speed"

    invoke-virtual {p1, v2}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v1, v2}, Lio/realm/WordsColumnsConfigRealmProxyInterface;->realmSet$speed(I)V

    .line 420
    :cond_c
    const-string v1, "trainingDuration"

    invoke-virtual {p1, v1}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_e

    .line 421
    const-string v1, "trainingDuration"

    invoke-virtual {p1, v1}, Lorg/json/JSONObject;->isNull(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_d

    .line 422
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "Trying to set non-nullable field \'trainingDuration\' to null."

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_d
    move-object v1, v7

    .line 424
    check-cast v1, Lio/realm/WordsColumnsConfigRealmProxyInterface;

    const-string v2, "trainingDuration"

    invoke-virtual {p1, v2}, Lorg/json/JSONObject;->getLong(Ljava/lang/String;)J

    move-result-wide v2

    invoke-interface {v1, v2, v3}, Lio/realm/WordsColumnsConfigRealmProxyInterface;->realmSet$trainingDuration(J)V

    .line 427
    :cond_e
    return-object v7
.end method

.method public static createRealmObjectSchema(Lio/realm/RealmSchema;)Lio/realm/RealmObjectSchema;
    .locals 11
    .param p0, "realmSchema"    # Lio/realm/RealmSchema;

    .prologue
    const/4 v3, 0x1

    const/4 v7, 0x0

    .line 230
    const-string v0, "WordsColumnsConfig"

    invoke-virtual {p0, v0}, Lio/realm/RealmSchema;->contains(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 231
    const-string v0, "WordsColumnsConfig"

    invoke-virtual {p0, v0}, Lio/realm/RealmSchema;->create(Ljava/lang/String;)Lio/realm/RealmObjectSchema;

    move-result-object v10

    .line 232
    .local v10, "realmObjectSchema":Lio/realm/RealmObjectSchema;
    new-instance v0, Lio/realm/Property;

    const-string v1, "id"

    sget-object v2, Lio/realm/RealmFieldType;->INTEGER:Lio/realm/RealmFieldType;

    move v4, v3

    move v5, v3

    invoke-direct/range {v0 .. v5}, Lio/realm/Property;-><init>(Ljava/lang/String;Lio/realm/RealmFieldType;ZZZ)V

    invoke-virtual {v10, v0}, Lio/realm/RealmObjectSchema;->add(Lio/realm/Property;)Lio/realm/RealmObjectSchema;

    .line 233
    new-instance v4, Lio/realm/Property;

    const-string v5, "rowCount"

    sget-object v6, Lio/realm/RealmFieldType;->INTEGER:Lio/realm/RealmFieldType;

    move v8, v7

    move v9, v3

    invoke-direct/range {v4 .. v9}, Lio/realm/Property;-><init>(Ljava/lang/String;Lio/realm/RealmFieldType;ZZZ)V

    invoke-virtual {v10, v4}, Lio/realm/RealmObjectSchema;->add(Lio/realm/Property;)Lio/realm/RealmObjectSchema;

    .line 234
    new-instance v4, Lio/realm/Property;

    const-string v5, "columnCount"

    sget-object v6, Lio/realm/RealmFieldType;->INTEGER:Lio/realm/RealmFieldType;

    move v8, v7

    move v9, v3

    invoke-direct/range {v4 .. v9}, Lio/realm/Property;-><init>(Ljava/lang/String;Lio/realm/RealmFieldType;ZZZ)V

    invoke-virtual {v10, v4}, Lio/realm/RealmObjectSchema;->add(Lio/realm/Property;)Lio/realm/RealmObjectSchema;

    .line 235
    new-instance v4, Lio/realm/Property;

    const-string v5, "wordsPerItem"

    sget-object v6, Lio/realm/RealmFieldType;->INTEGER:Lio/realm/RealmFieldType;

    move v8, v7

    move v9, v3

    invoke-direct/range {v4 .. v9}, Lio/realm/Property;-><init>(Ljava/lang/String;Lio/realm/RealmFieldType;ZZZ)V

    invoke-virtual {v10, v4}, Lio/realm/RealmObjectSchema;->add(Lio/realm/Property;)Lio/realm/RealmObjectSchema;

    .line 236
    new-instance v4, Lio/realm/Property;

    const-string v5, "speed"

    sget-object v6, Lio/realm/RealmFieldType;->INTEGER:Lio/realm/RealmFieldType;

    move v8, v7

    move v9, v3

    invoke-direct/range {v4 .. v9}, Lio/realm/Property;-><init>(Ljava/lang/String;Lio/realm/RealmFieldType;ZZZ)V

    invoke-virtual {v10, v4}, Lio/realm/RealmObjectSchema;->add(Lio/realm/Property;)Lio/realm/RealmObjectSchema;

    .line 237
    new-instance v4, Lio/realm/Property;

    const-string v5, "trainingDuration"

    sget-object v6, Lio/realm/RealmFieldType;->INTEGER:Lio/realm/RealmFieldType;

    move v8, v7

    move v9, v3

    invoke-direct/range {v4 .. v9}, Lio/realm/Property;-><init>(Ljava/lang/String;Lio/realm/RealmFieldType;ZZZ)V

    invoke-virtual {v10, v4}, Lio/realm/RealmObjectSchema;->add(Lio/realm/Property;)Lio/realm/RealmObjectSchema;

    .line 240
    .end local v10    # "realmObjectSchema":Lio/realm/RealmObjectSchema;
    :goto_0
    return-object v10

    :cond_0
    const-string v0, "WordsColumnsConfig"

    invoke-virtual {p0, v0}, Lio/realm/RealmSchema;->get(Ljava/lang/String;)Lio/realm/RealmObjectSchema;

    move-result-object v10

    goto :goto_0
.end method

.method public static createUsingJsonStream(Lio/realm/Realm;Landroid/util/JsonReader;)Lcom/safonov/speedreading/training/fragment/wordscolumns/repository/entity/WordsColumnsConfig;
    .locals 6
    .param p0, "realm"    # Lio/realm/Realm;
    .param p1, "reader"    # Landroid/util/JsonReader;
    .annotation build Landroid/annotation/TargetApi;
        value = 0xb
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 434
    const/4 v0, 0x0

    .line 435
    .local v0, "jsonHasPrimaryKey":Z
    new-instance v2, Lcom/safonov/speedreading/training/fragment/wordscolumns/repository/entity/WordsColumnsConfig;

    invoke-direct {v2}, Lcom/safonov/speedreading/training/fragment/wordscolumns/repository/entity/WordsColumnsConfig;-><init>()V

    .line 436
    .local v2, "obj":Lcom/safonov/speedreading/training/fragment/wordscolumns/repository/entity/WordsColumnsConfig;
    invoke-virtual {p1}, Landroid/util/JsonReader;->beginObject()V

    .line 437
    :goto_0
    invoke-virtual {p1}, Landroid/util/JsonReader;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_c

    .line 438
    invoke-virtual {p1}, Landroid/util/JsonReader;->nextName()Ljava/lang/String;

    move-result-object v1

    .line 439
    .local v1, "name":Ljava/lang/String;
    const-string v3, "id"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 440
    invoke-virtual {p1}, Landroid/util/JsonReader;->peek()Landroid/util/JsonToken;

    move-result-object v3

    sget-object v4, Landroid/util/JsonToken;->NULL:Landroid/util/JsonToken;

    if-ne v3, v4, :cond_0

    .line 441
    invoke-virtual {p1}, Landroid/util/JsonReader;->skipValue()V

    .line 442
    new-instance v3, Ljava/lang/IllegalArgumentException;

    const-string v4, "Trying to set non-nullable field \'id\' to null."

    invoke-direct {v3, v4}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v3

    :cond_0
    move-object v3, v2

    .line 444
    check-cast v3, Lio/realm/WordsColumnsConfigRealmProxyInterface;

    invoke-virtual {p1}, Landroid/util/JsonReader;->nextInt()I

    move-result v4

    invoke-interface {v3, v4}, Lio/realm/WordsColumnsConfigRealmProxyInterface;->realmSet$id(I)V

    .line 446
    const/4 v0, 0x1

    goto :goto_0

    .line 447
    :cond_1
    const-string v3, "rowCount"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 448
    invoke-virtual {p1}, Landroid/util/JsonReader;->peek()Landroid/util/JsonToken;

    move-result-object v3

    sget-object v4, Landroid/util/JsonToken;->NULL:Landroid/util/JsonToken;

    if-ne v3, v4, :cond_2

    .line 449
    invoke-virtual {p1}, Landroid/util/JsonReader;->skipValue()V

    .line 450
    new-instance v3, Ljava/lang/IllegalArgumentException;

    const-string v4, "Trying to set non-nullable field \'rowCount\' to null."

    invoke-direct {v3, v4}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v3

    :cond_2
    move-object v3, v2

    .line 452
    check-cast v3, Lio/realm/WordsColumnsConfigRealmProxyInterface;

    invoke-virtual {p1}, Landroid/util/JsonReader;->nextInt()I

    move-result v4

    invoke-interface {v3, v4}, Lio/realm/WordsColumnsConfigRealmProxyInterface;->realmSet$rowCount(I)V

    goto :goto_0

    .line 454
    :cond_3
    const-string v3, "columnCount"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_5

    .line 455
    invoke-virtual {p1}, Landroid/util/JsonReader;->peek()Landroid/util/JsonToken;

    move-result-object v3

    sget-object v4, Landroid/util/JsonToken;->NULL:Landroid/util/JsonToken;

    if-ne v3, v4, :cond_4

    .line 456
    invoke-virtual {p1}, Landroid/util/JsonReader;->skipValue()V

    .line 457
    new-instance v3, Ljava/lang/IllegalArgumentException;

    const-string v4, "Trying to set non-nullable field \'columnCount\' to null."

    invoke-direct {v3, v4}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v3

    :cond_4
    move-object v3, v2

    .line 459
    check-cast v3, Lio/realm/WordsColumnsConfigRealmProxyInterface;

    invoke-virtual {p1}, Landroid/util/JsonReader;->nextInt()I

    move-result v4

    invoke-interface {v3, v4}, Lio/realm/WordsColumnsConfigRealmProxyInterface;->realmSet$columnCount(I)V

    goto :goto_0

    .line 461
    :cond_5
    const-string v3, "wordsPerItem"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_7

    .line 462
    invoke-virtual {p1}, Landroid/util/JsonReader;->peek()Landroid/util/JsonToken;

    move-result-object v3

    sget-object v4, Landroid/util/JsonToken;->NULL:Landroid/util/JsonToken;

    if-ne v3, v4, :cond_6

    .line 463
    invoke-virtual {p1}, Landroid/util/JsonReader;->skipValue()V

    .line 464
    new-instance v3, Ljava/lang/IllegalArgumentException;

    const-string v4, "Trying to set non-nullable field \'wordsPerItem\' to null."

    invoke-direct {v3, v4}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v3

    :cond_6
    move-object v3, v2

    .line 466
    check-cast v3, Lio/realm/WordsColumnsConfigRealmProxyInterface;

    invoke-virtual {p1}, Landroid/util/JsonReader;->nextInt()I

    move-result v4

    invoke-interface {v3, v4}, Lio/realm/WordsColumnsConfigRealmProxyInterface;->realmSet$wordsPerItem(I)V

    goto/16 :goto_0

    .line 468
    :cond_7
    const-string v3, "speed"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_9

    .line 469
    invoke-virtual {p1}, Landroid/util/JsonReader;->peek()Landroid/util/JsonToken;

    move-result-object v3

    sget-object v4, Landroid/util/JsonToken;->NULL:Landroid/util/JsonToken;

    if-ne v3, v4, :cond_8

    .line 470
    invoke-virtual {p1}, Landroid/util/JsonReader;->skipValue()V

    .line 471
    new-instance v3, Ljava/lang/IllegalArgumentException;

    const-string v4, "Trying to set non-nullable field \'speed\' to null."

    invoke-direct {v3, v4}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v3

    :cond_8
    move-object v3, v2

    .line 473
    check-cast v3, Lio/realm/WordsColumnsConfigRealmProxyInterface;

    invoke-virtual {p1}, Landroid/util/JsonReader;->nextInt()I

    move-result v4

    invoke-interface {v3, v4}, Lio/realm/WordsColumnsConfigRealmProxyInterface;->realmSet$speed(I)V

    goto/16 :goto_0

    .line 475
    :cond_9
    const-string v3, "trainingDuration"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_b

    .line 476
    invoke-virtual {p1}, Landroid/util/JsonReader;->peek()Landroid/util/JsonToken;

    move-result-object v3

    sget-object v4, Landroid/util/JsonToken;->NULL:Landroid/util/JsonToken;

    if-ne v3, v4, :cond_a

    .line 477
    invoke-virtual {p1}, Landroid/util/JsonReader;->skipValue()V

    .line 478
    new-instance v3, Ljava/lang/IllegalArgumentException;

    const-string v4, "Trying to set non-nullable field \'trainingDuration\' to null."

    invoke-direct {v3, v4}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v3

    :cond_a
    move-object v3, v2

    .line 480
    check-cast v3, Lio/realm/WordsColumnsConfigRealmProxyInterface;

    invoke-virtual {p1}, Landroid/util/JsonReader;->nextLong()J

    move-result-wide v4

    invoke-interface {v3, v4, v5}, Lio/realm/WordsColumnsConfigRealmProxyInterface;->realmSet$trainingDuration(J)V

    goto/16 :goto_0

    .line 483
    :cond_b
    invoke-virtual {p1}, Landroid/util/JsonReader;->skipValue()V

    goto/16 :goto_0

    .line 486
    .end local v1    # "name":Ljava/lang/String;
    :cond_c
    invoke-virtual {p1}, Landroid/util/JsonReader;->endObject()V

    .line 487
    if-nez v0, :cond_d

    .line 488
    new-instance v3, Ljava/lang/IllegalArgumentException;

    const-string v4, "JSON object doesn\'t have the primary key field \'id\'."

    invoke-direct {v3, v4}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 490
    :cond_d
    invoke-virtual {p0, v2}, Lio/realm/Realm;->copyToRealm(Lio/realm/RealmModel;)Lio/realm/RealmModel;

    move-result-object v2

    .end local v2    # "obj":Lcom/safonov/speedreading/training/fragment/wordscolumns/repository/entity/WordsColumnsConfig;
    check-cast v2, Lcom/safonov/speedreading/training/fragment/wordscolumns/repository/entity/WordsColumnsConfig;

    .line 491
    .restart local v2    # "obj":Lcom/safonov/speedreading/training/fragment/wordscolumns/repository/entity/WordsColumnsConfig;
    return-object v2
.end method

.method public static getFieldNames()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 356
    sget-object v0, Lio/realm/WordsColumnsConfigRealmProxy;->FIELD_NAMES:Ljava/util/List;

    return-object v0
.end method

.method public static getTableName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 352
    const-string v0, "class_WordsColumnsConfig"

    return-object v0
.end method

.method public static initTable(Lio/realm/internal/SharedRealm;)Lio/realm/internal/Table;
    .locals 4
    .param p0, "sharedRealm"    # Lio/realm/internal/SharedRealm;

    .prologue
    const/4 v3, 0x0

    .line 244
    const-string v1, "class_WordsColumnsConfig"

    invoke-virtual {p0, v1}, Lio/realm/internal/SharedRealm;->hasTable(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 245
    const-string v1, "class_WordsColumnsConfig"

    invoke-virtual {p0, v1}, Lio/realm/internal/SharedRealm;->getTable(Ljava/lang/String;)Lio/realm/internal/Table;

    move-result-object v0

    .line 246
    .local v0, "table":Lio/realm/internal/Table;
    sget-object v1, Lio/realm/RealmFieldType;->INTEGER:Lio/realm/RealmFieldType;

    const-string v2, "id"

    invoke-virtual {v0, v1, v2, v3}, Lio/realm/internal/Table;->addColumn(Lio/realm/RealmFieldType;Ljava/lang/String;Z)J

    .line 247
    sget-object v1, Lio/realm/RealmFieldType;->INTEGER:Lio/realm/RealmFieldType;

    const-string v2, "rowCount"

    invoke-virtual {v0, v1, v2, v3}, Lio/realm/internal/Table;->addColumn(Lio/realm/RealmFieldType;Ljava/lang/String;Z)J

    .line 248
    sget-object v1, Lio/realm/RealmFieldType;->INTEGER:Lio/realm/RealmFieldType;

    const-string v2, "columnCount"

    invoke-virtual {v0, v1, v2, v3}, Lio/realm/internal/Table;->addColumn(Lio/realm/RealmFieldType;Ljava/lang/String;Z)J

    .line 249
    sget-object v1, Lio/realm/RealmFieldType;->INTEGER:Lio/realm/RealmFieldType;

    const-string v2, "wordsPerItem"

    invoke-virtual {v0, v1, v2, v3}, Lio/realm/internal/Table;->addColumn(Lio/realm/RealmFieldType;Ljava/lang/String;Z)J

    .line 250
    sget-object v1, Lio/realm/RealmFieldType;->INTEGER:Lio/realm/RealmFieldType;

    const-string v2, "speed"

    invoke-virtual {v0, v1, v2, v3}, Lio/realm/internal/Table;->addColumn(Lio/realm/RealmFieldType;Ljava/lang/String;Z)J

    .line 251
    sget-object v1, Lio/realm/RealmFieldType;->INTEGER:Lio/realm/RealmFieldType;

    const-string v2, "trainingDuration"

    invoke-virtual {v0, v1, v2, v3}, Lio/realm/internal/Table;->addColumn(Lio/realm/RealmFieldType;Ljava/lang/String;Z)J

    .line 252
    const-string v1, "id"

    invoke-virtual {v0, v1}, Lio/realm/internal/Table;->getColumnIndex(Ljava/lang/String;)J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lio/realm/internal/Table;->addSearchIndex(J)V

    .line 253
    const-string v1, "id"

    invoke-virtual {v0, v1}, Lio/realm/internal/Table;->setPrimaryKey(Ljava/lang/String;)V

    .line 256
    .end local v0    # "table":Lio/realm/internal/Table;
    :goto_0
    return-object v0

    :cond_0
    const-string v1, "class_WordsColumnsConfig"

    invoke-virtual {p0, v1}, Lio/realm/internal/SharedRealm;->getTable(Ljava/lang/String;)Lio/realm/internal/Table;

    move-result-object v0

    goto :goto_0
.end method

.method public static insert(Lio/realm/Realm;Lcom/safonov/speedreading/training/fragment/wordscolumns/repository/entity/WordsColumnsConfig;Ljava/util/Map;)J
    .locals 18
    .param p0, "realm"    # Lio/realm/Realm;
    .param p1, "object"    # Lcom/safonov/speedreading/training/fragment/wordscolumns/repository/entity/WordsColumnsConfig;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/realm/Realm;",
            "Lcom/safonov/speedreading/training/fragment/wordscolumns/repository/entity/WordsColumnsConfig;",
            "Ljava/util/Map",
            "<",
            "Lio/realm/RealmModel;",
            "Ljava/lang/Long;",
            ">;)J"
        }
    .end annotation

    .prologue
    .line 551
    .local p2, "cache":Ljava/util/Map;, "Ljava/util/Map<Lio/realm/RealmModel;Ljava/lang/Long;>;"
    move-object/from16 v0, p1

    instance-of v6, v0, Lio/realm/internal/RealmObjectProxy;

    if-eqz v6, :cond_0

    move-object/from16 v6, p1

    check-cast v6, Lio/realm/internal/RealmObjectProxy;

    invoke-interface {v6}, Lio/realm/internal/RealmObjectProxy;->realmGet$proxyState()Lio/realm/ProxyState;

    move-result-object v6

    invoke-virtual {v6}, Lio/realm/ProxyState;->getRealm$realm()Lio/realm/BaseRealm;

    move-result-object v6

    if-eqz v6, :cond_0

    move-object/from16 v6, p1

    check-cast v6, Lio/realm/internal/RealmObjectProxy;

    invoke-interface {v6}, Lio/realm/internal/RealmObjectProxy;->realmGet$proxyState()Lio/realm/ProxyState;

    move-result-object v6

    invoke-virtual {v6}, Lio/realm/ProxyState;->getRealm$realm()Lio/realm/BaseRealm;

    move-result-object v6

    invoke-virtual {v6}, Lio/realm/BaseRealm;->getPath()Ljava/lang/String;

    move-result-object v6

    invoke-virtual/range {p0 .. p0}, Lio/realm/Realm;->getPath()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 552
    check-cast p1, Lio/realm/internal/RealmObjectProxy;

    .end local p1    # "object":Lcom/safonov/speedreading/training/fragment/wordscolumns/repository/entity/WordsColumnsConfig;
    invoke-interface/range {p1 .. p1}, Lio/realm/internal/RealmObjectProxy;->realmGet$proxyState()Lio/realm/ProxyState;

    move-result-object v6

    invoke-virtual {v6}, Lio/realm/ProxyState;->getRow$realm()Lio/realm/internal/Row;

    move-result-object v6

    invoke-interface {v6}, Lio/realm/internal/Row;->getIndex()J

    move-result-wide v10

    .line 574
    :goto_0
    return-wide v10

    .line 554
    .restart local p1    # "object":Lcom/safonov/speedreading/training/fragment/wordscolumns/repository/entity/WordsColumnsConfig;
    :cond_0
    const-class v6, Lcom/safonov/speedreading/training/fragment/wordscolumns/repository/entity/WordsColumnsConfig;

    move-object/from16 v0, p0

    invoke-virtual {v0, v6}, Lio/realm/Realm;->getTable(Ljava/lang/Class;)Lio/realm/internal/Table;

    move-result-object v17

    .line 555
    .local v17, "table":Lio/realm/internal/Table;
    invoke-virtual/range {v17 .. v17}, Lio/realm/internal/Table;->getNativeTablePointer()J

    move-result-wide v2

    .line 556
    .local v2, "tableNativePtr":J
    move-object/from16 v0, p0

    iget-object v6, v0, Lio/realm/Realm;->schema:Lio/realm/RealmSchema;

    const-class v7, Lcom/safonov/speedreading/training/fragment/wordscolumns/repository/entity/WordsColumnsConfig;

    invoke-virtual {v6, v7}, Lio/realm/RealmSchema;->getColumnInfo(Ljava/lang/Class;)Lio/realm/internal/ColumnInfo;

    move-result-object v15

    check-cast v15, Lio/realm/WordsColumnsConfigRealmProxy$WordsColumnsConfigColumnInfo;

    .line 557
    .local v15, "columnInfo":Lio/realm/WordsColumnsConfigRealmProxy$WordsColumnsConfigColumnInfo;
    invoke-virtual/range {v17 .. v17}, Lio/realm/internal/Table;->getPrimaryKey()J

    move-result-wide v4

    .line 558
    .local v4, "pkColumnIndex":J
    const-wide/16 v10, -0x1

    .local v10, "rowIndex":J
    move-object/from16 v6, p1

    .line 559
    check-cast v6, Lio/realm/WordsColumnsConfigRealmProxyInterface;

    invoke-interface {v6}, Lio/realm/WordsColumnsConfigRealmProxyInterface;->realmGet$id()I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v16

    .line 560
    .local v16, "primaryKeyValue":Ljava/lang/Integer;
    if-eqz v16, :cond_1

    move-object/from16 v6, p1

    .line 561
    check-cast v6, Lio/realm/WordsColumnsConfigRealmProxyInterface;

    invoke-interface {v6}, Lio/realm/WordsColumnsConfigRealmProxyInterface;->realmGet$id()I

    move-result v6

    int-to-long v6, v6

    invoke-static/range {v2 .. v7}, Lio/realm/internal/Table;->nativeFindFirstInt(JJJ)J

    move-result-wide v10

    .line 563
    :cond_1
    const-wide/16 v6, -0x1

    cmp-long v6, v10, v6

    if-nez v6, :cond_2

    move-object/from16 v6, p1

    .line 564
    check-cast v6, Lio/realm/WordsColumnsConfigRealmProxyInterface;

    invoke-interface {v6}, Lio/realm/WordsColumnsConfigRealmProxyInterface;->realmGet$id()I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    const/4 v7, 0x0

    move-object/from16 v0, v17

    invoke-virtual {v0, v6, v7}, Lio/realm/internal/Table;->addEmptyRowWithPrimaryKey(Ljava/lang/Object;Z)J

    move-result-wide v10

    .line 568
    :goto_1
    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    move-object/from16 v0, p2

    move-object/from16 v1, p1

    invoke-interface {v0, v1, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 569
    iget-wide v8, v15, Lio/realm/WordsColumnsConfigRealmProxy$WordsColumnsConfigColumnInfo;->rowCountIndex:J

    move-object/from16 v6, p1

    check-cast v6, Lio/realm/WordsColumnsConfigRealmProxyInterface;

    invoke-interface {v6}, Lio/realm/WordsColumnsConfigRealmProxyInterface;->realmGet$rowCount()I

    move-result v6

    int-to-long v12, v6

    const/4 v14, 0x0

    move-wide v6, v2

    invoke-static/range {v6 .. v14}, Lio/realm/internal/Table;->nativeSetLong(JJJJZ)V

    .line 570
    iget-wide v8, v15, Lio/realm/WordsColumnsConfigRealmProxy$WordsColumnsConfigColumnInfo;->columnCountIndex:J

    move-object/from16 v6, p1

    check-cast v6, Lio/realm/WordsColumnsConfigRealmProxyInterface;

    invoke-interface {v6}, Lio/realm/WordsColumnsConfigRealmProxyInterface;->realmGet$columnCount()I

    move-result v6

    int-to-long v12, v6

    const/4 v14, 0x0

    move-wide v6, v2

    invoke-static/range {v6 .. v14}, Lio/realm/internal/Table;->nativeSetLong(JJJJZ)V

    .line 571
    iget-wide v8, v15, Lio/realm/WordsColumnsConfigRealmProxy$WordsColumnsConfigColumnInfo;->wordsPerItemIndex:J

    move-object/from16 v6, p1

    check-cast v6, Lio/realm/WordsColumnsConfigRealmProxyInterface;

    invoke-interface {v6}, Lio/realm/WordsColumnsConfigRealmProxyInterface;->realmGet$wordsPerItem()I

    move-result v6

    int-to-long v12, v6

    const/4 v14, 0x0

    move-wide v6, v2

    invoke-static/range {v6 .. v14}, Lio/realm/internal/Table;->nativeSetLong(JJJJZ)V

    .line 572
    iget-wide v8, v15, Lio/realm/WordsColumnsConfigRealmProxy$WordsColumnsConfigColumnInfo;->speedIndex:J

    move-object/from16 v6, p1

    check-cast v6, Lio/realm/WordsColumnsConfigRealmProxyInterface;

    invoke-interface {v6}, Lio/realm/WordsColumnsConfigRealmProxyInterface;->realmGet$speed()I

    move-result v6

    int-to-long v12, v6

    const/4 v14, 0x0

    move-wide v6, v2

    invoke-static/range {v6 .. v14}, Lio/realm/internal/Table;->nativeSetLong(JJJJZ)V

    .line 573
    iget-wide v8, v15, Lio/realm/WordsColumnsConfigRealmProxy$WordsColumnsConfigColumnInfo;->trainingDurationIndex:J

    check-cast p1, Lio/realm/WordsColumnsConfigRealmProxyInterface;

    .end local p1    # "object":Lcom/safonov/speedreading/training/fragment/wordscolumns/repository/entity/WordsColumnsConfig;
    invoke-interface/range {p1 .. p1}, Lio/realm/WordsColumnsConfigRealmProxyInterface;->realmGet$trainingDuration()J

    move-result-wide v12

    const/4 v14, 0x0

    move-wide v6, v2

    invoke-static/range {v6 .. v14}, Lio/realm/internal/Table;->nativeSetLong(JJJJZ)V

    goto/16 :goto_0

    .line 566
    .restart local p1    # "object":Lcom/safonov/speedreading/training/fragment/wordscolumns/repository/entity/WordsColumnsConfig;
    :cond_2
    invoke-static/range {v16 .. v16}, Lio/realm/internal/Table;->throwDuplicatePrimaryKeyException(Ljava/lang/Object;)V

    goto :goto_1
.end method

.method public static insert(Lio/realm/Realm;Ljava/util/Iterator;Ljava/util/Map;)V
    .locals 19
    .param p0, "realm"    # Lio/realm/Realm;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/realm/Realm;",
            "Ljava/util/Iterator",
            "<+",
            "Lio/realm/RealmModel;",
            ">;",
            "Ljava/util/Map",
            "<",
            "Lio/realm/RealmModel;",
            "Ljava/lang/Long;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 578
    .local p1, "objects":Ljava/util/Iterator;, "Ljava/util/Iterator<+Lio/realm/RealmModel;>;"
    .local p2, "cache":Ljava/util/Map;, "Ljava/util/Map<Lio/realm/RealmModel;Ljava/lang/Long;>;"
    const-class v6, Lcom/safonov/speedreading/training/fragment/wordscolumns/repository/entity/WordsColumnsConfig;

    move-object/from16 v0, p0

    invoke-virtual {v0, v6}, Lio/realm/Realm;->getTable(Ljava/lang/Class;)Lio/realm/internal/Table;

    move-result-object v18

    .line 579
    .local v18, "table":Lio/realm/internal/Table;
    invoke-virtual/range {v18 .. v18}, Lio/realm/internal/Table;->getNativeTablePointer()J

    move-result-wide v2

    .line 580
    .local v2, "tableNativePtr":J
    move-object/from16 v0, p0

    iget-object v6, v0, Lio/realm/Realm;->schema:Lio/realm/RealmSchema;

    const-class v7, Lcom/safonov/speedreading/training/fragment/wordscolumns/repository/entity/WordsColumnsConfig;

    invoke-virtual {v6, v7}, Lio/realm/RealmSchema;->getColumnInfo(Ljava/lang/Class;)Lio/realm/internal/ColumnInfo;

    move-result-object v15

    check-cast v15, Lio/realm/WordsColumnsConfigRealmProxy$WordsColumnsConfigColumnInfo;

    .line 581
    .local v15, "columnInfo":Lio/realm/WordsColumnsConfigRealmProxy$WordsColumnsConfigColumnInfo;
    invoke-virtual/range {v18 .. v18}, Lio/realm/internal/Table;->getPrimaryKey()J

    move-result-wide v4

    .line 582
    .local v4, "pkColumnIndex":J
    const/16 v16, 0x0

    .line 583
    .local v16, "object":Lcom/safonov/speedreading/training/fragment/wordscolumns/repository/entity/WordsColumnsConfig;
    :cond_0
    :goto_0
    invoke-interface/range {p1 .. p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_4

    .line 584
    invoke-interface/range {p1 .. p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v16

    .end local v16    # "object":Lcom/safonov/speedreading/training/fragment/wordscolumns/repository/entity/WordsColumnsConfig;
    check-cast v16, Lcom/safonov/speedreading/training/fragment/wordscolumns/repository/entity/WordsColumnsConfig;

    .line 585
    .restart local v16    # "object":Lcom/safonov/speedreading/training/fragment/wordscolumns/repository/entity/WordsColumnsConfig;
    move-object/from16 v0, p2

    move-object/from16 v1, v16

    invoke-interface {v0, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_0

    .line 586
    move-object/from16 v0, v16

    instance-of v6, v0, Lio/realm/internal/RealmObjectProxy;

    if-eqz v6, :cond_1

    move-object/from16 v6, v16

    check-cast v6, Lio/realm/internal/RealmObjectProxy;

    invoke-interface {v6}, Lio/realm/internal/RealmObjectProxy;->realmGet$proxyState()Lio/realm/ProxyState;

    move-result-object v6

    invoke-virtual {v6}, Lio/realm/ProxyState;->getRealm$realm()Lio/realm/BaseRealm;

    move-result-object v6

    if-eqz v6, :cond_1

    move-object/from16 v6, v16

    check-cast v6, Lio/realm/internal/RealmObjectProxy;

    invoke-interface {v6}, Lio/realm/internal/RealmObjectProxy;->realmGet$proxyState()Lio/realm/ProxyState;

    move-result-object v6

    invoke-virtual {v6}, Lio/realm/ProxyState;->getRealm$realm()Lio/realm/BaseRealm;

    move-result-object v6

    invoke-virtual {v6}, Lio/realm/BaseRealm;->getPath()Ljava/lang/String;

    move-result-object v6

    invoke-virtual/range {p0 .. p0}, Lio/realm/Realm;->getPath()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1

    move-object/from16 v6, v16

    .line 587
    check-cast v6, Lio/realm/internal/RealmObjectProxy;

    invoke-interface {v6}, Lio/realm/internal/RealmObjectProxy;->realmGet$proxyState()Lio/realm/ProxyState;

    move-result-object v6

    invoke-virtual {v6}, Lio/realm/ProxyState;->getRow$realm()Lio/realm/internal/Row;

    move-result-object v6

    invoke-interface {v6}, Lio/realm/internal/Row;->getIndex()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    move-object/from16 v0, p2

    move-object/from16 v1, v16

    invoke-interface {v0, v1, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 590
    :cond_1
    const-wide/16 v10, -0x1

    .local v10, "rowIndex":J
    move-object/from16 v6, v16

    .line 591
    check-cast v6, Lio/realm/WordsColumnsConfigRealmProxyInterface;

    invoke-interface {v6}, Lio/realm/WordsColumnsConfigRealmProxyInterface;->realmGet$id()I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v17

    .line 592
    .local v17, "primaryKeyValue":Ljava/lang/Integer;
    if-eqz v17, :cond_2

    move-object/from16 v6, v16

    .line 593
    check-cast v6, Lio/realm/WordsColumnsConfigRealmProxyInterface;

    invoke-interface {v6}, Lio/realm/WordsColumnsConfigRealmProxyInterface;->realmGet$id()I

    move-result v6

    int-to-long v6, v6

    invoke-static/range {v2 .. v7}, Lio/realm/internal/Table;->nativeFindFirstInt(JJJ)J

    move-result-wide v10

    .line 595
    :cond_2
    const-wide/16 v6, -0x1

    cmp-long v6, v10, v6

    if-nez v6, :cond_3

    move-object/from16 v6, v16

    .line 596
    check-cast v6, Lio/realm/WordsColumnsConfigRealmProxyInterface;

    invoke-interface {v6}, Lio/realm/WordsColumnsConfigRealmProxyInterface;->realmGet$id()I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    const/4 v7, 0x0

    move-object/from16 v0, v18

    invoke-virtual {v0, v6, v7}, Lio/realm/internal/Table;->addEmptyRowWithPrimaryKey(Ljava/lang/Object;Z)J

    move-result-wide v10

    .line 600
    :goto_1
    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    move-object/from16 v0, p2

    move-object/from16 v1, v16

    invoke-interface {v0, v1, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 601
    iget-wide v8, v15, Lio/realm/WordsColumnsConfigRealmProxy$WordsColumnsConfigColumnInfo;->rowCountIndex:J

    move-object/from16 v6, v16

    check-cast v6, Lio/realm/WordsColumnsConfigRealmProxyInterface;

    invoke-interface {v6}, Lio/realm/WordsColumnsConfigRealmProxyInterface;->realmGet$rowCount()I

    move-result v6

    int-to-long v12, v6

    const/4 v14, 0x0

    move-wide v6, v2

    invoke-static/range {v6 .. v14}, Lio/realm/internal/Table;->nativeSetLong(JJJJZ)V

    .line 602
    iget-wide v8, v15, Lio/realm/WordsColumnsConfigRealmProxy$WordsColumnsConfigColumnInfo;->columnCountIndex:J

    move-object/from16 v6, v16

    check-cast v6, Lio/realm/WordsColumnsConfigRealmProxyInterface;

    invoke-interface {v6}, Lio/realm/WordsColumnsConfigRealmProxyInterface;->realmGet$columnCount()I

    move-result v6

    int-to-long v12, v6

    const/4 v14, 0x0

    move-wide v6, v2

    invoke-static/range {v6 .. v14}, Lio/realm/internal/Table;->nativeSetLong(JJJJZ)V

    .line 603
    iget-wide v8, v15, Lio/realm/WordsColumnsConfigRealmProxy$WordsColumnsConfigColumnInfo;->wordsPerItemIndex:J

    move-object/from16 v6, v16

    check-cast v6, Lio/realm/WordsColumnsConfigRealmProxyInterface;

    invoke-interface {v6}, Lio/realm/WordsColumnsConfigRealmProxyInterface;->realmGet$wordsPerItem()I

    move-result v6

    int-to-long v12, v6

    const/4 v14, 0x0

    move-wide v6, v2

    invoke-static/range {v6 .. v14}, Lio/realm/internal/Table;->nativeSetLong(JJJJZ)V

    .line 604
    iget-wide v8, v15, Lio/realm/WordsColumnsConfigRealmProxy$WordsColumnsConfigColumnInfo;->speedIndex:J

    move-object/from16 v6, v16

    check-cast v6, Lio/realm/WordsColumnsConfigRealmProxyInterface;

    invoke-interface {v6}, Lio/realm/WordsColumnsConfigRealmProxyInterface;->realmGet$speed()I

    move-result v6

    int-to-long v12, v6

    const/4 v14, 0x0

    move-wide v6, v2

    invoke-static/range {v6 .. v14}, Lio/realm/internal/Table;->nativeSetLong(JJJJZ)V

    .line 605
    iget-wide v8, v15, Lio/realm/WordsColumnsConfigRealmProxy$WordsColumnsConfigColumnInfo;->trainingDurationIndex:J

    move-object/from16 v6, v16

    check-cast v6, Lio/realm/WordsColumnsConfigRealmProxyInterface;

    invoke-interface {v6}, Lio/realm/WordsColumnsConfigRealmProxyInterface;->realmGet$trainingDuration()J

    move-result-wide v12

    const/4 v14, 0x0

    move-wide v6, v2

    invoke-static/range {v6 .. v14}, Lio/realm/internal/Table;->nativeSetLong(JJJJZ)V

    goto/16 :goto_0

    .line 598
    :cond_3
    invoke-static/range {v17 .. v17}, Lio/realm/internal/Table;->throwDuplicatePrimaryKeyException(Ljava/lang/Object;)V

    goto :goto_1

    .line 608
    .end local v10    # "rowIndex":J
    .end local v17    # "primaryKeyValue":Ljava/lang/Integer;
    :cond_4
    return-void
.end method

.method public static insertOrUpdate(Lio/realm/Realm;Lcom/safonov/speedreading/training/fragment/wordscolumns/repository/entity/WordsColumnsConfig;Ljava/util/Map;)J
    .locals 18
    .param p0, "realm"    # Lio/realm/Realm;
    .param p1, "object"    # Lcom/safonov/speedreading/training/fragment/wordscolumns/repository/entity/WordsColumnsConfig;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/realm/Realm;",
            "Lcom/safonov/speedreading/training/fragment/wordscolumns/repository/entity/WordsColumnsConfig;",
            "Ljava/util/Map",
            "<",
            "Lio/realm/RealmModel;",
            "Ljava/lang/Long;",
            ">;)J"
        }
    .end annotation

    .prologue
    .line 611
    .local p2, "cache":Ljava/util/Map;, "Ljava/util/Map<Lio/realm/RealmModel;Ljava/lang/Long;>;"
    move-object/from16 v0, p1

    instance-of v6, v0, Lio/realm/internal/RealmObjectProxy;

    if-eqz v6, :cond_0

    move-object/from16 v6, p1

    check-cast v6, Lio/realm/internal/RealmObjectProxy;

    invoke-interface {v6}, Lio/realm/internal/RealmObjectProxy;->realmGet$proxyState()Lio/realm/ProxyState;

    move-result-object v6

    invoke-virtual {v6}, Lio/realm/ProxyState;->getRealm$realm()Lio/realm/BaseRealm;

    move-result-object v6

    if-eqz v6, :cond_0

    move-object/from16 v6, p1

    check-cast v6, Lio/realm/internal/RealmObjectProxy;

    invoke-interface {v6}, Lio/realm/internal/RealmObjectProxy;->realmGet$proxyState()Lio/realm/ProxyState;

    move-result-object v6

    invoke-virtual {v6}, Lio/realm/ProxyState;->getRealm$realm()Lio/realm/BaseRealm;

    move-result-object v6

    invoke-virtual {v6}, Lio/realm/BaseRealm;->getPath()Ljava/lang/String;

    move-result-object v6

    invoke-virtual/range {p0 .. p0}, Lio/realm/Realm;->getPath()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 612
    check-cast p1, Lio/realm/internal/RealmObjectProxy;

    .end local p1    # "object":Lcom/safonov/speedreading/training/fragment/wordscolumns/repository/entity/WordsColumnsConfig;
    invoke-interface/range {p1 .. p1}, Lio/realm/internal/RealmObjectProxy;->realmGet$proxyState()Lio/realm/ProxyState;

    move-result-object v6

    invoke-virtual {v6}, Lio/realm/ProxyState;->getRow$realm()Lio/realm/internal/Row;

    move-result-object v6

    invoke-interface {v6}, Lio/realm/internal/Row;->getIndex()J

    move-result-wide v10

    .line 632
    :goto_0
    return-wide v10

    .line 614
    .restart local p1    # "object":Lcom/safonov/speedreading/training/fragment/wordscolumns/repository/entity/WordsColumnsConfig;
    :cond_0
    const-class v6, Lcom/safonov/speedreading/training/fragment/wordscolumns/repository/entity/WordsColumnsConfig;

    move-object/from16 v0, p0

    invoke-virtual {v0, v6}, Lio/realm/Realm;->getTable(Ljava/lang/Class;)Lio/realm/internal/Table;

    move-result-object v17

    .line 615
    .local v17, "table":Lio/realm/internal/Table;
    invoke-virtual/range {v17 .. v17}, Lio/realm/internal/Table;->getNativeTablePointer()J

    move-result-wide v2

    .line 616
    .local v2, "tableNativePtr":J
    move-object/from16 v0, p0

    iget-object v6, v0, Lio/realm/Realm;->schema:Lio/realm/RealmSchema;

    const-class v7, Lcom/safonov/speedreading/training/fragment/wordscolumns/repository/entity/WordsColumnsConfig;

    invoke-virtual {v6, v7}, Lio/realm/RealmSchema;->getColumnInfo(Ljava/lang/Class;)Lio/realm/internal/ColumnInfo;

    move-result-object v15

    check-cast v15, Lio/realm/WordsColumnsConfigRealmProxy$WordsColumnsConfigColumnInfo;

    .line 617
    .local v15, "columnInfo":Lio/realm/WordsColumnsConfigRealmProxy$WordsColumnsConfigColumnInfo;
    invoke-virtual/range {v17 .. v17}, Lio/realm/internal/Table;->getPrimaryKey()J

    move-result-wide v4

    .line 618
    .local v4, "pkColumnIndex":J
    const-wide/16 v10, -0x1

    .local v10, "rowIndex":J
    move-object/from16 v6, p1

    .line 619
    check-cast v6, Lio/realm/WordsColumnsConfigRealmProxyInterface;

    invoke-interface {v6}, Lio/realm/WordsColumnsConfigRealmProxyInterface;->realmGet$id()I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v16

    .line 620
    .local v16, "primaryKeyValue":Ljava/lang/Integer;
    if-eqz v16, :cond_1

    move-object/from16 v6, p1

    .line 621
    check-cast v6, Lio/realm/WordsColumnsConfigRealmProxyInterface;

    invoke-interface {v6}, Lio/realm/WordsColumnsConfigRealmProxyInterface;->realmGet$id()I

    move-result v6

    int-to-long v6, v6

    invoke-static/range {v2 .. v7}, Lio/realm/internal/Table;->nativeFindFirstInt(JJJ)J

    move-result-wide v10

    .line 623
    :cond_1
    const-wide/16 v6, -0x1

    cmp-long v6, v10, v6

    if-nez v6, :cond_2

    move-object/from16 v6, p1

    .line 624
    check-cast v6, Lio/realm/WordsColumnsConfigRealmProxyInterface;

    invoke-interface {v6}, Lio/realm/WordsColumnsConfigRealmProxyInterface;->realmGet$id()I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    const/4 v7, 0x0

    move-object/from16 v0, v17

    invoke-virtual {v0, v6, v7}, Lio/realm/internal/Table;->addEmptyRowWithPrimaryKey(Ljava/lang/Object;Z)J

    move-result-wide v10

    .line 626
    :cond_2
    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    move-object/from16 v0, p2

    move-object/from16 v1, p1

    invoke-interface {v0, v1, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 627
    iget-wide v8, v15, Lio/realm/WordsColumnsConfigRealmProxy$WordsColumnsConfigColumnInfo;->rowCountIndex:J

    move-object/from16 v6, p1

    check-cast v6, Lio/realm/WordsColumnsConfigRealmProxyInterface;

    invoke-interface {v6}, Lio/realm/WordsColumnsConfigRealmProxyInterface;->realmGet$rowCount()I

    move-result v6

    int-to-long v12, v6

    const/4 v14, 0x0

    move-wide v6, v2

    invoke-static/range {v6 .. v14}, Lio/realm/internal/Table;->nativeSetLong(JJJJZ)V

    .line 628
    iget-wide v8, v15, Lio/realm/WordsColumnsConfigRealmProxy$WordsColumnsConfigColumnInfo;->columnCountIndex:J

    move-object/from16 v6, p1

    check-cast v6, Lio/realm/WordsColumnsConfigRealmProxyInterface;

    invoke-interface {v6}, Lio/realm/WordsColumnsConfigRealmProxyInterface;->realmGet$columnCount()I

    move-result v6

    int-to-long v12, v6

    const/4 v14, 0x0

    move-wide v6, v2

    invoke-static/range {v6 .. v14}, Lio/realm/internal/Table;->nativeSetLong(JJJJZ)V

    .line 629
    iget-wide v8, v15, Lio/realm/WordsColumnsConfigRealmProxy$WordsColumnsConfigColumnInfo;->wordsPerItemIndex:J

    move-object/from16 v6, p1

    check-cast v6, Lio/realm/WordsColumnsConfigRealmProxyInterface;

    invoke-interface {v6}, Lio/realm/WordsColumnsConfigRealmProxyInterface;->realmGet$wordsPerItem()I

    move-result v6

    int-to-long v12, v6

    const/4 v14, 0x0

    move-wide v6, v2

    invoke-static/range {v6 .. v14}, Lio/realm/internal/Table;->nativeSetLong(JJJJZ)V

    .line 630
    iget-wide v8, v15, Lio/realm/WordsColumnsConfigRealmProxy$WordsColumnsConfigColumnInfo;->speedIndex:J

    move-object/from16 v6, p1

    check-cast v6, Lio/realm/WordsColumnsConfigRealmProxyInterface;

    invoke-interface {v6}, Lio/realm/WordsColumnsConfigRealmProxyInterface;->realmGet$speed()I

    move-result v6

    int-to-long v12, v6

    const/4 v14, 0x0

    move-wide v6, v2

    invoke-static/range {v6 .. v14}, Lio/realm/internal/Table;->nativeSetLong(JJJJZ)V

    .line 631
    iget-wide v8, v15, Lio/realm/WordsColumnsConfigRealmProxy$WordsColumnsConfigColumnInfo;->trainingDurationIndex:J

    check-cast p1, Lio/realm/WordsColumnsConfigRealmProxyInterface;

    .end local p1    # "object":Lcom/safonov/speedreading/training/fragment/wordscolumns/repository/entity/WordsColumnsConfig;
    invoke-interface/range {p1 .. p1}, Lio/realm/WordsColumnsConfigRealmProxyInterface;->realmGet$trainingDuration()J

    move-result-wide v12

    const/4 v14, 0x0

    move-wide v6, v2

    invoke-static/range {v6 .. v14}, Lio/realm/internal/Table;->nativeSetLong(JJJJZ)V

    goto/16 :goto_0
.end method

.method public static insertOrUpdate(Lio/realm/Realm;Ljava/util/Iterator;Ljava/util/Map;)V
    .locals 19
    .param p0, "realm"    # Lio/realm/Realm;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/realm/Realm;",
            "Ljava/util/Iterator",
            "<+",
            "Lio/realm/RealmModel;",
            ">;",
            "Ljava/util/Map",
            "<",
            "Lio/realm/RealmModel;",
            "Ljava/lang/Long;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 636
    .local p1, "objects":Ljava/util/Iterator;, "Ljava/util/Iterator<+Lio/realm/RealmModel;>;"
    .local p2, "cache":Ljava/util/Map;, "Ljava/util/Map<Lio/realm/RealmModel;Ljava/lang/Long;>;"
    const-class v6, Lcom/safonov/speedreading/training/fragment/wordscolumns/repository/entity/WordsColumnsConfig;

    move-object/from16 v0, p0

    invoke-virtual {v0, v6}, Lio/realm/Realm;->getTable(Ljava/lang/Class;)Lio/realm/internal/Table;

    move-result-object v18

    .line 637
    .local v18, "table":Lio/realm/internal/Table;
    invoke-virtual/range {v18 .. v18}, Lio/realm/internal/Table;->getNativeTablePointer()J

    move-result-wide v2

    .line 638
    .local v2, "tableNativePtr":J
    move-object/from16 v0, p0

    iget-object v6, v0, Lio/realm/Realm;->schema:Lio/realm/RealmSchema;

    const-class v7, Lcom/safonov/speedreading/training/fragment/wordscolumns/repository/entity/WordsColumnsConfig;

    invoke-virtual {v6, v7}, Lio/realm/RealmSchema;->getColumnInfo(Ljava/lang/Class;)Lio/realm/internal/ColumnInfo;

    move-result-object v15

    check-cast v15, Lio/realm/WordsColumnsConfigRealmProxy$WordsColumnsConfigColumnInfo;

    .line 639
    .local v15, "columnInfo":Lio/realm/WordsColumnsConfigRealmProxy$WordsColumnsConfigColumnInfo;
    invoke-virtual/range {v18 .. v18}, Lio/realm/internal/Table;->getPrimaryKey()J

    move-result-wide v4

    .line 640
    .local v4, "pkColumnIndex":J
    const/16 v16, 0x0

    .line 641
    .local v16, "object":Lcom/safonov/speedreading/training/fragment/wordscolumns/repository/entity/WordsColumnsConfig;
    :cond_0
    :goto_0
    invoke-interface/range {p1 .. p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_4

    .line 642
    invoke-interface/range {p1 .. p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v16

    .end local v16    # "object":Lcom/safonov/speedreading/training/fragment/wordscolumns/repository/entity/WordsColumnsConfig;
    check-cast v16, Lcom/safonov/speedreading/training/fragment/wordscolumns/repository/entity/WordsColumnsConfig;

    .line 643
    .restart local v16    # "object":Lcom/safonov/speedreading/training/fragment/wordscolumns/repository/entity/WordsColumnsConfig;
    move-object/from16 v0, p2

    move-object/from16 v1, v16

    invoke-interface {v0, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_0

    .line 644
    move-object/from16 v0, v16

    instance-of v6, v0, Lio/realm/internal/RealmObjectProxy;

    if-eqz v6, :cond_1

    move-object/from16 v6, v16

    check-cast v6, Lio/realm/internal/RealmObjectProxy;

    invoke-interface {v6}, Lio/realm/internal/RealmObjectProxy;->realmGet$proxyState()Lio/realm/ProxyState;

    move-result-object v6

    invoke-virtual {v6}, Lio/realm/ProxyState;->getRealm$realm()Lio/realm/BaseRealm;

    move-result-object v6

    if-eqz v6, :cond_1

    move-object/from16 v6, v16

    check-cast v6, Lio/realm/internal/RealmObjectProxy;

    invoke-interface {v6}, Lio/realm/internal/RealmObjectProxy;->realmGet$proxyState()Lio/realm/ProxyState;

    move-result-object v6

    invoke-virtual {v6}, Lio/realm/ProxyState;->getRealm$realm()Lio/realm/BaseRealm;

    move-result-object v6

    invoke-virtual {v6}, Lio/realm/BaseRealm;->getPath()Ljava/lang/String;

    move-result-object v6

    invoke-virtual/range {p0 .. p0}, Lio/realm/Realm;->getPath()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1

    move-object/from16 v6, v16

    .line 645
    check-cast v6, Lio/realm/internal/RealmObjectProxy;

    invoke-interface {v6}, Lio/realm/internal/RealmObjectProxy;->realmGet$proxyState()Lio/realm/ProxyState;

    move-result-object v6

    invoke-virtual {v6}, Lio/realm/ProxyState;->getRow$realm()Lio/realm/internal/Row;

    move-result-object v6

    invoke-interface {v6}, Lio/realm/internal/Row;->getIndex()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    move-object/from16 v0, p2

    move-object/from16 v1, v16

    invoke-interface {v0, v1, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 648
    :cond_1
    const-wide/16 v10, -0x1

    .local v10, "rowIndex":J
    move-object/from16 v6, v16

    .line 649
    check-cast v6, Lio/realm/WordsColumnsConfigRealmProxyInterface;

    invoke-interface {v6}, Lio/realm/WordsColumnsConfigRealmProxyInterface;->realmGet$id()I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v17

    .line 650
    .local v17, "primaryKeyValue":Ljava/lang/Integer;
    if-eqz v17, :cond_2

    move-object/from16 v6, v16

    .line 651
    check-cast v6, Lio/realm/WordsColumnsConfigRealmProxyInterface;

    invoke-interface {v6}, Lio/realm/WordsColumnsConfigRealmProxyInterface;->realmGet$id()I

    move-result v6

    int-to-long v6, v6

    invoke-static/range {v2 .. v7}, Lio/realm/internal/Table;->nativeFindFirstInt(JJJ)J

    move-result-wide v10

    .line 653
    :cond_2
    const-wide/16 v6, -0x1

    cmp-long v6, v10, v6

    if-nez v6, :cond_3

    move-object/from16 v6, v16

    .line 654
    check-cast v6, Lio/realm/WordsColumnsConfigRealmProxyInterface;

    invoke-interface {v6}, Lio/realm/WordsColumnsConfigRealmProxyInterface;->realmGet$id()I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    const/4 v7, 0x0

    move-object/from16 v0, v18

    invoke-virtual {v0, v6, v7}, Lio/realm/internal/Table;->addEmptyRowWithPrimaryKey(Ljava/lang/Object;Z)J

    move-result-wide v10

    .line 656
    :cond_3
    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    move-object/from16 v0, p2

    move-object/from16 v1, v16

    invoke-interface {v0, v1, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 657
    iget-wide v8, v15, Lio/realm/WordsColumnsConfigRealmProxy$WordsColumnsConfigColumnInfo;->rowCountIndex:J

    move-object/from16 v6, v16

    check-cast v6, Lio/realm/WordsColumnsConfigRealmProxyInterface;

    invoke-interface {v6}, Lio/realm/WordsColumnsConfigRealmProxyInterface;->realmGet$rowCount()I

    move-result v6

    int-to-long v12, v6

    const/4 v14, 0x0

    move-wide v6, v2

    invoke-static/range {v6 .. v14}, Lio/realm/internal/Table;->nativeSetLong(JJJJZ)V

    .line 658
    iget-wide v8, v15, Lio/realm/WordsColumnsConfigRealmProxy$WordsColumnsConfigColumnInfo;->columnCountIndex:J

    move-object/from16 v6, v16

    check-cast v6, Lio/realm/WordsColumnsConfigRealmProxyInterface;

    invoke-interface {v6}, Lio/realm/WordsColumnsConfigRealmProxyInterface;->realmGet$columnCount()I

    move-result v6

    int-to-long v12, v6

    const/4 v14, 0x0

    move-wide v6, v2

    invoke-static/range {v6 .. v14}, Lio/realm/internal/Table;->nativeSetLong(JJJJZ)V

    .line 659
    iget-wide v8, v15, Lio/realm/WordsColumnsConfigRealmProxy$WordsColumnsConfigColumnInfo;->wordsPerItemIndex:J

    move-object/from16 v6, v16

    check-cast v6, Lio/realm/WordsColumnsConfigRealmProxyInterface;

    invoke-interface {v6}, Lio/realm/WordsColumnsConfigRealmProxyInterface;->realmGet$wordsPerItem()I

    move-result v6

    int-to-long v12, v6

    const/4 v14, 0x0

    move-wide v6, v2

    invoke-static/range {v6 .. v14}, Lio/realm/internal/Table;->nativeSetLong(JJJJZ)V

    .line 660
    iget-wide v8, v15, Lio/realm/WordsColumnsConfigRealmProxy$WordsColumnsConfigColumnInfo;->speedIndex:J

    move-object/from16 v6, v16

    check-cast v6, Lio/realm/WordsColumnsConfigRealmProxyInterface;

    invoke-interface {v6}, Lio/realm/WordsColumnsConfigRealmProxyInterface;->realmGet$speed()I

    move-result v6

    int-to-long v12, v6

    const/4 v14, 0x0

    move-wide v6, v2

    invoke-static/range {v6 .. v14}, Lio/realm/internal/Table;->nativeSetLong(JJJJZ)V

    .line 661
    iget-wide v8, v15, Lio/realm/WordsColumnsConfigRealmProxy$WordsColumnsConfigColumnInfo;->trainingDurationIndex:J

    move-object/from16 v6, v16

    check-cast v6, Lio/realm/WordsColumnsConfigRealmProxyInterface;

    invoke-interface {v6}, Lio/realm/WordsColumnsConfigRealmProxyInterface;->realmGet$trainingDuration()J

    move-result-wide v12

    const/4 v14, 0x0

    move-wide v6, v2

    invoke-static/range {v6 .. v14}, Lio/realm/internal/Table;->nativeSetLong(JJJJZ)V

    goto/16 :goto_0

    .line 664
    .end local v10    # "rowIndex":J
    .end local v17    # "primaryKeyValue":Ljava/lang/Integer;
    :cond_4
    return-void
.end method

.method static update(Lio/realm/Realm;Lcom/safonov/speedreading/training/fragment/wordscolumns/repository/entity/WordsColumnsConfig;Lcom/safonov/speedreading/training/fragment/wordscolumns/repository/entity/WordsColumnsConfig;Ljava/util/Map;)Lcom/safonov/speedreading/training/fragment/wordscolumns/repository/entity/WordsColumnsConfig;
    .locals 4
    .param p0, "realm"    # Lio/realm/Realm;
    .param p1, "realmObject"    # Lcom/safonov/speedreading/training/fragment/wordscolumns/repository/entity/WordsColumnsConfig;
    .param p2, "newObject"    # Lcom/safonov/speedreading/training/fragment/wordscolumns/repository/entity/WordsColumnsConfig;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/realm/Realm;",
            "Lcom/safonov/speedreading/training/fragment/wordscolumns/repository/entity/WordsColumnsConfig;",
            "Lcom/safonov/speedreading/training/fragment/wordscolumns/repository/entity/WordsColumnsConfig;",
            "Ljava/util/Map",
            "<",
            "Lio/realm/RealmModel;",
            "Lio/realm/internal/RealmObjectProxy;",
            ">;)",
            "Lcom/safonov/speedreading/training/fragment/wordscolumns/repository/entity/WordsColumnsConfig;"
        }
    .end annotation

    .prologue
    .line 694
    .local p3, "cache":Ljava/util/Map;, "Ljava/util/Map<Lio/realm/RealmModel;Lio/realm/internal/RealmObjectProxy;>;"
    move-object v0, p1

    check-cast v0, Lio/realm/WordsColumnsConfigRealmProxyInterface;

    move-object v1, p2

    check-cast v1, Lio/realm/WordsColumnsConfigRealmProxyInterface;

    invoke-interface {v1}, Lio/realm/WordsColumnsConfigRealmProxyInterface;->realmGet$rowCount()I

    move-result v1

    invoke-interface {v0, v1}, Lio/realm/WordsColumnsConfigRealmProxyInterface;->realmSet$rowCount(I)V

    move-object v0, p1

    .line 695
    check-cast v0, Lio/realm/WordsColumnsConfigRealmProxyInterface;

    move-object v1, p2

    check-cast v1, Lio/realm/WordsColumnsConfigRealmProxyInterface;

    invoke-interface {v1}, Lio/realm/WordsColumnsConfigRealmProxyInterface;->realmGet$columnCount()I

    move-result v1

    invoke-interface {v0, v1}, Lio/realm/WordsColumnsConfigRealmProxyInterface;->realmSet$columnCount(I)V

    move-object v0, p1

    .line 696
    check-cast v0, Lio/realm/WordsColumnsConfigRealmProxyInterface;

    move-object v1, p2

    check-cast v1, Lio/realm/WordsColumnsConfigRealmProxyInterface;

    invoke-interface {v1}, Lio/realm/WordsColumnsConfigRealmProxyInterface;->realmGet$wordsPerItem()I

    move-result v1

    invoke-interface {v0, v1}, Lio/realm/WordsColumnsConfigRealmProxyInterface;->realmSet$wordsPerItem(I)V

    move-object v0, p1

    .line 697
    check-cast v0, Lio/realm/WordsColumnsConfigRealmProxyInterface;

    move-object v1, p2

    check-cast v1, Lio/realm/WordsColumnsConfigRealmProxyInterface;

    invoke-interface {v1}, Lio/realm/WordsColumnsConfigRealmProxyInterface;->realmGet$speed()I

    move-result v1

    invoke-interface {v0, v1}, Lio/realm/WordsColumnsConfigRealmProxyInterface;->realmSet$speed(I)V

    move-object v0, p1

    .line 698
    check-cast v0, Lio/realm/WordsColumnsConfigRealmProxyInterface;

    check-cast p2, Lio/realm/WordsColumnsConfigRealmProxyInterface;

    .end local p2    # "newObject":Lcom/safonov/speedreading/training/fragment/wordscolumns/repository/entity/WordsColumnsConfig;
    invoke-interface {p2}, Lio/realm/WordsColumnsConfigRealmProxyInterface;->realmGet$trainingDuration()J

    move-result-wide v2

    invoke-interface {v0, v2, v3}, Lio/realm/WordsColumnsConfigRealmProxyInterface;->realmSet$trainingDuration(J)V

    .line 699
    return-object p1
.end method

.method public static validateTable(Lio/realm/internal/SharedRealm;Z)Lio/realm/WordsColumnsConfigRealmProxy$WordsColumnsConfigColumnInfo;
    .locals 12
    .param p0, "sharedRealm"    # Lio/realm/internal/SharedRealm;
    .param p1, "allowExtraColumns"    # Z

    .prologue
    const-wide/16 v8, 0x6

    .line 260
    const-string v7, "class_WordsColumnsConfig"

    invoke-virtual {p0, v7}, Lio/realm/internal/SharedRealm;->hasTable(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_18

    .line 261
    const-string v7, "class_WordsColumnsConfig"

    invoke-virtual {p0, v7}, Lio/realm/internal/SharedRealm;->getTable(Ljava/lang/String;)Lio/realm/internal/Table;

    move-result-object v6

    .line 262
    .local v6, "table":Lio/realm/internal/Table;
    invoke-virtual {v6}, Lio/realm/internal/Table;->getColumnCount()J

    move-result-wide v0

    .line 263
    .local v0, "columnCount":J
    cmp-long v7, v0, v8

    if-eqz v7, :cond_1

    .line 264
    cmp-long v7, v0, v8

    if-gez v7, :cond_0

    .line 265
    new-instance v7, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->getPath()Ljava/lang/String;

    move-result-object v8

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Field count is less than expected - expected 6 but was "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-direct {v7, v8, v9}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v7

    .line 267
    :cond_0
    if-eqz p1, :cond_2

    .line 268
    const-string v7, "Field count is more than expected - expected 6 but was %1$d"

    const/4 v8, 0x1

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v10

    aput-object v10, v8, v9

    invoke-static {v7, v8}, Lio/realm/log/RealmLog;->debug(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 273
    :cond_1
    new-instance v3, Ljava/util/HashMap;

    invoke-direct {v3}, Ljava/util/HashMap;-><init>()V

    .line 274
    .local v3, "columnTypes":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lio/realm/RealmFieldType;>;"
    const-wide/16 v4, 0x0

    .local v4, "i":J
    :goto_0
    cmp-long v7, v4, v0

    if-gez v7, :cond_3

    .line 275
    invoke-virtual {v6, v4, v5}, Lio/realm/internal/Table;->getColumnName(J)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v4, v5}, Lio/realm/internal/Table;->getColumnType(J)Lio/realm/RealmFieldType;

    move-result-object v8

    invoke-interface {v3, v7, v8}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 274
    const-wide/16 v8, 0x1

    add-long/2addr v4, v8

    goto :goto_0

    .line 270
    .end local v3    # "columnTypes":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lio/realm/RealmFieldType;>;"
    .end local v4    # "i":J
    :cond_2
    new-instance v7, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->getPath()Ljava/lang/String;

    move-result-object v8

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Field count is more than expected - expected 6 but was "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-direct {v7, v8, v9}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v7

    .line 278
    .restart local v3    # "columnTypes":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lio/realm/RealmFieldType;>;"
    .restart local v4    # "i":J
    :cond_3
    new-instance v2, Lio/realm/WordsColumnsConfigRealmProxy$WordsColumnsConfigColumnInfo;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->getPath()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v2, v7, v6}, Lio/realm/WordsColumnsConfigRealmProxy$WordsColumnsConfigColumnInfo;-><init>(Ljava/lang/String;Lio/realm/internal/Table;)V

    .line 280
    .local v2, "columnInfo":Lio/realm/WordsColumnsConfigRealmProxy$WordsColumnsConfigColumnInfo;
    invoke-virtual {v6}, Lio/realm/internal/Table;->hasPrimaryKey()Z

    move-result v7

    if-nez v7, :cond_4

    .line 281
    new-instance v7, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->getPath()Ljava/lang/String;

    move-result-object v8

    const-string v9, "Primary key not defined for field \'id\' in existing Realm file. @PrimaryKey was added."

    invoke-direct {v7, v8, v9}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v7

    .line 283
    :cond_4
    invoke-virtual {v6}, Lio/realm/internal/Table;->getPrimaryKey()J

    move-result-wide v8

    iget-wide v10, v2, Lio/realm/WordsColumnsConfigRealmProxy$WordsColumnsConfigColumnInfo;->idIndex:J

    cmp-long v7, v8, v10

    if-eqz v7, :cond_5

    .line 284
    new-instance v7, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->getPath()Ljava/lang/String;

    move-result-object v8

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Primary Key annotation definition was changed, from field "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v6}, Lio/realm/internal/Table;->getPrimaryKey()J

    move-result-wide v10

    invoke-virtual {v6, v10, v11}, Lio/realm/internal/Table;->getColumnName(J)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " to field id"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-direct {v7, v8, v9}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v7

    .line 288
    :cond_5
    const-string v7, "id"

    invoke-interface {v3, v7}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_6

    .line 289
    new-instance v7, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->getPath()Ljava/lang/String;

    move-result-object v8

    const-string v9, "Missing field \'id\' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn()."

    invoke-direct {v7, v8, v9}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v7

    .line 291
    :cond_6
    const-string v7, "id"

    invoke-interface {v3, v7}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    sget-object v8, Lio/realm/RealmFieldType;->INTEGER:Lio/realm/RealmFieldType;

    if-eq v7, v8, :cond_7

    .line 292
    new-instance v7, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->getPath()Ljava/lang/String;

    move-result-object v8

    const-string v9, "Invalid type \'int\' for field \'id\' in existing Realm file."

    invoke-direct {v7, v8, v9}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v7

    .line 294
    :cond_7
    iget-wide v8, v2, Lio/realm/WordsColumnsConfigRealmProxy$WordsColumnsConfigColumnInfo;->idIndex:J

    invoke-virtual {v6, v8, v9}, Lio/realm/internal/Table;->isColumnNullable(J)Z

    move-result v7

    if-eqz v7, :cond_8

    iget-wide v8, v2, Lio/realm/WordsColumnsConfigRealmProxy$WordsColumnsConfigColumnInfo;->idIndex:J

    invoke-virtual {v6, v8, v9}, Lio/realm/internal/Table;->findFirstNull(J)J

    move-result-wide v8

    const-wide/16 v10, -0x1

    cmp-long v7, v8, v10

    if-eqz v7, :cond_8

    .line 295
    new-instance v7, Ljava/lang/IllegalStateException;

    const-string v8, "Cannot migrate an object with null value in field \'id\'. Either maintain the same type for primary key field \'id\', or remove the object with null value before migration."

    invoke-direct {v7, v8}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v7

    .line 297
    :cond_8
    const-string v7, "id"

    invoke-virtual {v6, v7}, Lio/realm/internal/Table;->getColumnIndex(Ljava/lang/String;)J

    move-result-wide v8

    invoke-virtual {v6, v8, v9}, Lio/realm/internal/Table;->hasSearchIndex(J)Z

    move-result v7

    if-nez v7, :cond_9

    .line 298
    new-instance v7, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->getPath()Ljava/lang/String;

    move-result-object v8

    const-string v9, "Index not defined for field \'id\' in existing Realm file. Either set @Index or migrate using io.realm.internal.Table.removeSearchIndex()."

    invoke-direct {v7, v8, v9}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v7

    .line 300
    :cond_9
    const-string v7, "rowCount"

    invoke-interface {v3, v7}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_a

    .line 301
    new-instance v7, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->getPath()Ljava/lang/String;

    move-result-object v8

    const-string v9, "Missing field \'rowCount\' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn()."

    invoke-direct {v7, v8, v9}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v7

    .line 303
    :cond_a
    const-string v7, "rowCount"

    invoke-interface {v3, v7}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    sget-object v8, Lio/realm/RealmFieldType;->INTEGER:Lio/realm/RealmFieldType;

    if-eq v7, v8, :cond_b

    .line 304
    new-instance v7, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->getPath()Ljava/lang/String;

    move-result-object v8

    const-string v9, "Invalid type \'int\' for field \'rowCount\' in existing Realm file."

    invoke-direct {v7, v8, v9}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v7

    .line 306
    :cond_b
    iget-wide v8, v2, Lio/realm/WordsColumnsConfigRealmProxy$WordsColumnsConfigColumnInfo;->rowCountIndex:J

    invoke-virtual {v6, v8, v9}, Lio/realm/internal/Table;->isColumnNullable(J)Z

    move-result v7

    if-eqz v7, :cond_c

    .line 307
    new-instance v7, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->getPath()Ljava/lang/String;

    move-result-object v8

    const-string v9, "Field \'rowCount\' does support null values in the existing Realm file. Use corresponding boxed type for field \'rowCount\' or migrate using RealmObjectSchema.setNullable()."

    invoke-direct {v7, v8, v9}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v7

    .line 309
    :cond_c
    const-string v7, "columnCount"

    invoke-interface {v3, v7}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_d

    .line 310
    new-instance v7, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->getPath()Ljava/lang/String;

    move-result-object v8

    const-string v9, "Missing field \'columnCount\' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn()."

    invoke-direct {v7, v8, v9}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v7

    .line 312
    :cond_d
    const-string v7, "columnCount"

    invoke-interface {v3, v7}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    sget-object v8, Lio/realm/RealmFieldType;->INTEGER:Lio/realm/RealmFieldType;

    if-eq v7, v8, :cond_e

    .line 313
    new-instance v7, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->getPath()Ljava/lang/String;

    move-result-object v8

    const-string v9, "Invalid type \'int\' for field \'columnCount\' in existing Realm file."

    invoke-direct {v7, v8, v9}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v7

    .line 315
    :cond_e
    iget-wide v8, v2, Lio/realm/WordsColumnsConfigRealmProxy$WordsColumnsConfigColumnInfo;->columnCountIndex:J

    invoke-virtual {v6, v8, v9}, Lio/realm/internal/Table;->isColumnNullable(J)Z

    move-result v7

    if-eqz v7, :cond_f

    .line 316
    new-instance v7, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->getPath()Ljava/lang/String;

    move-result-object v8

    const-string v9, "Field \'columnCount\' does support null values in the existing Realm file. Use corresponding boxed type for field \'columnCount\' or migrate using RealmObjectSchema.setNullable()."

    invoke-direct {v7, v8, v9}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v7

    .line 318
    :cond_f
    const-string v7, "wordsPerItem"

    invoke-interface {v3, v7}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_10

    .line 319
    new-instance v7, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->getPath()Ljava/lang/String;

    move-result-object v8

    const-string v9, "Missing field \'wordsPerItem\' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn()."

    invoke-direct {v7, v8, v9}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v7

    .line 321
    :cond_10
    const-string v7, "wordsPerItem"

    invoke-interface {v3, v7}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    sget-object v8, Lio/realm/RealmFieldType;->INTEGER:Lio/realm/RealmFieldType;

    if-eq v7, v8, :cond_11

    .line 322
    new-instance v7, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->getPath()Ljava/lang/String;

    move-result-object v8

    const-string v9, "Invalid type \'int\' for field \'wordsPerItem\' in existing Realm file."

    invoke-direct {v7, v8, v9}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v7

    .line 324
    :cond_11
    iget-wide v8, v2, Lio/realm/WordsColumnsConfigRealmProxy$WordsColumnsConfigColumnInfo;->wordsPerItemIndex:J

    invoke-virtual {v6, v8, v9}, Lio/realm/internal/Table;->isColumnNullable(J)Z

    move-result v7

    if-eqz v7, :cond_12

    .line 325
    new-instance v7, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->getPath()Ljava/lang/String;

    move-result-object v8

    const-string v9, "Field \'wordsPerItem\' does support null values in the existing Realm file. Use corresponding boxed type for field \'wordsPerItem\' or migrate using RealmObjectSchema.setNullable()."

    invoke-direct {v7, v8, v9}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v7

    .line 327
    :cond_12
    const-string v7, "speed"

    invoke-interface {v3, v7}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_13

    .line 328
    new-instance v7, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->getPath()Ljava/lang/String;

    move-result-object v8

    const-string v9, "Missing field \'speed\' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn()."

    invoke-direct {v7, v8, v9}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v7

    .line 330
    :cond_13
    const-string v7, "speed"

    invoke-interface {v3, v7}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    sget-object v8, Lio/realm/RealmFieldType;->INTEGER:Lio/realm/RealmFieldType;

    if-eq v7, v8, :cond_14

    .line 331
    new-instance v7, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->getPath()Ljava/lang/String;

    move-result-object v8

    const-string v9, "Invalid type \'int\' for field \'speed\' in existing Realm file."

    invoke-direct {v7, v8, v9}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v7

    .line 333
    :cond_14
    iget-wide v8, v2, Lio/realm/WordsColumnsConfigRealmProxy$WordsColumnsConfigColumnInfo;->speedIndex:J

    invoke-virtual {v6, v8, v9}, Lio/realm/internal/Table;->isColumnNullable(J)Z

    move-result v7

    if-eqz v7, :cond_15

    .line 334
    new-instance v7, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->getPath()Ljava/lang/String;

    move-result-object v8

    const-string v9, "Field \'speed\' does support null values in the existing Realm file. Use corresponding boxed type for field \'speed\' or migrate using RealmObjectSchema.setNullable()."

    invoke-direct {v7, v8, v9}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v7

    .line 336
    :cond_15
    const-string v7, "trainingDuration"

    invoke-interface {v3, v7}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_16

    .line 337
    new-instance v7, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->getPath()Ljava/lang/String;

    move-result-object v8

    const-string v9, "Missing field \'trainingDuration\' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn()."

    invoke-direct {v7, v8, v9}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v7

    .line 339
    :cond_16
    const-string v7, "trainingDuration"

    invoke-interface {v3, v7}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    sget-object v8, Lio/realm/RealmFieldType;->INTEGER:Lio/realm/RealmFieldType;

    if-eq v7, v8, :cond_17

    .line 340
    new-instance v7, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->getPath()Ljava/lang/String;

    move-result-object v8

    const-string v9, "Invalid type \'long\' for field \'trainingDuration\' in existing Realm file."

    invoke-direct {v7, v8, v9}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v7

    .line 342
    :cond_17
    iget-wide v8, v2, Lio/realm/WordsColumnsConfigRealmProxy$WordsColumnsConfigColumnInfo;->trainingDurationIndex:J

    invoke-virtual {v6, v8, v9}, Lio/realm/internal/Table;->isColumnNullable(J)Z

    move-result v7

    if-eqz v7, :cond_19

    .line 343
    new-instance v7, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->getPath()Ljava/lang/String;

    move-result-object v8

    const-string v9, "Field \'trainingDuration\' does support null values in the existing Realm file. Use corresponding boxed type for field \'trainingDuration\' or migrate using RealmObjectSchema.setNullable()."

    invoke-direct {v7, v8, v9}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v7

    .line 347
    .end local v0    # "columnCount":J
    .end local v2    # "columnInfo":Lio/realm/WordsColumnsConfigRealmProxy$WordsColumnsConfigColumnInfo;
    .end local v3    # "columnTypes":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lio/realm/RealmFieldType;>;"
    .end local v4    # "i":J
    .end local v6    # "table":Lio/realm/internal/Table;
    :cond_18
    new-instance v7, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->getPath()Ljava/lang/String;

    move-result-object v8

    const-string v9, "The \'WordsColumnsConfig\' class is missing from the schema for this Realm."

    invoke-direct {v7, v8, v9}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v7

    .line 345
    .restart local v0    # "columnCount":J
    .restart local v2    # "columnInfo":Lio/realm/WordsColumnsConfigRealmProxy$WordsColumnsConfigColumnInfo;
    .restart local v3    # "columnTypes":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lio/realm/RealmFieldType;>;"
    .restart local v4    # "i":J
    .restart local v6    # "table":Lio/realm/internal/Table;
    :cond_19
    return-object v2
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 12
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v5, 0x1

    const/4 v6, 0x0

    .line 755
    if-ne p0, p1, :cond_1

    .line 769
    :cond_0
    :goto_0
    return v5

    .line 756
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v7

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v8

    if-eq v7, v8, :cond_3

    :cond_2
    move v5, v6

    goto :goto_0

    :cond_3
    move-object v0, p1

    .line 757
    check-cast v0, Lio/realm/WordsColumnsConfigRealmProxy;

    .line 759
    .local v0, "aWordsColumnsConfig":Lio/realm/WordsColumnsConfigRealmProxy;
    iget-object v7, p0, Lio/realm/WordsColumnsConfigRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v7}, Lio/realm/ProxyState;->getRealm$realm()Lio/realm/BaseRealm;

    move-result-object v7

    invoke-virtual {v7}, Lio/realm/BaseRealm;->getPath()Ljava/lang/String;

    move-result-object v3

    .line 760
    .local v3, "path":Ljava/lang/String;
    iget-object v7, v0, Lio/realm/WordsColumnsConfigRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v7}, Lio/realm/ProxyState;->getRealm$realm()Lio/realm/BaseRealm;

    move-result-object v7

    invoke-virtual {v7}, Lio/realm/BaseRealm;->getPath()Ljava/lang/String;

    move-result-object v1

    .line 761
    .local v1, "otherPath":Ljava/lang/String;
    if-eqz v3, :cond_5

    invoke-virtual {v3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_6

    :cond_4
    move v5, v6

    goto :goto_0

    :cond_5
    if-nez v1, :cond_4

    .line 763
    :cond_6
    iget-object v7, p0, Lio/realm/WordsColumnsConfigRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v7}, Lio/realm/ProxyState;->getRow$realm()Lio/realm/internal/Row;

    move-result-object v7

    invoke-interface {v7}, Lio/realm/internal/Row;->getTable()Lio/realm/internal/Table;

    move-result-object v7

    invoke-virtual {v7}, Lio/realm/internal/Table;->getName()Ljava/lang/String;

    move-result-object v4

    .line 764
    .local v4, "tableName":Ljava/lang/String;
    iget-object v7, v0, Lio/realm/WordsColumnsConfigRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v7}, Lio/realm/ProxyState;->getRow$realm()Lio/realm/internal/Row;

    move-result-object v7

    invoke-interface {v7}, Lio/realm/internal/Row;->getTable()Lio/realm/internal/Table;

    move-result-object v7

    invoke-virtual {v7}, Lio/realm/internal/Table;->getName()Ljava/lang/String;

    move-result-object v2

    .line 765
    .local v2, "otherTableName":Ljava/lang/String;
    if-eqz v4, :cond_8

    invoke-virtual {v4, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_9

    :cond_7
    move v5, v6

    goto :goto_0

    :cond_8
    if-nez v2, :cond_7

    .line 767
    :cond_9
    iget-object v7, p0, Lio/realm/WordsColumnsConfigRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v7}, Lio/realm/ProxyState;->getRow$realm()Lio/realm/internal/Row;

    move-result-object v7

    invoke-interface {v7}, Lio/realm/internal/Row;->getIndex()J

    move-result-wide v8

    iget-object v7, v0, Lio/realm/WordsColumnsConfigRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v7}, Lio/realm/ProxyState;->getRow$realm()Lio/realm/internal/Row;

    move-result-object v7

    invoke-interface {v7}, Lio/realm/internal/Row;->getIndex()J

    move-result-wide v10

    cmp-long v7, v8, v10

    if-eqz v7, :cond_0

    move v5, v6

    goto :goto_0
.end method

.method public hashCode()I
    .locals 8

    .prologue
    const/4 v5, 0x0

    .line 742
    iget-object v6, p0, Lio/realm/WordsColumnsConfigRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v6}, Lio/realm/ProxyState;->getRealm$realm()Lio/realm/BaseRealm;

    move-result-object v6

    invoke-virtual {v6}, Lio/realm/BaseRealm;->getPath()Ljava/lang/String;

    move-result-object v0

    .line 743
    .local v0, "realmName":Ljava/lang/String;
    iget-object v6, p0, Lio/realm/WordsColumnsConfigRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v6}, Lio/realm/ProxyState;->getRow$realm()Lio/realm/internal/Row;

    move-result-object v6

    invoke-interface {v6}, Lio/realm/internal/Row;->getTable()Lio/realm/internal/Table;

    move-result-object v6

    invoke-virtual {v6}, Lio/realm/internal/Table;->getName()Ljava/lang/String;

    move-result-object v4

    .line 744
    .local v4, "tableName":Ljava/lang/String;
    iget-object v6, p0, Lio/realm/WordsColumnsConfigRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v6}, Lio/realm/ProxyState;->getRow$realm()Lio/realm/internal/Row;

    move-result-object v6

    invoke-interface {v6}, Lio/realm/internal/Row;->getIndex()J

    move-result-wide v2

    .line 746
    .local v2, "rowIndex":J
    const/16 v1, 0x11

    .line 747
    .local v1, "result":I
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v6

    :goto_0
    add-int/lit16 v1, v6, 0x20f

    .line 748
    mul-int/lit8 v6, v1, 0x1f

    if-eqz v4, :cond_0

    invoke-virtual {v4}, Ljava/lang/String;->hashCode()I

    move-result v5

    :cond_0
    add-int v1, v6, v5

    .line 749
    mul-int/lit8 v5, v1, 0x1f

    const/16 v6, 0x20

    ushr-long v6, v2, v6

    xor-long/2addr v6, v2

    long-to-int v6, v6

    add-int v1, v5, v6

    .line 750
    return v1

    :cond_1
    move v6, v5

    .line 747
    goto :goto_0
.end method

.method public realm$injectObjectContext()V
    .locals 3

    .prologue
    .line 101
    iget-object v1, p0, Lio/realm/WordsColumnsConfigRealmProxy;->proxyState:Lio/realm/ProxyState;

    if-eqz v1, :cond_0

    .line 111
    :goto_0
    return-void

    .line 104
    :cond_0
    sget-object v1, Lio/realm/BaseRealm;->objectContext:Lio/realm/BaseRealm$ThreadLocalRealmObjectContext;

    invoke-virtual {v1}, Lio/realm/BaseRealm$ThreadLocalRealmObjectContext;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/realm/BaseRealm$RealmObjectContext;

    .line 105
    .local v0, "context":Lio/realm/BaseRealm$RealmObjectContext;
    invoke-virtual {v0}, Lio/realm/BaseRealm$RealmObjectContext;->getColumnInfo()Lio/realm/internal/ColumnInfo;

    move-result-object v1

    check-cast v1, Lio/realm/WordsColumnsConfigRealmProxy$WordsColumnsConfigColumnInfo;

    iput-object v1, p0, Lio/realm/WordsColumnsConfigRealmProxy;->columnInfo:Lio/realm/WordsColumnsConfigRealmProxy$WordsColumnsConfigColumnInfo;

    .line 106
    new-instance v1, Lio/realm/ProxyState;

    invoke-direct {v1, p0}, Lio/realm/ProxyState;-><init>(Lio/realm/RealmModel;)V

    iput-object v1, p0, Lio/realm/WordsColumnsConfigRealmProxy;->proxyState:Lio/realm/ProxyState;

    .line 107
    iget-object v1, p0, Lio/realm/WordsColumnsConfigRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v0}, Lio/realm/BaseRealm$RealmObjectContext;->getRealm()Lio/realm/BaseRealm;

    move-result-object v2

    invoke-virtual {v1, v2}, Lio/realm/ProxyState;->setRealm$realm(Lio/realm/BaseRealm;)V

    .line 108
    iget-object v1, p0, Lio/realm/WordsColumnsConfigRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v0}, Lio/realm/BaseRealm$RealmObjectContext;->getRow()Lio/realm/internal/Row;

    move-result-object v2

    invoke-virtual {v1, v2}, Lio/realm/ProxyState;->setRow$realm(Lio/realm/internal/Row;)V

    .line 109
    iget-object v1, p0, Lio/realm/WordsColumnsConfigRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v0}, Lio/realm/BaseRealm$RealmObjectContext;->getAcceptDefaultValue()Z

    move-result v2

    invoke-virtual {v1, v2}, Lio/realm/ProxyState;->setAcceptDefaultValue$realm(Z)V

    .line 110
    iget-object v1, p0, Lio/realm/WordsColumnsConfigRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v0}, Lio/realm/BaseRealm$RealmObjectContext;->getExcludeFields()Ljava/util/List;

    move-result-object v2

    invoke-virtual {v1, v2}, Lio/realm/ProxyState;->setExcludeFields$realm(Ljava/util/List;)V

    goto :goto_0
.end method

.method public realmGet$columnCount()I
    .locals 4

    .prologue
    .line 151
    iget-object v0, p0, Lio/realm/WordsColumnsConfigRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v0}, Lio/realm/ProxyState;->getRealm$realm()Lio/realm/BaseRealm;

    move-result-object v0

    invoke-virtual {v0}, Lio/realm/BaseRealm;->checkIfValid()V

    .line 152
    iget-object v0, p0, Lio/realm/WordsColumnsConfigRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v0}, Lio/realm/ProxyState;->getRow$realm()Lio/realm/internal/Row;

    move-result-object v0

    iget-object v1, p0, Lio/realm/WordsColumnsConfigRealmProxy;->columnInfo:Lio/realm/WordsColumnsConfigRealmProxy$WordsColumnsConfigColumnInfo;

    iget-wide v2, v1, Lio/realm/WordsColumnsConfigRealmProxy$WordsColumnsConfigColumnInfo;->columnCountIndex:J

    invoke-interface {v0, v2, v3}, Lio/realm/internal/Row;->getLong(J)J

    move-result-wide v0

    long-to-int v0, v0

    return v0
.end method

.method public realmGet$id()I
    .locals 4

    .prologue
    .line 115
    iget-object v0, p0, Lio/realm/WordsColumnsConfigRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v0}, Lio/realm/ProxyState;->getRealm$realm()Lio/realm/BaseRealm;

    move-result-object v0

    invoke-virtual {v0}, Lio/realm/BaseRealm;->checkIfValid()V

    .line 116
    iget-object v0, p0, Lio/realm/WordsColumnsConfigRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v0}, Lio/realm/ProxyState;->getRow$realm()Lio/realm/internal/Row;

    move-result-object v0

    iget-object v1, p0, Lio/realm/WordsColumnsConfigRealmProxy;->columnInfo:Lio/realm/WordsColumnsConfigRealmProxy$WordsColumnsConfigColumnInfo;

    iget-wide v2, v1, Lio/realm/WordsColumnsConfigRealmProxy$WordsColumnsConfigColumnInfo;->idIndex:J

    invoke-interface {v0, v2, v3}, Lio/realm/internal/Row;->getLong(J)J

    move-result-wide v0

    long-to-int v0, v0

    return v0
.end method

.method public realmGet$proxyState()Lio/realm/ProxyState;
    .locals 1

    .prologue
    .line 737
    iget-object v0, p0, Lio/realm/WordsColumnsConfigRealmProxy;->proxyState:Lio/realm/ProxyState;

    return-object v0
.end method

.method public realmGet$rowCount()I
    .locals 4

    .prologue
    .line 131
    iget-object v0, p0, Lio/realm/WordsColumnsConfigRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v0}, Lio/realm/ProxyState;->getRealm$realm()Lio/realm/BaseRealm;

    move-result-object v0

    invoke-virtual {v0}, Lio/realm/BaseRealm;->checkIfValid()V

    .line 132
    iget-object v0, p0, Lio/realm/WordsColumnsConfigRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v0}, Lio/realm/ProxyState;->getRow$realm()Lio/realm/internal/Row;

    move-result-object v0

    iget-object v1, p0, Lio/realm/WordsColumnsConfigRealmProxy;->columnInfo:Lio/realm/WordsColumnsConfigRealmProxy$WordsColumnsConfigColumnInfo;

    iget-wide v2, v1, Lio/realm/WordsColumnsConfigRealmProxy$WordsColumnsConfigColumnInfo;->rowCountIndex:J

    invoke-interface {v0, v2, v3}, Lio/realm/internal/Row;->getLong(J)J

    move-result-wide v0

    long-to-int v0, v0

    return v0
.end method

.method public realmGet$speed()I
    .locals 4

    .prologue
    .line 191
    iget-object v0, p0, Lio/realm/WordsColumnsConfigRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v0}, Lio/realm/ProxyState;->getRealm$realm()Lio/realm/BaseRealm;

    move-result-object v0

    invoke-virtual {v0}, Lio/realm/BaseRealm;->checkIfValid()V

    .line 192
    iget-object v0, p0, Lio/realm/WordsColumnsConfigRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v0}, Lio/realm/ProxyState;->getRow$realm()Lio/realm/internal/Row;

    move-result-object v0

    iget-object v1, p0, Lio/realm/WordsColumnsConfigRealmProxy;->columnInfo:Lio/realm/WordsColumnsConfigRealmProxy$WordsColumnsConfigColumnInfo;

    iget-wide v2, v1, Lio/realm/WordsColumnsConfigRealmProxy$WordsColumnsConfigColumnInfo;->speedIndex:J

    invoke-interface {v0, v2, v3}, Lio/realm/internal/Row;->getLong(J)J

    move-result-wide v0

    long-to-int v0, v0

    return v0
.end method

.method public realmGet$trainingDuration()J
    .locals 4

    .prologue
    .line 211
    iget-object v0, p0, Lio/realm/WordsColumnsConfigRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v0}, Lio/realm/ProxyState;->getRealm$realm()Lio/realm/BaseRealm;

    move-result-object v0

    invoke-virtual {v0}, Lio/realm/BaseRealm;->checkIfValid()V

    .line 212
    iget-object v0, p0, Lio/realm/WordsColumnsConfigRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v0}, Lio/realm/ProxyState;->getRow$realm()Lio/realm/internal/Row;

    move-result-object v0

    iget-object v1, p0, Lio/realm/WordsColumnsConfigRealmProxy;->columnInfo:Lio/realm/WordsColumnsConfigRealmProxy$WordsColumnsConfigColumnInfo;

    iget-wide v2, v1, Lio/realm/WordsColumnsConfigRealmProxy$WordsColumnsConfigColumnInfo;->trainingDurationIndex:J

    invoke-interface {v0, v2, v3}, Lio/realm/internal/Row;->getLong(J)J

    move-result-wide v0

    return-wide v0
.end method

.method public realmGet$wordsPerItem()I
    .locals 4

    .prologue
    .line 171
    iget-object v0, p0, Lio/realm/WordsColumnsConfigRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v0}, Lio/realm/ProxyState;->getRealm$realm()Lio/realm/BaseRealm;

    move-result-object v0

    invoke-virtual {v0}, Lio/realm/BaseRealm;->checkIfValid()V

    .line 172
    iget-object v0, p0, Lio/realm/WordsColumnsConfigRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v0}, Lio/realm/ProxyState;->getRow$realm()Lio/realm/internal/Row;

    move-result-object v0

    iget-object v1, p0, Lio/realm/WordsColumnsConfigRealmProxy;->columnInfo:Lio/realm/WordsColumnsConfigRealmProxy$WordsColumnsConfigColumnInfo;

    iget-wide v2, v1, Lio/realm/WordsColumnsConfigRealmProxy$WordsColumnsConfigColumnInfo;->wordsPerItemIndex:J

    invoke-interface {v0, v2, v3}, Lio/realm/internal/Row;->getLong(J)J

    move-result-wide v0

    long-to-int v0, v0

    return v0
.end method

.method public realmSet$columnCount(I)V
    .locals 9
    .param p1, "value"    # I

    .prologue
    .line 156
    iget-object v1, p0, Lio/realm/WordsColumnsConfigRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v1}, Lio/realm/ProxyState;->isUnderConstruction()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 157
    iget-object v1, p0, Lio/realm/WordsColumnsConfigRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v1}, Lio/realm/ProxyState;->getAcceptDefaultValue$realm()Z

    move-result v1

    if-nez v1, :cond_0

    .line 167
    :goto_0
    return-void

    .line 160
    :cond_0
    iget-object v1, p0, Lio/realm/WordsColumnsConfigRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v1}, Lio/realm/ProxyState;->getRow$realm()Lio/realm/internal/Row;

    move-result-object v0

    .line 161
    .local v0, "row":Lio/realm/internal/Row;
    invoke-interface {v0}, Lio/realm/internal/Row;->getTable()Lio/realm/internal/Table;

    move-result-object v1

    iget-object v2, p0, Lio/realm/WordsColumnsConfigRealmProxy;->columnInfo:Lio/realm/WordsColumnsConfigRealmProxy$WordsColumnsConfigColumnInfo;

    iget-wide v2, v2, Lio/realm/WordsColumnsConfigRealmProxy$WordsColumnsConfigColumnInfo;->columnCountIndex:J

    invoke-interface {v0}, Lio/realm/internal/Row;->getIndex()J

    move-result-wide v4

    int-to-long v6, p1

    const/4 v8, 0x1

    invoke-virtual/range {v1 .. v8}, Lio/realm/internal/Table;->setLong(JJJZ)V

    goto :goto_0

    .line 165
    .end local v0    # "row":Lio/realm/internal/Row;
    :cond_1
    iget-object v1, p0, Lio/realm/WordsColumnsConfigRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v1}, Lio/realm/ProxyState;->getRealm$realm()Lio/realm/BaseRealm;

    move-result-object v1

    invoke-virtual {v1}, Lio/realm/BaseRealm;->checkIfValid()V

    .line 166
    iget-object v1, p0, Lio/realm/WordsColumnsConfigRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v1}, Lio/realm/ProxyState;->getRow$realm()Lio/realm/internal/Row;

    move-result-object v1

    iget-object v2, p0, Lio/realm/WordsColumnsConfigRealmProxy;->columnInfo:Lio/realm/WordsColumnsConfigRealmProxy$WordsColumnsConfigColumnInfo;

    iget-wide v2, v2, Lio/realm/WordsColumnsConfigRealmProxy$WordsColumnsConfigColumnInfo;->columnCountIndex:J

    int-to-long v4, p1

    invoke-interface {v1, v2, v3, v4, v5}, Lio/realm/internal/Row;->setLong(JJ)V

    goto :goto_0
.end method

.method public realmSet$id(I)V
    .locals 2
    .param p1, "value"    # I

    .prologue
    .line 120
    iget-object v0, p0, Lio/realm/WordsColumnsConfigRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v0}, Lio/realm/ProxyState;->isUnderConstruction()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 122
    return-void

    .line 125
    :cond_0
    iget-object v0, p0, Lio/realm/WordsColumnsConfigRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v0}, Lio/realm/ProxyState;->getRealm$realm()Lio/realm/BaseRealm;

    move-result-object v0

    invoke-virtual {v0}, Lio/realm/BaseRealm;->checkIfValid()V

    .line 126
    new-instance v0, Lio/realm/exceptions/RealmException;

    const-string v1, "Primary key field \'id\' cannot be changed after object was created."

    invoke-direct {v0, v1}, Lio/realm/exceptions/RealmException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public realmSet$rowCount(I)V
    .locals 9
    .param p1, "value"    # I

    .prologue
    .line 136
    iget-object v1, p0, Lio/realm/WordsColumnsConfigRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v1}, Lio/realm/ProxyState;->isUnderConstruction()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 137
    iget-object v1, p0, Lio/realm/WordsColumnsConfigRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v1}, Lio/realm/ProxyState;->getAcceptDefaultValue$realm()Z

    move-result v1

    if-nez v1, :cond_0

    .line 147
    :goto_0
    return-void

    .line 140
    :cond_0
    iget-object v1, p0, Lio/realm/WordsColumnsConfigRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v1}, Lio/realm/ProxyState;->getRow$realm()Lio/realm/internal/Row;

    move-result-object v0

    .line 141
    .local v0, "row":Lio/realm/internal/Row;
    invoke-interface {v0}, Lio/realm/internal/Row;->getTable()Lio/realm/internal/Table;

    move-result-object v1

    iget-object v2, p0, Lio/realm/WordsColumnsConfigRealmProxy;->columnInfo:Lio/realm/WordsColumnsConfigRealmProxy$WordsColumnsConfigColumnInfo;

    iget-wide v2, v2, Lio/realm/WordsColumnsConfigRealmProxy$WordsColumnsConfigColumnInfo;->rowCountIndex:J

    invoke-interface {v0}, Lio/realm/internal/Row;->getIndex()J

    move-result-wide v4

    int-to-long v6, p1

    const/4 v8, 0x1

    invoke-virtual/range {v1 .. v8}, Lio/realm/internal/Table;->setLong(JJJZ)V

    goto :goto_0

    .line 145
    .end local v0    # "row":Lio/realm/internal/Row;
    :cond_1
    iget-object v1, p0, Lio/realm/WordsColumnsConfigRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v1}, Lio/realm/ProxyState;->getRealm$realm()Lio/realm/BaseRealm;

    move-result-object v1

    invoke-virtual {v1}, Lio/realm/BaseRealm;->checkIfValid()V

    .line 146
    iget-object v1, p0, Lio/realm/WordsColumnsConfigRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v1}, Lio/realm/ProxyState;->getRow$realm()Lio/realm/internal/Row;

    move-result-object v1

    iget-object v2, p0, Lio/realm/WordsColumnsConfigRealmProxy;->columnInfo:Lio/realm/WordsColumnsConfigRealmProxy$WordsColumnsConfigColumnInfo;

    iget-wide v2, v2, Lio/realm/WordsColumnsConfigRealmProxy$WordsColumnsConfigColumnInfo;->rowCountIndex:J

    int-to-long v4, p1

    invoke-interface {v1, v2, v3, v4, v5}, Lio/realm/internal/Row;->setLong(JJ)V

    goto :goto_0
.end method

.method public realmSet$speed(I)V
    .locals 9
    .param p1, "value"    # I

    .prologue
    .line 196
    iget-object v1, p0, Lio/realm/WordsColumnsConfigRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v1}, Lio/realm/ProxyState;->isUnderConstruction()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 197
    iget-object v1, p0, Lio/realm/WordsColumnsConfigRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v1}, Lio/realm/ProxyState;->getAcceptDefaultValue$realm()Z

    move-result v1

    if-nez v1, :cond_0

    .line 207
    :goto_0
    return-void

    .line 200
    :cond_0
    iget-object v1, p0, Lio/realm/WordsColumnsConfigRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v1}, Lio/realm/ProxyState;->getRow$realm()Lio/realm/internal/Row;

    move-result-object v0

    .line 201
    .local v0, "row":Lio/realm/internal/Row;
    invoke-interface {v0}, Lio/realm/internal/Row;->getTable()Lio/realm/internal/Table;

    move-result-object v1

    iget-object v2, p0, Lio/realm/WordsColumnsConfigRealmProxy;->columnInfo:Lio/realm/WordsColumnsConfigRealmProxy$WordsColumnsConfigColumnInfo;

    iget-wide v2, v2, Lio/realm/WordsColumnsConfigRealmProxy$WordsColumnsConfigColumnInfo;->speedIndex:J

    invoke-interface {v0}, Lio/realm/internal/Row;->getIndex()J

    move-result-wide v4

    int-to-long v6, p1

    const/4 v8, 0x1

    invoke-virtual/range {v1 .. v8}, Lio/realm/internal/Table;->setLong(JJJZ)V

    goto :goto_0

    .line 205
    .end local v0    # "row":Lio/realm/internal/Row;
    :cond_1
    iget-object v1, p0, Lio/realm/WordsColumnsConfigRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v1}, Lio/realm/ProxyState;->getRealm$realm()Lio/realm/BaseRealm;

    move-result-object v1

    invoke-virtual {v1}, Lio/realm/BaseRealm;->checkIfValid()V

    .line 206
    iget-object v1, p0, Lio/realm/WordsColumnsConfigRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v1}, Lio/realm/ProxyState;->getRow$realm()Lio/realm/internal/Row;

    move-result-object v1

    iget-object v2, p0, Lio/realm/WordsColumnsConfigRealmProxy;->columnInfo:Lio/realm/WordsColumnsConfigRealmProxy$WordsColumnsConfigColumnInfo;

    iget-wide v2, v2, Lio/realm/WordsColumnsConfigRealmProxy$WordsColumnsConfigColumnInfo;->speedIndex:J

    int-to-long v4, p1

    invoke-interface {v1, v2, v3, v4, v5}, Lio/realm/internal/Row;->setLong(JJ)V

    goto :goto_0
.end method

.method public realmSet$trainingDuration(J)V
    .locals 9
    .param p1, "value"    # J

    .prologue
    .line 216
    iget-object v1, p0, Lio/realm/WordsColumnsConfigRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v1}, Lio/realm/ProxyState;->isUnderConstruction()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 217
    iget-object v1, p0, Lio/realm/WordsColumnsConfigRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v1}, Lio/realm/ProxyState;->getAcceptDefaultValue$realm()Z

    move-result v1

    if-nez v1, :cond_0

    .line 227
    :goto_0
    return-void

    .line 220
    :cond_0
    iget-object v1, p0, Lio/realm/WordsColumnsConfigRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v1}, Lio/realm/ProxyState;->getRow$realm()Lio/realm/internal/Row;

    move-result-object v0

    .line 221
    .local v0, "row":Lio/realm/internal/Row;
    invoke-interface {v0}, Lio/realm/internal/Row;->getTable()Lio/realm/internal/Table;

    move-result-object v1

    iget-object v2, p0, Lio/realm/WordsColumnsConfigRealmProxy;->columnInfo:Lio/realm/WordsColumnsConfigRealmProxy$WordsColumnsConfigColumnInfo;

    iget-wide v2, v2, Lio/realm/WordsColumnsConfigRealmProxy$WordsColumnsConfigColumnInfo;->trainingDurationIndex:J

    invoke-interface {v0}, Lio/realm/internal/Row;->getIndex()J

    move-result-wide v4

    const/4 v8, 0x1

    move-wide v6, p1

    invoke-virtual/range {v1 .. v8}, Lio/realm/internal/Table;->setLong(JJJZ)V

    goto :goto_0

    .line 225
    .end local v0    # "row":Lio/realm/internal/Row;
    :cond_1
    iget-object v1, p0, Lio/realm/WordsColumnsConfigRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v1}, Lio/realm/ProxyState;->getRealm$realm()Lio/realm/BaseRealm;

    move-result-object v1

    invoke-virtual {v1}, Lio/realm/BaseRealm;->checkIfValid()V

    .line 226
    iget-object v1, p0, Lio/realm/WordsColumnsConfigRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v1}, Lio/realm/ProxyState;->getRow$realm()Lio/realm/internal/Row;

    move-result-object v1

    iget-object v2, p0, Lio/realm/WordsColumnsConfigRealmProxy;->columnInfo:Lio/realm/WordsColumnsConfigRealmProxy$WordsColumnsConfigColumnInfo;

    iget-wide v2, v2, Lio/realm/WordsColumnsConfigRealmProxy$WordsColumnsConfigColumnInfo;->trainingDurationIndex:J

    invoke-interface {v1, v2, v3, p1, p2}, Lio/realm/internal/Row;->setLong(JJ)V

    goto :goto_0
.end method

.method public realmSet$wordsPerItem(I)V
    .locals 9
    .param p1, "value"    # I

    .prologue
    .line 176
    iget-object v1, p0, Lio/realm/WordsColumnsConfigRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v1}, Lio/realm/ProxyState;->isUnderConstruction()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 177
    iget-object v1, p0, Lio/realm/WordsColumnsConfigRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v1}, Lio/realm/ProxyState;->getAcceptDefaultValue$realm()Z

    move-result v1

    if-nez v1, :cond_0

    .line 187
    :goto_0
    return-void

    .line 180
    :cond_0
    iget-object v1, p0, Lio/realm/WordsColumnsConfigRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v1}, Lio/realm/ProxyState;->getRow$realm()Lio/realm/internal/Row;

    move-result-object v0

    .line 181
    .local v0, "row":Lio/realm/internal/Row;
    invoke-interface {v0}, Lio/realm/internal/Row;->getTable()Lio/realm/internal/Table;

    move-result-object v1

    iget-object v2, p0, Lio/realm/WordsColumnsConfigRealmProxy;->columnInfo:Lio/realm/WordsColumnsConfigRealmProxy$WordsColumnsConfigColumnInfo;

    iget-wide v2, v2, Lio/realm/WordsColumnsConfigRealmProxy$WordsColumnsConfigColumnInfo;->wordsPerItemIndex:J

    invoke-interface {v0}, Lio/realm/internal/Row;->getIndex()J

    move-result-wide v4

    int-to-long v6, p1

    const/4 v8, 0x1

    invoke-virtual/range {v1 .. v8}, Lio/realm/internal/Table;->setLong(JJJZ)V

    goto :goto_0

    .line 185
    .end local v0    # "row":Lio/realm/internal/Row;
    :cond_1
    iget-object v1, p0, Lio/realm/WordsColumnsConfigRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v1}, Lio/realm/ProxyState;->getRealm$realm()Lio/realm/BaseRealm;

    move-result-object v1

    invoke-virtual {v1}, Lio/realm/BaseRealm;->checkIfValid()V

    .line 186
    iget-object v1, p0, Lio/realm/WordsColumnsConfigRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v1}, Lio/realm/ProxyState;->getRow$realm()Lio/realm/internal/Row;

    move-result-object v1

    iget-object v2, p0, Lio/realm/WordsColumnsConfigRealmProxy;->columnInfo:Lio/realm/WordsColumnsConfigRealmProxy$WordsColumnsConfigColumnInfo;

    iget-wide v2, v2, Lio/realm/WordsColumnsConfigRealmProxy$WordsColumnsConfigColumnInfo;->wordsPerItemIndex:J

    int-to-long v4, p1

    invoke-interface {v1, v2, v3, v4, v5}, Lio/realm/internal/Row;->setLong(JJ)V

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 704
    invoke-static {p0}, Lio/realm/RealmObject;->isValid(Lio/realm/RealmModel;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 705
    const-string v1, "Invalid object"

    .line 732
    :goto_0
    return-object v1

    .line 707
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "WordsColumnsConfig = ["

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 708
    .local v0, "stringBuilder":Ljava/lang/StringBuilder;
    const-string v1, "{id:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 709
    invoke-virtual {p0}, Lio/realm/WordsColumnsConfigRealmProxy;->realmGet$id()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 710
    const-string v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 711
    const-string v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 712
    const-string v1, "{rowCount:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 713
    invoke-virtual {p0}, Lio/realm/WordsColumnsConfigRealmProxy;->realmGet$rowCount()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 714
    const-string v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 715
    const-string v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 716
    const-string v1, "{columnCount:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 717
    invoke-virtual {p0}, Lio/realm/WordsColumnsConfigRealmProxy;->realmGet$columnCount()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 718
    const-string v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 719
    const-string v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 720
    const-string v1, "{wordsPerItem:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 721
    invoke-virtual {p0}, Lio/realm/WordsColumnsConfigRealmProxy;->realmGet$wordsPerItem()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 722
    const-string v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 723
    const-string v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 724
    const-string v1, "{speed:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 725
    invoke-virtual {p0}, Lio/realm/WordsColumnsConfigRealmProxy;->realmGet$speed()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 726
    const-string v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 727
    const-string v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 728
    const-string v1, "{trainingDuration:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 729
    invoke-virtual {p0}, Lio/realm/WordsColumnsConfigRealmProxy;->realmGet$trainingDuration()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 730
    const-string v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 731
    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 732
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_0
.end method
