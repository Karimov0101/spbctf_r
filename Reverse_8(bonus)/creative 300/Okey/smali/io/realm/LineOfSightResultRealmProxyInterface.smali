.class public interface abstract Lio/realm/LineOfSightResultRealmProxyInterface;
.super Ljava/lang/Object;
.source "LineOfSightResultRealmProxyInterface.java"


# virtual methods
.method public abstract realmGet$config()Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightConfig;
.end method

.method public abstract realmGet$foundMistakeCount()I
.end method

.method public abstract realmGet$id()I
.end method

.method public abstract realmGet$mistakeCount()I
.end method

.method public abstract realmGet$unixTime()J
.end method

.method public abstract realmSet$config(Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightConfig;)V
.end method

.method public abstract realmSet$foundMistakeCount(I)V
.end method

.method public abstract realmSet$id(I)V
.end method

.method public abstract realmSet$mistakeCount(I)V
.end method

.method public abstract realmSet$unixTime(J)V
.end method
