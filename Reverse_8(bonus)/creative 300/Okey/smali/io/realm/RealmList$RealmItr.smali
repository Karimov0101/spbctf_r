.class Lio/realm/RealmList$RealmItr;
.super Ljava/lang/Object;
.source "RealmList.java"

# interfaces
.implements Ljava/util/Iterator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lio/realm/RealmList;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "RealmItr"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Iterator",
        "<TE;>;"
    }
.end annotation


# instance fields
.field cursor:I

.field expectedModCount:I

.field lastRet:I

.field final synthetic this$0:Lio/realm/RealmList;


# direct methods
.method private constructor <init>(Lio/realm/RealmList;)V
    .locals 1

    .prologue
    .line 957
    .local p0, "this":Lio/realm/RealmList$RealmItr;, "Lio/realm/RealmList<TE;>.RealmItr;"
    iput-object p1, p0, Lio/realm/RealmList$RealmItr;->this$0:Lio/realm/RealmList;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 961
    const/4 v0, 0x0

    iput v0, p0, Lio/realm/RealmList$RealmItr;->cursor:I

    .line 968
    const/4 v0, -0x1

    iput v0, p0, Lio/realm/RealmList$RealmItr;->lastRet:I

    .line 975
    iget-object v0, p0, Lio/realm/RealmList$RealmItr;->this$0:Lio/realm/RealmList;

    invoke-static {v0}, Lio/realm/RealmList;->access$100(Lio/realm/RealmList;)I

    move-result v0

    iput v0, p0, Lio/realm/RealmList$RealmItr;->expectedModCount:I

    return-void
.end method

.method synthetic constructor <init>(Lio/realm/RealmList;Lio/realm/RealmList$1;)V
    .locals 0
    .param p1, "x0"    # Lio/realm/RealmList;
    .param p2, "x1"    # Lio/realm/RealmList$1;

    .prologue
    .line 957
    .local p0, "this":Lio/realm/RealmList$RealmItr;, "Lio/realm/RealmList<TE;>.RealmItr;"
    invoke-direct {p0, p1}, Lio/realm/RealmList$RealmItr;-><init>(Lio/realm/RealmList;)V

    return-void
.end method


# virtual methods
.method final checkConcurrentModification()V
    .locals 2

    .prologue
    .line 1033
    .local p0, "this":Lio/realm/RealmList$RealmItr;, "Lio/realm/RealmList<TE;>.RealmItr;"
    iget-object v0, p0, Lio/realm/RealmList$RealmItr;->this$0:Lio/realm/RealmList;

    invoke-static {v0}, Lio/realm/RealmList;->access$300(Lio/realm/RealmList;)I

    move-result v0

    iget v1, p0, Lio/realm/RealmList$RealmItr;->expectedModCount:I

    if-eq v0, v1, :cond_0

    .line 1034
    new-instance v0, Ljava/util/ConcurrentModificationException;

    invoke-direct {v0}, Ljava/util/ConcurrentModificationException;-><init>()V

    throw v0

    .line 1036
    :cond_0
    return-void
.end method

.method public hasNext()Z
    .locals 2

    .prologue
    .line 981
    .local p0, "this":Lio/realm/RealmList$RealmItr;, "Lio/realm/RealmList<TE;>.RealmItr;"
    iget-object v0, p0, Lio/realm/RealmList$RealmItr;->this$0:Lio/realm/RealmList;

    iget-object v0, v0, Lio/realm/RealmList;->realm:Lio/realm/BaseRealm;

    invoke-virtual {v0}, Lio/realm/BaseRealm;->checkIfValid()V

    .line 982
    invoke-virtual {p0}, Lio/realm/RealmList$RealmItr;->checkConcurrentModification()V

    .line 983
    iget v0, p0, Lio/realm/RealmList$RealmItr;->cursor:I

    iget-object v1, p0, Lio/realm/RealmList$RealmItr;->this$0:Lio/realm/RealmList;

    invoke-virtual {v1}, Lio/realm/RealmList;->size()I

    move-result v1

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public next()Lio/realm/RealmModel;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TE;"
        }
    .end annotation

    .prologue
    .line 990
    .local p0, "this":Lio/realm/RealmList$RealmItr;, "Lio/realm/RealmList<TE;>.RealmItr;"
    iget-object v3, p0, Lio/realm/RealmList$RealmItr;->this$0:Lio/realm/RealmList;

    iget-object v3, v3, Lio/realm/RealmList;->realm:Lio/realm/BaseRealm;

    invoke-virtual {v3}, Lio/realm/BaseRealm;->checkIfValid()V

    .line 991
    invoke-virtual {p0}, Lio/realm/RealmList$RealmItr;->checkConcurrentModification()V

    .line 992
    iget v1, p0, Lio/realm/RealmList$RealmItr;->cursor:I

    .line 994
    .local v1, "i":I
    :try_start_0
    iget-object v3, p0, Lio/realm/RealmList$RealmItr;->this$0:Lio/realm/RealmList;

    invoke-virtual {v3, v1}, Lio/realm/RealmList;->get(I)Lio/realm/RealmModel;

    move-result-object v2

    .line 995
    .local v2, "next":Lio/realm/RealmModel;, "TE;"
    iput v1, p0, Lio/realm/RealmList$RealmItr;->lastRet:I

    .line 996
    add-int/lit8 v3, v1, 0x1

    iput v3, p0, Lio/realm/RealmList$RealmItr;->cursor:I
    :try_end_0
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    .line 997
    return-object v2

    .line 998
    .end local v2    # "next":Lio/realm/RealmModel;, "TE;"
    :catch_0
    move-exception v0

    .line 999
    .local v0, "e":Ljava/lang/IndexOutOfBoundsException;
    invoke-virtual {p0}, Lio/realm/RealmList$RealmItr;->checkConcurrentModification()V

    .line 1000
    new-instance v3, Ljava/util/NoSuchElementException;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Cannot access index "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " when size is "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lio/realm/RealmList$RealmItr;->this$0:Lio/realm/RealmList;

    invoke-virtual {v5}, Lio/realm/RealmList;->size()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ". Remember to check hasNext() before using next()."

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/util/NoSuchElementException;-><init>(Ljava/lang/String;)V

    throw v3
.end method

.method public bridge synthetic next()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 957
    .local p0, "this":Lio/realm/RealmList$RealmItr;, "Lio/realm/RealmList<TE;>.RealmItr;"
    invoke-virtual {p0}, Lio/realm/RealmList$RealmItr;->next()Lio/realm/RealmModel;

    move-result-object v0

    return-object v0
.end method

.method public remove()V
    .locals 3

    .prologue
    .line 1008
    .local p0, "this":Lio/realm/RealmList$RealmItr;, "Lio/realm/RealmList<TE;>.RealmItr;"
    iget-object v1, p0, Lio/realm/RealmList$RealmItr;->this$0:Lio/realm/RealmList;

    iget-object v1, v1, Lio/realm/RealmList;->realm:Lio/realm/BaseRealm;

    invoke-virtual {v1}, Lio/realm/BaseRealm;->checkIfValid()V

    .line 1009
    iget v1, p0, Lio/realm/RealmList$RealmItr;->lastRet:I

    if-gez v1, :cond_0

    .line 1010
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "Cannot call remove() twice. Must call next() in between."

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 1012
    :cond_0
    invoke-virtual {p0}, Lio/realm/RealmList$RealmItr;->checkConcurrentModification()V

    .line 1015
    :try_start_0
    iget-object v1, p0, Lio/realm/RealmList$RealmItr;->this$0:Lio/realm/RealmList;

    iget v2, p0, Lio/realm/RealmList$RealmItr;->lastRet:I

    invoke-virtual {v1, v2}, Lio/realm/RealmList;->remove(I)Lio/realm/RealmModel;

    .line 1016
    iget v1, p0, Lio/realm/RealmList$RealmItr;->lastRet:I

    iget v2, p0, Lio/realm/RealmList$RealmItr;->cursor:I

    if-ge v1, v2, :cond_1

    .line 1017
    iget v1, p0, Lio/realm/RealmList$RealmItr;->cursor:I

    add-int/lit8 v1, v1, -0x1

    iput v1, p0, Lio/realm/RealmList$RealmItr;->cursor:I

    .line 1019
    :cond_1
    const/4 v1, -0x1

    iput v1, p0, Lio/realm/RealmList$RealmItr;->lastRet:I

    .line 1020
    iget-object v1, p0, Lio/realm/RealmList$RealmItr;->this$0:Lio/realm/RealmList;

    invoke-static {v1}, Lio/realm/RealmList;->access$200(Lio/realm/RealmList;)I

    move-result v1

    iput v1, p0, Lio/realm/RealmList$RealmItr;->expectedModCount:I
    :try_end_0
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1024
    return-void

    .line 1021
    :catch_0
    move-exception v0

    .line 1022
    .local v0, "e":Ljava/lang/IndexOutOfBoundsException;
    new-instance v1, Ljava/util/ConcurrentModificationException;

    invoke-direct {v1}, Ljava/util/ConcurrentModificationException;-><init>()V

    throw v1
.end method
