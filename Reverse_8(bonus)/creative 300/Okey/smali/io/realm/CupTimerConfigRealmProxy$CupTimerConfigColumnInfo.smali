.class final Lio/realm/CupTimerConfigRealmProxy$CupTimerConfigColumnInfo;
.super Lio/realm/internal/ColumnInfo;
.source "CupTimerConfigRealmProxy.java"

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lio/realm/CupTimerConfigRealmProxy;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = "CupTimerConfigColumnInfo"
.end annotation


# instance fields
.field public durationIndex:J

.field public idIndex:J

.field public typeIndex:J


# direct methods
.method constructor <init>(Ljava/lang/String;Lio/realm/internal/Table;)V
    .locals 4
    .param p1, "path"    # Ljava/lang/String;
    .param p2, "table"    # Lio/realm/internal/Table;

    .prologue
    .line 41
    invoke-direct {p0}, Lio/realm/internal/ColumnInfo;-><init>()V

    .line 42
    new-instance v0, Ljava/util/HashMap;

    const/4 v1, 0x3

    invoke-direct {v0, v1}, Ljava/util/HashMap;-><init>(I)V

    .line 43
    .local v0, "indicesMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Long;>;"
    const-string v1, "CupTimerConfig"

    const-string v2, "id"

    invoke-virtual {p0, p1, p2, v1, v2}, Lio/realm/CupTimerConfigRealmProxy$CupTimerConfigColumnInfo;->getValidColumnIndex(Ljava/lang/String;Lio/realm/internal/Table;Ljava/lang/String;Ljava/lang/String;)J

    move-result-wide v2

    iput-wide v2, p0, Lio/realm/CupTimerConfigRealmProxy$CupTimerConfigColumnInfo;->idIndex:J

    .line 44
    const-string v1, "id"

    iget-wide v2, p0, Lio/realm/CupTimerConfigRealmProxy$CupTimerConfigColumnInfo;->idIndex:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 45
    const-string v1, "CupTimerConfig"

    const-string v2, "duration"

    invoke-virtual {p0, p1, p2, v1, v2}, Lio/realm/CupTimerConfigRealmProxy$CupTimerConfigColumnInfo;->getValidColumnIndex(Ljava/lang/String;Lio/realm/internal/Table;Ljava/lang/String;Ljava/lang/String;)J

    move-result-wide v2

    iput-wide v2, p0, Lio/realm/CupTimerConfigRealmProxy$CupTimerConfigColumnInfo;->durationIndex:J

    .line 46
    const-string v1, "duration"

    iget-wide v2, p0, Lio/realm/CupTimerConfigRealmProxy$CupTimerConfigColumnInfo;->durationIndex:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 47
    const-string v1, "CupTimerConfig"

    const-string v2, "type"

    invoke-virtual {p0, p1, p2, v1, v2}, Lio/realm/CupTimerConfigRealmProxy$CupTimerConfigColumnInfo;->getValidColumnIndex(Ljava/lang/String;Lio/realm/internal/Table;Ljava/lang/String;Ljava/lang/String;)J

    move-result-wide v2

    iput-wide v2, p0, Lio/realm/CupTimerConfigRealmProxy$CupTimerConfigColumnInfo;->typeIndex:J

    .line 48
    const-string v1, "type"

    iget-wide v2, p0, Lio/realm/CupTimerConfigRealmProxy$CupTimerConfigColumnInfo;->typeIndex:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 50
    invoke-virtual {p0, v0}, Lio/realm/CupTimerConfigRealmProxy$CupTimerConfigColumnInfo;->setIndicesMap(Ljava/util/Map;)V

    .line 51
    return-void
.end method


# virtual methods
.method public final clone()Lio/realm/CupTimerConfigRealmProxy$CupTimerConfigColumnInfo;
    .locals 1

    .prologue
    .line 65
    invoke-super {p0}, Lio/realm/internal/ColumnInfo;->clone()Lio/realm/internal/ColumnInfo;

    move-result-object v0

    check-cast v0, Lio/realm/CupTimerConfigRealmProxy$CupTimerConfigColumnInfo;

    return-object v0
.end method

.method public bridge synthetic clone()Lio/realm/internal/ColumnInfo;
    .locals 1

    .prologue
    .line 34
    invoke-virtual {p0}, Lio/realm/CupTimerConfigRealmProxy$CupTimerConfigColumnInfo;->clone()Lio/realm/CupTimerConfigRealmProxy$CupTimerConfigColumnInfo;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 34
    invoke-virtual {p0}, Lio/realm/CupTimerConfigRealmProxy$CupTimerConfigColumnInfo;->clone()Lio/realm/CupTimerConfigRealmProxy$CupTimerConfigColumnInfo;

    move-result-object v0

    return-object v0
.end method

.method public final copyColumnInfoFrom(Lio/realm/internal/ColumnInfo;)V
    .locals 4
    .param p1, "other"    # Lio/realm/internal/ColumnInfo;

    .prologue
    .line 55
    move-object v0, p1

    check-cast v0, Lio/realm/CupTimerConfigRealmProxy$CupTimerConfigColumnInfo;

    .line 56
    .local v0, "otherInfo":Lio/realm/CupTimerConfigRealmProxy$CupTimerConfigColumnInfo;
    iget-wide v2, v0, Lio/realm/CupTimerConfigRealmProxy$CupTimerConfigColumnInfo;->idIndex:J

    iput-wide v2, p0, Lio/realm/CupTimerConfigRealmProxy$CupTimerConfigColumnInfo;->idIndex:J

    .line 57
    iget-wide v2, v0, Lio/realm/CupTimerConfigRealmProxy$CupTimerConfigColumnInfo;->durationIndex:J

    iput-wide v2, p0, Lio/realm/CupTimerConfigRealmProxy$CupTimerConfigColumnInfo;->durationIndex:J

    .line 58
    iget-wide v2, v0, Lio/realm/CupTimerConfigRealmProxy$CupTimerConfigColumnInfo;->typeIndex:J

    iput-wide v2, p0, Lio/realm/CupTimerConfigRealmProxy$CupTimerConfigColumnInfo;->typeIndex:J

    .line 60
    invoke-virtual {v0}, Lio/realm/CupTimerConfigRealmProxy$CupTimerConfigColumnInfo;->getIndicesMap()Ljava/util/Map;

    move-result-object v1

    invoke-virtual {p0, v1}, Lio/realm/CupTimerConfigRealmProxy$CupTimerConfigColumnInfo;->setIndicesMap(Ljava/util/Map;)V

    .line 61
    return-void
.end method
