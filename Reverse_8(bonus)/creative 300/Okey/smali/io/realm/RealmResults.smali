.class public Lio/realm/RealmResults;
.super Lio/realm/OrderedRealmCollectionImpl;
.source "RealmResults.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "Lio/realm/RealmModel;",
        ">",
        "Lio/realm/OrderedRealmCollectionImpl",
        "<TE;>;"
    }
.end annotation


# direct methods
.method constructor <init>(Lio/realm/BaseRealm;Lio/realm/internal/Collection;Ljava/lang/Class;)V
    .locals 0
    .param p1, "realm"    # Lio/realm/BaseRealm;
    .param p2, "collection"    # Lio/realm/internal/Collection;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/realm/BaseRealm;",
            "Lio/realm/internal/Collection;",
            "Ljava/lang/Class",
            "<TE;>;)V"
        }
    .end annotation

    .prologue
    .line 56
    .local p0, "this":Lio/realm/RealmResults;, "Lio/realm/RealmResults<TE;>;"
    .local p3, "clazz":Ljava/lang/Class;, "Ljava/lang/Class<TE;>;"
    invoke-direct {p0, p1, p2, p3}, Lio/realm/OrderedRealmCollectionImpl;-><init>(Lio/realm/BaseRealm;Lio/realm/internal/Collection;Ljava/lang/Class;)V

    .line 57
    return-void
.end method

.method constructor <init>(Lio/realm/BaseRealm;Lio/realm/internal/Collection;Ljava/lang/String;)V
    .locals 0
    .param p1, "realm"    # Lio/realm/BaseRealm;
    .param p2, "collection"    # Lio/realm/internal/Collection;
    .param p3, "className"    # Ljava/lang/String;

    .prologue
    .line 60
    .local p0, "this":Lio/realm/RealmResults;, "Lio/realm/RealmResults<TE;>;"
    invoke-direct {p0, p1, p2, p3}, Lio/realm/OrderedRealmCollectionImpl;-><init>(Lio/realm/BaseRealm;Lio/realm/internal/Collection;Ljava/lang/String;)V

    .line 61
    return-void
.end method

.method private checkForAddRemoveListener(Ljava/lang/Object;Z)V
    .locals 2
    .param p1, "listener"    # Ljava/lang/Object;
    .param p2, "checkListener"    # Z

    .prologue
    .line 135
    .local p0, "this":Lio/realm/RealmResults;, "Lio/realm/RealmResults<TE;>;"
    if-eqz p2, :cond_0

    if-nez p1, :cond_0

    .line 136
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Listener should not be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 138
    :cond_0
    iget-object v0, p0, Lio/realm/RealmResults;->realm:Lio/realm/BaseRealm;

    invoke-virtual {v0}, Lio/realm/BaseRealm;->checkIfValid()V

    .line 139
    iget-object v0, p0, Lio/realm/RealmResults;->realm:Lio/realm/BaseRealm;

    iget-object v0, v0, Lio/realm/BaseRealm;->sharedRealm:Lio/realm/internal/SharedRealm;

    iget-object v0, v0, Lio/realm/internal/SharedRealm;->capabilities:Lio/realm/internal/Capabilities;

    const-string v1, "Listeners cannot be used on current thread."

    invoke-interface {v0, v1}, Lio/realm/internal/Capabilities;->checkCanDeliverNotification(Ljava/lang/String;)V

    .line 140
    return-void
.end method


# virtual methods
.method public bridge synthetic add(ILio/realm/RealmModel;)V
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 53
    .local p0, "this":Lio/realm/RealmResults;, "Lio/realm/RealmResults<TE;>;"
    invoke-super {p0, p1, p2}, Lio/realm/OrderedRealmCollectionImpl;->add(ILio/realm/RealmModel;)V

    return-void
.end method

.method public bridge synthetic add(Lio/realm/RealmModel;)Z
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 53
    .local p0, "this":Lio/realm/RealmResults;, "Lio/realm/RealmResults<TE;>;"
    invoke-super {p0, p1}, Lio/realm/OrderedRealmCollectionImpl;->add(Lio/realm/RealmModel;)Z

    move-result v0

    return v0
.end method

.method public bridge synthetic addAll(ILjava/util/Collection;)Z
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 53
    .local p0, "this":Lio/realm/RealmResults;, "Lio/realm/RealmResults<TE;>;"
    invoke-super {p0, p1, p2}, Lio/realm/OrderedRealmCollectionImpl;->addAll(ILjava/util/Collection;)Z

    move-result v0

    return v0
.end method

.method public bridge synthetic addAll(Ljava/util/Collection;)Z
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 53
    .local p0, "this":Lio/realm/RealmResults;, "Lio/realm/RealmResults<TE;>;"
    invoke-super {p0, p1}, Lio/realm/OrderedRealmCollectionImpl;->addAll(Ljava/util/Collection;)Z

    move-result v0

    return v0
.end method

.method public addChangeListener(Lio/realm/OrderedRealmCollectionChangeListener;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/realm/OrderedRealmCollectionChangeListener",
            "<",
            "Lio/realm/RealmResults",
            "<TE;>;>;)V"
        }
    .end annotation

    .prologue
    .line 130
    .local p0, "this":Lio/realm/RealmResults;, "Lio/realm/RealmResults<TE;>;"
    .local p1, "listener":Lio/realm/OrderedRealmCollectionChangeListener;, "Lio/realm/OrderedRealmCollectionChangeListener<Lio/realm/RealmResults<TE;>;>;"
    const/4 v0, 0x1

    invoke-direct {p0, p1, v0}, Lio/realm/RealmResults;->checkForAddRemoveListener(Ljava/lang/Object;Z)V

    .line 131
    iget-object v0, p0, Lio/realm/RealmResults;->collection:Lio/realm/internal/Collection;

    invoke-virtual {v0, p0, p1}, Lio/realm/internal/Collection;->addListener(Ljava/lang/Object;Lio/realm/OrderedRealmCollectionChangeListener;)V

    .line 132
    return-void
.end method

.method public addChangeListener(Lio/realm/RealmChangeListener;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/realm/RealmChangeListener",
            "<",
            "Lio/realm/RealmResults",
            "<TE;>;>;)V"
        }
    .end annotation

    .prologue
    .line 117
    .local p0, "this":Lio/realm/RealmResults;, "Lio/realm/RealmResults<TE;>;"
    .local p1, "listener":Lio/realm/RealmChangeListener;, "Lio/realm/RealmChangeListener<Lio/realm/RealmResults<TE;>;>;"
    const/4 v0, 0x1

    invoke-direct {p0, p1, v0}, Lio/realm/RealmResults;->checkForAddRemoveListener(Ljava/lang/Object;Z)V

    .line 118
    iget-object v0, p0, Lio/realm/RealmResults;->collection:Lio/realm/internal/Collection;

    invoke-virtual {v0, p0, p1}, Lio/realm/internal/Collection;->addListener(Ljava/lang/Object;Lio/realm/RealmChangeListener;)V

    .line 119
    return-void
.end method

.method public asObservable()Lrx/Observable;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable",
            "<",
            "Lio/realm/RealmResults",
            "<TE;>;>;"
        }
    .end annotation

    .prologue
    .line 217
    .local p0, "this":Lio/realm/RealmResults;, "Lio/realm/RealmResults<TE;>;"
    iget-object v3, p0, Lio/realm/RealmResults;->realm:Lio/realm/BaseRealm;

    instance-of v3, v3, Lio/realm/Realm;

    if-eqz v3, :cond_0

    .line 218
    iget-object v3, p0, Lio/realm/RealmResults;->realm:Lio/realm/BaseRealm;

    iget-object v3, v3, Lio/realm/BaseRealm;->configuration:Lio/realm/RealmConfiguration;

    invoke-virtual {v3}, Lio/realm/RealmConfiguration;->getRxFactory()Lio/realm/rx/RxObservableFactory;

    move-result-object v4

    iget-object v3, p0, Lio/realm/RealmResults;->realm:Lio/realm/BaseRealm;

    check-cast v3, Lio/realm/Realm;

    invoke-interface {v4, v3, p0}, Lio/realm/rx/RxObservableFactory;->from(Lio/realm/Realm;Lio/realm/RealmResults;)Lrx/Observable;

    move-result-object v2

    .line 224
    :goto_0
    return-object v2

    .line 219
    :cond_0
    iget-object v3, p0, Lio/realm/RealmResults;->realm:Lio/realm/BaseRealm;

    instance-of v3, v3, Lio/realm/DynamicRealm;

    if-eqz v3, :cond_1

    .line 220
    iget-object v0, p0, Lio/realm/RealmResults;->realm:Lio/realm/BaseRealm;

    check-cast v0, Lio/realm/DynamicRealm;

    .line 221
    .local v0, "dynamicRealm":Lio/realm/DynamicRealm;
    move-object v1, p0

    .line 223
    .local v1, "dynamicResults":Lio/realm/RealmResults;, "Lio/realm/RealmResults<Lio/realm/DynamicRealmObject;>;"
    iget-object v3, p0, Lio/realm/RealmResults;->realm:Lio/realm/BaseRealm;

    iget-object v3, v3, Lio/realm/BaseRealm;->configuration:Lio/realm/RealmConfiguration;

    invoke-virtual {v3}, Lio/realm/RealmConfiguration;->getRxFactory()Lio/realm/rx/RxObservableFactory;

    move-result-object v3

    invoke-interface {v3, v0, v1}, Lio/realm/rx/RxObservableFactory;->from(Lio/realm/DynamicRealm;Lio/realm/RealmResults;)Lrx/Observable;

    move-result-object v2

    .line 224
    .local v2, "results":Lrx/Observable;
    goto :goto_0

    .line 226
    .end local v0    # "dynamicRealm":Lio/realm/DynamicRealm;
    .end local v1    # "dynamicResults":Lio/realm/RealmResults;, "Lio/realm/RealmResults<Lio/realm/DynamicRealmObject;>;"
    .end local v2    # "results":Lrx/Observable;
    :cond_1
    new-instance v3, Ljava/lang/UnsupportedOperationException;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v5, p0, Lio/realm/RealmResults;->realm:Lio/realm/BaseRealm;

    invoke-virtual {v5}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " does not support RxJava."

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v3
.end method

.method public bridge synthetic average(Ljava/lang/String;)D
    .locals 2

    .prologue
    .line 53
    .local p0, "this":Lio/realm/RealmResults;, "Lio/realm/RealmResults<TE;>;"
    invoke-super {p0, p1}, Lio/realm/OrderedRealmCollectionImpl;->average(Ljava/lang/String;)D

    move-result-wide v0

    return-wide v0
.end method

.method public bridge synthetic clear()V
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 53
    .local p0, "this":Lio/realm/RealmResults;, "Lio/realm/RealmResults<TE;>;"
    invoke-super {p0}, Lio/realm/OrderedRealmCollectionImpl;->clear()V

    return-void
.end method

.method public bridge synthetic contains(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 53
    .local p0, "this":Lio/realm/RealmResults;, "Lio/realm/RealmResults<TE;>;"
    invoke-super {p0, p1}, Lio/realm/OrderedRealmCollectionImpl;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public bridge synthetic createSnapshot()Lio/realm/OrderedRealmCollectionSnapshot;
    .locals 1

    .prologue
    .line 53
    .local p0, "this":Lio/realm/RealmResults;, "Lio/realm/RealmResults<TE;>;"
    invoke-super {p0}, Lio/realm/OrderedRealmCollectionImpl;->createSnapshot()Lio/realm/OrderedRealmCollectionSnapshot;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic deleteAllFromRealm()Z
    .locals 1

    .prologue
    .line 53
    .local p0, "this":Lio/realm/RealmResults;, "Lio/realm/RealmResults<TE;>;"
    invoke-super {p0}, Lio/realm/OrderedRealmCollectionImpl;->deleteAllFromRealm()Z

    move-result v0

    return v0
.end method

.method public bridge synthetic deleteFirstFromRealm()Z
    .locals 1

    .prologue
    .line 53
    .local p0, "this":Lio/realm/RealmResults;, "Lio/realm/RealmResults<TE;>;"
    invoke-super {p0}, Lio/realm/OrderedRealmCollectionImpl;->deleteFirstFromRealm()Z

    move-result v0

    return v0
.end method

.method public bridge synthetic deleteFromRealm(I)V
    .locals 0

    .prologue
    .line 53
    .local p0, "this":Lio/realm/RealmResults;, "Lio/realm/RealmResults<TE;>;"
    invoke-super {p0, p1}, Lio/realm/OrderedRealmCollectionImpl;->deleteFromRealm(I)V

    return-void
.end method

.method public bridge synthetic deleteLastFromRealm()Z
    .locals 1

    .prologue
    .line 53
    .local p0, "this":Lio/realm/RealmResults;, "Lio/realm/RealmResults<TE;>;"
    invoke-super {p0}, Lio/realm/OrderedRealmCollectionImpl;->deleteLastFromRealm()Z

    move-result v0

    return v0
.end method

.method public distinct(Ljava/lang/String;)Lio/realm/RealmResults;
    .locals 3
    .param p1, "fieldName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lio/realm/RealmResults",
            "<TE;>;"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 236
    .local p0, "this":Lio/realm/RealmResults;, "Lio/realm/RealmResults<TE;>;"
    iget-object v2, p0, Lio/realm/RealmResults;->collection:Lio/realm/internal/Collection;

    invoke-virtual {v2}, Lio/realm/internal/Collection;->getTable()Lio/realm/internal/Table;

    move-result-object v2

    invoke-static {v2, p1}, Lio/realm/internal/SortDescriptor;->getInstanceForDistinct(Lio/realm/internal/Table;Ljava/lang/String;)Lio/realm/internal/SortDescriptor;

    move-result-object v1

    .line 237
    .local v1, "distinctDescriptor":Lio/realm/internal/SortDescriptor;
    iget-object v2, p0, Lio/realm/RealmResults;->collection:Lio/realm/internal/Collection;

    invoke-virtual {v2, v1}, Lio/realm/internal/Collection;->distinct(Lio/realm/internal/SortDescriptor;)Lio/realm/internal/Collection;

    move-result-object v0

    .line 238
    .local v0, "distinctCollection":Lio/realm/internal/Collection;
    invoke-virtual {p0, v0}, Lio/realm/RealmResults;->createLoadedResults(Lio/realm/internal/Collection;)Lio/realm/RealmResults;

    move-result-object v2

    return-object v2
.end method

.method public varargs distinct(Ljava/lang/String;[Ljava/lang/String;)Lio/realm/RealmResults;
    .locals 1
    .param p1, "firstFieldName"    # Ljava/lang/String;
    .param p2, "remainingFieldNames"    # [Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "[",
            "Ljava/lang/String;",
            ")",
            "Lio/realm/RealmResults",
            "<TE;>;"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 256
    .local p0, "this":Lio/realm/RealmResults;, "Lio/realm/RealmResults<TE;>;"
    invoke-virtual {p0}, Lio/realm/RealmResults;->where()Lio/realm/RealmQuery;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lio/realm/RealmQuery;->distinct(Ljava/lang/String;[Ljava/lang/String;)Lio/realm/RealmResults;

    move-result-object v0

    return-object v0
.end method

.method public distinctAsync(Ljava/lang/String;)Lio/realm/RealmResults;
    .locals 1
    .param p1, "fieldName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lio/realm/RealmResults",
            "<TE;>;"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 247
    .local p0, "this":Lio/realm/RealmResults;, "Lio/realm/RealmResults<TE;>;"
    invoke-virtual {p0}, Lio/realm/RealmResults;->where()Lio/realm/RealmQuery;

    move-result-object v0

    invoke-virtual {v0, p1}, Lio/realm/RealmQuery;->distinctAsync(Ljava/lang/String;)Lio/realm/RealmResults;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic first()Lio/realm/RealmModel;
    .locals 1

    .prologue
    .line 53
    .local p0, "this":Lio/realm/RealmResults;, "Lio/realm/RealmResults<TE;>;"
    invoke-super {p0}, Lio/realm/OrderedRealmCollectionImpl;->first()Lio/realm/RealmModel;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic first(Lio/realm/RealmModel;)Lio/realm/RealmModel;
    .locals 1

    .prologue
    .line 53
    .local p0, "this":Lio/realm/RealmResults;, "Lio/realm/RealmResults<TE;>;"
    invoke-super {p0, p1}, Lio/realm/OrderedRealmCollectionImpl;->first(Lio/realm/RealmModel;)Lio/realm/RealmModel;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get(I)Lio/realm/RealmModel;
    .locals 1

    .prologue
    .line 53
    .local p0, "this":Lio/realm/RealmResults;, "Lio/realm/RealmResults<TE;>;"
    invoke-super {p0, p1}, Lio/realm/OrderedRealmCollectionImpl;->get(I)Lio/realm/RealmModel;

    move-result-object v0

    return-object v0
.end method

.method public isLoaded()Z
    .locals 1

    .prologue
    .line 88
    .local p0, "this":Lio/realm/RealmResults;, "Lio/realm/RealmResults<TE;>;"
    iget-object v0, p0, Lio/realm/RealmResults;->realm:Lio/realm/BaseRealm;

    invoke-virtual {v0}, Lio/realm/BaseRealm;->checkIfValid()V

    .line 89
    iget-object v0, p0, Lio/realm/RealmResults;->collection:Lio/realm/internal/Collection;

    invoke-virtual {v0}, Lio/realm/internal/Collection;->isLoaded()Z

    move-result v0

    return v0
.end method

.method public bridge synthetic isManaged()Z
    .locals 1

    .prologue
    .line 53
    .local p0, "this":Lio/realm/RealmResults;, "Lio/realm/RealmResults<TE;>;"
    invoke-super {p0}, Lio/realm/OrderedRealmCollectionImpl;->isManaged()Z

    move-result v0

    return v0
.end method

.method public bridge synthetic isValid()Z
    .locals 1

    .prologue
    .line 53
    .local p0, "this":Lio/realm/RealmResults;, "Lio/realm/RealmResults<TE;>;"
    invoke-super {p0}, Lio/realm/OrderedRealmCollectionImpl;->isValid()Z

    move-result v0

    return v0
.end method

.method public bridge synthetic iterator()Ljava/util/Iterator;
    .locals 1

    .prologue
    .line 53
    .local p0, "this":Lio/realm/RealmResults;, "Lio/realm/RealmResults<TE;>;"
    invoke-super {p0}, Lio/realm/OrderedRealmCollectionImpl;->iterator()Ljava/util/Iterator;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic last()Lio/realm/RealmModel;
    .locals 1

    .prologue
    .line 53
    .local p0, "this":Lio/realm/RealmResults;, "Lio/realm/RealmResults<TE;>;"
    invoke-super {p0}, Lio/realm/OrderedRealmCollectionImpl;->last()Lio/realm/RealmModel;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic last(Lio/realm/RealmModel;)Lio/realm/RealmModel;
    .locals 1

    .prologue
    .line 53
    .local p0, "this":Lio/realm/RealmResults;, "Lio/realm/RealmResults<TE;>;"
    invoke-super {p0, p1}, Lio/realm/OrderedRealmCollectionImpl;->last(Lio/realm/RealmModel;)Lio/realm/RealmModel;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic listIterator()Ljava/util/ListIterator;
    .locals 1

    .prologue
    .line 53
    .local p0, "this":Lio/realm/RealmResults;, "Lio/realm/RealmResults<TE;>;"
    invoke-super {p0}, Lio/realm/OrderedRealmCollectionImpl;->listIterator()Ljava/util/ListIterator;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic listIterator(I)Ljava/util/ListIterator;
    .locals 1

    .prologue
    .line 53
    .local p0, "this":Lio/realm/RealmResults;, "Lio/realm/RealmResults<TE;>;"
    invoke-super {p0, p1}, Lio/realm/OrderedRealmCollectionImpl;->listIterator(I)Ljava/util/ListIterator;

    move-result-object v0

    return-object v0
.end method

.method public load()Z
    .locals 1

    .prologue
    .line 103
    .local p0, "this":Lio/realm/RealmResults;, "Lio/realm/RealmResults<TE;>;"
    iget-object v0, p0, Lio/realm/RealmResults;->realm:Lio/realm/BaseRealm;

    invoke-virtual {v0}, Lio/realm/BaseRealm;->checkIfValid()V

    .line 104
    iget-object v0, p0, Lio/realm/RealmResults;->collection:Lio/realm/internal/Collection;

    invoke-virtual {v0}, Lio/realm/internal/Collection;->load()V

    .line 105
    const/4 v0, 0x1

    return v0
.end method

.method public bridge synthetic max(Ljava/lang/String;)Ljava/lang/Number;
    .locals 1

    .prologue
    .line 53
    .local p0, "this":Lio/realm/RealmResults;, "Lio/realm/RealmResults<TE;>;"
    invoke-super {p0, p1}, Lio/realm/OrderedRealmCollectionImpl;->max(Ljava/lang/String;)Ljava/lang/Number;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic maxDate(Ljava/lang/String;)Ljava/util/Date;
    .locals 1

    .prologue
    .line 53
    .local p0, "this":Lio/realm/RealmResults;, "Lio/realm/RealmResults<TE;>;"
    invoke-super {p0, p1}, Lio/realm/OrderedRealmCollectionImpl;->maxDate(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic min(Ljava/lang/String;)Ljava/lang/Number;
    .locals 1

    .prologue
    .line 53
    .local p0, "this":Lio/realm/RealmResults;, "Lio/realm/RealmResults<TE;>;"
    invoke-super {p0, p1}, Lio/realm/OrderedRealmCollectionImpl;->min(Ljava/lang/String;)Ljava/lang/Number;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic minDate(Ljava/lang/String;)Ljava/util/Date;
    .locals 1

    .prologue
    .line 53
    .local p0, "this":Lio/realm/RealmResults;, "Lio/realm/RealmResults<TE;>;"
    invoke-super {p0, p1}, Lio/realm/OrderedRealmCollectionImpl;->minDate(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic remove(I)Lio/realm/RealmModel;
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 53
    .local p0, "this":Lio/realm/RealmResults;, "Lio/realm/RealmResults<TE;>;"
    invoke-super {p0, p1}, Lio/realm/OrderedRealmCollectionImpl;->remove(I)Lio/realm/RealmModel;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic remove(Ljava/lang/Object;)Z
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 53
    .local p0, "this":Lio/realm/RealmResults;, "Lio/realm/RealmResults<TE;>;"
    invoke-super {p0, p1}, Lio/realm/OrderedRealmCollectionImpl;->remove(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public bridge synthetic removeAll(Ljava/util/Collection;)Z
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 53
    .local p0, "this":Lio/realm/RealmResults;, "Lio/realm/RealmResults<TE;>;"
    invoke-super {p0, p1}, Lio/realm/OrderedRealmCollectionImpl;->removeAll(Ljava/util/Collection;)Z

    move-result v0

    return v0
.end method

.method public removeAllChangeListeners()V
    .locals 2

    .prologue
    .line 149
    .local p0, "this":Lio/realm/RealmResults;, "Lio/realm/RealmResults<TE;>;"
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lio/realm/RealmResults;->checkForAddRemoveListener(Ljava/lang/Object;Z)V

    .line 150
    iget-object v0, p0, Lio/realm/RealmResults;->collection:Lio/realm/internal/Collection;

    invoke-virtual {v0}, Lio/realm/internal/Collection;->removeAllListeners()V

    .line 151
    return-void
.end method

.method public removeChangeListener(Lio/realm/OrderedRealmCollectionChangeListener;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/realm/OrderedRealmCollectionChangeListener",
            "<",
            "Lio/realm/RealmResults",
            "<TE;>;>;)V"
        }
    .end annotation

    .prologue
    .line 184
    .local p0, "this":Lio/realm/RealmResults;, "Lio/realm/RealmResults<TE;>;"
    .local p1, "listener":Lio/realm/OrderedRealmCollectionChangeListener;, "Lio/realm/OrderedRealmCollectionChangeListener<Lio/realm/RealmResults<TE;>;>;"
    const/4 v0, 0x1

    invoke-direct {p0, p1, v0}, Lio/realm/RealmResults;->checkForAddRemoveListener(Ljava/lang/Object;Z)V

    .line 185
    iget-object v0, p0, Lio/realm/RealmResults;->collection:Lio/realm/internal/Collection;

    invoke-virtual {v0, p0, p1}, Lio/realm/internal/Collection;->removeListener(Ljava/lang/Object;Lio/realm/OrderedRealmCollectionChangeListener;)V

    .line 186
    return-void
.end method

.method public removeChangeListener(Lio/realm/RealmChangeListener;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/realm/RealmChangeListener",
            "<",
            "Lio/realm/RealmResults",
            "<TE;>;>;)V"
        }
    .end annotation

    .prologue
    .line 171
    .local p0, "this":Lio/realm/RealmResults;, "Lio/realm/RealmResults<TE;>;"
    .local p1, "listener":Lio/realm/RealmChangeListener;, "Lio/realm/RealmChangeListener<Lio/realm/RealmResults<TE;>;>;"
    const/4 v0, 0x1

    invoke-direct {p0, p1, v0}, Lio/realm/RealmResults;->checkForAddRemoveListener(Ljava/lang/Object;Z)V

    .line 172
    iget-object v0, p0, Lio/realm/RealmResults;->collection:Lio/realm/internal/Collection;

    invoke-virtual {v0, p0, p1}, Lio/realm/internal/Collection;->removeListener(Ljava/lang/Object;Lio/realm/RealmChangeListener;)V

    .line 173
    return-void
.end method

.method public removeChangeListeners()V
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 159
    .local p0, "this":Lio/realm/RealmResults;, "Lio/realm/RealmResults<TE;>;"
    invoke-virtual {p0}, Lio/realm/RealmResults;->removeAllChangeListeners()V

    .line 160
    return-void
.end method

.method public bridge synthetic retainAll(Ljava/util/Collection;)Z
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 53
    .local p0, "this":Lio/realm/RealmResults;, "Lio/realm/RealmResults<TE;>;"
    invoke-super {p0, p1}, Lio/realm/OrderedRealmCollectionImpl;->retainAll(Ljava/util/Collection;)Z

    move-result v0

    return v0
.end method

.method public bridge synthetic set(ILio/realm/RealmModel;)Lio/realm/RealmModel;
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 53
    .local p0, "this":Lio/realm/RealmResults;, "Lio/realm/RealmResults<TE;>;"
    invoke-super {p0, p1, p2}, Lio/realm/OrderedRealmCollectionImpl;->set(ILio/realm/RealmModel;)Lio/realm/RealmModel;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic size()I
    .locals 1

    .prologue
    .line 53
    .local p0, "this":Lio/realm/RealmResults;, "Lio/realm/RealmResults<TE;>;"
    invoke-super {p0}, Lio/realm/OrderedRealmCollectionImpl;->size()I

    move-result v0

    return v0
.end method

.method public bridge synthetic sort(Ljava/lang/String;)Lio/realm/RealmResults;
    .locals 1

    .prologue
    .line 53
    .local p0, "this":Lio/realm/RealmResults;, "Lio/realm/RealmResults<TE;>;"
    invoke-super {p0, p1}, Lio/realm/OrderedRealmCollectionImpl;->sort(Ljava/lang/String;)Lio/realm/RealmResults;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic sort(Ljava/lang/String;Lio/realm/Sort;)Lio/realm/RealmResults;
    .locals 1

    .prologue
    .line 53
    .local p0, "this":Lio/realm/RealmResults;, "Lio/realm/RealmResults<TE;>;"
    invoke-super {p0, p1, p2}, Lio/realm/OrderedRealmCollectionImpl;->sort(Ljava/lang/String;Lio/realm/Sort;)Lio/realm/RealmResults;

    move-result-object v0

    return-object v0
.end method

.method public sort(Ljava/lang/String;Lio/realm/Sort;Ljava/lang/String;Lio/realm/Sort;)Lio/realm/RealmResults;
    .locals 4
    .param p1, "fieldName1"    # Ljava/lang/String;
    .param p2, "sortOrder1"    # Lio/realm/Sort;
    .param p3, "fieldName2"    # Ljava/lang/String;
    .param p4, "sortOrder2"    # Lio/realm/Sort;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lio/realm/Sort;",
            "Ljava/lang/String;",
            "Lio/realm/Sort;",
            ")",
            "Lio/realm/RealmResults",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .local p0, "this":Lio/realm/RealmResults;, "Lio/realm/RealmResults<TE;>;"
    const/4 v1, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 78
    new-array v0, v1, [Ljava/lang/String;

    aput-object p1, v0, v2

    aput-object p3, v0, v3

    new-array v1, v1, [Lio/realm/Sort;

    aput-object p2, v1, v2

    aput-object p4, v1, v3

    invoke-virtual {p0, v0, v1}, Lio/realm/RealmResults;->sort([Ljava/lang/String;[Lio/realm/Sort;)Lio/realm/RealmResults;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic sort([Ljava/lang/String;[Lio/realm/Sort;)Lio/realm/RealmResults;
    .locals 1

    .prologue
    .line 53
    .local p0, "this":Lio/realm/RealmResults;, "Lio/realm/RealmResults<TE;>;"
    invoke-super {p0, p1, p2}, Lio/realm/OrderedRealmCollectionImpl;->sort([Ljava/lang/String;[Lio/realm/Sort;)Lio/realm/RealmResults;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic sum(Ljava/lang/String;)Ljava/lang/Number;
    .locals 1

    .prologue
    .line 53
    .local p0, "this":Lio/realm/RealmResults;, "Lio/realm/RealmResults<TE;>;"
    invoke-super {p0, p1}, Lio/realm/OrderedRealmCollectionImpl;->sum(Ljava/lang/String;)Ljava/lang/Number;

    move-result-object v0

    return-object v0
.end method

.method public where()Lio/realm/RealmQuery;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/realm/RealmQuery",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 68
    .local p0, "this":Lio/realm/RealmResults;, "Lio/realm/RealmResults<TE;>;"
    iget-object v0, p0, Lio/realm/RealmResults;->realm:Lio/realm/BaseRealm;

    invoke-virtual {v0}, Lio/realm/BaseRealm;->checkIfValid()V

    .line 69
    invoke-static {p0}, Lio/realm/RealmQuery;->createQueryFromResult(Lio/realm/RealmResults;)Lio/realm/RealmQuery;

    move-result-object v0

    return-object v0
.end method
