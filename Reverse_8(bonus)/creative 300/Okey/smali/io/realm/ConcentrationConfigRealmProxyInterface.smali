.class public interface abstract Lio/realm/ConcentrationConfigRealmProxyInterface;
.super Ljava/lang/Object;
.source "ConcentrationConfigRealmProxyInterface.java"


# virtual methods
.method public abstract realmGet$circlesCount()I
.end method

.method public abstract realmGet$circlesCountCustom()I
.end method

.method public abstract realmGet$circlesRadius()I
.end method

.method public abstract realmGet$circlesSizeCustom()I
.end method

.method public abstract realmGet$circlesSpeed()I
.end method

.method public abstract realmGet$circlesSpeedCustom()I
.end method

.method public abstract realmGet$complexity()I
.end method

.method public abstract realmGet$grayTime()I
.end method

.method public abstract realmGet$id()I
.end method

.method public abstract realmSet$circlesCount(I)V
.end method

.method public abstract realmSet$circlesCountCustom(I)V
.end method

.method public abstract realmSet$circlesRadius(I)V
.end method

.method public abstract realmSet$circlesSizeCustom(I)V
.end method

.method public abstract realmSet$circlesSpeed(I)V
.end method

.method public abstract realmSet$circlesSpeedCustom(I)V
.end method

.method public abstract realmSet$complexity(I)V
.end method

.method public abstract realmSet$grayTime(I)V
.end method

.method public abstract realmSet$id(I)V
.end method
