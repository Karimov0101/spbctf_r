.class public Lio/realm/LineOfSightResultRealmProxy;
.super Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightResult;
.source "LineOfSightResultRealmProxy.java"

# interfaces
.implements Lio/realm/internal/RealmObjectProxy;
.implements Lio/realm/LineOfSightResultRealmProxyInterface;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lio/realm/LineOfSightResultRealmProxy$LineOfSightResultColumnInfo;
    }
.end annotation


# static fields
.field private static final FIELD_NAMES:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private columnInfo:Lio/realm/LineOfSightResultRealmProxy$LineOfSightResultColumnInfo;

.field private proxyState:Lio/realm/ProxyState;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/realm/ProxyState",
            "<",
            "Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightResult;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 81
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 82
    .local v0, "fieldNames":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const-string v1, "id"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 83
    const-string v1, "mistakeCount"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 84
    const-string v1, "foundMistakeCount"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 85
    const-string v1, "unixTime"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 86
    const-string v1, "config"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 87
    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    sput-object v1, Lio/realm/LineOfSightResultRealmProxy;->FIELD_NAMES:Ljava/util/List;

    .line 88
    return-void
.end method

.method constructor <init>()V
    .locals 1

    .prologue
    .line 90
    invoke-direct {p0}, Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightResult;-><init>()V

    .line 91
    iget-object v0, p0, Lio/realm/LineOfSightResultRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v0}, Lio/realm/ProxyState;->setConstructionFinished()V

    .line 92
    return-void
.end method

.method public static copy(Lio/realm/Realm;Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightResult;ZLjava/util/Map;)Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightResult;
    .locals 8
    .param p0, "realm"    # Lio/realm/Realm;
    .param p1, "newObject"    # Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightResult;
    .param p2, "update"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/realm/Realm;",
            "Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightResult;",
            "Z",
            "Ljava/util/Map",
            "<",
            "Lio/realm/RealmModel;",
            "Lio/realm/internal/RealmObjectProxy;",
            ">;)",
            "Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightResult;"
        }
    .end annotation

    .prologue
    .line 528
    .local p3, "cache":Ljava/util/Map;, "Ljava/util/Map<Lio/realm/RealmModel;Lio/realm/internal/RealmObjectProxy;>;"
    invoke-interface {p3, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lio/realm/internal/RealmObjectProxy;

    .line 529
    .local v1, "cachedRealmObject":Lio/realm/internal/RealmObjectProxy;
    if-eqz v1, :cond_0

    .line 530
    check-cast v1, Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightResult;

    .line 550
    .end local v1    # "cachedRealmObject":Lio/realm/internal/RealmObjectProxy;
    .end local p1    # "newObject":Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightResult;
    :goto_0
    return-object v1

    .line 533
    .restart local v1    # "cachedRealmObject":Lio/realm/internal/RealmObjectProxy;
    .restart local p1    # "newObject":Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightResult;
    :cond_0
    const-class v5, Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightResult;

    move-object v4, p1

    check-cast v4, Lio/realm/LineOfSightResultRealmProxyInterface;

    invoke-interface {v4}, Lio/realm/LineOfSightResultRealmProxyInterface;->realmGet$id()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    const/4 v6, 0x0

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v7

    invoke-virtual {p0, v5, v4, v6, v7}, Lio/realm/Realm;->createObjectInternal(Ljava/lang/Class;Ljava/lang/Object;ZLjava/util/List;)Lio/realm/RealmModel;

    move-result-object v3

    check-cast v3, Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightResult;

    .local v3, "realmObject":Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightResult;
    move-object v4, v3

    .line 534
    check-cast v4, Lio/realm/internal/RealmObjectProxy;

    invoke-interface {p3, p1, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-object v4, v3

    .line 535
    check-cast v4, Lio/realm/LineOfSightResultRealmProxyInterface;

    move-object v5, p1

    check-cast v5, Lio/realm/LineOfSightResultRealmProxyInterface;

    invoke-interface {v5}, Lio/realm/LineOfSightResultRealmProxyInterface;->realmGet$mistakeCount()I

    move-result v5

    invoke-interface {v4, v5}, Lio/realm/LineOfSightResultRealmProxyInterface;->realmSet$mistakeCount(I)V

    move-object v4, v3

    .line 536
    check-cast v4, Lio/realm/LineOfSightResultRealmProxyInterface;

    move-object v5, p1

    check-cast v5, Lio/realm/LineOfSightResultRealmProxyInterface;

    invoke-interface {v5}, Lio/realm/LineOfSightResultRealmProxyInterface;->realmGet$foundMistakeCount()I

    move-result v5

    invoke-interface {v4, v5}, Lio/realm/LineOfSightResultRealmProxyInterface;->realmSet$foundMistakeCount(I)V

    move-object v4, v3

    .line 537
    check-cast v4, Lio/realm/LineOfSightResultRealmProxyInterface;

    move-object v5, p1

    check-cast v5, Lio/realm/LineOfSightResultRealmProxyInterface;

    invoke-interface {v5}, Lio/realm/LineOfSightResultRealmProxyInterface;->realmGet$unixTime()J

    move-result-wide v6

    invoke-interface {v4, v6, v7}, Lio/realm/LineOfSightResultRealmProxyInterface;->realmSet$unixTime(J)V

    .line 539
    check-cast p1, Lio/realm/LineOfSightResultRealmProxyInterface;

    .end local p1    # "newObject":Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightResult;
    invoke-interface {p1}, Lio/realm/LineOfSightResultRealmProxyInterface;->realmGet$config()Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightConfig;

    move-result-object v2

    .line 540
    .local v2, "configObj":Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightConfig;
    if-eqz v2, :cond_2

    .line 541
    invoke-interface {p3, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightConfig;

    .line 542
    .local v0, "cacheconfig":Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightConfig;
    if-eqz v0, :cond_1

    move-object v4, v3

    .line 543
    check-cast v4, Lio/realm/LineOfSightResultRealmProxyInterface;

    invoke-interface {v4, v0}, Lio/realm/LineOfSightResultRealmProxyInterface;->realmSet$config(Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightConfig;)V

    .end local v0    # "cacheconfig":Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightConfig;
    :goto_1
    move-object v1, v3

    .line 550
    goto :goto_0

    .restart local v0    # "cacheconfig":Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightConfig;
    :cond_1
    move-object v4, v3

    .line 545
    check-cast v4, Lio/realm/LineOfSightResultRealmProxyInterface;

    invoke-static {p0, v2, p2, p3}, Lio/realm/LineOfSightConfigRealmProxy;->copyOrUpdate(Lio/realm/Realm;Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightConfig;ZLjava/util/Map;)Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightConfig;

    move-result-object v5

    invoke-interface {v4, v5}, Lio/realm/LineOfSightResultRealmProxyInterface;->realmSet$config(Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightConfig;)V

    goto :goto_1

    .end local v0    # "cacheconfig":Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightConfig;
    :cond_2
    move-object v4, v3

    .line 548
    check-cast v4, Lio/realm/LineOfSightResultRealmProxyInterface;

    const/4 v5, 0x0

    invoke-interface {v4, v5}, Lio/realm/LineOfSightResultRealmProxyInterface;->realmSet$config(Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightConfig;)V

    goto :goto_1
.end method

.method public static copyOrUpdate(Lio/realm/Realm;Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightResult;ZLjava/util/Map;)Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightResult;
    .locals 19
    .param p0, "realm"    # Lio/realm/Realm;
    .param p1, "object"    # Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightResult;
    .param p2, "update"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/realm/Realm;",
            "Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightResult;",
            "Z",
            "Ljava/util/Map",
            "<",
            "Lio/realm/RealmModel;",
            "Lio/realm/internal/RealmObjectProxy;",
            ">;)",
            "Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightResult;"
        }
    .end annotation

    .prologue
    .line 489
    .local p3, "cache":Ljava/util/Map;, "Ljava/util/Map<Lio/realm/RealmModel;Lio/realm/internal/RealmObjectProxy;>;"
    move-object/from16 v0, p1

    instance-of v5, v0, Lio/realm/internal/RealmObjectProxy;

    if-eqz v5, :cond_0

    move-object/from16 v5, p1

    check-cast v5, Lio/realm/internal/RealmObjectProxy;

    invoke-interface {v5}, Lio/realm/internal/RealmObjectProxy;->realmGet$proxyState()Lio/realm/ProxyState;

    move-result-object v5

    invoke-virtual {v5}, Lio/realm/ProxyState;->getRealm$realm()Lio/realm/BaseRealm;

    move-result-object v5

    if-eqz v5, :cond_0

    move-object/from16 v5, p1

    check-cast v5, Lio/realm/internal/RealmObjectProxy;

    invoke-interface {v5}, Lio/realm/internal/RealmObjectProxy;->realmGet$proxyState()Lio/realm/ProxyState;

    move-result-object v5

    invoke-virtual {v5}, Lio/realm/ProxyState;->getRealm$realm()Lio/realm/BaseRealm;

    move-result-object v5

    iget-wide v6, v5, Lio/realm/BaseRealm;->threadId:J

    move-object/from16 v0, p0

    iget-wide v8, v0, Lio/realm/Realm;->threadId:J

    cmp-long v5, v6, v8

    if-eqz v5, :cond_0

    .line 490
    new-instance v5, Ljava/lang/IllegalArgumentException;

    const-string v6, "Objects which belong to Realm instances in other threads cannot be copied into this Realm instance."

    invoke-direct {v5, v6}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v5

    .line 492
    :cond_0
    move-object/from16 v0, p1

    instance-of v5, v0, Lio/realm/internal/RealmObjectProxy;

    if-eqz v5, :cond_1

    move-object/from16 v5, p1

    check-cast v5, Lio/realm/internal/RealmObjectProxy;

    invoke-interface {v5}, Lio/realm/internal/RealmObjectProxy;->realmGet$proxyState()Lio/realm/ProxyState;

    move-result-object v5

    invoke-virtual {v5}, Lio/realm/ProxyState;->getRealm$realm()Lio/realm/BaseRealm;

    move-result-object v5

    if-eqz v5, :cond_1

    move-object/from16 v5, p1

    check-cast v5, Lio/realm/internal/RealmObjectProxy;

    invoke-interface {v5}, Lio/realm/internal/RealmObjectProxy;->realmGet$proxyState()Lio/realm/ProxyState;

    move-result-object v5

    invoke-virtual {v5}, Lio/realm/ProxyState;->getRealm$realm()Lio/realm/BaseRealm;

    move-result-object v5

    invoke-virtual {v5}, Lio/realm/BaseRealm;->getPath()Ljava/lang/String;

    move-result-object v5

    invoke-virtual/range {p0 .. p0}, Lio/realm/Realm;->getPath()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 522
    .end local p1    # "object":Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightResult;
    :goto_0
    return-object p1

    .line 495
    .restart local p1    # "object":Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightResult;
    :cond_1
    sget-object v5, Lio/realm/BaseRealm;->objectContext:Lio/realm/BaseRealm$ThreadLocalRealmObjectContext;

    invoke-virtual {v5}, Lio/realm/BaseRealm$ThreadLocalRealmObjectContext;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lio/realm/BaseRealm$RealmObjectContext;

    .line 496
    .local v4, "objectContext":Lio/realm/BaseRealm$RealmObjectContext;
    move-object/from16 v0, p3

    move-object/from16 v1, p1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lio/realm/internal/RealmObjectProxy;

    .line 497
    .local v10, "cachedRealmObject":Lio/realm/internal/RealmObjectProxy;
    if-eqz v10, :cond_2

    .line 498
    check-cast v10, Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightResult;

    .end local v10    # "cachedRealmObject":Lio/realm/internal/RealmObjectProxy;
    move-object/from16 p1, v10

    goto :goto_0

    .line 500
    .restart local v10    # "cachedRealmObject":Lio/realm/internal/RealmObjectProxy;
    :cond_2
    const/4 v14, 0x0

    .line 501
    .local v14, "realmObject":Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightResult;
    move/from16 v11, p2

    .line 502
    .local v11, "canUpdate":Z
    if-eqz v11, :cond_3

    .line 503
    const-class v5, Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightResult;

    move-object/from16 v0, p0

    invoke-virtual {v0, v5}, Lio/realm/Realm;->getTable(Ljava/lang/Class;)Lio/realm/internal/Table;

    move-result-object v18

    .line 504
    .local v18, "table":Lio/realm/internal/Table;
    invoke-virtual/range {v18 .. v18}, Lio/realm/internal/Table;->getPrimaryKey()J

    move-result-wide v12

    .local v12, "pkColumnIndex":J
    move-object/from16 v5, p1

    .line 505
    check-cast v5, Lio/realm/LineOfSightResultRealmProxyInterface;

    invoke-interface {v5}, Lio/realm/LineOfSightResultRealmProxyInterface;->realmGet$id()I

    move-result v5

    int-to-long v6, v5

    move-object/from16 v0, v18

    invoke-virtual {v0, v12, v13, v6, v7}, Lio/realm/internal/Table;->findFirstLong(JJ)J

    move-result-wide v16

    .line 506
    .local v16, "rowIndex":J
    const-wide/16 v6, -0x1

    cmp-long v5, v16, v6

    if-eqz v5, :cond_4

    .line 508
    :try_start_0
    move-object/from16 v0, v18

    move-wide/from16 v1, v16

    invoke-virtual {v0, v1, v2}, Lio/realm/internal/Table;->getUncheckedRow(J)Lio/realm/internal/UncheckedRow;

    move-result-object v6

    move-object/from16 v0, p0

    iget-object v5, v0, Lio/realm/Realm;->schema:Lio/realm/RealmSchema;

    const-class v7, Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightResult;

    invoke-virtual {v5, v7}, Lio/realm/RealmSchema;->getColumnInfo(Ljava/lang/Class;)Lio/realm/internal/ColumnInfo;

    move-result-object v7

    const/4 v8, 0x0

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v9

    move-object/from16 v5, p0

    invoke-virtual/range {v4 .. v9}, Lio/realm/BaseRealm$RealmObjectContext;->set(Lio/realm/BaseRealm;Lio/realm/internal/Row;Lio/realm/internal/ColumnInfo;ZLjava/util/List;)V

    .line 509
    new-instance v15, Lio/realm/LineOfSightResultRealmProxy;

    invoke-direct {v15}, Lio/realm/LineOfSightResultRealmProxy;-><init>()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 510
    .end local v14    # "realmObject":Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightResult;
    .local v15, "realmObject":Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightResult;
    :try_start_1
    move-object v0, v15

    check-cast v0, Lio/realm/internal/RealmObjectProxy;

    move-object v5, v0

    move-object/from16 v0, p3

    move-object/from16 v1, p1

    invoke-interface {v0, v1, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 512
    invoke-virtual {v4}, Lio/realm/BaseRealm$RealmObjectContext;->clear()V

    move-object v14, v15

    .line 519
    .end local v12    # "pkColumnIndex":J
    .end local v15    # "realmObject":Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightResult;
    .end local v16    # "rowIndex":J
    .end local v18    # "table":Lio/realm/internal/Table;
    .restart local v14    # "realmObject":Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightResult;
    :cond_3
    :goto_1
    if-eqz v11, :cond_5

    .line 520
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p3

    invoke-static {v0, v14, v1, v2}, Lio/realm/LineOfSightResultRealmProxy;->update(Lio/realm/Realm;Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightResult;Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightResult;Ljava/util/Map;)Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightResult;

    move-result-object p1

    goto :goto_0

    .line 512
    .restart local v12    # "pkColumnIndex":J
    .restart local v16    # "rowIndex":J
    .restart local v18    # "table":Lio/realm/internal/Table;
    :catchall_0
    move-exception v5

    :goto_2
    invoke-virtual {v4}, Lio/realm/BaseRealm$RealmObjectContext;->clear()V

    throw v5

    .line 515
    :cond_4
    const/4 v11, 0x0

    goto :goto_1

    .line 522
    .end local v12    # "pkColumnIndex":J
    .end local v16    # "rowIndex":J
    .end local v18    # "table":Lio/realm/internal/Table;
    :cond_5
    invoke-static/range {p0 .. p3}, Lio/realm/LineOfSightResultRealmProxy;->copy(Lio/realm/Realm;Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightResult;ZLjava/util/Map;)Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightResult;

    move-result-object p1

    goto/16 :goto_0

    .line 512
    .end local v14    # "realmObject":Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightResult;
    .restart local v12    # "pkColumnIndex":J
    .restart local v15    # "realmObject":Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightResult;
    .restart local v16    # "rowIndex":J
    .restart local v18    # "table":Lio/realm/internal/Table;
    :catchall_1
    move-exception v5

    move-object v14, v15

    .end local v15    # "realmObject":Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightResult;
    .restart local v14    # "realmObject":Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightResult;
    goto :goto_2
.end method

.method public static createDetachedCopy(Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightResult;IILjava/util/Map;)Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightResult;
    .locals 6
    .param p0, "realmObject"    # Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightResult;
    .param p1, "currentDepth"    # I
    .param p2, "maxDepth"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightResult;",
            "II",
            "Ljava/util/Map",
            "<",
            "Lio/realm/RealmModel;",
            "Lio/realm/internal/RealmObjectProxy$CacheData",
            "<",
            "Lio/realm/RealmModel;",
            ">;>;)",
            "Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightResult;"
        }
    .end annotation

    .prologue
    .line 703
    .local p3, "cache":Ljava/util/Map;, "Ljava/util/Map<Lio/realm/RealmModel;Lio/realm/internal/RealmObjectProxy$CacheData<Lio/realm/RealmModel;>;>;"
    if-gt p1, p2, :cond_0

    if-nez p0, :cond_1

    .line 704
    :cond_0
    const/4 v2, 0x0

    .line 727
    .end local p0    # "realmObject":Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightResult;
    :goto_0
    return-object v2

    .line 706
    .restart local p0    # "realmObject":Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightResult;
    :cond_1
    invoke-interface {p3, p0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/realm/internal/RealmObjectProxy$CacheData;

    .line 708
    .local v0, "cachedObject":Lio/realm/internal/RealmObjectProxy$CacheData;, "Lio/realm/internal/RealmObjectProxy$CacheData<Lio/realm/RealmModel;>;"
    if-eqz v0, :cond_3

    .line 710
    iget v2, v0, Lio/realm/internal/RealmObjectProxy$CacheData;->minDepth:I

    if-lt p1, v2, :cond_2

    .line 711
    iget-object v2, v0, Lio/realm/internal/RealmObjectProxy$CacheData;->object:Lio/realm/RealmModel;

    check-cast v2, Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightResult;

    goto :goto_0

    .line 713
    :cond_2
    iget-object v1, v0, Lio/realm/internal/RealmObjectProxy$CacheData;->object:Lio/realm/RealmModel;

    check-cast v1, Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightResult;

    .line 714
    .local v1, "unmanagedObject":Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightResult;
    iput p1, v0, Lio/realm/internal/RealmObjectProxy$CacheData;->minDepth:I

    :goto_1
    move-object v2, v1

    .line 720
    check-cast v2, Lio/realm/LineOfSightResultRealmProxyInterface;

    move-object v3, p0

    check-cast v3, Lio/realm/LineOfSightResultRealmProxyInterface;

    invoke-interface {v3}, Lio/realm/LineOfSightResultRealmProxyInterface;->realmGet$id()I

    move-result v3

    invoke-interface {v2, v3}, Lio/realm/LineOfSightResultRealmProxyInterface;->realmSet$id(I)V

    move-object v2, v1

    .line 721
    check-cast v2, Lio/realm/LineOfSightResultRealmProxyInterface;

    move-object v3, p0

    check-cast v3, Lio/realm/LineOfSightResultRealmProxyInterface;

    invoke-interface {v3}, Lio/realm/LineOfSightResultRealmProxyInterface;->realmGet$mistakeCount()I

    move-result v3

    invoke-interface {v2, v3}, Lio/realm/LineOfSightResultRealmProxyInterface;->realmSet$mistakeCount(I)V

    move-object v2, v1

    .line 722
    check-cast v2, Lio/realm/LineOfSightResultRealmProxyInterface;

    move-object v3, p0

    check-cast v3, Lio/realm/LineOfSightResultRealmProxyInterface;

    invoke-interface {v3}, Lio/realm/LineOfSightResultRealmProxyInterface;->realmGet$foundMistakeCount()I

    move-result v3

    invoke-interface {v2, v3}, Lio/realm/LineOfSightResultRealmProxyInterface;->realmSet$foundMistakeCount(I)V

    move-object v2, v1

    .line 723
    check-cast v2, Lio/realm/LineOfSightResultRealmProxyInterface;

    move-object v3, p0

    check-cast v3, Lio/realm/LineOfSightResultRealmProxyInterface;

    invoke-interface {v3}, Lio/realm/LineOfSightResultRealmProxyInterface;->realmGet$unixTime()J

    move-result-wide v4

    invoke-interface {v2, v4, v5}, Lio/realm/LineOfSightResultRealmProxyInterface;->realmSet$unixTime(J)V

    move-object v2, v1

    .line 726
    check-cast v2, Lio/realm/LineOfSightResultRealmProxyInterface;

    check-cast p0, Lio/realm/LineOfSightResultRealmProxyInterface;

    .end local p0    # "realmObject":Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightResult;
    invoke-interface {p0}, Lio/realm/LineOfSightResultRealmProxyInterface;->realmGet$config()Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightConfig;

    move-result-object v3

    add-int/lit8 v4, p1, 0x1

    invoke-static {v3, v4, p2, p3}, Lio/realm/LineOfSightConfigRealmProxy;->createDetachedCopy(Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightConfig;IILjava/util/Map;)Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightConfig;

    move-result-object v3

    invoke-interface {v2, v3}, Lio/realm/LineOfSightResultRealmProxyInterface;->realmSet$config(Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightConfig;)V

    move-object v2, v1

    .line 727
    goto :goto_0

    .line 717
    .end local v1    # "unmanagedObject":Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightResult;
    .restart local p0    # "realmObject":Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightResult;
    :cond_3
    new-instance v1, Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightResult;

    invoke-direct {v1}, Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightResult;-><init>()V

    .line 718
    .restart local v1    # "unmanagedObject":Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightResult;
    new-instance v2, Lio/realm/internal/RealmObjectProxy$CacheData;

    invoke-direct {v2, p1, v1}, Lio/realm/internal/RealmObjectProxy$CacheData;-><init>(ILio/realm/RealmModel;)V

    invoke-interface {p3, p0, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1
.end method

.method public static createOrUpdateUsingJsonObject(Lio/realm/Realm;Lorg/json/JSONObject;Z)Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightResult;
    .locals 16
    .param p0, "realm"    # Lio/realm/Realm;
    .param p1, "json"    # Lorg/json/JSONObject;
    .param p2, "update"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .prologue
    .line 365
    new-instance v9, Ljava/util/ArrayList;

    const/4 v3, 0x1

    invoke-direct {v9, v3}, Ljava/util/ArrayList;-><init>(I)V

    .line 366
    .local v9, "excludeFields":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const/4 v10, 0x0

    .line 367
    .local v10, "obj":Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightResult;
    if-eqz p2, :cond_1

    .line 368
    const-class v3, Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightResult;

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Lio/realm/Realm;->getTable(Ljava/lang/Class;)Lio/realm/internal/Table;

    move-result-object v11

    .line 369
    .local v11, "table":Lio/realm/internal/Table;
    invoke-virtual {v11}, Lio/realm/internal/Table;->getPrimaryKey()J

    move-result-wide v12

    .line 370
    .local v12, "pkColumnIndex":J
    const-wide/16 v14, -0x1

    .line 371
    .local v14, "rowIndex":J
    const-string v3, "id"

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Lorg/json/JSONObject;->isNull(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 372
    const-string v3, "id"

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Lorg/json/JSONObject;->getLong(Ljava/lang/String;)J

    move-result-wide v4

    invoke-virtual {v11, v12, v13, v4, v5}, Lio/realm/internal/Table;->findFirstLong(JJ)J

    move-result-wide v14

    .line 374
    :cond_0
    const-wide/16 v4, -0x1

    cmp-long v3, v14, v4

    if-eqz v3, :cond_1

    .line 375
    sget-object v3, Lio/realm/BaseRealm;->objectContext:Lio/realm/BaseRealm$ThreadLocalRealmObjectContext;

    invoke-virtual {v3}, Lio/realm/BaseRealm$ThreadLocalRealmObjectContext;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lio/realm/BaseRealm$RealmObjectContext;

    .line 377
    .local v2, "objectContext":Lio/realm/BaseRealm$RealmObjectContext;
    :try_start_0
    invoke-virtual {v11, v14, v15}, Lio/realm/internal/Table;->getUncheckedRow(J)Lio/realm/internal/UncheckedRow;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v3, v0, Lio/realm/Realm;->schema:Lio/realm/RealmSchema;

    const-class v5, Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightResult;

    invoke-virtual {v3, v5}, Lio/realm/RealmSchema;->getColumnInfo(Ljava/lang/Class;)Lio/realm/internal/ColumnInfo;

    move-result-object v5

    const/4 v6, 0x0

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v7

    move-object/from16 v3, p0

    invoke-virtual/range {v2 .. v7}, Lio/realm/BaseRealm$RealmObjectContext;->set(Lio/realm/BaseRealm;Lio/realm/internal/Row;Lio/realm/internal/ColumnInfo;ZLjava/util/List;)V

    .line 378
    new-instance v10, Lio/realm/LineOfSightResultRealmProxy;

    .end local v10    # "obj":Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightResult;
    invoke-direct {v10}, Lio/realm/LineOfSightResultRealmProxy;-><init>()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 380
    .restart local v10    # "obj":Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightResult;
    invoke-virtual {v2}, Lio/realm/BaseRealm$RealmObjectContext;->clear()V

    .line 384
    .end local v2    # "objectContext":Lio/realm/BaseRealm$RealmObjectContext;
    .end local v11    # "table":Lio/realm/internal/Table;
    .end local v12    # "pkColumnIndex":J
    .end local v14    # "rowIndex":J
    :cond_1
    if-nez v10, :cond_3

    .line 385
    const-string v3, "config"

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 386
    const-string v3, "config"

    invoke-interface {v9, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 388
    :cond_2
    const-string v3, "id"

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_5

    .line 389
    const-string v3, "id"

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Lorg/json/JSONObject;->isNull(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 390
    const-class v3, Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightResult;

    const/4 v4, 0x0

    const/4 v5, 0x1

    move-object/from16 v0, p0

    invoke-virtual {v0, v3, v4, v5, v9}, Lio/realm/Realm;->createObjectInternal(Ljava/lang/Class;Ljava/lang/Object;ZLjava/util/List;)Lio/realm/RealmModel;

    move-result-object v10

    .end local v10    # "obj":Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightResult;
    check-cast v10, Lio/realm/LineOfSightResultRealmProxy;

    .line 398
    .restart local v10    # "obj":Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightResult;
    :cond_3
    :goto_0
    const-string v3, "mistakeCount"

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_7

    .line 399
    const-string v3, "mistakeCount"

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Lorg/json/JSONObject;->isNull(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_6

    .line 400
    new-instance v3, Ljava/lang/IllegalArgumentException;

    const-string v4, "Trying to set non-nullable field \'mistakeCount\' to null."

    invoke-direct {v3, v4}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 380
    .end local v10    # "obj":Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightResult;
    .restart local v2    # "objectContext":Lio/realm/BaseRealm$RealmObjectContext;
    .restart local v11    # "table":Lio/realm/internal/Table;
    .restart local v12    # "pkColumnIndex":J
    .restart local v14    # "rowIndex":J
    :catchall_0
    move-exception v3

    invoke-virtual {v2}, Lio/realm/BaseRealm$RealmObjectContext;->clear()V

    throw v3

    .line 392
    .end local v2    # "objectContext":Lio/realm/BaseRealm$RealmObjectContext;
    .end local v11    # "table":Lio/realm/internal/Table;
    .end local v12    # "pkColumnIndex":J
    .end local v14    # "rowIndex":J
    .restart local v10    # "obj":Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightResult;
    :cond_4
    const-class v3, Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightResult;

    const-string v4, "id"

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    const/4 v5, 0x1

    move-object/from16 v0, p0

    invoke-virtual {v0, v3, v4, v5, v9}, Lio/realm/Realm;->createObjectInternal(Ljava/lang/Class;Ljava/lang/Object;ZLjava/util/List;)Lio/realm/RealmModel;

    move-result-object v10

    .end local v10    # "obj":Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightResult;
    check-cast v10, Lio/realm/LineOfSightResultRealmProxy;

    .restart local v10    # "obj":Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightResult;
    goto :goto_0

    .line 395
    :cond_5
    new-instance v3, Ljava/lang/IllegalArgumentException;

    const-string v4, "JSON object doesn\'t have the primary key field \'id\'."

    invoke-direct {v3, v4}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v3

    :cond_6
    move-object v3, v10

    .line 402
    check-cast v3, Lio/realm/LineOfSightResultRealmProxyInterface;

    const-string v4, "mistakeCount"

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v4

    invoke-interface {v3, v4}, Lio/realm/LineOfSightResultRealmProxyInterface;->realmSet$mistakeCount(I)V

    .line 405
    :cond_7
    const-string v3, "foundMistakeCount"

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_9

    .line 406
    const-string v3, "foundMistakeCount"

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Lorg/json/JSONObject;->isNull(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_8

    .line 407
    new-instance v3, Ljava/lang/IllegalArgumentException;

    const-string v4, "Trying to set non-nullable field \'foundMistakeCount\' to null."

    invoke-direct {v3, v4}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v3

    :cond_8
    move-object v3, v10

    .line 409
    check-cast v3, Lio/realm/LineOfSightResultRealmProxyInterface;

    const-string v4, "foundMistakeCount"

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v4

    invoke-interface {v3, v4}, Lio/realm/LineOfSightResultRealmProxyInterface;->realmSet$foundMistakeCount(I)V

    .line 412
    :cond_9
    const-string v3, "unixTime"

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_b

    .line 413
    const-string v3, "unixTime"

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Lorg/json/JSONObject;->isNull(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_a

    .line 414
    new-instance v3, Ljava/lang/IllegalArgumentException;

    const-string v4, "Trying to set non-nullable field \'unixTime\' to null."

    invoke-direct {v3, v4}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v3

    :cond_a
    move-object v3, v10

    .line 416
    check-cast v3, Lio/realm/LineOfSightResultRealmProxyInterface;

    const-string v4, "unixTime"

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Lorg/json/JSONObject;->getLong(Ljava/lang/String;)J

    move-result-wide v4

    invoke-interface {v3, v4, v5}, Lio/realm/LineOfSightResultRealmProxyInterface;->realmSet$unixTime(J)V

    .line 419
    :cond_b
    const-string v3, "config"

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_c

    .line 420
    const-string v3, "config"

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Lorg/json/JSONObject;->isNull(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_d

    move-object v3, v10

    .line 421
    check-cast v3, Lio/realm/LineOfSightResultRealmProxyInterface;

    const/4 v4, 0x0

    invoke-interface {v3, v4}, Lio/realm/LineOfSightResultRealmProxyInterface;->realmSet$config(Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightConfig;)V

    .line 427
    :cond_c
    :goto_1
    return-object v10

    .line 423
    :cond_d
    const-string v3, "config"

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v3

    move-object/from16 v0, p0

    move/from16 v1, p2

    invoke-static {v0, v3, v1}, Lio/realm/LineOfSightConfigRealmProxy;->createOrUpdateUsingJsonObject(Lio/realm/Realm;Lorg/json/JSONObject;Z)Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightConfig;

    move-result-object v8

    .local v8, "configObj":Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightConfig;
    move-object v3, v10

    .line 424
    check-cast v3, Lio/realm/LineOfSightResultRealmProxyInterface;

    invoke-interface {v3, v8}, Lio/realm/LineOfSightResultRealmProxyInterface;->realmSet$config(Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightConfig;)V

    goto :goto_1
.end method

.method public static createRealmObjectSchema(Lio/realm/RealmSchema;)Lio/realm/RealmObjectSchema;
    .locals 11
    .param p0, "realmSchema"    # Lio/realm/RealmSchema;

    .prologue
    const/4 v3, 0x1

    const/4 v7, 0x0

    .line 234
    const-string v0, "LineOfSightResult"

    invoke-virtual {p0, v0}, Lio/realm/RealmSchema;->contains(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 235
    const-string v0, "LineOfSightResult"

    invoke-virtual {p0, v0}, Lio/realm/RealmSchema;->create(Ljava/lang/String;)Lio/realm/RealmObjectSchema;

    move-result-object v10

    .line 236
    .local v10, "realmObjectSchema":Lio/realm/RealmObjectSchema;
    new-instance v0, Lio/realm/Property;

    const-string v1, "id"

    sget-object v2, Lio/realm/RealmFieldType;->INTEGER:Lio/realm/RealmFieldType;

    move v4, v3

    move v5, v3

    invoke-direct/range {v0 .. v5}, Lio/realm/Property;-><init>(Ljava/lang/String;Lio/realm/RealmFieldType;ZZZ)V

    invoke-virtual {v10, v0}, Lio/realm/RealmObjectSchema;->add(Lio/realm/Property;)Lio/realm/RealmObjectSchema;

    .line 237
    new-instance v4, Lio/realm/Property;

    const-string v5, "mistakeCount"

    sget-object v6, Lio/realm/RealmFieldType;->INTEGER:Lio/realm/RealmFieldType;

    move v8, v7

    move v9, v3

    invoke-direct/range {v4 .. v9}, Lio/realm/Property;-><init>(Ljava/lang/String;Lio/realm/RealmFieldType;ZZZ)V

    invoke-virtual {v10, v4}, Lio/realm/RealmObjectSchema;->add(Lio/realm/Property;)Lio/realm/RealmObjectSchema;

    .line 238
    new-instance v4, Lio/realm/Property;

    const-string v5, "foundMistakeCount"

    sget-object v6, Lio/realm/RealmFieldType;->INTEGER:Lio/realm/RealmFieldType;

    move v8, v7

    move v9, v3

    invoke-direct/range {v4 .. v9}, Lio/realm/Property;-><init>(Ljava/lang/String;Lio/realm/RealmFieldType;ZZZ)V

    invoke-virtual {v10, v4}, Lio/realm/RealmObjectSchema;->add(Lio/realm/Property;)Lio/realm/RealmObjectSchema;

    .line 239
    new-instance v4, Lio/realm/Property;

    const-string v5, "unixTime"

    sget-object v6, Lio/realm/RealmFieldType;->INTEGER:Lio/realm/RealmFieldType;

    move v8, v7

    move v9, v3

    invoke-direct/range {v4 .. v9}, Lio/realm/Property;-><init>(Ljava/lang/String;Lio/realm/RealmFieldType;ZZZ)V

    invoke-virtual {v10, v4}, Lio/realm/RealmObjectSchema;->add(Lio/realm/Property;)Lio/realm/RealmObjectSchema;

    .line 240
    const-string v0, "LineOfSightConfig"

    invoke-virtual {p0, v0}, Lio/realm/RealmSchema;->contains(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 241
    invoke-static {p0}, Lio/realm/LineOfSightConfigRealmProxy;->createRealmObjectSchema(Lio/realm/RealmSchema;)Lio/realm/RealmObjectSchema;

    .line 243
    :cond_0
    new-instance v0, Lio/realm/Property;

    const-string v1, "config"

    sget-object v2, Lio/realm/RealmFieldType;->OBJECT:Lio/realm/RealmFieldType;

    const-string v3, "LineOfSightConfig"

    invoke-virtual {p0, v3}, Lio/realm/RealmSchema;->get(Ljava/lang/String;)Lio/realm/RealmObjectSchema;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lio/realm/Property;-><init>(Ljava/lang/String;Lio/realm/RealmFieldType;Lio/realm/RealmObjectSchema;)V

    invoke-virtual {v10, v0}, Lio/realm/RealmObjectSchema;->add(Lio/realm/Property;)Lio/realm/RealmObjectSchema;

    .line 246
    .end local v10    # "realmObjectSchema":Lio/realm/RealmObjectSchema;
    :goto_0
    return-object v10

    :cond_1
    const-string v0, "LineOfSightResult"

    invoke-virtual {p0, v0}, Lio/realm/RealmSchema;->get(Ljava/lang/String;)Lio/realm/RealmObjectSchema;

    move-result-object v10

    goto :goto_0
.end method

.method public static createUsingJsonStream(Lio/realm/Realm;Landroid/util/JsonReader;)Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightResult;
    .locals 8
    .param p0, "realm"    # Lio/realm/Realm;
    .param p1, "reader"    # Landroid/util/JsonReader;
    .annotation build Landroid/annotation/TargetApi;
        value = 0xb
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 434
    const/4 v1, 0x0

    .line 435
    .local v1, "jsonHasPrimaryKey":Z
    new-instance v3, Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightResult;

    invoke-direct {v3}, Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightResult;-><init>()V

    .line 436
    .local v3, "obj":Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightResult;
    invoke-virtual {p1}, Landroid/util/JsonReader;->beginObject()V

    .line 437
    :goto_0
    invoke-virtual {p1}, Landroid/util/JsonReader;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_a

    .line 438
    invoke-virtual {p1}, Landroid/util/JsonReader;->nextName()Ljava/lang/String;

    move-result-object v2

    .line 439
    .local v2, "name":Ljava/lang/String;
    const-string v4, "id"

    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 440
    invoke-virtual {p1}, Landroid/util/JsonReader;->peek()Landroid/util/JsonToken;

    move-result-object v4

    sget-object v5, Landroid/util/JsonToken;->NULL:Landroid/util/JsonToken;

    if-ne v4, v5, :cond_0

    .line 441
    invoke-virtual {p1}, Landroid/util/JsonReader;->skipValue()V

    .line 442
    new-instance v4, Ljava/lang/IllegalArgumentException;

    const-string v5, "Trying to set non-nullable field \'id\' to null."

    invoke-direct {v4, v5}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v4

    :cond_0
    move-object v4, v3

    .line 444
    check-cast v4, Lio/realm/LineOfSightResultRealmProxyInterface;

    invoke-virtual {p1}, Landroid/util/JsonReader;->nextInt()I

    move-result v5

    invoke-interface {v4, v5}, Lio/realm/LineOfSightResultRealmProxyInterface;->realmSet$id(I)V

    .line 446
    const/4 v1, 0x1

    goto :goto_0

    .line 447
    :cond_1
    const-string v4, "mistakeCount"

    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 448
    invoke-virtual {p1}, Landroid/util/JsonReader;->peek()Landroid/util/JsonToken;

    move-result-object v4

    sget-object v5, Landroid/util/JsonToken;->NULL:Landroid/util/JsonToken;

    if-ne v4, v5, :cond_2

    .line 449
    invoke-virtual {p1}, Landroid/util/JsonReader;->skipValue()V

    .line 450
    new-instance v4, Ljava/lang/IllegalArgumentException;

    const-string v5, "Trying to set non-nullable field \'mistakeCount\' to null."

    invoke-direct {v4, v5}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v4

    :cond_2
    move-object v4, v3

    .line 452
    check-cast v4, Lio/realm/LineOfSightResultRealmProxyInterface;

    invoke-virtual {p1}, Landroid/util/JsonReader;->nextInt()I

    move-result v5

    invoke-interface {v4, v5}, Lio/realm/LineOfSightResultRealmProxyInterface;->realmSet$mistakeCount(I)V

    goto :goto_0

    .line 454
    :cond_3
    const-string v4, "foundMistakeCount"

    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_5

    .line 455
    invoke-virtual {p1}, Landroid/util/JsonReader;->peek()Landroid/util/JsonToken;

    move-result-object v4

    sget-object v5, Landroid/util/JsonToken;->NULL:Landroid/util/JsonToken;

    if-ne v4, v5, :cond_4

    .line 456
    invoke-virtual {p1}, Landroid/util/JsonReader;->skipValue()V

    .line 457
    new-instance v4, Ljava/lang/IllegalArgumentException;

    const-string v5, "Trying to set non-nullable field \'foundMistakeCount\' to null."

    invoke-direct {v4, v5}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v4

    :cond_4
    move-object v4, v3

    .line 459
    check-cast v4, Lio/realm/LineOfSightResultRealmProxyInterface;

    invoke-virtual {p1}, Landroid/util/JsonReader;->nextInt()I

    move-result v5

    invoke-interface {v4, v5}, Lio/realm/LineOfSightResultRealmProxyInterface;->realmSet$foundMistakeCount(I)V

    goto :goto_0

    .line 461
    :cond_5
    const-string v4, "unixTime"

    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_7

    .line 462
    invoke-virtual {p1}, Landroid/util/JsonReader;->peek()Landroid/util/JsonToken;

    move-result-object v4

    sget-object v5, Landroid/util/JsonToken;->NULL:Landroid/util/JsonToken;

    if-ne v4, v5, :cond_6

    .line 463
    invoke-virtual {p1}, Landroid/util/JsonReader;->skipValue()V

    .line 464
    new-instance v4, Ljava/lang/IllegalArgumentException;

    const-string v5, "Trying to set non-nullable field \'unixTime\' to null."

    invoke-direct {v4, v5}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v4

    :cond_6
    move-object v4, v3

    .line 466
    check-cast v4, Lio/realm/LineOfSightResultRealmProxyInterface;

    invoke-virtual {p1}, Landroid/util/JsonReader;->nextLong()J

    move-result-wide v6

    invoke-interface {v4, v6, v7}, Lio/realm/LineOfSightResultRealmProxyInterface;->realmSet$unixTime(J)V

    goto/16 :goto_0

    .line 468
    :cond_7
    const-string v4, "config"

    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_9

    .line 469
    invoke-virtual {p1}, Landroid/util/JsonReader;->peek()Landroid/util/JsonToken;

    move-result-object v4

    sget-object v5, Landroid/util/JsonToken;->NULL:Landroid/util/JsonToken;

    if-ne v4, v5, :cond_8

    .line 470
    invoke-virtual {p1}, Landroid/util/JsonReader;->skipValue()V

    move-object v4, v3

    .line 471
    check-cast v4, Lio/realm/LineOfSightResultRealmProxyInterface;

    const/4 v5, 0x0

    invoke-interface {v4, v5}, Lio/realm/LineOfSightResultRealmProxyInterface;->realmSet$config(Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightConfig;)V

    goto/16 :goto_0

    .line 473
    :cond_8
    invoke-static {p0, p1}, Lio/realm/LineOfSightConfigRealmProxy;->createUsingJsonStream(Lio/realm/Realm;Landroid/util/JsonReader;)Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightConfig;

    move-result-object v0

    .local v0, "configObj":Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightConfig;
    move-object v4, v3

    .line 474
    check-cast v4, Lio/realm/LineOfSightResultRealmProxyInterface;

    invoke-interface {v4, v0}, Lio/realm/LineOfSightResultRealmProxyInterface;->realmSet$config(Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightConfig;)V

    goto/16 :goto_0

    .line 477
    .end local v0    # "configObj":Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightConfig;
    :cond_9
    invoke-virtual {p1}, Landroid/util/JsonReader;->skipValue()V

    goto/16 :goto_0

    .line 480
    .end local v2    # "name":Ljava/lang/String;
    :cond_a
    invoke-virtual {p1}, Landroid/util/JsonReader;->endObject()V

    .line 481
    if-nez v1, :cond_b

    .line 482
    new-instance v4, Ljava/lang/IllegalArgumentException;

    const-string v5, "JSON object doesn\'t have the primary key field \'id\'."

    invoke-direct {v4, v5}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 484
    :cond_b
    invoke-virtual {p0, v3}, Lio/realm/Realm;->copyToRealm(Lio/realm/RealmModel;)Lio/realm/RealmModel;

    move-result-object v3

    .end local v3    # "obj":Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightResult;
    check-cast v3, Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightResult;

    .line 485
    .restart local v3    # "obj":Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightResult;
    return-object v3
.end method

.method public static getFieldNames()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 359
    sget-object v0, Lio/realm/LineOfSightResultRealmProxy;->FIELD_NAMES:Ljava/util/List;

    return-object v0
.end method

.method public static getTableName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 355
    const-string v0, "class_LineOfSightResult"

    return-object v0
.end method

.method public static initTable(Lio/realm/internal/SharedRealm;)Lio/realm/internal/Table;
    .locals 4
    .param p0, "sharedRealm"    # Lio/realm/internal/SharedRealm;

    .prologue
    const/4 v3, 0x0

    .line 250
    const-string v1, "class_LineOfSightResult"

    invoke-virtual {p0, v1}, Lio/realm/internal/SharedRealm;->hasTable(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 251
    const-string v1, "class_LineOfSightResult"

    invoke-virtual {p0, v1}, Lio/realm/internal/SharedRealm;->getTable(Ljava/lang/String;)Lio/realm/internal/Table;

    move-result-object v0

    .line 252
    .local v0, "table":Lio/realm/internal/Table;
    sget-object v1, Lio/realm/RealmFieldType;->INTEGER:Lio/realm/RealmFieldType;

    const-string v2, "id"

    invoke-virtual {v0, v1, v2, v3}, Lio/realm/internal/Table;->addColumn(Lio/realm/RealmFieldType;Ljava/lang/String;Z)J

    .line 253
    sget-object v1, Lio/realm/RealmFieldType;->INTEGER:Lio/realm/RealmFieldType;

    const-string v2, "mistakeCount"

    invoke-virtual {v0, v1, v2, v3}, Lio/realm/internal/Table;->addColumn(Lio/realm/RealmFieldType;Ljava/lang/String;Z)J

    .line 254
    sget-object v1, Lio/realm/RealmFieldType;->INTEGER:Lio/realm/RealmFieldType;

    const-string v2, "foundMistakeCount"

    invoke-virtual {v0, v1, v2, v3}, Lio/realm/internal/Table;->addColumn(Lio/realm/RealmFieldType;Ljava/lang/String;Z)J

    .line 255
    sget-object v1, Lio/realm/RealmFieldType;->INTEGER:Lio/realm/RealmFieldType;

    const-string v2, "unixTime"

    invoke-virtual {v0, v1, v2, v3}, Lio/realm/internal/Table;->addColumn(Lio/realm/RealmFieldType;Ljava/lang/String;Z)J

    .line 256
    const-string v1, "class_LineOfSightConfig"

    invoke-virtual {p0, v1}, Lio/realm/internal/SharedRealm;->hasTable(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 257
    invoke-static {p0}, Lio/realm/LineOfSightConfigRealmProxy;->initTable(Lio/realm/internal/SharedRealm;)Lio/realm/internal/Table;

    .line 259
    :cond_0
    sget-object v1, Lio/realm/RealmFieldType;->OBJECT:Lio/realm/RealmFieldType;

    const-string v2, "config"

    const-string v3, "class_LineOfSightConfig"

    invoke-virtual {p0, v3}, Lio/realm/internal/SharedRealm;->getTable(Ljava/lang/String;)Lio/realm/internal/Table;

    move-result-object v3

    invoke-virtual {v0, v1, v2, v3}, Lio/realm/internal/Table;->addColumnLink(Lio/realm/RealmFieldType;Ljava/lang/String;Lio/realm/internal/Table;)J

    .line 260
    const-string v1, "id"

    invoke-virtual {v0, v1}, Lio/realm/internal/Table;->getColumnIndex(Ljava/lang/String;)J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lio/realm/internal/Table;->addSearchIndex(J)V

    .line 261
    const-string v1, "id"

    invoke-virtual {v0, v1}, Lio/realm/internal/Table;->setPrimaryKey(Ljava/lang/String;)V

    .line 264
    .end local v0    # "table":Lio/realm/internal/Table;
    :goto_0
    return-object v0

    :cond_1
    const-string v1, "class_LineOfSightResult"

    invoke-virtual {p0, v1}, Lio/realm/internal/SharedRealm;->getTable(Ljava/lang/String;)Lio/realm/internal/Table;

    move-result-object v0

    goto :goto_0
.end method

.method public static insert(Lio/realm/Realm;Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightResult;Ljava/util/Map;)J
    .locals 22
    .param p0, "realm"    # Lio/realm/Realm;
    .param p1, "object"    # Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightResult;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/realm/Realm;",
            "Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightResult;",
            "Ljava/util/Map",
            "<",
            "Lio/realm/RealmModel;",
            "Ljava/lang/Long;",
            ">;)J"
        }
    .end annotation

    .prologue
    .line 555
    .local p2, "cache":Ljava/util/Map;, "Ljava/util/Map<Lio/realm/RealmModel;Ljava/lang/Long;>;"
    move-object/from16 v0, p1

    instance-of v8, v0, Lio/realm/internal/RealmObjectProxy;

    if-eqz v8, :cond_1

    move-object/from16 v8, p1

    check-cast v8, Lio/realm/internal/RealmObjectProxy;

    invoke-interface {v8}, Lio/realm/internal/RealmObjectProxy;->realmGet$proxyState()Lio/realm/ProxyState;

    move-result-object v8

    invoke-virtual {v8}, Lio/realm/ProxyState;->getRealm$realm()Lio/realm/BaseRealm;

    move-result-object v8

    if-eqz v8, :cond_1

    move-object/from16 v8, p1

    check-cast v8, Lio/realm/internal/RealmObjectProxy;

    invoke-interface {v8}, Lio/realm/internal/RealmObjectProxy;->realmGet$proxyState()Lio/realm/ProxyState;

    move-result-object v8

    invoke-virtual {v8}, Lio/realm/ProxyState;->getRealm$realm()Lio/realm/BaseRealm;

    move-result-object v8

    invoke-virtual {v8}, Lio/realm/BaseRealm;->getPath()Ljava/lang/String;

    move-result-object v8

    invoke-virtual/range {p0 .. p0}, Lio/realm/Realm;->getPath()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_1

    .line 556
    check-cast p1, Lio/realm/internal/RealmObjectProxy;

    .end local p1    # "object":Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightResult;
    invoke-interface/range {p1 .. p1}, Lio/realm/internal/RealmObjectProxy;->realmGet$proxyState()Lio/realm/ProxyState;

    move-result-object v8

    invoke-virtual {v8}, Lio/realm/ProxyState;->getRow$realm()Lio/realm/internal/Row;

    move-result-object v8

    invoke-interface {v8}, Lio/realm/internal/Row;->getIndex()J

    move-result-wide v12

    .line 585
    :cond_0
    :goto_0
    return-wide v12

    .line 558
    .restart local p1    # "object":Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightResult;
    :cond_1
    const-class v8, Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightResult;

    move-object/from16 v0, p0

    invoke-virtual {v0, v8}, Lio/realm/Realm;->getTable(Ljava/lang/Class;)Lio/realm/internal/Table;

    move-result-object v21

    .line 559
    .local v21, "table":Lio/realm/internal/Table;
    invoke-virtual/range {v21 .. v21}, Lio/realm/internal/Table;->getNativeTablePointer()J

    move-result-wide v4

    .line 560
    .local v4, "tableNativePtr":J
    move-object/from16 v0, p0

    iget-object v8, v0, Lio/realm/Realm;->schema:Lio/realm/RealmSchema;

    const-class v9, Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightResult;

    invoke-virtual {v8, v9}, Lio/realm/RealmSchema;->getColumnInfo(Ljava/lang/Class;)Lio/realm/internal/ColumnInfo;

    move-result-object v18

    check-cast v18, Lio/realm/LineOfSightResultRealmProxy$LineOfSightResultColumnInfo;

    .line 561
    .local v18, "columnInfo":Lio/realm/LineOfSightResultRealmProxy$LineOfSightResultColumnInfo;
    invoke-virtual/range {v21 .. v21}, Lio/realm/internal/Table;->getPrimaryKey()J

    move-result-wide v6

    .line 562
    .local v6, "pkColumnIndex":J
    const-wide/16 v12, -0x1

    .local v12, "rowIndex":J
    move-object/from16 v8, p1

    .line 563
    check-cast v8, Lio/realm/LineOfSightResultRealmProxyInterface;

    invoke-interface {v8}, Lio/realm/LineOfSightResultRealmProxyInterface;->realmGet$id()I

    move-result v8

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v20

    .line 564
    .local v20, "primaryKeyValue":Ljava/lang/Integer;
    if-eqz v20, :cond_2

    move-object/from16 v8, p1

    .line 565
    check-cast v8, Lio/realm/LineOfSightResultRealmProxyInterface;

    invoke-interface {v8}, Lio/realm/LineOfSightResultRealmProxyInterface;->realmGet$id()I

    move-result v8

    int-to-long v8, v8

    invoke-static/range {v4 .. v9}, Lio/realm/internal/Table;->nativeFindFirstInt(JJJ)J

    move-result-wide v12

    .line 567
    :cond_2
    const-wide/16 v8, -0x1

    cmp-long v8, v12, v8

    if-nez v8, :cond_4

    move-object/from16 v8, p1

    .line 568
    check-cast v8, Lio/realm/LineOfSightResultRealmProxyInterface;

    invoke-interface {v8}, Lio/realm/LineOfSightResultRealmProxyInterface;->realmGet$id()I

    move-result v8

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    const/4 v9, 0x0

    move-object/from16 v0, v21

    invoke-virtual {v0, v8, v9}, Lio/realm/internal/Table;->addEmptyRowWithPrimaryKey(Ljava/lang/Object;Z)J

    move-result-wide v12

    .line 572
    :goto_1
    invoke-static {v12, v13}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    move-object/from16 v0, p2

    move-object/from16 v1, p1

    invoke-interface {v0, v1, v8}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 573
    move-object/from16 v0, v18

    iget-wide v10, v0, Lio/realm/LineOfSightResultRealmProxy$LineOfSightResultColumnInfo;->mistakeCountIndex:J

    move-object/from16 v8, p1

    check-cast v8, Lio/realm/LineOfSightResultRealmProxyInterface;

    invoke-interface {v8}, Lio/realm/LineOfSightResultRealmProxyInterface;->realmGet$mistakeCount()I

    move-result v8

    int-to-long v14, v8

    const/16 v16, 0x0

    move-wide v8, v4

    invoke-static/range {v8 .. v16}, Lio/realm/internal/Table;->nativeSetLong(JJJJZ)V

    .line 574
    move-object/from16 v0, v18

    iget-wide v10, v0, Lio/realm/LineOfSightResultRealmProxy$LineOfSightResultColumnInfo;->foundMistakeCountIndex:J

    move-object/from16 v8, p1

    check-cast v8, Lio/realm/LineOfSightResultRealmProxyInterface;

    invoke-interface {v8}, Lio/realm/LineOfSightResultRealmProxyInterface;->realmGet$foundMistakeCount()I

    move-result v8

    int-to-long v14, v8

    const/16 v16, 0x0

    move-wide v8, v4

    invoke-static/range {v8 .. v16}, Lio/realm/internal/Table;->nativeSetLong(JJJJZ)V

    .line 575
    move-object/from16 v0, v18

    iget-wide v10, v0, Lio/realm/LineOfSightResultRealmProxy$LineOfSightResultColumnInfo;->unixTimeIndex:J

    move-object/from16 v8, p1

    check-cast v8, Lio/realm/LineOfSightResultRealmProxyInterface;

    invoke-interface {v8}, Lio/realm/LineOfSightResultRealmProxyInterface;->realmGet$unixTime()J

    move-result-wide v14

    const/16 v16, 0x0

    move-wide v8, v4

    invoke-static/range {v8 .. v16}, Lio/realm/internal/Table;->nativeSetLong(JJJJZ)V

    .line 577
    check-cast p1, Lio/realm/LineOfSightResultRealmProxyInterface;

    .end local p1    # "object":Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightResult;
    invoke-interface/range {p1 .. p1}, Lio/realm/LineOfSightResultRealmProxyInterface;->realmGet$config()Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightConfig;

    move-result-object v19

    .line 578
    .local v19, "configObj":Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightConfig;
    if-eqz v19, :cond_0

    .line 579
    move-object/from16 v0, p2

    move-object/from16 v1, v19

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v17

    check-cast v17, Ljava/lang/Long;

    .line 580
    .local v17, "cacheconfig":Ljava/lang/Long;
    if-nez v17, :cond_3

    .line 581
    move-object/from16 v0, p0

    move-object/from16 v1, v19

    move-object/from16 v2, p2

    invoke-static {v0, v1, v2}, Lio/realm/LineOfSightConfigRealmProxy;->insert(Lio/realm/Realm;Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightConfig;Ljava/util/Map;)J

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v17

    .line 583
    :cond_3
    move-object/from16 v0, v18

    iget-wide v10, v0, Lio/realm/LineOfSightResultRealmProxy$LineOfSightResultColumnInfo;->configIndex:J

    invoke-virtual/range {v17 .. v17}, Ljava/lang/Long;->longValue()J

    move-result-wide v14

    const/16 v16, 0x0

    move-wide v8, v4

    invoke-static/range {v8 .. v16}, Lio/realm/internal/Table;->nativeSetLink(JJJJZ)V

    goto/16 :goto_0

    .line 570
    .end local v17    # "cacheconfig":Ljava/lang/Long;
    .end local v19    # "configObj":Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightConfig;
    .restart local p1    # "object":Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightResult;
    :cond_4
    invoke-static/range {v20 .. v20}, Lio/realm/internal/Table;->throwDuplicatePrimaryKeyException(Ljava/lang/Object;)V

    goto :goto_1
.end method

.method public static insert(Lio/realm/Realm;Ljava/util/Iterator;Ljava/util/Map;)V
    .locals 23
    .param p0, "realm"    # Lio/realm/Realm;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/realm/Realm;",
            "Ljava/util/Iterator",
            "<+",
            "Lio/realm/RealmModel;",
            ">;",
            "Ljava/util/Map",
            "<",
            "Lio/realm/RealmModel;",
            "Ljava/lang/Long;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 589
    .local p1, "objects":Ljava/util/Iterator;, "Ljava/util/Iterator<+Lio/realm/RealmModel;>;"
    .local p2, "cache":Ljava/util/Map;, "Ljava/util/Map<Lio/realm/RealmModel;Ljava/lang/Long;>;"
    const-class v8, Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightResult;

    move-object/from16 v0, p0

    invoke-virtual {v0, v8}, Lio/realm/Realm;->getTable(Ljava/lang/Class;)Lio/realm/internal/Table;

    move-result-object v22

    .line 590
    .local v22, "table":Lio/realm/internal/Table;
    invoke-virtual/range {v22 .. v22}, Lio/realm/internal/Table;->getNativeTablePointer()J

    move-result-wide v4

    .line 591
    .local v4, "tableNativePtr":J
    move-object/from16 v0, p0

    iget-object v8, v0, Lio/realm/Realm;->schema:Lio/realm/RealmSchema;

    const-class v9, Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightResult;

    invoke-virtual {v8, v9}, Lio/realm/RealmSchema;->getColumnInfo(Ljava/lang/Class;)Lio/realm/internal/ColumnInfo;

    move-result-object v18

    check-cast v18, Lio/realm/LineOfSightResultRealmProxy$LineOfSightResultColumnInfo;

    .line 592
    .local v18, "columnInfo":Lio/realm/LineOfSightResultRealmProxy$LineOfSightResultColumnInfo;
    invoke-virtual/range {v22 .. v22}, Lio/realm/internal/Table;->getPrimaryKey()J

    move-result-wide v6

    .line 593
    .local v6, "pkColumnIndex":J
    const/16 v20, 0x0

    .line 594
    .local v20, "object":Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightResult;
    :cond_0
    :goto_0
    invoke-interface/range {p1 .. p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_5

    .line 595
    invoke-interface/range {p1 .. p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v20

    .end local v20    # "object":Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightResult;
    check-cast v20, Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightResult;

    .line 596
    .restart local v20    # "object":Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightResult;
    move-object/from16 v0, p2

    move-object/from16 v1, v20

    invoke-interface {v0, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v8

    if-nez v8, :cond_0

    .line 597
    move-object/from16 v0, v20

    instance-of v8, v0, Lio/realm/internal/RealmObjectProxy;

    if-eqz v8, :cond_1

    move-object/from16 v8, v20

    check-cast v8, Lio/realm/internal/RealmObjectProxy;

    invoke-interface {v8}, Lio/realm/internal/RealmObjectProxy;->realmGet$proxyState()Lio/realm/ProxyState;

    move-result-object v8

    invoke-virtual {v8}, Lio/realm/ProxyState;->getRealm$realm()Lio/realm/BaseRealm;

    move-result-object v8

    if-eqz v8, :cond_1

    move-object/from16 v8, v20

    check-cast v8, Lio/realm/internal/RealmObjectProxy;

    invoke-interface {v8}, Lio/realm/internal/RealmObjectProxy;->realmGet$proxyState()Lio/realm/ProxyState;

    move-result-object v8

    invoke-virtual {v8}, Lio/realm/ProxyState;->getRealm$realm()Lio/realm/BaseRealm;

    move-result-object v8

    invoke-virtual {v8}, Lio/realm/BaseRealm;->getPath()Ljava/lang/String;

    move-result-object v8

    invoke-virtual/range {p0 .. p0}, Lio/realm/Realm;->getPath()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_1

    move-object/from16 v8, v20

    .line 598
    check-cast v8, Lio/realm/internal/RealmObjectProxy;

    invoke-interface {v8}, Lio/realm/internal/RealmObjectProxy;->realmGet$proxyState()Lio/realm/ProxyState;

    move-result-object v8

    invoke-virtual {v8}, Lio/realm/ProxyState;->getRow$realm()Lio/realm/internal/Row;

    move-result-object v8

    invoke-interface {v8}, Lio/realm/internal/Row;->getIndex()J

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    move-object/from16 v0, p2

    move-object/from16 v1, v20

    invoke-interface {v0, v1, v8}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 601
    :cond_1
    const-wide/16 v12, -0x1

    .local v12, "rowIndex":J
    move-object/from16 v8, v20

    .line 602
    check-cast v8, Lio/realm/LineOfSightResultRealmProxyInterface;

    invoke-interface {v8}, Lio/realm/LineOfSightResultRealmProxyInterface;->realmGet$id()I

    move-result v8

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v21

    .line 603
    .local v21, "primaryKeyValue":Ljava/lang/Integer;
    if-eqz v21, :cond_2

    move-object/from16 v8, v20

    .line 604
    check-cast v8, Lio/realm/LineOfSightResultRealmProxyInterface;

    invoke-interface {v8}, Lio/realm/LineOfSightResultRealmProxyInterface;->realmGet$id()I

    move-result v8

    int-to-long v8, v8

    invoke-static/range {v4 .. v9}, Lio/realm/internal/Table;->nativeFindFirstInt(JJJ)J

    move-result-wide v12

    .line 606
    :cond_2
    const-wide/16 v8, -0x1

    cmp-long v8, v12, v8

    if-nez v8, :cond_4

    move-object/from16 v8, v20

    .line 607
    check-cast v8, Lio/realm/LineOfSightResultRealmProxyInterface;

    invoke-interface {v8}, Lio/realm/LineOfSightResultRealmProxyInterface;->realmGet$id()I

    move-result v8

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    const/4 v9, 0x0

    move-object/from16 v0, v22

    invoke-virtual {v0, v8, v9}, Lio/realm/internal/Table;->addEmptyRowWithPrimaryKey(Ljava/lang/Object;Z)J

    move-result-wide v12

    .line 611
    :goto_1
    invoke-static {v12, v13}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    move-object/from16 v0, p2

    move-object/from16 v1, v20

    invoke-interface {v0, v1, v8}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 612
    move-object/from16 v0, v18

    iget-wide v10, v0, Lio/realm/LineOfSightResultRealmProxy$LineOfSightResultColumnInfo;->mistakeCountIndex:J

    move-object/from16 v8, v20

    check-cast v8, Lio/realm/LineOfSightResultRealmProxyInterface;

    invoke-interface {v8}, Lio/realm/LineOfSightResultRealmProxyInterface;->realmGet$mistakeCount()I

    move-result v8

    int-to-long v14, v8

    const/16 v16, 0x0

    move-wide v8, v4

    invoke-static/range {v8 .. v16}, Lio/realm/internal/Table;->nativeSetLong(JJJJZ)V

    .line 613
    move-object/from16 v0, v18

    iget-wide v10, v0, Lio/realm/LineOfSightResultRealmProxy$LineOfSightResultColumnInfo;->foundMistakeCountIndex:J

    move-object/from16 v8, v20

    check-cast v8, Lio/realm/LineOfSightResultRealmProxyInterface;

    invoke-interface {v8}, Lio/realm/LineOfSightResultRealmProxyInterface;->realmGet$foundMistakeCount()I

    move-result v8

    int-to-long v14, v8

    const/16 v16, 0x0

    move-wide v8, v4

    invoke-static/range {v8 .. v16}, Lio/realm/internal/Table;->nativeSetLong(JJJJZ)V

    .line 614
    move-object/from16 v0, v18

    iget-wide v10, v0, Lio/realm/LineOfSightResultRealmProxy$LineOfSightResultColumnInfo;->unixTimeIndex:J

    move-object/from16 v8, v20

    check-cast v8, Lio/realm/LineOfSightResultRealmProxyInterface;

    invoke-interface {v8}, Lio/realm/LineOfSightResultRealmProxyInterface;->realmGet$unixTime()J

    move-result-wide v14

    const/16 v16, 0x0

    move-wide v8, v4

    invoke-static/range {v8 .. v16}, Lio/realm/internal/Table;->nativeSetLong(JJJJZ)V

    move-object/from16 v8, v20

    .line 616
    check-cast v8, Lio/realm/LineOfSightResultRealmProxyInterface;

    invoke-interface {v8}, Lio/realm/LineOfSightResultRealmProxyInterface;->realmGet$config()Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightConfig;

    move-result-object v19

    .line 617
    .local v19, "configObj":Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightConfig;
    if-eqz v19, :cond_0

    .line 618
    move-object/from16 v0, p2

    move-object/from16 v1, v19

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v17

    check-cast v17, Ljava/lang/Long;

    .line 619
    .local v17, "cacheconfig":Ljava/lang/Long;
    if-nez v17, :cond_3

    .line 620
    move-object/from16 v0, p0

    move-object/from16 v1, v19

    move-object/from16 v2, p2

    invoke-static {v0, v1, v2}, Lio/realm/LineOfSightConfigRealmProxy;->insert(Lio/realm/Realm;Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightConfig;Ljava/util/Map;)J

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v17

    .line 622
    :cond_3
    move-object/from16 v0, v18

    iget-wide v10, v0, Lio/realm/LineOfSightResultRealmProxy$LineOfSightResultColumnInfo;->configIndex:J

    invoke-virtual/range {v17 .. v17}, Ljava/lang/Long;->longValue()J

    move-result-wide v14

    const/16 v16, 0x0

    move-object/from16 v9, v22

    invoke-virtual/range {v9 .. v16}, Lio/realm/internal/Table;->setLink(JJJZ)V

    goto/16 :goto_0

    .line 609
    .end local v17    # "cacheconfig":Ljava/lang/Long;
    .end local v19    # "configObj":Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightConfig;
    :cond_4
    invoke-static/range {v21 .. v21}, Lio/realm/internal/Table;->throwDuplicatePrimaryKeyException(Ljava/lang/Object;)V

    goto :goto_1

    .line 626
    .end local v12    # "rowIndex":J
    .end local v21    # "primaryKeyValue":Ljava/lang/Integer;
    :cond_5
    return-void
.end method

.method public static insertOrUpdate(Lio/realm/Realm;Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightResult;Ljava/util/Map;)J
    .locals 22
    .param p0, "realm"    # Lio/realm/Realm;
    .param p1, "object"    # Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightResult;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/realm/Realm;",
            "Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightResult;",
            "Ljava/util/Map",
            "<",
            "Lio/realm/RealmModel;",
            "Ljava/lang/Long;",
            ">;)J"
        }
    .end annotation

    .prologue
    .line 629
    .local p2, "cache":Ljava/util/Map;, "Ljava/util/Map<Lio/realm/RealmModel;Ljava/lang/Long;>;"
    move-object/from16 v0, p1

    instance-of v8, v0, Lio/realm/internal/RealmObjectProxy;

    if-eqz v8, :cond_0

    move-object/from16 v8, p1

    check-cast v8, Lio/realm/internal/RealmObjectProxy;

    invoke-interface {v8}, Lio/realm/internal/RealmObjectProxy;->realmGet$proxyState()Lio/realm/ProxyState;

    move-result-object v8

    invoke-virtual {v8}, Lio/realm/ProxyState;->getRealm$realm()Lio/realm/BaseRealm;

    move-result-object v8

    if-eqz v8, :cond_0

    move-object/from16 v8, p1

    check-cast v8, Lio/realm/internal/RealmObjectProxy;

    invoke-interface {v8}, Lio/realm/internal/RealmObjectProxy;->realmGet$proxyState()Lio/realm/ProxyState;

    move-result-object v8

    invoke-virtual {v8}, Lio/realm/ProxyState;->getRealm$realm()Lio/realm/BaseRealm;

    move-result-object v8

    invoke-virtual {v8}, Lio/realm/BaseRealm;->getPath()Ljava/lang/String;

    move-result-object v8

    invoke-virtual/range {p0 .. p0}, Lio/realm/Realm;->getPath()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_0

    .line 630
    check-cast p1, Lio/realm/internal/RealmObjectProxy;

    .end local p1    # "object":Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightResult;
    invoke-interface/range {p1 .. p1}, Lio/realm/internal/RealmObjectProxy;->realmGet$proxyState()Lio/realm/ProxyState;

    move-result-object v8

    invoke-virtual {v8}, Lio/realm/ProxyState;->getRow$realm()Lio/realm/internal/Row;

    move-result-object v8

    invoke-interface {v8}, Lio/realm/internal/Row;->getIndex()J

    move-result-wide v12

    .line 659
    :goto_0
    return-wide v12

    .line 632
    .restart local p1    # "object":Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightResult;
    :cond_0
    const-class v8, Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightResult;

    move-object/from16 v0, p0

    invoke-virtual {v0, v8}, Lio/realm/Realm;->getTable(Ljava/lang/Class;)Lio/realm/internal/Table;

    move-result-object v21

    .line 633
    .local v21, "table":Lio/realm/internal/Table;
    invoke-virtual/range {v21 .. v21}, Lio/realm/internal/Table;->getNativeTablePointer()J

    move-result-wide v4

    .line 634
    .local v4, "tableNativePtr":J
    move-object/from16 v0, p0

    iget-object v8, v0, Lio/realm/Realm;->schema:Lio/realm/RealmSchema;

    const-class v9, Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightResult;

    invoke-virtual {v8, v9}, Lio/realm/RealmSchema;->getColumnInfo(Ljava/lang/Class;)Lio/realm/internal/ColumnInfo;

    move-result-object v18

    check-cast v18, Lio/realm/LineOfSightResultRealmProxy$LineOfSightResultColumnInfo;

    .line 635
    .local v18, "columnInfo":Lio/realm/LineOfSightResultRealmProxy$LineOfSightResultColumnInfo;
    invoke-virtual/range {v21 .. v21}, Lio/realm/internal/Table;->getPrimaryKey()J

    move-result-wide v6

    .line 636
    .local v6, "pkColumnIndex":J
    const-wide/16 v12, -0x1

    .local v12, "rowIndex":J
    move-object/from16 v8, p1

    .line 637
    check-cast v8, Lio/realm/LineOfSightResultRealmProxyInterface;

    invoke-interface {v8}, Lio/realm/LineOfSightResultRealmProxyInterface;->realmGet$id()I

    move-result v8

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v20

    .line 638
    .local v20, "primaryKeyValue":Ljava/lang/Integer;
    if-eqz v20, :cond_1

    move-object/from16 v8, p1

    .line 639
    check-cast v8, Lio/realm/LineOfSightResultRealmProxyInterface;

    invoke-interface {v8}, Lio/realm/LineOfSightResultRealmProxyInterface;->realmGet$id()I

    move-result v8

    int-to-long v8, v8

    invoke-static/range {v4 .. v9}, Lio/realm/internal/Table;->nativeFindFirstInt(JJJ)J

    move-result-wide v12

    .line 641
    :cond_1
    const-wide/16 v8, -0x1

    cmp-long v8, v12, v8

    if-nez v8, :cond_2

    move-object/from16 v8, p1

    .line 642
    check-cast v8, Lio/realm/LineOfSightResultRealmProxyInterface;

    invoke-interface {v8}, Lio/realm/LineOfSightResultRealmProxyInterface;->realmGet$id()I

    move-result v8

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    const/4 v9, 0x0

    move-object/from16 v0, v21

    invoke-virtual {v0, v8, v9}, Lio/realm/internal/Table;->addEmptyRowWithPrimaryKey(Ljava/lang/Object;Z)J

    move-result-wide v12

    .line 644
    :cond_2
    invoke-static {v12, v13}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    move-object/from16 v0, p2

    move-object/from16 v1, p1

    invoke-interface {v0, v1, v8}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 645
    move-object/from16 v0, v18

    iget-wide v10, v0, Lio/realm/LineOfSightResultRealmProxy$LineOfSightResultColumnInfo;->mistakeCountIndex:J

    move-object/from16 v8, p1

    check-cast v8, Lio/realm/LineOfSightResultRealmProxyInterface;

    invoke-interface {v8}, Lio/realm/LineOfSightResultRealmProxyInterface;->realmGet$mistakeCount()I

    move-result v8

    int-to-long v14, v8

    const/16 v16, 0x0

    move-wide v8, v4

    invoke-static/range {v8 .. v16}, Lio/realm/internal/Table;->nativeSetLong(JJJJZ)V

    .line 646
    move-object/from16 v0, v18

    iget-wide v10, v0, Lio/realm/LineOfSightResultRealmProxy$LineOfSightResultColumnInfo;->foundMistakeCountIndex:J

    move-object/from16 v8, p1

    check-cast v8, Lio/realm/LineOfSightResultRealmProxyInterface;

    invoke-interface {v8}, Lio/realm/LineOfSightResultRealmProxyInterface;->realmGet$foundMistakeCount()I

    move-result v8

    int-to-long v14, v8

    const/16 v16, 0x0

    move-wide v8, v4

    invoke-static/range {v8 .. v16}, Lio/realm/internal/Table;->nativeSetLong(JJJJZ)V

    .line 647
    move-object/from16 v0, v18

    iget-wide v10, v0, Lio/realm/LineOfSightResultRealmProxy$LineOfSightResultColumnInfo;->unixTimeIndex:J

    move-object/from16 v8, p1

    check-cast v8, Lio/realm/LineOfSightResultRealmProxyInterface;

    invoke-interface {v8}, Lio/realm/LineOfSightResultRealmProxyInterface;->realmGet$unixTime()J

    move-result-wide v14

    const/16 v16, 0x0

    move-wide v8, v4

    invoke-static/range {v8 .. v16}, Lio/realm/internal/Table;->nativeSetLong(JJJJZ)V

    .line 649
    check-cast p1, Lio/realm/LineOfSightResultRealmProxyInterface;

    .end local p1    # "object":Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightResult;
    invoke-interface/range {p1 .. p1}, Lio/realm/LineOfSightResultRealmProxyInterface;->realmGet$config()Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightConfig;

    move-result-object v19

    .line 650
    .local v19, "configObj":Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightConfig;
    if-eqz v19, :cond_4

    .line 651
    move-object/from16 v0, p2

    move-object/from16 v1, v19

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v17

    check-cast v17, Ljava/lang/Long;

    .line 652
    .local v17, "cacheconfig":Ljava/lang/Long;
    if-nez v17, :cond_3

    .line 653
    move-object/from16 v0, p0

    move-object/from16 v1, v19

    move-object/from16 v2, p2

    invoke-static {v0, v1, v2}, Lio/realm/LineOfSightConfigRealmProxy;->insertOrUpdate(Lio/realm/Realm;Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightConfig;Ljava/util/Map;)J

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v17

    .line 655
    :cond_3
    move-object/from16 v0, v18

    iget-wide v10, v0, Lio/realm/LineOfSightResultRealmProxy$LineOfSightResultColumnInfo;->configIndex:J

    invoke-virtual/range {v17 .. v17}, Ljava/lang/Long;->longValue()J

    move-result-wide v14

    const/16 v16, 0x0

    move-wide v8, v4

    invoke-static/range {v8 .. v16}, Lio/realm/internal/Table;->nativeSetLink(JJJJZ)V

    goto/16 :goto_0

    .line 657
    .end local v17    # "cacheconfig":Ljava/lang/Long;
    :cond_4
    move-object/from16 v0, v18

    iget-wide v10, v0, Lio/realm/LineOfSightResultRealmProxy$LineOfSightResultColumnInfo;->configIndex:J

    move-wide v8, v4

    invoke-static/range {v8 .. v13}, Lio/realm/internal/Table;->nativeNullifyLink(JJJ)V

    goto/16 :goto_0
.end method

.method public static insertOrUpdate(Lio/realm/Realm;Ljava/util/Iterator;Ljava/util/Map;)V
    .locals 23
    .param p0, "realm"    # Lio/realm/Realm;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/realm/Realm;",
            "Ljava/util/Iterator",
            "<+",
            "Lio/realm/RealmModel;",
            ">;",
            "Ljava/util/Map",
            "<",
            "Lio/realm/RealmModel;",
            "Ljava/lang/Long;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 663
    .local p1, "objects":Ljava/util/Iterator;, "Ljava/util/Iterator<+Lio/realm/RealmModel;>;"
    .local p2, "cache":Ljava/util/Map;, "Ljava/util/Map<Lio/realm/RealmModel;Ljava/lang/Long;>;"
    const-class v8, Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightResult;

    move-object/from16 v0, p0

    invoke-virtual {v0, v8}, Lio/realm/Realm;->getTable(Ljava/lang/Class;)Lio/realm/internal/Table;

    move-result-object v22

    .line 664
    .local v22, "table":Lio/realm/internal/Table;
    invoke-virtual/range {v22 .. v22}, Lio/realm/internal/Table;->getNativeTablePointer()J

    move-result-wide v4

    .line 665
    .local v4, "tableNativePtr":J
    move-object/from16 v0, p0

    iget-object v8, v0, Lio/realm/Realm;->schema:Lio/realm/RealmSchema;

    const-class v9, Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightResult;

    invoke-virtual {v8, v9}, Lio/realm/RealmSchema;->getColumnInfo(Ljava/lang/Class;)Lio/realm/internal/ColumnInfo;

    move-result-object v18

    check-cast v18, Lio/realm/LineOfSightResultRealmProxy$LineOfSightResultColumnInfo;

    .line 666
    .local v18, "columnInfo":Lio/realm/LineOfSightResultRealmProxy$LineOfSightResultColumnInfo;
    invoke-virtual/range {v22 .. v22}, Lio/realm/internal/Table;->getPrimaryKey()J

    move-result-wide v6

    .line 667
    .local v6, "pkColumnIndex":J
    const/16 v20, 0x0

    .line 668
    .local v20, "object":Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightResult;
    :cond_0
    :goto_0
    invoke-interface/range {p1 .. p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_6

    .line 669
    invoke-interface/range {p1 .. p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v20

    .end local v20    # "object":Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightResult;
    check-cast v20, Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightResult;

    .line 670
    .restart local v20    # "object":Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightResult;
    move-object/from16 v0, p2

    move-object/from16 v1, v20

    invoke-interface {v0, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v8

    if-nez v8, :cond_0

    .line 671
    move-object/from16 v0, v20

    instance-of v8, v0, Lio/realm/internal/RealmObjectProxy;

    if-eqz v8, :cond_1

    move-object/from16 v8, v20

    check-cast v8, Lio/realm/internal/RealmObjectProxy;

    invoke-interface {v8}, Lio/realm/internal/RealmObjectProxy;->realmGet$proxyState()Lio/realm/ProxyState;

    move-result-object v8

    invoke-virtual {v8}, Lio/realm/ProxyState;->getRealm$realm()Lio/realm/BaseRealm;

    move-result-object v8

    if-eqz v8, :cond_1

    move-object/from16 v8, v20

    check-cast v8, Lio/realm/internal/RealmObjectProxy;

    invoke-interface {v8}, Lio/realm/internal/RealmObjectProxy;->realmGet$proxyState()Lio/realm/ProxyState;

    move-result-object v8

    invoke-virtual {v8}, Lio/realm/ProxyState;->getRealm$realm()Lio/realm/BaseRealm;

    move-result-object v8

    invoke-virtual {v8}, Lio/realm/BaseRealm;->getPath()Ljava/lang/String;

    move-result-object v8

    invoke-virtual/range {p0 .. p0}, Lio/realm/Realm;->getPath()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_1

    move-object/from16 v8, v20

    .line 672
    check-cast v8, Lio/realm/internal/RealmObjectProxy;

    invoke-interface {v8}, Lio/realm/internal/RealmObjectProxy;->realmGet$proxyState()Lio/realm/ProxyState;

    move-result-object v8

    invoke-virtual {v8}, Lio/realm/ProxyState;->getRow$realm()Lio/realm/internal/Row;

    move-result-object v8

    invoke-interface {v8}, Lio/realm/internal/Row;->getIndex()J

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    move-object/from16 v0, p2

    move-object/from16 v1, v20

    invoke-interface {v0, v1, v8}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 675
    :cond_1
    const-wide/16 v12, -0x1

    .local v12, "rowIndex":J
    move-object/from16 v8, v20

    .line 676
    check-cast v8, Lio/realm/LineOfSightResultRealmProxyInterface;

    invoke-interface {v8}, Lio/realm/LineOfSightResultRealmProxyInterface;->realmGet$id()I

    move-result v8

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v21

    .line 677
    .local v21, "primaryKeyValue":Ljava/lang/Integer;
    if-eqz v21, :cond_2

    move-object/from16 v8, v20

    .line 678
    check-cast v8, Lio/realm/LineOfSightResultRealmProxyInterface;

    invoke-interface {v8}, Lio/realm/LineOfSightResultRealmProxyInterface;->realmGet$id()I

    move-result v8

    int-to-long v8, v8

    invoke-static/range {v4 .. v9}, Lio/realm/internal/Table;->nativeFindFirstInt(JJJ)J

    move-result-wide v12

    .line 680
    :cond_2
    const-wide/16 v8, -0x1

    cmp-long v8, v12, v8

    if-nez v8, :cond_3

    move-object/from16 v8, v20

    .line 681
    check-cast v8, Lio/realm/LineOfSightResultRealmProxyInterface;

    invoke-interface {v8}, Lio/realm/LineOfSightResultRealmProxyInterface;->realmGet$id()I

    move-result v8

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    const/4 v9, 0x0

    move-object/from16 v0, v22

    invoke-virtual {v0, v8, v9}, Lio/realm/internal/Table;->addEmptyRowWithPrimaryKey(Ljava/lang/Object;Z)J

    move-result-wide v12

    .line 683
    :cond_3
    invoke-static {v12, v13}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    move-object/from16 v0, p2

    move-object/from16 v1, v20

    invoke-interface {v0, v1, v8}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 684
    move-object/from16 v0, v18

    iget-wide v10, v0, Lio/realm/LineOfSightResultRealmProxy$LineOfSightResultColumnInfo;->mistakeCountIndex:J

    move-object/from16 v8, v20

    check-cast v8, Lio/realm/LineOfSightResultRealmProxyInterface;

    invoke-interface {v8}, Lio/realm/LineOfSightResultRealmProxyInterface;->realmGet$mistakeCount()I

    move-result v8

    int-to-long v14, v8

    const/16 v16, 0x0

    move-wide v8, v4

    invoke-static/range {v8 .. v16}, Lio/realm/internal/Table;->nativeSetLong(JJJJZ)V

    .line 685
    move-object/from16 v0, v18

    iget-wide v10, v0, Lio/realm/LineOfSightResultRealmProxy$LineOfSightResultColumnInfo;->foundMistakeCountIndex:J

    move-object/from16 v8, v20

    check-cast v8, Lio/realm/LineOfSightResultRealmProxyInterface;

    invoke-interface {v8}, Lio/realm/LineOfSightResultRealmProxyInterface;->realmGet$foundMistakeCount()I

    move-result v8

    int-to-long v14, v8

    const/16 v16, 0x0

    move-wide v8, v4

    invoke-static/range {v8 .. v16}, Lio/realm/internal/Table;->nativeSetLong(JJJJZ)V

    .line 686
    move-object/from16 v0, v18

    iget-wide v10, v0, Lio/realm/LineOfSightResultRealmProxy$LineOfSightResultColumnInfo;->unixTimeIndex:J

    move-object/from16 v8, v20

    check-cast v8, Lio/realm/LineOfSightResultRealmProxyInterface;

    invoke-interface {v8}, Lio/realm/LineOfSightResultRealmProxyInterface;->realmGet$unixTime()J

    move-result-wide v14

    const/16 v16, 0x0

    move-wide v8, v4

    invoke-static/range {v8 .. v16}, Lio/realm/internal/Table;->nativeSetLong(JJJJZ)V

    move-object/from16 v8, v20

    .line 688
    check-cast v8, Lio/realm/LineOfSightResultRealmProxyInterface;

    invoke-interface {v8}, Lio/realm/LineOfSightResultRealmProxyInterface;->realmGet$config()Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightConfig;

    move-result-object v19

    .line 689
    .local v19, "configObj":Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightConfig;
    if-eqz v19, :cond_5

    .line 690
    move-object/from16 v0, p2

    move-object/from16 v1, v19

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v17

    check-cast v17, Ljava/lang/Long;

    .line 691
    .local v17, "cacheconfig":Ljava/lang/Long;
    if-nez v17, :cond_4

    .line 692
    move-object/from16 v0, p0

    move-object/from16 v1, v19

    move-object/from16 v2, p2

    invoke-static {v0, v1, v2}, Lio/realm/LineOfSightConfigRealmProxy;->insertOrUpdate(Lio/realm/Realm;Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightConfig;Ljava/util/Map;)J

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v17

    .line 694
    :cond_4
    move-object/from16 v0, v18

    iget-wide v10, v0, Lio/realm/LineOfSightResultRealmProxy$LineOfSightResultColumnInfo;->configIndex:J

    invoke-virtual/range {v17 .. v17}, Ljava/lang/Long;->longValue()J

    move-result-wide v14

    const/16 v16, 0x0

    move-wide v8, v4

    invoke-static/range {v8 .. v16}, Lio/realm/internal/Table;->nativeSetLink(JJJJZ)V

    goto/16 :goto_0

    .line 696
    .end local v17    # "cacheconfig":Ljava/lang/Long;
    :cond_5
    move-object/from16 v0, v18

    iget-wide v10, v0, Lio/realm/LineOfSightResultRealmProxy$LineOfSightResultColumnInfo;->configIndex:J

    move-wide v8, v4

    invoke-static/range {v8 .. v13}, Lio/realm/internal/Table;->nativeNullifyLink(JJJ)V

    goto/16 :goto_0

    .line 700
    .end local v12    # "rowIndex":J
    .end local v19    # "configObj":Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightConfig;
    .end local v21    # "primaryKeyValue":Ljava/lang/Integer;
    :cond_6
    return-void
.end method

.method static update(Lio/realm/Realm;Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightResult;Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightResult;Ljava/util/Map;)Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightResult;
    .locals 6
    .param p0, "realm"    # Lio/realm/Realm;
    .param p1, "realmObject"    # Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightResult;
    .param p2, "newObject"    # Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightResult;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/realm/Realm;",
            "Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightResult;",
            "Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightResult;",
            "Ljava/util/Map",
            "<",
            "Lio/realm/RealmModel;",
            "Lio/realm/internal/RealmObjectProxy;",
            ">;)",
            "Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightResult;"
        }
    .end annotation

    .prologue
    .line 731
    .local p3, "cache":Ljava/util/Map;, "Ljava/util/Map<Lio/realm/RealmModel;Lio/realm/internal/RealmObjectProxy;>;"
    move-object v2, p1

    check-cast v2, Lio/realm/LineOfSightResultRealmProxyInterface;

    move-object v3, p2

    check-cast v3, Lio/realm/LineOfSightResultRealmProxyInterface;

    invoke-interface {v3}, Lio/realm/LineOfSightResultRealmProxyInterface;->realmGet$mistakeCount()I

    move-result v3

    invoke-interface {v2, v3}, Lio/realm/LineOfSightResultRealmProxyInterface;->realmSet$mistakeCount(I)V

    move-object v2, p1

    .line 732
    check-cast v2, Lio/realm/LineOfSightResultRealmProxyInterface;

    move-object v3, p2

    check-cast v3, Lio/realm/LineOfSightResultRealmProxyInterface;

    invoke-interface {v3}, Lio/realm/LineOfSightResultRealmProxyInterface;->realmGet$foundMistakeCount()I

    move-result v3

    invoke-interface {v2, v3}, Lio/realm/LineOfSightResultRealmProxyInterface;->realmSet$foundMistakeCount(I)V

    move-object v2, p1

    .line 733
    check-cast v2, Lio/realm/LineOfSightResultRealmProxyInterface;

    move-object v3, p2

    check-cast v3, Lio/realm/LineOfSightResultRealmProxyInterface;

    invoke-interface {v3}, Lio/realm/LineOfSightResultRealmProxyInterface;->realmGet$unixTime()J

    move-result-wide v4

    invoke-interface {v2, v4, v5}, Lio/realm/LineOfSightResultRealmProxyInterface;->realmSet$unixTime(J)V

    .line 734
    check-cast p2, Lio/realm/LineOfSightResultRealmProxyInterface;

    .end local p2    # "newObject":Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightResult;
    invoke-interface {p2}, Lio/realm/LineOfSightResultRealmProxyInterface;->realmGet$config()Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightConfig;

    move-result-object v1

    .line 735
    .local v1, "configObj":Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightConfig;
    if-eqz v1, :cond_1

    .line 736
    invoke-interface {p3, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightConfig;

    .line 737
    .local v0, "cacheconfig":Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightConfig;
    if-eqz v0, :cond_0

    move-object v2, p1

    .line 738
    check-cast v2, Lio/realm/LineOfSightResultRealmProxyInterface;

    invoke-interface {v2, v0}, Lio/realm/LineOfSightResultRealmProxyInterface;->realmSet$config(Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightConfig;)V

    .line 745
    .end local v0    # "cacheconfig":Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightConfig;
    :goto_0
    return-object p1

    .restart local v0    # "cacheconfig":Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightConfig;
    :cond_0
    move-object v2, p1

    .line 740
    check-cast v2, Lio/realm/LineOfSightResultRealmProxyInterface;

    const/4 v3, 0x1

    invoke-static {p0, v1, v3, p3}, Lio/realm/LineOfSightConfigRealmProxy;->copyOrUpdate(Lio/realm/Realm;Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightConfig;ZLjava/util/Map;)Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightConfig;

    move-result-object v3

    invoke-interface {v2, v3}, Lio/realm/LineOfSightResultRealmProxyInterface;->realmSet$config(Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightConfig;)V

    goto :goto_0

    .end local v0    # "cacheconfig":Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightConfig;
    :cond_1
    move-object v2, p1

    .line 743
    check-cast v2, Lio/realm/LineOfSightResultRealmProxyInterface;

    const/4 v3, 0x0

    invoke-interface {v2, v3}, Lio/realm/LineOfSightResultRealmProxyInterface;->realmSet$config(Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightConfig;)V

    goto :goto_0
.end method

.method public static validateTable(Lio/realm/internal/SharedRealm;Z)Lio/realm/LineOfSightResultRealmProxy$LineOfSightResultColumnInfo;
    .locals 14
    .param p0, "sharedRealm"    # Lio/realm/internal/SharedRealm;
    .param p1, "allowExtraColumns"    # Z

    .prologue
    const-wide/16 v10, 0x5

    .line 268
    const-string v8, "class_LineOfSightResult"

    invoke-virtual {p0, v8}, Lio/realm/internal/SharedRealm;->hasTable(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_16

    .line 269
    const-string v8, "class_LineOfSightResult"

    invoke-virtual {p0, v8}, Lio/realm/internal/SharedRealm;->getTable(Ljava/lang/String;)Lio/realm/internal/Table;

    move-result-object v6

    .line 270
    .local v6, "table":Lio/realm/internal/Table;
    invoke-virtual {v6}, Lio/realm/internal/Table;->getColumnCount()J

    move-result-wide v0

    .line 271
    .local v0, "columnCount":J
    cmp-long v8, v0, v10

    if-eqz v8, :cond_1

    .line 272
    cmp-long v8, v0, v10

    if-gez v8, :cond_0

    .line 273
    new-instance v8, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->getPath()Ljava/lang/String;

    move-result-object v9

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "Field count is less than expected - expected 5 but was "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-direct {v8, v9, v10}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v8

    .line 275
    :cond_0
    if-eqz p1, :cond_2

    .line 276
    const-string v8, "Field count is more than expected - expected 5 but was %1$d"

    const/4 v9, 0x1

    new-array v9, v9, [Ljava/lang/Object;

    const/4 v10, 0x0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v11

    aput-object v11, v9, v10

    invoke-static {v8, v9}, Lio/realm/log/RealmLog;->debug(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 281
    :cond_1
    new-instance v3, Ljava/util/HashMap;

    invoke-direct {v3}, Ljava/util/HashMap;-><init>()V

    .line 282
    .local v3, "columnTypes":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lio/realm/RealmFieldType;>;"
    const-wide/16 v4, 0x0

    .local v4, "i":J
    :goto_0
    cmp-long v8, v4, v0

    if-gez v8, :cond_3

    .line 283
    invoke-virtual {v6, v4, v5}, Lio/realm/internal/Table;->getColumnName(J)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v4, v5}, Lio/realm/internal/Table;->getColumnType(J)Lio/realm/RealmFieldType;

    move-result-object v9

    invoke-interface {v3, v8, v9}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 282
    const-wide/16 v8, 0x1

    add-long/2addr v4, v8

    goto :goto_0

    .line 278
    .end local v3    # "columnTypes":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lio/realm/RealmFieldType;>;"
    .end local v4    # "i":J
    :cond_2
    new-instance v8, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->getPath()Ljava/lang/String;

    move-result-object v9

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "Field count is more than expected - expected 5 but was "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-direct {v8, v9, v10}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v8

    .line 286
    .restart local v3    # "columnTypes":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lio/realm/RealmFieldType;>;"
    .restart local v4    # "i":J
    :cond_3
    new-instance v2, Lio/realm/LineOfSightResultRealmProxy$LineOfSightResultColumnInfo;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->getPath()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v2, v8, v6}, Lio/realm/LineOfSightResultRealmProxy$LineOfSightResultColumnInfo;-><init>(Ljava/lang/String;Lio/realm/internal/Table;)V

    .line 288
    .local v2, "columnInfo":Lio/realm/LineOfSightResultRealmProxy$LineOfSightResultColumnInfo;
    invoke-virtual {v6}, Lio/realm/internal/Table;->hasPrimaryKey()Z

    move-result v8

    if-nez v8, :cond_4

    .line 289
    new-instance v8, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->getPath()Ljava/lang/String;

    move-result-object v9

    const-string v10, "Primary key not defined for field \'id\' in existing Realm file. @PrimaryKey was added."

    invoke-direct {v8, v9, v10}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v8

    .line 291
    :cond_4
    invoke-virtual {v6}, Lio/realm/internal/Table;->getPrimaryKey()J

    move-result-wide v8

    iget-wide v10, v2, Lio/realm/LineOfSightResultRealmProxy$LineOfSightResultColumnInfo;->idIndex:J

    cmp-long v8, v8, v10

    if-eqz v8, :cond_5

    .line 292
    new-instance v8, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->getPath()Ljava/lang/String;

    move-result-object v9

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "Primary Key annotation definition was changed, from field "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v6}, Lio/realm/internal/Table;->getPrimaryKey()J

    move-result-wide v12

    invoke-virtual {v6, v12, v13}, Lio/realm/internal/Table;->getColumnName(J)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, " to field id"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-direct {v8, v9, v10}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v8

    .line 296
    :cond_5
    const-string v8, "id"

    invoke-interface {v3, v8}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v8

    if-nez v8, :cond_6

    .line 297
    new-instance v8, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->getPath()Ljava/lang/String;

    move-result-object v9

    const-string v10, "Missing field \'id\' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn()."

    invoke-direct {v8, v9, v10}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v8

    .line 299
    :cond_6
    const-string v8, "id"

    invoke-interface {v3, v8}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    sget-object v9, Lio/realm/RealmFieldType;->INTEGER:Lio/realm/RealmFieldType;

    if-eq v8, v9, :cond_7

    .line 300
    new-instance v8, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->getPath()Ljava/lang/String;

    move-result-object v9

    const-string v10, "Invalid type \'int\' for field \'id\' in existing Realm file."

    invoke-direct {v8, v9, v10}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v8

    .line 302
    :cond_7
    iget-wide v8, v2, Lio/realm/LineOfSightResultRealmProxy$LineOfSightResultColumnInfo;->idIndex:J

    invoke-virtual {v6, v8, v9}, Lio/realm/internal/Table;->isColumnNullable(J)Z

    move-result v8

    if-eqz v8, :cond_8

    iget-wide v8, v2, Lio/realm/LineOfSightResultRealmProxy$LineOfSightResultColumnInfo;->idIndex:J

    invoke-virtual {v6, v8, v9}, Lio/realm/internal/Table;->findFirstNull(J)J

    move-result-wide v8

    const-wide/16 v10, -0x1

    cmp-long v8, v8, v10

    if-eqz v8, :cond_8

    .line 303
    new-instance v8, Ljava/lang/IllegalStateException;

    const-string v9, "Cannot migrate an object with null value in field \'id\'. Either maintain the same type for primary key field \'id\', or remove the object with null value before migration."

    invoke-direct {v8, v9}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v8

    .line 305
    :cond_8
    const-string v8, "id"

    invoke-virtual {v6, v8}, Lio/realm/internal/Table;->getColumnIndex(Ljava/lang/String;)J

    move-result-wide v8

    invoke-virtual {v6, v8, v9}, Lio/realm/internal/Table;->hasSearchIndex(J)Z

    move-result v8

    if-nez v8, :cond_9

    .line 306
    new-instance v8, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->getPath()Ljava/lang/String;

    move-result-object v9

    const-string v10, "Index not defined for field \'id\' in existing Realm file. Either set @Index or migrate using io.realm.internal.Table.removeSearchIndex()."

    invoke-direct {v8, v9, v10}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v8

    .line 308
    :cond_9
    const-string v8, "mistakeCount"

    invoke-interface {v3, v8}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v8

    if-nez v8, :cond_a

    .line 309
    new-instance v8, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->getPath()Ljava/lang/String;

    move-result-object v9

    const-string v10, "Missing field \'mistakeCount\' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn()."

    invoke-direct {v8, v9, v10}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v8

    .line 311
    :cond_a
    const-string v8, "mistakeCount"

    invoke-interface {v3, v8}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    sget-object v9, Lio/realm/RealmFieldType;->INTEGER:Lio/realm/RealmFieldType;

    if-eq v8, v9, :cond_b

    .line 312
    new-instance v8, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->getPath()Ljava/lang/String;

    move-result-object v9

    const-string v10, "Invalid type \'int\' for field \'mistakeCount\' in existing Realm file."

    invoke-direct {v8, v9, v10}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v8

    .line 314
    :cond_b
    iget-wide v8, v2, Lio/realm/LineOfSightResultRealmProxy$LineOfSightResultColumnInfo;->mistakeCountIndex:J

    invoke-virtual {v6, v8, v9}, Lio/realm/internal/Table;->isColumnNullable(J)Z

    move-result v8

    if-eqz v8, :cond_c

    .line 315
    new-instance v8, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->getPath()Ljava/lang/String;

    move-result-object v9

    const-string v10, "Field \'mistakeCount\' does support null values in the existing Realm file. Use corresponding boxed type for field \'mistakeCount\' or migrate using RealmObjectSchema.setNullable()."

    invoke-direct {v8, v9, v10}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v8

    .line 317
    :cond_c
    const-string v8, "foundMistakeCount"

    invoke-interface {v3, v8}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v8

    if-nez v8, :cond_d

    .line 318
    new-instance v8, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->getPath()Ljava/lang/String;

    move-result-object v9

    const-string v10, "Missing field \'foundMistakeCount\' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn()."

    invoke-direct {v8, v9, v10}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v8

    .line 320
    :cond_d
    const-string v8, "foundMistakeCount"

    invoke-interface {v3, v8}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    sget-object v9, Lio/realm/RealmFieldType;->INTEGER:Lio/realm/RealmFieldType;

    if-eq v8, v9, :cond_e

    .line 321
    new-instance v8, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->getPath()Ljava/lang/String;

    move-result-object v9

    const-string v10, "Invalid type \'int\' for field \'foundMistakeCount\' in existing Realm file."

    invoke-direct {v8, v9, v10}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v8

    .line 323
    :cond_e
    iget-wide v8, v2, Lio/realm/LineOfSightResultRealmProxy$LineOfSightResultColumnInfo;->foundMistakeCountIndex:J

    invoke-virtual {v6, v8, v9}, Lio/realm/internal/Table;->isColumnNullable(J)Z

    move-result v8

    if-eqz v8, :cond_f

    .line 324
    new-instance v8, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->getPath()Ljava/lang/String;

    move-result-object v9

    const-string v10, "Field \'foundMistakeCount\' does support null values in the existing Realm file. Use corresponding boxed type for field \'foundMistakeCount\' or migrate using RealmObjectSchema.setNullable()."

    invoke-direct {v8, v9, v10}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v8

    .line 326
    :cond_f
    const-string v8, "unixTime"

    invoke-interface {v3, v8}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v8

    if-nez v8, :cond_10

    .line 327
    new-instance v8, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->getPath()Ljava/lang/String;

    move-result-object v9

    const-string v10, "Missing field \'unixTime\' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn()."

    invoke-direct {v8, v9, v10}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v8

    .line 329
    :cond_10
    const-string v8, "unixTime"

    invoke-interface {v3, v8}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    sget-object v9, Lio/realm/RealmFieldType;->INTEGER:Lio/realm/RealmFieldType;

    if-eq v8, v9, :cond_11

    .line 330
    new-instance v8, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->getPath()Ljava/lang/String;

    move-result-object v9

    const-string v10, "Invalid type \'long\' for field \'unixTime\' in existing Realm file."

    invoke-direct {v8, v9, v10}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v8

    .line 332
    :cond_11
    iget-wide v8, v2, Lio/realm/LineOfSightResultRealmProxy$LineOfSightResultColumnInfo;->unixTimeIndex:J

    invoke-virtual {v6, v8, v9}, Lio/realm/internal/Table;->isColumnNullable(J)Z

    move-result v8

    if-eqz v8, :cond_12

    .line 333
    new-instance v8, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->getPath()Ljava/lang/String;

    move-result-object v9

    const-string v10, "Field \'unixTime\' does support null values in the existing Realm file. Use corresponding boxed type for field \'unixTime\' or migrate using RealmObjectSchema.setNullable()."

    invoke-direct {v8, v9, v10}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v8

    .line 335
    :cond_12
    const-string v8, "config"

    invoke-interface {v3, v8}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v8

    if-nez v8, :cond_13

    .line 336
    new-instance v8, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->getPath()Ljava/lang/String;

    move-result-object v9

    const-string v10, "Missing field \'config\' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn()."

    invoke-direct {v8, v9, v10}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v8

    .line 338
    :cond_13
    const-string v8, "config"

    invoke-interface {v3, v8}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    sget-object v9, Lio/realm/RealmFieldType;->OBJECT:Lio/realm/RealmFieldType;

    if-eq v8, v9, :cond_14

    .line 339
    new-instance v8, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->getPath()Ljava/lang/String;

    move-result-object v9

    const-string v10, "Invalid type \'LineOfSightConfig\' for field \'config\'"

    invoke-direct {v8, v9, v10}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v8

    .line 341
    :cond_14
    const-string v8, "class_LineOfSightConfig"

    invoke-virtual {p0, v8}, Lio/realm/internal/SharedRealm;->hasTable(Ljava/lang/String;)Z

    move-result v8

    if-nez v8, :cond_15

    .line 342
    new-instance v8, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->getPath()Ljava/lang/String;

    move-result-object v9

    const-string v10, "Missing class \'class_LineOfSightConfig\' for field \'config\'"

    invoke-direct {v8, v9, v10}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v8

    .line 344
    :cond_15
    const-string v8, "class_LineOfSightConfig"

    invoke-virtual {p0, v8}, Lio/realm/internal/SharedRealm;->getTable(Ljava/lang/String;)Lio/realm/internal/Table;

    move-result-object v7

    .line 345
    .local v7, "table_4":Lio/realm/internal/Table;
    iget-wide v8, v2, Lio/realm/LineOfSightResultRealmProxy$LineOfSightResultColumnInfo;->configIndex:J

    invoke-virtual {v6, v8, v9}, Lio/realm/internal/Table;->getLinkTarget(J)Lio/realm/internal/Table;

    move-result-object v8

    invoke-virtual {v8, v7}, Lio/realm/internal/Table;->hasSameSchema(Lio/realm/internal/Table;)Z

    move-result v8

    if-nez v8, :cond_17

    .line 346
    new-instance v8, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->getPath()Ljava/lang/String;

    move-result-object v9

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "Invalid RealmObject for field \'config\': \'"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    iget-wide v12, v2, Lio/realm/LineOfSightResultRealmProxy$LineOfSightResultColumnInfo;->configIndex:J

    invoke-virtual {v6, v12, v13}, Lio/realm/internal/Table;->getLinkTarget(J)Lio/realm/internal/Table;

    move-result-object v11

    invoke-virtual {v11}, Lio/realm/internal/Table;->getName()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, "\' expected - was \'"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v7}, Lio/realm/internal/Table;->getName()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, "\'"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-direct {v8, v9, v10}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v8

    .line 350
    .end local v0    # "columnCount":J
    .end local v2    # "columnInfo":Lio/realm/LineOfSightResultRealmProxy$LineOfSightResultColumnInfo;
    .end local v3    # "columnTypes":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lio/realm/RealmFieldType;>;"
    .end local v4    # "i":J
    .end local v6    # "table":Lio/realm/internal/Table;
    .end local v7    # "table_4":Lio/realm/internal/Table;
    :cond_16
    new-instance v8, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->getPath()Ljava/lang/String;

    move-result-object v9

    const-string v10, "The \'LineOfSightResult\' class is missing from the schema for this Realm."

    invoke-direct {v8, v9, v10}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v8

    .line 348
    .restart local v0    # "columnCount":J
    .restart local v2    # "columnInfo":Lio/realm/LineOfSightResultRealmProxy$LineOfSightResultColumnInfo;
    .restart local v3    # "columnTypes":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lio/realm/RealmFieldType;>;"
    .restart local v4    # "i":J
    .restart local v6    # "table":Lio/realm/internal/Table;
    .restart local v7    # "table_4":Lio/realm/internal/Table;
    :cond_17
    return-object v2
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 12
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v5, 0x1

    const/4 v6, 0x0

    .line 797
    if-ne p0, p1, :cond_1

    .line 811
    :cond_0
    :goto_0
    return v5

    .line 798
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v7

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v8

    if-eq v7, v8, :cond_3

    :cond_2
    move v5, v6

    goto :goto_0

    :cond_3
    move-object v0, p1

    .line 799
    check-cast v0, Lio/realm/LineOfSightResultRealmProxy;

    .line 801
    .local v0, "aLineOfSightResult":Lio/realm/LineOfSightResultRealmProxy;
    iget-object v7, p0, Lio/realm/LineOfSightResultRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v7}, Lio/realm/ProxyState;->getRealm$realm()Lio/realm/BaseRealm;

    move-result-object v7

    invoke-virtual {v7}, Lio/realm/BaseRealm;->getPath()Ljava/lang/String;

    move-result-object v3

    .line 802
    .local v3, "path":Ljava/lang/String;
    iget-object v7, v0, Lio/realm/LineOfSightResultRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v7}, Lio/realm/ProxyState;->getRealm$realm()Lio/realm/BaseRealm;

    move-result-object v7

    invoke-virtual {v7}, Lio/realm/BaseRealm;->getPath()Ljava/lang/String;

    move-result-object v1

    .line 803
    .local v1, "otherPath":Ljava/lang/String;
    if-eqz v3, :cond_5

    invoke-virtual {v3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_6

    :cond_4
    move v5, v6

    goto :goto_0

    :cond_5
    if-nez v1, :cond_4

    .line 805
    :cond_6
    iget-object v7, p0, Lio/realm/LineOfSightResultRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v7}, Lio/realm/ProxyState;->getRow$realm()Lio/realm/internal/Row;

    move-result-object v7

    invoke-interface {v7}, Lio/realm/internal/Row;->getTable()Lio/realm/internal/Table;

    move-result-object v7

    invoke-virtual {v7}, Lio/realm/internal/Table;->getName()Ljava/lang/String;

    move-result-object v4

    .line 806
    .local v4, "tableName":Ljava/lang/String;
    iget-object v7, v0, Lio/realm/LineOfSightResultRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v7}, Lio/realm/ProxyState;->getRow$realm()Lio/realm/internal/Row;

    move-result-object v7

    invoke-interface {v7}, Lio/realm/internal/Row;->getTable()Lio/realm/internal/Table;

    move-result-object v7

    invoke-virtual {v7}, Lio/realm/internal/Table;->getName()Ljava/lang/String;

    move-result-object v2

    .line 807
    .local v2, "otherTableName":Ljava/lang/String;
    if-eqz v4, :cond_8

    invoke-virtual {v4, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_9

    :cond_7
    move v5, v6

    goto :goto_0

    :cond_8
    if-nez v2, :cond_7

    .line 809
    :cond_9
    iget-object v7, p0, Lio/realm/LineOfSightResultRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v7}, Lio/realm/ProxyState;->getRow$realm()Lio/realm/internal/Row;

    move-result-object v7

    invoke-interface {v7}, Lio/realm/internal/Row;->getIndex()J

    move-result-wide v8

    iget-object v7, v0, Lio/realm/LineOfSightResultRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v7}, Lio/realm/ProxyState;->getRow$realm()Lio/realm/internal/Row;

    move-result-object v7

    invoke-interface {v7}, Lio/realm/internal/Row;->getIndex()J

    move-result-wide v10

    cmp-long v7, v8, v10

    if-eqz v7, :cond_0

    move v5, v6

    goto :goto_0
.end method

.method public hashCode()I
    .locals 8

    .prologue
    const/4 v5, 0x0

    .line 784
    iget-object v6, p0, Lio/realm/LineOfSightResultRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v6}, Lio/realm/ProxyState;->getRealm$realm()Lio/realm/BaseRealm;

    move-result-object v6

    invoke-virtual {v6}, Lio/realm/BaseRealm;->getPath()Ljava/lang/String;

    move-result-object v0

    .line 785
    .local v0, "realmName":Ljava/lang/String;
    iget-object v6, p0, Lio/realm/LineOfSightResultRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v6}, Lio/realm/ProxyState;->getRow$realm()Lio/realm/internal/Row;

    move-result-object v6

    invoke-interface {v6}, Lio/realm/internal/Row;->getTable()Lio/realm/internal/Table;

    move-result-object v6

    invoke-virtual {v6}, Lio/realm/internal/Table;->getName()Ljava/lang/String;

    move-result-object v4

    .line 786
    .local v4, "tableName":Ljava/lang/String;
    iget-object v6, p0, Lio/realm/LineOfSightResultRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v6}, Lio/realm/ProxyState;->getRow$realm()Lio/realm/internal/Row;

    move-result-object v6

    invoke-interface {v6}, Lio/realm/internal/Row;->getIndex()J

    move-result-wide v2

    .line 788
    .local v2, "rowIndex":J
    const/16 v1, 0x11

    .line 789
    .local v1, "result":I
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v6

    :goto_0
    add-int/lit16 v1, v6, 0x20f

    .line 790
    mul-int/lit8 v6, v1, 0x1f

    if-eqz v4, :cond_0

    invoke-virtual {v4}, Ljava/lang/String;->hashCode()I

    move-result v5

    :cond_0
    add-int v1, v6, v5

    .line 791
    mul-int/lit8 v5, v1, 0x1f

    const/16 v6, 0x20

    ushr-long v6, v2, v6

    xor-long/2addr v6, v2

    long-to-int v6, v6

    add-int v1, v5, v6

    .line 792
    return v1

    :cond_1
    move v6, v5

    .line 789
    goto :goto_0
.end method

.method public realm$injectObjectContext()V
    .locals 3

    .prologue
    .line 96
    iget-object v1, p0, Lio/realm/LineOfSightResultRealmProxy;->proxyState:Lio/realm/ProxyState;

    if-eqz v1, :cond_0

    .line 106
    :goto_0
    return-void

    .line 99
    :cond_0
    sget-object v1, Lio/realm/BaseRealm;->objectContext:Lio/realm/BaseRealm$ThreadLocalRealmObjectContext;

    invoke-virtual {v1}, Lio/realm/BaseRealm$ThreadLocalRealmObjectContext;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/realm/BaseRealm$RealmObjectContext;

    .line 100
    .local v0, "context":Lio/realm/BaseRealm$RealmObjectContext;
    invoke-virtual {v0}, Lio/realm/BaseRealm$RealmObjectContext;->getColumnInfo()Lio/realm/internal/ColumnInfo;

    move-result-object v1

    check-cast v1, Lio/realm/LineOfSightResultRealmProxy$LineOfSightResultColumnInfo;

    iput-object v1, p0, Lio/realm/LineOfSightResultRealmProxy;->columnInfo:Lio/realm/LineOfSightResultRealmProxy$LineOfSightResultColumnInfo;

    .line 101
    new-instance v1, Lio/realm/ProxyState;

    invoke-direct {v1, p0}, Lio/realm/ProxyState;-><init>(Lio/realm/RealmModel;)V

    iput-object v1, p0, Lio/realm/LineOfSightResultRealmProxy;->proxyState:Lio/realm/ProxyState;

    .line 102
    iget-object v1, p0, Lio/realm/LineOfSightResultRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v0}, Lio/realm/BaseRealm$RealmObjectContext;->getRealm()Lio/realm/BaseRealm;

    move-result-object v2

    invoke-virtual {v1, v2}, Lio/realm/ProxyState;->setRealm$realm(Lio/realm/BaseRealm;)V

    .line 103
    iget-object v1, p0, Lio/realm/LineOfSightResultRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v0}, Lio/realm/BaseRealm$RealmObjectContext;->getRow()Lio/realm/internal/Row;

    move-result-object v2

    invoke-virtual {v1, v2}, Lio/realm/ProxyState;->setRow$realm(Lio/realm/internal/Row;)V

    .line 104
    iget-object v1, p0, Lio/realm/LineOfSightResultRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v0}, Lio/realm/BaseRealm$RealmObjectContext;->getAcceptDefaultValue()Z

    move-result v2

    invoke-virtual {v1, v2}, Lio/realm/ProxyState;->setAcceptDefaultValue$realm(Z)V

    .line 105
    iget-object v1, p0, Lio/realm/LineOfSightResultRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v0}, Lio/realm/BaseRealm$RealmObjectContext;->getExcludeFields()Ljava/util/List;

    move-result-object v2

    invoke-virtual {v1, v2}, Lio/realm/ProxyState;->setExcludeFields$realm(Ljava/util/List;)V

    goto :goto_0
.end method

.method public realmGet$config()Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightConfig;
    .locals 6

    .prologue
    .line 185
    iget-object v0, p0, Lio/realm/LineOfSightResultRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v0}, Lio/realm/ProxyState;->getRealm$realm()Lio/realm/BaseRealm;

    move-result-object v0

    invoke-virtual {v0}, Lio/realm/BaseRealm;->checkIfValid()V

    .line 186
    iget-object v0, p0, Lio/realm/LineOfSightResultRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v0}, Lio/realm/ProxyState;->getRow$realm()Lio/realm/internal/Row;

    move-result-object v0

    iget-object v1, p0, Lio/realm/LineOfSightResultRealmProxy;->columnInfo:Lio/realm/LineOfSightResultRealmProxy$LineOfSightResultColumnInfo;

    iget-wide v2, v1, Lio/realm/LineOfSightResultRealmProxy$LineOfSightResultColumnInfo;->configIndex:J

    invoke-interface {v0, v2, v3}, Lio/realm/internal/Row;->isNullLink(J)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 187
    const/4 v0, 0x0

    .line 189
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lio/realm/LineOfSightResultRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v0}, Lio/realm/ProxyState;->getRealm$realm()Lio/realm/BaseRealm;

    move-result-object v0

    const-class v1, Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightConfig;

    iget-object v2, p0, Lio/realm/LineOfSightResultRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v2}, Lio/realm/ProxyState;->getRow$realm()Lio/realm/internal/Row;

    move-result-object v2

    iget-object v3, p0, Lio/realm/LineOfSightResultRealmProxy;->columnInfo:Lio/realm/LineOfSightResultRealmProxy$LineOfSightResultColumnInfo;

    iget-wide v4, v3, Lio/realm/LineOfSightResultRealmProxy$LineOfSightResultColumnInfo;->configIndex:J

    invoke-interface {v2, v4, v5}, Lio/realm/internal/Row;->getLink(J)J

    move-result-wide v2

    const/4 v4, 0x0

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v5

    invoke-virtual/range {v0 .. v5}, Lio/realm/BaseRealm;->get(Ljava/lang/Class;JZLjava/util/List;)Lio/realm/RealmModel;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightConfig;

    goto :goto_0
.end method

.method public realmGet$foundMistakeCount()I
    .locals 4

    .prologue
    .line 146
    iget-object v0, p0, Lio/realm/LineOfSightResultRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v0}, Lio/realm/ProxyState;->getRealm$realm()Lio/realm/BaseRealm;

    move-result-object v0

    invoke-virtual {v0}, Lio/realm/BaseRealm;->checkIfValid()V

    .line 147
    iget-object v0, p0, Lio/realm/LineOfSightResultRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v0}, Lio/realm/ProxyState;->getRow$realm()Lio/realm/internal/Row;

    move-result-object v0

    iget-object v1, p0, Lio/realm/LineOfSightResultRealmProxy;->columnInfo:Lio/realm/LineOfSightResultRealmProxy$LineOfSightResultColumnInfo;

    iget-wide v2, v1, Lio/realm/LineOfSightResultRealmProxy$LineOfSightResultColumnInfo;->foundMistakeCountIndex:J

    invoke-interface {v0, v2, v3}, Lio/realm/internal/Row;->getLong(J)J

    move-result-wide v0

    long-to-int v0, v0

    return v0
.end method

.method public realmGet$id()I
    .locals 4

    .prologue
    .line 110
    iget-object v0, p0, Lio/realm/LineOfSightResultRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v0}, Lio/realm/ProxyState;->getRealm$realm()Lio/realm/BaseRealm;

    move-result-object v0

    invoke-virtual {v0}, Lio/realm/BaseRealm;->checkIfValid()V

    .line 111
    iget-object v0, p0, Lio/realm/LineOfSightResultRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v0}, Lio/realm/ProxyState;->getRow$realm()Lio/realm/internal/Row;

    move-result-object v0

    iget-object v1, p0, Lio/realm/LineOfSightResultRealmProxy;->columnInfo:Lio/realm/LineOfSightResultRealmProxy$LineOfSightResultColumnInfo;

    iget-wide v2, v1, Lio/realm/LineOfSightResultRealmProxy$LineOfSightResultColumnInfo;->idIndex:J

    invoke-interface {v0, v2, v3}, Lio/realm/internal/Row;->getLong(J)J

    move-result-wide v0

    long-to-int v0, v0

    return v0
.end method

.method public realmGet$mistakeCount()I
    .locals 4

    .prologue
    .line 126
    iget-object v0, p0, Lio/realm/LineOfSightResultRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v0}, Lio/realm/ProxyState;->getRealm$realm()Lio/realm/BaseRealm;

    move-result-object v0

    invoke-virtual {v0}, Lio/realm/BaseRealm;->checkIfValid()V

    .line 127
    iget-object v0, p0, Lio/realm/LineOfSightResultRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v0}, Lio/realm/ProxyState;->getRow$realm()Lio/realm/internal/Row;

    move-result-object v0

    iget-object v1, p0, Lio/realm/LineOfSightResultRealmProxy;->columnInfo:Lio/realm/LineOfSightResultRealmProxy$LineOfSightResultColumnInfo;

    iget-wide v2, v1, Lio/realm/LineOfSightResultRealmProxy$LineOfSightResultColumnInfo;->mistakeCountIndex:J

    invoke-interface {v0, v2, v3}, Lio/realm/internal/Row;->getLong(J)J

    move-result-wide v0

    long-to-int v0, v0

    return v0
.end method

.method public realmGet$proxyState()Lio/realm/ProxyState;
    .locals 1

    .prologue
    .line 779
    iget-object v0, p0, Lio/realm/LineOfSightResultRealmProxy;->proxyState:Lio/realm/ProxyState;

    return-object v0
.end method

.method public realmGet$unixTime()J
    .locals 4

    .prologue
    .line 166
    iget-object v0, p0, Lio/realm/LineOfSightResultRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v0}, Lio/realm/ProxyState;->getRealm$realm()Lio/realm/BaseRealm;

    move-result-object v0

    invoke-virtual {v0}, Lio/realm/BaseRealm;->checkIfValid()V

    .line 167
    iget-object v0, p0, Lio/realm/LineOfSightResultRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v0}, Lio/realm/ProxyState;->getRow$realm()Lio/realm/internal/Row;

    move-result-object v0

    iget-object v1, p0, Lio/realm/LineOfSightResultRealmProxy;->columnInfo:Lio/realm/LineOfSightResultRealmProxy$LineOfSightResultColumnInfo;

    iget-wide v2, v1, Lio/realm/LineOfSightResultRealmProxy$LineOfSightResultColumnInfo;->unixTimeIndex:J

    invoke-interface {v0, v2, v3}, Lio/realm/internal/Row;->getLong(J)J

    move-result-wide v0

    return-wide v0
.end method

.method public realmSet$config(Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightConfig;)V
    .locals 9
    .param p1, "value"    # Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightConfig;

    .prologue
    .line 193
    iget-object v1, p0, Lio/realm/LineOfSightResultRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v1}, Lio/realm/ProxyState;->isUnderConstruction()Z

    move-result v1

    if-eqz v1, :cond_6

    .line 194
    iget-object v1, p0, Lio/realm/LineOfSightResultRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v1}, Lio/realm/ProxyState;->getAcceptDefaultValue$realm()Z

    move-result v1

    if-nez v1, :cond_1

    .line 231
    :cond_0
    :goto_0
    return-void

    .line 197
    :cond_1
    iget-object v1, p0, Lio/realm/LineOfSightResultRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v1}, Lio/realm/ProxyState;->getExcludeFields$realm()Ljava/util/List;

    move-result-object v1

    const-string v2, "config"

    invoke-interface {v1, v2}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 200
    if-eqz p1, :cond_2

    invoke-static {p1}, Lio/realm/RealmObject;->isManaged(Lio/realm/RealmModel;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 201
    iget-object v1, p0, Lio/realm/LineOfSightResultRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v1}, Lio/realm/ProxyState;->getRealm$realm()Lio/realm/BaseRealm;

    move-result-object v1

    check-cast v1, Lio/realm/Realm;

    invoke-virtual {v1, p1}, Lio/realm/Realm;->copyToRealm(Lio/realm/RealmModel;)Lio/realm/RealmModel;

    move-result-object p1

    .end local p1    # "value":Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightConfig;
    check-cast p1, Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightConfig;

    .line 203
    .restart local p1    # "value":Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightConfig;
    :cond_2
    iget-object v1, p0, Lio/realm/LineOfSightResultRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v1}, Lio/realm/ProxyState;->getRow$realm()Lio/realm/internal/Row;

    move-result-object v0

    .line 204
    .local v0, "row":Lio/realm/internal/Row;
    if-nez p1, :cond_3

    .line 206
    iget-object v1, p0, Lio/realm/LineOfSightResultRealmProxy;->columnInfo:Lio/realm/LineOfSightResultRealmProxy$LineOfSightResultColumnInfo;

    iget-wide v2, v1, Lio/realm/LineOfSightResultRealmProxy$LineOfSightResultColumnInfo;->configIndex:J

    invoke-interface {v0, v2, v3}, Lio/realm/internal/Row;->nullifyLink(J)V

    goto :goto_0

    .line 209
    :cond_3
    invoke-static {p1}, Lio/realm/RealmObject;->isValid(Lio/realm/RealmModel;)Z

    move-result v1

    if-nez v1, :cond_4

    .line 210
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "\'value\' is not a valid managed object."

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_4
    move-object v1, p1

    .line 212
    check-cast v1, Lio/realm/internal/RealmObjectProxy;

    invoke-interface {v1}, Lio/realm/internal/RealmObjectProxy;->realmGet$proxyState()Lio/realm/ProxyState;

    move-result-object v1

    invoke-virtual {v1}, Lio/realm/ProxyState;->getRealm$realm()Lio/realm/BaseRealm;

    move-result-object v1

    iget-object v2, p0, Lio/realm/LineOfSightResultRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v2}, Lio/realm/ProxyState;->getRealm$realm()Lio/realm/BaseRealm;

    move-result-object v2

    if-eq v1, v2, :cond_5

    .line 213
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "\'value\' belongs to a different Realm."

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 215
    :cond_5
    invoke-interface {v0}, Lio/realm/internal/Row;->getTable()Lio/realm/internal/Table;

    move-result-object v1

    iget-object v2, p0, Lio/realm/LineOfSightResultRealmProxy;->columnInfo:Lio/realm/LineOfSightResultRealmProxy$LineOfSightResultColumnInfo;

    iget-wide v2, v2, Lio/realm/LineOfSightResultRealmProxy$LineOfSightResultColumnInfo;->configIndex:J

    invoke-interface {v0}, Lio/realm/internal/Row;->getIndex()J

    move-result-wide v4

    move-object v6, p1

    check-cast v6, Lio/realm/internal/RealmObjectProxy;

    invoke-interface {v6}, Lio/realm/internal/RealmObjectProxy;->realmGet$proxyState()Lio/realm/ProxyState;

    move-result-object v6

    invoke-virtual {v6}, Lio/realm/ProxyState;->getRow$realm()Lio/realm/internal/Row;

    move-result-object v6

    invoke-interface {v6}, Lio/realm/internal/Row;->getIndex()J

    move-result-wide v6

    const/4 v8, 0x1

    invoke-virtual/range {v1 .. v8}, Lio/realm/internal/Table;->setLink(JJJZ)V

    goto :goto_0

    .line 219
    .end local v0    # "row":Lio/realm/internal/Row;
    :cond_6
    iget-object v1, p0, Lio/realm/LineOfSightResultRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v1}, Lio/realm/ProxyState;->getRealm$realm()Lio/realm/BaseRealm;

    move-result-object v1

    invoke-virtual {v1}, Lio/realm/BaseRealm;->checkIfValid()V

    .line 220
    if-nez p1, :cond_7

    .line 221
    iget-object v1, p0, Lio/realm/LineOfSightResultRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v1}, Lio/realm/ProxyState;->getRow$realm()Lio/realm/internal/Row;

    move-result-object v1

    iget-object v2, p0, Lio/realm/LineOfSightResultRealmProxy;->columnInfo:Lio/realm/LineOfSightResultRealmProxy$LineOfSightResultColumnInfo;

    iget-wide v2, v2, Lio/realm/LineOfSightResultRealmProxy$LineOfSightResultColumnInfo;->configIndex:J

    invoke-interface {v1, v2, v3}, Lio/realm/internal/Row;->nullifyLink(J)V

    goto/16 :goto_0

    .line 224
    :cond_7
    invoke-static {p1}, Lio/realm/RealmObject;->isManaged(Lio/realm/RealmModel;)Z

    move-result v1

    if-eqz v1, :cond_8

    invoke-static {p1}, Lio/realm/RealmObject;->isValid(Lio/realm/RealmModel;)Z

    move-result v1

    if-nez v1, :cond_9

    .line 225
    :cond_8
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "\'value\' is not a valid managed object."

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_9
    move-object v1, p1

    .line 227
    check-cast v1, Lio/realm/internal/RealmObjectProxy;

    invoke-interface {v1}, Lio/realm/internal/RealmObjectProxy;->realmGet$proxyState()Lio/realm/ProxyState;

    move-result-object v1

    invoke-virtual {v1}, Lio/realm/ProxyState;->getRealm$realm()Lio/realm/BaseRealm;

    move-result-object v1

    iget-object v2, p0, Lio/realm/LineOfSightResultRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v2}, Lio/realm/ProxyState;->getRealm$realm()Lio/realm/BaseRealm;

    move-result-object v2

    if-eq v1, v2, :cond_a

    .line 228
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "\'value\' belongs to a different Realm."

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 230
    :cond_a
    iget-object v1, p0, Lio/realm/LineOfSightResultRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v1}, Lio/realm/ProxyState;->getRow$realm()Lio/realm/internal/Row;

    move-result-object v2

    iget-object v1, p0, Lio/realm/LineOfSightResultRealmProxy;->columnInfo:Lio/realm/LineOfSightResultRealmProxy$LineOfSightResultColumnInfo;

    iget-wide v4, v1, Lio/realm/LineOfSightResultRealmProxy$LineOfSightResultColumnInfo;->configIndex:J

    move-object v1, p1

    check-cast v1, Lio/realm/internal/RealmObjectProxy;

    invoke-interface {v1}, Lio/realm/internal/RealmObjectProxy;->realmGet$proxyState()Lio/realm/ProxyState;

    move-result-object v1

    invoke-virtual {v1}, Lio/realm/ProxyState;->getRow$realm()Lio/realm/internal/Row;

    move-result-object v1

    invoke-interface {v1}, Lio/realm/internal/Row;->getIndex()J

    move-result-wide v6

    invoke-interface {v2, v4, v5, v6, v7}, Lio/realm/internal/Row;->setLink(JJ)V

    goto/16 :goto_0
.end method

.method public realmSet$foundMistakeCount(I)V
    .locals 9
    .param p1, "value"    # I

    .prologue
    .line 151
    iget-object v1, p0, Lio/realm/LineOfSightResultRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v1}, Lio/realm/ProxyState;->isUnderConstruction()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 152
    iget-object v1, p0, Lio/realm/LineOfSightResultRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v1}, Lio/realm/ProxyState;->getAcceptDefaultValue$realm()Z

    move-result v1

    if-nez v1, :cond_0

    .line 162
    :goto_0
    return-void

    .line 155
    :cond_0
    iget-object v1, p0, Lio/realm/LineOfSightResultRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v1}, Lio/realm/ProxyState;->getRow$realm()Lio/realm/internal/Row;

    move-result-object v0

    .line 156
    .local v0, "row":Lio/realm/internal/Row;
    invoke-interface {v0}, Lio/realm/internal/Row;->getTable()Lio/realm/internal/Table;

    move-result-object v1

    iget-object v2, p0, Lio/realm/LineOfSightResultRealmProxy;->columnInfo:Lio/realm/LineOfSightResultRealmProxy$LineOfSightResultColumnInfo;

    iget-wide v2, v2, Lio/realm/LineOfSightResultRealmProxy$LineOfSightResultColumnInfo;->foundMistakeCountIndex:J

    invoke-interface {v0}, Lio/realm/internal/Row;->getIndex()J

    move-result-wide v4

    int-to-long v6, p1

    const/4 v8, 0x1

    invoke-virtual/range {v1 .. v8}, Lio/realm/internal/Table;->setLong(JJJZ)V

    goto :goto_0

    .line 160
    .end local v0    # "row":Lio/realm/internal/Row;
    :cond_1
    iget-object v1, p0, Lio/realm/LineOfSightResultRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v1}, Lio/realm/ProxyState;->getRealm$realm()Lio/realm/BaseRealm;

    move-result-object v1

    invoke-virtual {v1}, Lio/realm/BaseRealm;->checkIfValid()V

    .line 161
    iget-object v1, p0, Lio/realm/LineOfSightResultRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v1}, Lio/realm/ProxyState;->getRow$realm()Lio/realm/internal/Row;

    move-result-object v1

    iget-object v2, p0, Lio/realm/LineOfSightResultRealmProxy;->columnInfo:Lio/realm/LineOfSightResultRealmProxy$LineOfSightResultColumnInfo;

    iget-wide v2, v2, Lio/realm/LineOfSightResultRealmProxy$LineOfSightResultColumnInfo;->foundMistakeCountIndex:J

    int-to-long v4, p1

    invoke-interface {v1, v2, v3, v4, v5}, Lio/realm/internal/Row;->setLong(JJ)V

    goto :goto_0
.end method

.method public realmSet$id(I)V
    .locals 2
    .param p1, "value"    # I

    .prologue
    .line 115
    iget-object v0, p0, Lio/realm/LineOfSightResultRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v0}, Lio/realm/ProxyState;->isUnderConstruction()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 117
    return-void

    .line 120
    :cond_0
    iget-object v0, p0, Lio/realm/LineOfSightResultRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v0}, Lio/realm/ProxyState;->getRealm$realm()Lio/realm/BaseRealm;

    move-result-object v0

    invoke-virtual {v0}, Lio/realm/BaseRealm;->checkIfValid()V

    .line 121
    new-instance v0, Lio/realm/exceptions/RealmException;

    const-string v1, "Primary key field \'id\' cannot be changed after object was created."

    invoke-direct {v0, v1}, Lio/realm/exceptions/RealmException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public realmSet$mistakeCount(I)V
    .locals 9
    .param p1, "value"    # I

    .prologue
    .line 131
    iget-object v1, p0, Lio/realm/LineOfSightResultRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v1}, Lio/realm/ProxyState;->isUnderConstruction()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 132
    iget-object v1, p0, Lio/realm/LineOfSightResultRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v1}, Lio/realm/ProxyState;->getAcceptDefaultValue$realm()Z

    move-result v1

    if-nez v1, :cond_0

    .line 142
    :goto_0
    return-void

    .line 135
    :cond_0
    iget-object v1, p0, Lio/realm/LineOfSightResultRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v1}, Lio/realm/ProxyState;->getRow$realm()Lio/realm/internal/Row;

    move-result-object v0

    .line 136
    .local v0, "row":Lio/realm/internal/Row;
    invoke-interface {v0}, Lio/realm/internal/Row;->getTable()Lio/realm/internal/Table;

    move-result-object v1

    iget-object v2, p0, Lio/realm/LineOfSightResultRealmProxy;->columnInfo:Lio/realm/LineOfSightResultRealmProxy$LineOfSightResultColumnInfo;

    iget-wide v2, v2, Lio/realm/LineOfSightResultRealmProxy$LineOfSightResultColumnInfo;->mistakeCountIndex:J

    invoke-interface {v0}, Lio/realm/internal/Row;->getIndex()J

    move-result-wide v4

    int-to-long v6, p1

    const/4 v8, 0x1

    invoke-virtual/range {v1 .. v8}, Lio/realm/internal/Table;->setLong(JJJZ)V

    goto :goto_0

    .line 140
    .end local v0    # "row":Lio/realm/internal/Row;
    :cond_1
    iget-object v1, p0, Lio/realm/LineOfSightResultRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v1}, Lio/realm/ProxyState;->getRealm$realm()Lio/realm/BaseRealm;

    move-result-object v1

    invoke-virtual {v1}, Lio/realm/BaseRealm;->checkIfValid()V

    .line 141
    iget-object v1, p0, Lio/realm/LineOfSightResultRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v1}, Lio/realm/ProxyState;->getRow$realm()Lio/realm/internal/Row;

    move-result-object v1

    iget-object v2, p0, Lio/realm/LineOfSightResultRealmProxy;->columnInfo:Lio/realm/LineOfSightResultRealmProxy$LineOfSightResultColumnInfo;

    iget-wide v2, v2, Lio/realm/LineOfSightResultRealmProxy$LineOfSightResultColumnInfo;->mistakeCountIndex:J

    int-to-long v4, p1

    invoke-interface {v1, v2, v3, v4, v5}, Lio/realm/internal/Row;->setLong(JJ)V

    goto :goto_0
.end method

.method public realmSet$unixTime(J)V
    .locals 9
    .param p1, "value"    # J

    .prologue
    .line 171
    iget-object v1, p0, Lio/realm/LineOfSightResultRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v1}, Lio/realm/ProxyState;->isUnderConstruction()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 172
    iget-object v1, p0, Lio/realm/LineOfSightResultRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v1}, Lio/realm/ProxyState;->getAcceptDefaultValue$realm()Z

    move-result v1

    if-nez v1, :cond_0

    .line 182
    :goto_0
    return-void

    .line 175
    :cond_0
    iget-object v1, p0, Lio/realm/LineOfSightResultRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v1}, Lio/realm/ProxyState;->getRow$realm()Lio/realm/internal/Row;

    move-result-object v0

    .line 176
    .local v0, "row":Lio/realm/internal/Row;
    invoke-interface {v0}, Lio/realm/internal/Row;->getTable()Lio/realm/internal/Table;

    move-result-object v1

    iget-object v2, p0, Lio/realm/LineOfSightResultRealmProxy;->columnInfo:Lio/realm/LineOfSightResultRealmProxy$LineOfSightResultColumnInfo;

    iget-wide v2, v2, Lio/realm/LineOfSightResultRealmProxy$LineOfSightResultColumnInfo;->unixTimeIndex:J

    invoke-interface {v0}, Lio/realm/internal/Row;->getIndex()J

    move-result-wide v4

    const/4 v8, 0x1

    move-wide v6, p1

    invoke-virtual/range {v1 .. v8}, Lio/realm/internal/Table;->setLong(JJJZ)V

    goto :goto_0

    .line 180
    .end local v0    # "row":Lio/realm/internal/Row;
    :cond_1
    iget-object v1, p0, Lio/realm/LineOfSightResultRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v1}, Lio/realm/ProxyState;->getRealm$realm()Lio/realm/BaseRealm;

    move-result-object v1

    invoke-virtual {v1}, Lio/realm/BaseRealm;->checkIfValid()V

    .line 181
    iget-object v1, p0, Lio/realm/LineOfSightResultRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v1}, Lio/realm/ProxyState;->getRow$realm()Lio/realm/internal/Row;

    move-result-object v1

    iget-object v2, p0, Lio/realm/LineOfSightResultRealmProxy;->columnInfo:Lio/realm/LineOfSightResultRealmProxy$LineOfSightResultColumnInfo;

    iget-wide v2, v2, Lio/realm/LineOfSightResultRealmProxy$LineOfSightResultColumnInfo;->unixTimeIndex:J

    invoke-interface {v1, v2, v3, p1, p2}, Lio/realm/internal/Row;->setLong(JJ)V

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 750
    invoke-static {p0}, Lio/realm/RealmObject;->isValid(Lio/realm/RealmModel;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 751
    const-string v1, "Invalid object"

    .line 774
    :goto_0
    return-object v1

    .line 753
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "LineOfSightResult = ["

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 754
    .local v0, "stringBuilder":Ljava/lang/StringBuilder;
    const-string v1, "{id:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 755
    invoke-virtual {p0}, Lio/realm/LineOfSightResultRealmProxy;->realmGet$id()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 756
    const-string v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 757
    const-string v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 758
    const-string v1, "{mistakeCount:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 759
    invoke-virtual {p0}, Lio/realm/LineOfSightResultRealmProxy;->realmGet$mistakeCount()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 760
    const-string v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 761
    const-string v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 762
    const-string v1, "{foundMistakeCount:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 763
    invoke-virtual {p0}, Lio/realm/LineOfSightResultRealmProxy;->realmGet$foundMistakeCount()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 764
    const-string v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 765
    const-string v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 766
    const-string v1, "{unixTime:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 767
    invoke-virtual {p0}, Lio/realm/LineOfSightResultRealmProxy;->realmGet$unixTime()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 768
    const-string v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 769
    const-string v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 770
    const-string v1, "{config:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 771
    invoke-virtual {p0}, Lio/realm/LineOfSightResultRealmProxy;->realmGet$config()Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightConfig;

    move-result-object v1

    if-eqz v1, :cond_1

    const-string v1, "LineOfSightConfig"

    :goto_1
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 772
    const-string v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 773
    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 774
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 771
    :cond_1
    const-string v1, "null"

    goto :goto_1
.end method
