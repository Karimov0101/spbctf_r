.class public Lio/realm/PassCourseResultRealmProxy;
.super Lcom/safonov/speedreading/training/fragment/passcource/repository/etity/PassCourseResult;
.source "PassCourseResultRealmProxy.java"

# interfaces
.implements Lio/realm/internal/RealmObjectProxy;
.implements Lio/realm/PassCourseResultRealmProxyInterface;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lio/realm/PassCourseResultRealmProxy$PassCourseResultColumnInfo;
    }
.end annotation


# static fields
.field private static final FIELD_NAMES:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private columnInfo:Lio/realm/PassCourseResultRealmProxy$PassCourseResultColumnInfo;

.field private proxyState:Lio/realm/ProxyState;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/realm/ProxyState",
            "<",
            "Lcom/safonov/speedreading/training/fragment/passcource/repository/etity/PassCourseResult;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 77
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 78
    .local v0, "fieldNames":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const-string v1, "id"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 79
    const-string v1, "type"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 80
    const-string v1, "score"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 81
    const-string v1, "unixTime"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 82
    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    sput-object v1, Lio/realm/PassCourseResultRealmProxy;->FIELD_NAMES:Ljava/util/List;

    .line 83
    return-void
.end method

.method constructor <init>()V
    .locals 1

    .prologue
    .line 85
    invoke-direct {p0}, Lcom/safonov/speedreading/training/fragment/passcource/repository/etity/PassCourseResult;-><init>()V

    .line 86
    iget-object v0, p0, Lio/realm/PassCourseResultRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v0}, Lio/realm/ProxyState;->setConstructionFinished()V

    .line 87
    return-void
.end method

.method public static copy(Lio/realm/Realm;Lcom/safonov/speedreading/training/fragment/passcource/repository/etity/PassCourseResult;ZLjava/util/Map;)Lcom/safonov/speedreading/training/fragment/passcource/repository/etity/PassCourseResult;
    .locals 6
    .param p0, "realm"    # Lio/realm/Realm;
    .param p1, "newObject"    # Lcom/safonov/speedreading/training/fragment/passcource/repository/etity/PassCourseResult;
    .param p2, "update"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/realm/Realm;",
            "Lcom/safonov/speedreading/training/fragment/passcource/repository/etity/PassCourseResult;",
            "Z",
            "Ljava/util/Map",
            "<",
            "Lio/realm/RealmModel;",
            "Lio/realm/internal/RealmObjectProxy;",
            ">;)",
            "Lcom/safonov/speedreading/training/fragment/passcource/repository/etity/PassCourseResult;"
        }
    .end annotation

    .prologue
    .line 434
    .local p3, "cache":Ljava/util/Map;, "Ljava/util/Map<Lio/realm/RealmModel;Lio/realm/internal/RealmObjectProxy;>;"
    invoke-interface {p3, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/realm/internal/RealmObjectProxy;

    .line 435
    .local v0, "cachedRealmObject":Lio/realm/internal/RealmObjectProxy;
    if-eqz v0, :cond_0

    .line 436
    check-cast v0, Lcom/safonov/speedreading/training/fragment/passcource/repository/etity/PassCourseResult;

    .line 444
    .end local v0    # "cachedRealmObject":Lio/realm/internal/RealmObjectProxy;
    .end local p1    # "newObject":Lcom/safonov/speedreading/training/fragment/passcource/repository/etity/PassCourseResult;
    :goto_0
    return-object v0

    .line 439
    .restart local v0    # "cachedRealmObject":Lio/realm/internal/RealmObjectProxy;
    .restart local p1    # "newObject":Lcom/safonov/speedreading/training/fragment/passcource/repository/etity/PassCourseResult;
    :cond_0
    const-class v3, Lcom/safonov/speedreading/training/fragment/passcource/repository/etity/PassCourseResult;

    move-object v2, p1

    check-cast v2, Lio/realm/PassCourseResultRealmProxyInterface;

    invoke-interface {v2}, Lio/realm/PassCourseResultRealmProxyInterface;->realmGet$id()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v4, 0x0

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v5

    invoke-virtual {p0, v3, v2, v4, v5}, Lio/realm/Realm;->createObjectInternal(Ljava/lang/Class;Ljava/lang/Object;ZLjava/util/List;)Lio/realm/RealmModel;

    move-result-object v1

    check-cast v1, Lcom/safonov/speedreading/training/fragment/passcource/repository/etity/PassCourseResult;

    .local v1, "realmObject":Lcom/safonov/speedreading/training/fragment/passcource/repository/etity/PassCourseResult;
    move-object v2, v1

    .line 440
    check-cast v2, Lio/realm/internal/RealmObjectProxy;

    invoke-interface {p3, p1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-object v2, v1

    .line 441
    check-cast v2, Lio/realm/PassCourseResultRealmProxyInterface;

    move-object v3, p1

    check-cast v3, Lio/realm/PassCourseResultRealmProxyInterface;

    invoke-interface {v3}, Lio/realm/PassCourseResultRealmProxyInterface;->realmGet$type()I

    move-result v3

    invoke-interface {v2, v3}, Lio/realm/PassCourseResultRealmProxyInterface;->realmSet$type(I)V

    move-object v2, v1

    .line 442
    check-cast v2, Lio/realm/PassCourseResultRealmProxyInterface;

    move-object v3, p1

    check-cast v3, Lio/realm/PassCourseResultRealmProxyInterface;

    invoke-interface {v3}, Lio/realm/PassCourseResultRealmProxyInterface;->realmGet$score()I

    move-result v3

    invoke-interface {v2, v3}, Lio/realm/PassCourseResultRealmProxyInterface;->realmSet$score(I)V

    move-object v2, v1

    .line 443
    check-cast v2, Lio/realm/PassCourseResultRealmProxyInterface;

    check-cast p1, Lio/realm/PassCourseResultRealmProxyInterface;

    .end local p1    # "newObject":Lcom/safonov/speedreading/training/fragment/passcource/repository/etity/PassCourseResult;
    invoke-interface {p1}, Lio/realm/PassCourseResultRealmProxyInterface;->realmGet$unixTime()J

    move-result-wide v4

    invoke-interface {v2, v4, v5}, Lio/realm/PassCourseResultRealmProxyInterface;->realmSet$unixTime(J)V

    move-object v0, v1

    .line 444
    goto :goto_0
.end method

.method public static copyOrUpdate(Lio/realm/Realm;Lcom/safonov/speedreading/training/fragment/passcource/repository/etity/PassCourseResult;ZLjava/util/Map;)Lcom/safonov/speedreading/training/fragment/passcource/repository/etity/PassCourseResult;
    .locals 19
    .param p0, "realm"    # Lio/realm/Realm;
    .param p1, "object"    # Lcom/safonov/speedreading/training/fragment/passcource/repository/etity/PassCourseResult;
    .param p2, "update"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/realm/Realm;",
            "Lcom/safonov/speedreading/training/fragment/passcource/repository/etity/PassCourseResult;",
            "Z",
            "Ljava/util/Map",
            "<",
            "Lio/realm/RealmModel;",
            "Lio/realm/internal/RealmObjectProxy;",
            ">;)",
            "Lcom/safonov/speedreading/training/fragment/passcource/repository/etity/PassCourseResult;"
        }
    .end annotation

    .prologue
    .line 395
    .local p3, "cache":Ljava/util/Map;, "Ljava/util/Map<Lio/realm/RealmModel;Lio/realm/internal/RealmObjectProxy;>;"
    move-object/from16 v0, p1

    instance-of v5, v0, Lio/realm/internal/RealmObjectProxy;

    if-eqz v5, :cond_0

    move-object/from16 v5, p1

    check-cast v5, Lio/realm/internal/RealmObjectProxy;

    invoke-interface {v5}, Lio/realm/internal/RealmObjectProxy;->realmGet$proxyState()Lio/realm/ProxyState;

    move-result-object v5

    invoke-virtual {v5}, Lio/realm/ProxyState;->getRealm$realm()Lio/realm/BaseRealm;

    move-result-object v5

    if-eqz v5, :cond_0

    move-object/from16 v5, p1

    check-cast v5, Lio/realm/internal/RealmObjectProxy;

    invoke-interface {v5}, Lio/realm/internal/RealmObjectProxy;->realmGet$proxyState()Lio/realm/ProxyState;

    move-result-object v5

    invoke-virtual {v5}, Lio/realm/ProxyState;->getRealm$realm()Lio/realm/BaseRealm;

    move-result-object v5

    iget-wide v6, v5, Lio/realm/BaseRealm;->threadId:J

    move-object/from16 v0, p0

    iget-wide v8, v0, Lio/realm/Realm;->threadId:J

    cmp-long v5, v6, v8

    if-eqz v5, :cond_0

    .line 396
    new-instance v5, Ljava/lang/IllegalArgumentException;

    const-string v6, "Objects which belong to Realm instances in other threads cannot be copied into this Realm instance."

    invoke-direct {v5, v6}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v5

    .line 398
    :cond_0
    move-object/from16 v0, p1

    instance-of v5, v0, Lio/realm/internal/RealmObjectProxy;

    if-eqz v5, :cond_1

    move-object/from16 v5, p1

    check-cast v5, Lio/realm/internal/RealmObjectProxy;

    invoke-interface {v5}, Lio/realm/internal/RealmObjectProxy;->realmGet$proxyState()Lio/realm/ProxyState;

    move-result-object v5

    invoke-virtual {v5}, Lio/realm/ProxyState;->getRealm$realm()Lio/realm/BaseRealm;

    move-result-object v5

    if-eqz v5, :cond_1

    move-object/from16 v5, p1

    check-cast v5, Lio/realm/internal/RealmObjectProxy;

    invoke-interface {v5}, Lio/realm/internal/RealmObjectProxy;->realmGet$proxyState()Lio/realm/ProxyState;

    move-result-object v5

    invoke-virtual {v5}, Lio/realm/ProxyState;->getRealm$realm()Lio/realm/BaseRealm;

    move-result-object v5

    invoke-virtual {v5}, Lio/realm/BaseRealm;->getPath()Ljava/lang/String;

    move-result-object v5

    invoke-virtual/range {p0 .. p0}, Lio/realm/Realm;->getPath()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 428
    .end local p1    # "object":Lcom/safonov/speedreading/training/fragment/passcource/repository/etity/PassCourseResult;
    :goto_0
    return-object p1

    .line 401
    .restart local p1    # "object":Lcom/safonov/speedreading/training/fragment/passcource/repository/etity/PassCourseResult;
    :cond_1
    sget-object v5, Lio/realm/BaseRealm;->objectContext:Lio/realm/BaseRealm$ThreadLocalRealmObjectContext;

    invoke-virtual {v5}, Lio/realm/BaseRealm$ThreadLocalRealmObjectContext;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lio/realm/BaseRealm$RealmObjectContext;

    .line 402
    .local v4, "objectContext":Lio/realm/BaseRealm$RealmObjectContext;
    move-object/from16 v0, p3

    move-object/from16 v1, p1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lio/realm/internal/RealmObjectProxy;

    .line 403
    .local v10, "cachedRealmObject":Lio/realm/internal/RealmObjectProxy;
    if-eqz v10, :cond_2

    .line 404
    check-cast v10, Lcom/safonov/speedreading/training/fragment/passcource/repository/etity/PassCourseResult;

    .end local v10    # "cachedRealmObject":Lio/realm/internal/RealmObjectProxy;
    move-object/from16 p1, v10

    goto :goto_0

    .line 406
    .restart local v10    # "cachedRealmObject":Lio/realm/internal/RealmObjectProxy;
    :cond_2
    const/4 v14, 0x0

    .line 407
    .local v14, "realmObject":Lcom/safonov/speedreading/training/fragment/passcource/repository/etity/PassCourseResult;
    move/from16 v11, p2

    .line 408
    .local v11, "canUpdate":Z
    if-eqz v11, :cond_3

    .line 409
    const-class v5, Lcom/safonov/speedreading/training/fragment/passcource/repository/etity/PassCourseResult;

    move-object/from16 v0, p0

    invoke-virtual {v0, v5}, Lio/realm/Realm;->getTable(Ljava/lang/Class;)Lio/realm/internal/Table;

    move-result-object v18

    .line 410
    .local v18, "table":Lio/realm/internal/Table;
    invoke-virtual/range {v18 .. v18}, Lio/realm/internal/Table;->getPrimaryKey()J

    move-result-wide v12

    .local v12, "pkColumnIndex":J
    move-object/from16 v5, p1

    .line 411
    check-cast v5, Lio/realm/PassCourseResultRealmProxyInterface;

    invoke-interface {v5}, Lio/realm/PassCourseResultRealmProxyInterface;->realmGet$id()I

    move-result v5

    int-to-long v6, v5

    move-object/from16 v0, v18

    invoke-virtual {v0, v12, v13, v6, v7}, Lio/realm/internal/Table;->findFirstLong(JJ)J

    move-result-wide v16

    .line 412
    .local v16, "rowIndex":J
    const-wide/16 v6, -0x1

    cmp-long v5, v16, v6

    if-eqz v5, :cond_4

    .line 414
    :try_start_0
    move-object/from16 v0, v18

    move-wide/from16 v1, v16

    invoke-virtual {v0, v1, v2}, Lio/realm/internal/Table;->getUncheckedRow(J)Lio/realm/internal/UncheckedRow;

    move-result-object v6

    move-object/from16 v0, p0

    iget-object v5, v0, Lio/realm/Realm;->schema:Lio/realm/RealmSchema;

    const-class v7, Lcom/safonov/speedreading/training/fragment/passcource/repository/etity/PassCourseResult;

    invoke-virtual {v5, v7}, Lio/realm/RealmSchema;->getColumnInfo(Ljava/lang/Class;)Lio/realm/internal/ColumnInfo;

    move-result-object v7

    const/4 v8, 0x0

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v9

    move-object/from16 v5, p0

    invoke-virtual/range {v4 .. v9}, Lio/realm/BaseRealm$RealmObjectContext;->set(Lio/realm/BaseRealm;Lio/realm/internal/Row;Lio/realm/internal/ColumnInfo;ZLjava/util/List;)V

    .line 415
    new-instance v15, Lio/realm/PassCourseResultRealmProxy;

    invoke-direct {v15}, Lio/realm/PassCourseResultRealmProxy;-><init>()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 416
    .end local v14    # "realmObject":Lcom/safonov/speedreading/training/fragment/passcource/repository/etity/PassCourseResult;
    .local v15, "realmObject":Lcom/safonov/speedreading/training/fragment/passcource/repository/etity/PassCourseResult;
    :try_start_1
    move-object v0, v15

    check-cast v0, Lio/realm/internal/RealmObjectProxy;

    move-object v5, v0

    move-object/from16 v0, p3

    move-object/from16 v1, p1

    invoke-interface {v0, v1, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 418
    invoke-virtual {v4}, Lio/realm/BaseRealm$RealmObjectContext;->clear()V

    move-object v14, v15

    .line 425
    .end local v12    # "pkColumnIndex":J
    .end local v15    # "realmObject":Lcom/safonov/speedreading/training/fragment/passcource/repository/etity/PassCourseResult;
    .end local v16    # "rowIndex":J
    .end local v18    # "table":Lio/realm/internal/Table;
    .restart local v14    # "realmObject":Lcom/safonov/speedreading/training/fragment/passcource/repository/etity/PassCourseResult;
    :cond_3
    :goto_1
    if-eqz v11, :cond_5

    .line 426
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p3

    invoke-static {v0, v14, v1, v2}, Lio/realm/PassCourseResultRealmProxy;->update(Lio/realm/Realm;Lcom/safonov/speedreading/training/fragment/passcource/repository/etity/PassCourseResult;Lcom/safonov/speedreading/training/fragment/passcource/repository/etity/PassCourseResult;Ljava/util/Map;)Lcom/safonov/speedreading/training/fragment/passcource/repository/etity/PassCourseResult;

    move-result-object p1

    goto :goto_0

    .line 418
    .restart local v12    # "pkColumnIndex":J
    .restart local v16    # "rowIndex":J
    .restart local v18    # "table":Lio/realm/internal/Table;
    :catchall_0
    move-exception v5

    :goto_2
    invoke-virtual {v4}, Lio/realm/BaseRealm$RealmObjectContext;->clear()V

    throw v5

    .line 421
    :cond_4
    const/4 v11, 0x0

    goto :goto_1

    .line 428
    .end local v12    # "pkColumnIndex":J
    .end local v16    # "rowIndex":J
    .end local v18    # "table":Lio/realm/internal/Table;
    :cond_5
    invoke-static/range {p0 .. p3}, Lio/realm/PassCourseResultRealmProxy;->copy(Lio/realm/Realm;Lcom/safonov/speedreading/training/fragment/passcource/repository/etity/PassCourseResult;ZLjava/util/Map;)Lcom/safonov/speedreading/training/fragment/passcource/repository/etity/PassCourseResult;

    move-result-object p1

    goto/16 :goto_0

    .line 418
    .end local v14    # "realmObject":Lcom/safonov/speedreading/training/fragment/passcource/repository/etity/PassCourseResult;
    .restart local v12    # "pkColumnIndex":J
    .restart local v15    # "realmObject":Lcom/safonov/speedreading/training/fragment/passcource/repository/etity/PassCourseResult;
    .restart local v16    # "rowIndex":J
    .restart local v18    # "table":Lio/realm/internal/Table;
    :catchall_1
    move-exception v5

    move-object v14, v15

    .end local v15    # "realmObject":Lcom/safonov/speedreading/training/fragment/passcource/repository/etity/PassCourseResult;
    .restart local v14    # "realmObject":Lcom/safonov/speedreading/training/fragment/passcource/repository/etity/PassCourseResult;
    goto :goto_2
.end method

.method public static createDetachedCopy(Lcom/safonov/speedreading/training/fragment/passcource/repository/etity/PassCourseResult;IILjava/util/Map;)Lcom/safonov/speedreading/training/fragment/passcource/repository/etity/PassCourseResult;
    .locals 6
    .param p0, "realmObject"    # Lcom/safonov/speedreading/training/fragment/passcource/repository/etity/PassCourseResult;
    .param p1, "currentDepth"    # I
    .param p2, "maxDepth"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/safonov/speedreading/training/fragment/passcource/repository/etity/PassCourseResult;",
            "II",
            "Ljava/util/Map",
            "<",
            "Lio/realm/RealmModel;",
            "Lio/realm/internal/RealmObjectProxy$CacheData",
            "<",
            "Lio/realm/RealmModel;",
            ">;>;)",
            "Lcom/safonov/speedreading/training/fragment/passcource/repository/etity/PassCourseResult;"
        }
    .end annotation

    .prologue
    .line 557
    .local p3, "cache":Ljava/util/Map;, "Ljava/util/Map<Lio/realm/RealmModel;Lio/realm/internal/RealmObjectProxy$CacheData<Lio/realm/RealmModel;>;>;"
    if-gt p1, p2, :cond_0

    if-nez p0, :cond_1

    .line 558
    :cond_0
    const/4 v2, 0x0

    .line 578
    .end local p0    # "realmObject":Lcom/safonov/speedreading/training/fragment/passcource/repository/etity/PassCourseResult;
    :goto_0
    return-object v2

    .line 560
    .restart local p0    # "realmObject":Lcom/safonov/speedreading/training/fragment/passcource/repository/etity/PassCourseResult;
    :cond_1
    invoke-interface {p3, p0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/realm/internal/RealmObjectProxy$CacheData;

    .line 562
    .local v0, "cachedObject":Lio/realm/internal/RealmObjectProxy$CacheData;, "Lio/realm/internal/RealmObjectProxy$CacheData<Lio/realm/RealmModel;>;"
    if-eqz v0, :cond_3

    .line 564
    iget v2, v0, Lio/realm/internal/RealmObjectProxy$CacheData;->minDepth:I

    if-lt p1, v2, :cond_2

    .line 565
    iget-object v2, v0, Lio/realm/internal/RealmObjectProxy$CacheData;->object:Lio/realm/RealmModel;

    check-cast v2, Lcom/safonov/speedreading/training/fragment/passcource/repository/etity/PassCourseResult;

    goto :goto_0

    .line 567
    :cond_2
    iget-object v1, v0, Lio/realm/internal/RealmObjectProxy$CacheData;->object:Lio/realm/RealmModel;

    check-cast v1, Lcom/safonov/speedreading/training/fragment/passcource/repository/etity/PassCourseResult;

    .line 568
    .local v1, "unmanagedObject":Lcom/safonov/speedreading/training/fragment/passcource/repository/etity/PassCourseResult;
    iput p1, v0, Lio/realm/internal/RealmObjectProxy$CacheData;->minDepth:I

    :goto_1
    move-object v2, v1

    .line 574
    check-cast v2, Lio/realm/PassCourseResultRealmProxyInterface;

    move-object v3, p0

    check-cast v3, Lio/realm/PassCourseResultRealmProxyInterface;

    invoke-interface {v3}, Lio/realm/PassCourseResultRealmProxyInterface;->realmGet$id()I

    move-result v3

    invoke-interface {v2, v3}, Lio/realm/PassCourseResultRealmProxyInterface;->realmSet$id(I)V

    move-object v2, v1

    .line 575
    check-cast v2, Lio/realm/PassCourseResultRealmProxyInterface;

    move-object v3, p0

    check-cast v3, Lio/realm/PassCourseResultRealmProxyInterface;

    invoke-interface {v3}, Lio/realm/PassCourseResultRealmProxyInterface;->realmGet$type()I

    move-result v3

    invoke-interface {v2, v3}, Lio/realm/PassCourseResultRealmProxyInterface;->realmSet$type(I)V

    move-object v2, v1

    .line 576
    check-cast v2, Lio/realm/PassCourseResultRealmProxyInterface;

    move-object v3, p0

    check-cast v3, Lio/realm/PassCourseResultRealmProxyInterface;

    invoke-interface {v3}, Lio/realm/PassCourseResultRealmProxyInterface;->realmGet$score()I

    move-result v3

    invoke-interface {v2, v3}, Lio/realm/PassCourseResultRealmProxyInterface;->realmSet$score(I)V

    move-object v2, v1

    .line 577
    check-cast v2, Lio/realm/PassCourseResultRealmProxyInterface;

    check-cast p0, Lio/realm/PassCourseResultRealmProxyInterface;

    .end local p0    # "realmObject":Lcom/safonov/speedreading/training/fragment/passcource/repository/etity/PassCourseResult;
    invoke-interface {p0}, Lio/realm/PassCourseResultRealmProxyInterface;->realmGet$unixTime()J

    move-result-wide v4

    invoke-interface {v2, v4, v5}, Lio/realm/PassCourseResultRealmProxyInterface;->realmSet$unixTime(J)V

    move-object v2, v1

    .line 578
    goto :goto_0

    .line 571
    .end local v1    # "unmanagedObject":Lcom/safonov/speedreading/training/fragment/passcource/repository/etity/PassCourseResult;
    .restart local p0    # "realmObject":Lcom/safonov/speedreading/training/fragment/passcource/repository/etity/PassCourseResult;
    :cond_3
    new-instance v1, Lcom/safonov/speedreading/training/fragment/passcource/repository/etity/PassCourseResult;

    invoke-direct {v1}, Lcom/safonov/speedreading/training/fragment/passcource/repository/etity/PassCourseResult;-><init>()V

    .line 572
    .restart local v1    # "unmanagedObject":Lcom/safonov/speedreading/training/fragment/passcource/repository/etity/PassCourseResult;
    new-instance v2, Lio/realm/internal/RealmObjectProxy$CacheData;

    invoke-direct {v2, p1, v1}, Lio/realm/internal/RealmObjectProxy$CacheData;-><init>(ILio/realm/RealmModel;)V

    invoke-interface {p3, p0, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1
.end method

.method public static createOrUpdateUsingJsonObject(Lio/realm/Realm;Lorg/json/JSONObject;Z)Lcom/safonov/speedreading/training/fragment/passcource/repository/etity/PassCourseResult;
    .locals 13
    .param p0, "realm"    # Lio/realm/Realm;
    .param p1, "json"    # Lorg/json/JSONObject;
    .param p2, "update"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .prologue
    .line 290
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v6

    .line 291
    .local v6, "excludeFields":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const/4 v7, 0x0

    .line 292
    .local v7, "obj":Lcom/safonov/speedreading/training/fragment/passcource/repository/etity/PassCourseResult;
    if-eqz p2, :cond_1

    .line 293
    const-class v1, Lcom/safonov/speedreading/training/fragment/passcource/repository/etity/PassCourseResult;

    invoke-virtual {p0, v1}, Lio/realm/Realm;->getTable(Ljava/lang/Class;)Lio/realm/internal/Table;

    move-result-object v12

    .line 294
    .local v12, "table":Lio/realm/internal/Table;
    invoke-virtual {v12}, Lio/realm/internal/Table;->getPrimaryKey()J

    move-result-wide v8

    .line 295
    .local v8, "pkColumnIndex":J
    const-wide/16 v10, -0x1

    .line 296
    .local v10, "rowIndex":J
    const-string v1, "id"

    invoke-virtual {p1, v1}, Lorg/json/JSONObject;->isNull(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 297
    const-string v1, "id"

    invoke-virtual {p1, v1}, Lorg/json/JSONObject;->getLong(Ljava/lang/String;)J

    move-result-wide v2

    invoke-virtual {v12, v8, v9, v2, v3}, Lio/realm/internal/Table;->findFirstLong(JJ)J

    move-result-wide v10

    .line 299
    :cond_0
    const-wide/16 v2, -0x1

    cmp-long v1, v10, v2

    if-eqz v1, :cond_1

    .line 300
    sget-object v1, Lio/realm/BaseRealm;->objectContext:Lio/realm/BaseRealm$ThreadLocalRealmObjectContext;

    invoke-virtual {v1}, Lio/realm/BaseRealm$ThreadLocalRealmObjectContext;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/realm/BaseRealm$RealmObjectContext;

    .line 302
    .local v0, "objectContext":Lio/realm/BaseRealm$RealmObjectContext;
    :try_start_0
    invoke-virtual {v12, v10, v11}, Lio/realm/internal/Table;->getUncheckedRow(J)Lio/realm/internal/UncheckedRow;

    move-result-object v2

    iget-object v1, p0, Lio/realm/Realm;->schema:Lio/realm/RealmSchema;

    const-class v3, Lcom/safonov/speedreading/training/fragment/passcource/repository/etity/PassCourseResult;

    invoke-virtual {v1, v3}, Lio/realm/RealmSchema;->getColumnInfo(Ljava/lang/Class;)Lio/realm/internal/ColumnInfo;

    move-result-object v3

    const/4 v4, 0x0

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v5

    move-object v1, p0

    invoke-virtual/range {v0 .. v5}, Lio/realm/BaseRealm$RealmObjectContext;->set(Lio/realm/BaseRealm;Lio/realm/internal/Row;Lio/realm/internal/ColumnInfo;ZLjava/util/List;)V

    .line 303
    new-instance v7, Lio/realm/PassCourseResultRealmProxy;

    .end local v7    # "obj":Lcom/safonov/speedreading/training/fragment/passcource/repository/etity/PassCourseResult;
    invoke-direct {v7}, Lio/realm/PassCourseResultRealmProxy;-><init>()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 305
    .restart local v7    # "obj":Lcom/safonov/speedreading/training/fragment/passcource/repository/etity/PassCourseResult;
    invoke-virtual {v0}, Lio/realm/BaseRealm$RealmObjectContext;->clear()V

    .line 309
    .end local v0    # "objectContext":Lio/realm/BaseRealm$RealmObjectContext;
    .end local v8    # "pkColumnIndex":J
    .end local v10    # "rowIndex":J
    .end local v12    # "table":Lio/realm/internal/Table;
    :cond_1
    if-nez v7, :cond_2

    .line 310
    const-string v1, "id"

    invoke-virtual {p1, v1}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 311
    const-string v1, "id"

    invoke-virtual {p1, v1}, Lorg/json/JSONObject;->isNull(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 312
    const-class v1, Lcom/safonov/speedreading/training/fragment/passcource/repository/etity/PassCourseResult;

    const/4 v2, 0x0

    const/4 v3, 0x1

    invoke-virtual {p0, v1, v2, v3, v6}, Lio/realm/Realm;->createObjectInternal(Ljava/lang/Class;Ljava/lang/Object;ZLjava/util/List;)Lio/realm/RealmModel;

    move-result-object v7

    .end local v7    # "obj":Lcom/safonov/speedreading/training/fragment/passcource/repository/etity/PassCourseResult;
    check-cast v7, Lio/realm/PassCourseResultRealmProxy;

    .line 320
    .restart local v7    # "obj":Lcom/safonov/speedreading/training/fragment/passcource/repository/etity/PassCourseResult;
    :cond_2
    :goto_0
    const-string v1, "type"

    invoke-virtual {p1, v1}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 321
    const-string v1, "type"

    invoke-virtual {p1, v1}, Lorg/json/JSONObject;->isNull(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 322
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "Trying to set non-nullable field \'type\' to null."

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 305
    .end local v7    # "obj":Lcom/safonov/speedreading/training/fragment/passcource/repository/etity/PassCourseResult;
    .restart local v0    # "objectContext":Lio/realm/BaseRealm$RealmObjectContext;
    .restart local v8    # "pkColumnIndex":J
    .restart local v10    # "rowIndex":J
    .restart local v12    # "table":Lio/realm/internal/Table;
    :catchall_0
    move-exception v1

    invoke-virtual {v0}, Lio/realm/BaseRealm$RealmObjectContext;->clear()V

    throw v1

    .line 314
    .end local v0    # "objectContext":Lio/realm/BaseRealm$RealmObjectContext;
    .end local v8    # "pkColumnIndex":J
    .end local v10    # "rowIndex":J
    .end local v12    # "table":Lio/realm/internal/Table;
    .restart local v7    # "obj":Lcom/safonov/speedreading/training/fragment/passcource/repository/etity/PassCourseResult;
    :cond_3
    const-class v1, Lcom/safonov/speedreading/training/fragment/passcource/repository/etity/PassCourseResult;

    const-string v2, "id"

    invoke-virtual {p1, v2}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v3, 0x1

    invoke-virtual {p0, v1, v2, v3, v6}, Lio/realm/Realm;->createObjectInternal(Ljava/lang/Class;Ljava/lang/Object;ZLjava/util/List;)Lio/realm/RealmModel;

    move-result-object v7

    .end local v7    # "obj":Lcom/safonov/speedreading/training/fragment/passcource/repository/etity/PassCourseResult;
    check-cast v7, Lio/realm/PassCourseResultRealmProxy;

    .restart local v7    # "obj":Lcom/safonov/speedreading/training/fragment/passcource/repository/etity/PassCourseResult;
    goto :goto_0

    .line 317
    :cond_4
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "JSON object doesn\'t have the primary key field \'id\'."

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_5
    move-object v1, v7

    .line 324
    check-cast v1, Lio/realm/PassCourseResultRealmProxyInterface;

    const-string v2, "type"

    invoke-virtual {p1, v2}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v1, v2}, Lio/realm/PassCourseResultRealmProxyInterface;->realmSet$type(I)V

    .line 327
    :cond_6
    const-string v1, "score"

    invoke-virtual {p1, v1}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_8

    .line 328
    const-string v1, "score"

    invoke-virtual {p1, v1}, Lorg/json/JSONObject;->isNull(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_7

    .line 329
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "Trying to set non-nullable field \'score\' to null."

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_7
    move-object v1, v7

    .line 331
    check-cast v1, Lio/realm/PassCourseResultRealmProxyInterface;

    const-string v2, "score"

    invoke-virtual {p1, v2}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v1, v2}, Lio/realm/PassCourseResultRealmProxyInterface;->realmSet$score(I)V

    .line 334
    :cond_8
    const-string v1, "unixTime"

    invoke-virtual {p1, v1}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_a

    .line 335
    const-string v1, "unixTime"

    invoke-virtual {p1, v1}, Lorg/json/JSONObject;->isNull(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_9

    .line 336
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "Trying to set non-nullable field \'unixTime\' to null."

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_9
    move-object v1, v7

    .line 338
    check-cast v1, Lio/realm/PassCourseResultRealmProxyInterface;

    const-string v2, "unixTime"

    invoke-virtual {p1, v2}, Lorg/json/JSONObject;->getLong(Ljava/lang/String;)J

    move-result-wide v2

    invoke-interface {v1, v2, v3}, Lio/realm/PassCourseResultRealmProxyInterface;->realmSet$unixTime(J)V

    .line 341
    :cond_a
    return-object v7
.end method

.method public static createRealmObjectSchema(Lio/realm/RealmSchema;)Lio/realm/RealmObjectSchema;
    .locals 11
    .param p0, "realmSchema"    # Lio/realm/RealmSchema;

    .prologue
    const/4 v3, 0x1

    const/4 v7, 0x0

    .line 180
    const-string v0, "PassCourseResult"

    invoke-virtual {p0, v0}, Lio/realm/RealmSchema;->contains(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 181
    const-string v0, "PassCourseResult"

    invoke-virtual {p0, v0}, Lio/realm/RealmSchema;->create(Ljava/lang/String;)Lio/realm/RealmObjectSchema;

    move-result-object v10

    .line 182
    .local v10, "realmObjectSchema":Lio/realm/RealmObjectSchema;
    new-instance v0, Lio/realm/Property;

    const-string v1, "id"

    sget-object v2, Lio/realm/RealmFieldType;->INTEGER:Lio/realm/RealmFieldType;

    move v4, v3

    move v5, v3

    invoke-direct/range {v0 .. v5}, Lio/realm/Property;-><init>(Ljava/lang/String;Lio/realm/RealmFieldType;ZZZ)V

    invoke-virtual {v10, v0}, Lio/realm/RealmObjectSchema;->add(Lio/realm/Property;)Lio/realm/RealmObjectSchema;

    .line 183
    new-instance v4, Lio/realm/Property;

    const-string v5, "type"

    sget-object v6, Lio/realm/RealmFieldType;->INTEGER:Lio/realm/RealmFieldType;

    move v8, v7

    move v9, v3

    invoke-direct/range {v4 .. v9}, Lio/realm/Property;-><init>(Ljava/lang/String;Lio/realm/RealmFieldType;ZZZ)V

    invoke-virtual {v10, v4}, Lio/realm/RealmObjectSchema;->add(Lio/realm/Property;)Lio/realm/RealmObjectSchema;

    .line 184
    new-instance v4, Lio/realm/Property;

    const-string v5, "score"

    sget-object v6, Lio/realm/RealmFieldType;->INTEGER:Lio/realm/RealmFieldType;

    move v8, v7

    move v9, v3

    invoke-direct/range {v4 .. v9}, Lio/realm/Property;-><init>(Ljava/lang/String;Lio/realm/RealmFieldType;ZZZ)V

    invoke-virtual {v10, v4}, Lio/realm/RealmObjectSchema;->add(Lio/realm/Property;)Lio/realm/RealmObjectSchema;

    .line 185
    new-instance v4, Lio/realm/Property;

    const-string v5, "unixTime"

    sget-object v6, Lio/realm/RealmFieldType;->INTEGER:Lio/realm/RealmFieldType;

    move v8, v7

    move v9, v3

    invoke-direct/range {v4 .. v9}, Lio/realm/Property;-><init>(Ljava/lang/String;Lio/realm/RealmFieldType;ZZZ)V

    invoke-virtual {v10, v4}, Lio/realm/RealmObjectSchema;->add(Lio/realm/Property;)Lio/realm/RealmObjectSchema;

    .line 188
    .end local v10    # "realmObjectSchema":Lio/realm/RealmObjectSchema;
    :goto_0
    return-object v10

    :cond_0
    const-string v0, "PassCourseResult"

    invoke-virtual {p0, v0}, Lio/realm/RealmSchema;->get(Ljava/lang/String;)Lio/realm/RealmObjectSchema;

    move-result-object v10

    goto :goto_0
.end method

.method public static createUsingJsonStream(Lio/realm/Realm;Landroid/util/JsonReader;)Lcom/safonov/speedreading/training/fragment/passcource/repository/etity/PassCourseResult;
    .locals 6
    .param p0, "realm"    # Lio/realm/Realm;
    .param p1, "reader"    # Landroid/util/JsonReader;
    .annotation build Landroid/annotation/TargetApi;
        value = 0xb
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 348
    const/4 v0, 0x0

    .line 349
    .local v0, "jsonHasPrimaryKey":Z
    new-instance v2, Lcom/safonov/speedreading/training/fragment/passcource/repository/etity/PassCourseResult;

    invoke-direct {v2}, Lcom/safonov/speedreading/training/fragment/passcource/repository/etity/PassCourseResult;-><init>()V

    .line 350
    .local v2, "obj":Lcom/safonov/speedreading/training/fragment/passcource/repository/etity/PassCourseResult;
    invoke-virtual {p1}, Landroid/util/JsonReader;->beginObject()V

    .line 351
    :goto_0
    invoke-virtual {p1}, Landroid/util/JsonReader;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_8

    .line 352
    invoke-virtual {p1}, Landroid/util/JsonReader;->nextName()Ljava/lang/String;

    move-result-object v1

    .line 353
    .local v1, "name":Ljava/lang/String;
    const-string v3, "id"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 354
    invoke-virtual {p1}, Landroid/util/JsonReader;->peek()Landroid/util/JsonToken;

    move-result-object v3

    sget-object v4, Landroid/util/JsonToken;->NULL:Landroid/util/JsonToken;

    if-ne v3, v4, :cond_0

    .line 355
    invoke-virtual {p1}, Landroid/util/JsonReader;->skipValue()V

    .line 356
    new-instance v3, Ljava/lang/IllegalArgumentException;

    const-string v4, "Trying to set non-nullable field \'id\' to null."

    invoke-direct {v3, v4}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v3

    :cond_0
    move-object v3, v2

    .line 358
    check-cast v3, Lio/realm/PassCourseResultRealmProxyInterface;

    invoke-virtual {p1}, Landroid/util/JsonReader;->nextInt()I

    move-result v4

    invoke-interface {v3, v4}, Lio/realm/PassCourseResultRealmProxyInterface;->realmSet$id(I)V

    .line 360
    const/4 v0, 0x1

    goto :goto_0

    .line 361
    :cond_1
    const-string v3, "type"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 362
    invoke-virtual {p1}, Landroid/util/JsonReader;->peek()Landroid/util/JsonToken;

    move-result-object v3

    sget-object v4, Landroid/util/JsonToken;->NULL:Landroid/util/JsonToken;

    if-ne v3, v4, :cond_2

    .line 363
    invoke-virtual {p1}, Landroid/util/JsonReader;->skipValue()V

    .line 364
    new-instance v3, Ljava/lang/IllegalArgumentException;

    const-string v4, "Trying to set non-nullable field \'type\' to null."

    invoke-direct {v3, v4}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v3

    :cond_2
    move-object v3, v2

    .line 366
    check-cast v3, Lio/realm/PassCourseResultRealmProxyInterface;

    invoke-virtual {p1}, Landroid/util/JsonReader;->nextInt()I

    move-result v4

    invoke-interface {v3, v4}, Lio/realm/PassCourseResultRealmProxyInterface;->realmSet$type(I)V

    goto :goto_0

    .line 368
    :cond_3
    const-string v3, "score"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_5

    .line 369
    invoke-virtual {p1}, Landroid/util/JsonReader;->peek()Landroid/util/JsonToken;

    move-result-object v3

    sget-object v4, Landroid/util/JsonToken;->NULL:Landroid/util/JsonToken;

    if-ne v3, v4, :cond_4

    .line 370
    invoke-virtual {p1}, Landroid/util/JsonReader;->skipValue()V

    .line 371
    new-instance v3, Ljava/lang/IllegalArgumentException;

    const-string v4, "Trying to set non-nullable field \'score\' to null."

    invoke-direct {v3, v4}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v3

    :cond_4
    move-object v3, v2

    .line 373
    check-cast v3, Lio/realm/PassCourseResultRealmProxyInterface;

    invoke-virtual {p1}, Landroid/util/JsonReader;->nextInt()I

    move-result v4

    invoke-interface {v3, v4}, Lio/realm/PassCourseResultRealmProxyInterface;->realmSet$score(I)V

    goto :goto_0

    .line 375
    :cond_5
    const-string v3, "unixTime"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_7

    .line 376
    invoke-virtual {p1}, Landroid/util/JsonReader;->peek()Landroid/util/JsonToken;

    move-result-object v3

    sget-object v4, Landroid/util/JsonToken;->NULL:Landroid/util/JsonToken;

    if-ne v3, v4, :cond_6

    .line 377
    invoke-virtual {p1}, Landroid/util/JsonReader;->skipValue()V

    .line 378
    new-instance v3, Ljava/lang/IllegalArgumentException;

    const-string v4, "Trying to set non-nullable field \'unixTime\' to null."

    invoke-direct {v3, v4}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v3

    :cond_6
    move-object v3, v2

    .line 380
    check-cast v3, Lio/realm/PassCourseResultRealmProxyInterface;

    invoke-virtual {p1}, Landroid/util/JsonReader;->nextLong()J

    move-result-wide v4

    invoke-interface {v3, v4, v5}, Lio/realm/PassCourseResultRealmProxyInterface;->realmSet$unixTime(J)V

    goto/16 :goto_0

    .line 383
    :cond_7
    invoke-virtual {p1}, Landroid/util/JsonReader;->skipValue()V

    goto/16 :goto_0

    .line 386
    .end local v1    # "name":Ljava/lang/String;
    :cond_8
    invoke-virtual {p1}, Landroid/util/JsonReader;->endObject()V

    .line 387
    if-nez v0, :cond_9

    .line 388
    new-instance v3, Ljava/lang/IllegalArgumentException;

    const-string v4, "JSON object doesn\'t have the primary key field \'id\'."

    invoke-direct {v3, v4}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 390
    :cond_9
    invoke-virtual {p0, v2}, Lio/realm/Realm;->copyToRealm(Lio/realm/RealmModel;)Lio/realm/RealmModel;

    move-result-object v2

    .end local v2    # "obj":Lcom/safonov/speedreading/training/fragment/passcource/repository/etity/PassCourseResult;
    check-cast v2, Lcom/safonov/speedreading/training/fragment/passcource/repository/etity/PassCourseResult;

    .line 391
    .restart local v2    # "obj":Lcom/safonov/speedreading/training/fragment/passcource/repository/etity/PassCourseResult;
    return-object v2
.end method

.method public static getFieldNames()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 284
    sget-object v0, Lio/realm/PassCourseResultRealmProxy;->FIELD_NAMES:Ljava/util/List;

    return-object v0
.end method

.method public static getTableName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 280
    const-string v0, "class_PassCourseResult"

    return-object v0
.end method

.method public static initTable(Lio/realm/internal/SharedRealm;)Lio/realm/internal/Table;
    .locals 4
    .param p0, "sharedRealm"    # Lio/realm/internal/SharedRealm;

    .prologue
    const/4 v3, 0x0

    .line 192
    const-string v1, "class_PassCourseResult"

    invoke-virtual {p0, v1}, Lio/realm/internal/SharedRealm;->hasTable(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 193
    const-string v1, "class_PassCourseResult"

    invoke-virtual {p0, v1}, Lio/realm/internal/SharedRealm;->getTable(Ljava/lang/String;)Lio/realm/internal/Table;

    move-result-object v0

    .line 194
    .local v0, "table":Lio/realm/internal/Table;
    sget-object v1, Lio/realm/RealmFieldType;->INTEGER:Lio/realm/RealmFieldType;

    const-string v2, "id"

    invoke-virtual {v0, v1, v2, v3}, Lio/realm/internal/Table;->addColumn(Lio/realm/RealmFieldType;Ljava/lang/String;Z)J

    .line 195
    sget-object v1, Lio/realm/RealmFieldType;->INTEGER:Lio/realm/RealmFieldType;

    const-string v2, "type"

    invoke-virtual {v0, v1, v2, v3}, Lio/realm/internal/Table;->addColumn(Lio/realm/RealmFieldType;Ljava/lang/String;Z)J

    .line 196
    sget-object v1, Lio/realm/RealmFieldType;->INTEGER:Lio/realm/RealmFieldType;

    const-string v2, "score"

    invoke-virtual {v0, v1, v2, v3}, Lio/realm/internal/Table;->addColumn(Lio/realm/RealmFieldType;Ljava/lang/String;Z)J

    .line 197
    sget-object v1, Lio/realm/RealmFieldType;->INTEGER:Lio/realm/RealmFieldType;

    const-string v2, "unixTime"

    invoke-virtual {v0, v1, v2, v3}, Lio/realm/internal/Table;->addColumn(Lio/realm/RealmFieldType;Ljava/lang/String;Z)J

    .line 198
    const-string v1, "id"

    invoke-virtual {v0, v1}, Lio/realm/internal/Table;->getColumnIndex(Ljava/lang/String;)J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lio/realm/internal/Table;->addSearchIndex(J)V

    .line 199
    const-string v1, "id"

    invoke-virtual {v0, v1}, Lio/realm/internal/Table;->setPrimaryKey(Ljava/lang/String;)V

    .line 202
    .end local v0    # "table":Lio/realm/internal/Table;
    :goto_0
    return-object v0

    :cond_0
    const-string v1, "class_PassCourseResult"

    invoke-virtual {p0, v1}, Lio/realm/internal/SharedRealm;->getTable(Ljava/lang/String;)Lio/realm/internal/Table;

    move-result-object v0

    goto :goto_0
.end method

.method public static insert(Lio/realm/Realm;Lcom/safonov/speedreading/training/fragment/passcource/repository/etity/PassCourseResult;Ljava/util/Map;)J
    .locals 18
    .param p0, "realm"    # Lio/realm/Realm;
    .param p1, "object"    # Lcom/safonov/speedreading/training/fragment/passcource/repository/etity/PassCourseResult;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/realm/Realm;",
            "Lcom/safonov/speedreading/training/fragment/passcource/repository/etity/PassCourseResult;",
            "Ljava/util/Map",
            "<",
            "Lio/realm/RealmModel;",
            "Ljava/lang/Long;",
            ">;)J"
        }
    .end annotation

    .prologue
    .line 449
    .local p2, "cache":Ljava/util/Map;, "Ljava/util/Map<Lio/realm/RealmModel;Ljava/lang/Long;>;"
    move-object/from16 v0, p1

    instance-of v6, v0, Lio/realm/internal/RealmObjectProxy;

    if-eqz v6, :cond_0

    move-object/from16 v6, p1

    check-cast v6, Lio/realm/internal/RealmObjectProxy;

    invoke-interface {v6}, Lio/realm/internal/RealmObjectProxy;->realmGet$proxyState()Lio/realm/ProxyState;

    move-result-object v6

    invoke-virtual {v6}, Lio/realm/ProxyState;->getRealm$realm()Lio/realm/BaseRealm;

    move-result-object v6

    if-eqz v6, :cond_0

    move-object/from16 v6, p1

    check-cast v6, Lio/realm/internal/RealmObjectProxy;

    invoke-interface {v6}, Lio/realm/internal/RealmObjectProxy;->realmGet$proxyState()Lio/realm/ProxyState;

    move-result-object v6

    invoke-virtual {v6}, Lio/realm/ProxyState;->getRealm$realm()Lio/realm/BaseRealm;

    move-result-object v6

    invoke-virtual {v6}, Lio/realm/BaseRealm;->getPath()Ljava/lang/String;

    move-result-object v6

    invoke-virtual/range {p0 .. p0}, Lio/realm/Realm;->getPath()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 450
    check-cast p1, Lio/realm/internal/RealmObjectProxy;

    .end local p1    # "object":Lcom/safonov/speedreading/training/fragment/passcource/repository/etity/PassCourseResult;
    invoke-interface/range {p1 .. p1}, Lio/realm/internal/RealmObjectProxy;->realmGet$proxyState()Lio/realm/ProxyState;

    move-result-object v6

    invoke-virtual {v6}, Lio/realm/ProxyState;->getRow$realm()Lio/realm/internal/Row;

    move-result-object v6

    invoke-interface {v6}, Lio/realm/internal/Row;->getIndex()J

    move-result-wide v10

    .line 470
    :goto_0
    return-wide v10

    .line 452
    .restart local p1    # "object":Lcom/safonov/speedreading/training/fragment/passcource/repository/etity/PassCourseResult;
    :cond_0
    const-class v6, Lcom/safonov/speedreading/training/fragment/passcource/repository/etity/PassCourseResult;

    move-object/from16 v0, p0

    invoke-virtual {v0, v6}, Lio/realm/Realm;->getTable(Ljava/lang/Class;)Lio/realm/internal/Table;

    move-result-object v17

    .line 453
    .local v17, "table":Lio/realm/internal/Table;
    invoke-virtual/range {v17 .. v17}, Lio/realm/internal/Table;->getNativeTablePointer()J

    move-result-wide v2

    .line 454
    .local v2, "tableNativePtr":J
    move-object/from16 v0, p0

    iget-object v6, v0, Lio/realm/Realm;->schema:Lio/realm/RealmSchema;

    const-class v7, Lcom/safonov/speedreading/training/fragment/passcource/repository/etity/PassCourseResult;

    invoke-virtual {v6, v7}, Lio/realm/RealmSchema;->getColumnInfo(Ljava/lang/Class;)Lio/realm/internal/ColumnInfo;

    move-result-object v15

    check-cast v15, Lio/realm/PassCourseResultRealmProxy$PassCourseResultColumnInfo;

    .line 455
    .local v15, "columnInfo":Lio/realm/PassCourseResultRealmProxy$PassCourseResultColumnInfo;
    invoke-virtual/range {v17 .. v17}, Lio/realm/internal/Table;->getPrimaryKey()J

    move-result-wide v4

    .line 456
    .local v4, "pkColumnIndex":J
    const-wide/16 v10, -0x1

    .local v10, "rowIndex":J
    move-object/from16 v6, p1

    .line 457
    check-cast v6, Lio/realm/PassCourseResultRealmProxyInterface;

    invoke-interface {v6}, Lio/realm/PassCourseResultRealmProxyInterface;->realmGet$id()I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v16

    .line 458
    .local v16, "primaryKeyValue":Ljava/lang/Integer;
    if-eqz v16, :cond_1

    move-object/from16 v6, p1

    .line 459
    check-cast v6, Lio/realm/PassCourseResultRealmProxyInterface;

    invoke-interface {v6}, Lio/realm/PassCourseResultRealmProxyInterface;->realmGet$id()I

    move-result v6

    int-to-long v6, v6

    invoke-static/range {v2 .. v7}, Lio/realm/internal/Table;->nativeFindFirstInt(JJJ)J

    move-result-wide v10

    .line 461
    :cond_1
    const-wide/16 v6, -0x1

    cmp-long v6, v10, v6

    if-nez v6, :cond_2

    move-object/from16 v6, p1

    .line 462
    check-cast v6, Lio/realm/PassCourseResultRealmProxyInterface;

    invoke-interface {v6}, Lio/realm/PassCourseResultRealmProxyInterface;->realmGet$id()I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    const/4 v7, 0x0

    move-object/from16 v0, v17

    invoke-virtual {v0, v6, v7}, Lio/realm/internal/Table;->addEmptyRowWithPrimaryKey(Ljava/lang/Object;Z)J

    move-result-wide v10

    .line 466
    :goto_1
    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    move-object/from16 v0, p2

    move-object/from16 v1, p1

    invoke-interface {v0, v1, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 467
    iget-wide v8, v15, Lio/realm/PassCourseResultRealmProxy$PassCourseResultColumnInfo;->typeIndex:J

    move-object/from16 v6, p1

    check-cast v6, Lio/realm/PassCourseResultRealmProxyInterface;

    invoke-interface {v6}, Lio/realm/PassCourseResultRealmProxyInterface;->realmGet$type()I

    move-result v6

    int-to-long v12, v6

    const/4 v14, 0x0

    move-wide v6, v2

    invoke-static/range {v6 .. v14}, Lio/realm/internal/Table;->nativeSetLong(JJJJZ)V

    .line 468
    iget-wide v8, v15, Lio/realm/PassCourseResultRealmProxy$PassCourseResultColumnInfo;->scoreIndex:J

    move-object/from16 v6, p1

    check-cast v6, Lio/realm/PassCourseResultRealmProxyInterface;

    invoke-interface {v6}, Lio/realm/PassCourseResultRealmProxyInterface;->realmGet$score()I

    move-result v6

    int-to-long v12, v6

    const/4 v14, 0x0

    move-wide v6, v2

    invoke-static/range {v6 .. v14}, Lio/realm/internal/Table;->nativeSetLong(JJJJZ)V

    .line 469
    iget-wide v8, v15, Lio/realm/PassCourseResultRealmProxy$PassCourseResultColumnInfo;->unixTimeIndex:J

    check-cast p1, Lio/realm/PassCourseResultRealmProxyInterface;

    .end local p1    # "object":Lcom/safonov/speedreading/training/fragment/passcource/repository/etity/PassCourseResult;
    invoke-interface/range {p1 .. p1}, Lio/realm/PassCourseResultRealmProxyInterface;->realmGet$unixTime()J

    move-result-wide v12

    const/4 v14, 0x0

    move-wide v6, v2

    invoke-static/range {v6 .. v14}, Lio/realm/internal/Table;->nativeSetLong(JJJJZ)V

    goto/16 :goto_0

    .line 464
    .restart local p1    # "object":Lcom/safonov/speedreading/training/fragment/passcource/repository/etity/PassCourseResult;
    :cond_2
    invoke-static/range {v16 .. v16}, Lio/realm/internal/Table;->throwDuplicatePrimaryKeyException(Ljava/lang/Object;)V

    goto :goto_1
.end method

.method public static insert(Lio/realm/Realm;Ljava/util/Iterator;Ljava/util/Map;)V
    .locals 19
    .param p0, "realm"    # Lio/realm/Realm;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/realm/Realm;",
            "Ljava/util/Iterator",
            "<+",
            "Lio/realm/RealmModel;",
            ">;",
            "Ljava/util/Map",
            "<",
            "Lio/realm/RealmModel;",
            "Ljava/lang/Long;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 474
    .local p1, "objects":Ljava/util/Iterator;, "Ljava/util/Iterator<+Lio/realm/RealmModel;>;"
    .local p2, "cache":Ljava/util/Map;, "Ljava/util/Map<Lio/realm/RealmModel;Ljava/lang/Long;>;"
    const-class v6, Lcom/safonov/speedreading/training/fragment/passcource/repository/etity/PassCourseResult;

    move-object/from16 v0, p0

    invoke-virtual {v0, v6}, Lio/realm/Realm;->getTable(Ljava/lang/Class;)Lio/realm/internal/Table;

    move-result-object v18

    .line 475
    .local v18, "table":Lio/realm/internal/Table;
    invoke-virtual/range {v18 .. v18}, Lio/realm/internal/Table;->getNativeTablePointer()J

    move-result-wide v2

    .line 476
    .local v2, "tableNativePtr":J
    move-object/from16 v0, p0

    iget-object v6, v0, Lio/realm/Realm;->schema:Lio/realm/RealmSchema;

    const-class v7, Lcom/safonov/speedreading/training/fragment/passcource/repository/etity/PassCourseResult;

    invoke-virtual {v6, v7}, Lio/realm/RealmSchema;->getColumnInfo(Ljava/lang/Class;)Lio/realm/internal/ColumnInfo;

    move-result-object v15

    check-cast v15, Lio/realm/PassCourseResultRealmProxy$PassCourseResultColumnInfo;

    .line 477
    .local v15, "columnInfo":Lio/realm/PassCourseResultRealmProxy$PassCourseResultColumnInfo;
    invoke-virtual/range {v18 .. v18}, Lio/realm/internal/Table;->getPrimaryKey()J

    move-result-wide v4

    .line 478
    .local v4, "pkColumnIndex":J
    const/16 v16, 0x0

    .line 479
    .local v16, "object":Lcom/safonov/speedreading/training/fragment/passcource/repository/etity/PassCourseResult;
    :cond_0
    :goto_0
    invoke-interface/range {p1 .. p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_4

    .line 480
    invoke-interface/range {p1 .. p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v16

    .end local v16    # "object":Lcom/safonov/speedreading/training/fragment/passcource/repository/etity/PassCourseResult;
    check-cast v16, Lcom/safonov/speedreading/training/fragment/passcource/repository/etity/PassCourseResult;

    .line 481
    .restart local v16    # "object":Lcom/safonov/speedreading/training/fragment/passcource/repository/etity/PassCourseResult;
    move-object/from16 v0, p2

    move-object/from16 v1, v16

    invoke-interface {v0, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_0

    .line 482
    move-object/from16 v0, v16

    instance-of v6, v0, Lio/realm/internal/RealmObjectProxy;

    if-eqz v6, :cond_1

    move-object/from16 v6, v16

    check-cast v6, Lio/realm/internal/RealmObjectProxy;

    invoke-interface {v6}, Lio/realm/internal/RealmObjectProxy;->realmGet$proxyState()Lio/realm/ProxyState;

    move-result-object v6

    invoke-virtual {v6}, Lio/realm/ProxyState;->getRealm$realm()Lio/realm/BaseRealm;

    move-result-object v6

    if-eqz v6, :cond_1

    move-object/from16 v6, v16

    check-cast v6, Lio/realm/internal/RealmObjectProxy;

    invoke-interface {v6}, Lio/realm/internal/RealmObjectProxy;->realmGet$proxyState()Lio/realm/ProxyState;

    move-result-object v6

    invoke-virtual {v6}, Lio/realm/ProxyState;->getRealm$realm()Lio/realm/BaseRealm;

    move-result-object v6

    invoke-virtual {v6}, Lio/realm/BaseRealm;->getPath()Ljava/lang/String;

    move-result-object v6

    invoke-virtual/range {p0 .. p0}, Lio/realm/Realm;->getPath()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1

    move-object/from16 v6, v16

    .line 483
    check-cast v6, Lio/realm/internal/RealmObjectProxy;

    invoke-interface {v6}, Lio/realm/internal/RealmObjectProxy;->realmGet$proxyState()Lio/realm/ProxyState;

    move-result-object v6

    invoke-virtual {v6}, Lio/realm/ProxyState;->getRow$realm()Lio/realm/internal/Row;

    move-result-object v6

    invoke-interface {v6}, Lio/realm/internal/Row;->getIndex()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    move-object/from16 v0, p2

    move-object/from16 v1, v16

    invoke-interface {v0, v1, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 486
    :cond_1
    const-wide/16 v10, -0x1

    .local v10, "rowIndex":J
    move-object/from16 v6, v16

    .line 487
    check-cast v6, Lio/realm/PassCourseResultRealmProxyInterface;

    invoke-interface {v6}, Lio/realm/PassCourseResultRealmProxyInterface;->realmGet$id()I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v17

    .line 488
    .local v17, "primaryKeyValue":Ljava/lang/Integer;
    if-eqz v17, :cond_2

    move-object/from16 v6, v16

    .line 489
    check-cast v6, Lio/realm/PassCourseResultRealmProxyInterface;

    invoke-interface {v6}, Lio/realm/PassCourseResultRealmProxyInterface;->realmGet$id()I

    move-result v6

    int-to-long v6, v6

    invoke-static/range {v2 .. v7}, Lio/realm/internal/Table;->nativeFindFirstInt(JJJ)J

    move-result-wide v10

    .line 491
    :cond_2
    const-wide/16 v6, -0x1

    cmp-long v6, v10, v6

    if-nez v6, :cond_3

    move-object/from16 v6, v16

    .line 492
    check-cast v6, Lio/realm/PassCourseResultRealmProxyInterface;

    invoke-interface {v6}, Lio/realm/PassCourseResultRealmProxyInterface;->realmGet$id()I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    const/4 v7, 0x0

    move-object/from16 v0, v18

    invoke-virtual {v0, v6, v7}, Lio/realm/internal/Table;->addEmptyRowWithPrimaryKey(Ljava/lang/Object;Z)J

    move-result-wide v10

    .line 496
    :goto_1
    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    move-object/from16 v0, p2

    move-object/from16 v1, v16

    invoke-interface {v0, v1, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 497
    iget-wide v8, v15, Lio/realm/PassCourseResultRealmProxy$PassCourseResultColumnInfo;->typeIndex:J

    move-object/from16 v6, v16

    check-cast v6, Lio/realm/PassCourseResultRealmProxyInterface;

    invoke-interface {v6}, Lio/realm/PassCourseResultRealmProxyInterface;->realmGet$type()I

    move-result v6

    int-to-long v12, v6

    const/4 v14, 0x0

    move-wide v6, v2

    invoke-static/range {v6 .. v14}, Lio/realm/internal/Table;->nativeSetLong(JJJJZ)V

    .line 498
    iget-wide v8, v15, Lio/realm/PassCourseResultRealmProxy$PassCourseResultColumnInfo;->scoreIndex:J

    move-object/from16 v6, v16

    check-cast v6, Lio/realm/PassCourseResultRealmProxyInterface;

    invoke-interface {v6}, Lio/realm/PassCourseResultRealmProxyInterface;->realmGet$score()I

    move-result v6

    int-to-long v12, v6

    const/4 v14, 0x0

    move-wide v6, v2

    invoke-static/range {v6 .. v14}, Lio/realm/internal/Table;->nativeSetLong(JJJJZ)V

    .line 499
    iget-wide v8, v15, Lio/realm/PassCourseResultRealmProxy$PassCourseResultColumnInfo;->unixTimeIndex:J

    move-object/from16 v6, v16

    check-cast v6, Lio/realm/PassCourseResultRealmProxyInterface;

    invoke-interface {v6}, Lio/realm/PassCourseResultRealmProxyInterface;->realmGet$unixTime()J

    move-result-wide v12

    const/4 v14, 0x0

    move-wide v6, v2

    invoke-static/range {v6 .. v14}, Lio/realm/internal/Table;->nativeSetLong(JJJJZ)V

    goto/16 :goto_0

    .line 494
    :cond_3
    invoke-static/range {v17 .. v17}, Lio/realm/internal/Table;->throwDuplicatePrimaryKeyException(Ljava/lang/Object;)V

    goto :goto_1

    .line 502
    .end local v10    # "rowIndex":J
    .end local v17    # "primaryKeyValue":Ljava/lang/Integer;
    :cond_4
    return-void
.end method

.method public static insertOrUpdate(Lio/realm/Realm;Lcom/safonov/speedreading/training/fragment/passcource/repository/etity/PassCourseResult;Ljava/util/Map;)J
    .locals 18
    .param p0, "realm"    # Lio/realm/Realm;
    .param p1, "object"    # Lcom/safonov/speedreading/training/fragment/passcource/repository/etity/PassCourseResult;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/realm/Realm;",
            "Lcom/safonov/speedreading/training/fragment/passcource/repository/etity/PassCourseResult;",
            "Ljava/util/Map",
            "<",
            "Lio/realm/RealmModel;",
            "Ljava/lang/Long;",
            ">;)J"
        }
    .end annotation

    .prologue
    .line 505
    .local p2, "cache":Ljava/util/Map;, "Ljava/util/Map<Lio/realm/RealmModel;Ljava/lang/Long;>;"
    move-object/from16 v0, p1

    instance-of v6, v0, Lio/realm/internal/RealmObjectProxy;

    if-eqz v6, :cond_0

    move-object/from16 v6, p1

    check-cast v6, Lio/realm/internal/RealmObjectProxy;

    invoke-interface {v6}, Lio/realm/internal/RealmObjectProxy;->realmGet$proxyState()Lio/realm/ProxyState;

    move-result-object v6

    invoke-virtual {v6}, Lio/realm/ProxyState;->getRealm$realm()Lio/realm/BaseRealm;

    move-result-object v6

    if-eqz v6, :cond_0

    move-object/from16 v6, p1

    check-cast v6, Lio/realm/internal/RealmObjectProxy;

    invoke-interface {v6}, Lio/realm/internal/RealmObjectProxy;->realmGet$proxyState()Lio/realm/ProxyState;

    move-result-object v6

    invoke-virtual {v6}, Lio/realm/ProxyState;->getRealm$realm()Lio/realm/BaseRealm;

    move-result-object v6

    invoke-virtual {v6}, Lio/realm/BaseRealm;->getPath()Ljava/lang/String;

    move-result-object v6

    invoke-virtual/range {p0 .. p0}, Lio/realm/Realm;->getPath()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 506
    check-cast p1, Lio/realm/internal/RealmObjectProxy;

    .end local p1    # "object":Lcom/safonov/speedreading/training/fragment/passcource/repository/etity/PassCourseResult;
    invoke-interface/range {p1 .. p1}, Lio/realm/internal/RealmObjectProxy;->realmGet$proxyState()Lio/realm/ProxyState;

    move-result-object v6

    invoke-virtual {v6}, Lio/realm/ProxyState;->getRow$realm()Lio/realm/internal/Row;

    move-result-object v6

    invoke-interface {v6}, Lio/realm/internal/Row;->getIndex()J

    move-result-wide v10

    .line 524
    :goto_0
    return-wide v10

    .line 508
    .restart local p1    # "object":Lcom/safonov/speedreading/training/fragment/passcource/repository/etity/PassCourseResult;
    :cond_0
    const-class v6, Lcom/safonov/speedreading/training/fragment/passcource/repository/etity/PassCourseResult;

    move-object/from16 v0, p0

    invoke-virtual {v0, v6}, Lio/realm/Realm;->getTable(Ljava/lang/Class;)Lio/realm/internal/Table;

    move-result-object v17

    .line 509
    .local v17, "table":Lio/realm/internal/Table;
    invoke-virtual/range {v17 .. v17}, Lio/realm/internal/Table;->getNativeTablePointer()J

    move-result-wide v2

    .line 510
    .local v2, "tableNativePtr":J
    move-object/from16 v0, p0

    iget-object v6, v0, Lio/realm/Realm;->schema:Lio/realm/RealmSchema;

    const-class v7, Lcom/safonov/speedreading/training/fragment/passcource/repository/etity/PassCourseResult;

    invoke-virtual {v6, v7}, Lio/realm/RealmSchema;->getColumnInfo(Ljava/lang/Class;)Lio/realm/internal/ColumnInfo;

    move-result-object v15

    check-cast v15, Lio/realm/PassCourseResultRealmProxy$PassCourseResultColumnInfo;

    .line 511
    .local v15, "columnInfo":Lio/realm/PassCourseResultRealmProxy$PassCourseResultColumnInfo;
    invoke-virtual/range {v17 .. v17}, Lio/realm/internal/Table;->getPrimaryKey()J

    move-result-wide v4

    .line 512
    .local v4, "pkColumnIndex":J
    const-wide/16 v10, -0x1

    .local v10, "rowIndex":J
    move-object/from16 v6, p1

    .line 513
    check-cast v6, Lio/realm/PassCourseResultRealmProxyInterface;

    invoke-interface {v6}, Lio/realm/PassCourseResultRealmProxyInterface;->realmGet$id()I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v16

    .line 514
    .local v16, "primaryKeyValue":Ljava/lang/Integer;
    if-eqz v16, :cond_1

    move-object/from16 v6, p1

    .line 515
    check-cast v6, Lio/realm/PassCourseResultRealmProxyInterface;

    invoke-interface {v6}, Lio/realm/PassCourseResultRealmProxyInterface;->realmGet$id()I

    move-result v6

    int-to-long v6, v6

    invoke-static/range {v2 .. v7}, Lio/realm/internal/Table;->nativeFindFirstInt(JJJ)J

    move-result-wide v10

    .line 517
    :cond_1
    const-wide/16 v6, -0x1

    cmp-long v6, v10, v6

    if-nez v6, :cond_2

    move-object/from16 v6, p1

    .line 518
    check-cast v6, Lio/realm/PassCourseResultRealmProxyInterface;

    invoke-interface {v6}, Lio/realm/PassCourseResultRealmProxyInterface;->realmGet$id()I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    const/4 v7, 0x0

    move-object/from16 v0, v17

    invoke-virtual {v0, v6, v7}, Lio/realm/internal/Table;->addEmptyRowWithPrimaryKey(Ljava/lang/Object;Z)J

    move-result-wide v10

    .line 520
    :cond_2
    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    move-object/from16 v0, p2

    move-object/from16 v1, p1

    invoke-interface {v0, v1, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 521
    iget-wide v8, v15, Lio/realm/PassCourseResultRealmProxy$PassCourseResultColumnInfo;->typeIndex:J

    move-object/from16 v6, p1

    check-cast v6, Lio/realm/PassCourseResultRealmProxyInterface;

    invoke-interface {v6}, Lio/realm/PassCourseResultRealmProxyInterface;->realmGet$type()I

    move-result v6

    int-to-long v12, v6

    const/4 v14, 0x0

    move-wide v6, v2

    invoke-static/range {v6 .. v14}, Lio/realm/internal/Table;->nativeSetLong(JJJJZ)V

    .line 522
    iget-wide v8, v15, Lio/realm/PassCourseResultRealmProxy$PassCourseResultColumnInfo;->scoreIndex:J

    move-object/from16 v6, p1

    check-cast v6, Lio/realm/PassCourseResultRealmProxyInterface;

    invoke-interface {v6}, Lio/realm/PassCourseResultRealmProxyInterface;->realmGet$score()I

    move-result v6

    int-to-long v12, v6

    const/4 v14, 0x0

    move-wide v6, v2

    invoke-static/range {v6 .. v14}, Lio/realm/internal/Table;->nativeSetLong(JJJJZ)V

    .line 523
    iget-wide v8, v15, Lio/realm/PassCourseResultRealmProxy$PassCourseResultColumnInfo;->unixTimeIndex:J

    check-cast p1, Lio/realm/PassCourseResultRealmProxyInterface;

    .end local p1    # "object":Lcom/safonov/speedreading/training/fragment/passcource/repository/etity/PassCourseResult;
    invoke-interface/range {p1 .. p1}, Lio/realm/PassCourseResultRealmProxyInterface;->realmGet$unixTime()J

    move-result-wide v12

    const/4 v14, 0x0

    move-wide v6, v2

    invoke-static/range {v6 .. v14}, Lio/realm/internal/Table;->nativeSetLong(JJJJZ)V

    goto/16 :goto_0
.end method

.method public static insertOrUpdate(Lio/realm/Realm;Ljava/util/Iterator;Ljava/util/Map;)V
    .locals 19
    .param p0, "realm"    # Lio/realm/Realm;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/realm/Realm;",
            "Ljava/util/Iterator",
            "<+",
            "Lio/realm/RealmModel;",
            ">;",
            "Ljava/util/Map",
            "<",
            "Lio/realm/RealmModel;",
            "Ljava/lang/Long;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 528
    .local p1, "objects":Ljava/util/Iterator;, "Ljava/util/Iterator<+Lio/realm/RealmModel;>;"
    .local p2, "cache":Ljava/util/Map;, "Ljava/util/Map<Lio/realm/RealmModel;Ljava/lang/Long;>;"
    const-class v6, Lcom/safonov/speedreading/training/fragment/passcource/repository/etity/PassCourseResult;

    move-object/from16 v0, p0

    invoke-virtual {v0, v6}, Lio/realm/Realm;->getTable(Ljava/lang/Class;)Lio/realm/internal/Table;

    move-result-object v18

    .line 529
    .local v18, "table":Lio/realm/internal/Table;
    invoke-virtual/range {v18 .. v18}, Lio/realm/internal/Table;->getNativeTablePointer()J

    move-result-wide v2

    .line 530
    .local v2, "tableNativePtr":J
    move-object/from16 v0, p0

    iget-object v6, v0, Lio/realm/Realm;->schema:Lio/realm/RealmSchema;

    const-class v7, Lcom/safonov/speedreading/training/fragment/passcource/repository/etity/PassCourseResult;

    invoke-virtual {v6, v7}, Lio/realm/RealmSchema;->getColumnInfo(Ljava/lang/Class;)Lio/realm/internal/ColumnInfo;

    move-result-object v15

    check-cast v15, Lio/realm/PassCourseResultRealmProxy$PassCourseResultColumnInfo;

    .line 531
    .local v15, "columnInfo":Lio/realm/PassCourseResultRealmProxy$PassCourseResultColumnInfo;
    invoke-virtual/range {v18 .. v18}, Lio/realm/internal/Table;->getPrimaryKey()J

    move-result-wide v4

    .line 532
    .local v4, "pkColumnIndex":J
    const/16 v16, 0x0

    .line 533
    .local v16, "object":Lcom/safonov/speedreading/training/fragment/passcource/repository/etity/PassCourseResult;
    :cond_0
    :goto_0
    invoke-interface/range {p1 .. p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_4

    .line 534
    invoke-interface/range {p1 .. p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v16

    .end local v16    # "object":Lcom/safonov/speedreading/training/fragment/passcource/repository/etity/PassCourseResult;
    check-cast v16, Lcom/safonov/speedreading/training/fragment/passcource/repository/etity/PassCourseResult;

    .line 535
    .restart local v16    # "object":Lcom/safonov/speedreading/training/fragment/passcource/repository/etity/PassCourseResult;
    move-object/from16 v0, p2

    move-object/from16 v1, v16

    invoke-interface {v0, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_0

    .line 536
    move-object/from16 v0, v16

    instance-of v6, v0, Lio/realm/internal/RealmObjectProxy;

    if-eqz v6, :cond_1

    move-object/from16 v6, v16

    check-cast v6, Lio/realm/internal/RealmObjectProxy;

    invoke-interface {v6}, Lio/realm/internal/RealmObjectProxy;->realmGet$proxyState()Lio/realm/ProxyState;

    move-result-object v6

    invoke-virtual {v6}, Lio/realm/ProxyState;->getRealm$realm()Lio/realm/BaseRealm;

    move-result-object v6

    if-eqz v6, :cond_1

    move-object/from16 v6, v16

    check-cast v6, Lio/realm/internal/RealmObjectProxy;

    invoke-interface {v6}, Lio/realm/internal/RealmObjectProxy;->realmGet$proxyState()Lio/realm/ProxyState;

    move-result-object v6

    invoke-virtual {v6}, Lio/realm/ProxyState;->getRealm$realm()Lio/realm/BaseRealm;

    move-result-object v6

    invoke-virtual {v6}, Lio/realm/BaseRealm;->getPath()Ljava/lang/String;

    move-result-object v6

    invoke-virtual/range {p0 .. p0}, Lio/realm/Realm;->getPath()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1

    move-object/from16 v6, v16

    .line 537
    check-cast v6, Lio/realm/internal/RealmObjectProxy;

    invoke-interface {v6}, Lio/realm/internal/RealmObjectProxy;->realmGet$proxyState()Lio/realm/ProxyState;

    move-result-object v6

    invoke-virtual {v6}, Lio/realm/ProxyState;->getRow$realm()Lio/realm/internal/Row;

    move-result-object v6

    invoke-interface {v6}, Lio/realm/internal/Row;->getIndex()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    move-object/from16 v0, p2

    move-object/from16 v1, v16

    invoke-interface {v0, v1, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 540
    :cond_1
    const-wide/16 v10, -0x1

    .local v10, "rowIndex":J
    move-object/from16 v6, v16

    .line 541
    check-cast v6, Lio/realm/PassCourseResultRealmProxyInterface;

    invoke-interface {v6}, Lio/realm/PassCourseResultRealmProxyInterface;->realmGet$id()I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v17

    .line 542
    .local v17, "primaryKeyValue":Ljava/lang/Integer;
    if-eqz v17, :cond_2

    move-object/from16 v6, v16

    .line 543
    check-cast v6, Lio/realm/PassCourseResultRealmProxyInterface;

    invoke-interface {v6}, Lio/realm/PassCourseResultRealmProxyInterface;->realmGet$id()I

    move-result v6

    int-to-long v6, v6

    invoke-static/range {v2 .. v7}, Lio/realm/internal/Table;->nativeFindFirstInt(JJJ)J

    move-result-wide v10

    .line 545
    :cond_2
    const-wide/16 v6, -0x1

    cmp-long v6, v10, v6

    if-nez v6, :cond_3

    move-object/from16 v6, v16

    .line 546
    check-cast v6, Lio/realm/PassCourseResultRealmProxyInterface;

    invoke-interface {v6}, Lio/realm/PassCourseResultRealmProxyInterface;->realmGet$id()I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    const/4 v7, 0x0

    move-object/from16 v0, v18

    invoke-virtual {v0, v6, v7}, Lio/realm/internal/Table;->addEmptyRowWithPrimaryKey(Ljava/lang/Object;Z)J

    move-result-wide v10

    .line 548
    :cond_3
    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    move-object/from16 v0, p2

    move-object/from16 v1, v16

    invoke-interface {v0, v1, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 549
    iget-wide v8, v15, Lio/realm/PassCourseResultRealmProxy$PassCourseResultColumnInfo;->typeIndex:J

    move-object/from16 v6, v16

    check-cast v6, Lio/realm/PassCourseResultRealmProxyInterface;

    invoke-interface {v6}, Lio/realm/PassCourseResultRealmProxyInterface;->realmGet$type()I

    move-result v6

    int-to-long v12, v6

    const/4 v14, 0x0

    move-wide v6, v2

    invoke-static/range {v6 .. v14}, Lio/realm/internal/Table;->nativeSetLong(JJJJZ)V

    .line 550
    iget-wide v8, v15, Lio/realm/PassCourseResultRealmProxy$PassCourseResultColumnInfo;->scoreIndex:J

    move-object/from16 v6, v16

    check-cast v6, Lio/realm/PassCourseResultRealmProxyInterface;

    invoke-interface {v6}, Lio/realm/PassCourseResultRealmProxyInterface;->realmGet$score()I

    move-result v6

    int-to-long v12, v6

    const/4 v14, 0x0

    move-wide v6, v2

    invoke-static/range {v6 .. v14}, Lio/realm/internal/Table;->nativeSetLong(JJJJZ)V

    .line 551
    iget-wide v8, v15, Lio/realm/PassCourseResultRealmProxy$PassCourseResultColumnInfo;->unixTimeIndex:J

    move-object/from16 v6, v16

    check-cast v6, Lio/realm/PassCourseResultRealmProxyInterface;

    invoke-interface {v6}, Lio/realm/PassCourseResultRealmProxyInterface;->realmGet$unixTime()J

    move-result-wide v12

    const/4 v14, 0x0

    move-wide v6, v2

    invoke-static/range {v6 .. v14}, Lio/realm/internal/Table;->nativeSetLong(JJJJZ)V

    goto/16 :goto_0

    .line 554
    .end local v10    # "rowIndex":J
    .end local v17    # "primaryKeyValue":Ljava/lang/Integer;
    :cond_4
    return-void
.end method

.method static update(Lio/realm/Realm;Lcom/safonov/speedreading/training/fragment/passcource/repository/etity/PassCourseResult;Lcom/safonov/speedreading/training/fragment/passcource/repository/etity/PassCourseResult;Ljava/util/Map;)Lcom/safonov/speedreading/training/fragment/passcource/repository/etity/PassCourseResult;
    .locals 4
    .param p0, "realm"    # Lio/realm/Realm;
    .param p1, "realmObject"    # Lcom/safonov/speedreading/training/fragment/passcource/repository/etity/PassCourseResult;
    .param p2, "newObject"    # Lcom/safonov/speedreading/training/fragment/passcource/repository/etity/PassCourseResult;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/realm/Realm;",
            "Lcom/safonov/speedreading/training/fragment/passcource/repository/etity/PassCourseResult;",
            "Lcom/safonov/speedreading/training/fragment/passcource/repository/etity/PassCourseResult;",
            "Ljava/util/Map",
            "<",
            "Lio/realm/RealmModel;",
            "Lio/realm/internal/RealmObjectProxy;",
            ">;)",
            "Lcom/safonov/speedreading/training/fragment/passcource/repository/etity/PassCourseResult;"
        }
    .end annotation

    .prologue
    .line 582
    .local p3, "cache":Ljava/util/Map;, "Ljava/util/Map<Lio/realm/RealmModel;Lio/realm/internal/RealmObjectProxy;>;"
    move-object v0, p1

    check-cast v0, Lio/realm/PassCourseResultRealmProxyInterface;

    move-object v1, p2

    check-cast v1, Lio/realm/PassCourseResultRealmProxyInterface;

    invoke-interface {v1}, Lio/realm/PassCourseResultRealmProxyInterface;->realmGet$type()I

    move-result v1

    invoke-interface {v0, v1}, Lio/realm/PassCourseResultRealmProxyInterface;->realmSet$type(I)V

    move-object v0, p1

    .line 583
    check-cast v0, Lio/realm/PassCourseResultRealmProxyInterface;

    move-object v1, p2

    check-cast v1, Lio/realm/PassCourseResultRealmProxyInterface;

    invoke-interface {v1}, Lio/realm/PassCourseResultRealmProxyInterface;->realmGet$score()I

    move-result v1

    invoke-interface {v0, v1}, Lio/realm/PassCourseResultRealmProxyInterface;->realmSet$score(I)V

    move-object v0, p1

    .line 584
    check-cast v0, Lio/realm/PassCourseResultRealmProxyInterface;

    check-cast p2, Lio/realm/PassCourseResultRealmProxyInterface;

    .end local p2    # "newObject":Lcom/safonov/speedreading/training/fragment/passcource/repository/etity/PassCourseResult;
    invoke-interface {p2}, Lio/realm/PassCourseResultRealmProxyInterface;->realmGet$unixTime()J

    move-result-wide v2

    invoke-interface {v0, v2, v3}, Lio/realm/PassCourseResultRealmProxyInterface;->realmSet$unixTime(J)V

    .line 585
    return-object p1
.end method

.method public static validateTable(Lio/realm/internal/SharedRealm;Z)Lio/realm/PassCourseResultRealmProxy$PassCourseResultColumnInfo;
    .locals 12
    .param p0, "sharedRealm"    # Lio/realm/internal/SharedRealm;
    .param p1, "allowExtraColumns"    # Z

    .prologue
    const-wide/16 v8, 0x4

    .line 206
    const-string v7, "class_PassCourseResult"

    invoke-virtual {p0, v7}, Lio/realm/internal/SharedRealm;->hasTable(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_12

    .line 207
    const-string v7, "class_PassCourseResult"

    invoke-virtual {p0, v7}, Lio/realm/internal/SharedRealm;->getTable(Ljava/lang/String;)Lio/realm/internal/Table;

    move-result-object v6

    .line 208
    .local v6, "table":Lio/realm/internal/Table;
    invoke-virtual {v6}, Lio/realm/internal/Table;->getColumnCount()J

    move-result-wide v0

    .line 209
    .local v0, "columnCount":J
    cmp-long v7, v0, v8

    if-eqz v7, :cond_1

    .line 210
    cmp-long v7, v0, v8

    if-gez v7, :cond_0

    .line 211
    new-instance v7, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->getPath()Ljava/lang/String;

    move-result-object v8

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Field count is less than expected - expected 4 but was "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-direct {v7, v8, v9}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v7

    .line 213
    :cond_0
    if-eqz p1, :cond_2

    .line 214
    const-string v7, "Field count is more than expected - expected 4 but was %1$d"

    const/4 v8, 0x1

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v10

    aput-object v10, v8, v9

    invoke-static {v7, v8}, Lio/realm/log/RealmLog;->debug(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 219
    :cond_1
    new-instance v3, Ljava/util/HashMap;

    invoke-direct {v3}, Ljava/util/HashMap;-><init>()V

    .line 220
    .local v3, "columnTypes":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lio/realm/RealmFieldType;>;"
    const-wide/16 v4, 0x0

    .local v4, "i":J
    :goto_0
    cmp-long v7, v4, v0

    if-gez v7, :cond_3

    .line 221
    invoke-virtual {v6, v4, v5}, Lio/realm/internal/Table;->getColumnName(J)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v4, v5}, Lio/realm/internal/Table;->getColumnType(J)Lio/realm/RealmFieldType;

    move-result-object v8

    invoke-interface {v3, v7, v8}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 220
    const-wide/16 v8, 0x1

    add-long/2addr v4, v8

    goto :goto_0

    .line 216
    .end local v3    # "columnTypes":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lio/realm/RealmFieldType;>;"
    .end local v4    # "i":J
    :cond_2
    new-instance v7, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->getPath()Ljava/lang/String;

    move-result-object v8

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Field count is more than expected - expected 4 but was "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-direct {v7, v8, v9}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v7

    .line 224
    .restart local v3    # "columnTypes":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lio/realm/RealmFieldType;>;"
    .restart local v4    # "i":J
    :cond_3
    new-instance v2, Lio/realm/PassCourseResultRealmProxy$PassCourseResultColumnInfo;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->getPath()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v2, v7, v6}, Lio/realm/PassCourseResultRealmProxy$PassCourseResultColumnInfo;-><init>(Ljava/lang/String;Lio/realm/internal/Table;)V

    .line 226
    .local v2, "columnInfo":Lio/realm/PassCourseResultRealmProxy$PassCourseResultColumnInfo;
    invoke-virtual {v6}, Lio/realm/internal/Table;->hasPrimaryKey()Z

    move-result v7

    if-nez v7, :cond_4

    .line 227
    new-instance v7, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->getPath()Ljava/lang/String;

    move-result-object v8

    const-string v9, "Primary key not defined for field \'id\' in existing Realm file. @PrimaryKey was added."

    invoke-direct {v7, v8, v9}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v7

    .line 229
    :cond_4
    invoke-virtual {v6}, Lio/realm/internal/Table;->getPrimaryKey()J

    move-result-wide v8

    iget-wide v10, v2, Lio/realm/PassCourseResultRealmProxy$PassCourseResultColumnInfo;->idIndex:J

    cmp-long v7, v8, v10

    if-eqz v7, :cond_5

    .line 230
    new-instance v7, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->getPath()Ljava/lang/String;

    move-result-object v8

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Primary Key annotation definition was changed, from field "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v6}, Lio/realm/internal/Table;->getPrimaryKey()J

    move-result-wide v10

    invoke-virtual {v6, v10, v11}, Lio/realm/internal/Table;->getColumnName(J)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " to field id"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-direct {v7, v8, v9}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v7

    .line 234
    :cond_5
    const-string v7, "id"

    invoke-interface {v3, v7}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_6

    .line 235
    new-instance v7, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->getPath()Ljava/lang/String;

    move-result-object v8

    const-string v9, "Missing field \'id\' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn()."

    invoke-direct {v7, v8, v9}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v7

    .line 237
    :cond_6
    const-string v7, "id"

    invoke-interface {v3, v7}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    sget-object v8, Lio/realm/RealmFieldType;->INTEGER:Lio/realm/RealmFieldType;

    if-eq v7, v8, :cond_7

    .line 238
    new-instance v7, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->getPath()Ljava/lang/String;

    move-result-object v8

    const-string v9, "Invalid type \'int\' for field \'id\' in existing Realm file."

    invoke-direct {v7, v8, v9}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v7

    .line 240
    :cond_7
    iget-wide v8, v2, Lio/realm/PassCourseResultRealmProxy$PassCourseResultColumnInfo;->idIndex:J

    invoke-virtual {v6, v8, v9}, Lio/realm/internal/Table;->isColumnNullable(J)Z

    move-result v7

    if-eqz v7, :cond_8

    iget-wide v8, v2, Lio/realm/PassCourseResultRealmProxy$PassCourseResultColumnInfo;->idIndex:J

    invoke-virtual {v6, v8, v9}, Lio/realm/internal/Table;->findFirstNull(J)J

    move-result-wide v8

    const-wide/16 v10, -0x1

    cmp-long v7, v8, v10

    if-eqz v7, :cond_8

    .line 241
    new-instance v7, Ljava/lang/IllegalStateException;

    const-string v8, "Cannot migrate an object with null value in field \'id\'. Either maintain the same type for primary key field \'id\', or remove the object with null value before migration."

    invoke-direct {v7, v8}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v7

    .line 243
    :cond_8
    const-string v7, "id"

    invoke-virtual {v6, v7}, Lio/realm/internal/Table;->getColumnIndex(Ljava/lang/String;)J

    move-result-wide v8

    invoke-virtual {v6, v8, v9}, Lio/realm/internal/Table;->hasSearchIndex(J)Z

    move-result v7

    if-nez v7, :cond_9

    .line 244
    new-instance v7, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->getPath()Ljava/lang/String;

    move-result-object v8

    const-string v9, "Index not defined for field \'id\' in existing Realm file. Either set @Index or migrate using io.realm.internal.Table.removeSearchIndex()."

    invoke-direct {v7, v8, v9}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v7

    .line 246
    :cond_9
    const-string v7, "type"

    invoke-interface {v3, v7}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_a

    .line 247
    new-instance v7, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->getPath()Ljava/lang/String;

    move-result-object v8

    const-string v9, "Missing field \'type\' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn()."

    invoke-direct {v7, v8, v9}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v7

    .line 249
    :cond_a
    const-string v7, "type"

    invoke-interface {v3, v7}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    sget-object v8, Lio/realm/RealmFieldType;->INTEGER:Lio/realm/RealmFieldType;

    if-eq v7, v8, :cond_b

    .line 250
    new-instance v7, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->getPath()Ljava/lang/String;

    move-result-object v8

    const-string v9, "Invalid type \'int\' for field \'type\' in existing Realm file."

    invoke-direct {v7, v8, v9}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v7

    .line 252
    :cond_b
    iget-wide v8, v2, Lio/realm/PassCourseResultRealmProxy$PassCourseResultColumnInfo;->typeIndex:J

    invoke-virtual {v6, v8, v9}, Lio/realm/internal/Table;->isColumnNullable(J)Z

    move-result v7

    if-eqz v7, :cond_c

    .line 253
    new-instance v7, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->getPath()Ljava/lang/String;

    move-result-object v8

    const-string v9, "Field \'type\' does support null values in the existing Realm file. Use corresponding boxed type for field \'type\' or migrate using RealmObjectSchema.setNullable()."

    invoke-direct {v7, v8, v9}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v7

    .line 255
    :cond_c
    const-string v7, "score"

    invoke-interface {v3, v7}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_d

    .line 256
    new-instance v7, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->getPath()Ljava/lang/String;

    move-result-object v8

    const-string v9, "Missing field \'score\' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn()."

    invoke-direct {v7, v8, v9}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v7

    .line 258
    :cond_d
    const-string v7, "score"

    invoke-interface {v3, v7}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    sget-object v8, Lio/realm/RealmFieldType;->INTEGER:Lio/realm/RealmFieldType;

    if-eq v7, v8, :cond_e

    .line 259
    new-instance v7, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->getPath()Ljava/lang/String;

    move-result-object v8

    const-string v9, "Invalid type \'int\' for field \'score\' in existing Realm file."

    invoke-direct {v7, v8, v9}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v7

    .line 261
    :cond_e
    iget-wide v8, v2, Lio/realm/PassCourseResultRealmProxy$PassCourseResultColumnInfo;->scoreIndex:J

    invoke-virtual {v6, v8, v9}, Lio/realm/internal/Table;->isColumnNullable(J)Z

    move-result v7

    if-eqz v7, :cond_f

    .line 262
    new-instance v7, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->getPath()Ljava/lang/String;

    move-result-object v8

    const-string v9, "Field \'score\' does support null values in the existing Realm file. Use corresponding boxed type for field \'score\' or migrate using RealmObjectSchema.setNullable()."

    invoke-direct {v7, v8, v9}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v7

    .line 264
    :cond_f
    const-string v7, "unixTime"

    invoke-interface {v3, v7}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_10

    .line 265
    new-instance v7, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->getPath()Ljava/lang/String;

    move-result-object v8

    const-string v9, "Missing field \'unixTime\' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn()."

    invoke-direct {v7, v8, v9}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v7

    .line 267
    :cond_10
    const-string v7, "unixTime"

    invoke-interface {v3, v7}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    sget-object v8, Lio/realm/RealmFieldType;->INTEGER:Lio/realm/RealmFieldType;

    if-eq v7, v8, :cond_11

    .line 268
    new-instance v7, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->getPath()Ljava/lang/String;

    move-result-object v8

    const-string v9, "Invalid type \'long\' for field \'unixTime\' in existing Realm file."

    invoke-direct {v7, v8, v9}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v7

    .line 270
    :cond_11
    iget-wide v8, v2, Lio/realm/PassCourseResultRealmProxy$PassCourseResultColumnInfo;->unixTimeIndex:J

    invoke-virtual {v6, v8, v9}, Lio/realm/internal/Table;->isColumnNullable(J)Z

    move-result v7

    if-eqz v7, :cond_13

    .line 271
    new-instance v7, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->getPath()Ljava/lang/String;

    move-result-object v8

    const-string v9, "Field \'unixTime\' does support null values in the existing Realm file. Use corresponding boxed type for field \'unixTime\' or migrate using RealmObjectSchema.setNullable()."

    invoke-direct {v7, v8, v9}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v7

    .line 275
    .end local v0    # "columnCount":J
    .end local v2    # "columnInfo":Lio/realm/PassCourseResultRealmProxy$PassCourseResultColumnInfo;
    .end local v3    # "columnTypes":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lio/realm/RealmFieldType;>;"
    .end local v4    # "i":J
    .end local v6    # "table":Lio/realm/internal/Table;
    :cond_12
    new-instance v7, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->getPath()Ljava/lang/String;

    move-result-object v8

    const-string v9, "The \'PassCourseResult\' class is missing from the schema for this Realm."

    invoke-direct {v7, v8, v9}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v7

    .line 273
    .restart local v0    # "columnCount":J
    .restart local v2    # "columnInfo":Lio/realm/PassCourseResultRealmProxy$PassCourseResultColumnInfo;
    .restart local v3    # "columnTypes":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lio/realm/RealmFieldType;>;"
    .restart local v4    # "i":J
    .restart local v6    # "table":Lio/realm/internal/Table;
    :cond_13
    return-object v2
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 12
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v5, 0x1

    const/4 v6, 0x0

    .line 633
    if-ne p0, p1, :cond_1

    .line 647
    :cond_0
    :goto_0
    return v5

    .line 634
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v7

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v8

    if-eq v7, v8, :cond_3

    :cond_2
    move v5, v6

    goto :goto_0

    :cond_3
    move-object v0, p1

    .line 635
    check-cast v0, Lio/realm/PassCourseResultRealmProxy;

    .line 637
    .local v0, "aPassCourseResult":Lio/realm/PassCourseResultRealmProxy;
    iget-object v7, p0, Lio/realm/PassCourseResultRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v7}, Lio/realm/ProxyState;->getRealm$realm()Lio/realm/BaseRealm;

    move-result-object v7

    invoke-virtual {v7}, Lio/realm/BaseRealm;->getPath()Ljava/lang/String;

    move-result-object v3

    .line 638
    .local v3, "path":Ljava/lang/String;
    iget-object v7, v0, Lio/realm/PassCourseResultRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v7}, Lio/realm/ProxyState;->getRealm$realm()Lio/realm/BaseRealm;

    move-result-object v7

    invoke-virtual {v7}, Lio/realm/BaseRealm;->getPath()Ljava/lang/String;

    move-result-object v1

    .line 639
    .local v1, "otherPath":Ljava/lang/String;
    if-eqz v3, :cond_5

    invoke-virtual {v3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_6

    :cond_4
    move v5, v6

    goto :goto_0

    :cond_5
    if-nez v1, :cond_4

    .line 641
    :cond_6
    iget-object v7, p0, Lio/realm/PassCourseResultRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v7}, Lio/realm/ProxyState;->getRow$realm()Lio/realm/internal/Row;

    move-result-object v7

    invoke-interface {v7}, Lio/realm/internal/Row;->getTable()Lio/realm/internal/Table;

    move-result-object v7

    invoke-virtual {v7}, Lio/realm/internal/Table;->getName()Ljava/lang/String;

    move-result-object v4

    .line 642
    .local v4, "tableName":Ljava/lang/String;
    iget-object v7, v0, Lio/realm/PassCourseResultRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v7}, Lio/realm/ProxyState;->getRow$realm()Lio/realm/internal/Row;

    move-result-object v7

    invoke-interface {v7}, Lio/realm/internal/Row;->getTable()Lio/realm/internal/Table;

    move-result-object v7

    invoke-virtual {v7}, Lio/realm/internal/Table;->getName()Ljava/lang/String;

    move-result-object v2

    .line 643
    .local v2, "otherTableName":Ljava/lang/String;
    if-eqz v4, :cond_8

    invoke-virtual {v4, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_9

    :cond_7
    move v5, v6

    goto :goto_0

    :cond_8
    if-nez v2, :cond_7

    .line 645
    :cond_9
    iget-object v7, p0, Lio/realm/PassCourseResultRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v7}, Lio/realm/ProxyState;->getRow$realm()Lio/realm/internal/Row;

    move-result-object v7

    invoke-interface {v7}, Lio/realm/internal/Row;->getIndex()J

    move-result-wide v8

    iget-object v7, v0, Lio/realm/PassCourseResultRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v7}, Lio/realm/ProxyState;->getRow$realm()Lio/realm/internal/Row;

    move-result-object v7

    invoke-interface {v7}, Lio/realm/internal/Row;->getIndex()J

    move-result-wide v10

    cmp-long v7, v8, v10

    if-eqz v7, :cond_0

    move v5, v6

    goto :goto_0
.end method

.method public hashCode()I
    .locals 8

    .prologue
    const/4 v5, 0x0

    .line 620
    iget-object v6, p0, Lio/realm/PassCourseResultRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v6}, Lio/realm/ProxyState;->getRealm$realm()Lio/realm/BaseRealm;

    move-result-object v6

    invoke-virtual {v6}, Lio/realm/BaseRealm;->getPath()Ljava/lang/String;

    move-result-object v0

    .line 621
    .local v0, "realmName":Ljava/lang/String;
    iget-object v6, p0, Lio/realm/PassCourseResultRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v6}, Lio/realm/ProxyState;->getRow$realm()Lio/realm/internal/Row;

    move-result-object v6

    invoke-interface {v6}, Lio/realm/internal/Row;->getTable()Lio/realm/internal/Table;

    move-result-object v6

    invoke-virtual {v6}, Lio/realm/internal/Table;->getName()Ljava/lang/String;

    move-result-object v4

    .line 622
    .local v4, "tableName":Ljava/lang/String;
    iget-object v6, p0, Lio/realm/PassCourseResultRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v6}, Lio/realm/ProxyState;->getRow$realm()Lio/realm/internal/Row;

    move-result-object v6

    invoke-interface {v6}, Lio/realm/internal/Row;->getIndex()J

    move-result-wide v2

    .line 624
    .local v2, "rowIndex":J
    const/16 v1, 0x11

    .line 625
    .local v1, "result":I
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v6

    :goto_0
    add-int/lit16 v1, v6, 0x20f

    .line 626
    mul-int/lit8 v6, v1, 0x1f

    if-eqz v4, :cond_0

    invoke-virtual {v4}, Ljava/lang/String;->hashCode()I

    move-result v5

    :cond_0
    add-int v1, v6, v5

    .line 627
    mul-int/lit8 v5, v1, 0x1f

    const/16 v6, 0x20

    ushr-long v6, v2, v6

    xor-long/2addr v6, v2

    long-to-int v6, v6

    add-int v1, v5, v6

    .line 628
    return v1

    :cond_1
    move v6, v5

    .line 625
    goto :goto_0
.end method

.method public realm$injectObjectContext()V
    .locals 3

    .prologue
    .line 91
    iget-object v1, p0, Lio/realm/PassCourseResultRealmProxy;->proxyState:Lio/realm/ProxyState;

    if-eqz v1, :cond_0

    .line 101
    :goto_0
    return-void

    .line 94
    :cond_0
    sget-object v1, Lio/realm/BaseRealm;->objectContext:Lio/realm/BaseRealm$ThreadLocalRealmObjectContext;

    invoke-virtual {v1}, Lio/realm/BaseRealm$ThreadLocalRealmObjectContext;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/realm/BaseRealm$RealmObjectContext;

    .line 95
    .local v0, "context":Lio/realm/BaseRealm$RealmObjectContext;
    invoke-virtual {v0}, Lio/realm/BaseRealm$RealmObjectContext;->getColumnInfo()Lio/realm/internal/ColumnInfo;

    move-result-object v1

    check-cast v1, Lio/realm/PassCourseResultRealmProxy$PassCourseResultColumnInfo;

    iput-object v1, p0, Lio/realm/PassCourseResultRealmProxy;->columnInfo:Lio/realm/PassCourseResultRealmProxy$PassCourseResultColumnInfo;

    .line 96
    new-instance v1, Lio/realm/ProxyState;

    invoke-direct {v1, p0}, Lio/realm/ProxyState;-><init>(Lio/realm/RealmModel;)V

    iput-object v1, p0, Lio/realm/PassCourseResultRealmProxy;->proxyState:Lio/realm/ProxyState;

    .line 97
    iget-object v1, p0, Lio/realm/PassCourseResultRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v0}, Lio/realm/BaseRealm$RealmObjectContext;->getRealm()Lio/realm/BaseRealm;

    move-result-object v2

    invoke-virtual {v1, v2}, Lio/realm/ProxyState;->setRealm$realm(Lio/realm/BaseRealm;)V

    .line 98
    iget-object v1, p0, Lio/realm/PassCourseResultRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v0}, Lio/realm/BaseRealm$RealmObjectContext;->getRow()Lio/realm/internal/Row;

    move-result-object v2

    invoke-virtual {v1, v2}, Lio/realm/ProxyState;->setRow$realm(Lio/realm/internal/Row;)V

    .line 99
    iget-object v1, p0, Lio/realm/PassCourseResultRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v0}, Lio/realm/BaseRealm$RealmObjectContext;->getAcceptDefaultValue()Z

    move-result v2

    invoke-virtual {v1, v2}, Lio/realm/ProxyState;->setAcceptDefaultValue$realm(Z)V

    .line 100
    iget-object v1, p0, Lio/realm/PassCourseResultRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v0}, Lio/realm/BaseRealm$RealmObjectContext;->getExcludeFields()Ljava/util/List;

    move-result-object v2

    invoke-virtual {v1, v2}, Lio/realm/ProxyState;->setExcludeFields$realm(Ljava/util/List;)V

    goto :goto_0
.end method

.method public realmGet$id()I
    .locals 4

    .prologue
    .line 105
    iget-object v0, p0, Lio/realm/PassCourseResultRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v0}, Lio/realm/ProxyState;->getRealm$realm()Lio/realm/BaseRealm;

    move-result-object v0

    invoke-virtual {v0}, Lio/realm/BaseRealm;->checkIfValid()V

    .line 106
    iget-object v0, p0, Lio/realm/PassCourseResultRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v0}, Lio/realm/ProxyState;->getRow$realm()Lio/realm/internal/Row;

    move-result-object v0

    iget-object v1, p0, Lio/realm/PassCourseResultRealmProxy;->columnInfo:Lio/realm/PassCourseResultRealmProxy$PassCourseResultColumnInfo;

    iget-wide v2, v1, Lio/realm/PassCourseResultRealmProxy$PassCourseResultColumnInfo;->idIndex:J

    invoke-interface {v0, v2, v3}, Lio/realm/internal/Row;->getLong(J)J

    move-result-wide v0

    long-to-int v0, v0

    return v0
.end method

.method public realmGet$proxyState()Lio/realm/ProxyState;
    .locals 1

    .prologue
    .line 615
    iget-object v0, p0, Lio/realm/PassCourseResultRealmProxy;->proxyState:Lio/realm/ProxyState;

    return-object v0
.end method

.method public realmGet$score()I
    .locals 4

    .prologue
    .line 141
    iget-object v0, p0, Lio/realm/PassCourseResultRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v0}, Lio/realm/ProxyState;->getRealm$realm()Lio/realm/BaseRealm;

    move-result-object v0

    invoke-virtual {v0}, Lio/realm/BaseRealm;->checkIfValid()V

    .line 142
    iget-object v0, p0, Lio/realm/PassCourseResultRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v0}, Lio/realm/ProxyState;->getRow$realm()Lio/realm/internal/Row;

    move-result-object v0

    iget-object v1, p0, Lio/realm/PassCourseResultRealmProxy;->columnInfo:Lio/realm/PassCourseResultRealmProxy$PassCourseResultColumnInfo;

    iget-wide v2, v1, Lio/realm/PassCourseResultRealmProxy$PassCourseResultColumnInfo;->scoreIndex:J

    invoke-interface {v0, v2, v3}, Lio/realm/internal/Row;->getLong(J)J

    move-result-wide v0

    long-to-int v0, v0

    return v0
.end method

.method public realmGet$type()I
    .locals 4

    .prologue
    .line 121
    iget-object v0, p0, Lio/realm/PassCourseResultRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v0}, Lio/realm/ProxyState;->getRealm$realm()Lio/realm/BaseRealm;

    move-result-object v0

    invoke-virtual {v0}, Lio/realm/BaseRealm;->checkIfValid()V

    .line 122
    iget-object v0, p0, Lio/realm/PassCourseResultRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v0}, Lio/realm/ProxyState;->getRow$realm()Lio/realm/internal/Row;

    move-result-object v0

    iget-object v1, p0, Lio/realm/PassCourseResultRealmProxy;->columnInfo:Lio/realm/PassCourseResultRealmProxy$PassCourseResultColumnInfo;

    iget-wide v2, v1, Lio/realm/PassCourseResultRealmProxy$PassCourseResultColumnInfo;->typeIndex:J

    invoke-interface {v0, v2, v3}, Lio/realm/internal/Row;->getLong(J)J

    move-result-wide v0

    long-to-int v0, v0

    return v0
.end method

.method public realmGet$unixTime()J
    .locals 4

    .prologue
    .line 161
    iget-object v0, p0, Lio/realm/PassCourseResultRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v0}, Lio/realm/ProxyState;->getRealm$realm()Lio/realm/BaseRealm;

    move-result-object v0

    invoke-virtual {v0}, Lio/realm/BaseRealm;->checkIfValid()V

    .line 162
    iget-object v0, p0, Lio/realm/PassCourseResultRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v0}, Lio/realm/ProxyState;->getRow$realm()Lio/realm/internal/Row;

    move-result-object v0

    iget-object v1, p0, Lio/realm/PassCourseResultRealmProxy;->columnInfo:Lio/realm/PassCourseResultRealmProxy$PassCourseResultColumnInfo;

    iget-wide v2, v1, Lio/realm/PassCourseResultRealmProxy$PassCourseResultColumnInfo;->unixTimeIndex:J

    invoke-interface {v0, v2, v3}, Lio/realm/internal/Row;->getLong(J)J

    move-result-wide v0

    return-wide v0
.end method

.method public realmSet$id(I)V
    .locals 2
    .param p1, "value"    # I

    .prologue
    .line 110
    iget-object v0, p0, Lio/realm/PassCourseResultRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v0}, Lio/realm/ProxyState;->isUnderConstruction()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 112
    return-void

    .line 115
    :cond_0
    iget-object v0, p0, Lio/realm/PassCourseResultRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v0}, Lio/realm/ProxyState;->getRealm$realm()Lio/realm/BaseRealm;

    move-result-object v0

    invoke-virtual {v0}, Lio/realm/BaseRealm;->checkIfValid()V

    .line 116
    new-instance v0, Lio/realm/exceptions/RealmException;

    const-string v1, "Primary key field \'id\' cannot be changed after object was created."

    invoke-direct {v0, v1}, Lio/realm/exceptions/RealmException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public realmSet$score(I)V
    .locals 9
    .param p1, "value"    # I

    .prologue
    .line 146
    iget-object v1, p0, Lio/realm/PassCourseResultRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v1}, Lio/realm/ProxyState;->isUnderConstruction()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 147
    iget-object v1, p0, Lio/realm/PassCourseResultRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v1}, Lio/realm/ProxyState;->getAcceptDefaultValue$realm()Z

    move-result v1

    if-nez v1, :cond_0

    .line 157
    :goto_0
    return-void

    .line 150
    :cond_0
    iget-object v1, p0, Lio/realm/PassCourseResultRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v1}, Lio/realm/ProxyState;->getRow$realm()Lio/realm/internal/Row;

    move-result-object v0

    .line 151
    .local v0, "row":Lio/realm/internal/Row;
    invoke-interface {v0}, Lio/realm/internal/Row;->getTable()Lio/realm/internal/Table;

    move-result-object v1

    iget-object v2, p0, Lio/realm/PassCourseResultRealmProxy;->columnInfo:Lio/realm/PassCourseResultRealmProxy$PassCourseResultColumnInfo;

    iget-wide v2, v2, Lio/realm/PassCourseResultRealmProxy$PassCourseResultColumnInfo;->scoreIndex:J

    invoke-interface {v0}, Lio/realm/internal/Row;->getIndex()J

    move-result-wide v4

    int-to-long v6, p1

    const/4 v8, 0x1

    invoke-virtual/range {v1 .. v8}, Lio/realm/internal/Table;->setLong(JJJZ)V

    goto :goto_0

    .line 155
    .end local v0    # "row":Lio/realm/internal/Row;
    :cond_1
    iget-object v1, p0, Lio/realm/PassCourseResultRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v1}, Lio/realm/ProxyState;->getRealm$realm()Lio/realm/BaseRealm;

    move-result-object v1

    invoke-virtual {v1}, Lio/realm/BaseRealm;->checkIfValid()V

    .line 156
    iget-object v1, p0, Lio/realm/PassCourseResultRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v1}, Lio/realm/ProxyState;->getRow$realm()Lio/realm/internal/Row;

    move-result-object v1

    iget-object v2, p0, Lio/realm/PassCourseResultRealmProxy;->columnInfo:Lio/realm/PassCourseResultRealmProxy$PassCourseResultColumnInfo;

    iget-wide v2, v2, Lio/realm/PassCourseResultRealmProxy$PassCourseResultColumnInfo;->scoreIndex:J

    int-to-long v4, p1

    invoke-interface {v1, v2, v3, v4, v5}, Lio/realm/internal/Row;->setLong(JJ)V

    goto :goto_0
.end method

.method public realmSet$type(I)V
    .locals 9
    .param p1, "value"    # I

    .prologue
    .line 126
    iget-object v1, p0, Lio/realm/PassCourseResultRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v1}, Lio/realm/ProxyState;->isUnderConstruction()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 127
    iget-object v1, p0, Lio/realm/PassCourseResultRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v1}, Lio/realm/ProxyState;->getAcceptDefaultValue$realm()Z

    move-result v1

    if-nez v1, :cond_0

    .line 137
    :goto_0
    return-void

    .line 130
    :cond_0
    iget-object v1, p0, Lio/realm/PassCourseResultRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v1}, Lio/realm/ProxyState;->getRow$realm()Lio/realm/internal/Row;

    move-result-object v0

    .line 131
    .local v0, "row":Lio/realm/internal/Row;
    invoke-interface {v0}, Lio/realm/internal/Row;->getTable()Lio/realm/internal/Table;

    move-result-object v1

    iget-object v2, p0, Lio/realm/PassCourseResultRealmProxy;->columnInfo:Lio/realm/PassCourseResultRealmProxy$PassCourseResultColumnInfo;

    iget-wide v2, v2, Lio/realm/PassCourseResultRealmProxy$PassCourseResultColumnInfo;->typeIndex:J

    invoke-interface {v0}, Lio/realm/internal/Row;->getIndex()J

    move-result-wide v4

    int-to-long v6, p1

    const/4 v8, 0x1

    invoke-virtual/range {v1 .. v8}, Lio/realm/internal/Table;->setLong(JJJZ)V

    goto :goto_0

    .line 135
    .end local v0    # "row":Lio/realm/internal/Row;
    :cond_1
    iget-object v1, p0, Lio/realm/PassCourseResultRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v1}, Lio/realm/ProxyState;->getRealm$realm()Lio/realm/BaseRealm;

    move-result-object v1

    invoke-virtual {v1}, Lio/realm/BaseRealm;->checkIfValid()V

    .line 136
    iget-object v1, p0, Lio/realm/PassCourseResultRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v1}, Lio/realm/ProxyState;->getRow$realm()Lio/realm/internal/Row;

    move-result-object v1

    iget-object v2, p0, Lio/realm/PassCourseResultRealmProxy;->columnInfo:Lio/realm/PassCourseResultRealmProxy$PassCourseResultColumnInfo;

    iget-wide v2, v2, Lio/realm/PassCourseResultRealmProxy$PassCourseResultColumnInfo;->typeIndex:J

    int-to-long v4, p1

    invoke-interface {v1, v2, v3, v4, v5}, Lio/realm/internal/Row;->setLong(JJ)V

    goto :goto_0
.end method

.method public realmSet$unixTime(J)V
    .locals 9
    .param p1, "value"    # J

    .prologue
    .line 166
    iget-object v1, p0, Lio/realm/PassCourseResultRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v1}, Lio/realm/ProxyState;->isUnderConstruction()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 167
    iget-object v1, p0, Lio/realm/PassCourseResultRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v1}, Lio/realm/ProxyState;->getAcceptDefaultValue$realm()Z

    move-result v1

    if-nez v1, :cond_0

    .line 177
    :goto_0
    return-void

    .line 170
    :cond_0
    iget-object v1, p0, Lio/realm/PassCourseResultRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v1}, Lio/realm/ProxyState;->getRow$realm()Lio/realm/internal/Row;

    move-result-object v0

    .line 171
    .local v0, "row":Lio/realm/internal/Row;
    invoke-interface {v0}, Lio/realm/internal/Row;->getTable()Lio/realm/internal/Table;

    move-result-object v1

    iget-object v2, p0, Lio/realm/PassCourseResultRealmProxy;->columnInfo:Lio/realm/PassCourseResultRealmProxy$PassCourseResultColumnInfo;

    iget-wide v2, v2, Lio/realm/PassCourseResultRealmProxy$PassCourseResultColumnInfo;->unixTimeIndex:J

    invoke-interface {v0}, Lio/realm/internal/Row;->getIndex()J

    move-result-wide v4

    const/4 v8, 0x1

    move-wide v6, p1

    invoke-virtual/range {v1 .. v8}, Lio/realm/internal/Table;->setLong(JJJZ)V

    goto :goto_0

    .line 175
    .end local v0    # "row":Lio/realm/internal/Row;
    :cond_1
    iget-object v1, p0, Lio/realm/PassCourseResultRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v1}, Lio/realm/ProxyState;->getRealm$realm()Lio/realm/BaseRealm;

    move-result-object v1

    invoke-virtual {v1}, Lio/realm/BaseRealm;->checkIfValid()V

    .line 176
    iget-object v1, p0, Lio/realm/PassCourseResultRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v1}, Lio/realm/ProxyState;->getRow$realm()Lio/realm/internal/Row;

    move-result-object v1

    iget-object v2, p0, Lio/realm/PassCourseResultRealmProxy;->columnInfo:Lio/realm/PassCourseResultRealmProxy$PassCourseResultColumnInfo;

    iget-wide v2, v2, Lio/realm/PassCourseResultRealmProxy$PassCourseResultColumnInfo;->unixTimeIndex:J

    invoke-interface {v1, v2, v3, p1, p2}, Lio/realm/internal/Row;->setLong(JJ)V

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 590
    invoke-static {p0}, Lio/realm/RealmObject;->isValid(Lio/realm/RealmModel;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 591
    const-string v1, "Invalid object"

    .line 610
    :goto_0
    return-object v1

    .line 593
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "PassCourseResult = ["

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 594
    .local v0, "stringBuilder":Ljava/lang/StringBuilder;
    const-string v1, "{id:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 595
    invoke-virtual {p0}, Lio/realm/PassCourseResultRealmProxy;->realmGet$id()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 596
    const-string v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 597
    const-string v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 598
    const-string v1, "{type:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 599
    invoke-virtual {p0}, Lio/realm/PassCourseResultRealmProxy;->realmGet$type()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 600
    const-string v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 601
    const-string v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 602
    const-string v1, "{score:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 603
    invoke-virtual {p0}, Lio/realm/PassCourseResultRealmProxy;->realmGet$score()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 604
    const-string v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 605
    const-string v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 606
    const-string v1, "{unixTime:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 607
    invoke-virtual {p0}, Lio/realm/PassCourseResultRealmProxy;->realmGet$unixTime()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 608
    const-string v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 609
    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 610
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method
