.class public interface abstract Lio/realm/WordBlockResultRealmProxyInterface;
.super Ljava/lang/Object;
.source "WordBlockResultRealmProxyInterface.java"


# virtual methods
.method public abstract realmGet$config()Lcom/safonov/speedreading/training/fragment/wordsblock/repository/entity/WordBlockConfig;
.end method

.method public abstract realmGet$id()I
.end method

.method public abstract realmGet$unixTime()J
.end method

.method public abstract realmSet$config(Lcom/safonov/speedreading/training/fragment/wordsblock/repository/entity/WordBlockConfig;)V
.end method

.method public abstract realmSet$id(I)V
.end method

.method public abstract realmSet$unixTime(J)V
.end method
