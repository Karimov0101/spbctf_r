.class public interface abstract Lio/realm/PassCourseResultRealmProxyInterface;
.super Ljava/lang/Object;
.source "PassCourseResultRealmProxyInterface.java"


# virtual methods
.method public abstract realmGet$id()I
.end method

.method public abstract realmGet$score()I
.end method

.method public abstract realmGet$type()I
.end method

.method public abstract realmGet$unixTime()J
.end method

.method public abstract realmSet$id(I)V
.end method

.method public abstract realmSet$score(I)V
.end method

.method public abstract realmSet$type(I)V
.end method

.method public abstract realmSet$unixTime(J)V
.end method
