.class public interface abstract Lio/realm/SchulteTableResultRealmProxyInterface;
.super Ljava/lang/Object;
.source "SchulteTableResultRealmProxyInterface.java"


# virtual methods
.method public abstract realmGet$config()Lcom/safonov/speedreading/training/fragment/schultetable/repository/entity/SchulteTableConfig;
.end method

.method public abstract realmGet$id()I
.end method

.method public abstract realmGet$time()J
.end method

.method public abstract realmGet$unixTime()J
.end method

.method public abstract realmSet$config(Lcom/safonov/speedreading/training/fragment/schultetable/repository/entity/SchulteTableConfig;)V
.end method

.method public abstract realmSet$id(I)V
.end method

.method public abstract realmSet$time(J)V
.end method

.method public abstract realmSet$unixTime(J)V
.end method
