.class public interface abstract Lio/realm/WordsColumnsResultRealmProxyInterface;
.super Ljava/lang/Object;
.source "WordsColumnsResultRealmProxyInterface.java"


# virtual methods
.method public abstract realmGet$config()Lcom/safonov/speedreading/training/fragment/wordscolumns/repository/entity/WordsColumnsConfig;
.end method

.method public abstract realmGet$id()I
.end method

.method public abstract realmGet$unixTime()J
.end method

.method public abstract realmSet$config(Lcom/safonov/speedreading/training/fragment/wordscolumns/repository/entity/WordsColumnsConfig;)V
.end method

.method public abstract realmSet$id(I)V
.end method

.method public abstract realmSet$unixTime(J)V
.end method
