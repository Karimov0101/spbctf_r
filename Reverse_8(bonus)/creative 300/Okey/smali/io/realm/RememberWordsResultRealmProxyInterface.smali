.class public interface abstract Lio/realm/RememberWordsResultRealmProxyInterface;
.super Ljava/lang/Object;
.source "RememberWordsResultRealmProxyInterface.java"


# virtual methods
.method public abstract realmGet$config()Lcom/safonov/speedreading/training/fragment/rememberwords/repository/entity/RememberWordsConfig;
.end method

.method public abstract realmGet$id()I
.end method

.method public abstract realmGet$score()I
.end method

.method public abstract realmSet$config(Lcom/safonov/speedreading/training/fragment/rememberwords/repository/entity/RememberWordsConfig;)V
.end method

.method public abstract realmSet$id(I)V
.end method

.method public abstract realmSet$score(I)V
.end method
