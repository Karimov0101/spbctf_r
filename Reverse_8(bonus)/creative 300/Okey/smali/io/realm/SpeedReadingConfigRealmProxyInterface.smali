.class public interface abstract Lio/realm/SpeedReadingConfigRealmProxyInterface;
.super Ljava/lang/Object;
.source "SpeedReadingConfigRealmProxyInterface.java"


# virtual methods
.method public abstract realmGet$id()I
.end method

.method public abstract realmGet$maxWordShowCount()I
.end method

.method public abstract realmGet$minSpeed()I
.end method

.method public abstract realmGet$minWordShowCount()I
.end method

.method public abstract realmGet$speed()I
.end method

.method public abstract realmGet$speedStep()I
.end method

.method public abstract realmGet$trainingShowCount()I
.end method

.method public abstract realmSet$id(I)V
.end method

.method public abstract realmSet$maxWordShowCount(I)V
.end method

.method public abstract realmSet$minSpeed(I)V
.end method

.method public abstract realmSet$minWordShowCount(I)V
.end method

.method public abstract realmSet$speed(I)V
.end method

.method public abstract realmSet$speedStep(I)V
.end method

.method public abstract realmSet$trainingShowCount(I)V
.end method
