.class final Lio/realm/GreenDotConfigRealmProxy$GreenDotConfigColumnInfo;
.super Lio/realm/internal/ColumnInfo;
.source "GreenDotConfigRealmProxy.java"

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lio/realm/GreenDotConfigRealmProxy;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = "GreenDotConfigColumnInfo"
.end annotation


# instance fields
.field public durationIndex:J

.field public idIndex:J


# direct methods
.method constructor <init>(Ljava/lang/String;Lio/realm/internal/Table;)V
    .locals 4
    .param p1, "path"    # Ljava/lang/String;
    .param p2, "table"    # Lio/realm/internal/Table;

    .prologue
    .line 40
    invoke-direct {p0}, Lio/realm/internal/ColumnInfo;-><init>()V

    .line 41
    new-instance v0, Ljava/util/HashMap;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, Ljava/util/HashMap;-><init>(I)V

    .line 42
    .local v0, "indicesMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Long;>;"
    const-string v1, "GreenDotConfig"

    const-string v2, "id"

    invoke-virtual {p0, p1, p2, v1, v2}, Lio/realm/GreenDotConfigRealmProxy$GreenDotConfigColumnInfo;->getValidColumnIndex(Ljava/lang/String;Lio/realm/internal/Table;Ljava/lang/String;Ljava/lang/String;)J

    move-result-wide v2

    iput-wide v2, p0, Lio/realm/GreenDotConfigRealmProxy$GreenDotConfigColumnInfo;->idIndex:J

    .line 43
    const-string v1, "id"

    iget-wide v2, p0, Lio/realm/GreenDotConfigRealmProxy$GreenDotConfigColumnInfo;->idIndex:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 44
    const-string v1, "GreenDotConfig"

    const-string v2, "duration"

    invoke-virtual {p0, p1, p2, v1, v2}, Lio/realm/GreenDotConfigRealmProxy$GreenDotConfigColumnInfo;->getValidColumnIndex(Ljava/lang/String;Lio/realm/internal/Table;Ljava/lang/String;Ljava/lang/String;)J

    move-result-wide v2

    iput-wide v2, p0, Lio/realm/GreenDotConfigRealmProxy$GreenDotConfigColumnInfo;->durationIndex:J

    .line 45
    const-string v1, "duration"

    iget-wide v2, p0, Lio/realm/GreenDotConfigRealmProxy$GreenDotConfigColumnInfo;->durationIndex:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 47
    invoke-virtual {p0, v0}, Lio/realm/GreenDotConfigRealmProxy$GreenDotConfigColumnInfo;->setIndicesMap(Ljava/util/Map;)V

    .line 48
    return-void
.end method


# virtual methods
.method public final clone()Lio/realm/GreenDotConfigRealmProxy$GreenDotConfigColumnInfo;
    .locals 1

    .prologue
    .line 61
    invoke-super {p0}, Lio/realm/internal/ColumnInfo;->clone()Lio/realm/internal/ColumnInfo;

    move-result-object v0

    check-cast v0, Lio/realm/GreenDotConfigRealmProxy$GreenDotConfigColumnInfo;

    return-object v0
.end method

.method public bridge synthetic clone()Lio/realm/internal/ColumnInfo;
    .locals 1

    .prologue
    .line 34
    invoke-virtual {p0}, Lio/realm/GreenDotConfigRealmProxy$GreenDotConfigColumnInfo;->clone()Lio/realm/GreenDotConfigRealmProxy$GreenDotConfigColumnInfo;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 34
    invoke-virtual {p0}, Lio/realm/GreenDotConfigRealmProxy$GreenDotConfigColumnInfo;->clone()Lio/realm/GreenDotConfigRealmProxy$GreenDotConfigColumnInfo;

    move-result-object v0

    return-object v0
.end method

.method public final copyColumnInfoFrom(Lio/realm/internal/ColumnInfo;)V
    .locals 4
    .param p1, "other"    # Lio/realm/internal/ColumnInfo;

    .prologue
    .line 52
    move-object v0, p1

    check-cast v0, Lio/realm/GreenDotConfigRealmProxy$GreenDotConfigColumnInfo;

    .line 53
    .local v0, "otherInfo":Lio/realm/GreenDotConfigRealmProxy$GreenDotConfigColumnInfo;
    iget-wide v2, v0, Lio/realm/GreenDotConfigRealmProxy$GreenDotConfigColumnInfo;->idIndex:J

    iput-wide v2, p0, Lio/realm/GreenDotConfigRealmProxy$GreenDotConfigColumnInfo;->idIndex:J

    .line 54
    iget-wide v2, v0, Lio/realm/GreenDotConfigRealmProxy$GreenDotConfigColumnInfo;->durationIndex:J

    iput-wide v2, p0, Lio/realm/GreenDotConfigRealmProxy$GreenDotConfigColumnInfo;->durationIndex:J

    .line 56
    invoke-virtual {v0}, Lio/realm/GreenDotConfigRealmProxy$GreenDotConfigColumnInfo;->getIndicesMap()Ljava/util/Map;

    move-result-object v1

    invoke-virtual {p0, v1}, Lio/realm/GreenDotConfigRealmProxy$GreenDotConfigColumnInfo;->setIndicesMap(Ljava/util/Map;)V

    .line 57
    return-void
.end method
