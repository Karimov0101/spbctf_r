.class final Lio/realm/WordPairsConfigRealmProxy$WordPairsConfigColumnInfo;
.super Lio/realm/internal/ColumnInfo;
.source "WordPairsConfigRealmProxy.java"

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lio/realm/WordPairsConfigRealmProxy;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = "WordPairsConfigColumnInfo"
.end annotation


# instance fields
.field public columnCountIndex:J

.field public differentWordPairsCountIndex:J

.field public idIndex:J

.field public rowCountIndex:J

.field public trainingDurationIndex:J


# direct methods
.method constructor <init>(Ljava/lang/String;Lio/realm/internal/Table;)V
    .locals 4
    .param p1, "path"    # Ljava/lang/String;
    .param p2, "table"    # Lio/realm/internal/Table;

    .prologue
    .line 43
    invoke-direct {p0}, Lio/realm/internal/ColumnInfo;-><init>()V

    .line 44
    new-instance v0, Ljava/util/HashMap;

    const/4 v1, 0x5

    invoke-direct {v0, v1}, Ljava/util/HashMap;-><init>(I)V

    .line 45
    .local v0, "indicesMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Long;>;"
    const-string v1, "WordPairsConfig"

    const-string v2, "id"

    invoke-virtual {p0, p1, p2, v1, v2}, Lio/realm/WordPairsConfigRealmProxy$WordPairsConfigColumnInfo;->getValidColumnIndex(Ljava/lang/String;Lio/realm/internal/Table;Ljava/lang/String;Ljava/lang/String;)J

    move-result-wide v2

    iput-wide v2, p0, Lio/realm/WordPairsConfigRealmProxy$WordPairsConfigColumnInfo;->idIndex:J

    .line 46
    const-string v1, "id"

    iget-wide v2, p0, Lio/realm/WordPairsConfigRealmProxy$WordPairsConfigColumnInfo;->idIndex:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 47
    const-string v1, "WordPairsConfig"

    const-string v2, "rowCount"

    invoke-virtual {p0, p1, p2, v1, v2}, Lio/realm/WordPairsConfigRealmProxy$WordPairsConfigColumnInfo;->getValidColumnIndex(Ljava/lang/String;Lio/realm/internal/Table;Ljava/lang/String;Ljava/lang/String;)J

    move-result-wide v2

    iput-wide v2, p0, Lio/realm/WordPairsConfigRealmProxy$WordPairsConfigColumnInfo;->rowCountIndex:J

    .line 48
    const-string v1, "rowCount"

    iget-wide v2, p0, Lio/realm/WordPairsConfigRealmProxy$WordPairsConfigColumnInfo;->rowCountIndex:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 49
    const-string v1, "WordPairsConfig"

    const-string v2, "columnCount"

    invoke-virtual {p0, p1, p2, v1, v2}, Lio/realm/WordPairsConfigRealmProxy$WordPairsConfigColumnInfo;->getValidColumnIndex(Ljava/lang/String;Lio/realm/internal/Table;Ljava/lang/String;Ljava/lang/String;)J

    move-result-wide v2

    iput-wide v2, p0, Lio/realm/WordPairsConfigRealmProxy$WordPairsConfigColumnInfo;->columnCountIndex:J

    .line 50
    const-string v1, "columnCount"

    iget-wide v2, p0, Lio/realm/WordPairsConfigRealmProxy$WordPairsConfigColumnInfo;->columnCountIndex:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 51
    const-string v1, "WordPairsConfig"

    const-string v2, "differentWordPairsCount"

    invoke-virtual {p0, p1, p2, v1, v2}, Lio/realm/WordPairsConfigRealmProxy$WordPairsConfigColumnInfo;->getValidColumnIndex(Ljava/lang/String;Lio/realm/internal/Table;Ljava/lang/String;Ljava/lang/String;)J

    move-result-wide v2

    iput-wide v2, p0, Lio/realm/WordPairsConfigRealmProxy$WordPairsConfigColumnInfo;->differentWordPairsCountIndex:J

    .line 52
    const-string v1, "differentWordPairsCount"

    iget-wide v2, p0, Lio/realm/WordPairsConfigRealmProxy$WordPairsConfigColumnInfo;->differentWordPairsCountIndex:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 53
    const-string v1, "WordPairsConfig"

    const-string v2, "trainingDuration"

    invoke-virtual {p0, p1, p2, v1, v2}, Lio/realm/WordPairsConfigRealmProxy$WordPairsConfigColumnInfo;->getValidColumnIndex(Ljava/lang/String;Lio/realm/internal/Table;Ljava/lang/String;Ljava/lang/String;)J

    move-result-wide v2

    iput-wide v2, p0, Lio/realm/WordPairsConfigRealmProxy$WordPairsConfigColumnInfo;->trainingDurationIndex:J

    .line 54
    const-string v1, "trainingDuration"

    iget-wide v2, p0, Lio/realm/WordPairsConfigRealmProxy$WordPairsConfigColumnInfo;->trainingDurationIndex:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 56
    invoke-virtual {p0, v0}, Lio/realm/WordPairsConfigRealmProxy$WordPairsConfigColumnInfo;->setIndicesMap(Ljava/util/Map;)V

    .line 57
    return-void
.end method


# virtual methods
.method public final clone()Lio/realm/WordPairsConfigRealmProxy$WordPairsConfigColumnInfo;
    .locals 1

    .prologue
    .line 73
    invoke-super {p0}, Lio/realm/internal/ColumnInfo;->clone()Lio/realm/internal/ColumnInfo;

    move-result-object v0

    check-cast v0, Lio/realm/WordPairsConfigRealmProxy$WordPairsConfigColumnInfo;

    return-object v0
.end method

.method public bridge synthetic clone()Lio/realm/internal/ColumnInfo;
    .locals 1

    .prologue
    .line 34
    invoke-virtual {p0}, Lio/realm/WordPairsConfigRealmProxy$WordPairsConfigColumnInfo;->clone()Lio/realm/WordPairsConfigRealmProxy$WordPairsConfigColumnInfo;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 34
    invoke-virtual {p0}, Lio/realm/WordPairsConfigRealmProxy$WordPairsConfigColumnInfo;->clone()Lio/realm/WordPairsConfigRealmProxy$WordPairsConfigColumnInfo;

    move-result-object v0

    return-object v0
.end method

.method public final copyColumnInfoFrom(Lio/realm/internal/ColumnInfo;)V
    .locals 4
    .param p1, "other"    # Lio/realm/internal/ColumnInfo;

    .prologue
    .line 61
    move-object v0, p1

    check-cast v0, Lio/realm/WordPairsConfigRealmProxy$WordPairsConfigColumnInfo;

    .line 62
    .local v0, "otherInfo":Lio/realm/WordPairsConfigRealmProxy$WordPairsConfigColumnInfo;
    iget-wide v2, v0, Lio/realm/WordPairsConfigRealmProxy$WordPairsConfigColumnInfo;->idIndex:J

    iput-wide v2, p0, Lio/realm/WordPairsConfigRealmProxy$WordPairsConfigColumnInfo;->idIndex:J

    .line 63
    iget-wide v2, v0, Lio/realm/WordPairsConfigRealmProxy$WordPairsConfigColumnInfo;->rowCountIndex:J

    iput-wide v2, p0, Lio/realm/WordPairsConfigRealmProxy$WordPairsConfigColumnInfo;->rowCountIndex:J

    .line 64
    iget-wide v2, v0, Lio/realm/WordPairsConfigRealmProxy$WordPairsConfigColumnInfo;->columnCountIndex:J

    iput-wide v2, p0, Lio/realm/WordPairsConfigRealmProxy$WordPairsConfigColumnInfo;->columnCountIndex:J

    .line 65
    iget-wide v2, v0, Lio/realm/WordPairsConfigRealmProxy$WordPairsConfigColumnInfo;->differentWordPairsCountIndex:J

    iput-wide v2, p0, Lio/realm/WordPairsConfigRealmProxy$WordPairsConfigColumnInfo;->differentWordPairsCountIndex:J

    .line 66
    iget-wide v2, v0, Lio/realm/WordPairsConfigRealmProxy$WordPairsConfigColumnInfo;->trainingDurationIndex:J

    iput-wide v2, p0, Lio/realm/WordPairsConfigRealmProxy$WordPairsConfigColumnInfo;->trainingDurationIndex:J

    .line 68
    invoke-virtual {v0}, Lio/realm/WordPairsConfigRealmProxy$WordPairsConfigColumnInfo;->getIndicesMap()Ljava/util/Map;

    move-result-object v1

    invoke-virtual {p0, v1}, Lio/realm/WordPairsConfigRealmProxy$WordPairsConfigColumnInfo;->setIndicesMap(Ljava/util/Map;)V

    .line 69
    return-void
.end method
