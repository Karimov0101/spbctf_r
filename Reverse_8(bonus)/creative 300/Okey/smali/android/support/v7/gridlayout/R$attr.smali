.class public final Landroid/support/v7/gridlayout/R$attr;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/support/v7/gridlayout/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "attr"
.end annotation


# static fields
.field public static final alignmentMode:I = 0x7f04002a

.field public static final columnCount:I = 0x7f040071

.field public static final columnOrderPreserved:I = 0x7f040072

.field public static final font:I = 0x7f0400ba

.field public static final fontProviderAuthority:I = 0x7f0400bc

.field public static final fontProviderCerts:I = 0x7f0400bd

.field public static final fontProviderFetchStrategy:I = 0x7f0400be

.field public static final fontProviderFetchTimeout:I = 0x7f0400bf

.field public static final fontProviderPackage:I = 0x7f0400c0

.field public static final fontProviderQuery:I = 0x7f0400c1

.field public static final fontStyle:I = 0x7f0400c2

.field public static final fontWeight:I = 0x7f0400c3

.field public static final layout_column:I = 0x7f0400ee

.field public static final layout_columnSpan:I = 0x7f0400ef

.field public static final layout_columnWeight:I = 0x7f0400f0

.field public static final layout_gravity:I = 0x7f040123

.field public static final layout_row:I = 0x7f040127

.field public static final layout_rowSpan:I = 0x7f040128

.field public static final layout_rowWeight:I = 0x7f040129

.field public static final orientation:I = 0x7f04014a

.field public static final rowCount:I = 0x7f040187

.field public static final rowOrderPreserved:I = 0x7f040188

.field public static final useDefaultMargins:I = 0x7f0401f7


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
