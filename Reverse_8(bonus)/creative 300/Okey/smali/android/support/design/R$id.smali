.class public final Landroid/support/design/R$id;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/support/design/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "id"
.end annotation


# static fields
.field public static final action0:I = 0x7f090008

.field public static final action_bar:I = 0x7f090009

.field public static final action_bar_activity_content:I = 0x7f09000a

.field public static final action_bar_container:I = 0x7f09000b

.field public static final action_bar_root:I = 0x7f09000d

.field public static final action_bar_spinner:I = 0x7f09000e

.field public static final action_bar_subtitle:I = 0x7f09000f

.field public static final action_bar_title:I = 0x7f090010

.field public static final action_container:I = 0x7f090011

.field public static final action_context_bar:I = 0x7f090012

.field public static final action_divider:I = 0x7f090013

.field public static final action_image:I = 0x7f090015

.field public static final action_menu_divider:I = 0x7f090017

.field public static final action_menu_presenter:I = 0x7f090018

.field public static final action_mode_bar:I = 0x7f090019

.field public static final action_mode_bar_stub:I = 0x7f09001a

.field public static final action_mode_close_button:I = 0x7f09001b

.field public static final action_text:I = 0x7f09001e

.field public static final actions:I = 0x7f090020

.field public static final activity_chooser_view_content:I = 0x7f090021

.field public static final add:I = 0x7f090023

.field public static final alertTitle:I = 0x7f090024

.field public static final async:I = 0x7f09002c

.field public static final auto:I = 0x7f09002f

.field public static final blocking:I = 0x7f09003b

.field public static final bottom:I = 0x7f090042

.field public static final buttonPanel:I = 0x7f090044

.field public static final cancel_action:I = 0x7f090045

.field public static final center:I = 0x7f090046

.field public static final checkbox:I = 0x7f09004a

.field public static final chronometer:I = 0x7f09004b

.field public static final container:I = 0x7f090064

.field public static final contentPanel:I = 0x7f090065

.field public static final coordinator:I = 0x7f09006b

.field public static final custom:I = 0x7f090070

.field public static final customPanel:I = 0x7f090071

.field public static final decor_content_parent:I = 0x7f090072

.field public static final default_activity_button:I = 0x7f090073

.field public static final design_bottom_sheet:I = 0x7f090077

.field public static final design_menu_item_action_area:I = 0x7f090078

.field public static final design_menu_item_action_area_stub:I = 0x7f090079

.field public static final design_menu_item_text:I = 0x7f09007a

.field public static final design_navigation_view:I = 0x7f09007b

.field public static final edit_query:I = 0x7f090083

.field public static final end:I = 0x7f090085

.field public static final end_padder:I = 0x7f090086

.field public static final expand_activities_button:I = 0x7f09008c

.field public static final expanded_menu:I = 0x7f09008d

.field public static final fill:I = 0x7f090092

.field public static final fixed:I = 0x7f090095

.field public static final forever:I = 0x7f09009b

.field public static final ghost_view:I = 0x7f0900a0

.field public static final home:I = 0x7f0900ab

.field public static final icon:I = 0x7f0900af

.field public static final icon_group:I = 0x7f0900b1

.field public static final image:I = 0x7f0900b3

.field public static final info:I = 0x7f0900b8

.field public static final italic:I = 0x7f0900be

.field public static final item_touch_helper_previous_elevation:I = 0x7f0900d0

.field public static final largeLabel:I = 0x7f0900d5

.field public static final left:I = 0x7f0900d6

.field public static final line1:I = 0x7f0900d9

.field public static final line3:I = 0x7f0900da

.field public static final listMode:I = 0x7f0900e5

.field public static final list_item:I = 0x7f0900e6

.field public static final masked:I = 0x7f0900ea

.field public static final media_actions:I = 0x7f090104

.field public static final message:I = 0x7f090105

.field public static final mini:I = 0x7f090107

.field public static final multiply:I = 0x7f09010d

.field public static final navigation_header_container:I = 0x7f09010f

.field public static final none:I = 0x7f09011f

.field public static final normal:I = 0x7f090120

.field public static final notification_background:I = 0x7f090121

.field public static final notification_main_column:I = 0x7f090122

.field public static final notification_main_column_container:I = 0x7f090123

.field public static final parallax:I = 0x7f09012b

.field public static final parentPanel:I = 0x7f09012d

.field public static final parent_matrix:I = 0x7f09012e

.field public static final pin:I = 0x7f090137

.field public static final progress_circular:I = 0x7f09014f

.field public static final progress_horizontal:I = 0x7f090150

.field public static final radio:I = 0x7f090156

.field public static final right:I = 0x7f090176

.field public static final right_icon:I = 0x7f090177

.field public static final right_side:I = 0x7f090178

.field public static final save_image_matrix:I = 0x7f09017b

.field public static final save_non_transition_alpha:I = 0x7f09017c

.field public static final save_scale_type:I = 0x7f09017d

.field public static final screen:I = 0x7f09018a

.field public static final scrollIndicatorDown:I = 0x7f09018c

.field public static final scrollIndicatorUp:I = 0x7f09018d

.field public static final scrollView:I = 0x7f09018e

.field public static final scrollable:I = 0x7f09018f

.field public static final search_badge:I = 0x7f090190

.field public static final search_bar:I = 0x7f090191

.field public static final search_button:I = 0x7f090192

.field public static final search_close_btn:I = 0x7f090193

.field public static final search_edit_frame:I = 0x7f090194

.field public static final search_go_btn:I = 0x7f090195

.field public static final search_mag_icon:I = 0x7f090196

.field public static final search_plate:I = 0x7f090197

.field public static final search_src_text:I = 0x7f090198

.field public static final search_voice_btn:I = 0x7f090199

.field public static final select_dialog_listview:I = 0x7f09019c

.field public static final shortcut:I = 0x7f09019f

.field public static final smallLabel:I = 0x7f0901a5

.field public static final snackbar_action:I = 0x7f0901a6

.field public static final snackbar_text:I = 0x7f0901a7

.field public static final spacer:I = 0x7f0901a9

.field public static final split_action_bar:I = 0x7f0901ca

.field public static final src_atop:I = 0x7f0901cd

.field public static final src_in:I = 0x7f0901ce

.field public static final src_over:I = 0x7f0901cf

.field public static final start:I = 0x7f0901d1

.field public static final status_bar_latest_event_content:I = 0x7f0901d6

.field public static final submenuarrow:I = 0x7f0901d7

.field public static final submit_area:I = 0x7f0901d8

.field public static final tabMode:I = 0x7f0901db

.field public static final text:I = 0x7f0901de

.field public static final text2:I = 0x7f0901df

.field public static final textSpacerNoButtons:I = 0x7f0901e0

.field public static final textSpacerNoTitle:I = 0x7f0901e1

.field public static final text_input_password_toggle:I = 0x7f090202

.field public static final textinput_counter:I = 0x7f090203

.field public static final textinput_error:I = 0x7f090204

.field public static final time:I = 0x7f090206

.field public static final title:I = 0x7f090209

.field public static final titleDividerNoCustom:I = 0x7f09020a

.field public static final title_template:I = 0x7f09020b

.field public static final top:I = 0x7f09020e

.field public static final topPanel:I = 0x7f09020f

.field public static final touch_outside:I = 0x7f090210

.field public static final transition_current_scene:I = 0x7f090229

.field public static final transition_layout_save:I = 0x7f09022a

.field public static final transition_position:I = 0x7f09022b

.field public static final transition_scene_layoutid_cache:I = 0x7f09022c

.field public static final transition_transform:I = 0x7f09022d

.field public static final uniform:I = 0x7f09023e

.field public static final up:I = 0x7f09023f

.field public static final view_offset_helper:I = 0x7f090242

.field public static final visible:I = 0x7f090244

.field public static final wrap_content:I = 0x7f09024b


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 748
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
