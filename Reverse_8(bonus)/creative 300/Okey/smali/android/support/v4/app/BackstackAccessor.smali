.class public Landroid/support/v4/app/BackstackAccessor;
.super Ljava/lang/Object;
.source "BackstackAccessor.java"


# direct methods
.method private constructor <init>()V
    .locals 2

    .prologue
    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Not instantiatable"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public static isFragmentOnBackStack(Landroid/support/v4/app/Fragment;)Z
    .locals 1
    .param p0, "fragment"    # Landroid/support/v4/app/Fragment;

    .prologue
    .line 40
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->isInBackStack()Z

    move-result v0

    return v0
.end method
